using System.Runtime.CompilerServices;
using UnityEngine;

public static class ExtensionMethods 
{
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static bool isAssigned(this MonoBehaviour o) {
		return !ReferenceEquals(o, null);
	}
}