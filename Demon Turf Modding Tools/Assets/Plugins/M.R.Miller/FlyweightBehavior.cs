﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyweightBehavior<T, TData, TManager> : MonoBehaviour, IFlyweight
	where T : FlyweightBehavior<T, TData, TManager>
	where TData : struct
	where TManager : FlyweightManager<T, TData, TManager> {
	private TManager _manager;
	protected TManager manager {
		get {
		
			if (!_manager.isAssigned()) {
				_manager = FlyweightManager<T, TData, TManager>.getInstance(); //findManager();
			}
			return _manager;
		}
	}
	TData localData;
	protected int index = -1;

	protected ref TData data {
		get {
			if (index == -1) return ref localData;
			return ref manager[index];
		}
	}
	
//   TManager findManager() {
// #if UNITY_EDITOR
//     if (!UnityEditor.EditorApplication.isPlaying) {
//       GameObject manager = GameObject.Find(typeof(TManager).Name);
//       if (manager == null) {
//         manager = new GameObject(typeof(TManager).Name);
//         manager.hideFlags = HideFlags.DontSave;
//         return manager.AddComponent<TManager>();
//       }
//       return manager.GetComponent<TManager>();
//     }
// #endif // UNITY_EDITOR
//     return lookupManager();
//   }

	protected virtual void OnEnable() {
		index = manager.register((T)this);
		manager[index] = localData;
	}

	protected virtual void OnDisable() {
		if (index >= 0)
		{
			manager.unregister((T)this, index);
			index = -1;
		}
	}
	
	public void OnBecameVisible() {
		if (!enabled) return;
		_manager.becameVisible(index);
	}
	
	public void OnBecameInvisible() {
		if (!enabled) return;
		_manager.becameInvisible(index);
	}

	void IFlyweight.ForceRegister () 
	{
		if (index < 0)
			OnEnable ();
	} 
}

public interface IFlyweight 
{
	void ForceRegister ();
}