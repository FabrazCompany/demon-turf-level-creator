﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Jobs;
using Unity.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif // UNITY_EDITOR
using Unity.Jobs;

public class FlyweightManager<T, TData, TManager> : MonoBehaviour
	where T : FlyweightBehavior<T, TData, TManager>
	where TData : struct
	where TManager : FlyweightManager<T, TData, TManager> {
	struct VariableHider {}
	private new VariableHider transform = default;
	
	protected struct Slot {
		public T flyweight;
		public int jobIndex;
		public TData data;
		public bool active;
		public bool visible;
	}
	private PooledList<int> _freeSlots;
	private bool _freeSlotsAssigned = false;
	protected ref PooledList<int> freeSlots {
		get {
			if (!_freeSlotsAssigned) {
				_freeSlots = new PooledList<int>(2048);
				_freeSlotsAssigned = true;
			}
			return ref _freeSlots;
		}
	}
	private PooledList<Slot> _slots;
	private bool _slotsAssigned = false;
	protected ref PooledList<Slot> slots {
		get {
			if (!_slotsAssigned) {
				_slots = new PooledList<Slot>(2048);
				_slotsAssigned = true;
			}
			return ref _slots;
		}
	}
	private static TManager instance;
	private static TManager findOrMakeInstance() {
		string name = typeof(TManager).Name;
		var go = GameObject.Find(name);
		if (go != null) return go.GetComponent<TManager>();
		
		go = new GameObject(typeof(TManager).Name);
		if (Application.isPlaying) {
			DontDestroyOnLoad(go);
		}
		go.hideFlags = HideFlags.HideAndDontSave;
		var manager = go.AddComponent<TManager>();
	#if UNITY_EDITOR
		AssemblyReloadEvents.beforeAssemblyReload += manager.beforeAssemblyReload;
		EditorApplication.playModeStateChanged += manager.playModeStateChanged;
	#endif // UNITY_EDITOR
		return manager;
	}
	
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static TManager getInstance() {
		if (!instance.isAssigned()) {
			instance = findOrMakeInstance();
		}
		return instance;
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public int register(T flyweight) {
		
		if (_freeSlotsAssigned && _freeSlots.Count > 0) {
			int index = _freeSlots.PopBack();
			slots[index] = new Slot { flyweight = flyweight, active = true };
			return index;
		}
		slots.Add(new Slot { flyweight = flyweight, active = true });
		return slots.Count - 1;
	}
	
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public void unregister(T flyweight, int index) {
		if (_slotsAssigned && index < _slots.Count) {
			ref var slot = ref _slots[index];
			Debug.Assert(slot.flyweight == flyweight);
			slot.flyweight = null;
			slot.active = false;
			slot.jobIndex = -1;
			freeSlots.Add(index);
		}
	}
	
	public virtual void unregisterAll() {
		if (_slotsAssigned) {
			_slots.Clear();
			_slots.Dispose();
			_slotsAssigned = false;
		}
		if (_freeSlotsAssigned) {
			_freeSlots.Clear();
			_freeSlots.Dispose();
			_freeSlotsAssigned = false;
		}
	}


	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public void becameVisible(int index) {
		if (index < 0) return;
		_slots[index].visible = true;
	}
	
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public void becameInvisible(int index) {
		if (index < 0) return;
		_slots[index].visible = false;
	}
	
	public ref TData this[int index] {
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		get => ref slots[index].data;
	}

	void OnDestroy() {
		unregisterAll();
	}
	
#if UNITY_EDITOR  
	static void destroyInstance() {
		if (instance.isAssigned()) {
			DestroyImmediate(instance.gameObject);
			instance = null;
		}
	}
	void beforeAssemblyReload() {
		destroyInstance();
	}

	void playModeStateChanged(PlayModeStateChange state) {
		if (state == PlayModeStateChange.ExitingEditMode) {
			destroyInstance();
		}
	}
#endif // UNITY_EDITOR
}



// public class FlyweightManagerTransformJob<T, TData, TJobData, TManager> : FlyweightManager<T, TData, TManager>
// 	where T : FlyweightTransformBehavior<T, TData, TJobData, TManager>
// 	where TData : struct
// 	where TJobData : struct
// 	where TManager : FlyweightManagerTransformJob<T, TData, TJobData, TManager> {
	
// 	protected virtual int initialCapacity => 1024;
// 	private TransformAccessArray _transforms;
// 	private NativeArray<int> _transformIndexMap;
// 	private bool _transformsAssigned = false;
// 	protected ref TransformAccessArray transforms {
// 		get {
// 			if (!_transformsAssigned) {
// 				_transforms = new TransformAccessArray(slots.Capacity);
// 				_transformIndexMap = new NativeArray<int>(slots.Capacity, Allocator.Persistent);
// 				_transformsAssigned = true;
// 			}
// 			return ref _transforms;
// 		}
// 	}

// 	protected ref NativeArray<int> transformIndexMap {
// 		get {
// 			Debug.Assert(_transformsAssigned);
// 			return ref _transformIndexMap;
// 		}
// 	}

// 	private NativeArray<TData> _dataFields;
// 	private bool _dataFieldsAssigned = false;
// 	protected ref NativeArray<TData> dataFields {
// 		get {
// 			if (!_dataFieldsAssigned) {
// 				_dataFields = new NativeArray<TData>(slots.Capacity, Allocator.Persistent);
// 				_dataFieldsAssigned = true;
// 			}
// 			return ref _dataFields;
// 		}
// 	}

// 	protected int numJobData => transforms.length;

// 	public int registerJobData(int slotIndex, ref TData data, Transform _tr)
// 	{
// 		ref var t = ref transforms;
// 		t.Add(_tr);
// 		int jobIndex = t.length - 1;
// 		// TODO: Resize array to fit if length is too large
// 		dataFields[jobIndex] = data;
// 		transformIndexMap[jobIndex] = slotIndex;
// 		return jobIndex;
// 	}
// 	public void unregisterJobData(int jobIndex)
// 	{
// 		if (_transformsAssigned && jobIndex < _transforms.length)
// 		{
// 			_transforms.RemoveAtSwapBack(jobIndex);
// 			if (_transforms.length > 0)
// 			{
// 				int movedSlotIndex = _transformIndexMap[_transforms.length];
// 				_transformIndexMap[jobIndex] = movedSlotIndex;
// 				slots[movedSlotIndex].flyweight.setJobIndex (jobIndex);
// 			}
// 			if (_dataFieldsAssigned)
// 			{
// 				_dataFields[jobIndex] = _dataFields[_transforms.length];
// 			}
// 		}
// 	}

// 	public void setJobData(int jobIndex, ref TData _data)
// 	{
// 		dataFields[jobIndex] = _data;
// 	}

// 	public override void unregisterAll()
// 	{
// 		base.unregisterAll();
// 		if (_transformsAssigned) {
// 			_transforms.Dispose();
// 			_transformIndexMap.Dispose();
// 			_transformsAssigned = false;
// 		}
// 		if (_dataFieldsAssigned) {
// 			_dataFields.Dispose();
// 			_dataFieldsAssigned = false;
// 		}
// 	} 
// }