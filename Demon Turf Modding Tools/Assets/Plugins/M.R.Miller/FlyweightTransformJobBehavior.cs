using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FlyweightTransformJobBehavior<T, TData, TJobData, TManager> : FlyweightBehavior<T, TData, TManager>
	where T : FlyweightTransformJobBehavior<T, TData, TJobData, TManager>
	where TData : struct
	where TJobData : struct
	where TManager : FlyweightTransformJobManager<T, TData, TJobData, TManager> {

	protected virtual bool SetTransform => false;
	private int jobIndex = -1;
	public void setJobIndex (int newIndex) => jobIndex = newIndex;
	protected TJobData getJobData => manager.getJobData (jobIndex);
	protected bool isSet => jobIndex >= 0;
	protected void UpdateJobData(ref TJobData jobData)
	{
		if (jobIndex < 0) return;
		manager.setJobData(jobIndex, ref jobData);
	}
	protected bool log;
	protected abstract TJobData InitializeJobData ();
	protected override void OnEnable() {
		base.OnEnable ();		
		jobIndex = manager.registerJobData (index, InitializeJobData (), transform);
		if (log)
			Debug.Log ($"Registering ({gameObject.GetInstanceID ()}) - Index: {index} - Job Index: {jobIndex}");
		Debug.Assert(jobIndex >= 0);
	}

	protected override void OnDisable() {
		Debug.Assert(jobIndex >= 0);
		if (jobIndex >= 0)
		{
			if (log)
				Debug.Log ($"Deregistering ({gameObject.GetInstanceID ()}) - Index: {index} - Job Index: {jobIndex}");
			manager.unregisterJobData(jobIndex);
			jobIndex = -1;
		}
		base.OnDisable ();
	}
}