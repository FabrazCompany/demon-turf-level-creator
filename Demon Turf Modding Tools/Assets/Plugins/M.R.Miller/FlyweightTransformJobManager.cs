using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Jobs;
using Unity.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif // UNITY_EDITOR
using Unity.Jobs;

public class FlyweightTransformJobManager<T, TData, TJobData, TManager> : FlyweightManager<T, TData, TManager>
	where T : FlyweightTransformJobBehavior<T, TData, TJobData, TManager>
	where TData : struct
	where TJobData : struct
	where TManager : FlyweightTransformJobManager<T, TData, TJobData, TManager> {
	
	protected virtual int initialCapacity => 1024;
	private TransformAccessArray _transforms;
	private NativeArray<int> _transformIndexMap;
	private bool _transformsAssigned = false;
	protected ref TransformAccessArray transforms {
		get {
			if (!_transformsAssigned) {
				_transforms = new TransformAccessArray(slots.Capacity);
				_transformIndexMap = new NativeArray<int>(slots.Capacity, Allocator.Persistent);
				_transformsAssigned = true;
			}
			return ref _transforms;
		}
	}

	protected ref NativeArray<int> transformIndexMap {
		get {
			Debug.Assert(_transformsAssigned);
			return ref _transformIndexMap;
		}
	}

	private NativeArray<TJobData> _dataFields;
	private bool _dataFieldsAssigned = false;
	protected ref NativeArray<TJobData> dataFields {
		get {
			if (!_dataFieldsAssigned) {
				_dataFields = new NativeArray<TJobData>(slots.Capacity, Allocator.Persistent);
				_dataFieldsAssigned = true;
			}
			return ref _dataFields;
		}
	}
	protected int numJobData => transforms.length;
	public int registerJobData(int slotIndex, TJobData data, Transform _tr)
	{
		ref var t = ref transforms;
		t.Add(_tr);
		int jobIndex = t.length - 1;
		// TODO: Resize array to fit if length is too large
		
		slots[slotIndex].jobIndex = jobIndex;
		dataFields[jobIndex] = data;
		transformIndexMap[jobIndex] = slotIndex;
		return jobIndex;
	}
	public void unregisterJobData(int jobIndex)
	{
		if (_transformsAssigned && jobIndex < _transforms.length)
		{
			_transforms.RemoveAtSwapBack(jobIndex);
			if (jobIndex < _transforms.length)
			{
				int movedSlotIndex = _transformIndexMap[_transforms.length];
				_transformIndexMap[jobIndex] = movedSlotIndex;
				slots[movedSlotIndex].jobIndex = jobIndex;
				slots[movedSlotIndex].flyweight.setJobIndex (jobIndex);
			}
			swapDataFields (jobIndex, _transforms.length);
		}
	}

	protected virtual void swapDataFields (int currentIndex, int lastIndex)
	{
		if (!_dataFieldsAssigned) return;
		_dataFields[currentIndex] = _dataFields[lastIndex];
	}

	public void setJobData(int jobIndex, ref TJobData _data) => dataFields[jobIndex] = _data;
	public TJobData getJobData (int jobIndex) => dataFields[jobIndex];

	public override void unregisterAll()
	{
		base.unregisterAll();
		if (_transformsAssigned) {
			_transforms.Dispose();
			_transformIndexMap.Dispose();
			_transformsAssigned = false;
		}
		if (_dataFieldsAssigned) {
			_dataFields.Dispose();
			_dataFieldsAssigned = false;
		}
	} 
}