using System;
using UnityEngine;

public class FlyweightVisibility : MonoBehaviour
{
	// [SerializeField] FlyweightBehavior flyweight;
	void OnBecomeVisible ()
	{
		// flyweight.OnBecomeVisible ();
	}
	void OnBecomeInvisible ()
	{
		// flyweight.OnBecomeInvisible ();
	}
}