﻿#if !UNITY_EDITOR
#undef CACHE_DEBUG
#endif // !UNITY_EDITOR
using System;
using System.Buffers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct PooledListIterator<T> : IEnumerator<T> {
  private T[] _list;
  private int _count;
  private int _index;

  public PooledListIterator(T[] list, int count) {
    _list = list;
    _index = -1;
    _count = count;
  }

  public bool MoveNext() {
    return ++_index < _count;
  }

  public void Reset() {
    _index = -1;
  }

  object IEnumerator.Current => _list[_index];

  public T Current => _list[_index];

  public void Dispose() {
  }
}

#if CACHE_DEBUG
public class PooledList<T> : TrackedDisposable<PooledList<T>> {
#else
public struct PooledList<T> : IDisposable {
#endif //CACHE_DEBUG
  private T[] _items;
  private bool _disposed;
  private int _count;

  public PooledList(int capacity = 0) {
    _items = null;
    _count = 0;
    _disposed = false;
    Capacity = capacity;
  }

  public PooledListIterator<T> GetEnumerator() {
    return new PooledListIterator<T>(_items, _count);
  }
  
    public static implicit operator bool(PooledList<T> o) {
#if CACHE_DEBUG
      return o != null && o.Capacity > 0;
#endif // CACHE_DEBUG
      return o.Capacity > 0;
    }
  
  public int Count => _count;
  public int Capacity {
    get => _items != null ? _items.Length : 0;
    set {
      if (value <= 0 || (_items != null && value < _items.Length)) return;
      var pool = ArrayPool<T>.Shared;
      var newItems = pool.Rent(Mathf.NextPowerOfTwo(value));
      if (_items != null) {
        Array.Copy(_items, 0, newItems, 0, _count);
        pool.Return(_items);
      }
      _items = newItems;
    }
  }
  
  public void Resize(int length) {
    if (length <= _count) return;
    if (length >= Capacity) {
      Capacity = length;
    }
    // Zero out the new elements
    for (int i = _count; i < length; ++ i) {
      _items[i] = default;
    }
    _count = length;
  }
  
  public T[] RawItems => _items;
  
  public Span<T> Span => new Span<T>(_items, 0, _count);

  public ref T this[int index] {
    get {
      if (index >= _count) throw new IndexOutOfRangeException();
      return ref _items[index];
    }
  }

  public void Add(T item) {
    if (_count >= Capacity) {
      Capacity = Math.Max(Capacity * 2, 4);
    }
    _items[_count++] = item;
  }
  
  public bool Contains(T item) {
    for (int i = 0; i < _count; ++ i) {
      if (EqualityComparer<T>.Default.Equals(_items[i], item)) return true;
    }
    return false;
  }
  
  public int IndexOf(T item) {
    return _count > 0 ? Array.IndexOf(_items, item) : -1;
  }
  
  public void Remove(T item) {
    if (_count <= 0) return;
    RemoveAt(IndexOf(item));
  }
  
  public void RemoveAt(int index) {
    if (index < 0 || index >= _count) return;
    Array.Copy(_items, index + 1, _items, index, _count - index);
    -- _count;
  }
  
  public void Sort(IComparer<T> comparer) {
    if (_count <= 0) return;
    Array.Sort(_items, 0, _count, comparer);
  }
  
  public void Sort() {
    if (_count <= 0) return;
    Array.Sort(_items, 0, _count);
  }
  
  public T PopBack() {
    if (_count == 0) throw new IndexOutOfRangeException();
    return _items[--_count];
  }
  
  // shamelessly ripped from Microsoft's reference implementation for List
  public delegate bool Matcher<TContext>(in T o, in TContext context);
  public int RemoveAll<TContext>(Matcher<TContext> match, in TContext context) {
    if( match == null) return 0;

    // Find the first item which needs to be removed.
    int freeIndex = 0;   // the first free slot in items array
    while (freeIndex < _count && !match(in _items[freeIndex], in context)) ++freeIndex;            
    if (freeIndex >= _count) return 0;
    
    int current = freeIndex + 1;
    while (current < _count) {
      // Find the first item which needs to be kept.
      while (current < _count && match(in _items[current], in context)) current++;            

      if (current < _count) {
        _items[freeIndex++] = _items[current++]; // copy item to the free slot.
      }
    }                       
    
    Array.Clear(_items, freeIndex, _count - freeIndex);
    int result = _count - freeIndex;
    _count = freeIndex;
    return result;
  }

  public void Clear() {
    _count = 0;
  }

#if CACHE_DEBUG
  public override bool disposed => _disposed;
  public override void Dispose() {
    base.Dispose();
#else
  public bool disposed => _disposed;
  public void Dispose() {
#endif // CACHE_DEBUG
    if (_items != null) {
      Debug.Assert(!disposed);
      ArrayPool<T>.Shared.Return(_items);
      _items = null;
      _count = 0;
    }
    _disposed = true;
  }
}
