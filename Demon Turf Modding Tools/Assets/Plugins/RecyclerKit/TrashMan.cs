using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR
using System.Linq;
#endif



public class TrashMan : MonoBehaviour
{
	/// <summary>
	/// access to the singleton
	/// </summary>
	public static TrashMan instance;

	/// <summary>
	/// stores the recycle bins and is used to populate the lookup Dictionaries at startup
	/// </summary>
	[HideInInspector]
	public List<TrashManRecycleBin> recycleBinCollection;

	/// <summary>
	/// this is how often in seconds TrashMan should cull excess objects. Setting this to 0 or a negative number will
	/// fully turn off automatic culling. You can then use the TrashManRecycleBin.cullExcessObjects method manually if
	/// you would still like to do any culling.
	/// </summary>
	public float cullExcessObjectsInterval = 10f;

	/// <summary>
	/// if true, DontDestroyOnLoad will be called on the TrashMan
	/// </summary>
	public bool persistBetweenScenes = false;

	/// <summary>
	/// uses the GameObject instanceId as its key for fast look-ups
	/// </summary>
	Dictionary<int,TrashManRecycleBin> _instanceIdToRecycleBin = new Dictionary<int,TrashManRecycleBin>();

	/// <summary>
	/// uses the pool name to find the GameObject instanceId
	/// </summary>
	Dictionary<string,int> _poolNameToInstanceId = new Dictionary<string,int>();

	// public static bool dynamicallyPopulatePool = true;

	[HideInInspector]
	public new Transform transform;

	bool isInitialized;
	public static bool IsInitialized => instance?.isInitialized ?? false;


	#region MonoBehaviour

	void Awake()
	{
		if( instance != null )
		{
			Destroy( gameObject );
		}
		else
		{
			transform = gameObject.transform;
			instance = this;
			initializePrefabPools();

			if( persistBetweenScenes )
				DontDestroyOnLoad( gameObject );
		}

		// only cull if we have an interval greater than 0
		if( cullExcessObjectsInterval > 0 )
			StartCoroutine( cullExcessObjects() );

		SceneManager.activeSceneChanged += activeSceneChanged;
	}


	void activeSceneChanged( Scene oldScene, Scene newScene )
	{
		if( oldScene.name == null )
			return;
		
		for( var i = recycleBinCollection.Count - 1; i >= 0; i-- )
		{
			if( !recycleBinCollection[i].persistBetweenScenes )
				removeRecycleBin( recycleBinCollection[i] );
		}
	}


	void OnApplicationQuit()
	{
		instance = null;
	}

	#endregion


	#region Private

	/// <summary>
	/// coroutine that runs every couple seconds and removes any objects created over the recycle bins limit
	/// </summary>
	/// <returns>The excess objects.</returns>
	IEnumerator cullExcessObjects()
	{
		var waiter = new WaitForSeconds( cullExcessObjectsInterval );

		while( true )
		{
			for( var i = 0; i < recycleBinCollection.Count; i++ )
				recycleBinCollection[i].cullExcessObjects();

			yield return waiter;
		}
	}


	/// <summary>
	/// populats the lookup dictionaries
	/// </summary>
	void initializePrefabPools()
	{
		if( recycleBinCollection == null )
			return;

		isInitialized = false;
		// var defaultSetting = dynamicallyPopulatePool;
		// dynamicallyPopulatePool = false;
		foreach( var recycleBin in recycleBinCollection )
		{
			if( recycleBin == null || recycleBin.prefab == null )
				continue;

			recycleBin.initialize();
			_instanceIdToRecycleBin.Add( recycleBin.prefab.GetInstanceID(), recycleBin );
			_poolNameToInstanceId.Add( recycleBin.prefab.name, recycleBin.prefab.GetInstanceID() );
		}
		isInitialized = true;
		// dynamicallyPopulatePool = defaultSetting;
	}


	/// <summary>
	/// internal method that actually does the work of grabbing the item from the bin and returning it
	/// </summary>
	/// <param name="gameObjectInstanceId">Game object instance identifier.</param>
	static GameObject spawn( int gameObjectInstanceId, Vector3 position, Quaternion rotation, Transform parent, bool activate = true )
	{
		if( instance._instanceIdToRecycleBin.ContainsKey( gameObjectInstanceId ) )
		{
			var newGo = instance._instanceIdToRecycleBin[gameObjectInstanceId].spawn();
			if( newGo != null )
			{
				var newTransform = newGo.transform;
				newTransform.SetParent (parent, false);

				newTransform.position = position;
				newTransform.rotation = rotation;

				newGo.SetActive( activate );
			}

			return newGo;
		}

		return null;
	}


	/// <summary>
	/// internal coroutine for despawning after a delay
	/// </summary>
	/// <returns>The despawn after delay.</returns>
	/// <param name="go">Go.</param>
	/// <param name="delayInSeconds">Delay in seconds.</param>
	IEnumerator internalDespawnAfterDelay( GameObject go, float delayInSeconds )
	{
		yield return new WaitForSeconds( delayInSeconds );
		despawn( go );
	}

	#endregion


	#region Public

	public static bool isManaged (string name) => instance._poolNameToInstanceId.ContainsKey( name );

	/// <summary>
	/// tells TrashMan to start managing the recycle bin at runtime
	/// </summary>
	/// <param name="recycleBin">Recycle bin.</param>
	public static void manageRecycleBin( TrashManRecycleBin recycleBin )
	{
		// make sure we can safely add the bin!
		if( instance._poolNameToInstanceId.ContainsKey( recycleBin.prefab.name ) )
		{
			Debug.LogError( "Cannot manage the recycle bin because there is already a GameObject with the name (" + recycleBin.prefab.name + ") being managed" );
			return;
		}

		// Debug.Log ("Adding Object: " + recycleBin.prefab.name);
		instance.recycleBinCollection.Add( recycleBin );
		recycleBin.initialize();
		instance._instanceIdToRecycleBin.Add( recycleBin.prefab.GetInstanceID(), recycleBin );
		instance._poolNameToInstanceId.Add( recycleBin.prefab.name, recycleBin.prefab.GetInstanceID() );
	}


	/// <summary>
	/// stops managing the recycle bin optionally destroying all managed objects
	/// </summary>
	/// <param name="recycleBin">Recycle bin.</param>
	/// <param name="shouldDestroyAllManagedObjects">If set to <c>true</c> should destroy all managed objects.</param>
	public static void removeRecycleBin( TrashManRecycleBin recycleBin, bool shouldDestroyAllManagedObjects = true )
	{
		var recycleBinName = recycleBin.prefab.name;

		// make sure we are managing the bin first
		if( instance._poolNameToInstanceId.ContainsKey( recycleBinName ) )
		{
			instance._poolNameToInstanceId.Remove( recycleBinName );
			instance._instanceIdToRecycleBin.Remove( recycleBin.prefab.GetInstanceID() );
			instance.recycleBinCollection.Remove( recycleBin );
			recycleBin.clearBin( shouldDestroyAllManagedObjects );
		}
	}


	/// <summary>
	/// pulls an object out of the recycle bin
	/// </summary>
	/// <param name="go">Go.</param>
	public static GameObject spawn( GameObject go, Vector3 position = default( Vector3 ), Quaternion rotation = default( Quaternion ), Transform parent = null, bool activate = true)
	{
		if (!instance._instanceIdToRecycleBin.ContainsKey( go.GetInstanceID() ))
		{
			var bin = new TrashManRecycleBin ();
			bin.prefab = go;
			bin.instancesToPreallocate = 4;
			manageRecycleBin (bin);
		}
		return spawn( go.GetInstanceID(), position, rotation, parent, activate );
	}


	/// <summary>
	/// pulls an object out of the recycle bin using the bin's name
	/// </summary>
	public static GameObject spawn( string gameObjectName, Vector3 position = default( Vector3 ), Quaternion rotation = default( Quaternion ), Transform parent = null )
	{
		int instanceId = -1;
		if( instance._poolNameToInstanceId.TryGetValue( gameObjectName, out instanceId ) )
		{
			return spawn( instanceId, position, rotation, parent );
		}
		else
		{
			Debug.LogError( "attempted to spawn a GameObject from recycle bin (" + gameObjectName + ") but there is no recycle bin setup for it" );
			return null;
		}
	}


	/// <summary>
	/// sticks the GameObject back into its recycle bin. If the GameObject has no bin it is destroyed.
	/// </summary>
	/// <param name="go">Go.</param>
	public static void despawn( GameObject go )
	{
		if( go == null )
			return;

		var goName = go.name;
		// Debug.Log ("Despawning: " + goName, go);
		if( !instance._poolNameToInstanceId.ContainsKey( goName ) )
		{
			Debug.LogWarning ("Destroying Object: " + go.name);
			Destroy( go );
		}
		else
		{
			// Debug.Log ()
			instance._instanceIdToRecycleBin[instance._poolNameToInstanceId[goName]].despawn( go );

            if( go.transform as RectTransform != null )
                go.transform.SetParent( instance.transform, false );
            else
                go.transform.parent = instance.transform;
		}
	}


	/// <summary>
	/// sticks the GameObject back into it's recycle bin after a delay. If the GameObject has no bin it is destroyed.
	/// </summary>
	/// <param name="go">Go.</param>
	public static void despawnAfterDelay( GameObject go, float delayInSeconds )
	{
		if( go == null )
			return;

		instance.StartCoroutine( instance.internalDespawnAfterDelay( go, delayInSeconds ) );
	}


	/// <summary>
	/// gets the recycle bin for the given GameObject name. Returns null if none exists.
	/// </summary>
	public static TrashManRecycleBin recycleBinForGameObjectName( string gameObjectName )
	{
		if( instance._poolNameToInstanceId.ContainsKey( gameObjectName ) )
		{
			var instanceId = instance._poolNameToInstanceId[gameObjectName];
			return instance._instanceIdToRecycleBin[instanceId];
		}
		return null;
	}


	/// <summary>
	/// gets the recycle bin for the given GameObject. Returns null if none exists.
	/// </summary>
	/// <returns>The bin for game object.</returns>
	/// <param name="go">Go.</param>
	public static TrashManRecycleBin recycleBinForGameObject( GameObject go )
	{
		TrashManRecycleBin recycleBin;
		if( instance._instanceIdToRecycleBin.TryGetValue( go.GetInstanceID(), out recycleBin ) )
			return recycleBin;
		return null;
	}


	#endregion


	#region Custom Extensions
	#if UNITY_EDITOR
	[ContextMenu ("Sort items alphabetically")]
	public void SortPrefabsAlphabetically () {
		// recycleBinCollection.Sort (new System.Comparison ())
		recycleBinCollection.Sort ((p1, p2) => p1.prefab.name.CompareTo (p2.prefab.name));
	}

	[ContextMenu ("Sort items categorically")]
	public void SortPrefabsCategorically () {
		// recycleBinCollection.Sort (new System.Comparison ())
		recycleBinCollection.Sort ((p1, p2) => p1.SortingName.CompareTo (p2.SortingName));
	}
	#endif
	#endregion

}
