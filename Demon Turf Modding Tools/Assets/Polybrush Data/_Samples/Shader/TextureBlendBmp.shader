Shader "Polybrush/Standard Texture Blend Bump" {
    Properties {
        _Metallic ("Metallic", Range(0, 1)) = 0.25
        _Gloss ("Gloss", Range(0, 1)) = 0.5
        _MainTex ("Base Color", 2D) = "white" {}
        _BaseBump ("Base Bump", 2D) = "bump" {}
        _Texture1 ("Texture 1", 2D) = "white" {}
        _Texture1Bump ("Texture 1 Bump", 2D) = "bump" {}
        _Texture2 ("Texture 2", 2D) = "white" {}
        _Texture2Bump ("Texture 2 Bump", 2D) = "bump" {}
        _Texture3 ("Texture 3", 2D) = "white" {}
        _Texture3Bump ("Texture 3 Bump", 2D) = "bump" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows
        #pragma target 3.0

        sampler2D _MainTex;
		sampler2D _BaseBump;
        sampler2D _Texture1;
        sampler2D _Texture1Bump;
        sampler2D _Texture2;
        sampler2D _Texture2Bump;
        sampler2D _Texture3;
        sampler2D _Texture3Bump;

        struct Input
        {
            float2 uv_MainTex : TEXCOORD0;
			float2 uv_Texture1 : TEXCOORD1;
			float2 uv_Texture2 : TEXCOORD2;
			float2 uv_Texture3 : TEXCOORD3;
			float4 color : COLOR;
        };

        half _Gloss;
        half _Metallic;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
            fixed3 col1 = tex2D (_MainTex, IN.uv_MainTex);
			fixed3 col2 = tex2D (_Texture1, IN.uv_Texture1);
			fixed3 col3 = tex2D (_Texture2, IN.uv_Texture2);
			fixed3 col4 = tex2D (_Texture3, IN.uv_Texture3);
			float4 vertexColor = normalize (IN.color);
			fixed3 c = (lerp(lerp(lerp(lerp(col1.rgb, col1.rgb, vertexColor.r), col2.rgb, vertexColor.g), col3.rgb, vertexColor.b), col4.rgb, vertexColor.a));

			fixed3 normal1 = UnpackNormal (tex2D (_BaseBump, IN.uv_MainTex));
			fixed3 normal2 = UnpackNormal (tex2D (_Texture1Bump, IN.uv_Texture1));
			fixed3 normal3 = UnpackNormal (tex2D (_Texture2Bump, IN.uv_Texture2));
			fixed3 normal4 = UnpackNormal (tex2D (_Texture3Bump, IN.uv_Texture3));
			float3 normalLocal = (lerp(lerp(lerp(lerp( normal1.rgb, normal1.rgb, vertexColor.r), normal2.rgb, vertexColor.g), normal3.rgb, vertexColor.b), normal4.rgb, vertexColor.a));

			o.Albedo = c.rgb;
			o.Normal = normalLocal; 
            o.Metallic = _Metallic;
            o.Smoothness = _Gloss;
            o.Alpha = 1;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
