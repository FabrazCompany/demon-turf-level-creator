Shader "Polybrush/Texture Blend Height Surface" {
    Properties {
        _MainTex ("Base Color", 2D) = "white" {}
        _BaseNormal ("Base Normal", 2D) = "bump" {}
        _Blend ("Blend", 2D) = "white" {}
        _BlendNormal ("Blend Normal", 2D) = "bump" {}
        _Noise ("Noise", 2D) = "white" {}
        _Metallic ("Metallic", Range(0, 1)) = 0
        _Gloss ("Gloss", Range(0, 1)) = 0.8
        _FillStrength ("Fill Strength", Range(0, 2)) = 1
    }
	SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard vertex:vert addshadow
        #pragma target 3.0

        sampler2D _MainTex;
		sampler2D _BaseNormal;
        sampler2D _Blend;
        sampler2D _BlendNormal;
		sampler2D _Noise;

        struct Input
        {
            float2 uv_MainTex : TEXCOORD0;
			float2 uv_Blend : TEXCOORD1;
			float2 uv_Noise : TEXCOORD2;
			float4 color : COLOR;
        };

        half _Gloss;
        half _Metallic;
		half _FillStrength;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)
		
		void vert (inout appdata_full v, out Input o)
		{
            UNITY_INITIALIZE_OUTPUT(Input, o);
			half3 wNormal = UnityObjectToWorldNormal(v.normal);
			half3 wTangent = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
			half3 wBitangent = normalize(cross(wNormal, wTangent) * v.tangent.w);
        }

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
			fixed4 col1 = tex2D (_MainTex, IN.uv_MainTex);
			fixed4 col2 = tex2D (_Blend, IN.uv_Blend);
			fixed4 noise = tex2D (_Noise, IN.uv_Noise);

			fixed3 normal1 = UnpackNormal (tex2D (_BaseNormal, IN.uv_MainTex));
			fixed3 normal2 = UnpackNormal (tex2D (_BlendNormal, IN.uv_MainTex));
			float3 blend = saturate((((1.0-col1.r)*saturate((noise.rgb/(1.0-col2.rgb))))*(IN.color.r*_FillStrength)));
			fixed3 c = lerp (col1.rgb, col2.rgb, blend);
			

            o.Albedo = c.rgb;
			o.Normal = normal1;
            o.Metallic = _Metallic;
            o.Smoothness = _Gloss;
            o.Alpha = 1;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
