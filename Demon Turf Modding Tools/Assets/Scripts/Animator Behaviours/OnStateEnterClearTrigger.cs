using UnityEngine;

public class OnStateEnterClearTrigger : StateMachineBehaviour
{
	[SerializeField] string triggerID;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.ResetTrigger (triggerID);
    }
}
