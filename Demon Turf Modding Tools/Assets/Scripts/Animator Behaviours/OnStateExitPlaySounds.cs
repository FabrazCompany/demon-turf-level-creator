using UnityEngine;

public class OnStateExitPlaySounds : StateMachineBehaviour
{
    [SerializeField] string[] allowedStates;
    [SerializeField] AudioClip onExitClip;
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (allowedStates?.Length > 0)
        {
            var layer = animator.GetLayerName (layerIndex) + ".";
            var startingState = animator.GetCurrentAnimatorStateInfo (layerIndex);
            var acceptedStateFound = false;
            for (int i = 0; i < allowedStates.Length; i++) 
            {
                if (startingState.IsName (layer + allowedStates))
                {
                    acceptedStateFound = true;
                    break;
                }
            }
            if (!acceptedStateFound)
                return;
        }
        if (onExitClip)
            SoundManagerStub.Instance.PlaySoundUI (onExitClip);
    }
}
