using System;
using UnityEngine;

[AttributeUsage(AttributeTargets.Field)]
public class ConditionalAttribute : PropertyAttribute
{
	public string PropertyToCheck;

	public object CompareValue;
	
	public bool Hide;
	public bool Read;

	public ConditionalAttribute(string propertyToCheck, object compareValue = null, bool hide = false, bool read = false)
	{
		PropertyToCheck = propertyToCheck;
		CompareValue = compareValue;
		Hide = hide;
		Read = read;
	}
}