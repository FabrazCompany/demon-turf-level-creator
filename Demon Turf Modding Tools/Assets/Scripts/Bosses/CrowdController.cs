﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Malee;
using DG.Tweening;

[System.Serializable]
public class SpectatorData 
{
	[Range (0,1)] public float chance = 1;
	[Range (0,5)] public float scale = 1;
	// public bool unique;
	[SpritePreview] public Sprite sprite;		
}
[System.Serializable]
public class SpectatorList : ReorderableArray<SpectatorData> {}

public class CrowdController : MonoBehaviour
{
	
	[SerializeField] float reactionDuration = 2f;
	[SerializeField] int initialExcitement = 0;
	[SerializeField] bool useReactions = true;
	[SerializeField, Range (0,1)] float initialFavorability = 1;

	[Header ("Triggers")]
	[SerializeField] bool triggerOnPunch = true;
	[SerializeField] bool triggerOnKnockedback = true;
	[SerializeField] bool triggerOnHookshot = true;
	
	[Header ("Visuals")]
	[SerializeField] UniqueSpectatorList uniqueSpectators;
	[SerializeField, Reorderable] SpectatorList spectators;
	[SerializeField, Reorderable] SpectatorList skipSpectators;
	[SerializeField] ParticleSystem skipParticles;
	[SerializeField, Range (0,1)] float uniqueSpectatorChance = 0.1f;
	[SerializeField] int uniqueSpectatorLimit = 1;
	[SerializeField] SpriteRenderer[] spectatorRends;
	[SerializeField] Animator[] spectatorRowAnims;
	
	[Header ("Sounds")]
	[SerializeField, Range (0,1)] float soundMurmurVolume = 1;
	[SerializeField, Range (0,1)] float soundMurmurHushedVolume = 0;
	[SerializeField] float soundMurmurFadeTransitionDur = 0.5f;
	[SerializeField] float soundMurmurFadeDur = 2;
	[SerializeField] AudioSource soundMurmur;
	[Space]
	[SerializeField] float soundReactionBuffer = 0.5f;
	[SerializeField] AudioSource soundCheer;
	[SerializeField] AudioClipSet soundCheerOptions;
	[SerializeField] AudioSource soundBoo;
	[SerializeField] AudioClipSet soundBooOptions;

	int currentExcitement;
	float currentFavorability;
	int currentUniques;
	bool skipped;

	void OnValidate ()
	{
		if (spectatorRends == null || spectatorRends.Length == 0)
			spectatorRends = GetComponentsInChildren<SpriteRenderer> ();
		if (spectatorRowAnims == null || spectatorRowAnims.Length == 0)
			spectatorRowAnims = GetComponentsInChildren<Animator> ();
	}
	
	void Awake ()
	{
#if !DT_MOD
		if (useReactions)
			EnableReactions ();
		else 
			BossController.onFightStarted += EnableReactions;
#endif
	}
	void Start ()
	{
#if !DT_MOD
		var levelData = GameDataManager.Instance.Data.GetLevelData (LevelUtility.GetLevelID ());
		skipped = levelData.SkippedStandard && levelData.BestTimeStandard <= 0;
#endif
		CrowdSpectators.Instance.Init (uniqueSpectators);
		SetFavorability (initialFavorability);
		SetCrowdExcitement (initialExcitement);
		RandomizeSpectators ();
	}
	void OnDestroy ()
	{
#if !DT_MOD
		BossController.onFightStarted -= EnableReactions;
		
		BossController.onExcitementAdjusted -= SetCrowdExcitement;
		BossController.onFavorabilityAdjusted -= SetFavorability;
		BossController.onExcitingMoment -= BossMoment;

		PlayerControllerEvents.onPlayerKnockedback -= BossMoment;
		PlayerPunchAttack.onPunchLanded -= PlayerMoment;
		PlayerControllerEvents.onHookshotPulled -= PlayerMoment;
#endif	
	}
	public void EnableReactions ()
	{
#if !DT_MOD
		BossController.onFightStarted -= EnableReactions;

		BossController.onExcitementAdjusted -= SetCrowdExcitement;
		BossController.onFavorabilityAdjusted -= SetFavorability;
		BossController.onExcitingMoment -= BossMoment;
		PlayerControllerEvents.onPlayerKnockedback -= BossMoment;
		PlayerPunchAttack.onPunchLanded -= PlayerMoment;
		PlayerControllerEvents.onHookshotPulled -= PlayerMoment;
		
		BossController.onExcitementAdjusted += SetCrowdExcitement;
		BossController.onFavorabilityAdjusted += SetFavorability;
		BossController.onExcitingMoment += BossMoment;

		if (triggerOnKnockedback)
			PlayerControllerEvents.onPlayerKnockedback += BossMoment;
		if (triggerOnPunch)
			PlayerPunchAttack.onPunchLanded += PlayerMoment;
		if (triggerOnHookshot)
			PlayerControllerEvents.onHookshotPulled += PlayerMoment;
#endif
	}
	void RandomizeSpectators ()
	{
		for (int i = 0; i < spectatorRends.Length; i++)
		{
			var rend = spectatorRends[i];
			rend.sprite = GetSprite (out var scale);
			rend.transform.localScale = Vector3.one * scale;
		}
	}
	void SetCrowdExcitement (int _excitement) => SetCrowdExcitement (_excitement, true);
	void SetCrowdExcitement (int _excitement, bool writeCurrent)
	{
		if (writeCurrent)
			currentExcitement = _excitement;
		for (int i = 0; i < spectatorRowAnims.Length; i++)
		{
			spectatorRowAnims[i].SetInteger ("excitement", _excitement);
		}
	}
	void SetFavorability (float _favorability) => currentFavorability = _favorability;
	Sprite GetSprite (out float scale)
	{
		Sprite returnSprite = null;
		scale = 1;
		if (CrowdSpectators.Instance.uniqueSprites.Count > 0 && Random.value <= uniqueSpectatorChance && currentUniques < uniqueSpectatorLimit)
		{
			var _crowd = CrowdSpectators.Instance.uniqueSprites;
			float targetWeight = 0;
			for (int i = 0; i < _crowd.Count; i++)
			{
				targetWeight += _crowd[i].chance;
			}

			targetWeight = Random.Range (0, targetWeight);
			float currentWeight = 0;
			for (int i = 0; i < _crowd.Count; i++)
			{
				currentWeight += _crowd[i].chance;
				if (currentWeight >= targetWeight)
				{
					scale = _crowd[i].scale;
					returnSprite = _crowd[i].sprite;
					_crowd.RemoveAt (i);
					break;
				}
			}
			currentUniques++;
		}
		else 
		{
			var _crowd = skipped ? skipSpectators : spectators;
			float targetWeight = 0;
			for (int i = 0; i < _crowd.Length; i++)
			{
				targetWeight += _crowd[i].chance;
			}
			targetWeight = Random.Range (0, targetWeight);
			float currentWeight = 0;
			for (int i = 0; i < _crowd.Length; i++)
			{
				currentWeight += _crowd[i].chance;
				if (currentWeight >= targetWeight)
				{
					scale = _crowd[i].scale;
					returnSprite = _crowd[i].sprite;
					break;
				}
			}
		}		
		return returnSprite;
	}
	void SoundReaction (AudioSource source, AudioClipSet options)
	{
		if (!options.HasSound) return;
		StartCoroutine (this.WaitAndDo (Random.Range (0, soundReactionBuffer), () => 
		{
			var clip = options.GetSound;
			source.PlayOneShot (clip);
			soundMurmur.DOKill ();
			soundMurmur.DOFade (soundMurmurHushedVolume, soundMurmurFadeTransitionDur).OnComplete (() => {
				soundMurmur.DOFade (soundMurmurVolume, soundMurmurFadeTransitionDur).SetDelay (soundMurmurFadeDur);
			});
		}));		
	}
	void PlayerMoment () 
	{
		if (Random.value < currentFavorability)
			SoundReaction (soundCheer, soundCheerOptions);
		else 
			SoundReaction (soundBoo, soundBooOptions);
		CrowdReaction (2);
	}
	void BossMoment () => BossMoment (true);
	void BossMoment (bool favorsBoss) 
	{
		if (favorsBoss)
		{
			if (Random.value >= currentFavorability)
				SoundReaction (soundCheer, soundCheerOptions);
			else 
				SoundReaction (soundBoo, soundBooOptions);
		}
		else 
			PlayerMoment ();
		
		CrowdReaction (2);
	}
	public void SetSkipParticles (bool state)
	{
		Debug.Log ("State: " + state);
		if (!skipped)
			return;
		if (state && !skipParticles.isPlaying)
			skipParticles.Play (true);
		else if (!state && skipParticles.isPlaying)
			skipParticles.Stop (true);
	}
	void CrowdReaction (int _excitement) => this.RestartCoroutine (CrowdReactionRoutine (_excitement), ref reactionRoutine);
	Coroutine reactionRoutine;
	IEnumerator CrowdReactionRoutine (int _excitement)
	{
		SetCrowdExcitement (_excitement, false);
		yield return new WaitForSeconds (reactionDuration);
		SetCrowdExcitement (currentExcitement, false);
	}
}