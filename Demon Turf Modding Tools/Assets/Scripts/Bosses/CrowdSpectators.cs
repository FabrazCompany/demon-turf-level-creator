using UnityEngine;
using System.Collections.Generic;

public class CrowdSpectators : MonoBehaviour
{
	public static CrowdSpectators Instance 
	{
		get 
		{
			if (instance == null)
				instance = new GameObject ("Crowd Spectators").AddComponent<CrowdSpectators> ();
			return instance;
		}
	}
	static CrowdSpectators instance;   

	void Awake ()
	{
		if (instance != null && instance != this)
			Destroy (gameObject);
		
		instance = this;
	}

	UniqueSpectatorList uniqueSpectatorList;
	public List<SpectatorData> uniqueSprites = new List<SpectatorData> ();
	public void Init (UniqueSpectatorList _uniqueSpectators)
	{
		if (uniqueSpectatorList)
			return;
		
		uniqueSpectatorList = _uniqueSpectators;
		uniqueSprites.Clear ();
		
		for (int i = 0; i < uniqueSpectatorList.uniqueSpectators.Length; i++)
		{
			if (Random.value > uniqueSpectatorList.uniqueSpectators[i].chance)
				continue;
			
			uniqueSprites.Add (uniqueSpectatorList.uniqueSpectators[i]);
			// uniqueSprites.Add (uniqueSpectatorList.uniqueSpectators[i].sprite);
		}
	}

	
}