using UnityEngine;

public class Turf4BossHandExploder : MonoBehaviour 
{
	[SerializeField] bool mute;
	[SerializeField] GameObject explosionEffect;
	[SerializeField] ReceiveKill killer;

	public void SetMute (bool _mute) => mute = _mute;

	bool preppingExplosion;

	void OnEnable ()
	{
		preppingExplosion = false;
	}
	public void PrepExplosion () => preppingExplosion = true;

	public void OnKilled () 
	{
		if (preppingExplosion)
			TrashMan.spawn (explosionEffect, transform.position);

		TrashMan.despawn (gameObject);
	}

	void OnCollisionEnter (Collision coll)
	{
		if (!preppingExplosion)
			return;
		if (!coll.rigidbody)
			return;
		if (mute)
			return;

#if !DT_MOD
		if (coll.rigidbody.GetComponentInParent<Turf4BossController> ())
			killer.TriggerKill ();
#endif
	}
}