using UnityEngine;
using UnityEngine.Events;

public class Turf4DamageTrigger : MonoBehaviour 
{
	void OnTriggerEnter (Collider hit)
	{
#if !DT_MOD
		var controller = hit.attachedRigidbody?.GetComponentInParent<Turf4BossController> ();
		if (!controller)
			return;
	
		controller.RegisterHit (hit);
#endif
	}
}