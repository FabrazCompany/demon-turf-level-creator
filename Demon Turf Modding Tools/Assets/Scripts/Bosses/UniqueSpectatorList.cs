using UnityEngine;
using Malee;

public class UniqueSpectatorList : ScriptableObject 
{
	[Reorderable] public SpectatorList uniqueSpectators;

}