using UnityEngine;

public class CameraRotationOverride : MonoBehaviour {
    [SerializeField] Transform rotationGuide;
	[SerializeField] float adjustSpeed = 5;

    void OnTriggerEnter (Collider other) {
		if (!other.TryGetComponent<IPlayerController> (out var player)) return;
        player.AddRotationOverride (rotationGuide, adjustSpeed);
    }  

    void OnTriggerExit (Collider other) {
        if (!other.TryGetComponent<IPlayerController> (out var player)) return;
        player.ClearRotationOverride ();
    } 

	void OnValidate ()
	{
		if (rotationGuide) return;
		rotationGuide = transform;
	} 

    void OnDrawGizmos () {
        if (!rotationGuide) return;
        Gizmos.color = Color.blue;
        Gizmos.DrawRay (transform.position, rotationGuide.forward * 2);
        Gizmos.DrawSphere (transform.position, 0.1f);
    }
}