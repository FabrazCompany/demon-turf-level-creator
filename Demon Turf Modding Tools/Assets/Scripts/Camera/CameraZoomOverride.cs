using UnityEngine;

public class CameraZoomOverride : MonoBehaviour {
    [SerializeField] float zoomScale = 2;
    void OnTriggerEnter (Collider other) {
		if (!other.TryGetComponent<IPlayerController> (out var player)) return;
        player.AddZoomOverride (zoomScale);
    }  

    void OnTriggerExit (Collider other) {
        if (!other.TryGetComponent<IPlayerController> (out var player)) return;
        player.ClearZoomOverride ();
    }  
}