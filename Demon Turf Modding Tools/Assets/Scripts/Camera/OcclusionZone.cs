﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OcclusionZone : MonoBehaviour
{
    void OnTriggerEnter (Collider hit)
	{
		if (!hit.CompareTag ("MainCamera")) return;
		if (hit.TryGetComponent<Camera> (out var cam))
			cam.useOcclusionCulling = true;
	}
	void OnTriggerExit (Collider hit)
	{
		if (!hit.CompareTag ("MainCamera")) return;
		if (hit.TryGetComponent<Camera> (out var cam))
			cam.useOcclusionCulling = false;
	}
}
