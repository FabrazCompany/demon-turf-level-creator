﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CollectableCollector : MonoBehaviour
{
    public UnityEvent onPickedUpCandy;
    public void PickupCandy ()
    {
        onPickedUpCandy?.Invoke ();
    }
    public UnityEvent onPickedUpCake;
    public void PickupCake ()
    {
        onPickedUpCake?.Invoke ();
    }
}
