﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent (typeof (UniqueID))]
public class PlayerCollectable : MonoBehaviour
{
    [SerializeField] CollectableType type;
	[SerializeField] bool overrideTeleport;
	[SerializeField, Conditional ("overrideTeleport")] TeleportTrigger closestTeleport;
	[SerializeField] bool overrideRoomDistance;
	[SerializeField, Conditional ("overrideRoomDistance")] float roomCheckDistance = 40;
    public UnityEvent onPickup;
    public UnityEvent onAlreadyPickedUp;

	public string getID => string.Format ("{0} : {1}" , type.ToString (), GetComponent<UniqueID> ().uniqueId);
    public CollectableType Type => type;
	public bool getOverrideTeleport => overrideTeleport;
	public TeleportTrigger getClosestTeleport => closestTeleport;
	public bool getOverrideRoomDistance => overrideRoomDistance;
	public float getRoomCheckDistance => roomCheckDistance;

    Vector3? cachedPosition;
    AttractToPlayer attractToPlayer;

    public static event Action<CollectableType> onPickedUp;
    
    void Awake () 
    {
        cachedPosition = transform.position;
        attractToPlayer = GetComponentInChildren<AttractToPlayer> (true);
    }
    void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponent<CollectableCollector> ();
        if (player == null)
            return;

        switch (type) {
            case CollectableType.Candy:
                player?.PickupCandy ();
                break;
            case CollectableType.Cake:
                player?.PickupCake ();
                break;
        }
        gameObject.SetActive (false);
        if (attractToPlayer)
            attractToPlayer.enabled = false;
        onPickup?.Invoke ();
        onPickedUp?.Invoke (type);
    }

    public void SpawnEffect (GameObject effect) 
    {
        Instantiate (effect, transform.position, Quaternion.identity);
    }

    public void SetAlreadyPickedUp ()
    {
        onAlreadyPickedUp?.Invoke ();
    }

#if UNITY_EDITOR
	[UnityEditor.MenuItem ("Fabraz Tools/Utilities/Count Total Candy")]
	static public void CheckCandyInScene () 
	{
		var collectables = new List<PlayerCollectable> (SceneUtility.FindAllSceneObjectsIncludingDisabled<PlayerCollectable>());
		collectables.RemoveAll (i => i.type != CollectableType.Candy);
        UnityEditor.EditorUtility.DisplayDialog ("Candy Count", $"Total Candy In Scene: {collectables.Count}", "Ok");
	}
#endif
}
