using System;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(ConditionalAttribute))]
public class ConditionalDrawer : PropertyDrawer
{

	private ConditionalAttribute Attribute => _attribute ?? (_attribute = attribute as ConditionalAttribute);

	private string PropertyToCheck => Attribute != null ? _attribute.PropertyToCheck : null;

	private object CompareValue => Attribute != null ? _attribute.CompareValue : null;
	private bool Hide => Attribute != null ? _attribute.Hide : false;
	private bool Read => Attribute != null ? _attribute.Read : false;


	private ConditionalAttribute _attribute;
	private bool _toShow = true;

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		return _toShow ? EditorGUI.GetPropertyHeight(property) : 0;
	}

	// TODO: Skip array fields
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		if (!PropertyToCheck.IsNullOrEmpty())
		{
			var conditionProperty = FindPropertyRelative(property, PropertyToCheck);
			// var conditionProperty = property.FindPropertyRelative (PropertyToCheck);
			if (conditionProperty != null)
			{
				// SerializedPropertyType,
				bool isBoolMatch = conditionProperty.propertyType == SerializedPropertyType.Boolean && conditionProperty.boolValue;
				string compareStringValue = CompareValue?.ToString().ToUpper() ?? "NULL";
				if (isBoolMatch && compareStringValue == "FALSE") isBoolMatch = false;

				string conditionPropertyStringValue = conditionProperty.AsStringValue().ToUpper();
				bool objectMatch = compareStringValue == conditionPropertyStringValue;

				if (Hide) {
					if (isBoolMatch || objectMatch) {
						_toShow = false;
						if (!Read)
							return;
					}
				} else {
					if (!isBoolMatch && !objectMatch)
					{
						_toShow = false;
						if (!Read)
							return;
					}
				}
				
			}
		}

		var guiState = GUI.enabled;
		if (Read && !_toShow)
			GUI.enabled = false;

		_toShow = true;
		EditorGUI.PropertyField(position, property, label, true);

		if (Read)
			GUI.enabled = guiState;
	}

	private SerializedProperty FindPropertyRelative(SerializedProperty property, string toGet)
	{
		if (property.depth == 0) return property.serializedObject.FindProperty(toGet);

		var path = property.propertyPath.Replace(".Array.data[", "[");
		var elements = path.Split('.');
		SerializedProperty parent = null;

		
		for (int i = 0; i < elements.Length - 1; i++)
		{
			var element = elements[i];
			int index = -1;
			if (element.Contains("["))
			{
				index = Convert.ToInt32(element.Substring(element.IndexOf("[", StringComparison.Ordinal)).Replace("[", "").Replace("]", ""));
				element = element.Substring(0, element.IndexOf("[", StringComparison.Ordinal));
			}
			
			parent = i == 0 ? 
				property.serializedObject.FindProperty(element) : 
				parent.FindPropertyRelative(element);

			if (index >= 0) parent = parent.GetArrayElementAtIndex(index);
		}

		return parent.FindPropertyRelative(toGet);
	}

   
}

public static class SerializedPropertyExtensions {
    public static bool IsNullOrEmpty(this string property) {
        return property == null || property.Equals (string.Empty);
    }
    /// <summary>
    /// Get string representation of serialized property, even for non-string fields
    /// </summary>
    public static string AsStringValue(this SerializedProperty property) {
        switch (property.propertyType)
        {
            case SerializedPropertyType.String:
                return property.stringValue;
			case SerializedPropertyType.Character:
            case SerializedPropertyType.Integer:
                if (property.type == "char") return System.Convert.ToChar(property.intValue).ToString();
                return property.intValue.ToString();
			case SerializedPropertyType.ObjectReference:
                return property.objectReferenceValue != null ? property.objectReferenceValue.ToString() : "null";
			case SerializedPropertyType.Boolean:
                return property.boolValue.ToString();
			case SerializedPropertyType.Enum:
                return property.enumNames[property.enumValueIndex];
			default:
                return string.Empty;
        }
    }
	
}
