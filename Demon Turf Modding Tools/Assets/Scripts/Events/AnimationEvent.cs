﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Malee;

public class AnimationEvent : MonoBehaviour {
	[SerializeField] UnityEvent[] events;
	[SerializeField, Reorderable] IDStampedEventList idEvents;

	Animator anim;

	void Awake () 
	{
		anim = GetComponent<Animator> ();
	}
	public void Trigger (int index) {
		if (events.Length <= 0)
			return;
		if (index >= events.Length) {
			return;
		}
		events[index].Invoke();
	}
	public void TriggerEvent (int index) {
		Trigger (index);
	}

	public void TriggerEventWithID (string id) {
		if (idEvents.Length <= 0)
			return;

		for (int i = 0; i < idEvents.Length; i++)
		{
			var idEvent = idEvents[i];
			if (idEvent.id == id)
			{
				idEvents[i].triggeredEvent?.Invoke ();
				return;
			}
		}

		Debug.LogWarningFormat (this, "Animation Event id {0} not found", id);
	}	
	
	[System.Serializable]
	class IDStampedEventList : ReorderableArray<IDStampedEvent> {}

	[System.Serializable]
	struct IDStampedEvent 
	{
		public string id;
		public UnityEvent triggeredEvent;
	}
}
