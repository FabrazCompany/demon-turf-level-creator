﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionEvent : MonoBehaviour
{
    [SerializeField, TagSelector] List<string> allowedTags;
    [SerializeField] List<GameObject> allowedObjects;
	[SerializeField] bool minimumTrigger;
	[SerializeField, Conditional ("minimumTrigger")] float minimumTriggerForce;
    [SerializeField] UnityEventCollision onEnter;
    [SerializeField] UnityEventCollision onStay;
    [SerializeField] UnityEventCollision onExit;
	bool ShouldProcess (Collision other)
	{
		if (allowedTags?.Count > 0 && !allowedTags.Contains (other.collider.tag))
            return false;
		if (allowedObjects?.Count > 0 && !allowedObjects.Contains (other.gameObject))
            return false;
		if (minimumTrigger && other.relativeVelocity.magnitude < minimumTriggerForce)
			return false;
		return true;
	}
    public void OnCollisionEnter (Collision other) {
        if (!ShouldProcess (other))
			return;
        onEnter?.Invoke (other);
    }
    public void OnCollisionStay (Collision other) {
        if (!ShouldProcess (other))
			return;
        onStay?.Invoke (other);
    }
    public void OnCollisionExit (Collision other) {
        if (!ShouldProcess (other))
			return;    
        onExit?.Invoke (other);
    }
}
