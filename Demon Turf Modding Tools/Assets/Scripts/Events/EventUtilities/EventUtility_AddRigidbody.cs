﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventUtility_AddRigidbody : MonoBehaviour
{
    [SerializeField] float mass = 4;
    [SerializeField] float drag = 0.5f;
    [SerializeField] float angularDrag = 0.05f;
    [SerializeField] bool useGravity = true;
    [SerializeField] bool isKinematic = false;
    [SerializeField] RigidbodyInterpolation interpolate;
    [SerializeField] CollisionDetectionMode collisionDetection;

    public void AddRigidbody () => AddRigidbody (gameObject);
    public void AddRigidbody (GameObject target) 
    {
        if (target.GetComponent<Rigidbody> ())
            return;

        var rigid = target.AddComponent<Rigidbody> ();
        rigid.mass = mass;
        rigid.drag = drag;
        rigid.angularDrag = angularDrag;
        rigid.useGravity = useGravity;
        rigid.isKinematic = isKinematic;
        rigid.interpolation = interpolate;
        rigid.collisionDetectionMode = collisionDetection;
    }
}
