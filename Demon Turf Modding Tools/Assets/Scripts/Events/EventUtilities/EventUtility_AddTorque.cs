using UnityEngine;

[RequireComponent (typeof (Rigidbody))]
public class EventUtility_AddTorque : MonoBehaviour
{
    [SerializeField] float torqueForce;
    Rigidbody rigid;

    void Awake () 
    {
        rigid = GetComponent<Rigidbody> ();
    }

    public void AddTorqueFromForce (Vector3 force) 
    {
        if (rigid.isKinematic)
            return;
        rigid.AddTorque (Random.onUnitSphere * torqueForce * force.magnitude, ForceMode.VelocityChange);
    }
}