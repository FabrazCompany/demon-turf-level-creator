﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventUtility_AnimationCurveControl : MonoBehaviour
{
    [SerializeField] AnimationCurve curve;
	[SerializeField] UnityEventFloat processedEvent;
	
	public void ProcessInput (float inputVal) => processedEvent?.Invoke (curve.Evaluate (inputVal));
}
