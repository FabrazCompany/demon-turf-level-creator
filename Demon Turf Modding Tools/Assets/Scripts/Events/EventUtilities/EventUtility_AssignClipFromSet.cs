using UnityEngine;

[RequireComponent (typeof(AudioSource))]
public class EventUtility_AssignClipFromSet : MonoBehaviour 
{
	[SerializeField] AudioClipSet clips;
	[SerializeField] AudioSource sound;
	void OnValidate ()
	{
		if (!sound)
			sound = GetComponent<AudioSource> ();
	}
	void Awake ()
	{
		if (!sound)
			sound = GetComponent<AudioSource> ();
	}

	public void AssignClip () => sound.clip = clips.GetSound;

	public void PlayRandomClip ()
	{
		AssignClip ();
		sound.Play ();
	}
}