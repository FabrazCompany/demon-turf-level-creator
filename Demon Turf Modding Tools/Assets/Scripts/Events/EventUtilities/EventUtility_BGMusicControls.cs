﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventUtility_BGMusicControls : MonoBehaviour
{
    public void StopBGMusic ()
    {
        SoundManagerStub.Instance.StopMusic ();
    }
    public void PlayBGMusic ()
    {
        SoundManagerStub.Instance.PlayMusic ();
    }

    public void PauseBGMusic ()
    {
        SoundManagerStub.Instance.PauseMusic ();
    }

    public void UnPauseBGMusic ()
    {
        SoundManagerStub.Instance.ResumeMusic ();
    }
}
