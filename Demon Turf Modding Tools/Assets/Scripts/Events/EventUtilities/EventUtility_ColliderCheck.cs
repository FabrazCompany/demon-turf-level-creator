using UnityEngine;

public class EventUtility_ColliderCheck : MonoBehaviour
{
    [SerializeField, TagSelector] string[] targetTags;
    [SerializeField] LayerMask targetLayers;
    [SerializeField] UnityEventCollider onTriggered;

    public void ColliderCheck (Collider hit) 
    {
        if (targetTags.Length > 0)
        {
            bool cancel = true;
            foreach (var tag in targetTags)
            {
                if (transform.CompareTag (tag))
                {
                    cancel = false;
                    break;
                }
            }    
            if (cancel)
                return;
        }

        if (!targetLayers.ContainsLayer (hit.gameObject.layer))
            return;        

        onTriggered?.Invoke (hit);
    }
}