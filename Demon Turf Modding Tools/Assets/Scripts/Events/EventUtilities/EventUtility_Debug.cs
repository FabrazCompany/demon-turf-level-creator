using UnityEngine;

public class EventUtility_Debug : MonoBehaviour
{
    public void Log (string msg) => Debug.Log (msg, this);
    public void LogWarning (string msg) => Debug.LogWarning (msg, this);
    public void LogError (string msg) => Debug.LogError (msg, this);
}