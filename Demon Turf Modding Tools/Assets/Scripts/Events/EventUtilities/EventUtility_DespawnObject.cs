﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventUtility_DespawnObject : MonoBehaviour
{
    public void DespawnObject (GameObject _target) {
        TrashMan.despawn (_target);
    }

    public void DestroyObject (GameObject _target) {
        Destroy (_target);
    }
}
