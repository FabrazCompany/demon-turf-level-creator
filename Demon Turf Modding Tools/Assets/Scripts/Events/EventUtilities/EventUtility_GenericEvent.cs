using UnityEngine;
using UnityEngine.Events;

public class EventUtility_GenericEvent : MonoBehaviour
{
    [SerializeField] UnityEvent onTriggered;

    public void Trigger () => onTriggered?.Invoke ();
}