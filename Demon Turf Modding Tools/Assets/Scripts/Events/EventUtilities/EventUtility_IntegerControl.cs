using UnityEngine;
using UnityEngine.Events;
using RotaryHeart.Lib.SerializableDictionary;

// []
#if UNITY_EDITOR
using UnityEditor;
[CustomEditor (typeof (EventUtility_IntegerControl))]
public class EventUtility_IntegerControlEditor : Editor
{}
#endif

public class EventUtility_IntegerControl : MonoBehaviour
{
    [SerializeField] IntegerDependentEventList integerEvents;
    public void OnTriggerIntegerEvent (int targetIndex) 
    {
        if (integerEvents.ContainsKey (targetIndex))
            integerEvents[targetIndex]?.Invoke ();
    }

    int counter;
    public void IncrementCounter () => counter++;
    public void DecrementCounter () => counter--;
    public void SetCounter (int value) => counter = value;
    public void OnTriggerIntegerWithCounter () => OnTriggerIntegerEvent (counter);

    [System.Serializable]
    class IntegerDependentEventList : SerializableDictionaryBase<int, UnityEvent> {}
}