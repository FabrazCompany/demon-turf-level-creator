﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AddressableAssets;

public class EventUtility_LoadScene : MonoBehaviour 
{
    public void LoadScene (string newScene) 
	{
		
#if !DT_MOD
		SceneTransitionManager.Instance.LoadScene (newScene);
#else 
		Debug.Log (newScene);
		Addressables.LoadSceneAsync (newScene);
#endif
	}
}
