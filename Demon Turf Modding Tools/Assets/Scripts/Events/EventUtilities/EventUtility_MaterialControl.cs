using UnityEngine;

[RequireComponent (typeof (Renderer))]
public class EventUtility_MaterialControl : MonoBehaviour
{

    [SerializeField] string targetProperty;
    [SerializeField] int targetMaterialIndex = 0;
    Renderer rend;
    Material mat;

    void Awake () 
    {
        rend = GetComponent<Renderer> ();
        mat = rend.materials [targetMaterialIndex];
    }
    // void OnDestroy () 
    // {
    //     Destroy (mat);
    // }

    public void SetPropertyValue (float val) => mat?.SetFloat (targetProperty, val);

    public void SetPropertyColor (Color color) => mat?.SetColor (targetProperty, color);
}