using UnityEngine;

[RequireComponent (typeof (Projector))]
public class EventUtility_MaterialControlProjector : MonoBehaviour
{

    [SerializeField] string targetProperty;
    Projector proj;
    Material mat;

    void Awake () 
    {
        proj = GetComponent<Projector> ();
        mat = new Material (proj.material);
        proj.material = mat;
    }
    void OnDestroy () 
    {
        Destroy (mat);
    }

    public void SetPropertyValue (float val) 
	{
		if (!mat) return;
		mat.SetFloat (targetProperty, val);
	}

    public void SetPropertyColor (Color color) 
	{
		if (!mat) return;
		mat.SetColor (targetProperty, color);
	}
}