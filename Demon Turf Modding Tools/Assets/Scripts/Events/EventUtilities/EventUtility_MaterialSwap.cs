using UnityEngine;
using System.Collections;

public class EventUtility_MaterialSwap : MonoBehaviour 
{
	[SerializeField] Renderer rend;
	[SerializeField] float swapDuration = 0.1f;
	[SerializeField] Material swappedMaterial;
	[SerializeField] int swappedIndex = 0;
	Material initialMaterial;
	Material[] materials;
	float timer;
	void OnValidate ()
	{
		rend = rend ?? GetComponent<Renderer> ();
	}

	void Awake () 
	{
		initialMaterial = rend.materials[swappedIndex];
		materials = rend.materials;
	}
	void OnEnable ()
	{
		timer = swapDuration;
		materials = rend.materials;
		materials[swappedIndex] = swappedMaterial;
		rend.materials = materials;
		// Debug.Log (rend.materials[swappedIndex]);
	}
	void OnDisable () 
	{
		materials = rend.materials;
		materials[swappedIndex] = initialMaterial;
		rend.materials = materials;
	}
	void Update ()
	{
		timer -= Time.deltaTime;
		if (timer <= 0)
			enabled = false;
	}
	// public void Trigger () => this.RestartCoroutine (TriggerRoutine (), ref triggerRoutine);
	// Coroutine triggerRoutine;
	// IEnumerator TriggerRoutine ()
	// {
	// 	rend.materials[swappedIndex] = swappedMaterial;
	// 	yield return new WaitForSeconds (swapDuration);
	// 	rend.materials[swappedIndex] = initialMaterial;
	// }
	

}