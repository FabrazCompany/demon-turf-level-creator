﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventUtility_PlaySound : MonoBehaviour
{
    [SerializeField] SoundMixerTarget mixerTarget;
    [SerializeField] AudioData playData;
	[SerializeField] bool drawDebug;
    public void Trigger2DSound (AudioClip audioClip) 
	{
        SoundManagerStub.Instance.PlaySoundGameplay2D (audioClip, transform.position, playData);
    }
    public void Trigger3DSound (AudioClip audioClip) 
	{
        SoundManagerStub.Instance.PlaySoundGameplay3D (audioClip, transform.position, playData);
    }

    Color selectedColor = Color.white;
    Color unselectedColor = Color.Lerp (Color.gray, Color.blue, 0.3f);
    void OnDrawGizmosSelected () 
    {
		if (!drawDebug)
            return;
        Gizmos.color = selectedColor;
        Gizmos.DrawWireSphere (transform.position, playData.minDistance);
        Gizmos.DrawWireSphere (transform.position, playData.maxDistance);
    }
}
