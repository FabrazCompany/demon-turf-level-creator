﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventUtility_PlaySoundFromSet : MonoBehaviour
{
    [SerializeField] SoundMixerTarget mixerTarget;
    [SerializeField] AudioData playData;
    [SerializeField] AudioClipSet audioSet;
    public void Trigger2DSound () => Trigger2DSound (transform.position);
	public void Trigger2DSound (Vector3 pos) 
	{
		SoundManagerStub.Instance.PlaySoundGameplay2D (audioSet.GetSound, pos, playData);
	}
	public void Trigger3DSound () => Trigger3DSound (transform.position);
    public void Trigger3DSound (Vector3 pos) 
	{
		SoundManagerStub.Instance.PlaySoundGameplay3D (audioSet.GetSound, pos, playData);
	}
    


    Color selectedColor = Color.white;
    Color unselectedColor = Color.Lerp (Color.gray, Color.blue, 0.3f);
    void OnDrawGizmosSelected () 
    {
        Gizmos.color = selectedColor;
        Gizmos.DrawWireSphere (transform.position, playData.minDistance);
        Gizmos.DrawWireSphere (transform.position, playData.maxDistance);
    }
}
