﻿using UnityEngine;
using UnityEngine.Events;

public class EventUtility_PlayerStateControl : MonoBehaviour
{
    [SerializeField] CharacterState[] targetStates;
    [SerializeField] UnityEvent onIsState;
    [SerializeField] UnityEvent onIsntState;

    public void Hit (Collider hit)
    {
        var player = hit.GetComponent<IPlayerController> ();
        if (player != null)
        {
            var currentState = player.characterState;
            for (int i = 0; i < targetStates.Length; i++)
            {
                if (targetStates[i] == currentState)
                {
                    onIsState?.Invoke ();
                    return;
                }
            }
            onIsntState?.Invoke ();
        }
    }
}
