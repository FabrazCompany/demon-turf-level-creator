using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventUtility_RandomizeRotation : MonoBehaviour {
    public void RandomizeZRotation () {
        transform.localEulerAngles = transform.localEulerAngles.With (z: Random.Range (0, 361));
    }
	public void RandomizeYRotation () {
        transform.localEulerAngles = transform.localEulerAngles.With (y: Random.Range (0, 361));
    }
}
