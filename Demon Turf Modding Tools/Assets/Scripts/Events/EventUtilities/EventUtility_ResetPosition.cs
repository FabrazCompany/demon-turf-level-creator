﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventUtility_ResetPosition : MonoBehaviour
{
	Vector3 defaultPos;
	void Start() {
		defaultPos = transform.position;
	}
	public void Reset () { 
		transform.position = defaultPos;
	}
}