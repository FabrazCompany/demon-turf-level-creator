using UnityEngine;

public class EventUtility_Rotation : MonoBehaviour
{
	[SerializeField, Range (-1, 1)] int rotateX;
	[SerializeField, Range (-1, 1)] int rotateY;
	[SerializeField, Range (-1, 1)] int rotateZ;
	[SerializeField] float force = 90;
    [SerializeField] AnimationCurve scalarCurve;

	public void ApplyRotation (float pct) 
	{
		// var vel = rigid.angularVelocity;
		var delta = Time.deltaTime;
		var rot = transform.localEulerAngles;
		rot.x += scalarCurve.Evaluate (pct) * force * rotateX * delta;
		rot.y += scalarCurve.Evaluate (pct) * force * rotateY * delta;
		rot.z += scalarCurve.Evaluate (pct) * force * rotateZ * delta;
		transform.localEulerAngles = rot;
	}
    // void Awake () 
    // {
    //     initialScale = transform.localScale;
    // }
	// public void ZeroScale () => transform.localScale = Vector3.zero;
    // public void ResetScale () => transform.localScale = initialScale;
	// public void ApplyScaling (float pct) => transform.localScale = Vector3.Lerp (initialScale, initialScale * targetScalar, scalarCurve.Evaluate (pct));
}