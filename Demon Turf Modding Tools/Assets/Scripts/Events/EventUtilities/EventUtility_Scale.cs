using UnityEngine;

public class EventUtility_Scale : MonoBehaviour
{
    Vector3 initialScale;
    [SerializeField] float targetScalar = 0;
    [SerializeField] AnimationCurve scalarCurve;

    void Awake () 
    {
        initialScale = transform.localScale;
    }
	public void ZeroScale () => transform.localScale = Vector3.zero;
    public void ResetScale () => transform.localScale = initialScale;
	public void ResetInitalScale () => initialScale = transform.localScale;
	public void ApplyScaling (float pct) => transform.localScale = Vector3.Lerp (initialScale, initialScale * targetScalar, scalarCurve.Evaluate (pct));
}