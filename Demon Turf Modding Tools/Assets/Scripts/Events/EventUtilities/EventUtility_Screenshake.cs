using System;
using UnityEngine;

public class EventUtility_Screenshake : MonoBehaviour 
{
	enum ShakeType {
		Decay,
		Sustain,
	}

	[SerializeField] ShakeType shakeType;
	[SerializeField] float magnitude;
	[SerializeField, Conditional ("shakeType", ShakeType.Decay)] float decay;

	static public event Action<float, float> onShakeDecayRequested;
	static public event Action<float> onShakeSustainRequested;
	static public event Action onShakeClearRequested;
	public void Trigger () 
	{
		switch (shakeType)
		{
			case ShakeType.Decay:
				onShakeDecayRequested?.Invoke (magnitude, decay);
				break;
			case ShakeType.Sustain:
				onShakeSustainRequested?.Invoke (magnitude);
				break;

		}
	} 

	public void Clear () => onShakeClearRequested?.Invoke ();

	// static public void TriggerShake (float magnitude) => onShakeSustainRequested?.Invoke (magnitude);
	// static public void TriggerShake (float magnitude, float decay) => onShakeDecayRequested?.Invoke (magnitude, decay);
	// static public void ClearShake () => onShakeClearRequested?.Invoke ();
}