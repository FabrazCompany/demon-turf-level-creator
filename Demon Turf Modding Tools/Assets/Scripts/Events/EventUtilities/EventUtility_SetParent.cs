using UnityEngine;

public class EventUtility_SetParent : MonoBehaviour 
{
    public void SetParent (Transform obj) 
    {
        if (obj)
            transform.SetParent (obj);
    }
    public void ClearParent () 
    {
        transform.SetParent (null);
    }

}