﻿using UnityEngine;

public class EventUtility_SetPlatformPath : MonoBehaviour
{
    [SerializeField] MovingPlatform platform;
    [SerializeField] BasePlatformPath[] paths;

    public void SetPath (int index)
    {
        if (platform == null)
        {
            Debug.LogError ("No Moving Platform assigned!");
            return;
        }
        if (index < 0 || index >= paths.Length)
        {
            Debug.LogError ("Index out of range for paths array!");
            return;
        }

        platform.SetPath (paths[index]);
    }
}
