using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventUtility_SetRotation : MonoBehaviour
{
	[SerializeField] Vector3 rotation;
	public void SetRotation () => transform.localEulerAngles = rotation;
}