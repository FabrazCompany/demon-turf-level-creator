using UnityEngine;

public class EventUtility_SpawnLayerDependentObject : MonoBehaviour
{
    [SerializeField] LayerMask triggerLayers;
    [SerializeField] GameObject spawned;
    public void TrySpawn (Collider hit)
    {
        if (triggerLayers.ContainsLayer (hit.gameObject.layer))
            TrashMan.spawn (spawned, transform.position);
    }
}