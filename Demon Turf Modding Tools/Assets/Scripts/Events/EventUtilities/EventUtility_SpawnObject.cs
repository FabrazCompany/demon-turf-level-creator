using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventUtility_SpawnObject : MonoBehaviour {
	[SerializeField] GameObject prefab;
	[SerializeField] float yOffset;
	[SerializeField] bool proximitySpawn;
	[SerializeField, Conditional ("proximitySpawn")] float proxmityDistance = 50f;

    public void SpawnObject (GameObject _object) => SpawnObject (_object, transform.position + (Vector3.up * yOffset), Quaternion.identity);
	public void SpawnObject () => SpawnObject (prefab, transform.position, transform.rotation);
	public void SpawnObject (Collider hit) => SpawnObject (prefab, hit.transform.position, Quaternion.identity);
	public void SpawnObjectFacingHit (Collider hit) => SpawnObject (prefab, transform.position, Quaternion.Euler (transform.HeadingPlanar (hit.transform)));
	public void SpawnObjectFacingForce (Vector3 force) => SpawnObject (prefab, transform.position, Quaternion.Euler (force));
	public void SpawnObject (GameObject obj, Vector3 pos, Quaternion rot) 
	{
		if (!obj)
			return;
		if (proximitySpawn && transform.DistanceSqr (Camera.main.transform.position) > proxmityDistance * proxmityDistance)
			return;
		TrashMan.spawn (obj, pos + (Vector3.up * yOffset), rot);
	}
}
