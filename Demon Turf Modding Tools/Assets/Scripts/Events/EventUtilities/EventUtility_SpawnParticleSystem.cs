using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventUtility_SpawnParticleSystem : MonoBehaviour {

    [SerializeField] bool overrideDuration;
    [SerializeField, Conditional ("overrideDuration")] float duration = 0.5f;

	[SerializeField] bool inheritRotation;

    public void SpawnParticleSystem (GameObject _object) {
        if (!_object.GetComponentInChildren<ParticleSystem> ())
        {
            Debug.Log ("No Particle System Present!", _object);
            return;
        }
            
		var rotation = inheritRotation ? transform.rotation : Quaternion.identity;
        var particle = TrashMan.spawn (_object, transform.position, rotation).GetComponentInChildren<ParticleSystem> (true);
        particle.Stop ();
        var main = particle.main;
        
        if (overrideDuration)
        {
            main.duration = duration;
        }

        particle.Play ();
    }
}
