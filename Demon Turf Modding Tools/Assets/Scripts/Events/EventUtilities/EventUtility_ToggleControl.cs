﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventUtility_ToggleControl : MonoBehaviour
{
    [SerializeField] UnityEvent onToggledActive;
    [SerializeField] UnityEvent onToggledInactive;

    public void ProcessToggle (bool active)
    {
        if (active)
            onToggledActive?.Invoke ();
        else
            onToggledInactive?.Invoke ();
    }    
}
