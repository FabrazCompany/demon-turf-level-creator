﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

[RequireComponent (typeof (Rigidbody))]
public class ImpactEffect : MonoBehaviour {
	public LayerMask groundLayer;
	public float repeatBuffer = 0.35f;
	public float velocityTiggerThreshold = 1;
	bool isVisible;
	float nextPlayTime;
	[SerializeField] UnityEvent onImpact;
	Rigidbody rigid;
	void Awake () 
	{
		rigid = GetComponent<Rigidbody> ();
	}

	const float startupIgnore = .5f;
	float startupTime;
	
	void OnCollisionEnter(Collision hit) 
	{
		if (rigid.IsSleeping()) return;
		if (nextPlayTime > Time.time) return;
		if (rigid.velocity.sqrMagnitude < velocityTiggerThreshold * velocityTiggerThreshold) return;
		nextPlayTime = Time.time + repeatBuffer;
		onImpact.Invoke();
	}
}
