﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnEnableEvent : MonoBehaviour
{
    
    [SerializeField] UnityEvent onEnable;

    bool startHasRun;

    void Start () 
    {
        onEnable?.Invoke ();
        startHasRun = true;
    }

    void OnEnable () 
    {
        if (startHasRun)
            onEnable?.Invoke ();
    }
}
