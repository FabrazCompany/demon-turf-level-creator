using System.Collections.Generic;
using UnityEngine;

public class OnParticleSystemCollisionListener : MonoBehaviour
{
	[SerializeField] ParticleSystem particle;
	[SerializeField] UnityEventVector3 onCollision;
	List<ParticleCollisionEvent> collisionEvents;
	void OnValidate ()
	{
		if (!particle)
			particle = GetComponent<ParticleSystem> ();
	}
	void Awake ()
	{
		collisionEvents = new List<ParticleCollisionEvent> ();
	}
	void OnParticleCollision(GameObject other)
    {
		int numCollisionEvents = particle.GetCollisionEvents(other, collisionEvents);
		for (int i = 0; i < numCollisionEvents; i++)
		{
			onCollision?.Invoke (collisionEvents[i].intersection);
		}
	}
}