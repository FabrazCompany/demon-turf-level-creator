using UnityEngine;
using UnityEngine.Events;

public class OnParticleSystemStoppedListener : MonoBehaviour
{
    [SerializeField] UnityEvent onParticleSystemDone;
    void OnParticleSystemStopped () {
        onParticleSystemDone?.Invoke ();
    }

}