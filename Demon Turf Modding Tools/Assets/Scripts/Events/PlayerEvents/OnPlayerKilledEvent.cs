﻿using UnityEngine;
using UnityEngine.Events;

public class OnPlayerKilledEvent : MonoBehaviour
{
    [SerializeField] UnityEvent onPlayerKilled;
    void OnEnable () 
    {
		PlayerControllerEvents.onReset += TriggerEvent;
    }
    void OnDisable () 
    {
		PlayerControllerEvents.onReset -= TriggerEvent;
    }
    void TriggerEvent () => onPlayerKilled?.Invoke ();
}
