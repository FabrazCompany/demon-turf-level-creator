﻿using UnityEngine;
using UnityEngine.Events;
public class Timer : MonoBehaviour {
	[SerializeField, ReadOnly] float timer;
	[SerializeField] float duration;
	[SerializeField] float variance;
	[SerializeField] bool ignoreTimeScale;
	[SerializeField] bool repeat;
	[SerializeField, Tooltip ("If true, progress event will decrease to 0, rather than increase to 1")] bool subtractiveProgress;

	[SerializeField] UnityEventFloat onTimerStarted;
	[SerializeField] UnityEventFloat onTimerUpdated;
	[SerializeField] UnityEvent onTimerFinished;

	float scalar = 1;	
	public float getDuration => duration;
	public void SetScalar (float _value) => scalar = _value;
	public UnityEvent getOnTimerFinished => onTimerFinished;



	#if UNITY_EDITOR
	public void InitEvents () 
	{
		if (onTimerStarted == null)
			onTimerStarted = new UnityEventFloat ();
		if (onTimerUpdated == null)
			onTimerUpdated = new UnityEventFloat ();
		if (onTimerFinished == null)
			onTimerFinished = new UnityEvent ();
	}
	#endif
	void OnEnable() 
	{
		if (variance > 0)
			timer = -(Random.value * variance);
		else timer = 0;
		onTimerStarted?.Invoke (duration);
	}
	void Update () {
		timer += (ignoreTimeScale ? Time.unscaledDeltaTime : Time.deltaTime) * scalar;
		var pct = timer / duration;
		if (subtractiveProgress)
			pct = 1 - pct;

		onTimerUpdated?.Invoke (pct);
		if (timer >= duration) 
		{
			if (repeat) 
			{
				timer = 0;
			} 
			else 
			{
				enabled = false;
			}
			onTimerFinished.Invoke();	
		}
	}

	public void SetTimer (float newDur, bool shouldEnable = true) 
	{
		timer = 0;
		duration = newDur;
		enabled = shouldEnable;
	}
}
