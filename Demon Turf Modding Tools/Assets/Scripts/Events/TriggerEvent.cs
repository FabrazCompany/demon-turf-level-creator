using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEvent : MonoBehaviour
{
    [SerializeField, TagSelector] List<string> allowedTags;
    [SerializeField] UnityEventCollider onEnter;
    [SerializeField] UnityEventCollider onExit;
    [SerializeField] float repeatDelay;
    float repeatTimer;

	bool ShouldProcess (Collider other)
	{
		if (repeatTimer > Time.time)
            return false;

		bool matchFound = true;
		for (int i = 0; i < allowedTags.Count; i++)
		{
			matchFound = other.CompareTag (allowedTags[i]);
			if (matchFound) break;
		}
		return matchFound;
	}
    public void OnTriggerEnter (Collider hit)
	{ 
		if (!ShouldProcess (hit))
			return;
        repeatTimer = Time.time + repeatDelay;
        onEnter?.Invoke (hit);
    }
    public void OnTriggerExit (Collider hit) {
        if (!ShouldProcess (hit))
			return;
        repeatTimer = Time.time + repeatDelay;
        onExit?.Invoke (hit);
    }
}
