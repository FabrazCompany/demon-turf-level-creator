﻿using UnityEngine;
using UnityEngine.Jobs;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;

public struct AIOptimizerData 
{	
	public bool updatePhysics;
}
public struct AIOptimizerJobData 
{
	public bool optimize;
	public float distPCT;
}

[BurstCompile(CompileSynchronously = false)]
struct AIOptimizerJob : IJobParallelForTransform
{
	public float3 camPos;
	public float maxDistance;
	
	[WriteOnly]
	public NativeArray<AIOptimizerJobData> stateArray;

	void IJobParallelForTransform.Execute(int transformIndex, TransformAccess transform)
	{
		float dist = math.distancesq (camPos, transform.position);
		float distCheck = (maxDistance * maxDistance);
		stateArray[transformIndex] = new AIOptimizerJobData () 
		{ 
			optimize = dist >= distCheck,
			distPCT = dist / distCheck
		};
	}	
}

public class AIManager : FlyweightTransformJobManager<AINPC, AIOptimizerData, AIOptimizerJobData, AIManager>
{
	Camera cam;
	Transform camTr;
	bool isJobScheduled;
	JobHandle jobHandle;
	NativeArray<AIOptimizerJobData> tempJobArray;

	const float optimizeDistanceStandard = 40f;
	const float optimizeDistanceBoss = 80;
	float optimizeDistance;

	protected virtual void Start () 
	{
#if !DT_MOD
		CameraSpawnedBroadcast.OnCameraSpawned += SetCamera;
		SceneTransitionManager.onLevelLoadTriggered += SetOptimizeDistance;
#endif
		SetOptimizeDistance (LevelUtility.GetLevelID ());
		SetCamera (Camera.main);
	}
	void OnDestroy () 
	{
#if !DT_MOD
		CameraSpawnedBroadcast.OnCameraSpawned -= SetCamera;
		SceneTransitionManager.onLevelLoadTriggered -= SetOptimizeDistance;
#endif
	}
	void SetCamera (Camera _cam) 
	{
		if (cam == _cam)
			return;

		cam = _cam;
		camTr = cam.transform;
	}
	void SetOptimizeDistance (LevelID levelID)
	{
		switch (levelID)
		{
			default:
				optimizeDistance = optimizeDistanceStandard;
				break;
			case LevelID.Apocadesert_Boss:
			case LevelID.Armadageddon_Boss:
			case LevelID.NewNeoCity_Boss:
			case LevelID.PeakPlateau_Boss:
				optimizeDistance = optimizeDistanceBoss;
				break;
		}
	}
	void FixedUpdate ()
	{
		var items = slots.RawItems;
		var length = slots.Count;
		for (int i = 0; i < length; i++)
		{
			ref var slot = ref items[i];
			if (!slot.active) continue;
			
			ref var d = ref slot.data;
			if (!d.updatePhysics) continue;

			ref var flyweight = ref slot.flyweight;
			flyweight.PhysicsUpdate ();
		}
	}
	void Update ()
	{
		if (SafetyDispose ()) return;

		var count = numJobData;
		tempJobArray = new NativeArray<AIOptimizerJobData> (count, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
		NativeArray<AIOptimizerJobData>.Copy (dataFields, 0, tempJobArray, 0, count);
		var job = new AIOptimizerJob ()
		{
			camPos = camTr.position,
			maxDistance = optimizeDistance,
			stateArray = tempJobArray
		};		
		jobHandle = job.Schedule (transforms);
		isJobScheduled = true;

		var items = slots.RawItems;
		var length = slots.Count;
		for (int i = 0; i < length; i++)
		{
			ref var slot = ref items[i];
			if (!slot.active || slot.jobIndex >= tempJobArray.Length) continue;
			
			ref var flyweight = ref slot.flyweight;
			flyweight.StateUpdate ();
		}
	}
	void LateUpdate ()
	{
		if (!camTr || !isJobScheduled) return;
			
		jobHandle.Complete ();		
		var items = slots.RawItems;
		var length = slots.Count;
		for (int i = 0; i < length; i++)
		{
			ref var slot = ref items[i];
			if (!slot.active || slot.jobIndex >= tempJobArray.Length) continue;
			
			ref var flyweight = ref slot.flyweight;
			var result = tempJobArray[slot.jobIndex];
			flyweight.OptimizeUpdate (result.optimize, result.distPCT);
			flyweight.VisualsUpdate ();
		}
		tempJobArray.Dispose ();
		isJobScheduled = false;
	}
	bool SafetyDispose ()
	{
		if (camTr) return false;

		if (isJobScheduled)
		{
			isJobScheduled = false;
			jobHandle.Complete ();
		}
		if (tempJobArray.IsCreated)
		{
			tempJobArray.Dispose ();
		}
		return true;
	}

	public override void unregisterAll ()
	{
		if (isJobScheduled)
		{
			jobHandle.Complete ();
			isJobScheduled = false;
		}
		if (tempJobArray.IsCreated)
			tempJobArray.Dispose ();
		base.unregisterAll ();
	}
}