using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AINPC : FlyweightTransformJobBehavior<AINPC, AIOptimizerData, AIOptimizerJobData, AIManager>, 
	IFlashTarget
{
	[Header ("General")]
	[SerializeField] protected LayerMask groundLayer;
	[SerializeField] protected float movementAnimationThreshold = 1;
	[SerializeField] protected float maxSpinSpeedStable = 8;
	[SerializeField] protected float maxSpinSpeed = 20;
	[SerializeField] protected Rigidbody rigid;
	[SerializeField] protected ModStub_AIPath pathing;
	[SerializeField] protected SpriteRenderer rend;
	[SerializeField] protected Animator anim;

	Transform mainCam;
	// float initialRepathRate;
	bool pathingUsesRotation = true;
	protected bool optimize;
	float optimizeTimer;
	bool hasParameter_isMoving;
	
	protected AISense_TargetDetector senses;
	protected List<IResumePathingModifier> resumePathingModifiers;

	protected SlowMoEntity timeScale;
	protected List<IPathingSpeedModifier> pathingSpeedModifiers;
	

	public bool isStunned { get; protected set; }
	public bool isStabilizing { get; protected set; }
	public virtual bool isAttacking => false;
	public float getTimeScale => timeScale ? timeScale.timeScale : 1;
	public float getDeltaTime => Time.deltaTime * getTimeScale;
	public bool isSlowed => timeScale ? timeScale.timeScale < 1 : false;
	

	public LayerMask GroundLayer => groundLayer;

	public UnityEventTransform onTargetSighted => senses?.onTargetSighted;
	public UnityEvent onTargetLost => senses?.onTargetLost;

	const float initialRepathRate = .3f;
	const float optimizedRepathRate = 4;
	bool hasPathing;
	bool hasRigid;
	int animID_moving;


	protected virtual void OnValidate ()
	{
		if (!rend)
			rend = GetComponentInChildren<SpriteRenderer> ();
		if (!rigid && TryGetComponent<Rigidbody> (out var _rigid))
			rigid = _rigid;
		if (!pathing && TryGetComponent<ModStub_AIPath> (out var _pathing))
			pathing = _pathing;
	}

	protected virtual void Awake () 
	{
		if (!rigid)
			rigid = GetComponent<Rigidbody> ();
		if (rigid)
			rigid.useGravity = false;
		if (!rend)
			rend = GetComponentInChildren<SpriteRenderer> ();
		if (!anim)
			anim = rend?.GetComponent<Animator> ();
		if (!pathing)
			pathing = GetComponent<ModStub_AIPath> ();
		if (pathing)
			pathing.FindComponents ();

		hasPathing = pathing != null;
		hasRigid = rigid != null;
		animID_moving = Animator.StringToHash ("isMoving");

		senses = GetComponentInChildren<AISense_TargetDetector> ();
		
		resumePathingModifiers = new List<IResumePathingModifier> ();
		GetComponentsInChildren<IResumePathingModifier> (true, resumePathingModifiers);
		
		timeScale = GetComponent<SlowMoEntity> () ?? gameObject.AddComponent<SlowMoEntity> ();
		timeScale.onTimeScaleChanged += OnTimeScaleAdjusted;
		
		pathingSpeedModifiers = new List<IPathingSpeedModifier> ();
		GetComponentsInChildren<IPathingSpeedModifier> (true, pathingSpeedModifiers);
		foreach (var pathingSpeedMod in pathingSpeedModifiers)
		{
			pathingSpeedMod.onModValuesUpdated += OnPathingSpeedModsAdjusted;
		}
		mainCam = Camera.main.transform;
	}
	protected virtual void Start ()
	{
		if (anim)
			hasParameter_isMoving = anim.HasParameter ("isMoving");
	}

	
	protected override AIOptimizerJobData InitializeJobData() => new AIOptimizerJobData () {};

	void OnDestroy () 
	{
		if (timeScale)
			timeScale.onTimeScaleChanged -= OnTimeScaleAdjusted;
		if (pathingSpeedModifiers != null)
		{
			foreach (var pathingSpeedMod in pathingSpeedModifiers)
			{
				if (pathingSpeedMod != null)
				pathingSpeedMod.onModValuesUpdated -= OnPathingSpeedModsAdjusted;
			}
		}		
	}

	protected override void OnEnable () 
	{
		base.OnEnable ();
		ref var d = ref data;
		d.updatePhysics = false; 

		
		if (hasRigid)
		{
			rigid.velocity = Vector3.zero;
			rigid.angularVelocity = Vector3.zero;
			rigid.isKinematic = false;
		}
		
		optimize = false;
		pathingUsesRotation = true;

		if (hasPathing)
		{
			pathing.canMove = true;
			pathing.isStopped = false;
			pathing.enableRotation = pathingUsesRotation;
			pathing.updateRotation = pathingUsesRotation;
			pathing.Teleport (transform.position, true);
		}
			
	}

	void OnTimeScaleAdjusted (float magnitudeDifference) 
	{
		if (hasPathing)
		{
			pathing.maxSpeed *= magnitudeDifference;
			pathing.rotationSpeed *= magnitudeDifference;
		}
	}

	public void OptimizeUpdate (bool _optimize, float _pct)
	{
		var wasOptimized = optimize;
		optimize = _optimize;
		if (hasPathing)
		{
			pathing.repathRate = Mathf.Lerp (initialRepathRate, optimizedRepathRate, _pct);
			pathing.canMove = !optimize;
		}
		if (hasRigid)
		{
			if (!optimize)
				rigid.isKinematic = false;
			else 
				rigid.isKinematic = !isStabilizing;
		}
	}
	public virtual void StateUpdate ()
	{
		if (optimize) return;
		
		UpdatePathing();
		SpinReactionUpdate ();
	} 
	public virtual void VisualsUpdate ()
	{
		if (optimize) return;
		if (!anim || !anim.isActiveAndEnabled || !hasParameter_isMoving) return;
		if (hasPathing)
			anim.SetBool(animID_moving, !pathing.isStopped 
										&& pathing.canMove 
										&& pathing.velocity.sqrMagnitude >= movementAnimationThreshold);
		else if (hasRigid)
			anim.SetBool(animID_moving, rigid.velocity.sqrMagnitude >= movementAnimationThreshold);
	}
	public virtual void PhysicsUpdate () {}

	
#region Pathing
	void UpdatePathing()
	{
		if (!hasPathing) return;

		if (pathing.isStopped && ShouldResumePathing()) 
		{
			pathing.isStopped = false;
			pathing.enableRotation = pathingUsesRotation;
			pathing.updateRotation = pathingUsesRotation;
			pathing.Teleport (transform.position);
			pathing.SearchPath ();
		} 
		else if (!pathing.isStopped && ShouldHaltPathing()) 
		{
			pathing.isStopped = true;
			pathing.enableRotation = false;
			pathing.updateRotation = false;
		}
	}
	bool ShouldHaltPathing ()
	{
		for (int i = 0; i < resumePathingModifiers.Count; i++)
		{
			if (resumePathingModifiers[i].ShouldSkipCheck ())
				continue;
			if (resumePathingModifiers[i].ShouldHaltPathing ())
				return true;
		}
		return false;
	}
	bool ShouldResumePathing ()
	{
		for (int i = 0; i < resumePathingModifiers.Count; i++) 
		{
			if (resumePathingModifiers[i].ShouldSkipCheck ())
				continue;
			if (!resumePathingModifiers[i].ShouldResumePathing ())
				return false;
		}
		return true;
	}
#endregion


	public float currentMoveSpeed { get; private set; }
	public float currentAcceleration { get; private set; }
	public float currentTurnSpeed { get; private set; }

	float? tempMoveSpeed { get; set; }
	float? tempAcceleration { get; set; }
	float? tempTurnSpeed { get; set; }
	public virtual bool isAwareOfTarget => senses ? senses.isAwareOfTarget : false;    

	public void SetMoveSpeeds (float speed, float accel, float turn) {
		currentAcceleration = accel;
		currentMoveSpeed = speed;
		currentTurnSpeed = turn;
		ResetMoveSpeeds ();
	}
	public void SetTemporaryMoveSpeeds (float speed, float accel, float turn) 
	{
		if (!hasPathing) return;

		tempMoveSpeed = speed;
		tempAcceleration = accel;
		tempTurnSpeed = turn;

		var mod = CalculateSpeedMod ();
		pathing.maxAcceleration = accel * mod;
		pathing.maxSpeed = speed * mod;
		pathing.rotationSpeed = turn * mod;
	}
	public void ResetMoveSpeeds () {
		if (!hasPathing) return;

		tempMoveSpeed = null;
		tempAcceleration = null;
		tempTurnSpeed = null;

		var mod = CalculateSpeedMod ();
		pathing.maxAcceleration = currentAcceleration * mod;
		pathing.maxSpeed = currentMoveSpeed * mod;
		pathing.rotationSpeed = currentTurnSpeed * mod;
	}
	public void SetRotationHandling (bool _enableRotation, bool _slowWhenNotFacingTarget)
	{
		if (hasPathing)
		{
			if (!isStabilizing)
			{
				pathing.enableRotation = _enableRotation;
				pathing.updateRotation = _enableRotation;
			}
				
			pathingUsesRotation = _enableRotation;
			pathing.slowWhenNotFacingTarget = _slowWhenNotFacingTarget;
		}
	}
	float CalculateSpeedMod () 
	{
		var mod = 1f;
		foreach (var speedMod in pathingSpeedModifiers)
		{
			mod *= speedMod.modifier;
		}
		return mod;
	}
	void OnPathingSpeedModsAdjusted (float changeMagnitude)
	{
		if (!hasPathing) return;
		var mod = CalculateSpeedMod ();
		pathing.AdjustTimeScale (mod);
	}

#region TURNING
	[Header ("Turning")]
	[SerializeField] float turnDampingCoffecient = 200;
	public Transform turnAimFocus;

	const float turnSpeedScalar = .004f;
	protected virtual bool shouldRotate => true;
	public virtual void RotateToTarget (Vector3 _target, float _turnSpeed, bool flattenY = true) {
		if (Mathf.Approximately (_target.x, transform.position.x) && Mathf.Approximately (_target.z, transform.position.z))
			return;
		if (flattenY)
			_target.y = transform.position.y;
		
		if (hasRigid && rigid.isKinematic)
		{   
			var pos = turnAimFocus ? turnAimFocus.position : transform.position;
			var heading = (_target - pos).normalized;
			rigid.MoveRotation (Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation (heading, transform.up), _turnSpeed * getDeltaTime));
		}
		else
		{
			RotateToTarget (_target, _turnSpeed);
		}
	}
	public void RotateToTarget (Vector3 _target) 
	{
		RotateToTarget (_target, currentTurnSpeed);
	}
	public void RotateToTarget (Vector3 _target, float _turnSpeed) 
	{
		if (!shouldRotate) return;
		var _targetDir = (_target.With (y: transform.position.y) - transform.position).normalized;
		var _endDir = Vector3.RotateTowards (transform.forward, _targetDir, _turnSpeed * Mathf.Deg2Rad * getDeltaTime, 1);
		transform.forward = _endDir;
	}
	public void SetInitialPositionAndRotation (Vector3 position, Quaternion rotation) 
	{
		pathing.FindComponents ();
		pathing.Teleport (position, true);
		pathing.rotation = rotation;
	}
#endregion

#region CUSTOM SPIN REACTION 
	[Header ("Custom Spin Reaction")]
	[SerializeField] bool spinReactionEnabled;
	[SerializeField] UnityEvent spinReaction_triggered;
	[SerializeField, Conditional ("spinReactionEnabled")] int spinReaction_triggerCount = 3;
	[SerializeField, Conditional ("spinReactionEnabled")] int spinReaction_decayRate = 2;
	[Space]
	[SerializeField, Conditional ("spinReactionEnabled")] SimpleBillboard spinReactionCustom_root;
	[SerializeField, Conditional ("spinReactionEnabled")] float spinReactionCustom_dur = .5f;
	[SerializeField, Conditional ("spinReactionEnabled")] float spinReactionCustom_force = 25f;
	[SerializeField, Conditional ("spinReactionEnabled")] AnimationCurve spinReactionCustom_curve; 
	
	int spinReactionTally;
	float spinDecayTimer;

	public void TriggerSpinReaction ()
	{
		if (!spinReactionEnabled)
			return;
		spinReactionTally++;
		spinDecayTimer = spinReaction_decayRate;
		if (spinReactionTally >= spinReaction_triggerCount)
		{
			spinReactionTally = 0;
			spinReaction_triggered?.Invoke ();
		}
		if (spinReactionCustom_root)
		{
			this.RestartCoroutine (SpinReactionCustomRoutine (), ref spinReactionCustomRoutine);
		}
	}
	Coroutine spinReactionCustomRoutine;
	IEnumerator SpinReactionCustomRoutine ()
	{
		var timer = 0f;
		if (hasRigid)
			rigid.velocity = Vector3.zero;

		var stoppedState = hasPathing ? pathing.isStopped : false;
		if (hasPathing)
		{
			pathing.isStopped = true;
		}
		while (timer < spinReactionCustom_dur)
		{	
			timer += getDeltaTime;
			spinReactionCustom_root.SetOffset (spinReactionCustom_curve.Evaluate (timer / spinReactionCustom_dur) * spinReactionCustom_force);
			yield return null;
		}
		if (hasPathing)
		{
			pathing.isStopped = stoppedState;
		}
		spinReactionCustom_root.SetOffset (0);
	}
	void SpinReactionUpdate ()
	{
		if (spinReactionTally <= 0) return;
		spinDecayTimer -= getDeltaTime;
		if (spinDecayTimer <= 0)
		{
			spinReactionTally--;
			spinDecayTimer = spinReaction_decayRate;
		}
	}
	public void ClearSpinReactions ()
	{
		spinReactionTally = 0;
	}
#endregion

#if UNITY_EDITOR

	[ContextMenu ("Snap To Ground")]
	public void SnapToGround () {
		if (Physics.Raycast (transform.position, Vector3.down, out RaycastHit hitInfo, 500, groundLayer)) {
			transform.position = transform.position.With (y: hitInfo.point.y);
		}
	}

#endif
}
