using UnityEngine;

public abstract class AIActive : MonoBehaviour, IResumePathingModifier
{    
	[SerializeField] protected bool drawDebug;
    [SerializeField] protected float destinationReachedProximity = 2;
    [Header ("Speed")]
    [SerializeField] protected float turnSpeed = 90;

    protected virtual float getMoveSpeed => 0;
    protected virtual float getMoveAcceleration => 0;

    
    public void SetTurnSpeed (float val) 
    {
        turnSpeed = val;
        CheckRefreshPathingValues ();
    }
    protected virtual void CheckRefreshPathingValues () 
    {
        if (enabled)
            owner?.SetMoveSpeeds (getMoveSpeed, getMoveAcceleration, turnSpeed);
    }

    protected AINPC owner;
	protected ModStub_AIPath movement;
    protected Transform target;
    protected AISense_HazardDetector hazardDetector;
    protected AISense_TargetDetector targetDetector;
    protected virtual bool ShouldHandleRotation () => false;
	protected float distanceSqr;
	bool IResumePathingModifier.ShouldSkipCheck () => !gameObject.activeInHierarchy;
    public virtual bool ShouldResumePathing () => !enabled || distanceSqr > destinationReachedProximity * destinationReachedProximity;
    public virtual bool ShouldHaltPathing () => enabled && distanceSqr < destinationReachedProximity * destinationReachedProximity;

    protected event System.Action onPreppingTrigger;

    protected virtual void Awake () 
    {
        owner = GetComponentInParent<AINPC> ();
		if (owner)
			movement = owner.GetComponent<ModStub_AIPath> ();

        var isPrimaryActive = owner?.transform == transform;
        hazardDetector = owner?.GetComponentInChildren<AISense_HazardDetector> (true);
        targetDetector = owner?.GetComponentInChildren<AISense_TargetDetector> (true);
        if (targetDetector)
            targetDetector.onTargetSet += AssignTarget;

        if (isPrimaryActive)   
        {
            targetDetector?.onTargetSighted.AddListener ((Transform tr) => TriggerActive ());
            targetDetector?.onTargetLost.AddListener (CancelActive);

            var attacks = owner?.GetComponentsInChildren<IAIAttack> (false);
            for (int i = 0; i < attacks.Length; i++)
            {
                var attack = attacks[i];
                if (attack.CancelPathingOnUse || attack.transform.GetComponent<AIActive> ())
                    attack.getOnAttackStarted.AddListener (CancelActive);
                attack.getOnAttackEnded.AddListener (TriggerActive);
            } 
        }
        else 
        {   // is sub action
            var pairedAttack = GetComponent<IAIAttack> ();
            pairedAttack?.getOnAttackStarted?.AddListener (TriggerActive);
            pairedAttack?.getOnAttackEnded?.AddListener (CancelActive);
        }

        var actives = GetComponentsInChildren<AIActive> (true);
        foreach (var active in actives)
        {
            if (active == this)
                continue;
            
            active.onPreppingTrigger += SafetyCancel;
        }
        enabled = false;
    }
    void OnDestroy () 
    {
        if (targetDetector)
            targetDetector.onTargetSet -= AssignTarget;

        var actives = GetComponentsInChildren<AIActive> (true);
        foreach (var active in actives)
        {
            if (active == this)
                return;
            
            active.onPreppingTrigger -= SafetyCancel;
        }
    }
	void OnDisable ()
	{
		if (!gameObject.activeSelf)
			enabled = true;
	}
    void AssignTarget (Transform _target) => target = _target;
    protected virtual void TriggerActive () 
    {
        owner?.SetMoveSpeeds (getMoveSpeed, getMoveAcceleration, turnSpeed);
        owner?.SetRotationHandling (true, true);
		if (movement)
			movement.enabled = true;
        enabled = true;
    }
    protected virtual void CancelActive ()
    {
        enabled = false;
    }
    protected void SafetyCancel () 
    {
        if (enabled)
            CancelActive ();
    }
    void Update ()
    {
        if (hazardDetector && hazardDetector.isActiveAndEnabled && hazardDetector.reacting)
            return;
		
		distanceSqr = Vector3.SqrMagnitude (transform.position - target.position.With (y: transform.position.y));
        UpdateDestination ();
    }
    protected virtual void FixedUpdate ()
    {
        if (ShouldHandleRotation () && owner && !owner.isStunned)
        {
            owner?.RotateToTarget (target.position, turnSpeed);
        }
    }

    protected abstract void UpdateDestination ();

    protected virtual void OnDrawGizmosSelected()
    {
        if (!drawDebug)
            return;
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere (transform.position, destinationReachedProximity);
    }
}

public abstract class AIActiveMovement : AIActive
{
    [SerializeField] protected float moveSpeed = 4;
    [SerializeField] protected float moveAcceleration = 5;

    protected override float getMoveSpeed => moveSpeed;
    protected override float getMoveAcceleration => moveAcceleration;
    public void SetMoveSpeed (float val) 
    {
        moveSpeed = val;
        CheckRefreshPathingValues ();
    }
    public void SetMoveAcceleration (float val) 
    {
        moveAcceleration = val;
        CheckRefreshPathingValues ();
    }
}