﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (SubmersionListener))]
public class AIActive_BuoyantStationary : AIActiveMovement
{
    [SerializeField] float maxDistanceFromStartPoint = 5;

    [Header ("Handling")]
    [SerializeField] AnimationCurve facingSpeedDropoff;
    [SerializeField] float approachingDestinationDistance = 4;
    [SerializeField] AnimationCurve approachingDestinationDropoff;
    [SerializeField] float turningVerticalLimit = .5f;
    
    bool paddleTriggered;
    bool atRest;
    Rigidbody rigid;
    SubmersionListener submersion;
    // AITrait_Aquatic aquatic;
    Vector3 startPos;
    

    protected override void Awake ()
    {
        base.Awake ();
        rigid = owner.GetComponent<Rigidbody> ();
        submersion = owner.GetComponent<SubmersionListener> ();
        atRest = true;
        startPos = transform.position;
    }
    public void TriggerPaddle ()
    {
        if (!enabled) return;
        if (!submersion.getUnderLiquid) return;
        if (atRest) return;
        
        paddleTriggered = true;
    }
    protected override void UpdateDestination () {
		if (atRest)
        {
            if (distanceSqr >= maxDistanceFromStartPoint * maxDistanceFromStartPoint)
                atRest = false;
        }
        else
        {
            if (distanceSqr <= destinationReachedProximity * destinationReachedProximity)
                atRest = true;
        }
    }

    protected override void FixedUpdate ()
    {
        base.FixedUpdate ();

		distanceSqr = Vector3.SqrMagnitude (transform.position - startPos);
		if (atRest) return;

		owner.RotateToTarget (startPos, turnSpeed);
		
		if (!paddleTriggered)
			return;
		
		paddleTriggered = false;

		var targetSpeed = moveAcceleration;
		if (Mathf.Abs (transform.position.y - startPos.y) > turningVerticalLimit)
		{
			var flatHeading = startPos.With (y: transform.position.y) - transform.position;
			targetSpeed *= facingSpeedDropoff.Evaluate (1 - (Vector3.Angle (transform.forward, flatHeading) / 180));
		}
		if (distanceSqr < approachingDestinationDistance * approachingDestinationDistance)
		{
			var distance = Vector3.Distance (transform.position, startPos);
			targetSpeed *= approachingDestinationDropoff.Evaluate (distance / approachingDestinationDistance);
		}
		var heading = (startPos - transform.position).normalized;
		rigid.AddForce (heading * targetSpeed, ForceMode.Impulse);
    }



    const float debugSphereRadius = .1f;
#if UNITY_EDITOR
    protected override void OnDrawGizmosSelected() 
    {
        base.OnDrawGizmosSelected ();
		if (!drawDebug)
            return;
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere (transform.position, maxDistanceFromStartPoint);
    }
#endif
}
