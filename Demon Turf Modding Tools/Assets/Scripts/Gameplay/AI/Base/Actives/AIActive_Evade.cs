using UnityEngine;
#if !DT_EXPORT && !DT_MOD
using Pathfinding;
#endif
public class AIActive_Evade : AIActiveMovement
{
    [Header ("Evasion")]
    [SerializeField, VectorLabels ("Min", "Max")] Vector2 preferredDistance = new Vector2 (8, 10);
    [SerializeField] float fleeSpeed = 8;
    [SerializeField] float fleeAcceleration = 5f;
    [SerializeField] float fleePathUpdateRate = 0.5f;
    [SerializeField] int targetGScore = 4000;
    [SerializeField] int spread = 4000;
    [SerializeField] bool stayFacingTarget;

    ModStub_Seeker seeker;
    float repathTimer;
    bool fleeing;
    bool calculatingPath;
	bool shouldFlee;
	bool shouldPursue;
    bool shouldSpaceTarget => !shouldFlee && !shouldPursue;
    Vector3 fleeDestination;

    public override bool ShouldHaltPathing() => base.ShouldHaltPathing () && !shouldFlee;
    protected override bool ShouldHandleRotation () => stayFacingTarget && movement.enabled;

    public void SetStayFacingTarget (bool faceTarget) => stayFacingTarget = faceTarget;
    public void SetFleeSpeed (float value) => fleeSpeed = value;
    public void SetFleeAcceleration (float value) => fleeAcceleration = value;
    public void SetPreferredDistanceMin (float value) => preferredDistance.x = value;
    public void SetPreferredDistanceMax (float value) => preferredDistance.y = value;

    protected override void Awake () 
    {
        base.Awake ();
        seeker = owner.GetComponent<ModStub_Seeker> ();
		fleePoint = new GameObject ("Flee Point").transform;
    }
	Transform fleePoint;
    void OnEnable () 
    {
        calculatingPath = false;
        hazardDetector?.onHazardDetected.AddListener (NoticeHazard);
    }
    void OnDisable ()
    {
        hazardDetector?.onHazardDetected.RemoveListener (NoticeHazard);
    }
    protected override void TriggerActive()
    {        
        base.TriggerActive ();
        movement.canSearch = false;
        if (stayFacingTarget)
            owner.SetRotationHandling (false, false);
    }
    protected override void CancelActive () 
    {
        if (!enabled)
            return;
        base.CancelActive ();
        movement.canSearch = true;
        movement.canMove = true;
        movement.destination = transform.position;
    }
    protected override void UpdateDestination () 
    {
		var wasPreviouslyFleeing = shouldFlee;
		shouldFlee = distanceSqr <= preferredDistance.x * preferredDistance.x;
		shouldPursue = distanceSqr >= preferredDistance.y * preferredDistance.y;
        if (shouldFlee)    // too close to player, run!
        {
            var fleeDir = (movement.steeringTarget - transform.position).normalized;
            var targetDir = (target.position - transform.position).normalized;
            var routeCutoff = Vector3.Dot (fleeDir, targetDir) > 0.5f;
            if (!fleeing || (repathTimer < Time.time && (routeCutoff || movement.reachedDestination)))
            {
                Flee ();
            }
        }
        else if (shouldPursue)  //too far from player, pursue!
        {
            if (!movement.canMove || movement.reachedDestination || !movement.hasPath || wasPreviouslyFleeing)
            {
                Pursue ();
            }
        }
        else            //in buffer zone, stay still and follow player rotation
        {
            if (movement.canMove)
            {
                movement.canMove = false;
                fleeing = false;
            }
        }
		fleePoint.position = fleeDestination;
        movement.destination = fleeing ? fleeDestination : target.position;
    }
    void NoticeHazard () 
    {
        movement.canMove = true;
    }
    void Flee ()
    {
        if (calculatingPath) return;
        fleeing = true;
        repathTimer = Time.time + fleePathUpdateRate;
        calculatingPath = true;
#if !DT_EXPORT && !DT_MOD
		FleePath path = FleePath.Construct (transform.position, target.position, targetGScore, SetEscapePath);
        path.aimStrength = 0.9f;
        path.spread = spread;
        AstarPath.StartPath (path, true);
#endif
        owner.SetMoveSpeeds (fleeSpeed, fleeAcceleration, turnSpeed);
    }
#if !DT_EXPORT && !DT_MOD
    void SetEscapePath (Path p)
    {
		if (!seeker) return;
        FleePath fpath = p as FleePath;
        fleeDestination = fpath.endPoint;
        movement.canMove = true;
        calculatingPath = false;
        seeker.StartPath(transform.position, fleeDestination);
    }
#endif
    void Pursue ()
    {
        movement.SearchPath ();
        movement.canMove = true;
        fleeing = false;
        owner.SetMoveSpeeds (moveSpeed, moveAcceleration, turnSpeed);
    }
    

    protected override void OnDrawGizmosSelected()
    {
        base.OnDrawGizmosSelected ();
		if (!drawDebug) return;
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere (transform.position, preferredDistance.x);
        Gizmos.DrawWireSphere (transform.position, preferredDistance.y);
    }
}