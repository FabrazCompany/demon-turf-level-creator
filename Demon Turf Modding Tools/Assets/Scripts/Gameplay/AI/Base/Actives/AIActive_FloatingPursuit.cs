﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (SubmersionListener))]
public class AIActive_FloatingPursuit : AIActive
{
    [SerializeField] float moveSpeed = 20;
    [Header ("Handling")]
    [SerializeField] AnimationCurve facingSpeedDropoff;
    [SerializeField] float approachingDestinationDistance = 4;
    [SerializeField] AnimationCurve approachingDestinationDropoff;
    [SerializeField] float turningVerticalLimit = .5f;
    
    [Header ("Feedback")]
    [SerializeField] Transform facingRoot;
    [SerializeField] float facingRootAdjustSpeed = 90;

    bool paddleTriggered;
    Vector3 destination;
    Rigidbody rigid;
    SubmersionListener submersion;
    protected override void Awake ()
    {
        base.Awake ();
        rigid = owner.GetComponent<Rigidbody> ();
        submersion = owner.GetComponent<SubmersionListener> ();
    }
    public void TriggerPaddle ()
    {
        if (!enabled) return;        
        if (!submersion.getInLiquid) return;
            
        paddleTriggered = true;
    }
    protected override void UpdateDestination ()
    {
        destination = target.position;

        var heading = (destination - transform.position).normalized;
        if (heading != Vector3.zero)
        {
            var targetRotation = Quaternion.LookRotation (heading, Vector3.up);
            facingRoot.rotation = Quaternion.RotateTowards (facingRoot.rotation, targetRotation, facingRootAdjustSpeed * Time.deltaTime);
        }
    }
    protected override void FixedUpdate ()
    {
        base.FixedUpdate ();
		distanceSqr = Vector3.SqrMagnitude (transform.position - destination);
		if (distanceSqr <= destinationReachedProximity * destinationReachedProximity) return;

		owner.RotateToTarget (destination, turnSpeed);
		
		if (!paddleTriggered) return;
		
		paddleTriggered = false;

		var targetSpeed = moveSpeed;
		
		if (Mathf.Abs (transform.position.y - destination.y) > turningVerticalLimit)
		{
			var flatHeading = destination.With (y: transform.position.y) - transform.position;
			targetSpeed *= facingSpeedDropoff.Evaluate (1 - (Vector3.Angle (transform.forward, flatHeading) / 180));
		}
			
		var heading = (destination - transform.position).normalized;
		rigid.AddForce (heading * targetSpeed, ForceMode.Impulse);
    }
}
