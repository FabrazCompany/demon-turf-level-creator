using UnityEngine;

public class AIActive_LookAt : AIActive
{
    bool canTurn = true;
    public void SetCanTurn (bool _turn) => canTurn = _turn;
    protected override bool ShouldHandleRotation() => canTurn; 
    protected override void UpdateDestination () {}
}