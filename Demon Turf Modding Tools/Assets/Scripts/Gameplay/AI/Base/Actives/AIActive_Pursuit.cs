public class AIActive_Pursuit : AIActiveMovement
{
    protected override bool ShouldHandleRotation () => false; 
    protected override void UpdateDestination () 
    {
        movement.destination = target.position;
    }
}