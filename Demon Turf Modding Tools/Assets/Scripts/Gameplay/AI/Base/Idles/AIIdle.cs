using UnityEngine;
public abstract class AIIdle : MonoBehaviour
{   
	[SerializeField] protected bool drawDebug; 
    [SerializeField] protected float destinationReachedProximity = 0.01f;
    [Header ("Speed")]
    [SerializeField] protected float idleMoveSpeed = 2;
    [SerializeField] protected float idleMoveAcceleration = 5;
    [SerializeField] protected float idleTurnSpeed = 60;
	
    protected AINPC owner;
    protected Animator anim;    
    protected Vector3 startPos;

    protected virtual void Awake () 
    {
        anim = GetComponentInChildren <Animator> (true);
        owner = GetComponent<AINPC> ();

        var senses = owner.GetComponentInChildren<AISense_TargetDetector> (true);
        senses?.onTargetSighted.AddListener ((Transform tr) => CancelIdle ());
        senses?.onTargetLost.AddListener (TriggerIdle);
        enabled = true;
    }
    protected virtual void Start () 
    {
        startPos = transform.position;
        TriggerIdle ();
    }
	void OnDisable ()
	{
		if (!gameObject.activeSelf)
			enabled = true;
	}
    protected virtual void TriggerIdle () {
        owner.SetMoveSpeeds(idleMoveSpeed, idleMoveAcceleration, idleTurnSpeed);
        owner.SetRotationHandling (true, true);
        enabled = true;
    }
    protected virtual void CancelIdle () {
        enabled = false;
    }

    protected virtual void OnDrawGizmosSelected()
    {
        if (!drawDebug)
            return;
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere (transform.position, destinationReachedProximity);
    }
}