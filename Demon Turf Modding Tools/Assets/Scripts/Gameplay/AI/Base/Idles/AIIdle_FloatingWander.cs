﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (SubmersionListener))]
public class AIIdle_FloatingWander : AIIdle
{
    [Header ("Wandering")]
    [SerializeField] float wanderDistance = 5;
    [SerializeField] float wanderIdleMin = 1;
    [SerializeField] float wanderIdleMax = 2;
    [SerializeField] float minWanderDistance = 2;

    [Header ("Handling")]
    [SerializeField] AnimationCurve facingSpeedDropoff;
    [SerializeField] float approachingDestinationDistance = 4;
    [SerializeField] AnimationCurve approachingDestinationDropoff;
    [SerializeField] float turningVerticalLimit = .5f;
    
    [Header ("Feedback")]
    [SerializeField] Transform facingRoot;
    [SerializeField] float facingRootAdjustSpeed = 90;

    bool paddleTriggered;
    float idleTimer;  
    Vector3 destination;
    Rigidbody rigid;
    SubmersionListener submersion;
    // AITrait_Aquatic aquatic;

    protected override void Awake ()
    {
        base.Awake ();
        rigid = GetComponent<Rigidbody> ();
        submersion = GetComponent<SubmersionListener> ();
        // aquatic = GetComponent<AITrait_Aquatic> ();
    }
    protected override void TriggerIdle()
    {
        base.TriggerIdle ();
        idleTimer = Random.Range (wanderIdleMin, wanderIdleMax);
        UpdateWanderMarker ();
    }
    public void TriggerPaddle ()
    {
        if (!enabled) return;
        if (!submersion.getInLiquid) return;
        if (idleTimer > 0) return;
        
        paddleTriggered = true;
    }
	float distanceSqr;
    protected void Update () {
        if (idleTimer > 0) 
        {
            idleTimer -= Time.deltaTime;
            if (idleTimer <= 0) {
                UpdateWanderMarker ();
            }
        }
		else if (distanceSqr <= destinationReachedProximity * destinationReachedProximity && idleTimer <= 0) 
        {   //wander behavior and check for attack target
            idleTimer = Random.Range (wanderIdleMin, wanderIdleMax);
        } 

        if (rigid.velocity.sqrMagnitude > 0.1f)
        {
            var heading = (destination - transform.position).normalized;
            if (heading != Vector3.zero)
            {
                var targetRotation = Quaternion.LookRotation (heading, Vector3.up);
                facingRoot.rotation = Quaternion.RotateTowards (facingRoot.rotation, targetRotation, facingRootAdjustSpeed * Time.deltaTime);
            }
        }
    }

    void FixedUpdate ()
    {
		distanceSqr = Vector3.SqrMagnitude (transform.position - destination);
		if (distanceSqr <= destinationReachedProximity * destinationReachedProximity) return;
        
		owner.RotateToTarget (destination, idleTurnSpeed);
		
		if (!paddleTriggered) return;
		
		paddleTriggered = false;

		var targetSpeed = idleMoveSpeed;
		var flatHeading = destination.With (y: transform.position.y) - transform.position;

		if (Mathf.Abs (transform.position.y - destination.y) > turningVerticalLimit)
		{
			targetSpeed *= facingSpeedDropoff.Evaluate (1 - (Vector3.Angle (transform.forward, flatHeading) / 180));
		}
		
		
		if (distanceSqr < approachingDestinationDistance * approachingDestinationDistance)
		{
			var distance = Vector3.Distance (transform.position, destination);
			targetSpeed *= approachingDestinationDropoff.Evaluate (distance / approachingDestinationDistance);
		}
			
		var heading = (destination - transform.position).normalized;
		rigid.AddForce (heading * targetSpeed, ForceMode.Impulse);
        
    }
    
    
    void UpdateWanderMarker () 
    {
        var newDestination = GetRandomWanderPoint ();
        for (int i = 0; i < 50; i++)
        {
            var validSelection = true;
            if (Vector3.SqrMagnitude (transform.position - newDestination) <= minWanderDistance * minWanderDistance)
                validSelection = false;
            else if (Physics.Linecast (transform.position, newDestination, owner.GroundLayer))
                validSelection = false;                

            if (validSelection)
                break;
            else 
                newDestination = GetRandomWanderPoint ();
        }
        destination = newDestination;
    }
    Vector3 GetRandomWanderPoint()
    {
        var offset = Random.insideUnitSphere * wanderDistance;
        return startPos + offset;
    }



    const float debugSphereRadius = .1f;
    [Header ("Debugging")]
    [SerializeField] bool drawWandering = true;
#if UNITY_EDITOR
    protected override void OnDrawGizmosSelected() 
    {
        base.OnDrawGizmosSelected ();
		if (!drawDebug)
            return;
        if (drawWandering) 
        {
            var pos = Application.isPlaying ? startPos : transform.position;
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere (pos, wanderDistance);
            Gizmos.DrawSphere (destination, debugSphereRadius);

            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere (transform.position, minWanderDistance);
        }
    }
#endif
}
