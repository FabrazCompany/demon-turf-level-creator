using UnityEngine;

public class AIIdle_Patrol : AIIdle {
    enum LoopType {
        Loop,
        PingPong,
        OneWay
    }
    [Header ("Patrolling")]
    [SerializeField] Transform[] waypoints;
    [SerializeField] LoopType loopType;
    [SerializeField] float patrolIdleMin;
    [SerializeField] float patrolIdleMax;
	[SerializeField] float repathSafetyCheckRate = 0.2f;
    float idleTimer;  
	float repathSafetyCheck;
    int patrolIndex;
    int patrolDirection;
    protected ModStub_AIPath pathing;

    protected override void Awake ()
    {
        base.Awake ();
        pathing = GetComponent<ModStub_AIPath> ();
    }

    protected override void TriggerIdle()
    {
        base.TriggerIdle ();
        if (waypoints == null || waypoints.Length == 0) {
            Debug.LogWarning ("No waypoints set for enemy movement!", this);
            return;
        }
        patrolIndex = -1;
        patrolDirection = 1;
		idleTimer = Random.Range (patrolIdleMin, patrolIdleMax);
        // UpdatePatrolPoint ();
    }
    
    protected void Update () {
        if (waypoints == null || waypoints.Length == 0)
            return;

        if (pathing.reachedEndOfPath) 
        {   //wander behavior and check for attack target
			var delta = Time.deltaTime;
			repathSafetyCheck -= delta;
			if (repathSafetyCheck <= 0)
			{
				repathSafetyCheck = repathSafetyCheckRate;
				if (!pathing.InGraph ())
				{
					UpdatePatrolPoint ();
					return;
				}
			}


            if (idleTimer < 0)
                idleTimer = Random.Range (patrolIdleMin, patrolIdleMax);
            if (idleTimer >= 0) 
            {
                idleTimer -= delta;
                if (idleTimer <= 0) {
                    UpdatePatrolPoint ();
                }
            }
        }
    }

    void SetNearestPatrolPoint () {
        patrolIndex = 0;
        var dist = Mathf.Infinity;
        for (int i = 0; i < waypoints.Length; i++) {
            if (Vector3.SqrMagnitude (transform.position - waypoints[i].position) < dist * dist) {
                patrolIndex = i - patrolDirection;
            }
        }
        UpdatePatrolPoint ();
    }
    void UpdatePatrolPoint () {
        var newIndex = patrolIndex + patrolDirection;
        if (newIndex < 0) {
            if (loopType == LoopType.Loop) {
                patrolIndex = waypoints.Length;
            } else if (loopType == LoopType.PingPong) {
                patrolDirection *= -1;
            } else {
                enabled = false;
            }
        } else if (patrolIndex >= waypoints.Length - 1) {
            if (loopType == LoopType.Loop) {
                patrolIndex = -1;
            } else if (loopType == LoopType.PingPong) {
                patrolDirection *= -1;
            } else {
                enabled = false;
            }
        }

        patrolIndex += patrolDirection;
        idleTimer = -1;
        pathing.destination = waypoints[patrolIndex].position;
    }

    

    const float debugSphereRadius = .1f;
    [Header ("Debugging")]
    [SerializeField] bool drawPatrolRoute = true;
#if UNITY_EDITOR
    protected override void OnDrawGizmosSelected() 
    {
        base.OnDrawGizmosSelected ();
		if (!drawDebug)
            return;
        if (drawPatrolRoute) 
        {
            Gizmos.color = Color.green;
            for (int i = 0; i < waypoints?.Length; i++) 
            {
                if (waypoints[i]) {
                    Gizmos.DrawSphere (waypoints[i].position, debugSphereRadius); 
                    if (i < waypoints.Length - 2) 
                    {
                        Gizmos.DrawLine (waypoints[i].position, waypoints[i + 1].position);
                    } else if (loopType == LoopType.Loop) {
                        Gizmos.DrawLine (waypoints[i].position, waypoints[0].position);
                    }
                }
            }
        }
    }
#endif
}