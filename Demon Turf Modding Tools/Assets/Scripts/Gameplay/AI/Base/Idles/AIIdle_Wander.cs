using UnityEngine;

public class AIIdle_Wander : AIIdle {
    [Header ("Wandering")]
    [SerializeField] float wanderDistance = 5;
    [SerializeField] float wanderIdleMin = 1;
    [SerializeField] float wanderIdleMax = 2;
    float idleTimer;  
	float timeNotMoving;
	ModStub_AIPath pathing;
    ModStub_RVOController rvo;    
    ModStub_Seeker meseeks;
    const float idleSpeedThreshold = .5f;
	const float idleSpeedTimeout = 2;
    protected override void Awake ()
    {
        base.Awake ();
		pathing = GetComponent<ModStub_AIPath> ();
        rvo = GetComponent<ModStub_RVOController> ();
        meseeks = GetComponent<ModStub_Seeker> ();
    }
    protected override void Start() 
    {
        base.Start (); 
        
        if (meseeks)
        {
#if !DT_MOD && !DT_EXPORT
            meseeks.pathCallback += (Pathfinding.Path p) => {
                if (p.error)
                    UpdateWanderMarker ();
            };
#endif
        }
    }
    protected override void TriggerIdle()
    {
        base.TriggerIdle ();
        idleTimer = Random.Range (wanderIdleMin, wanderIdleMax);
    }
    protected void Update () 
    {
		if (!pathing.canMove) return;

		float deltaTime = owner.getDeltaTime;
        if (idleTimer <= 0) 
        {   //wander behavior and check for attack target
			var shouldRepath = pathing.reachedDestination;
			if (!shouldRepath)
			{
				if (pathing.velocity.sqrMagnitude < (idleSpeedThreshold * idleSpeedThreshold))
				{
					timeNotMoving += deltaTime;
					if (timeNotMoving >= idleSpeedTimeout)
					{
						shouldRepath = true;
					}
				}
				else 
					timeNotMoving = 0;
			}
			
			if (shouldRepath)
			{
				idleTimer = Random.Range (wanderIdleMin, wanderIdleMax);
				pathing.isStopped = true;
			}  
        }

		if (!pathing.pathPending && idleTimer > 0) 
        {
            idleTimer -= deltaTime;
            if (idleTimer <= 0) {
				pathing.isStopped = false;
				if (!pathing.hasPath || pathing.reachedDestination || timeNotMoving >= idleSpeedTimeout)
	                UpdateWanderMarker ();
            }
        }		
    }


    void UpdateWanderMarker () 
    {
#if !DT_MOD && !DT_EXPORT
        if (!AstarPath.active)
        {
            pathing.destination = transform.position;
            return;    
        }
        var targetNode = AstarPath.active.GetNearest (GetRandomWanderPoint ());
		pathing.destination = targetNode.position;
#endif
    }

    void RefreshWanderMarker () 
    {
        idleTimer = Random.Range (wanderIdleMin, wanderIdleMax);
        UpdateWanderMarker ();
    }
    Vector3 GetRandomWanderPoint()
    {
        var newPos = startPos;
        var offset = Random.insideUnitCircle * wanderDistance;
        newPos.x += offset.x;
        newPos.z += offset.y;
        return newPos;
    }


    const float debugSphereRadius = .1f;
    [Header ("Debugging")]
    [SerializeField] bool drawWandering = true;
#if UNITY_EDITOR
    protected override void OnDrawGizmosSelected() 
    {
        base.OnDrawGizmosSelected ();
		if (!drawDebug)
            return;
        if (drawWandering) 
        {
            var pos = Application.isPlaying ? startPos : transform.position;
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere (pos, wanderDistance);
            if (pathing)
                Gizmos.DrawSphere (pathing.destination, debugSphereRadius);
        }
    }
#endif
}