using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
// using Pathfinding;

public class AISense_HazardDetector : MonoBehaviour
{
	[SerializeField] bool drawDebug;
    [SerializeField] float repeatRate = 0.1f;
    [SerializeField] LayerMask threatLayer;

    [Header ("Reaction")]
    [SerializeField] float reactionDistance = 2;
    [SerializeField, Range (-1, 1), Tooltip ("Higher values ignore less directly threatening projectiles nearby")]  float evadeCaution = 0.7f;
    [SerializeField, Range (-1, 1), Tooltip ("Higher values ignore less directly threatening projectiles nearby")]  float panicCaution = 0.3f;
    [SerializeField, Tooltip ("Distance at which ai reacts, regardless of evade caution")] float panicDistance = 1.5f;
    [SerializeField, Range (-1, 1), Tooltip ("The lower the value, the wider range of detection. 1 is front, -1 is back.")] float awarnessSpread = -.3f;
    
    [Header ("Movement")]
    [SerializeField, Tooltip ("Distance ai will try to move to from threat")] float evadeDist = .5f;
    [SerializeField, Range (0, 1), Tooltip ("Higher values prioritizes following target over evading projectiles")] float followTargetWeight = 0f;
    [SerializeField] float reactionMoveSpeed = 4;
    [SerializeField] float reactionAcceleration = 10;
    [SerializeField] float reactionTurnSpeed = 120;
    [SerializeField] bool reactionRequireTurning;

    ModStub_AIPath ai;
    AIEnemy owner;
    AISense_TargetDetector senses;
    List<ObstaclePair> obstacles;
    Transform target;

    bool initialFacingBehaviour;
    float initialMoveSpeed;
    float initialAcceleration;
    float initialTurnSpeed;
    float chosenDirection;

    public UnityEvent onHazardDetected;
    public UnityEvent onHazardsCleared;
    public bool reacting => chosenDirection != 0;
    public LayerMask getThreatLayer => threatLayer;

    void Awake () 
    {
        ai = GetComponentInParent<ModStub_AIPath> ();
        owner = GetComponentInParent<AIEnemy> ();
        senses = owner.GetComponentInChildren<AISense_TargetDetector> (true);

        obstacles = new List<ObstaclePair> ();
        CacheMovementValues ();
        enabled = false;
        senses.onTargetSet += AssignTarget;
    }
    void OnDestroy () 
    {
        senses.onTargetSet -= AssignTarget;
    }
    void OnEnable () 
    {
        CacheMovementValues ();
    }
    void OnDisable () {
        obstacles?.Clear ();
        chosenDirection = 0;
        if (ai)
            ResetMovementValues ();
    }

    public bool ApproachingHazard (float distance) 
    {
        foreach (var obstacle in obstacles)
        {
            // if (Vector3.Distance (obstacle.gameObject.transform.position, transform.position) <= distance)
			if (Vector3.SqrMagnitude (transform.position - gameObject.transform.position) <= distance * distance)
            {
                var heading = (obstacle.gameObject.transform.position - transform.position).normalized;
                if (Vector3.Dot (heading, transform.forward) > 0.7f)
                {
                    return true;
                }
            }
        }
        return false;
    }

    void CacheMovementValues () 
    {
        initialFacingBehaviour = ai.slowWhenNotFacingTarget;
        initialMoveSpeed = ai.maxSpeed;
        initialAcceleration = ai.maxAcceleration;
        initialTurnSpeed = ai.rotationSpeed;
    }
    void SetMovementValues ()
    {
        ai.slowWhenNotFacingTarget = reactionRequireTurning;
        owner.SetTemporaryMoveSpeeds (reactionMoveSpeed, reactionAcceleration, reactionTurnSpeed);
    }
    void ResetMovementValues () 
    {
        ai.slowWhenNotFacingTarget = initialFacingBehaviour;
        owner.SetMoveSpeeds (owner.currentMoveSpeed, owner.currentAcceleration, owner.currentTurnSpeed);
        // owner.ResetMoveSpeeds ();
    }
    void AssignTarget (Transform newTarget) 
    {
        target = newTarget;
    }


    void OnTriggerEnter (Collider hit) {
        var kill = hit.GetComponent<IHazard> ();
        if (kill == null)
            return;
        
        if (!obstacles.Exists (i => i.hazard == kill)) 
        {
            obstacles.Add (new ObstaclePair () 
            {
                hazard = kill,
                gameObject = hit.gameObject
            });
            if (!enabled)
                enabled = true;
        }
    }
    void OnTriggerExit (Collider hit) {
        var kill = hit.GetComponent<IHazard> ();
        if (kill == null)
            return;
        
        if (obstacles.Exists (i => i.hazard == kill)) 
        {
            // obstacles.Remove (kill);
            obstacles.RemoveAll (i => i.hazard == kill);
        }
    }
    
    float timeout;
    void Update () 
    {
        if (obstacles.Count == 0)
        {
            enabled = false;
            return;
        }
        
        timeout -= Time.deltaTime;
        if (timeout > 0)
            return;
        
        var hazardHeading = Vector3.zero;
        var hazardPosition = Vector3.zero;
        var hazarThreat = 0f;
        var hazardSize = 0f;
        var count = 0;
        obstacles.RemoveAll (i => i.gameObject == null || !i.gameObject.activeInHierarchy);
        foreach (var obstacle in obstacles)
        {
            // var projectileDistance = Vector3.Distance (obstacle.hazard.transform.position, transform.position) - (obstacle.hazard.getSize * 0.5f);
			var projectileDistance = Vector3.SqrMagnitude (transform.position - obstacle.hazard.transform.position) - ((obstacle.hazard.getSize * 0.5f) * (obstacle.hazard.getSize * 0.5f));
            if (projectileDistance <= reactionDistance * reactionDistance)
            {
                var projectileVisibility = Vector3.Dot (transform.forward, (obstacle.hazard.transform.position - transform.position).normalized);
                if (projectileVisibility > awarnessSpread)
                {
                    var projectileThreat = Vector3.Dot (obstacle.hazard.transform.forward, (transform.position - obstacle.hazard.transform.position).normalized);
                    var considredThreat = projectileDistance < panicDistance * panicDistance 
                        ? projectileThreat > panicCaution 
                        : projectileThreat > evadeCaution;
                    if (considredThreat)
                    {
                        hazardHeading += (obstacle.hazard.transform.position - transform.position).normalized;
                        hazardPosition += obstacle.hazard.transform.position;
                        hazarThreat += projectileThreat;
                        hazardSize += obstacle.hazard.getSize;
                        count++;
                    }
                }
            }   
        }

        if (count > 0)
        {
            hazardHeading /= count;
            hazardPosition /= count;
            hazarThreat /= count;
            hazardSize /= count;

            if (chosenDirection == 0)   //first reaction
            {
                CacheMovementValues ();
                AssignChosenDirection (hazardPosition, hazardHeading);
                onHazardDetected?.Invoke ();
            }

            var awayDir = (hazardPosition - transform.position).normalized * -1;  //opposite direction of bullet
            var sideDir = hazardHeading.Rotate (chosenDirection);   //sideways direction of bullet

			var pct = Mathf.InverseLerp (panicDistance * panicDistance, reactionDistance * reactionDistance, Vector3.SqrMagnitude (transform.position - hazardPosition));
            var evadeDir = Vector3.Lerp (awayDir, sideDir, pct);     //lerp between evade values based on bullet heading
            SetMovementValues ();
            ai.destination = transform.position + (evadeDir * (evadeDist + hazardSize));
            ai.SearchPath ();
        }
        else 
        {
            if (chosenDirection != 0)  //was previously reacting
            {
                ai.destination = transform.position;
                onHazardsCleared?.Invoke ();
            }
            chosenDirection = 0;
            ResetMovementValues ();
        }

        timeout = repeatRate;
    }
    void AssignChosenDirection (Vector3 hazardPosition, Vector3 hazardHeading) 
    {
        var leftPosition = hazardHeading.Rotate (-90) * 2 + hazardPosition;
        var rightPosition = hazardHeading.Rotate (90) * 2 + hazardPosition;
        var chooseRight = Vector3.Dot (transform.forward, (rightPosition - transform.position).normalized) > Vector3.Dot (transform.forward, (leftPosition - transform.position).normalized);
        chosenDirection = chooseRight ? 90 : -90;
    }

    [ContextMenu ("Print Size")]
    void PrintSize () 
    {
        Debug.Log (obstacles?.Count);
    }
    void OnDrawGizmosSelected () 
    {
		if (!drawDebug)
            return;
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere (transform.position, reactionDistance);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere (transform.position, panicDistance);
    }

    struct ObstaclePair
    {
        public GameObject gameObject;
        public IHazard hazard;
    }
}
