using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class AISense_TargetDetector : MonoBehaviour, IResumePathingModifier
{
	[SerializeField] bool drawDebug;
    [SerializeField, ReadOnly] bool _isAwareOfTarget;
    [SerializeField, ReadOnly] bool _hasLineOfSight;
	[SerializeField] float refreshRate = 0.3f;
	[SerializeField] float refreshRateVar = 0.1f;
	[SerializeField] float refreshLoSRate = 0.3f;
	[SerializeField] float refreshLoSRateVar = 0.1f;
	float refreshRateTimer;
	float refreshRateLoSTimer;

    [Header ("Engaging")]
    [SerializeField] LayerMask groundLayer;
    [SerializeField] float facingAngle = 80;
    [SerializeField] float engageDistance = 8;
    float scaledEngageDistance => engageDistance * (targetTraits != null ? targetTraits.visibilityLevel : 1);
    [SerializeField] float engageDelay = 1;
    [SerializeField] bool faceTargetDuringDelay = true;
    public UnityEventTransform onTargetSighted;

    [Header ("Disengaging")]
    [SerializeField] float disengageDistance = 15;
    float scaledDisengageDistance => disengageDistance * (targetTraits != null ? targetTraits.visibilityLevel : 1);
    [SerializeField] float lineOfSightLostDuration = 5;
    [SerializeField] Vector3 lineOfSightOffset = new Vector3 (0, 0.25f, 0);
    public UnityEvent onTargetLost; 

    [Header ("Leading")]
    [SerializeField] bool useShotLeading = true;
    [SerializeField, Conditional ("useShotLeading")] float shotLeadingMinimumDistance = 2f;
    [SerializeField, Conditional ("useShotLeading")] float shotLeadingMaximumDistance = 10f;
    [SerializeField, Conditional ("useShotLeading"), Range (0,1)] float shotLeading = 0.75f;

    [Header ("Modifiers")]
    [SerializeField, UnityEngine.Serialization.FormerlySerializedAs ("useStartPointForDisengage")] bool requireProximityToStartPoint;

    [SerializeField] Transform attackOverride;
    

    public bool isAwareOfTarget => _isAwareOfTarget;
    public bool hasLineOfSight => _hasLineOfSight;
    public Transform Target => attackTarget ? attackTarget : target;
    public event Action<Transform> onTargetSet;
    public Vector3 getLineOfSightOffset => lineOfSightOffset;
    public Vector3 leadingPosition { get; private set; }
    public Vector3 averagedVeloctiy { get; private set; }
    

    AINPC owner;
	Rigidbody rigidSelf;
    Transform target;
    public ITargetTraits targetTraits { get; private set; }
    Transform attackTarget;
    Rigidbody targetRigid;
    
    float timeSinceSawTarget;
    float engageTimer;
    Vector3 startPos;
    bool canSearch = true;

    public void SetDisengageDistance (float val) => disengageDistance = val;
    public void SetTargetDiscovery (bool _canSearch) => canSearch = _canSearch;

    void Awake ()
    {
        owner = GetComponentInParent<AINPC> ();
		rigidSelf = GetComponentInParent<Rigidbody> ();
        cachedVelocities = new List<Vector3> (); 

        PlayerControllerEvents.onSpawned += SetTarget;

#if !DT_EXPORT && !DT_MOD
        var dialogueTrigger = owner?.GetComponentInChildren<TriggerDialogue> ();
        if (dialogueTrigger)
        {
            dialogueTrigger.onDialogueStarted?.AddListener (DetectTarget);
        }
#endif
    }
    void Start () 
    {   //set target
        startPos = transform.position;
        FindTarget ();
    }
	void OnEnable () 
	{
		if (target)
		{
			if (CanDetectTarget ())
				DetectTarget ();
			else 
				LoseTarget ();
		}
	}
	void OnDisable ()
	{
		_isAwareOfTarget = false;
		_hasLineOfSight = false;
	}
    void OnDestroy () 
    {
        PlayerControllerEvents.onSpawned -= SetTarget;

#if !DT_EXPORT && !DT_MOD
        var dialogueTrigger = owner?.GetComponentInChildren<TriggerDialogue> ();
        if (dialogueTrigger)
        {
            dialogueTrigger.onDialogueStarted?.RemoveListener (DetectTarget);
        }
#endif
    }
    
    void Update () 
    {
        if (engageTimer > 0) 
        {
            transform.forward = (target.position.With (y: transform.position.y) - transform.position).normalized;
            engageTimer -= Time.deltaTime;
        } 
        else 
        {
			var time = Time.timeSinceLevelLoad;
			if (refreshRateTimer <= time)
			{
				refreshRateTimer = time + refreshRate + (refreshRateVar * UnityEngine.Random.value);
				if (!_isAwareOfTarget) 
				{
					if (CanDetectTarget ()) 
					{
						DetectTarget ();
					}
				} 
				else 
				{
					_hasLineOfSight = HasLineOfSight ();
					if (HasLostTarget ()) 
					{
						LoseTarget ();
					}
				}
			}
        }
    }
#if !DT_EXPORT && !DT_MOD
    void DetectTarget (TriggerDialogue.DialogueInteractionData data) => DetectTarget ();
#endif
    void DetectTarget ()
    {
        _isAwareOfTarget = true;
        _hasLineOfSight = true;
        if (engageTimer > 0) {
            engageTimer = engageDelay;
        }
        onTargetSighted?.Invoke (target);
    }
    void LoseTarget () 
    {
        _isAwareOfTarget = false;
        _hasLineOfSight = false;
        engageTimer = -1;
        onTargetLost?.Invoke ();
    }
    bool CanDetectTarget () 
    {
        if (attackTarget)
            return canSearch;
        if (!canSearch)
            return false;
        if (!target)
            return false;
        if (requireProximityToStartPoint && Vector3.SqrMagnitude (startPos - target.position) > scaledDisengageDistance * scaledDisengageDistance)
            return false;
        if (Vector3.SqrMagnitude (transform.position - target.position) > scaledEngageDistance * scaledEngageDistance)
            return false;
        if (!IsFacingTarget ())
            return false;
        if (!HasLineOfSight ())
            return false;
        return true;
    }
    public void SetAwareOfTarget (bool val) {
        if (!enabled)
            val = false;
        _isAwareOfTarget = val;
    }
    bool HasLostTarget () 
    {
        if (owner && owner.isAttacking)
            return false;
        if (attackTarget)
            return !canSearch;
        if (!target)
            return true;        
        if (requireProximityToStartPoint && Vector3.SqrMagnitude (startPos - target.position) > scaledDisengageDistance * scaledDisengageDistance)
            return true;
        if (Vector3.SqrMagnitude (transform.position - target.position) > scaledDisengageDistance * scaledDisengageDistance)
            return true;
        if (!HasLineOfSight ()) 
		{                
            timeSinceSawTarget += Time.deltaTime;
            if (timeSinceSawTarget >= lineOfSightLostDuration)
                return true;
        } else {
            timeSinceSawTarget = 0;
        }
        return false;
    }

    void FixedUpdate () 
    {
        leadingPosition = SetAimLeading ();
    }
    List<Vector3> cachedVelocities;
    const int velocityTallyCount = 30;
    Vector3 lastPos;
    protected virtual Vector3 SetAimLeading () 
    {
        if (!useShotLeading || !targetRigid || !_isAwareOfTarget)
            return Vector3.zero;
        
        var velocity = (targetRigid.position - lastPos) / Time.fixedDeltaTime;
        lastPos = targetRigid.position;
        cachedVelocities.Insert (0, velocity);
        if (cachedVelocities.Count >= velocityTallyCount)
            cachedVelocities.RemoveAt (velocityTallyCount - 1);
        
        averagedVeloctiy = Vector3.zero;
        var count = cachedVelocities.Count;
        for (int i = 0; i < count; i++) 
        {
            averagedVeloctiy += cachedVelocities[i];
        }
        averagedVeloctiy /= count;

        var pct = Mathf.InverseLerp (shotLeadingMinimumDistance * shotLeadingMinimumDistance, shotLeadingMaximumDistance * shotLeadingMaximumDistance, Vector3.SqrMagnitude (transform.position - target.position.With (y: transform.position.y)));
        return Vector3.Lerp (Vector3.zero, averagedVeloctiy, pct * shotLeading);
    }

#region PUBLIC
	bool IResumePathingModifier.ShouldSkipCheck () => !gameObject.activeInHierarchy;
    bool IResumePathingModifier.ShouldHaltPathing () => engageTimer > 0;
    bool IResumePathingModifier.ShouldResumePathing () => engageTimer <= 0;

    public void ForceDetectTarget ()
    {
        if (!_isAwareOfTarget)
            DetectTarget ();
    }

    void FindTarget () 
    {
        if (target) return; //already has target, cancel

        // var player = FindObjectOfType <IPlayerController_AttackTarget> ();
		var player = gameObject.scene.FindSceneComponent<IPlayerController> ();
        if (player != null)
            SetTarget (player);
    }
    public void ClearAttackTarget () 
    {
        attackTarget = null;
        FindTarget ();
    }
    public void SetAttackTarget (IPlayerController player) 
    {
        attackTarget = player?.getTarget;
        SetTarget (player);
        
    }

    public void SetAttackTarget (Transform target) 
    {
        attackTarget = target;
        SetTarget (target);
    }
	// public void SetTarget(PlayerController player)
	// {

	// }
    public void SetTarget (IPlayerController player)
    {
		targetRigid = player?.getRigidbody;
		SetTarget (player?.getTarget);
		targetTraits = player?.getTargetTraits;
        // targetRigid = player?.GetComponent<Rigidbody> ();
        // SetTarget (player?.getTarget);
        // targetTraits = player?.GetComponent<ITargetTraits> ();
    } 
    void SetTarget (Transform newTarget) 
    {
        target = newTarget;
        targetTraits = target?.GetComponent<ITargetTraits> ();
        lastPos = target.position;
        engageTimer = -1;
        if (target)
            onTargetSet?.Invoke (target);
    }
	bool cachedLoS;
    bool HasLineOfSight () => HasLineOfSight (groundLayer);
    bool HasLineOfSight (LayerMask blockerLayer) 
    {
        if (!target) return false;
		var time = Time.timeSinceLevelLoad;
		if (refreshRateLoSTimer > time) return cachedLoS;
		
		refreshRateLoSTimer = time + refreshLoSRate + (refreshLoSRateVar * UnityEngine.Random.value);
        var eyePoint = transform.position + transform.InverseTransformDirection (lineOfSightOffset);
        var blocked = Physics.Linecast (eyePoint, target.position, out RaycastHit info, blockerLayer) && info.rigidbody != rigidSelf;
		cachedLoS = !blocked;
        return !blocked;
    }
    public bool IsFacingTarget () => IsFacingTarget (facingAngle);
    public bool IsFacingTarget (float angle) => target ? IsFacingTarget (target.position, angle) : false;
    public bool IsFacingTarget (Vector3 targetPos) => IsFacingTarget (target.position, facingAngle);
    public bool IsFacingTarget (Vector3 targetPos, float angle) 
    {
        var _targetFacing = (targetPos.With (y: transform.position.y) - transform.position).normalized;
        return Vector3.Angle (transform.forward, _targetFacing) < angle;
    }
    public bool IsTargetFacingSelf (Transform target, float angle)
    {
        var _targetFacing = (transform.position - target.position.With (y: transform.position.y)).normalized;
        return Vector3.Angle (target.forward, _targetFacing) < angle;
    }
#endregion
    void OnDrawGizmosSelected () 
    {
		if (!drawDebug)
            return;
        Gizmos.color = new Color (1, .5f, 0, 1);
        var eyePoint = transform.position + transform.TransformDirection (lineOfSightOffset);
        Gizmos.DrawWireSphere (eyePoint, scaledEngageDistance);
        Gizmos.DrawLine (eyePoint, eyePoint + (Vector3.RotateTowards (transform.forward, transform.right, facingAngle * Mathf.Deg2Rad, 0) * engageDistance));
        Gizmos.DrawLine (eyePoint, eyePoint + (Vector3.RotateTowards (transform.forward, -transform.right, facingAngle * Mathf.Deg2Rad, 0) * engageDistance));

        var checkPos = eyePoint;
        if (requireProximityToStartPoint && Application.isPlaying)
            checkPos = startPos;

        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere (checkPos, scaledDisengageDistance);
    }
}