using UnityEngine.Events;
public interface IAISenses 
{
	UnityEvent onTargetSighted { get; }	
	UnityEvent onTargetLost { get; }
}