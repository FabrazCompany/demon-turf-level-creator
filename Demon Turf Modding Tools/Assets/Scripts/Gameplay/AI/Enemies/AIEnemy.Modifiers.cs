using System;
using UnityEngine;
public partial class AIEnemy 
{
    [System.Serializable]
    class Traits 
    {
        [SerializeField] bool useSpriteSheetOverride;
        [SerializeField] Sprite[] spriteSheetOverrides;

        AIEnemy owner;
        public void Init (AIEnemy _owner) 
        {
            owner = _owner;
        }

        public void HandleSpriteSheetOverrides () {
            if (!useSpriteSheetOverride)
                return;

            var newSprite = Array.Find (spriteSheetOverrides, item => item.name == owner.rend.sprite.name);
            if (newSprite)
                owner.rend.sprite = newSprite;
        }
    }
}