using UnityEngine;
using UnityEngine.Events;

public partial class AIEnemy 
{
    [System.Serializable]
    class Rage 
    {
        [SerializeField] RageValues rageValues;
        [SerializeField] ParticleSystem rageParticle;
        AIEnemy owner;
        Transform self;
        float rageBuildup;
        float timeSinceSightedPlayer;
        public bool isEnraged => rageParticle.isPlaying;    

        public float rageKnockbackResistance => isEnraged ? rageValues.knockbackReduction : 1;
        public float rageStunResistance => isEnraged ? rageValues.stunReduction : 1;
        public float rageTimerRatio => isEnraged ? rageValues.timerRatios : 1;
        public float rageMoveSpeedRatio => isEnraged ? rageValues.moveSpeedRatio : 1;
        public float rageTurnSpeedRatio => isEnraged ? rageValues.turnSpeedRatio : 1;


        public void Init (AIEnemy _owner) {
            owner = _owner;
            self = owner.transform;
            rageBuildup = 0;
            timeSinceSightedPlayer = 0;

            
            
            owner.onBounce += () => rageBuildup += rageValues.bounceImpact;
            // owner.onKnockbackStarted += () => rageBuildup += rageValues.knockbackImpact;

            var stun = owner.GetComponent<ReceiveStun> ();
            // stun?.onStunned.AddListener (() => rageBuildup += rageValues.stunImpact);
            
        }
        public void AddToRage (float value) {
            SetRageBuildup (rageBuildup + value);
        }

        void SetRageBuildup (float value) {
            if (!rageValues.disableRage) {
                if (!isEnraged && value > rageValues.triggerThreshold) {
                    rageParticle.Play ();
                } else if (isEnraged && value <= 0) {
                    rageParticle.Stop ();
                }
            }
            rageBuildup = Mathf.Clamp (value, 0, rageValues.triggerThreshold);;
        }
        

        public void UpdateRage () {
            if (rageValues.disableRage)
                return;

            var _rageBuildup = rageBuildup;
            var deltaTime = Time.deltaTime;
            if (owner.isAwareOfTarget && timeSinceSightedPlayer > rageValues.outOfSightDelay) {
                _rageBuildup += (rageValues.outOfSightGainRate * deltaTime);
            } else {
                _rageBuildup += (rageValues.chillRate * deltaTime);
            }

            if (isEnraged) {
                _rageBuildup += rageValues.activeRageDecayRate * deltaTime;
                if (self.localScale != self.localScale * rageValues.scaleBuff) {
                    // self.localScale = Vector3.MoveTowards (self.localScale, owner.targetScale * rageValues.scaleBuff, rageValues.scaleBuffSpeed * deltaTime);
                }
            } else if (self.localScale != owner.startScale) {
                // self.localScale = Vector3.MoveTowards (self.localScale, owner.targetScale, rageValues.scaleBuffSpeed * deltaTime);
            }

            SetRageBuildup (_rageBuildup);
            var pct = rageBuildup / rageValues.triggerThreshold;
            owner.rend.color = Color.Lerp (Color.white, rageValues.color, rageValues.colorShiftRate.Evaluate (pct));
        }

        [System.Serializable]
        struct RageValues {
            [Header ("General")]
            public bool disableRage;
            public Color color;
            public AnimationCurve colorShiftRate;
            public float triggerThreshold;
            public float activeRageDecayRate;

            [Header ("Rage Effectors")]
            public float knockbackImpact;
            public float stunImpact;
            public float bounceImpact;
            public float chillRate;
            public float outOfSightDelay;
            public float outOfSightGainRate;

            [Header ("Buffs")]
            public float scaleBuff;
            public float scaleBuffSpeed;
            public float moveSpeedRatio;
            public float turnSpeedRatio;
            public float timerRatios;
            public float knockbackReduction;
            public float stunReduction;
        }
    }
}