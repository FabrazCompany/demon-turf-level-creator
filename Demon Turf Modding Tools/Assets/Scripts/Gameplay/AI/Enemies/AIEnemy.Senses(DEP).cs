// using UnityEngine;
// using UnityEngine.Events;

// public partial class AIEnemy 
// {
//     [System.Serializable]
//     class Senses 
//     {
//         [Header ("Engaging")]
//         [SerializeField] bool canEngage = true;
//         [SerializeField] float facingAngle;
//         [SerializeField] float engageDistance;
//         [SerializeField] float engageDelay;
//         public UnityEvent onTargetSighted;
 
//         [Header ("Disengaging")]
//         [SerializeField] float disengageDistance;
//         [SerializeField] float lineOfSightLostDuration = 5;
//         [SerializeField] float lineOfSightOffset = 0.25f;

//         [Header ("Speeds")]
//         [SerializeField] protected float moveSpeed;
//         [SerializeField] protected float moveAcceleration;
//         [SerializeField] protected float turnSpeed;

//         public UnityEvent onTargetLost; 
        
//         public bool isAwareOfTarget { get; private set; }
//         public Transform Target => target;

//         AIEnemy owner;
//         Transform self;
//         Transform target;
//         float timeSinceSawTarget;
//         float engageTimer;

//         public void Init (AIEnemy _owner) {
//             owner = _owner;
//             self = owner.transform;
//             engageTimer = -1;
//         }
//         public void SetTarget (Transform newTarget) {
//             target = newTarget;
//             engageTimer = -1;
//         }
//         public bool HasLineOfSight () {
//             return HasLineOfSight (owner.groundLayer);
//         }
//         public bool HasLineOfSight (LayerMask blockerLayer) {
//             if (!target)
//                 return false;
//             var eyePoint = self.position + self.up * lineOfSightOffset;
//             return !Physics.Linecast (eyePoint, target.position, blockerLayer);
//         }
//         public bool IsFacingTarget () {
//             return IsFacingTarget (facingAngle);
//         }
//         public bool IsFacingTarget (float angle) {
//             if (!target)
//                 return false;
//             var _targetFacing = (target.position.With (y: self.position.y) - self.position).normalized;
//             return Vector3.Angle (self.forward, _targetFacing) < angle;
//         }
//         public void UpdateSenses () {
//             if (owner.isStunned)
//                 return;
//             if (owner.isStabilizing) 
//                 return;
            
//             if (engageTimer > 0) 
//             {
//                 owner.FaceToPlayer ();
//                 engageTimer -= Time.deltaTime;
//                 if (engageTimer <= 0) 
//                 {
//                     owner.pathing.isStopped = false;
//                 }
//             } 
//             else 
//             {
//                 if (!isAwareOfTarget) 
//                 {
//                     if (CanDetectTarget ()) 
//                     {
//                         SetAwareOfTarget (true);
//                         if (engageTimer > 0) {
//                             engageTimer = engageDelay;
//                             owner.pathing.isStopped = true;
//                         }
//                         onTargetSighted?.Invoke ();
//                     }
//                 } 
//                 else 
//                 {
//                     if (HasLostTarget ()) 
//                     {
//                         SetAwareOfTarget (false);
//                         engageTimer = -1;
//                         onTargetLost?.Invoke ();
//                     }
//                 }
//             }
//         }
//         public void SetAwareOfTarget (bool val) {
//             if (!canEngage)
//                 val = false;
//             isAwareOfTarget = val;
//         }
//         bool CanDetectTarget () {
//             if (!canEngage)
//                 return false;
//             if (!target)
//                 return false;
//             if (Vector3.Distance (self.position, target.position) > engageDistance)
//                 return false;
//             if (!IsFacingTarget ())
//                 return false;
//             if (!HasLineOfSight ())
//                 return false;
//             return true;
//         }
//         public bool HasLostTarget () {
//             if (!target)
//                 return true;
//             if (Vector3.Distance (self.position, target.position) > disengageDistance)
//                 return true;
//             if (!HasLineOfSight ()) {                
//                 timeSinceSawTarget += Time.deltaTime;
//                 if (timeSinceSawTarget >= lineOfSightLostDuration) {
//                     return true;
//                 }
//             } else {
//                 timeSinceSawTarget = 0;
//             }
//             return false;
//         }

//         public void DrawGizmos (Transform transform) {
//             if (!canEngage)
//                 return;
                
//             // Gizmos.color = Color.yellow;
//             Gizmos.color = new Color (1, .5f, 0, 1);
//             Gizmos.DrawWireSphere (transform.position, engageDistance);
//             Gizmos.DrawLine (transform.position, transform.position + (Vector3.RotateTowards (transform.forward, transform.right, facingAngle * Mathf.Deg2Rad, 0) * engageDistance));
//             Gizmos.DrawLine (transform.position, transform.position + (Vector3.RotateTowards (transform.forward, -transform.right, facingAngle * Mathf.Deg2Rad, 0) * engageDistance));

            
//             Gizmos.DrawWireSphere (transform.position, disengageDistance);
//         }
//     }
// }