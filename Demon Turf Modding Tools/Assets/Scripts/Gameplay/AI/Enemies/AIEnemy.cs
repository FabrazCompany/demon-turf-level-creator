using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public partial class AIEnemy : AINPC, IResumePathingModifier
{

	enum GroundedCheckStyle 
	{
		Raycast,
		FallVelocity,
		Resting
	}
    [SerializeField] List<IAIAttack> attacks;
    
    [Header ("Visuals")]
    [SerializeField] bool useSpriteSheetOverride;
    [SerializeField] Sprite[] spriteSheetOverrides;
	[SerializeField] Sprite[] spriteSheetOriginals;
    [SerializeField] AITrait_StunStars stunStars;
    const float groundContactDistance = 0.1f;
    [SerializeField] float skinWidth = 0.05f;
    [SerializeField] float scaleVariance;
    Vector3 startScale;
    float startDrag;
	

    [Header ("Physics")]
    [SerializeField, ReadOnly] float currentSpeed;
    [SerializeField, ReadOnly] float currentTorque;
	[Space]
    [SerializeField] Collider bodyHurtbox;
	[SerializeField] Collider[] additionalHurtboxes;
    [SerializeField] PhysicMaterial stableMaterial;
    [SerializeField] PhysicMaterial unstableMaterial;
	[SerializeField] PhysicMaterial stableMaterialSlowed;
    [SerializeField] PhysicMaterial unstableMaterialSlowed;
	[SerializeField] PhysicMaterial frozenMaterial;
	[Space]
    [SerializeField] bool stabilizeOnGround = true;
    [SerializeField] float restabilizeDelay;
    [SerializeField] float spinDragRatio = 0.5f;
    [SerializeField] float stunDragRatio = 0.3f;
	[SerializeField] float freezeDragRatio = 0.05f;
	[Space]
	[SerializeField] Transform hitFreezeFrameRoot;
	[SerializeField] float hitFreezeFrameDuration = 0.2f;
	[SerializeField] float hitFreezeFrameMagnitude = 0.25f;
	[SerializeField] int hitFreezeFrameVibrato = 30;
	Vector3 hitFreezeFrameRootPosition;
	[Space]
	[SerializeField] float angleResistDelay = 1f;
	[SerializeField] float angleResistForce = 5;
	[SerializeField] float angleResistRange = 45;
	[SerializeField] float angleResistDrag = 2f;
	float angleResistTimer;
	[Space]
	[SerializeField] GroundedCheckStyle groundedCheckStyle = GroundedCheckStyle.Raycast;
	[SerializeField, Conditional ("groundedCheckStyle", GroundedCheckStyle.Resting)] float groundedVelocity = 1f;
    float restabilizeDelayTimer = -1;

	[Header ("Attributes")]
	[SerializeField] ReceiveFreeze[] freezes;

    RaycastHit[] raycastHits;
    const int collisionCheckCount = 5;
	float groundAngle;
	Vector3 groundAngleDir;
    //States
    public bool isAlive { get; private set; }
    public bool isGrounded { get; private set; }
    public bool isFlying { get; private set; }
    public bool isFlyingBackToGround { get; private set; }
    public bool isHeld { get; private set; }
	public bool isFrozen { get; private set; }
    public override bool isAwareOfTarget => senses != null && senses.isAwareOfTarget;
    public override bool isAttacking => activeAttack != null && activeAttack.enabled;
    public Rigidbody getRigid => rigid;
    
    public event Action onDestabilized;
    public event Action onStabilized;
    public event Action onBounce;
    public event Action onGrounded;
    public event Action onLeftGround;
    public event Action onAttackStarted;
    public event Action onAttackStopped;
    public event Action<AIEnemy> onKilled;

    public static event Action<AIEnemy> onSpawned;

    AISense_HazardDetector hazardSenses;
    public const float ledgeCheckDist = 2f;
    IAIAttack activeAttack;
    List<IAttackAvailabilityModifier> attackAvailabilityModifiers;
    DeliverRicochetKnockback ricochet;
    List<ReceiveKnockback> knockbacks;
    List<ReceiveSpin> spins;

	AITrait_Flying flight;
	RaycastOptimizer raycaster;
    
	bool SETTING_freezeFrameActive;

    public bool knockbackActive => knockbacks.Find (i => i.isActiveAndEnabled);
    public bool spinActive => spins.Find (i => i.isActiveAndEnabled);
    public bool ricochetActive => ricochet?.isActiveAndEnabled ?? false;

    public void RegisterKnockback (ReceiveKnockback knockback) 
    {
        if (knockbacks.Contains (knockback)) 
            return;
        knockbacks.Add (knockback);
        knockback.onAttacked.AddListener (OnKnockback);
    }
    public void UnregisterKnockback (ReceiveKnockback knockback) 
    {
        if (!knockbacks.Contains (knockback))
            return;
        knockbacks.Remove (knockback);
        knockback.onAttacked.RemoveListener (OnKnockback);
    }

    struct ParentData 
    {
        public Transform _parent;
        public Rigidbody _rigid;
    }

#region LIFECYCLE
	protected override void OnValidate () 
	{
		base.OnValidate ();
		if (!hitFreezeFrameRoot && rend)
			hitFreezeFrameRoot = rend.transform;
	}

	RigidbodyConstraints rigidbodyConstraints;
    protected override void Awake () 
    {
        base.Awake ();
        isAlive = true;
        hazardSenses = GetComponentInChildren<AISense_HazardDetector> ();
        raycastHits = new RaycastHit[collisionCheckCount];
        attacks = new List<IAIAttack> ();
		hitFreezeFrameRootPosition = hitFreezeFrameRoot.localPosition;
        GetComponentsInChildren<IAIAttack> (false, attacks);
        attacks.Sort ((a1, a2) => 
        {
            if (a1.getOrderOverride == -1 && a2.getOrderOverride >= 0)
                return 1;
            else if (a2.getOrderOverride == -1 && a1.getOrderOverride >= 0)
                return -1;
            else if (a1.getOrderOverride == -1 && a2.getOrderOverride == -1)
                return 0;
            else 
                return a1.getOrderOverride.CompareTo (a2.getOrderOverride);
        });        

		
        List<Rigidbody> rigidList = new List<Rigidbody> ();
        List<ParentData> rigidParentList = new List<ParentData> ();
        GetComponentsInChildren<Rigidbody> (true, rigidList);
        rigidList.Remove (rigid);
        if (rigidList.Count > 0)
        {
            for (int i = 0; i < rigidList.Count; i++) 
            {
                rigidParentList.Add (new ParentData () 
                {  
                    _rigid = rigidList[i],
                    _parent = rigidList[i].transform.parent,
                    
                });
                rigidList[i].transform.SetParent (null);
            }
        }
        
        ricochet = GetComponent<DeliverRicochetKnockback> ();
        GetComponentsInChildren<ReceiveKnockback> (true, knockbacks = new List<ReceiveKnockback> ());
        knockbacks.RemoveAll (i => i.GetComponentInParent<Rigidbody> () != rigid);

        GetComponentsInChildren<ReceiveSpin> (true, spins = new List<ReceiveSpin> ());
        GetComponentsInChildren<IAttackAvailabilityModifier> (true, attackAvailabilityModifiers = new List<IAttackAvailabilityModifier> ());
        
        startDrag = rigid.drag;
        rigid.maxAngularVelocity = maxSpinSpeedStable;
		rigidbodyConstraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
		rigid.constraints |= RigidbodyConstraints.FreezeRotationY;
        flight = GetComponent<AITrait_Flying> ();
        isFlying = flight != null;        


        for (int i = 0; i < rigidParentList.Count; i++) 
        {
            var entry = rigidParentList[i];
            entry._rigid.transform.SetParent (entry._parent);
        }

		if (!raycaster)
		{
			raycaster = RaycastOptimizer.Create (
				gameObject, 
				new RaycastCommand (transform.position, Vector3.down, groundContactDistance + skinWidth, groundLayer, 1), 
				new Vector3 (0, skinWidth, 0)
			);
		}
		
    }
    protected override void Start () 
    {   
		base.Start ();
        ricochet?.onRicocheted.AddListener (OnRicocheted);

        for (int i = 0; i < knockbacks.Count; i++)
        {
            knockbacks[i].onAttacked.AddListener (OnKnockback);
        }
        for (int i = 0; i < spins.Count; i++)
        {
            spins[i].onSpun.AddListener (OnSpin);
            spins[i].onSpinEnded.AddListener (OnSpinEnded);
			spins[i].onPreSpin += OnPreSpin;
        }

        var bounces = GetComponentsInChildren<DeliverBounce> (true);
        for (int i = 0; i < bounces.Length; i++)
        {
            var bounce = bounces[i];
            bounce.onBounceTriggered.AddListener (() => onBounce?.Invoke ());
        }
        
        var holds = GetComponentsInChildren<ReceiveHold> (true);
        for (int i = 0; i < holds.Length; i++)
        {
            var hold = holds[i];
            hold.onStartHold.AddListener (StartHold);
            hold.onEndHold.AddListener (EndHold);
        }
        
        var stuns = GetComponentsInChildren<ReceiveStun> (true);
        for (int i = 0; i < stuns.Length; i++)
        {
            var stun = stuns[i];
            stun.onStunned.AddListener (OnStunStart);
            stun.onStunEnded.AddListener (OnStunCleared);
        }
        
        var deaths = GetComponentsInChildren<ReceiveKill> (true);
        for (int i = 0; i < deaths.Length; i++)
        {
            var death = deaths[i];
            death.onKilled?.AddListener ((x) => TriggerDeath ());
        }

		for (int i = 0; i < freezes.Length; i++)
		{
			var freeze = freezes[i];
			freeze.onFrozen += OnFrozen;
			freeze.onUnfrozen += OnUnfrozen;
		}

#if !DT_MOD
		var gameData = GameDataManager.Instance.Data;
		SETTING_freezeFrameActive = gameData.GetSetting (SettingIDBool.FreezeFrame);
		gameData.onSettingAdjustedBool += OnSettingAdjusted;
#endif
        onSpawned?.Invoke (this);
	}
	void OnDestroy ()
	{
		for (int i = 0; i < freezes.Length; i++)
		{
			var freeze = freezes[i];
			freeze.onFrozen -= OnFrozen;
			freeze.onUnfrozen -= OnUnfrozen;
		}
		for (int i = 0; i < spins.Count; i++)
		{
			spins[i].onPreSpin -= OnPreSpin;
		}

#if !DT_MOD
		if (GameDataManager.IsInitialized)
		{
			GameDataManager.Instance.Data.onSettingAdjustedBool -= OnSettingAdjusted;
		}
#endif
	}
	
	protected override void OnEnable ()
	{
		base.OnEnable ();
		ref var d = ref data;
		d.updatePhysics = true;

		isFrozen = false;
		for (int i = 0; i < freezes.Length; i++)
		{
			freezes[i].ResetFreeze ();
		}
	}
	protected override void OnDisable () 
	{
		base.OnDisable ();
		if (attacks != null)
		{
			for (int i = 0; i < attacks.Count; i++)
			{
				attacks[i].enabled = false;
			}
		}
		
		activeAttack = null;
		if (pathing)
			pathing.enabled = true;
		if (flight)
			flight.enabled = true;
	}

	void OnSettingAdjusted (SettingIDBool settingID, bool value)
	{
		switch (settingID)
		{
			case SettingIDBool.FreezeFrame:
				SETTING_freezeFrameActive = value;
				break;
		}
	}


	public override void StateUpdate ()
	{
		base.StateUpdate ();
		UpdateAttacks();
	}
	public override void VisualsUpdate () 
    {
        base.VisualsUpdate ();
        if (useSpriteSheetOverride) 
		{
			var length = Mathf.Min (spriteSheetOriginals.Length, spriteSheetOverrides.Length);
			for (int i = 0; i < length; i++)
			{	
				if (spriteSheetOriginals[i] == rend.sprite)
				{
					rend.sprite = spriteSheetOverrides[i];
					break;
				}
			}
        }
    }
	public override void PhysicsUpdate () 
    {
		base.PhysicsUpdate ();
		UpdateGrounded ();
		if (isStabilizing)
		{
			if (isGrounded && rigid.velocity.y < 0)
			{
				rigid.velocity += groundAngleDir * angleResistForce * getDeltaTime;
			}
		}
        currentSpeed = rigid.velocity.magnitude;
        currentTorque = rigid.angularVelocity.magnitude;
        
        UpdateDrag ();
		if (ShouldStabilize ())
			Stabilize ();
        
    }
	bool IResumePathingModifier.ShouldSkipCheck () 
	{
		return !gameObject.activeInHierarchy;
	}
    bool IResumePathingModifier.ShouldHaltPathing ()
    {
        if (isStunned)
            return true;
        if (isStabilizing)
            return true;
        if (isAttacking && (activeAttack.CancelPathingOnUse || !activeAttack.CanPath))
            return true;
        return false;
    }
    bool IResumePathingModifier.ShouldResumePathing ()
    {
        if (isStunned)
            return false;
        if (isStabilizing)
            return false;
        if (isAttacking && (activeAttack.CancelPathingOnUse || !activeAttack.CanPath))
            return false;
        return true;
    }
    
    void Destabilize () 
    {
        isStabilizing = true;
		angleResistTimer = angleResistDelay;
		SetPhysicsMaterial (isSlowed ? unstableMaterialSlowed : unstableMaterial);
        if (anim)
            anim.SetBool ("isStabilizing", true);
		rigid.maxAngularVelocity = maxSpinSpeed;
		rigid.constraints = rigidbodyConstraints;
		if (pathing)
		{
			pathing.isStopped = true;
			pathing.enableRotation = false;
			pathing.updateRotation = false;
		}
        onDestabilized?.Invoke ();
    }
    bool ShouldStabilize ()
    {
        var shouldStabilize = isStabilizing
            && (isGrounded || !stabilizeOnGround)
            && !knockbackActive
            && !spinActive
            && !ricochetActive
            && !isHeld
			&& !isFrozen
            && !isStunned;

        if (shouldStabilize)
        {
            restabilizeDelayTimer -= getDeltaTime;
            if (restabilizeDelayTimer > 0)
                shouldStabilize = false;
            else
                restabilizeDelayTimer = -1;

            
            if (restabilizeDelayTimer == -1)
            {
                restabilizeDelayTimer = restabilizeDelay;
                if (anim)
                    anim.SetBool ("isStabilizing", false);
            }
        }
        return shouldStabilize;
    }
    void Stabilize () 
    {
        isStabilizing = false;
		SetPhysicsMaterial (isSlowed ? stableMaterialSlowed : stableMaterial);
        rigid.velocity = Vector3.zero;
        rigid.angularVelocity = Vector3.zero;
		rigid.maxAngularVelocity = maxSpinSpeedStable;
		rigid.constraints |= RigidbodyConstraints.FreezeRotationY;
		heldVelocity = Vector3.zero;
        if (anim)
            anim.SetBool ("isStabilizing", false);
		if (optimize)
			rigid.isKinematic = true;
        onStabilized?.Invoke ();
    }
	public bool useSphereCheck = true;
    void UpdateGrounded () 
    {
		var nowGrounded = raycaster.hit;
		if (isStabilizing)
		{
			groundAngle = nowGrounded ? raycaster.angle : 0;
			groundAngleDir = nowGrounded ? raycaster.normal : Vector3.zero;
		}		
        if (nowGrounded != isGrounded)
        {
            if (nowGrounded)
                onGrounded?.Invoke ();
            else 
                onLeftGround?.Invoke ();
        }
        isGrounded = nowGrounded;
    }
    void UpdateDrag ()
    {
        var drag = startDrag;
        if (isGrounded)
        {
            if (isStunned)
                drag *= stunDragRatio;
            if (spinActive)
                drag *= spinDragRatio;
			if (isFrozen)
				drag *= freezeDragRatio;
        }
		drag *= getTimeScale;
        rigid.drag = drag;
    }

    void UpdateAttacks()
    {
		if (optimize) return;
        if (activeAttack == null)
        {   
            if (!senses.isAwareOfTarget) return;

			bool proceed = true;
            for (int i = 0; i < attackAvailabilityModifiers.Count; i++) 
            {
                if (!attackAvailabilityModifiers[i].CanAttack ())
                {
                    proceed = false;
                    break;
                }
            }
            if (proceed) 
            {
                for (int i = 0; i < attacks.Count; i++)
                {   
                    if (attacks[i].CanAttack())
                    {
                        onAttackStarted?.Invoke ();
                        activeAttack = attacks[i];
                        activeAttack.TriggerAttack();
                        break;
                    }
                }
            }
            
        }
        else
        {
            var endAttack = !activeAttack.enabled;
            if (endAttack)
            {
                onAttackStopped?.Invoke ();
                activeAttack = null;
            }
        }
    }
	void SetPhysicsMaterial (PhysicMaterial mat) 
	{
		bodyHurtbox.material = mat;
		for (int i = 0; i < additionalHurtboxes.Length; i++)
			additionalHurtboxes[i].material = mat;
	}
    public void IgnoreAttackColliders (List<Collider> attackColliders)
    {
        for (int i = 0; i < attackColliders.Count; i++)
        {
            Physics.IgnoreCollision (bodyHurtbox, attackColliders[i]);
			for (int j = 0; j < additionalHurtboxes.Length; j++)
				Physics.IgnoreCollision (additionalHurtboxes[j], attackColliders[i]);
        }
    } 

    
    
#endregion

#region PUBLIC METHODS  
    public void TriggerDeath () 
    {
        if (!isAlive) return;
        isAlive = false;
        onKilled?.Invoke (this);
    }
	void TriggerFreezeFrameShake ()
	{
		if (SETTING_freezeFrameActive)
			timeScale.TriggerIndividualSlowdown (hitFreezeFrameDuration);
		hitFreezeFrameRoot.DOComplete ();
		hitFreezeFrameRoot.DOShakePosition (hitFreezeFrameDuration, hitFreezeFrameMagnitude, vibrato: hitFreezeFrameVibrato);
	}
    public void ClearAttackTarget () => senses.ClearAttackTarget ();
    public void SetAttackTarget (IPlayerController player) => senses.SetAttackTarget (player);
    public void SetAttackTarget (Transform target) => senses.SetAttackTarget (target);
    public void SetBodyHurtbox (Collider hurtbox) 
    {
        bodyHurtbox?.gameObject?.SetActive (false);
        bodyHurtbox = hurtbox;
        bodyHurtbox?.gameObject?.SetActive (true);
    }
    public void SetTargetDiscovery (bool canSearch) => senses.SetTargetDiscovery (canSearch);
    public Transform GetTarget () => senses.Target;
    public bool IsFacingTarget () => senses.IsFacingTarget ();
    public bool IsFacingTarget (Vector3 targetPos) => senses.IsFacingTarget (targetPos);
    public bool IsFacingTarget (float angle) => senses.IsFacingTarget (angle);
    public bool IsFacingTarget (Vector3 targetPos, float angle) => senses.IsFacingTarget (targetPos, angle);
    public bool HasLineOfSight () => senses.hasLineOfSight;
    public Vector3 GetGroundPoint (float distance) => FzPhysics.Raycast (bodyHurtbox.bounds.center, Vector3.down, out RaycastHit info, distance + bodyHurtbox.bounds.size.y + skinWidth, groundLayer) 
            ? info.point 
            : Vector3.positiveInfinity;

    public Vector3 GetDirectionToTarget () => (senses.Target.position.With (y: transform.position.y) - transform.position).normalized;
    
    public bool IsApproachingHazard (float distance) => hazardSenses && hazardSenses.ApproachingHazard (distance);
    
    void FaceToPlayer () {
        if (!senses.Target)
            return;
        
        var target = senses.Target;
        transform.forward = (target.position.With (y: transform.position.y) - transform.position).normalized;
    }
    
    protected override bool shouldRotate => !isStabilizing;
    public override void RotateToTarget(Vector3 _target, float _turnSpeed, bool flattenY = true)
    {
        if (isStabilizing)
            return;            
        base.RotateToTarget (_target, _turnSpeed, flattenY);
    }
    public void StopRotation () 
    {
        rigid.angularVelocity = Vector3.zero;
    }
    public void StopMomentum () 
    {
        rigid.velocity = Vector3.zero;
    }
#endregion

#region EVENTS
    void OnKnockback (Vector3 force) 
    {
		rigid.velocity += heldVelocity;
		heldVelocity = Vector3.zero;
        senses.ForceDetectTarget ();
        Destabilize ();
		TriggerFreezeFrameShake ();
    }
	void OnPreSpin ()
	{
		if (pathing)
		{
			pathing.enableRotation = false;
			pathing.updateRotation = false;
		}
			
		Destabilize ();
	}
    void OnSpin (float force)
    {
        senses.ForceDetectTarget ();
        stunStars.TriggerFromSpin (maxSpinSpeed);
    }
    void OnSpinEnded () 
    {
    }
    void OnRicocheted ()
    {
        senses.ForceDetectTarget ();
        Destabilize ();
    }
	Vector3 heldVelocity;
    void StartHold () 
    {
		heldVelocity = isStabilizing ? heldVelocity + rigid.velocity : Vector3.zero;
        isHeld = true;
        Destabilize ();
		SetPhysicsMaterial (isSlowed ? stableMaterialSlowed : stableMaterial);
    }
    void EndHold () 
    {
        isHeld = false;
    }

    void OnStunStart (float duration) {
        if (anim)
            anim.SetBool ("isStunned", true);
        isStunned = true;
        stunStars.TriggerFromStun (duration);
        Destabilize ();
    }
    void OnStunCleared () {
        if (anim)
            anim.SetBool ("isStunned", false);
        isStunned = false;
    }     

	public void OnFrozen ()
	{
		isFrozen = true;
		Destabilize ();
	}
	public void OnUnfrozen ()
	{
		isFrozen = false;
	}
#endregion
    public bool TriggerConditionCheck (TriggerCondition param) => AIEnemy.TriggerConditionCheck (param, this, GetTarget ());
    static public bool TriggerConditionCheck (TriggerCondition param, AIEnemy owner, Transform target)
    {
        switch (param.type)
        {
            default:    return false;
            case TriggerConditionType.IsGrounded:
                return owner.isGrounded;
            case TriggerConditionType.NotGrounded:
                return !owner.isGrounded;
            case TriggerConditionType.WithinRange:
                return Vector3.SqrMagnitude (owner.transform.position - target.position.With (y: owner.transform.position.y)) < param.value * param.value;
            case TriggerConditionType.OutOfRange:
                return Vector3.SqrMagnitude (owner.transform.position - target.position.With (y: owner.transform.position.y)) > param.value * param.value;
            
            case TriggerConditionType.FacingTarget:
                return owner.IsFacingTarget (target.position, param.value);
            case TriggerConditionType.NotFacingTarget:
                return !owner.IsFacingTarget (target.position, param.value);
            case TriggerConditionType.TargetFacingOwner:
                return owner.senses?.IsTargetFacingSelf (target, param.value) ?? false;
            case TriggerConditionType.TargetNotFacingOwner:
                return !owner.senses?.IsTargetFacingSelf (target, param.value) ?? false;

            case TriggerConditionType.LOSToTarget:
                return owner.HasLineOfSight ();
            
            case TriggerConditionType.TargetFrozen:
                return owner.senses?.targetTraits.isFrozen ?? false;
            case TriggerConditionType.TargetUnfrozen:
                return !owner.senses?.targetTraits.isFrozen ?? false;
            
            case TriggerConditionType.TargetReadyingAttack:
                return owner.senses?.targetTraits.isPreppingAttack ?? false;
            case TriggerConditionType.TargetNotReadyingAttack:
                return !owner.senses?.targetTraits.isPreppingAttack ?? false;

            case TriggerConditionType.TargetAttacking:
                return owner.senses?.targetTraits.isAttacking ?? false;
            case TriggerConditionType.TargetNotAttacking:
                return !owner.senses?.targetTraits.isAttacking ?? false;

            case TriggerConditionType.TargetRecoveringFromAttack:
                return owner.senses?.targetTraits.isRecoveringFromAttack ?? false;
            case TriggerConditionType.TargetNotRecoveringFromAttack:
                return !owner.senses?.targetTraits.isRecoveringFromAttack ?? false;
        }
    }
    public bool CancelConditionCheck (CancelCondition param) => AIEnemy.CancelConditionCheck (param, this, GetTarget ());
    static public bool CancelConditionCheck (CancelCondition param, AIEnemy owner, Transform target)
    {
        switch (param.type)
        {
            default:    return false;

            case CancelConditionType.Destabilized:
                return owner.isStabilizing;
            case CancelConditionType.KnockedBack:
                return owner.knockbackActive;
            case CancelConditionType.Spinning:
                return owner.spinActive;
            case CancelConditionType.Stunned:
                return owner.isStunned;

            case CancelConditionType.ApproachingWall:
				if (owner.cachedCastWallLastCheck > Time.realtimeSinceStartup)
					return owner.cachedCastWall;
				
				owner.cachedCastWallLastCheck = Time.realtimeSinceStartup + castRate;
                owner.cachedCastWall = Physics.RaycastNonAlloc (owner.transform.position + owner.transform.InverseTransformDirection (owner.senses.getLineOfSightOffset), owner.transform.forward, owner.raycastHits, param.value, owner.GroundLayer) > 0;
                return owner.cachedCastWall;
            case CancelConditionType.ApproachingThreat:
				if (owner.hazardSenses == null) return false;
				if (owner.cachedCastThreatLastCheck > Time.realtimeSinceStartup)
					return owner.cachedCastThreat;

				owner.cachedCastThreatLastCheck = Time.realtimeSinceStartup + castRate;
				owner.cachedCastThreat = false;
                var hits = Physics.RaycastNonAlloc (owner.transform.position + owner.transform.InverseTransformDirection (owner.senses.getLineOfSightOffset), owner.transform.forward, owner.raycastHits, param.value, owner.hazardSenses.getThreatLayer, QueryTriggerInteraction.Collide);
                if (hits > 0)
                {
                    for (int i = 0; i < hits; i++)
                    {
                        var hit = owner.raycastHits[i];
						if (hit.collider.TryGetComponent<DeliverKill> (out var kill))
						{
							owner.cachedCastThreat = true;
							break;
						}
                    }
                }
                return owner.cachedCastThreat;
            case CancelConditionType.ApproachingLedge:
				if (owner.cachedCastFloorLastCheck > Time.realtimeSinceStartup)
					return owner.cachedCastFloor;
				
				owner.cachedCastFloorLastCheck = Time.realtimeSinceStartup + castRate;
				owner.cachedCastFloor = Physics.RaycastNonAlloc (owner.transform.position + (owner.transform.up * 1) + (owner.transform.forward * param.value), Vector3.down, owner.raycastHits, ledgeCheckDist, owner.GroundLayer) == 0;
                return owner.cachedCastFloor;

            case CancelConditionType.WithinRange:
                return Vector3.SqrMagnitude (owner.transform.position - target.position) < param.value;
            case CancelConditionType.OutOfRange:
                return Vector3.SqrMagnitude (owner.transform.position - target.position) > param.value;
            
            case CancelConditionType.HazardApproaching:
                return owner.hazardSenses && owner.hazardSenses.reacting;
        }
    }
	const float castRate = 0.2f;
	[NonSerialized] float cachedCastWallLastCheck;
	[NonSerialized] bool cachedCastWall;
	[NonSerialized] float cachedCastFloorLastCheck;
	[NonSerialized] bool cachedCastFloor;
	[NonSerialized] float cachedCastThreatLastCheck;
	[NonSerialized] bool cachedCastThreat;

	[ContextMenu ("Teleport Test")]
	public void Test_TeleportUp2 () => StartCoroutine (TeleportTest ());
	IEnumerator TeleportTest () 
	{
		transform.position += Vector3.up * 2;
		var startTime = Time.realtimeSinceStartup;
		yield return new WaitForFixedUpdate ();
		yield return new WaitUntil (() => isGrounded);
		Debug.Log ("Drop Time: " + Mathf.Abs (Time.realtimeSinceStartup - startTime));
	}

	public void SetOriginalSprites (IEnumerable <Sprite>  sprites)
	{
		spriteSheetOriginals = new Sprite[spriteSheetOverrides.Length];
		for (int i = 0; i < spriteSheetOverrides.Length; i++)
		{
			foreach (var sprite in sprites)
			{
				if (spriteSheetOverrides[i].name.Equals (sprite.name))
				{
					spriteSheetOriginals[i] = sprite;
				}
			}
		}
	}
}