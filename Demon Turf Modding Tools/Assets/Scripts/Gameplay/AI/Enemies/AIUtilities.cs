using UnityEngine;

public static class AIUtilities
{
    public static void RotateToTarget (Transform self, Vector3 _target, float _turnSpeed) {
        var targetFacing = (_target.With(y: self.position.y) - self.position).normalized;
        self.forward = Vector3.RotateTowards(self.forward, targetFacing, _turnSpeed * Time.fixedDeltaTime * Mathf.Deg2Rad, 0);
    }
}