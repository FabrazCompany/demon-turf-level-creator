using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Malee;
using RotaryHeart.Lib.SerializableDictionary;
#if UNITY_EDITOR
using UnityEditor;
#endif
public abstract partial class AIAttack<T> : MonoBehaviour, IAIAttack where T : Enum
{
    //template
    enum AttackPhase {
        Inactive = -1, 
        Startup = 0,
        Active = 1,
        Cooldown = 2,
        Finished = 3, 
    }

    [Header ("General")]
	[SerializeField] protected bool drawDebug;
    [SerializeField] bool canAttack = true;
    bool canPath = true;
    [SerializeField] int orderOverride = -1;
    [SerializeField, FormerlySerializedAs ("_onAttackStarted")] protected UnityEvent onAttackStarted;
    [SerializeField, FormerlySerializedAs ("_onAttackStarted")] protected UnityEventInt onAttackPhaseTriggered;
    [SerializeField, FormerlySerializedAs ("_onAttackEnded")] protected UnityEvent onAttackEnded;
    [SerializeField] LayerMask threatLayer;
    float availabilityTimer;
    
    protected AIEnemy owner;
    protected ModStub_AIPath pathing;
    protected AISense_TargetDetector enemySensor;
    protected AISense_HazardDetector hazardSensor;
    protected Transform target;
    protected Rigidbody targetRigid;
    protected int attackPhase;
    protected abstract bool attackFinished { get; }
    protected virtual int startPhase => 0;
    protected abstract BaseAttackData Data { get; }
    protected Vector3 getLeadingPosition =>  enemySensor ? enemySensor.leadingPosition : Vector3.zero;

    
    
    public bool CancelPathingOnUse => Data.cancelPathingOnUse && !attackPathingOverride;
    public int getOrderOverride => orderOverride;
    public Vector3 getLineOfSightOffset => enemySensor ? enemySensor.getLineOfSightOffset : Vector3.zero;

    public UnityEvent getOnAttackStarted => onAttackStarted;
    public UnityEvent getOnAttackEnded => onAttackEnded;

    public void SetCanAttack (bool active) => canAttack = active;
    public void SetCanPath (bool active) => canPath = active;
    public bool CanPath => canPath;

    bool attackPathingOverride;
    
    // RaycastHit[] raycastHits;
    protected virtual void Awake () 
    {
        owner = GetComponentInParent<AIEnemy> ();
        pathing = owner.GetComponent<ModStub_AIPath> ();
        attackPathingOverride = GetComponent<AIActive> ();
        enemySensor = owner.GetComponentInChildren<AISense_TargetDetector> (true);
        if (enemySensor)
            enemySensor.onTargetSet += SetTarget;

        hazardSensor = owner.GetComponentInChildren<AISense_HazardDetector> (true);
        enabled = false;

        attackColliders = new List<Collider> ();
        // raycastHits = new RaycastHit[collisionCheckCount];
    }
    void OnDestroy () 
    {
        if (enemySensor)
            enemySensor.onTargetSet -= SetTarget;
    }
    protected virtual void SetTarget (Transform _target)
    {
        target = _target;
        targetRigid = _target.GetComponentInParent<Rigidbody> ();
    }
    public bool CanAttack () 
    {
        if (!canAttack)
            return false;
        if (!owner.isAwareOfTarget)
            return false;
        if (availabilityTimer > Time.time)
            return false;
        if (owner.isStabilizing)
            return false;
        if (owner.isStunned)
            return false;

        foreach (var param in Data.triggerConditions)
            if (!owner.TriggerConditionCheck (param))
                return false;

        return true;
    }    protected const float ledgeCheckDist = 2f;
    public bool ShouldCancel () 
    {
        int ind = 0;
        foreach (var phase in Data.cancellablePhases)
        {
            if ((int)(object)phase == attackPhase)
            {
                // break;
                foreach (var param in Data.cancelConditions[ind].list)
                    if (owner.CancelConditionCheck (param))
                        return true;
            }
            ind++;
        }
        return false;
    }
    public virtual void TriggerAttack () 
    {
        enabled = true;
        attackPhase = startPhase;
        pathing.canMove = !Data.cancelPathingOnUse;
        onAttackStarted?.Invoke ();
        onAttackPhaseTriggered?.Invoke (attackPhase);
    }
    
    public virtual void CancelAttack () 
    {
        if (!enabled) return;
        enabled = false;
        availabilityTimer = Time.time + Data.repeatDelay;
        pathing.canMove = true;
        onAttackEnded?.Invoke ();
    }
    public void ResetAvailabilityTimer () => availabilityTimer = Time.time + Data.repeatDelay;
    void Update () {
        StateUpdate ();
        
        if (TransitionReady ())
            StateTransition ();
        
        if (attackFinished || ShouldCancel ())
            CancelAttack ();
    }
    protected virtual void StateTransition ()
    {
        attackPhase++;
        onAttackPhaseTriggered?.Invoke (attackPhase);
    }
    
    protected virtual void StateUpdate () {}
    protected abstract bool TransitionReady ();
    
    
    List<Collider> attackColliders;
    protected void IgnoreAttackColliders (Transform attackRoot)
    {
        attackRoot.GetComponentsInChildren<Collider> (true, attackColliders);
        if (attackColliders.Count > 0)
            owner.IgnoreAttackColliders (attackColliders);
        
        var particleProjectile = attackRoot.GetComponentInChildren<ParticleProjectiles> (true);
        var particleProjectileHitbox = owner.GetComponentInChildren<ParticleProjectileTarget> ();
        if (particleProjectile && particleProjectileHitbox)
        {
            particleProjectile.IgnoreCollider (particleProjectileHitbox.transform);
        }
    }
    protected virtual void DrawGizmos () {}
    protected void OnDrawGizmosSelected () 
    {
		if (!drawDebug)
            return;
        if (!canAttack)
            return;
            
        Gizmos.color = Color.red;
        foreach (var trigger in Data.triggerConditions)
        {
            TriggerCondition.DrawGizmos (this, trigger);
        }
        Gizmos.color = Color.magenta;
        foreach (var phase in Data.cancelConditions)
        {
            foreach (var cancel in phase.list)
            {
                CancelCondition.DrawGizmos (this, cancel);
            }
        }
        DrawGizmos ();
    }

    void OnValidate () 
    {
		if (Data == null) return;
        if (Data.cancellablePhases?.Length < Data.cancelConditions?.Length)
        {
            var list = Data.cancelConditions;
            var target = Data.cancellablePhases.Length;
            while (list.Count > target)
            {
                list.RemoveAt (list.Count - 1);
            }
        }
    }
    [Serializable]
    public class BaseAttackData
    {
        
        [Header ("Triggering")]
        public float repeatDelay = 0.5f;
        public bool cancelPathingOnUse = true;
        [Reorderable] public TriggerConditionList triggerConditions;

        [Header ("Cancelling")]
        public T[] cancellablePhases;
        [Reorderable] public CancelConditionPhaseList cancelConditions;
    }
}
public abstract class AITimedAttack<T> : AIAttack<T> where T : Enum
{
    protected float attackTimer;
    protected override bool TransitionReady () 
    {
        attackTimer -= owner.getDeltaTime;
        return attackTimer <= 0;
    }
}

public interface IAIAttack
{
    string name { get; }
    Transform transform { get; }
    bool enabled { get; set; }
    bool CanPath { get; }
    bool CancelPathingOnUse { get; }
    
    UnityEvent getOnAttackStarted { get; }
    UnityEvent getOnAttackEnded { get; }
    int getOrderOverride { get; }
    Vector3 getLineOfSightOffset { get; }
    bool CanAttack ();
    void TriggerAttack ();
}