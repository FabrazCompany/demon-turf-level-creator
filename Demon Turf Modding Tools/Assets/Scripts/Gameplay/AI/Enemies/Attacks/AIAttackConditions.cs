using System;
using UnityEngine;
using Malee;

#if UNITY_EDITOR
using UnityEditor;
[CustomPropertyDrawer (typeof (TriggerCondition)),
CustomPropertyDrawer (typeof (CancelCondition))]
class ConditionTypeDrawer : PropertyDrawer 
{
	const float lineHeight = 16f;
	const float verticalPadding = 2f;
	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		if (property.type == typeof (TriggerCondition).ToString ())
			return GetLineCountTrigger (property);
		else if (property.type == typeof (CancelCondition).ToString ())
			return GetLineCountCancel (property);
		else 
			return base.GetPropertyHeight (property, label);
	}
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		EditorGUI.BeginProperty(position, label, property);
		EditorGUI.BeginChangeCheck();

		var rect = position;
		rect.y += verticalPadding;
		rect.height = lineHeight;

		var type = property.FindPropertyRelative("type");
		EditorGUI.PropertyField(rect, type);

		if (property.type == typeof (TriggerCondition).ToString ())
			DrawTriggersTrigger (property, type, ref rect);
		else if (property.type == typeof (CancelCondition).ToString ())
			DrawTriggersCancel (property, type, ref rect);

		EditorGUI.EndChangeCheck();
		EditorGUI.EndProperty();
	}
	void DrawField (SerializedProperty property, string name, ref Rect rect)
	{
		EditorGUI.indentLevel++;
		rect.y += lineHeight + verticalPadding;
		var value = property.FindPropertyRelative("value");
		value.floatValue = EditorGUI.FloatField(rect, name, value.floatValue);
		EditorGUI.indentLevel--;
	}

	float GetLineCountTrigger (SerializedProperty property) 
	{
		var type = (TriggerConditionType)property.FindPropertyRelative ("type").intValue;
		switch (type)
		{
			//one line
			default:
				return lineHeight + (verticalPadding * 2);

			//two lines
			case TriggerConditionType.WithinRange:
			case TriggerConditionType.OutOfRange:
			case TriggerConditionType.FacingTarget:
			case TriggerConditionType.NotFacingTarget:
			case TriggerConditionType.TargetFacingOwner:
			case TriggerConditionType.TargetNotFacingOwner:
				return (lineHeight * 2) + (verticalPadding * 3);
		}
	}
	float GetLineCountCancel (SerializedProperty property) 
	{
		var type = (CancelConditionType)property.FindPropertyRelative ("type").intValue;
		switch (type)
		{
			//one line
			default:
				return lineHeight + (verticalPadding * 2);

			//two lines
			case CancelConditionType.ApproachingLedge:
			case CancelConditionType.ApproachingThreat:
			case CancelConditionType.ApproachingWall:
			case CancelConditionType.OutOfRange:
			case CancelConditionType.WithinRange:
				return (lineHeight * 2) + (verticalPadding * 3);
		}
	}


	void DrawTriggersTrigger (SerializedProperty property, SerializedProperty type, ref Rect rect)
	{
		switch ((TriggerConditionType)type.intValue)
		{
			case TriggerConditionType.WithinRange:
			case TriggerConditionType.OutOfRange:
				DrawField(property, "Distance", ref rect);
				break;
			case TriggerConditionType.FacingTarget:
			case TriggerConditionType.NotFacingTarget:
			case TriggerConditionType.TargetFacingOwner:
			case TriggerConditionType.TargetNotFacingOwner:
				DrawField(property, "Angle", ref rect);
				break;
		}
	}

	void DrawTriggersCancel (SerializedProperty property, SerializedProperty type, ref Rect rect)
	{
		switch ((CancelConditionType)type.intValue)
		{
			case CancelConditionType.ApproachingLedge:
			case CancelConditionType.ApproachingThreat:
			case CancelConditionType.ApproachingWall:
			case CancelConditionType.OutOfRange:
			case CancelConditionType.WithinRange:
				DrawField(property, "Distance", ref rect);
				break;
		}
	}	
}
#endif

[Serializable]
public enum TriggerConditionType
{
	IsGrounded = 40,
	NotGrounded = 41,
	WithinRange = 50,
	OutOfRange = 55,
	
	FacingTarget = 60,
	NotFacingTarget = 61,
	TargetFacingOwner = 63,
	TargetNotFacingOwner = 64,

	LOSToTarget = 65,

	TargetFrozen = 80,
	TargetUnfrozen = 85,

	TargetReadyingAttack = 150,
	TargetNotReadyingAttack = 151,
	TargetAttacking = 152,
	TargetNotAttacking = 153,
	TargetRecoveringFromAttack = 155,
	TargetNotRecoveringFromAttack = 156,
}
[Serializable]

public enum CancelConditionType
{
	Destabilized = 50,
	KnockedBack = 52,
	Spinning = 53,
	Stunned = 55,
	Hold = 57,
	
	ApproachingThreat = 100,
	ApproachingWall = 110,
	ApproachingLedge = 115,

	WithinRange = 120,
	OutOfRange = 125,

	HazardApproaching = 130,
}

[Serializable]
public class TriggerConditionList : ReorderableArray<TriggerCondition> {}

[Serializable]
public struct TriggerCondition
{
	public TriggerConditionType type;
	public float value;

	const float gizmoLineLength = 4;
	static public void DrawGizmos (IAIAttack atk, TriggerCondition trigger)
    {
        switch (trigger.type)
        {
			case TriggerConditionType.OutOfRange:
            case TriggerConditionType.WithinRange:
                Gizmos.DrawWireSphere (atk.transform.position, trigger.value);
                break;
			case TriggerConditionType.FacingTarget: 
			case TriggerConditionType.NotFacingTarget: 
				var eyePoint = atk.transform.position + atk.transform.TransformDirection (atk.getLineOfSightOffset);
				Gizmos.DrawLine (eyePoint, eyePoint + (Vector3.RotateTowards (atk.transform.forward, atk.transform.right, trigger.value * 0.5f * Mathf.Deg2Rad, 0) * gizmoLineLength));
				Gizmos.DrawLine (eyePoint, eyePoint + (Vector3.RotateTowards (atk.transform.forward, -atk.transform.right, trigger.value * 0.5f * Mathf.Deg2Rad, 0) * gizmoLineLength));
				break;

			case TriggerConditionType.TargetFacingOwner: 
			case TriggerConditionType.TargetNotFacingOwner: 
				var targetPos = atk.transform.position + (atk.transform.forward * gizmoLineLength);
				// var targetDir = -atk.transform.forward;
				Gizmos.DrawLine (targetPos, targetPos + (Vector3.RotateTowards (-atk.transform.forward, atk.transform.right, trigger.value * 0.5f * Mathf.Deg2Rad, 0) * gizmoLineLength));
				Gizmos.DrawLine (targetPos, targetPos + (Vector3.RotateTowards (-atk.transform.forward, -atk.transform.right, trigger.value * 0.5f * Mathf.Deg2Rad, 0) * gizmoLineLength));
				break;
        }
    }
}

[Serializable]
public class CancelConditionPhaseList : ReorderableArray<CancelConditionPhase> {}

[Serializable]
public class CancelConditionPhase
{
	[Reorderable] public CancelConditionList list; 
}
[Serializable]
public class CancelConditionList : ReorderableArray<CancelCondition> {}

[Serializable]
public struct CancelCondition
{
	public CancelConditionType type;
	public float value;

	static public void DrawGizmos (IAIAttack atk, CancelCondition cancel) 
    {
        switch (cancel.type) 
        {
            case CancelConditionType.WithinRange:
            case CancelConditionType.OutOfRange:
                Gizmos.DrawWireSphere (atk.transform.position, cancel.value);
                break;
            case CancelConditionType.ApproachingWall:
            case CancelConditionType.ApproachingThreat:
                Gizmos.DrawRay (atk.transform.position  + atk.transform.InverseTransformDirection (atk.getLineOfSightOffset), atk.transform.forward * cancel.value);
                break;
            case CancelConditionType.ApproachingLedge:
                Gizmos.DrawRay (atk.transform.position + (atk.transform.up * 1) + (atk.transform.forward * cancel.value), Vector3.down * AIEnemy.ledgeCheckDist);
                break;
        }
    }
}


[Serializable]
public class PhaseCancelCondition <T> where T : Enum
{
	public T phase;
    [Reorderable] public CancelConditionList conditions;
}
public struct PhaseCancelCondition
{
	public int phase;
    [Reorderable] public CancelConditionList conditions;
}
