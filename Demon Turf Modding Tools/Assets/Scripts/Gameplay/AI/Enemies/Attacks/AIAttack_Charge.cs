using System;
using UnityEngine;
using UnityEngine.Events;

public class AIAttack_Charge : AITimedAttack<AIAttack_Charge.AttackPhase>
{
    public enum AttackPhase 
    {
        Inactive = -1, 
        Startup = 0,
        Charge = 1,
        Cooldown = 2,
        Finished = 3, 
    }

    [Header ("Charge")]
    [SerializeField] ChargeData attackData;
    [SerializeField] Collider attackHitbox;

    Animator anim;
    Rigidbody rigid;
    Vector3 attackPos;
    DeliverKnockback knocker;
    float aimGracePeriod;
    protected override bool attackFinished => attackPhase >= (int)AttackPhase.Finished;
    protected override BaseAttackData Data => attackData;
    protected override void Awake () 
    {
        base.Awake ();
        anim = owner.GetComponentInChildren <Animator> (true);
        rigid = owner.GetComponent<Rigidbody> ();
        attackTimer = 0;
        attackPhase = (int)AttackPhase.Inactive;

        if (attackHitbox)
            knocker = attackHitbox.GetComponent<DeliverKnockback> ();
        if (attackData.cancelOnHit && knocker)
            knocker.onHit.AddListener (OnAttackLanded);
    }
    public override void TriggerAttack () 
    {
        attackPhase = (int)AttackPhase.Startup;
        attackTimer = attackData.startup;
        aimGracePeriod = attackData.startupAimBuffer;
        anim?.SetInteger ("attackPhase", (int)attackPhase);
        base.TriggerAttack (); 
    }
    public override void CancelAttack () 
    {    
        if (attackHitbox)
            attackHitbox.enabled = false;
        attackPhase = (int)AttackPhase.Inactive;
        attackTimer = 0;
        anim?.SetInteger ("attackPhase", (int)attackPhase);
        base.CancelAttack ();
    }
    protected override bool TransitionReady()
    {
        var isReady = base.TransitionReady ();
        if (attackPhase == (int)AttackPhase.Startup && isReady)
        {
            aimGracePeriod -= owner.getDeltaTime;
            if (aimGracePeriod > 0 && !owner.IsFacingTarget (5))
                isReady = false;
        }
        return isReady;
    }
    float currentForce;
    protected override void StateTransition () {
        base.StateTransition ();
        switch ((AttackPhase)attackPhase)
        {
            case AttackPhase.Charge: // attack now active, setup hurtboxes
                if (attackHitbox)
                    attackHitbox.enabled = true;
                if (knocker)
                    knocker.OverrideForce (attackData.knockback);
                attackTimer = attackData.duration;
                attackPos = target.position;
                if (attackData.burstMomentum)
                {
                    var burstForce = (transform.forward * attackData.chargeForce) + (transform.up * attackData.chargeForceVertical);
                    rigid.AddForce (burstForce * owner.getTimeScale, ForceMode.VelocityChange);
                }
                break;
            case AttackPhase.Cooldown: // attack no longer active, cooldown
                if (attackData.burstMomentum && attackHitbox)
                    attackHitbox.enabled = false;
                attackTimer = attackData.cooldown;
                currentForce = attackData.chargeForce;
                break;
        }
    }
    
    protected void FixedUpdate () 
    {
        switch ((AttackPhase)attackPhase) {
            case AttackPhase.Startup:
                owner.RotateToTarget (target.position + getLeadingPosition, attackData.turnSpeed);
                break;
            case AttackPhase.Charge:
                if (!attackData.burstMomentum)
                    rigid.AddForce (transform.forward * attackData.chargeForce * owner.getTimeScale, ForceMode.Acceleration);
                break;
            case AttackPhase.Cooldown:
                if (!attackData.burstMomentum)
                {
					if (attackData.brakeForceAsDecay)
					{
						var drag = (1f / (1f + (attackData.brakeForce * owner.getDeltaTime)));
						currentForce *= drag;
						rigid.AddForce (transform.forward * currentForce * owner.getTimeScale, ForceMode.Acceleration);
					}
					else 
					{
						rigid.velocity = Vector3.MoveTowards (rigid.velocity, Vector3.zero, attackData.brakeForce * owner.getDeltaTime);
					}
                    
                    
                    if (currentForce <= attackData.minKnockbackSpeed)
                    {
                        // knocker
                        if (attackHitbox && attackHitbox.enabled)
                            attackHitbox.enabled = false;
                    }
                    else 
                    {
                        if (knocker) 
                        {
                            knocker.OverrideForce (attackData.knockback);
                        }
                        
                    }
                }
                break;
        }
    }

    void OnAttackLanded (Collider hit) 
    {    
        attackPhase = (int)AttackPhase.Cooldown;    //jump to cooldown phase
        if (attackHitbox)
            attackHitbox.enabled = false;
        attackTimer = attackData.cooldown;
        anim?.SetInteger ("attackPhase", (int)attackPhase);
        rigid.velocity = Vector3.zero;
        rigid.AddForce ((-transform.forward + (transform.up * 0.8f)) * attackData.bouncebackForce * owner.getTimeScale, ForceMode.VelocityChange);
    }
    

    [System.Serializable]
    class ChargeData : BaseAttackData 
    {
        public bool cancelOnHit = true; 
        public bool burstMomentum = false;
        [Header ("Force")]
        public float knockback = 30;
        // [Conditional ("burstMomentum", hide: true)]
        // public float minKnockback = 30;
        [Conditional ("burstMomentum", hide: true)]
        public float minKnockbackSpeed = 0.1f;

        [Header ("Timing")]
        public float startup = 1.5f;
        public float startupAimBuffer = .5f;
        public float duration = 0.8f;
        public float cooldown = 1;

        [Header ("Speeds")]
        public float turnSpeed = 110;
        public float chargeForce = 1000;
        public float chargeForceVertical = 0;
		public bool brakeForceAsDecay = true;
        public float brakeForce = 5;
        public float bouncebackForce = 10;

    }
}

