using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
using System;
using System.Reflection;
using UnityEditor;
using UnityEditor.Events;
[CustomEditor (typeof (AIAttack_GenericAttack))]
public class AIAttack_GenericAttackEditor : Editor 
{
    public override void OnInspectorGUI() {
        base.OnInspectorGUI ();
        if (GUILayout.Button ("Add Basic Phase"))
        {
            (serializedObject.targetObject as AIAttack_GenericAttack).AddAttackPhase ();
        }

        if (GUILayout.Button ("Add Timed Phase"))
        {
            (serializedObject.targetObject as AIAttack_GenericAttack).AddAttackPhaseTimed ();
        }

        if (GUILayout.Button ("Add Transitioned Phase"))
        {
            (serializedObject.targetObject as AIAttack_GenericAttack).AddAttackPhaseTransitioned ();
        }
    }
}

#endif

public class AIAttack_GenericAttack : AIAttack<AIAttack_GenericAttack.GenericPhase>
{

    #if UNITY_EDITOR
    public GenericAttackPhase AddAttackPhase () 
    {
        var newPhase = new GameObject ("Phase " + (transform.childCount + 1));
        newPhase.transform.SetParent (transform, false);
        return newPhase.AddComponent<GenericAttackPhase> ();
    }
    public void AddAttackPhaseTimed () 
    {
        var core = AddAttackPhase ();
        core.InitEvents ();
        var timer = core.gameObject.AddComponent<Timer> ();
        timer.InitEvents ();
        timer.enabled = false;
        UnityAction<bool> action = (UnityAction<bool>)UnityAction.CreateDelegate (typeof (UnityAction<bool>), timer, "set_enabled");
        UnityEventTools.AddBoolPersistentListener (core.getOnTriggered, action, true);
        UnityEventTools.AddBoolPersistentListener (core.getOnEnded, action, false);
        UnityEventTools.AddVoidPersistentListener (timer.getOnTimerFinished, core.Advance);
    }

    public void AddAttackPhaseTransitioned () 
    {
        var core = AddAttackPhase ();
        var transitions = core.gameObject.AddComponent<GenericAttackTransition> ();
    }
    #endif

    [SerializeField] GenericData data;
    [SerializeField] GenericAttackPhase[] attackPhases;
    
    protected override bool attackFinished => isFinished;
    bool isFinished = false;
    protected override BaseAttackData Data => data;
    protected override bool TransitionReady() => attackPhases[attackPhase].isDone;
    public override void TriggerAttack()
    {
        base.TriggerAttack ();
        isFinished = false;
        attackPhase = 0;
        attackPhases[attackPhase].Trigger ();
    }
    public override void CancelAttack()
    {
        base.CancelAttack ();
        foreach (var attack in attackPhases)
        {
            if (!attack.isDone)
                attack.Cleanup ();
        }
    }
    protected override void StateTransition () 
    {
        attackPhases[attackPhase].Cleanup ();
        base.StateTransition ();
        // Debug.Log ("Phase: " + attackPhase);
        if (attackPhase >= attackPhases.Length) {
            attackPhase = attackPhases.Length - 1;
            isFinished = true;
        }
        if (!attackFinished)
            attackPhases[attackPhase].Trigger ();
    }
    public void SetAttackPhase (int value) 
    {
        attackPhases[attackPhase].Cleanup ();
        attackPhase = value - 1;
        base.StateTransition ();
        // Debug.Log ("Phase: " + attackPhase);
        if (attackPhase >= attackPhases.Length) {
            attackPhase = attackPhases.Length - 1;
            isFinished = true;
        }
        if (!attackFinished)
            attackPhases[attackPhase].Trigger ();

    }

    [System.Serializable]
    public class GenericData : BaseAttackData {
        public bool handleRotation;
    }
    public enum GenericPhase
    {
        Phase0 = 0,
        Phase1 = 1,
        Phase2 = 2,
        Phase3 = 3,
        Phase4 = 4,
        Phase5 = 5,
        Phase6 = 6,
        Phase7 = 7,
        Phase8 = 8,
        Phase9 = 9,
        Phase10 = 10,
        Phase11 = 11,
        Phase12 = 12,
        Phase13 = 13,
        Phase14 = 14,
        Phase15 = 15,
        Phase16 = 16,
        Phase17 = 17,
        Phase18 = 18,
        Phase19 = 19,
        Phase20 = 20,
    }
}

