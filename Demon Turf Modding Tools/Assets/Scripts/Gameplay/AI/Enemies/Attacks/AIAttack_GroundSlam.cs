using System;
using UnityEngine;
using UnityEngine.Events;

public class AIAttack_GroundSlam : AIAttack<AIAttack_GroundSlam.AttackPhase>
{
    public enum AttackPhase 
    {
        Inactive = -1, 
        Startup = 0,
        Rise = 1,
        Dive = 2,
        Cooldown = 3,
        Finished = 4, 
    }
    [Header ("Body Slam")]
	[SerializeField] bool updateAnimatorState = true;
    [SerializeField] GroundSlamData attackData;
    [SerializeField] Collider attackHitbox;
    [SerializeField] GameObject impactPrefab;
    [SerializeField] Vector3 impactOffset = new Vector3 (0, 0.5f, 0);
    [SerializeField] UnityEvent onImpact;
	
    Animator anim;
    Rigidbody rigid;
	HasGravity gravity;
    float attackTimer;
    bool interrupted;
    // bool contactedGround;

    protected override bool attackFinished => attackPhase >= (int)AttackPhase.Finished;
    protected override BaseAttackData Data => attackData;
    protected override void Awake () {
        base.Awake ();
        anim = owner.GetComponentInChildren<Animator> (true);
        rigid = owner.GetComponent<Rigidbody> ();
		gravity = owner.GetComponent<HasGravity> ();
        attackTimer = 0;
        attackPhase = (int)AttackPhase.Inactive;
        
    }
    public override void TriggerAttack () 
    {   
        attackPhase = (int)AttackPhase.Startup; 
        attackTimer = attackData.startup;
        interrupted = false;
		if (updateAnimatorState)
        	anim?.SetInteger ("attackPhase", (int)attackPhase);
        base.TriggerAttack ();
    }
    public override void CancelAttack () 
    {
        if (attackHitbox)
            attackHitbox.enabled = false;
        attackPhase = (int)AttackPhase.Inactive;
		if (updateAnimatorState)
        	anim?.SetInteger ("attackPhase", (int)attackPhase);
        base.CancelAttack ();
    }

    protected override bool TransitionReady () 
    {
        switch ((AttackPhase)attackPhase)
        {
            case AttackPhase.Startup:
            case AttackPhase.Cooldown:
                attackTimer -= owner.getDeltaTime;
                return attackTimer <= 0;
            case AttackPhase.Rise:
                return rigid.velocity.y < 0;
            case AttackPhase.Dive:
                return owner.isGrounded || interrupted;// || contactedGround;
        }
        return false;
    }   
    protected override void StateTransition () 
    {
        base.StateTransition ();
		if (updateAnimatorState)
        	anim?.SetInteger ("attackPhase", (int)attackPhase);
        switch ((AttackPhase)attackPhase)
        {
            case AttackPhase.Rise:
                // rigid.useGravity = true;
				gravity.enabled = true;
                rigid.AddForce (Vector3.up * attackData.leapForce, ForceMode.VelocityChange);
                break;
            case AttackPhase.Dive:
                if (attackHitbox)
                    attackHitbox.enabled = true;
                break;
            case AttackPhase.Cooldown:
                onImpact?.Invoke ();
                if (!interrupted)
                {
                    var attack = TrashMan.spawn (impactPrefab, transform.position + impactOffset, Quaternion.identity);
                    
                    var targeted = attack.GetComponent<ITargeted> ();
                    targeted?.AssignTarget (target);

                    IgnoreAttackColliders (attack.transform);
                }
                
                if (attackHitbox)
                    attackHitbox.enabled = false;
                attackTimer = attackData.cooldown;
                if (attackData.bounceBackForce > 0)
                {
                    rigid.velocity = rigid.velocity.With (y: 0);
                    rigid.AddForce (Vector3.up * attackData.bounceBackForce, ForceMode.VelocityChange);
                }
                    
                break;
        }
    }
    protected void FixedUpdate()
    {
        switch ((AttackPhase)attackPhase) 
        {
            case AttackPhase.Startup:
                break;
            case AttackPhase.Dive:
                rigid.AddForce (Vector3.up * attackData.descentForce, ForceMode.Acceleration);
                break;
        }
    }
    public void OnDiveInterrupted ()
    {
        interrupted = true;
    }


    [System.Serializable]
    class GroundSlamData : BaseAttackData {
        [Header ("Timing")]
        public float startup;
        public float cooldown;

        [Header ("Physics")]
        public float leapForce;
        public float descentForce;
        public float bounceBackForce;
    }
}

