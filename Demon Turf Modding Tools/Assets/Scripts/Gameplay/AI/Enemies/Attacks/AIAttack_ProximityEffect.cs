﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Malee;

public class AIAttack_ProximityEffect : AIAttack<AIAttack_ProximityEffect.AttackPhase>
{
    public enum AttackPhase 
    {
        Inactve = -1,
        Startup = 0,
        Active = 1,
        Cooldown = 2,
        Finished = 3,
    }
    [SerializeField] ProximityAttackData attackData;

    protected override bool attackFinished => attackPhase >= (int)AttackPhase.Finished;
    protected override BaseAttackData Data => attackData;
    float attackTimer;

    public override void TriggerAttack () {
        base.TriggerAttack ();
        attackTimer = Time.time + attackData.startup;
        attackPhase = (int)AttackPhase.Startup;
    }

    protected override bool TransitionReady () 
    {
        switch ((AttackPhase)attackPhase)
        {
            case AttackPhase.Startup:
            case AttackPhase.Cooldown:
                return attackTimer <= Time.time;

            case AttackPhase.Active:
                // bool skhouldAdvance = false;
                foreach (var param in attackData.advanceConditions)
                    if (owner.CancelConditionCheck (param))
                        return true;
                break;
        }
        return false;
    }


    protected override void DrawGizmos()
    {
		if (!drawDebug)
            return;
        Gizmos.color = Color.red;
        foreach (var advance in attackData.advanceConditions)
        {
            CancelCondition.DrawGizmos (this, advance);
        }
    }
    
    [System.Serializable]
    class ProximityAttackData : BaseAttackData
    {
        public float startup = 0.5f;
        public float cooldown = 0.5f;
        [Reorderable] public CancelConditionList advanceConditions;
    }


}
