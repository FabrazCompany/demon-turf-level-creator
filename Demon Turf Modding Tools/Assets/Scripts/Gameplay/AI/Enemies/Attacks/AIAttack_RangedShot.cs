using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AIAttack_RangedShot : AITimedAttack<AIAttack_RangedShot.AttackPhase>
{
    public enum AttackPhase 
    {
        Inactive = -1, 
        Aim = 0,
        FireAndCooldown = 1,
        Finished = 2, 
    }
    [Header ("Ranged Shot")]
    [SerializeField] RangedShotData attackData;
    [SerializeField] ProjectileSpawner projectileEmitter;
    [SerializeField] Transform aimRoot;
    Vector3 originalAimRot;
    

    //lobbed only
    ProjectileSpawnerMod_Lobbed projectileMod_lobbed;
    Vector3 lobbedShotDir = Vector3.forward;
    
    //burst only
    int burstCount;
    
    
    protected override bool attackFinished => attackPhase >= (int)AttackPhase.Finished;
    protected override BaseAttackData Data => attackData;
    Vector3 targetPosition => target.position + Vector3.up * attackData.verticalAimPadding;
    bool cachedTurnSetting;

    protected override void Awake () 
    {
        base.Awake ();
        attackTimer = 0;
        attackPhase = (int)AttackPhase.Inactive;
        aimRoot = aimRoot ? aimRoot : projectileEmitter.transform;
        originalAimRot = aimRoot.localEulerAngles;
        projectileMod_lobbed = projectileEmitter.GetComponent<ProjectileSpawnerMod_Lobbed> ();
    }
    public override void TriggerAttack () 
    {
        base.TriggerAttack ();
        cachedTurnSetting = pathing.enableRotation;
        pathing.enableRotation = !attackData.useAiming;
        attackTimer = projectileEmitter.getStartupDuration;
        attackPhase = (int)AttackPhase.Aim;
        burstCount = 1;
    }
    public override void CancelAttack () {
        if (!enabled)
            return;
        base.CancelAttack ();
        attackTimer = 0;
        attackPhase = (int)AttackPhase.Inactive;
        pathing.enableRotation = cachedTurnSetting;
    }
    protected override void StateUpdate () {
        switch ((AttackPhase)attackPhase)
        {
            case AttackPhase.Inactive:
                VerticallyRotateToNeutral ();
                break;
            case AttackPhase.Aim:
                VerticallyRotateToTarget ();
                break; 
        }
    }
    protected void FixedUpdate () 
    {
        if (attackData.useAiming)
        {
            if (attackPhase == (int)AttackPhase.Aim && attackTimer > attackData.preFireBuffer)
            {
                owner.RotateToTarget (target.position + GetAimLeading(), attackData.aimSpeed);
            }
            else 
            {
                owner.RotateToTarget (owner.transform.forward, 0);
            }
        }
        
    }
    protected override bool TransitionReady () {
        switch ((AttackPhase)attackPhase)
        {
            case AttackPhase.Aim:
                bool shotReady = base.TransitionReady (); // shot timer
                if (!attackData.burstFire || burstCount <= 1)   //ignore additional fire criteria if mid burstfire
                {
                    if (projectileMod_lobbed && attackData.verticalAimSpeed > 0)
                    {
                        var shotEvaluation = FzMath.PredictiveAim (
                            projectileEmitter.transform.position, 
                            projectileEmitter.getBulletSpeed, 
                            targetPosition, 
                            enemySensor.averagedVeloctiy, 
                            projectileMod_lobbed.getGravity, 
                            out lobbedShotDir
                        );
                        shotReady &= shotEvaluation;
                    }
                    else 
                    {
                        lobbedShotDir = projectileEmitter.transform.forward * projectileEmitter.getBulletSpeed;
                    }

                    if (attackData.holdForTarget)
                    {
                        shotReady &= owner.IsFacingTarget (targetPosition + getLeadingPosition, attackData.shotAngle);
                    }
                }
                return shotReady;
        }
        return base.TransitionReady ();
    }

    Vector3 GetAimLeading ()
    {
        var val = getLeadingPosition;
        var shotDir = Vector3.zero;
        if (attackData.verticalAimSpeed > 0) 
        {
            var shotEvaluation = FzMath.PredictiveAim (
                projectileEmitter.transform.position, 
                projectileEmitter.getBulletSpeed, 
                targetPosition, 
                enemySensor.averagedVeloctiy, 
                0, 
                out shotDir
            );
            if (shotEvaluation)
                val = shotDir;
        }
        else 
        {
            val = lobbedShotDir;
        }
        
        return val;
    }
    protected override void StateTransition () {
        // attackPhase++;
        base.StateTransition ();
        switch ((AttackPhase)attackPhase)
        {
            case AttackPhase.FireAndCooldown: //attack no longer active, cooldown
                if (projectileMod_lobbed && attackData.useAiming && attackData.verticalAimSpeed > 0)
                {
                    var yAngle = Vector3.Angle (lobbedShotDir.normalized, lobbedShotDir.With (y: 0).normalized);
                    var dir = Quaternion.AngleAxis(-yAngle, transform.right) * transform.forward;
                    projectileMod_lobbed.SetLobForce (dir * lobbedShotDir.magnitude);
                    // projectileMod_lobbed.SetLobForce (lobbedShotDir);
                }
                
                projectileEmitter.FireProjectiles ();
                if (attackData.fireKnockback > 0)
                {
                    owner.getRigid.AddForce (-owner.transform.forward * attackData.fireKnockback, ForceMode.VelocityChange);
                }
                if (attackData.burstFire && burstCount < attackData.burstAmount)
                {
                    attackPhase--;
                    burstCount++;
                    attackTimer = attackData.burstRate;
                }
                else
                {
                    attackTimer = attackData.cooldown;
                    if (attackData.burstFire)
                        attackData.onBurstFireFinished?.Invoke ();                    
                }
                break;
        }
    }

    void VerticallyRotateToTarget ()
    {
        if (!attackData.useAiming)
            return;
        if (projectileMod_lobbed)
            return;
        var targetRot = Quaternion.LookRotation ((targetPosition - aimRoot.position).normalized, Vector3.up);
        var finalRot = Quaternion.RotateTowards (aimRoot.rotation, targetRot, attackData.verticalAimSpeed * owner.getDeltaTime).eulerAngles;
        finalRot.x = FzMath.ClampAngle (finalRot.x, originalAimRot.x + attackData.minVerticalAngle, originalAimRot.x + attackData.maxVerticalAngle);
        aimRoot.eulerAngles = finalRot;
        aimRoot.localEulerAngles = aimRoot.localEulerAngles.With (y:0, z:0);        
    }
    void VerticallyRotateToNeutral () 
    {
        if (!attackData.useAiming)
            return;
        if (projectileMod_lobbed)
            return;
        var finalRot = Quaternion.RotateTowards (aimRoot.rotation, Quaternion.identity, attackData.verticalAimSpeed * owner.getDeltaTime).eulerAngles;
        finalRot.x = FzMath.ClampAngle (finalRot.x, originalAimRot.x + attackData.minVerticalAngle, originalAimRot.x + attackData.maxVerticalAngle);
        aimRoot.eulerAngles = finalRot;
        aimRoot.localEulerAngles = aimRoot.localEulerAngles.With (y:0, z:0);
    }


    protected override void DrawGizmos () 
    {
		if (!drawDebug)
            return;
        if (!projectileEmitter)
            return;
        if (attackData.maxVerticalAngle != 0 || attackData.minVerticalAngle != 0)
        {
            Gizmos.DrawRay (projectileEmitter.transform.position, Quaternion.AngleAxis (attackData.maxVerticalAngle, Vector3.right) * transform.forward * 5);
            Gizmos.DrawRay (projectileEmitter.transform.position, Quaternion.AngleAxis (attackData.minVerticalAngle, Vector3.right) * transform.forward * 5);
        }
    }

    [System.Serializable]
    class RangedShotData : BaseAttackData 
    {
        [Header ("Aiming")]
        public bool useAiming = true;
        [Conditional ("useAiming")] public float aimSpeed = 150;
        [Conditional ("useAiming")] public float shotAngle = 5;
        [Conditional ("useAiming")] public float verticalAimSpeed = 140;
        [Conditional ("useAiming")] public float verticalAimPadding = 0.3f;
        [Conditional ("useAiming"), Range (0, 90)] public float maxVerticalAngle = 27.95f;
        [Conditional ("useAiming"), Range (-90, 0)] public float minVerticalAngle = -40;
        [Conditional ("useAiming")] public bool holdForTarget;

        [Header ("Timing")]
        public float preFireBuffer = 0.1f;
        public float cooldown = 0.5f;
        public float fireKnockback = 0;

        [Header ("Burst")]
        public bool burstFire;
        [Conditional ("burstFire")] public int burstAmount;
        [Conditional ("burstFire")] public float burstRate;
        // [Conditional ("burstFire")] 
        public UnityEvent onBurstFireFinished;
    }
}

