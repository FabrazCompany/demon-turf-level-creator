using UnityEngine;

public class GenericAttackHalt : MonoBehaviour
{
    [SerializeField] float friction = 5;
    // [SerializeField] Vector3 force;

    AIEnemy owner;
    GenericAttackPhase phase;

    void Awake () 
    {
        owner = GetComponentInParent<AIEnemy> ();
        phase = GetComponent<GenericAttackPhase> ();
        phase.getOnTriggered.AddListener (Enable);
        phase.getOnEnded.AddListener (Disable);
        enabled = false;
    }
    void OnDestroy () 
    {
        phase.getOnTriggered.RemoveListener (Enable);
        phase.getOnEnded.RemoveListener (Disable);
    }
    void Enable () => enabled = true;
    void Disable () => enabled = false; 

    void FixedUpdate () 
    {
		owner.getRigid.velocity = Vector3.MoveTowards (owner.getRigid.velocity, Vector3.zero, friction * owner.getDeltaTime);
        // owner.getRigid.AddForce (, ForceMode.Acceleration);
    }
}