using UnityEngine;

public class GenericAttackMovement : MonoBehaviour
{
    [SerializeField] Vector3 burst;
    [SerializeField] Vector3 force;

    AIEnemy owner;
    GenericAttackPhase phase;

    void Awake () 
    {
        owner = GetComponentInParent<AIEnemy> ();
        phase = GetComponent<GenericAttackPhase> ();
        phase.getOnTriggered.AddListener (Enable);
        phase.getOnEnded.AddListener (Disable);
        enabled = false;
    }
    void OnDestroy () 
    {
        phase.getOnTriggered.RemoveListener (Enable);
        phase.getOnEnded.RemoveListener (Disable);
    }
    void Enable () => enabled = true;
    void Disable () => enabled = false; 

    void OnEnable () 
    {
        owner.getRigid.AddForce (owner.transform.TransformDirection (burst), ForceMode.VelocityChange);
    }

    void FixedUpdate () 
    {
        owner.getRigid.AddForce (owner.transform.TransformDirection (force) * owner.getDeltaTime, ForceMode.Acceleration);
    }
}