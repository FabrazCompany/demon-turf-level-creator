using UnityEngine;
using UnityEngine.Events;

public class GenericAttackPhase : MonoBehaviour
{   
    [SerializeField] UnityEvent onTriggered;
    [SerializeField] UnityEvent onEnded;

    bool active;

    public virtual bool isDone { get; protected set; }
    public UnityEvent getOnTriggered => onTriggered;
    public UnityEvent getOnEnded => onEnded;

#if UNITY_EDITOR
    public void InitEvents () 
    {
        if (onTriggered == null)
            onTriggered = new UnityEvent ();
        if (onEnded == null)
            onEnded = new UnityEvent ();
    }
#endif

    public virtual void Trigger () 
    {
        isDone = false;
        active = true;
        onTriggered?.Invoke ();
    }
    public virtual void Cleanup () 
    {
        isDone = false;
        active = false;
        onEnded?.Invoke ();
    }
    public virtual void Advance () => isDone = true;
}