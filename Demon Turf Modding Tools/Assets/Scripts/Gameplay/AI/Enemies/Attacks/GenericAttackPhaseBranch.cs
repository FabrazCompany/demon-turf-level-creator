using UnityEngine;
using UnityEngine.Events;

public class GenericAttackPhaseBranch : GenericAttackPhase
{
    [SerializeField] UnityEvent onBranchSelected;
    [SerializeField] GenericAttackPhase successPhase;
    [SerializeField] GenericAttackPhase fallbackPhase;
    GenericAttackPhase targetedPhase;

    public override bool isDone => targetedPhase != null && targetedPhase.isDone; 
    public override void Cleanup() 
    {
        base.Cleanup ();
        if (targetedPhase)
            targetedPhase.Cleanup ();
    }
    public void Advance (bool success) 
    {
        onBranchSelected?.Invoke ();
        targetedPhase = success ? successPhase : fallbackPhase;
        targetedPhase.Trigger ();
    }
    public override void Advance () 
    {
        Debug.LogWarning ("Using base call of Advance in branched phase, verify if correct", this);
        this.Advance ();
    }
}