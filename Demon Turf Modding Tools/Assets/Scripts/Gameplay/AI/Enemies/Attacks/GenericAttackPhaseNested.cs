using UnityEngine;

public class GenericAttackPhaseNested : GenericAttackPhase
{
    [SerializeField] GenericAttackPhase[] subPhases;
    int phase;
    void Awake () 
    {
        enabled = false;
    }
    public override void Trigger ()
    {
        base.Trigger ();
        phase = 0;
        subPhases[phase].Trigger ();
        enabled = true;
    }
    public override void Cleanup () 
    {
        base.Cleanup();
        enabled = false;
    }
    public override void Advance () {}
    void Update () 
    {
        if (phase >= subPhases.Length || !subPhases[phase].isDone)
            return;
        
        subPhases[phase].Cleanup ();
        phase++;
        if (phase >= subPhases.Length)
        {
            isDone = true;
        }
        else 
        {
            subPhases[phase].Trigger ();
        }
    }
}