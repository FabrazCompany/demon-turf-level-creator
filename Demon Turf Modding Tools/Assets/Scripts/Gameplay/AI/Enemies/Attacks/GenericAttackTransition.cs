using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Malee;

[RequireComponent (typeof (GenericAttackPhase))]
public class GenericAttackTransition : MonoBehaviour
{
    [SerializeField] float checkRate = .5f;
    [Reorderable] public TriggerConditionList triggerTransitions;
    [Reorderable] public CancelConditionList cancelTransitions;
    [Space]
    [SerializeField] bool timed;
    [SerializeField, Conditional ("timed")] float waitDuration;
    [SerializeField, Conditional ("timed")] float waitVariance;

    AIEnemy owner;
    AIAttack_GenericAttack attack;
    GenericAttackPhase phase;
    
    void Awake () 
    {
        owner = GetComponentInParent<AIEnemy> ();
        attack = GetComponentInParent<AIAttack_GenericAttack> ();
        phase = GetComponent<GenericAttackPhase> ();
        phase.getOnTriggered.AddListener (Enable);
        phase.getOnEnded.AddListener (Disable);
        enabled = false;
    }
    void OnDestroy () 
    {
        phase.getOnTriggered.RemoveListener (Enable);
        phase.getOnEnded.RemoveListener (Disable);
    }
    void OnEnable () 
    {
        if (timed)
            StartCoroutine (TimedTransition ());
        else
            StartCoroutine (ConditionalTransition ());
    }
    void Enable () => enabled = true;
    void Disable () => enabled = false;

    IEnumerator ConditionalTransition () 
    {
        while (true)
        {
            for (int i = 0; i < triggerTransitions.Length; i++) 
            {
                if (owner.TriggerConditionCheck (triggerTransitions[i]))
                {
                    phase.Advance ();
                    yield break;
                }
            }
            for (int i = 0; i < cancelTransitions.Length; i++) 
            {
                if (owner.CancelConditionCheck (cancelTransitions[i]))
                {
                    phase.Advance ();
                    yield break;
                }
            }
            yield return new WaitForSeconds (checkRate);
        }
    }

    IEnumerator TimedTransition () 
    {
        yield return new WaitForSeconds (waitDuration + Random.Range (0, waitVariance));
        while (true)
        {
            bool breakout = true;
            for (int i = 0; i < triggerTransitions.Length; i++) 
            {
                if (!owner.TriggerConditionCheck (triggerTransitions[i]))
                {
                    breakout = false;
                    break;
                }
            }
            for (int i = 0; i < cancelTransitions.Length; i++) 
            {
                if (!owner.CancelConditionCheck (cancelTransitions[i]))
                {
                    breakout = false;
                    break;
                }
            }
            if (breakout)
                break;
            else 
                yield return new WaitForSeconds (checkRate);
        }
        phase.Advance ();
    }

    void OnDrawGizmosSelected () 
    {
        var atk = GetComponentInParent<AIAttack_GenericAttack> ();
        if (!atk)
            return;
        for (int i = 0; i < triggerTransitions.Length; i++)
        {
            TriggerCondition.DrawGizmos (atk, triggerTransitions[i]);
        }
        for (int i = 0; i < cancelTransitions.Length; i++)
        {
            CancelCondition.DrawGizmos (atk, cancelTransitions[i]);
        }
    }
}