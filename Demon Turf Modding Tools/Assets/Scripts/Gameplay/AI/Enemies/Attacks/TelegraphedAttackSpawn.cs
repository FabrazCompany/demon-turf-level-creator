using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class TelegraphedAttackSpawn : MonoBehaviour
{   
    [SerializeField] float triggerDelay = 1;
    [SerializeField] ParticleSystem outerParticles;
    [SerializeField] ParticleSystem innerParticles;
    [SerializeField] ParticleSystem triggerParticles;
    [SerializeField] float fadeOutDelay = .5f;
    [SerializeField] GameObject spawnedEffect;

    void OnEnable () 
    {
        StartCoroutine (TriggerLifecycle ());
    }

    IEnumerator TriggerLifecycle ()
    {
        var timer = -Time.deltaTime;
        while (timer < triggerDelay)
        {
            timer += Time.deltaTime;
            innerParticles.transform.localScale = Vector3.one * (timer / triggerDelay);
            yield return null;
        }
        outerParticles.Stop ();
        innerParticles.Stop ();
        triggerParticles.Play ();

        if (spawnedEffect)
            TrashMan.spawn (spawnedEffect, transform.position, transform.rotation);
        
        timer = -Time.deltaTime;
        while (timer < fadeOutDelay)
        {
            timer += Time.deltaTime;
            innerParticles.transform.localScale = Vector3.one *  (1 - (timer / fadeOutDelay));
            yield return null;
        }
        innerParticles.transform.localScale = Vector3.zero;
        yield return new WaitWhile (() => outerParticles.isPlaying || innerParticles.isPlaying || triggerParticles.isPlaying);
        TrashMan.despawn (gameObject);
    }
}