using System;
using UnityEngine;
using UnityEngine.Events;

public class CombatWaveZone : MonoBehaviour
{
	[SerializeField] float waveDelay = 5;
	[SerializeField] CombatZone[] waves;
	[SerializeField] bool clearOnExit;
	[SerializeField] bool isArena;

	[SerializeField] UnityEventInt onWaveTriggered;
	[SerializeField] UnityEventInt onWaveCleared;
	[SerializeField] UnityEvent onFailed;
	
	
	
	bool cleared;
	int waveIndex;
	CombatZone currentWave;
	IPlayerController player;

	public event Action onAllWavesCleared;

	ConditionalChallengeGUI.ChallengeType challengeType => isArena ? ConditionalChallengeGUI.ChallengeType.CombatZoneArena : ConditionalChallengeGUI.ChallengeType.CombatZoneWaves;

	public bool getCleared => cleared;
	
	void Start ()
	{
		waveIndex = 0;
		foreach (var wave in waves)
		{
			// var bin = new TrashManRecycleBin ();
			// bin.prefab = wave.gameObject;
			// bin.instancesToPreallocate = 2;
			// TrashMan.manageRecycleBin (bin);

			if (wave.gameObject.scene == gameObject.scene)
			{
				wave.gameObject.SetActive (false);
			}
		}
		SpawnWave ();

		PlayerControllerEvents.onReset += OnPlayerKilled;
	}
	void OnDestroy ()
	{
		PlayerControllerEvents.onReset -= OnPlayerKilled;
	}
	void OnTriggerEnter (Collider hit)
	{
		if (cleared)
			return;

		var _player = hit.GetComponent<IPlayerController> ();
		if (_player == null || player != null)
			return;

		if (player == null)
		{
			Trigger (_player);
		}
	}

	void OnTriggerExit (Collider hit) 
	{
		var _player = hit.GetComponent<IPlayerController> ();
		if (_player == null) return;

		Cancel (_player);
	}
	public void Trigger (IPlayerController _player)
	{
		player = _player;
		player.GetChallengeGUI.Trigger (challengeType, string.Format ("Wave {0}/{1}", waveIndex + 1, waves.Length), 0, currentWave.challengeCount, 0, false);
		currentWave.TriggerZone (player);
	}
	public void Cancel (IPlayerController _player) 
	{
		currentWave.OnTriggerExit (_player.getCollider);

		if (player != null)
		{
			player.RegisterCombatZone (false);
			player.GetChallengeGUI.Cancelled ();
			player = null;
		}
	}

	void SpawnWave () 
	{
		CleanupWave ();
		currentWave = Instantiate (waves[waveIndex], transform.position, transform.rotation);
		currentWave.gameObject.SetActive (true);
		currentWave.Init ();
		currentWave.SetExternallyManaged (true);
		currentWave.onZoneCleared.AddListener (OnWaveCompleted);
		onWaveTriggered?.Invoke (waveIndex);
	}
	void CleanupWave () 
	{
		if (!currentWave)
			return;
		
		currentWave.onZoneCleared.RemoveListener(OnWaveCompleted);
		Destroy (currentWave.gameObject);
		currentWave = null;
	}
	void DelayedTriggerWave () 
	{
		if (player == null)
			return;
		player.GetChallengeGUI.Trigger (challengeType, string.Format ("Wave {0}/{1}", waveIndex + 1, waves.Length), 0, currentWave.challengeCount, null, false);
		currentWave.TriggerZone (player);
	}
	void OnWaveCompleted () 
	{
		onWaveCleared?.Invoke (waveIndex);
		if (waveIndex < waves.Length - 1)
		{
			waveIndex++;
			SpawnWave ();
			StartCoroutine (this.WaitAndDo (waveDelay, DelayedTriggerWave));
		}
		else 
		{
			if (player != null)
			{	
				player.GetChallengeGUI.Completed ();
				player.onKilled -= OnPlayerKilled;
				player = null;
			}
			var collider = GetComponent<Collider> ();
			cleared = true;
			if (collider)
				collider.enabled = false;
			onAllWavesCleared?.Invoke ();
		}
	}
	void OnPlayerKilled () 
	{
		if (cleared)
			return;
		if (player != null)
		{	
			player.GetChallengeGUI.Fail ();
			player.onKilled -= OnPlayerKilled;
			player = null;
		}
		if (currentWave)
		{
			currentWave.CleanUpEncounter ();
			CleanupWave ();
		}
		waveIndex = 0;
		onFailed?.Invoke ();
		SpawnWave ();
	}

	[ContextMenu ("Complete")]
	public void DebugComplete ()
	{

	}
}