﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Malee;

public class CombatZone : PlayerChallengeZone<AIEnemy>
{
    [SerializeField] LayerMask enemyLayer;
    [SerializeField, Reorderable] CombatZoneResidentList residents;

    List<AIEnemy> addedResidents = new List<AIEnemy> ();
	Dictionary<AIEnemy, float> absentResidents = new Dictionary<AIEnemy, float> ();

    [Header ("Spawning Behaviour")]
    [SerializeField] ParticleSystem loopSpawnEffect;
    [SerializeField] ParticleSystem spawnBurstEffect;
    [SerializeField] float defaultSpawnDuration = 1;
    List<CombatZoneSpawn> spawnQueue = new List<CombatZoneSpawn> ();
    bool _externallyManaged;
	[SerializeField] AudioClipSet initialSpawnInAudio;
	
	[Header ("Extra Behaviours")]
	[SerializeField] bool hasDistanceKillForResidents;
	[SerializeField, Conditional ("hasDistanceKillForResidents")] float distanceKilled = 500f;
	float distanceKillTimer;
	const float distanceKillRepeatRate = 0.25f;

    protected override LayerMask residentLayer => enemyLayer;
    protected override bool externallyManaged => _externallyManaged;
    public void SetExternallyManaged (bool value) => _externallyManaged = value;
    protected override ConditionalChallengeGUI.ChallengeType challengeType => ConditionalChallengeGUI.ChallengeType.CombatZone;
    public override int challengeCount => residents.Count;
    protected override QueryTriggerInteraction queryType => QueryTriggerInteraction.Ignore;
    protected override bool failOnExit => false;
	protected override string getLayer => "TriggerCombat";

	bool initialSpawnTriggered;

#region LIFECYCLE
    protected override void OnDestroy ()
    {
        base.OnDestroy ();
        foreach (var resident in residents)
        {
            resident.onInstanceKilled -= OnResidentKilled;
        }
		foreach (var resident in addedResidents)
		{
			resident.onKilled -= OnResidentKilled;
		}
    }
	protected override void Awake ()
	{
		base.Awake ();
		absentResidents = new Dictionary<AIEnemy, float> ();
	}
    public override void Init () 
    {
        base.Init ();
        if (loopSpawnEffect && !TrashMan.isManaged (loopSpawnEffect.name))
        {
            var bin = new TrashManRecycleBin ();
            bin.prefab = loopSpawnEffect.gameObject;
            bin.instancesToPreallocate = 15;
            TrashMan.manageRecycleBin (bin);
        }

        if (spawnBurstEffect && !TrashMan.isManaged (spawnBurstEffect.name))
        {
            var bin = new TrashManRecycleBin ();
            bin.prefab = spawnBurstEffect.gameObject;
            bin.instancesToPreallocate = 15;
            TrashMan.manageRecycleBin (bin);
        }
        enabled = true;
    }

    void Update () 
    {
        if (spawnQueue.Count == 0)
        {
            enabled = false;
            return;
        }

        var deltaTime = Time.deltaTime;
        foreach (var spawn in spawnQueue)
        {
            spawn.Update (deltaTime);
        }
        spawnQueue.RemoveAll (spawn => spawn.spawnFinished);

		if (hasDistanceKillForResidents)
		{
			distanceKillTimer -= deltaTime;
			if (distanceKillTimer <= 0)
			{
				distanceKillTimer = distanceKillRepeatRate;
				foreach (var enemy in addedResidents)
				{
					if (transform.DistanceSqr (enemy.transform.position) > Mathf.Pow (distanceKilled, 2))
					{
						enemy.TriggerDeath ();
					}
				}
			}
		}	
    }

	Coroutine absentResidentRoutine;
	IEnumerator RemoveAbsentResidentsRoutine ()
	{
		while (absentResidents.Count > 0)
		{
			UpdateAbsentResidents ();
			yield return null;
		}
		absentResidentRoutine = null;
	}
	void UpdateAbsentResidents ()
	{
		if (absentResidents.Count == 0) return;
		for (int i = 0; i < residents.Count; i++)
		{

			var key = residents[i].getInstance;
			if (!key) continue;
			if (!absentResidents.ContainsKey (key)) continue;
			absentResidents[key] += Time.deltaTime;
			if (absentResidents[key] > 10)
			{
				var getKill = key.GetComponentInChildren<ReceiveKill> ();
				if (getKill)
				{
					absentResidents.Remove (key);
					getKill.TriggerKill ();
				}
			}
		}
	}
    public override void OnTriggerEnter(Collider other) 
    {
        CheckForMinion (other);
        base.OnTriggerEnter (other);   
		
		var enemy = other.GetComponentInParent<AIEnemy> ();
		if (enemy 
			&& residents.Find (i => i.isInstance (enemy)) != null 
			&& absentResidents.ContainsKey (enemy))
		{
			absentResidents.Remove (enemy);
		}
    }
	public override void OnTriggerExit (Collider other)
	{
		base.OnTriggerExit (other);

		var enemy = other.GetComponentInParent<AIEnemy> ();
		if (enemy 
			&& residents.Find (i => i.isInstance (enemy)) != null 
			&& !absentResidents.ContainsKey (enemy))
		{
			absentResidents.Add (enemy, 0);
			if (absentResidentRoutine == null)
			{
				this.RestartCoroutine (RemoveAbsentResidentsRoutine (), ref absentResidentRoutine);
			}
		}
	}
    void CheckForMinion (Collider other) 
    {
        var enemy = other.GetComponentInParent<AIEnemy> ();
        if (!enemy) return;
        foreach (var resident in residents)
        {
            if (resident.isInstance (enemy)) return;
        }
        if (addedResidents.Contains (enemy)) return;
        
        addedResidents.Add (enemy);
        enemy.onKilled += OnResidentKilled;
    }
    void ClearMinions () 
    {
        foreach (var enemy in addedResidents)
        {
            enemy.onKilled -= OnResidentKilled;
        }
        addedResidents.Clear ();
    }
    public override void TriggerZone (IPlayerController _player)
    {
        base.TriggerZone (_player);
		if (!gameObject.activeInHierarchy) return;
        foreach (var resident in residents)
            resident.OnEnter (player);
        enabled = true;
		
		if (!initialSpawnTriggered && initialSpawnInAudio.HasSound)
		{
			initialSpawnTriggered = true;
			SoundManagerStub.Instance.PlaySoundUI (initialSpawnInAudio.GetSound);
		}
    }
    protected override void ExitZone () 
    {
        base.ExitZone ();
		if (!gameObject.activeInHierarchy) return;        
        foreach (var resident in residents)
            resident.OnExit (false);
    }
    protected override void CleanupZone () 
    {
        base.CleanupZone ();
		if (!gameObject.activeInHierarchy) return;
        foreach (var spawn in spawnQueue)
            spawn.Cleanup ();

        spawnQueue.Clear ();
        
        foreach (var resident in residents)
            resident.OnExit (true);
    }

    void OnResidentKilled (AIEnemy enemy) => OnChallengeProgressed ();
    protected override int TallyProgress ()
    {
        int progress = 0;
        foreach (var resident in residents)
            if (!resident.isAlive)
                progress++;
        return progress;
    }

    protected override void OnPlayerReset () 
    {
        base.OnPlayerReset ();
		if (!gameObject.activeInHierarchy) return;
		if (cleared) return;
        foreach (var resident in residents)
        {
            resident.OnPlayerKilled ();
        }
		initialSpawnTriggered = false;
    }

#endregion

#region RESIDENT MANAGEMENT
    protected override void AddResidents () 
    {
        base.AddResidents ();
        if (Application.isPlaying)
        {
            foreach (var resident in residents)
            {
                resident.Init (loopSpawnEffect, spawnBurstEffect, spawnQueue);
                resident.onInstanceKilled += OnResidentKilled;
                resident.OnStart ();
            }
        }
        
    }
    protected override void AddResident (AIEnemy enemy) 
    {
        if (!enemy)
            return;
            
        if (enemy.name.Contains ("(Clone)"))
            return;

        if (!residents.Exists (i => i.getSource == enemy))
        {
            var resident = new CombatZoneResident (enemy, defaultSpawnDuration);
            residents.Add (resident);
        }
    }
    [ContextMenu ("Add Occupant Children")]
    void AddResidentChilderen () 
    {
        var residents = GetComponentsInChildren<AIEnemy> (true);
        foreach (var resident in residents)
        {
            AddResident (resident);
        }
    }
    [ContextMenu ("Clear Residents")]
    protected override void ClearResidents () 
    {
        foreach (var resident in residents)
        {
            resident.OnCleanup ();
        }
        residents.Clear ();
    }

    protected override void RefreshResidents () 
    {
        foreach (var resident in residents)
        {
            resident.OnRestore ();
        }
    }
    [ContextMenu ("Set To Spawn On Enter")]
    void SetResidentsToSpawnOnEnter () 
    {
        foreach (var resident in residents)
        {
            resident.SetSpawnOnEnter (true);
        }
    }

    
#endregion

    public void CleanUpEncounter ()
    {
        if (player != null)
        {
            if (!externallyManaged)
            {
                player.GetChallengeGUI.Fail ();
            }
            player.RegisterCombatZone (false);
            player = null;
        }
        foreach (var resident in residents)
        {
            if (resident.isAlive)
            {
                resident.OnCleanup ();
            }
        }
    }

    [Serializable]
    class CombatZoneResidentList : ReorderableArray<CombatZoneResident> {}
    [Serializable]
    class CombatZoneResident 
    {
        [SerializeField] AIEnemy source;
        [SerializeField] float startDelay = 0;
        [SerializeField] float spawnDelay = 0.3f;
        [SerializeField] bool spawnOnEntry;
        [SerializeField] bool despawnOnExit;

        AIEnemy instance;
        List<ICombatZoneCleanup> addedCleanup;// = new List<ICombatZoneCleanup> ();
        ParticleSystem spawnLoopEffect;
        ParticleSystem spawnBurstEffect;
        List<CombatZoneSpawn> spawnList;
        public AIEnemy getSource => source;
		public AIEnemy getInstance => instance;
        public bool isAlive => instance != null && instance.isAlive;
        bool killed;
        public event Action<AIEnemy> onInstanceKilled;

        public bool isInstance (AIEnemy enemy) => instance == enemy;

        public void SetSpawnOnEnter (bool active) => spawnOnEntry = active;

        public CombatZoneResident (AIEnemy _source, float _spawnDur)
        {
            source = _source;
            addedCleanup = new List<ICombatZoneCleanup> ();
            spawnDelay = _spawnDur;
        }

        public void Init (ParticleSystem _spawnLoopEffect, ParticleSystem _spawnBurstEffect, List<CombatZoneSpawn> _spawnList) 
        {
            spawnLoopEffect = _spawnLoopEffect;
            spawnBurstEffect = _spawnBurstEffect;
            spawnList = _spawnList;
            if (!source.name.Contains ("(Source)"))
                source.name += " (Source)";
            source.gameObject.SetActive (false);
            source.SetTargetDiscovery (false);
			spawnOnEntry = true;
        }
        public void OnStart () 
        {
            if (!spawnOnEntry && !killed)
                Spawn ();
            else if (isAlive)
                Despawn ();
        }
        public void OnEnter (IPlayerController player) 
        {
            if (!isAlive && !killed)
                Spawn (player);
            else 
            {
                instance.SetAttackTarget (player);
                instance.SetTargetDiscovery (true);
            }
        }
        public void OnExit (bool resetOnExit) 
        {
            if (despawnOnExit)
                DespawnWithParticle ();
            else if (isAlive)
                instance.SetTargetDiscovery (false);
                // instance.ClearAttackTarget ();

            if (resetOnExit)
            {
                if (!isAlive)
                {
                    killed = false;
                    if (!spawnOnEntry)
                        Spawn ();
                    if (spawnBurstEffect)
                        TrashMan.spawn (spawnBurstEffect.gameObject, source.transform.position);
                }
            }
        }
        public void OnCleanup () 
        {
            if (isAlive)
            {
                Despawn ();
            }
        }
        public void OnRestore () 
        {
            OnCleanup ();
            if (source)
                source.gameObject.SetActive (true);
        }
        public void OnPlayerKilled () 
        {
            killed = false;
            OnStart ();
        }

        void Spawn (IPlayerController player) 
        {
            var spawn = new CombatZoneSpawn 
            (
                startDelay, 
                spawnDelay, 
                spawnLoopEffect,
                source.transform.position,
                (CombatZoneSpawn _spawn) => 
                {
                    if (spawnBurstEffect)
                        TrashMan.spawn (spawnBurstEffect.gameObject, source.transform.position);
                    Spawn ();
                    instance.SetAttackTarget (player);
                }
            );
            spawnList.Add (spawn);
        }
        void Spawn () 
        {
            if (instance)
                Despawn ();
            instance = Instantiate (source, source.transform.position, source.transform.rotation, source.transform.parent) as AIEnemy;
			instance.SetInitialPositionAndRotation (source.transform.position, source.transform.rotation);
            instance.gameObject.SetActive (true);
            if (addedCleanup == null)
                addedCleanup = new List<ICombatZoneCleanup> ();
            instance.GetComponentsInChildren<ICombatZoneCleanup> (true, addedCleanup);
            instance.onKilled += OnKilled;
            killed = false;
        }
        void DespawnWithParticle ()
        {
			if (!isAlive) return;
            if (spawnBurstEffect)
            {
                if (instance)
                    TrashMan.spawn (spawnBurstEffect.gameObject, instance.transform.position);
                for (int i = 0; i < addedCleanup.Count; i++)
                {
                    TrashMan.spawn (spawnBurstEffect.gameObject, addedCleanup[i].transform.position);
                }
            }
            Despawn ();
        }
        void Despawn () 
        {
            instance.onKilled -= OnKilled;
            Destroy (instance.gameObject);
            for (int i = 0; i < addedCleanup.Count; i++)
            {
                addedCleanup[i].Cleanup ();
            }
            addedCleanup.Clear ();
            instance = null;
        }
        void OnKilled (AIEnemy instance) 
        {
            killed = true;
            onInstanceKilled?.Invoke (instance);
        }

		void CleanupEvents ()
		{
			if (instance)
				instance.onKilled -= OnKilled;
		}
        
    }

    class CombatZoneSpawn : IDisposable
    {
        float delayTimer;
        float spawnTimer;
        Vector3 spawnPosition;
        ParticleSystem spawnEffect;
        ParticleSystem effectInstance;
        Action<CombatZoneSpawn> onSpawnFinished;

        bool spawnInitiated => delayTimer <= 0;
        public bool spawnFinished => spawnTimer <= 0;

        public CombatZoneSpawn (float _start, float _spawn, ParticleSystem _effect, Vector3 _spawnPosition, Action<CombatZoneSpawn> _onSpawnFinished)
        {
            delayTimer = _start;
            spawnTimer = _start + _spawn;
            spawnEffect = _effect;
            spawnPosition = _spawnPosition;
            onSpawnFinished = _onSpawnFinished;
            
            if (spawnInitiated && spawnEffect)
			{
				if (effectInstance != null)
	            	effectInstance.Stop (true);
				effectInstance = TrashMan.spawn (spawnEffect.gameObject, spawnPosition).GetComponent<ParticleSystem> ();
			}
                
        }
        public void Update (float dt) 
        {
            var wasSpawning = spawnInitiated;
            var wasFinished = spawnFinished;

            delayTimer -= dt;
            spawnTimer -= dt;

            if (!wasSpawning && spawnInitiated && spawnEffect)
            {
				if (effectInstance != null)
	            	effectInstance.Stop (true);
                effectInstance = TrashMan.spawn (spawnEffect.gameObject, spawnPosition).GetComponent<ParticleSystem> ();
            }
            if (spawnFinished)
            {
                if (effectInstance != null)
	            	effectInstance.Stop (true);
                onSpawnFinished?.Invoke (this);
            }
        }
        public void Cleanup ()
        {
			if (effectInstance != null)
	            effectInstance.Stop (true);
        }

		public void Dispose()
		{
			if (effectInstance != null)
				effectInstance.Stop ();
		}
	}
}
