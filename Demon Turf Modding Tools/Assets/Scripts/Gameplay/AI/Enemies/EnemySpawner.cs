using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

    [SerializeField] AIEnemy enemy;
    [SerializeField] GameObject spawnFx;
    AIEnemy enemyInstance;
    public void SpawnEnemy (bool cleanupPrevious = false) {
        if (cleanupPrevious && enemyInstance)
            Destroy (enemyInstance.gameObject);

        TrashMan.spawn (spawnFx, transform.position);
        enemyInstance = TrashMan.spawn (enemy.gameObject, transform.position).GetComponent<AIEnemy> ();
    }

}

public interface IEnemy {
    Transform transform { get; set; }
}