using UnityEngine;

public interface ICombatZoneCleanup
{
    Transform transform { get; }    
    void Cleanup ();
}