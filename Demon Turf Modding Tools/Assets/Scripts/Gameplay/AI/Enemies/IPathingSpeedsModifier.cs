using System;
public interface IPathingSpeedModifier
{
    float modifier { get; }
    event Action<float> onModValuesUpdated;
}