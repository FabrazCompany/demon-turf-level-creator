using UnityEngine;

public interface ITargetTraits 
{
    bool isGrounded { get; }
    bool isFrozen { get; }
    bool isStabilizing { get; }
    bool isPreppingAttack { get; }
    bool isAttacking { get; }
    bool isRecoveringFromAttack { get; }
    float visibilityLevel { get; }
}