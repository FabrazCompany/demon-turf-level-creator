﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AITrait_BubbleShield : MonoBehaviour, IProjectileSpawnerMod
{
    [SerializeField] float knockbackAdjust = .5f;
    [SerializeField] float spinAdust = .5f;
    [SerializeField] float popForce = 15;
    [SerializeField] float deactivateDelay = 0.25f;
    AIEnemy owner;
    new Collider collider;
    ReceiveKnockback knockback;
    ReceiveSpin spin;
    ReceiveKill kill;

    List<ProjectileSpawner> spawners;

    void Awake () 
    {
        owner = GetComponentInParent<AIEnemy> ();
        collider = GetComponent<Collider> ();
        spawners = new List<ProjectileSpawner> ();
        owner.GetComponentsInChildren<ProjectileSpawner> (false, spawners);
        colliders = new List<Collider> ();
        knockback = owner.GetComponentInChildren<ReceiveKnockback> ();
        spin = owner.GetComponentInChildren<ReceiveSpin> ();
        kill = owner.GetComponentInChildren<ReceiveKill> ();
    }   

    void Start () 
    {
        knockback.SetVulnerability (knockbackAdjust);
        spin.SetSpinVulnerability (spinAdust);
        kill.SetMute (true);
        foreach (var spawner in spawners) 
        {
            spawner.RegisterMod (this);
        }
    } 

    

    void Pop (Vector3 dir)
    {
        knockback.ApplyKnockback (dir, popForce);
        Invoke ("DelayedPop", deactivateDelay);
    }
    void DelayedPop () 
    {
        knockback.ResetVulnerability ();
        spin.ResetVulnerability ();
        kill.SetMute (false);
        foreach (var spawner in spawners) 
        {
            spawner.UnregisterMod (this);
        }
        
        transform.SetParent (null);
        TrashMan.despawn (gameObject);
    }

    void OnTriggerEnter (Collider hit)
    {
        if (hit.transform.IsChildOf (owner.transform))
            return;

        var kill = hit.GetComponent<DeliverKill> ();
        if (!kill)
            return;
        
		if (hit.TryGetComponent<Projectile> (out var projectile))
		{
			projectile.Detonate ();
			return;
		}

        var point = Physics.ClosestPoint (transform.position, hit, hit.transform.position, hit.transform.rotation);
        var direction = (transform.position - point).normalized;
        StartCoroutine (this.WaitFrameAndDo (() => Pop (direction)));
    }

    List<Collider> colliders;
    void IProjectileSpawnerMod.ApplyMod(Projectile projectile)
    {
        projectile.GetComponentsInChildren<Collider> (false, colliders);
        foreach (var coll in colliders) 
        {
            Physics.IgnoreCollision (collider, coll, true);
            projectile.getOnReflect.AddListener ((ReflectProjectiles val) => 
            {
                Physics.IgnoreCollision (collider, coll, false);
            });
        }
    }
}
