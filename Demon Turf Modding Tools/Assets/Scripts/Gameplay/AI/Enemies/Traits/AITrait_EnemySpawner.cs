using UnityEngine;

public class AITrait_EnemySpawner : MonoBehaviour 
{

    AIEnemy owner;

    [SerializeField] Transform spawnPoint;
    [SerializeField] AIEnemy enemyPrefab;
    
    [SerializeField] GameObject spawnEffet;
    [SerializeField] float spawnForceUp;
    [SerializeField] float spawnForceForward;

    public event System.Action<AIEnemy> onEnemySpawned;
    
    void Awake () 
    {
        owner = GetComponentInParent<AIEnemy> ();
    }

    public void SpawnEnemy ()
    {
        TrashMan.spawn (spawnEffet, spawnPoint.position);
        var newEnemy = Instantiate (enemyPrefab, spawnPoint.position, Quaternion.identity);
        
        var force = owner.GetDirectionToTarget () * spawnForceForward;
        force.y = spawnForceUp;
        newEnemy.getRigid.AddForce (force, ForceMode.VelocityChange);
        
        onEnemySpawned?.Invoke (newEnemy);
    } 

}