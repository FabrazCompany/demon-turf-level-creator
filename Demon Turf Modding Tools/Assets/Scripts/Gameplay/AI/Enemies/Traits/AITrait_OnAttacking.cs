using UnityEngine;
using UnityEngine.Events;

public class AITrait_OnAttacking : MonoBehaviour
{
    [SerializeField] UnityEvent onAttackStarted;
    [SerializeField] UnityEvent onAttackStopped;
    void Awake ()
    {
        var owner = GetComponent<AIEnemy> ();
        if (!owner)
            return;

        owner.onAttackStarted += onAttackStarted.Invoke;
        owner.onAttackStopped += onAttackStopped.Invoke;
    } 
    void OnDestroy () 
    {
        var owner = GetComponent<AIEnemy> ();
        if (!owner)
            return;

        owner.onAttackStarted -= onAttackStarted.Invoke;
        owner.onAttackStopped -= onAttackStopped.Invoke;
    }


}