using UnityEngine;
using UnityEngine.Events;

[RequireComponent (typeof (AIEnemy))]
public class AITrait_OnDestabilized : MonoBehaviour
{
    [SerializeField] UnityEvent onDestabilized;
    void Awake () 
    {
        var owner = GetComponent<AIEnemy> ();
        if (!owner)
            return;

        owner.onDestabilized += onDestabilized.Invoke;
    }
    void OnDestroy () 
    {
        var owner = GetComponent<AIEnemy> ();
        if (!owner)
            return;

        owner.onDestabilized -= onDestabilized.Invoke;
    }

}