using UnityEngine;
using UnityEngine.Events;

[RequireComponent (typeof (AIEnemy))]
public class AITrait_OnGrounded : MonoBehaviour
{
    [SerializeField] UnityEvent onGrounded;
	AIEnemy owner;
    void Awake () 
    {
        owner = GetComponent<AIEnemy> ();
        if (!owner)
            return;
        owner.onGrounded += OnGrounded;
    }
	void Start ()
	{
		
	}
    void OnDestroy () 
    {
        if (!owner)
            return;	
        owner.onGrounded -= OnGrounded;
    }

	void OnGrounded ()
	{
		if (!enabled)
			return;
		onGrounded?.Invoke ();
	}

}