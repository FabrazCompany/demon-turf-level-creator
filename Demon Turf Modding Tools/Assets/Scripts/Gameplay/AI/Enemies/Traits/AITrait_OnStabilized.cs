using UnityEngine;
using UnityEngine.Events;

[RequireComponent (typeof (AIEnemy))]
public class AITrait_OnStabilized : MonoBehaviour
{
    [SerializeField] UnityEvent onStabilized;
    void Awake () 
    {
        var owner = GetComponent<AIEnemy> ();
        if (!owner)
            return;

        owner.onStabilized += onStabilized.Invoke;
    }
    void OnDestroy () 
    {
        var owner = GetComponent<AIEnemy> ();
        if (!owner)
            return;

        owner.onStabilized -= onStabilized.Invoke;
    }

}