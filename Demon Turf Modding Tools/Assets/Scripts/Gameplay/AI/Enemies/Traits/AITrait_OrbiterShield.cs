﻿using System.Collections.Generic;
using UnityEngine;

public class AITrait_OrbiterShield : MonoBehaviour
{
    [SerializeField] float radius;
    [SerializeField] float speedIdle;
    [SerializeField] float speedActive;
    [SerializeField] float acceleration = 2;

    float speedScalar = 1;
    float accelerationScalar = 1;

    AIEnemy enemy;
    List<AITrait_OrbiterShieldBit> bits;
    bool active;
    float targetSpeed;
    float currentSpeed;
    Quaternion currentRot;

    float currentAcceleration => acceleration * accelerationScalar;

    public void SetSpeedScalar (float val) => speedScalar = val;
    public void SetAccelerationScalar (float val) => accelerationScalar = val;

    void Awake () 
    {
        enemy = GetComponentInParent<AIEnemy> ();
        bits = new List<AITrait_OrbiterShieldBit> ();
        GetComponentsInChildren<AITrait_OrbiterShieldBit> (true, bits);

        TransformUtility.ArrangeObjects (transform, radius, Orientation.y, true, true, Vector3.left);
    }
    void Start () 
    {
        currentSpeed = speedIdle;
        foreach (var bit in bits)
        {
            if (!bit)
                continue;
            bit.onDetached += OnBitRemoved;
        }
    }
    void OnDestroy ()
    {
        foreach (var bit in bits)
        {
            if (!bit)
                continue;
            bit.onDetached -= OnBitRemoved;
        }
    }
    void OnEnable () 
    {
        currentRot = Quaternion.identity;
        speedScalar = 1;
        accelerationScalar = 1;
        targetSpeed = speedIdle;
    }
    void Update () 
    {
        var deltaTime = Time.deltaTime;
        currentSpeed = Mathf.MoveTowards (currentSpeed, targetSpeed * speedScalar, currentAcceleration * deltaTime);
        currentRot *= Quaternion.Euler (Vector3.up * currentSpeed * deltaTime);
        transform.rotation = currentRot;
    }

    public void ToggleActivate () => Activate (!active);
    public void Activate (bool state) 
    {
        if (active == state) return;

        active = state;
        targetSpeed = enemy != null && enemy.isAwareOfTarget ? speedActive : speedIdle;
        foreach (var bit in bits)
        {
            bit.Activate (state);
        }
    }
	public void ForceDeactivate (bool instant)
	{
		active = false;
        targetSpeed = speedIdle;
        foreach (var bit in bits)
        {
			bit.ForceDeactivate ();
        }
	}
    void OnBitRemoved (AITrait_OrbiterShieldBit bit)
    {
        if (bits.Contains (bit))
            bits.Remove (bit);
    }
    void OnValidate () 
    {
        TransformUtility.ArrangeObjects (transform, radius, Orientation.y, true, true, Vector3.left);
    }
}
