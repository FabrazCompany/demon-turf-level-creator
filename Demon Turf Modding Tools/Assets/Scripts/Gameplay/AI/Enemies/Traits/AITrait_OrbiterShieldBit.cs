using UnityEngine;

public class AITrait_OrbiterShieldBit : MonoBehaviour
{
    [SerializeField] Collider laser;
    [SerializeField] float growTime = .5f;
    [SerializeField] float shrinkTime = .25f;

    float timer;

    Collider bitCollider;
    Rigidbody rigid;
    HookshotTarget hookshotTarget;
    bool mute;
    public System.Action<AITrait_OrbiterShieldBit> onDetached;
    void Awake () 
    {
        rigid = GetComponent<Rigidbody> ();
        bitCollider = GetComponent<Collider> ();
        bitCollider.enabled = false;
        hookshotTarget = GetComponentInChildren<HookshotTarget> ();
    }
	public void ForceDeactivate ()
	{
		if (laser.transform.localScale.z == 0) return;

		timer = 0;
		enabled = true;
		laser.enabled = false;
        if (!laser.gameObject.activeSelf)
            laser.gameObject.SetActive (true);
	}
    public void Activate (bool state, bool instant = false)
    {
        if (mute)
            state = false;
        if (!instant)
        {
            timer = 0;
            enabled = true;
        }
        else 
        {
            laser.transform.localScale = laser.transform.localScale.With (z: state ? 1 : 0);
            enabled = false;
        }
        laser.enabled = state;
        if (!laser.gameObject.activeSelf)
            laser.gameObject.SetActive (true);
    }
    void Update () 
    {
        timer += Time.deltaTime;
        
        var maxTimer = laser.enabled ? growTime : shrinkTime;
        var start = laser.enabled ? 0 : 1;
        var end = laser.enabled ? 1 : 0;
        var scale = laser.transform.localScale;
        scale.z = Mathf.Lerp (start, end, timer / maxTimer);
        laser.transform.localScale = scale;
        
        if (timer >= maxTimer) 
        {
            if (!laser.enabled)
                laser.gameObject.SetActive (false);
            enabled = false;
        }
    }
    public void Hold () 
    {
        mute = true;
        Activate (false);
    }
    public void Detach (Vector3 force) 
    {
        Activate (false);
        bitCollider.enabled = true;
        transform.SetParent (null);
        rigid.isKinematic = false;
        rigid.AddForce (force, ForceMode.VelocityChange);
        onDetached?.Invoke (this);
    }
}