﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AITrait_ProximityEffect : MonoBehaviour
{
    [SerializeField] float range = 5;
    [SerializeField] float rangeBuffer = .5f;
    [SerializeField] bool flattenY;
    [SerializeField] UnityEvent onEnterProximity;
    [SerializeField] UnityEvent onExitProximity;
    AISense_TargetDetector senses;
    bool inProximity;

    void Awake () 
    {
        senses = GetComponentInChildren<AISense_TargetDetector> (true);
    }

    void Update () 
    {
        if (!senses?.Target)
            return;

        var target = senses.Target.position;
        if (flattenY)
            target.y = transform.position.y;
        
        var dist = Vector3.SqrMagnitude (transform.position - target);

        if (inProximity) 
        {
            if (dist >= (range + rangeBuffer) * (range + rangeBuffer))
            {
                inProximity = false;
                onExitProximity?.Invoke ();
            }
        }
        else 
        {
            if (dist <= (range - rangeBuffer) * (range - rangeBuffer))
            {
                inProximity = true;
                onEnterProximity?.Invoke ();
            }
        }
    }

    [SerializeField] bool drawGizmos;
    void OnDrawGizmos () 
    {
        if (!drawGizmos)
            return;

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere (transform.position, range);
    }

}
