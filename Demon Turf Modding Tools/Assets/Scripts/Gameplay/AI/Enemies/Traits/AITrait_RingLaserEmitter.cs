using UnityEngine;

public class AITrait_RingLaserEmitter : MonoBehaviour
{
    [Header ("Timing")]
    [SerializeField] float startup;
    [SerializeField] float cooldown;

    [Header ("Light")]
    [SerializeField] Light lit;
    [SerializeField] float litRange = 10;

    [Header ("Materials")]
    [SerializeField] ParticleSystem particles;
    ParticleProjectiles particleProjectiles;
    ParticleProjectileTarget particlesHitbox;
    [SerializeField] Renderer rend;
    [SerializeField] int matIndex;
    [SerializeField] Color surfaceColor;
    [SerializeField, ColorUsage (false, true)] Color emissionColor;

    [Header ("Triggers")]
    [SerializeField] bool triggerOnTargetSighted = true;
    AIEnemy owner;
    AISense_TargetDetector senses;
    Rigidbody rigid;
    Material mat;
    float timer;
    bool preppingAttack;
    void Awake () 
    {
        rigid = GetComponent<Rigidbody> ();
        mat = rend.materials[matIndex];
        owner = GetComponentInParent<AIEnemy> ();
        senses = owner.GetComponentInChildren<AISense_TargetDetector> (true);
        senses.onTargetSighted.AddListener (TargetSighted);
        senses.onTargetLost.AddListener (TargetLost);
        particleProjectiles = particles.GetComponent<ParticleProjectiles> ();
        particlesHitbox = owner.GetComponentInChildren<ParticleProjectileTarget> (true);
        if (particlesHitbox)
            particleProjectiles.IgnoreCollider (particlesHitbox.transform);
        Inactive ();
    }
    void OnEnable () 
    {
        preppingAttack = false;
        Inactive ();
    }
    void OnDestroy () 
    {
        Destroy (mat);
        if (senses)
        {
            senses.onTargetSighted.RemoveListener (TargetSighted);
            senses.onTargetLost.RemoveListener (TargetLost);
        }
    }

    void Update () 
    {
        if (preppingAttack)
        {
            Active (1 - (timer / startup));
        }

        timer -= Time.deltaTime;

        if (timer <= 0)
        {
            if (!preppingAttack)
            {
                timer = startup;
            }
            else 
            {
                particles.Play ();
                Inactive ();
            }
            preppingAttack = !preppingAttack;
        }
    }

    void Inactive () 
    {
        timer = cooldown;
        mat.SetColor ("_Color", Color.black);
        mat.SetColor ("_EmissionColor", Color.black);
        lit.range = 0;
    }
    void Active (float t)
    {
        mat.SetColor ("_Color", Color.Lerp (Color.black, surfaceColor, t));
        mat.SetColor ("_EmissionColor", Color.Lerp (Color.black, emissionColor, t));
        lit.range = Mathf.Lerp (0, litRange, t);
    }

    void TargetSighted (Transform tr) => enabled = true;
    void TargetLost () => enabled = false;

    public void Detach (Vector3 force) 
    {
        Inactive ();
        // enabled = false;
        if (particlesHitbox && particleProjectiles)
            particleProjectiles.TrackCollider (particlesHitbox.transform);
        transform.SetParent (null);
        rigid.isKinematic = false;
        rigid.AddForce (force, ForceMode.VelocityChange);
        if (senses)
        {
            senses.onTargetSighted.RemoveListener (TargetSighted);
            senses.onTargetLost.RemoveListener (TargetLost);
        }
        enabled = true;
    }
}