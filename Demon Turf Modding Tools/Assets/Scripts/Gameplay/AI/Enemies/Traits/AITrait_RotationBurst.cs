using UnityEngine;

public class AITrait_RotationBurst : MonoBehaviour
{
    [SerializeField] float rotationSpeed = 20;
    [SerializeField] float decaySpeed = 5;
    [SerializeField] Vector3 rotationAxis;
    float currentSpeed;

    void OnEnable () 
    {
        currentSpeed = rotationSpeed;
    }

    void Update () 
    {
        var dt = Time.deltaTime;
        currentSpeed -= decaySpeed * dt;
        transform.Rotate (rotationAxis, currentSpeed, Space.Self);
        if (currentSpeed <= 0)
            enabled = false;
    }

}