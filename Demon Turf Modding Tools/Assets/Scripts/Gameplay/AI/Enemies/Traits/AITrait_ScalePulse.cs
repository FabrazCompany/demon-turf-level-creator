﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AITrait_ScalePulse : MonoBehaviour
{
    [SerializeField] float magnitude;
    [SerializeField] float pulse;
    Vector3 originalScale;
    void Awake () 
    {
        originalScale = transform.localScale;
    }
    void OnDisable () 
    {
        transform.localScale = originalScale;
    }
    void Update () 
    {
        var pct = ((Mathf.Sin (Time.time * pulse) + 1) * 0.5f);
        transform.localScale = originalScale + (Vector3.one * pct * magnitude);
    }
}
