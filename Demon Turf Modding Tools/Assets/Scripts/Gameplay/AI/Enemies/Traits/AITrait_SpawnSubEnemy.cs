using System;
using UnityEngine;
using UnityEngine.Events;

public class AITrait_SpawnSubEnemy : MonoBehaviour
{
    [SerializeField] AIEnemy spawnedPrefab;
    [SerializeField] bool applySpawnForce;
    [SerializeField, Conditional ("applySpawnForce")] float spawnForceForward;
	[SerializeField] bool triggerDeathEvent = true;
    AIEnemy owner;
    AIEnemy spawned;

    public event Action<AIEnemy> enemySpawned;
    public event Action<AIEnemy> onSpawnedKilled;

    void Awake () 
    {
        owner = GetComponentInParent<AIEnemy> ();

    }
    void OnDestroy () 
    {
        if (spawned) 
        {
            spawned.onKilled -= OnSpawnedKilled;
        }
    }
    void OnSpawnedKilled (AIEnemy killed) => owner.TriggerDeath ();

    public void TriggerSpawn () 
    {
        spawned = TrashMan.spawn (spawnedPrefab.gameObject, transform.position, transform.rotation).GetComponent<AIEnemy> ();
        spawned.SetAttackTarget (owner.GetTarget ());
		if (triggerDeathEvent)
			spawned.onKilled += OnSpawnedKilled;

        if (applySpawnForce)
        {
            spawned.getRigid.AddForce (transform.forward * spawnForceForward, ForceMode.VelocityChange);
        }

        enemySpawned?.Invoke (spawned);
    }
}