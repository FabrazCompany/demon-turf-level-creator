using UnityEngine;

public class AITrait_SpawnSubObject : MonoBehaviour 
{
    [SerializeField] GameObject spawnedPrefab;
    AIEnemy owner;
    GameObject spawned;

    void Awake () 
    {
        owner = GetComponentInParent<AIEnemy> ();

    }
    void OnDestroy () 
    {
        if (spawned) 
        {
            Destroy (spawned.gameObject);
        }
    }

    public void TriggerSpawn () 
    {
        spawned = TrashMan.spawn (spawnedPrefab, transform.position, transform.rotation);
    }

	public void TriggerSpawnAndAddForceUp (float force) 
    {
        spawned = TrashMan.spawn (spawnedPrefab, transform.position, transform.rotation);
		if (spawned.TryGetComponent<Rigidbody> (out var rigid))
		{
			rigid.AddForce (Vector3.up * force, ForceMode.VelocityChange);
		}
    }
}