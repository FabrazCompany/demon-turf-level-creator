using UnityEngine;

public class AITrait_SpawnUnderPlayer : MonoBehaviour, ITargeted
{
    [SerializeField] bool spawnWhenDone;
    [SerializeField, Conditional ("spawnWhenDone", hide: true)] float spawnDelay;
    [SerializeField] GameObject attackEffect;
    [SerializeField] LayerMask triggerLayer;
    Transform target;
	Coroutine delayedTriggerRoutine;
    void ITargeted.AssignTarget (Transform _target) => target = _target;
    void ITargeted.ClearTarget () => target = null;
    void OnEnable () 
    {
        if (!spawnWhenDone)
			this.RestartCoroutine (this.WaitAndDo (spawnDelay, SpawnEffect), ref delayedTriggerRoutine);
    }
    void OnParticleSystemStopped () 
    {
        TrashMan.despawn (gameObject);
        if (spawnWhenDone && target)
            SpawnEffect ();
    }

    void SpawnEffect () 
    {
        var hit = Physics.Raycast (target.position + Vector3.up,Vector3.down, out RaycastHit info, 11, triggerLayer);
        TrashMan.spawn (attackEffect, hit ? info.point : target.position, Quaternion.Euler (Vector3.up * Random.value * 360));
    }
}