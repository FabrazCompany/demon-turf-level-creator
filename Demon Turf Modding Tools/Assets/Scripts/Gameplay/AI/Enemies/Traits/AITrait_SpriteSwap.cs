﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class AITrait_SpriteSwap : MonoBehaviour
{
    [SerializeField] SpriteRenderer rend;
	[SerializeField] Sprite[] spriteSheetOriginals;
    [SerializeField] Sprite[] spriteSheetOverrides;
	Sprite cachedSprite;

    void Awake () 
    {
        rend = rend ?? GetComponent<SpriteRenderer> ();
        if (!rend)
        {
            Debug.LogError ("No Sprite Renderer Assigned!", this);
            enabled = false;   
        }
		else 
			cachedSprite = rend.sprite;
    }

    void LateUpdate ()
    {
		var length = Mathf.Min (spriteSheetOriginals.Length, spriteSheetOverrides.Length);
		for (int i = 0; i < length; i++)
		{
			if (spriteSheetOriginals[i] == rend.sprite)
			{
				rend.sprite = spriteSheetOverrides[i];
				break;
			}
		}
    }

	public void SetSpritesheet (Sprite[]  sprites) 
	{
		if (rend == null || spriteSheetOriginals == null) return;
		spriteSheetOverrides = new Sprite[sprites.Length];
		for (int i = 0; i < spriteSheetOriginals.Length; i++)
		{
			foreach (var sprite in sprites)
			{
				if (spriteSheetOriginals[i].name.Equals (sprite.name))
				{
					spriteSheetOverrides[i] = sprite;
					if (cachedSprite == spriteSheetOriginals[i])
						rend.sprite = sprite;
					break;
				}
			}
		}
	}
	public void SetOriginalSprites (IEnumerable <Sprite>  sprites)
	{
		spriteSheetOriginals = new Sprite[spriteSheetOverrides.Length];
		for (int i = 0; i < spriteSheetOverrides.Length; i++)
		{
			foreach (var sprite in sprites)
			{
				if (spriteSheetOverrides[i].name.Equals (sprite.name))
				{
					spriteSheetOriginals[i] = sprite;
					break;
				}
			}
		}
	}
}
