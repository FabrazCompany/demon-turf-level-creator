using System.Collections;
using UnityEngine;
using UnityEngine.Events;
public class AITrait_Sticky : MonoBehaviour, ISticky
{

    [SerializeField] UnityEvent onStick;
    [SerializeField] UnityEvent onUnstick;
    [SerializeField] float restickyDelay = 0.5f;
    [SerializeField] float slowdownRatio = 0.1f;
    [SerializeField] float popForceHoriz = 10;
    [SerializeField] float popForceVert = 5;
    [SerializeField] float stickInset = 0.25f;
    [SerializeField] float unstickInset = 0.6f;
    IPlayerController player;
    AIEnemy owner;
    Rigidbody rigid;
    Collider stickyColl;

    void Awake () 
    {
        owner = GetComponentInParent<AIEnemy> ();
        rigid = owner.GetComponent<Rigidbody> ();
        stickyColl = GetComponent<Collider> ();
    }
	void OnEnable ()
	{
		stickyColl.enabled = true;
	}
	void OnDisable ()
	{
		if (!stickyColl.enabled)
			onUnstick?.Invoke ();
	}

    public void OnTriggerEnter (Collider hit) 
    {
        var _player = hit.GetComponent<IPlayerController> ();
        if (_player == null) return;
        
        if (_player.RegisterSticky (this))
        {
            player = _player;
            rigid.velocity = Vector3.zero;
            rigid.isKinematic = true;
            stickyColl.enabled = false;

            var dir = (transform.position - player.transform.position).With (y: 0).normalized;
            var position = player.transform.position;
            position += dir * stickInset;
            position.y = owner.transform.position.y;
            owner.transform.position = position;


            owner.transform.SetParent (player.transform);
            onStick?.Invoke ();
        }
    }

    public void OnSpin () => this.RestartCoroutine (OnSpinRoutine (), ref spinRoutine);
    Coroutine spinRoutine;
    IEnumerator OnSpinRoutine () 
    {
        onUnstick?.Invoke ();
        owner.transform.SetParent (null);
        var dir = (transform.position - player.transform.position).With (y: 0).normalized;

        var position = player.transform.position;
        position += dir * unstickInset;
        position.y = owner.transform.position.y;
        owner.transform.position = position;
        
        var popForce = dir * popForceHoriz;
        popForce.y += popForceVert;

        rigid.isKinematic = false;
        rigid.AddForce (dir, ForceMode.VelocityChange);
        
        player = null;
        
        yield return new WaitForSeconds (restickyDelay);
        stickyColl.enabled = true;
    }
}
