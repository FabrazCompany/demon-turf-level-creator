using UnityEngine;
using DG.Tweening;

public class AITrait_StunStars : MonoBehaviour 
{
    Rigidbody ownerRigid;
    SlowMoEntity timeEntity;
    [SerializeField] ParticleSystem starParticle;
    [SerializeField] SpriteRenderer starSprite;
    [SerializeField] float scaleDuration = 0.5f;
    [SerializeField] float scaleTarget = 0.5f;
    [SerializeField] float emissionMax = 20;
    [SerializeField] float emissionMin = 0;
    bool cleaningUp;
    float activeTimer;
    float bufferTimer;
    const float minBuffer = 0.2f; 

    float? duration;
    float? spinMax;

    
    void Awake () 
    {
        ownerRigid = GetComponentInParent<Rigidbody> ();
        timeEntity = ownerRigid.GetComponent<SlowMoEntity> ();
    }
    void OnEnable () 
    {
        starSprite.transform.localScale = Vector3.zero;
        starSprite.transform.DOScale (Vector3.one * scaleTarget, scaleDuration);
        cleaningUp = false;
        var emission = starParticle.emission;
        emission.rateOverTime = emissionMax;
        bufferTimer = minBuffer;
    }

    void Update ()
    {
        if (cleaningUp)
            return;

        var delta = Time.deltaTime * timeEntity?.timeScale ?? Time.deltaTime;
        var pct = 0f;
        if (spinMax != null)
        {
            pct += ownerRigid.angularVelocity.magnitude / spinMax.Value;
        }
        
        if (duration != null)
        {
            activeTimer -= delta;
            pct += activeTimer / duration.Value;

            if (spinMax != null)
                pct *= 0.5f;
        }

        if (bufferTimer > 0)
            bufferTimer -= delta;

        if (pct <= 0 && bufferTimer <= 0)
        {
            cleaningUp = true;
            starSprite.transform.DOScale (Vector3.zero, scaleDuration).OnComplete (() => gameObject.SetActive (false));
        }
        else 
        {
            var emission = starParticle.emission;
            emission.rateOverTime = Mathf.Lerp (emissionMin, emissionMax, pct);
        }            
    }

    public void TriggerFromStun (float _duration) 
    {
        duration = activeTimer = _duration;
		spinMax = null;
        if (!gameObject.activeInHierarchy)
            gameObject.SetActive (true);
    }
    public void TriggerFromSpin (float _maxTorque)
    {
        bufferTimer = minBuffer;
		duration = null;
        spinMax = _maxTorque;
        if (!gameObject.activeInHierarchy)
            gameObject.SetActive (true);
    }   
}