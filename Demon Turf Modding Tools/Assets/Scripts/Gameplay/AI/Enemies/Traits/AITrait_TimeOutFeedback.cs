using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class AITrait_TimeOutFeedback : MonoBehaviour 
{
	[SerializeField] ParticleSystem activeParticle;
	[SerializeField] float uiFadeDuration = 0.5f;
	[SerializeField] Canvas canvas;
	[SerializeField] CanvasGroup wrapperGroup;
	[SerializeField] Image timerImg;


	public void Trigger ()
	{
		canvas.enabled = true;
		gameObject.SetActive (true);
		wrapperGroup.DOKill ();
		wrapperGroup.DOFade (1, uiFadeDuration);
		activeParticle.Play ();
	}

	public void Deactivate () 
	{
		wrapperGroup.DOKill ();
		wrapperGroup.DOFade (0, uiFadeDuration).OnComplete (() =>
		{
			gameObject.SetActive (false);
			canvas.enabled = false;
		});
		activeParticle.Stop ();
	}

	public void UpdateTimer (float pct) => timerImg.fillAmount = pct;

}