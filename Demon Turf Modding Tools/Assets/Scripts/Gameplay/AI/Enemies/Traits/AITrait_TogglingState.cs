﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AITrait_TogglingState : MonoBehaviour
{
    [SerializeField] bool startActive;

    [Header ("Activation")]
    [SerializeField] float preppingActiveDuration = .5f;
    [SerializeField] float activeDuration = 4;
    [SerializeField] UnityEvent onPrepActivation;
    [SerializeField] UnityEvent onActivation;
    
    
    [Header ("Deactivation")]
    [SerializeField] float preppingInactiveDuration = .5f;
    [SerializeField] float inactiveDuration = 4;
    [SerializeField] UnityEvent onPrepDeactivation;
    [SerializeField] UnityEvent onDeactivation;


    [SerializeField, ReadOnly] float activationTimer;
    bool isPrepping;
    bool isActive;

    void Start () 
    {
        isActive = !startActive;
        TriggerChange ();
    }

    void PrepChange () 
    {
        isPrepping = true;

        if (!isActive)
        {
            onPrepActivation?.Invoke ();
            activationTimer = preppingActiveDuration;
        }
        else
        {  
            onPrepDeactivation?.Invoke ();
            activationTimer = preppingInactiveDuration;
        }
    }

    void TriggerChange ()
    {
        isPrepping = false;
        isActive = !isActive;

        if (isActive)
        {
            onActivation?.Invoke ();
            activationTimer = activeDuration;
        }
        else
        {
            onDeactivation?.Invoke ();
            activationTimer = inactiveDuration;
        }
    }

    void Update ()
    {
        activationTimer -= Time.deltaTime;
        if (activationTimer > 0)
            return;

        if (!isPrepping)
        {
            PrepChange ();
        }
        else
        {
            TriggerChange ();
        }
    }
}
