﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Events;
using DG.Tweening;

public class AITrait_CrustaceanHiding : MonoBehaviour//, IResumePathingModifier, IAttackAvailabilityModifier
{
    [SerializeField] Transform shellRoot;
    [SerializeField] float transitionDur;
    [SerializeField] float waitDur = 4;
    [SerializeField] float shakeDur = 0.8f;
    [SerializeField] float shakeMag = 1.5f;
    [SerializeField] int shakeVibrato = 10;
    [SerializeField] UnityEventFloat onHideStart;
    [SerializeField] UnityEvent onHideEnd;
    [SerializeField] UnityEvent onHideCleanup;

    AIEnemy owner;
    Transform shellParent;
    ParentConstraint constraint;
    
    
    float initialBodyYScale;
    Vector3 initialShellParentPos;
    

    [Header ("Detach")]
    [SerializeField] float ignoreDuration;
    
    void Awake () 
    {
        owner = GetComponent<AIEnemy> ();
        initialBodyYScale = transform.localScale.y;
        constraint = shellRoot.GetComponent<ParentConstraint> ();
        shellParent = shellRoot.parent;
        initialShellParentPos = shellParent.localPosition;
    }

    [ContextMenu ("Shrink")]
    public void Shrink () 
    {
        constraint.constraintActive = true;
        shellRoot.SetParent (null);
        // owner.setCurrentScale (initialBodyYScale * 0.1f);
        transform.DOScale (initialBodyYScale * 0.1f, transitionDur).SetLoops (0);
        onHideStart?.Invoke (waitDur + shakeDur);
        Invoke ("ShakeAnticipation", waitDur);
    }

    Tweener shakeTween;
    void ShakeAnticipation () 
    {
        shakeTween = shellParent.DOShakePosition (shakeDur, new Vector3 (1, 0, 1) * shakeMag, shakeVibrato, 90, true).OnComplete (() => 
        {
            Restore ();
        });
    }
    [ContextMenu ("Restore")]
    public void Restore () 
    {
        shakeTween.Kill ();
        shellParent.localPosition = initialShellParentPos;
        onHideEnd?.Invoke ();

        transform.DOScale (initialBodyYScale, transitionDur).OnComplete (() => 
        {            
            constraint.constraintActive = false;
            shellRoot.SetParent (shellParent);
            shellRoot.localPosition = Vector3.zero;
            shellRoot.localScale = Vector3.one;
            // owner.setCurrentScale ();
            onHideCleanup?.Invoke ();
        });
    }
}
