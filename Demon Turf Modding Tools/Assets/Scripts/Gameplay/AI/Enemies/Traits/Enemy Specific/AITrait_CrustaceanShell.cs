﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AITrait_CrustaceanShell : MonoBehaviour, ICombatZoneCleanup
{
    [SerializeField] float ignoreDuration = 0.2f;
    [SerializeField] Transform shellRoot;    
    [SerializeField] Collider[] enemyColliders;
    [SerializeField] Collider[] shellColliders;
    [SerializeField] UnityEvent onDetach;
    AIEnemy owner;
    Rigidbody ownerRigidbody;
    Rigidbody shellRigidbody;
    ReceiveKnockback shellKnockback;
    DeliverRicochetKnockback shellRicochet;

    void Awake () 
    {
        owner = GetComponent<AIEnemy> ();
        ownerRigidbody = GetComponent<Rigidbody> ();
        shellRigidbody = shellRoot.GetComponent<Rigidbody> ();
        shellKnockback = shellRigidbody.GetComponentInChildren<ReceiveKnockback> (true);
        shellRicochet = shellRigidbody.GetComponent<DeliverRicochetKnockback> ();
        SetColliders (true);
    }
    void Start () 
    {
        
        shellKnockback.SetRigid (ownerRigidbody);
        shellRicochet.SetRigid (ownerRigidbody);
        owner.RegisterKnockback (shellKnockback);
    }    

    public void Detach (Vector3 dir) 
    {
        shellRigidbody.GetComponent<ReceiveKnockback> ()?.SetRigid (shellRigidbody);
        shellRigidbody.GetComponent<DeliverRicochetKnockback> ()?.SetRigid (shellRigidbody);
        shellRigidbody.transform.SetParent (null);
        shellRigidbody.isKinematic = false;
        shellRigidbody.AddForce (dir, ForceMode.Impulse);
		shellRicochet.SetRigid (shellRigidbody);
		shellKnockback.SetRigid (shellRigidbody);
        owner.UnregisterKnockback (shellKnockback);
        onDetach?.Invoke ();
        SetColliders (true);
        if (gameObject.activeSelf)  //set colliders to ignore each other after setup from onDetach
        {
            StartCoroutine (this.WaitAndDo (ignoreDuration, () => 
            {
                SetColliders (false);
            }));
        }
    }

    void SetColliders (bool ignore) 
    {
        for (int i = 0; i < enemyColliders.Length; i++) 
        {
            var enemyCollider = enemyColliders[i];
            for (int j = 0; j < shellColliders.Length; j++)
            {
                Physics.IgnoreCollision (shellColliders[j], enemyCollider, ignore);
            }
        }
    }

    void ICombatZoneCleanup.Cleanup () 
    {
        if (shellRigidbody)
            Destroy (shellRigidbody.gameObject);
    }
}
