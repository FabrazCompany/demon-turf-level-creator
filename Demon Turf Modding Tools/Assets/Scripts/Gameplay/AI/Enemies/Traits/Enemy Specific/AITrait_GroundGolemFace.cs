using UnityEngine;

public class AITrait_GroundGolemFace : MonoBehaviour 
{
    // bool is
    [Header ("Faces")]
    [SerializeField] Sprite sprite_idle;
    [SerializeField] Sprite sprite_spotted;
    [SerializeField] Sprite sprite_spin;
    [SerializeField] Sprite sprite_hit;

    [SerializeField] Sprite sprite_attack;
    [SerializeField] Sprite sprite_dead;    
    
    SpriteRenderer face;
    AIEnemy owner;

    bool hasTarget => owner.isAwareOfTarget;
    bool isStabilizing => owner.isStabilizing;  
    bool isSpinning => owner.spinActive;

    void Awake () 
    {
        face = GetComponent<SpriteRenderer> ();
        owner = GetComponentInParent<AIEnemy> ();

        owner.onDestabilized += SetIdle;
        owner.onStabilized += SetIdle;
        owner.onAttackStarted += SetAttack;
        owner.onAttackStopped += SetIdle;
        owner.onKilled += SetDead;
    }
    void Start () 
    {
        owner?.onTargetSighted?.AddListener (SetIdle);
        owner?.onTargetLost?.AddListener (SetIdle);
    }
    void OnDestroy () 
    {
        owner?.onTargetSighted?.RemoveListener (SetIdle);
        owner?.onTargetLost?.RemoveListener (SetIdle);
        owner.onDestabilized -= SetIdle;
        owner.onStabilized -= SetIdle;
        owner.onAttackStarted -= SetAttack;
        owner.onAttackStopped -= SetIdle;
        owner.onKilled -= SetDead;
    }
    void SetIdle (Transform target) => SetIdle ();
    public void SetIdle () {
        if (isSpinning)
            face.sprite = sprite_spin;
        else if (isStabilizing)
            face.sprite = sprite_hit;
        else if (hasTarget) 
            face.sprite = sprite_spotted;
        else 
            face.sprite = sprite_idle;
    }
    public void SetAttack () => face.sprite = sprite_attack;
    public void SetDead (AIEnemy enemy) => face.sprite = sprite_dead;
}