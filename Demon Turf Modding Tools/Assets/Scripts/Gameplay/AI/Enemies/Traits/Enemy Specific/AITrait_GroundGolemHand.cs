﻿using UnityEngine;
using UnityEngine.Events;

public class AITrait_GroundGolemHand : MonoBehaviour
{
    //behaviours
    // - idle
    // - approaching guard
    // - charging toss - charge forward and toss up (fast/medium)
    // - medium-range jab   - freeze and shoot out punch (fast)
    // - close range slam   - freeze and slam down (medium/slow)
    public enum MovementMode
    {
        Attached,
        Launching,
        Halting,
        Stunned,
        Returning,
    }

	[SerializeField] AIEnemy owner;
	[SerializeField] ReceiveKnockback ownerKnockback;
	[SerializeField] float handAnimationSpeed = 1;

    [SerializeField] float maxForce = 50;
    [SerializeField] float punchForce = 30;
    [SerializeField] float stoppingForce = 15;
    [SerializeField] float returnForce = 10;

    [SerializeField] float playerPunchDeflectDuration = 5;
    [SerializeField] float correctAngleSpeed = 50;
	[SerializeField] GameObject despawnParticle;

    [Header ("Hand Physics")]
    [SerializeField] float handMass = 20;
    [SerializeField] float handDrag = 0.5f;
    [SerializeField] float handAngularDrag = 0.05f;
	[SerializeField] float attachedHandForceScalar = 0.5f;
    [SerializeField] UnityEvent onAttach;
    [SerializeField] UnityEvent onDetach;

    [Header ("Toss")]
    [SerializeField] Transform tossDirectionGuide;
    [SerializeField] Transform tossPoint;
    [SerializeField] float tossPointAdjustSpeed;
    [SerializeField] float tossForce = 800;

    [Header ("Separation")]
    [SerializeField] float deflectForce = 10;
    [SerializeField] float deflectDuration = 2;
    [SerializeField] float destabilizeDuration = 3;

	[Header ("Bounce")]
	[SerializeField] Collider bounceTrigger;
	[SerializeField] float bounceDuration = 1;
	[SerializeField] float bounceForce = 20;
	
    Transform wrist;
    Animator wristAnim;

    [SerializeField] Transform hand;
    Rigidbody handRigid;
	HasGravity handGravity;
    PlayerPunchAttackCanceller handCanceller;
    Animator handAnim;
    ReceiveKnockback handKnockback;

    public event System.Action<Collision> onHandCollision;
    public event System.Action<Collider> onHandHitCollision;
    public event System.Action onReattached;

	float lastHaltTime;
    bool isAttached;
    bool isAligned;
    public MovementMode movementMode { get; private set; }
    float stunTimer;
	public bool getIsAttached => isAttached;
    const float proximityCutoff = 0.1f;

    void Awake () 
    {
        wrist = hand.parent;
        wristAnim = GetComponent<Animator> ();
        handAnim = hand.GetComponent<Animator> ();
        handKnockback = hand.GetComponent<ReceiveKnockback> ();

        handRigid = hand.GetComponent<Rigidbody> ();
		handGravity = handRigid.GetComponent<HasGravity> ();
        handCanceller = hand.GetComponentInChildren<PlayerPunchAttackCanceller> ();
            
        movementMode = MovementMode.Attached;
        isAligned = true;
    }
    void Start ()
    {
		handAnim.SetFloat ("handSpeed", handAnimationSpeed);
        Attach (true);
    }
	void OnDestroy ()
	{
		if (isAttached) return;
		if (hand)
		{
			TrashMan.spawn (despawnParticle, hand.transform.position);
			Destroy (hand.gameObject);
		}
	}
    
    public void HandCollision (Collision collision) 
    {
        if (!collision.collider.GetComponent<ReceiveKnockback> ())
            onHandCollision?.Invoke (collision);
    }
    public void KnockbackCollision (Collider hit) => onHandHitCollision?.Invoke (hit);
    public void KnockbackReceived (Vector3 hitForce)
    {
		if (isAttached) 
		{
			ownerKnockback.ApplyKnockbackForce (hitForce, attachedHandForceScalar);
		}
		else 
		{
			switch (movementMode)
			{
				case MovementMode.Launching:
				case MovementMode.Returning:
				case MovementMode.Stunned:
					TriggerStun (playerPunchDeflectDuration);
					break;
			}
		} 
    }
    public void ApplyHandRigidbody (bool active) 
    {
        if (active)
        {
            handGravity.enabled = false;
			SetHandKinematic (true);
        }
        else 
        {
            if (!isAttached)
                return;

			SetHandKinematic (true);
        }
    }
    public void AllowHandDetach (bool active) 
    {
        if (active)
        {
            owner.onDestabilized += OnHit;
        }
        else 
        {
            owner.onDestabilized -= OnHit;
        }
    }

    void OnHit () 
    {
        owner.onDestabilized -= OnHit;
        Attach (false);
        TriggerStun (destabilizeDuration);
    }
#region PUNCH
    public void Launch (float burst) 
    {
        Attach (false);
        Align (false);
		SetHandKinematic (false);
        handRigid.AddForce (handRigid.transform.forward * burst, ForceMode.VelocityChange);
        movementMode = MovementMode.Launching;
    }
    public void Halt () 
	{
		lastHaltTime = Time.time;
		movementMode = MovementMode.Halting;
	}
    public void Return () 
    {
        handAnim.SetBool ("fist", false);
        wristAnim.SetBool ("fist", false);
        if (handGravity)
            handGravity.enabled = false;
		
		SetHandKinematic (true);
        movementMode = MovementMode.Returning;
    }
    public void FreezeLinearMomentum () 
    {
        if (handRigid) 
            handRigid.velocity = Vector3.zero;
    }
    public void FreezeAngularMomentum () 
    {
        if (handRigid) 
            handRigid.angularVelocity = Vector3.zero;
    } 
    public void TriggerStun (Collision collision) 
    {
        var contact = collision.contacts[0];
        TriggerStun (contact.point, contact.normal * deflectForce);
    }
    void TriggerStun (Vector3 position, Vector3 force) 
    {
        if (handRigid)
            handRigid.AddForceAtPosition (force, position, ForceMode.VelocityChange);
        TriggerStun (deflectDuration);
    }
	public void OnBounce ()
	{
		Attach (false);
		if (handRigid)
            handRigid.AddForceAtPosition ((transform.forward + -transform.up) * 0.5f, bounceTrigger.transform.position, ForceMode.VelocityChange);
        TriggerStun (bounceDuration);
	}
    void TriggerStun (float duration)
    {
        if (handGravity)
            handGravity.enabled = true;
        stunTimer = Time.time + duration;
        movementMode = MovementMode.Stunned;
        isAligned = false;
    }
#endregion
#region Toss
    Collider tossTarget;
#if !DT_EXPORT && !DT_MOD
    KinematicCharacterController.KinematicCharacterMotor tossTargetMotor;
#endif
    public void OnTossTargetAcquired (Collider hit)
    {
        var player = hit.GetComponent<IPlayerController> ();
        if (player != null)
        {
            tossTarget = hit;
            player.TriggerHold ();
        }
#if !DT_EXPORT && !DT_MOD
        tossTargetMotor = hit.GetComponent<KinematicCharacterController.KinematicCharacterMotor> ();
#endif
    }
    public void OnTossLaunch () 
    {
        var player = tossTarget.GetComponent<IPlayerController> ();
        if (player != null)
        {
			player.ClearHold ();
            player.AddKnockback (tossDirectionGuide.forward * tossForce);
        }
        tossTarget = null;
#if !DT_EXPORT && !DT_MOD
        tossTargetMotor = null;
#endif
    }

#endregion
    void Attach (bool attach) 
    {
        isAttached = attach;
        if (isAttached)
        {
            ApplyHandRigidbody (false);
			SetHandKinematic (true);
			handKnockback.SetExternalKnockback (ownerKnockback);
			handGravity.SetGravityScale (0);
			bounceTrigger.enabled = true;
            hand.SetParent (wrist);
            hand.localPosition = Vector3.zero;
			handRigid.velocity = Vector3.zero;
			handRigid.angularVelocity = Vector3.zero;
            handKnockback.SetRigid (owner.getRigid);
            owner.RegisterKnockback (handKnockback);
            StartCoroutine (this.WaitFrameAndDo (() => onAttach?.Invoke ()));
        }
        else 
        {
            ApplyHandRigidbody (true);
			handGravity.SetGravityScale (1);
			handKnockback.SetExternalKnockback (null);
			bounceTrigger.enabled = false;
            hand.SetParent (null);
            SetHandKinematic (false);
            handKnockback.SetRigid (handRigid);
            owner.UnregisterKnockback (handKnockback);
            onDetach?.Invoke ();
            wristAnim.SetBool ("attached", false);
        }   
    }

	void SetHandKinematic (bool isKinematic)
	{
		handRigid.isKinematic = isKinematic;
	}
	

    void Align (bool aligned) 
    {
        if (aligned)
        {
            handAnim.SetBool ("fist", false);
            wristAnim.SetBool ("fist", false);
        }
        isAligned = aligned;
    }

    void Update () 
    {
		if (isAttached)
		{
			hand.localPosition = Vector3.zero;
			hand.localEulerAngles = Vector3.zero;
		}
        switch (movementMode)
        {
            case MovementMode.Attached:
                break;
            case MovementMode.Halting:
				// if (Time.time - lastHaltTime > .5f)
				// 	Return ();
                break;
            case MovementMode.Launching:
                break;
            case MovementMode.Returning:
                var wasSeparate = !isAttached || !isAligned;
                if (wasSeparate)
                {
                    if (!isAttached && Vector3.SqrMagnitude (wrist.transform.position - hand.position) <= proximityCutoff * proximityCutoff)
                        Attach (true);
                    
                    if (!isAligned && Quaternion.Angle (wrist.transform.rotation, hand.rotation) <= 0) 
                        Align (true);

                    if (wasSeparate && isAttached && isAligned) 
                    {
                        movementMode = MovementMode.Attached;
                        wristAnim.SetBool ("attached", true);
                        handAnim.SetBool ("fist", false);
                        wristAnim.SetBool ("fist", false);
                        onReattached?.Invoke ();
                    }
                }
                break;
            case MovementMode.Stunned:
                if (stunTimer <= Time.time)
                {
                    movementMode = MovementMode.Returning;
					handRigid.isKinematic = true;
                    if (handGravity)
                        handGravity.enabled = false;
                }
                break;
        }
    }
    void FixedUpdate () 
    {      
        float dt = Time.deltaTime;  
        switch (movementMode)
        {
            case MovementMode.Attached:
                if (Quaternion.Angle (hand.rotation, wrist.transform.rotation) > 0) 
                    hand.rotation = Quaternion.RotateTowards (hand.rotation, wrist.transform.rotation, correctAngleSpeed * dt);
#if !DT_EXPORT && !DT_MOD
                if (tossTargetMotor)
                {
                    tossTargetMotor.SetPosition (Vector3.MoveTowards (tossTargetMotor.transform.position, tossPoint.position, tossPointAdjustSpeed * dt));
                }
#endif
                break;
            case MovementMode.Launching:
                handRigid.AddForce (handRigid.transform.forward * punchForce, ForceMode.Acceleration);
                if (handRigid.velocity.sqrMagnitude >= maxForce * maxForce)
                    handRigid.velocity = handRigid.velocity.normalized * maxForce;
                break;
            case MovementMode.Halting:
                handRigid.velocity = Vector3.MoveTowards (handRigid.velocity, Vector3.zero, stoppingForce * dt);
                break;
            case MovementMode.Returning:
                if (!isAttached)
                    handRigid.MovePosition (Vector3.MoveTowards (handRigid.position, wrist.transform.position, returnForce * dt));
                if (!isAligned)
                {
                    if (handRigid)
                        handRigid.MoveRotation (Quaternion.RotateTowards (handRigid.rotation, wrist.transform.rotation, correctAngleSpeed * dt));
                    else 
                        hand.rotation = Quaternion.RotateTowards (hand.rotation, wrist.transform.rotation, correctAngleSpeed * dt);
                }
                    
                break;
            case MovementMode.Stunned:
                break;
        }
    }
}
