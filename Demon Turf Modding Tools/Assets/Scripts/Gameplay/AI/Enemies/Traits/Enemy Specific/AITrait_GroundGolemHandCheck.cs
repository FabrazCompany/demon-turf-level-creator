﻿using UnityEngine;
using UnityEngine.Events;

public class AITrait_GroundGolemHandCheck : MonoBehaviour
{   
    [SerializeField] AITrait_GroundGolemHand owner;
    [SerializeField] UnityEvent onReattached;
    [SerializeField] UnityEventCollision onCollision;
    [SerializeField] UnityEventCollider onHit;
    [SerializeField] bool handStateCheck;
    [SerializeField, Conditional ("handStateCheck")] bool notifyOnSameState;
    [SerializeField, Conditional ("handStateCheck")] AITrait_GroundGolemHand.MovementMode movementMode;
    [SerializeField, Conditional ("handStateCheck")] UnityEvent onStateCheckTriggered;
    

    void OnReattached () => onReattached?.Invoke ();
    void OnCollision (Collision collision) => onCollision?.Invoke (collision);
    void OnHit (Collider hit) => onHit?.Invoke (hit);
    // void Awake () 
    // {
    //     owner = GetComponentInParent<AITrait_GroundGolemHand> ();
    // }

    void OnEnable () 
    {
        if (owner)
        {
            owner.onReattached += OnReattached;
            owner.onHandCollision += OnCollision;
            owner.onHandHitCollision += OnHit;
        }
		if (owner.getIsAttached)
		{
			OnReattached ();
		}
    }
    void OnDisable () 
    {
        if (owner)
        {
            owner.onReattached -= OnReattached;
            owner.onHandCollision -= OnCollision;
            owner.onHandHitCollision -= OnHit;
        }
    }
    void Update () 
    {
        if (handStateCheck)
        {
            var triggered = owner.movementMode == movementMode;
            if (!notifyOnSameState)
                triggered = !triggered;
            
            if (triggered)
                onStateCheckTriggered?.Invoke ();
        }
    }
}
