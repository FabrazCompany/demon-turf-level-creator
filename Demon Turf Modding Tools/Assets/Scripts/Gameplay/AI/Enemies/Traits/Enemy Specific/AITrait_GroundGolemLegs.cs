using UnityEngine;
using System.Collections;

public class AITrait_GroundGolemLegs : MonoBehaviour
{

    [SerializeField] Animator anim;
    [SerializeField] AIEnemy owner;
    [SerializeField] Rigidbody rigid;
	[SerializeField] ModStub_AIPath pathing;
	[SerializeField] float isMovingVelocityThreshold = .7f;
	[SerializeField] float isMovingAngularVelocityThreshold = 2;
	Transform tr;
	void OnValidate ()
	{
		if (!anim)
	        anim = GetComponent<Animator> ();
		if (!owner)
		    owner = GetComponentInParent<AIEnemy> ();
		if (owner)
		{
			if (!rigid)
				rigid = owner.GetComponent<Rigidbody> ();
			if (!pathing)
				pathing = owner.GetComponentInParent<ModStub_AIPath> ();
		}		
	}
    void Awake () 
    {
		tr = transform;
		lastForward = tr.forward;
        owner.onLeftGround += LeftGround;   
    }
    void Start () 
    {
        owner.onTargetSighted?.AddListener (TargetSighted);
        owner.onTargetLost?.AddListener (TargetLost);
    }
    void OnEnable () 
    {
        SetUrgencyScale (1);
		StartCoroutine (MovingLegsRoutine ());
    }

    void OnDestroy () 
    {
        if (owner) 
        {
            owner.onLeftGround -= LeftGround;
            owner.onTargetSighted?.RemoveListener (TargetSighted);
            owner.onTargetLost?.RemoveListener (TargetLost);
        }
    }

    void LeftGround () 
    {
        anim.SetBool ("isGrounded", owner.isGrounded);
        this.RestartCoroutine (JumpRoutine (), ref jumpRoutine);
    }
    void TargetSighted (Transform target) => anim.SetFloat ("urgency", 1);
    void TargetLost () => anim.SetFloat ("urgency", 0);

    public void SetUrgencyScale (float value) => anim.SetFloat ("urgencyScale", value);
	
	Vector3 lastForward;
	
	IEnumerator MovingLegsRoutine ()
	{
		var waitPeriod = new WaitForSeconds (.1f + (0.15f * Random.value));
		while (true)
		{
			float curTurnVelocity = Vector3.Angle (lastForward, tr.forward);
			lastForward = tr.forward;
			var isMoving = !pathing.isStopped 
							&& pathing.canMove 
							&& (pathing.velocity.sqrMagnitude >= isMovingVelocityThreshold || curTurnVelocity >= isMovingAngularVelocityThreshold);
			anim.SetBool ("isMovingLegs", isMoving);
			yield return waitPeriod;
		}
	}

    Coroutine jumpRoutine;
    IEnumerator JumpRoutine () 
    {
        while (!owner.isGrounded)
        {
            anim.SetFloat ("airVelocity", Mathf.Sign (rigid.velocity.y));
            yield return null;
        }
        anim.SetBool ("isGrounded", owner.isGrounded);
    }
}