using UnityEngine;

public class AITrait_GroundGolemLight : MonoBehaviour
{
    [Header ("Lights")]
    [SerializeField] GameObject light_idle;
    [SerializeField] GameObject light_spotted;
    [SerializeField] GameObject light_attacking;

    AIEnemy owner;

    void Awake () 
    {
        owner = GetComponentInParent<AIEnemy> ();

        owner.onDestabilized += ClearLights;
        owner.onStabilized += RefreshLight;
        owner.onAttackStarted += SetLight_Attacking;
        owner.onAttackStopped += RefreshLight;
        
    }
    void Start () 
    {
        owner.onTargetSighted?.AddListener (SetLight_Spotted);
        owner.onTargetLost?.AddListener (SetLight_Idle);
    }
    void OnDestroy () 
    {
        if (owner)
        {
            owner.onDestabilized -= ClearLights;
            owner.onStabilized -= RefreshLight;
            owner.onAttackStarted -= SetLight_Attacking;
            owner.onAttackStopped -= RefreshLight;
            owner.onTargetSighted?.RemoveListener (SetLight_Spotted);
            owner.onTargetLost?.RemoveListener (SetLight_Idle);
        }
    }
    public void RefreshLight () 
    {
        if (owner.isAwareOfTarget)
            SetLight_Spotted ();
        else 
            SetLight_Idle ();
    }
    void ClearLights () 
    {
        light_idle.SetActive (false);
        light_attacking.SetActive (false);
        light_spotted.SetActive (false);
    }
    void SetLight_Idle () 
    {
        if (!light_idle.activeInHierarchy)
        {
            light_idle.SetActive (true);
            light_attacking.SetActive (false);
            light_spotted.SetActive (false);
        }
    }
    void SetLight_Spotted (Transform target) => SetLight_Spotted ();
    void SetLight_Spotted () 
    {
        if (!light_spotted.activeInHierarchy)
        {
            light_spotted.SetActive (true);
            light_attacking.SetActive (false);
            light_idle.SetActive (false);
        }
    }
    void SetLight_Attacking () 
    {
        if (!light_attacking.activeInHierarchy)
        {
            light_attacking.SetActive (true);
            light_idle.SetActive (false);
            light_spotted.SetActive (false);
        }
    }
}