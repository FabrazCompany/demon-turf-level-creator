using UnityEngine;

public interface IRefreshOnDeath 
{
    Transform transform { get; }
}