﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent (typeof (SubmersionListener))]
public class AITrait_Buoyant : MonoBehaviour //: AITrait_Aquatic, ISwimmable
{
    [SerializeField] float buoyancySpeed = 1;
    [SerializeField] ParticleSystem[] managedWaterProjectors;
    Transform tr;
    Rigidbody rigid;
    SubmersionListener submersion;
    SwimmableVolume swimmingVolume;
    Vector3 waveOffsetPosition;

    void Awake ()
    {
        tr = transform;
        rigid = GetComponent<Rigidbody> ();
        submersion = GetComponent<SubmersionListener> ();
    }
    protected void Update ()
    {
        var pos = tr.position;
        pos -= submersion.getPrevWaveOffset;
        pos.y = submersion.getVolumeTop;
        waveOffsetPosition = Vector3.MoveTowards (tr.position, pos + submersion.getWaveOffset, buoyancySpeed * Time.deltaTime);
    }
    void FixedUpdate () 
    {
        if (submersion.getUnderLiquid)
        {   
            rigid.MovePosition (waveOffsetPosition);
        }
    }
}
