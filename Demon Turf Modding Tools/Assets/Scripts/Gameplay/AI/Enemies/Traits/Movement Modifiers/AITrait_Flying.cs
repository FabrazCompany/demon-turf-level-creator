using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using Malee;

public class AITrait_Flying : MonoBehaviour, IResumePathingModifier, IAttackAvailabilityModifier 
{
    [Serializable]
    enum FlightState 
    {
        Stable,         //at resting height, moves in sine bob motion
        Stabilizing,    //outside of resting height, moving to stable point
        ReturningToGround,        
    }
	
    [SerializeField] AIEnemy owner;
    [SerializeField] ModStub_AIPath pathing;
    [SerializeField] Rigidbody rigid;
	[SerializeField] SlowMoEntity slowMo;
	[SerializeField] HasGravity gravity;
	
    [SerializeField, ReadOnly] FlightState currentFlightState;
    [Header ("Cancel Triggers")]
    [SerializeField, Reorderable] CancelConditionList cancelConditions;
    [SerializeField] bool cancelDuringAttack = true;

    [Header ("Idle Flight")]
    [SerializeField] float restingHeight = 2;
    [SerializeField] float restabilizeDistance = 1;
    [SerializeField] float destabilizeDistance = 1;
    [SerializeField] float stabilizeSpeed = 3;
    [SerializeField] float minFallDistance; 

    [Header ("Bobbing")]
    [SerializeField] float bobSpeed = .1f;
    [SerializeField] float bobMagnitude = .25f;
    [SerializeField] float bobMagnitudeVariance = .2f;

    [SerializeField] UnityEvent onStabilize;
    [SerializeField] UnityEvent onDestabilize;
    [SerializeField] UnityEvent onReturnToGround;

    Vector3 lastGroundPosition;
    float targetRestingHeight;
    float targetBobHeight; 
    const float destinationReachedDist = 0.01f;
    float ignoreTimer;

    int bobDir = 1;   

	bool beingPulled;

	bool IResumePathingModifier.ShouldSkipCheck () 
	{
		return !gameObject.activeInHierarchy;
	}
    bool IResumePathingModifier.ShouldResumePathing () 
    {
        return currentFlightState == FlightState.Stable;
    }
    bool IResumePathingModifier.ShouldHaltPathing ()
    {
        return currentFlightState != FlightState.Stable;
    } 
    bool IAttackAvailabilityModifier.CanAttack ()
    {
        return currentFlightState == FlightState.Stable && !beingPulled;
    }
    void OnValidate ()
	{
		if (!owner)
	        owner = GetComponent<AIEnemy> ();
		if (!pathing)
	        pathing = GetComponent<ModStub_AIPath> ();
		if (!rigid)
	        rigid = GetComponent<Rigidbody> ();
		if (!gravity)
			gravity = GetComponent<HasGravity> ();
		if (!slowMo)
			slowMo = GetComponent<SlowMoEntity> ();
	}
    void Awake () 
    {
        lastGroundPosition = owner.GetGroundPoint (minFallDistance);
        if (lastGroundPosition != Vector3.positiveInfinity) 
        {   
            SetBobVals ();
        } 
        else 
        {
            Debug.LogWarning ("Flyer is starting far above ground!");
        }

        owner.onStabilized += OnStabilized;
        owner.onDestabilized += OnDestabilized;
    }
    void OnEnable () 
    {
		gravity.enabled = false;
		rigid.useGravity = false;
		beingPulled = false;
		if (owner)
		{
			CheckIsAboveGround ();
			SetFlightState (FlightState.Stabilizing);
		}
		
		if (gameObject.activeInHierarchy)
	        this.RestartCoroutine (GroundCheckRoutine (), ref groundCheckRoutine);
    }
    void OnDisable () 
    {
		gravity.enabled = true;
		if (!gameObject.activeSelf)
		{
			currentFlightState = FlightState.Stable;
			enabled = true;
		}
    }
	
    void OnDestroy () 
    {
        owner.onStabilized -= OnStabilized;
        owner.onDestabilized -= OnDestabilized;
    }
    void OnStabilized () 
    {
        enabled = true;
    }
    void OnDestabilized () 
    {
        enabled = false;
        SetFlightState (FlightState.Stabilizing);
    }
	public void SetPulledState (bool state) => beingPulled = state; 
    public void EnableFlight () 
    {
        if (owner.isStabilizing)
            return;
        enabled = true;
    }
    public void DisableFlight () 
    {
        enabled = false;
    }
    void SetFlightState (FlightState state)
    {
        if (currentFlightState == state)
            return;
        
        switch (state)
        {
            case FlightState.Stable:
                pathing.Teleport (transform.position, true); 
                SetBobVals();
                if (currentFlightState == FlightState.ReturningToGround)
                {
                    pathing.isStopped = false;
                    pathing.canMove = true;
                }
                onStabilize?.Invoke ();
                break;
            case FlightState.Stabilizing:
                if (currentFlightState == FlightState.ReturningToGround)
                {
                    pathing.isStopped = false;
                    pathing.canMove = true;
                }
                onDestabilize?.Invoke ();
                break;
            case FlightState.ReturningToGround:
#if !DT_MOD
                lastGroundPosition = AstarPath.active?.GetNearest(lastGroundPosition).position + (Vector3.up * restingHeight) ?? lastGroundPosition;
#endif
                pathing.isStopped = true;
                pathing.canMove = false;
                onReturnToGround?.Invoke ();
                break;
        }
        currentFlightState = state;
    }
    void Update ()
    {
        //transitions
        switch (currentFlightState)
        {
            case FlightState.Stable:
                if (Mathf.Abs(transform.position.y - targetRestingHeight) > destabilizeDistance)
                {
                    SetFlightState (FlightState.Stabilizing);
                }
                break;
            case FlightState.Stabilizing:
                if (Mathf.Abs(transform.position.y - targetRestingHeight) <= restabilizeDistance)
                {
                    SetFlightState (FlightState.Stable);
                }
                break;
        }

        //update
        switch (currentFlightState)
        {

            case FlightState.Stable:
                var flipBob = (bobDir > 0 && transform.position.y >= targetBobHeight) || (bobDir < 0 && transform.position.y <= targetBobHeight);
                if (flipBob)
                {
                    bobDir *= -1;
                    var magnitude = bobMagnitude + UnityEngine.Random.Range(0, bobMagnitudeVariance);
                    targetBobHeight = targetRestingHeight + (magnitude * bobDir);
                }
                break;
            case FlightState.Stabilizing:
                break;
        }
    }

    float groundCheckRate = 1f;
    Coroutine groundCheckRoutine;
    IEnumerator GroundCheckRoutine () 
    {
        while (true)
        {
            yield return new WaitForSeconds (groundCheckRate);
            yield return new WaitForFixedUpdate ();
            CheckIsAboveGround ();
        }
    }
    void CheckIsAboveGround()
    {
        var currentGroundPosition = owner.GetGroundPoint(minFallDistance);
        if (!currentGroundPosition.IsInfinity() || currentFlightState == FlightState.ReturningToGround)     //is above ground
        {
            if (currentFlightState == FlightState.ReturningToGround)
            {
                if (Vector3.SqrMagnitude(transform.position - lastGroundPosition) <= destinationReachedDist * destinationReachedDist)
                {
                    SetFlightState (FlightState.Stabilizing);
                }
            }
            else
            {
                lastGroundPosition = currentGroundPosition;
                targetRestingHeight = lastGroundPosition.y + restingHeight;
                ignoreTimer = 0;
            }
        }
        else
        {
            ignoreTimer += (Time.deltaTime * slowMo.timeScale);
            if (currentFlightState != FlightState.ReturningToGround)
            {
                SetFlightState (FlightState.ReturningToGround);
            }
        }
    }

    void FixedUpdate () 
    {
        if (cancelDuringAttack && owner.isAttacking)
            return;

        var pos = owner.transform.position;
        var rot = owner.transform.rotation;
        if (currentFlightState == FlightState.ReturningToGround) 
        {
            pos = Vector3.MoveTowards (pos, lastGroundPosition, stabilizeSpeed * Time.fixedDeltaTime * slowMo.timeScale);
            rigid.MovePosition (pos);
        } 
        else
        {
			var deltaTime = Time.fixedDeltaTime * slowMo.timeScale;
            if (currentFlightState == FlightState.Stable)
            {
                pos.y = Mathf.MoveTowards (pos.y, targetBobHeight, bobSpeed * deltaTime);
            }
            else if (currentFlightState == FlightState.Stabilizing)
            {
                pos.y = Mathf.MoveTowards (pos.y, targetRestingHeight, stabilizeSpeed * deltaTime);
            }
            if (pathing.canMove && pathing.enabled)
            {
                var delta = pos - transform.position;
                pathing.Move (delta);
            }
            else 
                rigid.MovePosition (pos);
        }        
    }

    void SetBobVals () 
    {
        if (lastGroundPosition == Vector3.positiveInfinity)
            return;
        targetRestingHeight = lastGroundPosition.y + restingHeight;
        bobDir = 1;            
        targetBobHeight = targetRestingHeight + (bobMagnitude * bobDir);
    }

    void OnDrawGizmos ()
    {
        Gizmos.color = Color.blue;
        if (currentFlightState == FlightState.ReturningToGround)
        {
            Gizmos.DrawSphere (lastGroundPosition, 0.25f);
        }
    }
}