using UnityEngine;

public class AITrait_Hopping : MonoBehaviour
{

    [Header ("Idle Values")]
    [SerializeField] float hopForceIdle = 3;
    [SerializeField] float hopForceIdleVariance = 0;
    [SerializeField] float repeatDelayIdle = 1.5f;
    [Header ("Idle Values")]
    [SerializeField] float hopForceActive = 3;
    [SerializeField] float hopForceActiveVariance = 0; 
    [SerializeField] float repeatDelayActive = 1.5f;

    [SerializeField] UnityEngine.Events.UnityEvent onHop;

    float triggerTime;
    AIEnemy enemy;
    Rigidbody rigid;

    float hopForce => (bool)enemy?.isAwareOfTarget ? hopForceActive : hopForceIdle;
    float hopVariance => (bool)enemy?.isAwareOfTarget ? hopForceActiveVariance : hopForceIdleVariance;
    float repeatDelay => (bool)enemy?.isAwareOfTarget ? repeatDelayActive : repeatDelayIdle;

    void Awake () 
    {
        enemy = GetComponentInParent<AIEnemy> ();
        rigid = enemy.GetComponent<Rigidbody> ();
    }
    public void Referesh () 
    {
        triggerTime = Time.time + repeatDelay;
        enabled = true;
    }

    void Update () 
    {
        if (triggerTime < Time.time 
            && enemy.isGrounded 
            && !enemy.isStabilizing
            && !enemy.isAttacking) 
        {
            triggerTime = Time.time + repeatDelay;
            rigid.AddForce (Vector3.up * (hopForce + Random.Range (0, hopVariance)), ForceMode.VelocityChange);
            onHop?.Invoke ();
        }
    }
}