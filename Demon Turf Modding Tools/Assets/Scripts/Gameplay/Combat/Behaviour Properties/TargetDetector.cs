﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent (typeof (SphereCollider))]
public class TargetDetector : MonoBehaviour
{
    [Header ("Engaging")]
    [SerializeField] LayerMask blockerLayer;
    [SerializeField] float facingAngle;
    [SerializeField] float engageDistance;
    public UnityEventTransform onTargetSighted;

    [Header ("Disengaging")]
    [SerializeField] float lineOfSightLostDuration = 5;
    [SerializeField] float lineOfSightOffset = 0.25f;

    public UnityEvent onTargetLost; 
    SphereCollider triggerArea;
    [SerializeField, ReadOnly] Transform activeTarget;
    float lastTimeSighted;

	public float getEngageDistance => engageDistance;

    void OnTriggerStay (Collider hit) 
    {
        if (!hit.attachedRigidbody)
            return;
        
        var targetable = hit.attachedRigidbody.GetComponent<ITargetable> ();
        if (targetable == null)
            return;

        if (!activeTarget)
        {
            if (CanDetectTarget (targetable.getTarget))
            {
                activeTarget = targetable.getTarget;
                onTargetSighted?.Invoke (targetable.getTarget);
            }
        } 
        else 
        {
            if (HasLostTarget (activeTarget))
            {
                activeTarget = null;
                onTargetLost?.Invoke ();
            }
        }
    }
    void OnTriggerExit (Collider hit)
    {
        if (activeTarget && hit?.attachedRigidbody?.transform == activeTarget)
        {
            activeTarget = null;
            onTargetLost?.Invoke ();
        }
    }
    bool CanDetectTarget (Transform target) {
        if (!enabled)
            return false;
        if (!target)
            return false;
        if (Vector3.SqrMagnitude (transform.position - target.position) > engageDistance * engageDistance)
            return false;
        if (!IsFacingTarget (target))
            return false;
        if (!HasLineOfSight (target))
            return false;
        return true;
    }
    bool HasLineOfSight (Transform target) {
        if (!target)
            return false;
        if (lineOfSightLostDuration > 0 && !IsFacingTarget (target))
            return false;
        var eyePoint = transform.position + transform.up * lineOfSightOffset;
        return !Physics.Linecast (eyePoint, target.position, blockerLayer);
    }
    bool IsFacingTarget (Transform target) {
        if (!target)
            return false;
        return Vector3.Angle (transform.forward, (target.position - transform.position).normalized) < facingAngle;
    }

    bool HasLostTarget (Transform target) {
        if (!HasLineOfSight (target)) {                
            if (lastTimeSighted + lineOfSightLostDuration < Time.time) {
                return true;
            }
        } else {
            lastTimeSighted = Time.time;
        }
        return false;
    }

    public void OnDrawGizmosSelected () {
        Gizmos.color = Color.yellow;
        Gizmos.matrix = transform.localToWorldMatrix;

        Gizmos.DrawWireSphere (Vector3.zero, engageDistance);

        Gizmos.DrawRay (Vector3.zero, Quaternion.AngleAxis (facingAngle, Vector3.right) * Vector3.forward * engageDistance);
        Gizmos.DrawRay (Vector3.zero, Quaternion.AngleAxis (facingAngle, -Vector3.right) * Vector3.forward * engageDistance);
        Gizmos.DrawRay (Vector3.zero, Quaternion.AngleAxis (facingAngle, Vector3.up) * Vector3.forward * engageDistance);
        Gizmos.DrawRay (Vector3.zero, Quaternion.AngleAxis (facingAngle, -Vector3.up) * Vector3.forward * engageDistance);



        Gizmos.color = Color.blue;
        if (triggerArea == null)
            triggerArea = GetComponent<SphereCollider> ();
        Gizmos.DrawWireSphere (Vector3.zero, triggerArea.radius);
    }
}

public interface ITargetable 
{
    Transform getTarget { get; }
}