﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TargetRotationTracking : MonoBehaviour
{
	[SerializeField] bool useLocal;
    [SerializeField] bool lockX;
    [SerializeField, Conditional ("lockX", hide: true)] RotationLimit xLimit;
    [SerializeField] bool lockY;
    [SerializeField, Conditional ("lockY", hide: true)] RotationLimit yLimit;

    [SerializeField] float turnSpeed = 5f;
    [SerializeField, Range (0, 360)] float minimumValidAngle = 5f;
    Transform target;
    bool facingTarget;

    Quaternion originalRotation;
    Vector3 originalAngles;
	float speed;
    

    [SerializeField] UnityEvent onStartedFacingTarget;
    [SerializeField] UnityEvent onStoppedFacingTarget;
    void Awake () 
    {
		speed = turnSpeed;
		if (useLocal)
		{
			originalRotation = transform.localRotation;
			originalAngles = transform.localEulerAngles;
		}
		else 
		{
			originalRotation = transform.rotation;
			originalAngles = transform.eulerAngles;
		}
        
    }
    void Update()
    {
        if (!target) {
			if (useLocal)
			{
				transform.localRotation = Quaternion.RotateTowards (transform.localRotation, originalRotation, speed * Time.deltaTime);    
				if (transform.localRotation == originalRotation)
				{
					enabled = false;
				}
			}
			else 
			{
				transform.rotation = Quaternion.RotateTowards (transform.rotation, originalRotation, speed * Time.deltaTime);    
				if (transform.rotation == originalRotation)
				{
					enabled = false;
				}
			}
            
            return;
        }
        
		Vector3 heading = (target.position - transform.position).normalized;
		Quaternion targetRotation = Quaternion.LookRotation (heading, Vector3.up);
		Quaternion endRotation = Quaternion.RotateTowards (transform.rotation, targetRotation, speed * Time.deltaTime);
		
		if (useLocal)
		{	
			Vector3 endRotationEuler = (Quaternion.Inverse(transform.parent.rotation) * endRotation).eulerAngles;
			if (lockX)
				endRotationEuler.x = transform.localEulerAngles.x;
			else if (xLimit.hasLimit)
				endRotationEuler.x = FzMath.ClampAngle (endRotationEuler.x, originalAngles.x + xLimit.limitMin, originalAngles.x + xLimit.limitMax);

			if (lockY)
				endRotationEuler.y = transform.localEulerAngles.y;
			else if (yLimit.hasLimit)
				endRotationEuler.y = FzMath.ClampAngle (endRotationEuler.y, originalAngles.y + yLimit.limitMin, originalAngles.y + yLimit.limitMax);        
			transform.localEulerAngles = endRotationEuler;
		}
		else 
		{
			Vector3 endRotationEuler = endRotation.eulerAngles;
			if (lockX)
				endRotationEuler.x = transform.eulerAngles.x;
			else if (xLimit.hasLimit)
				endRotationEuler.x = FzMath.ClampAngle (endRotationEuler.x, originalAngles.x + xLimit.limitMin, originalAngles.x + xLimit.limitMax);

			if (lockY)
				endRotationEuler.y = transform.eulerAngles.y;
			else if (yLimit.hasLimit)
				endRotationEuler.y = FzMath.ClampAngle (endRotationEuler.y, originalAngles.y + yLimit.limitMin, originalAngles.y + yLimit.limitMax);        
			transform.eulerAngles = endRotationEuler;
		}
        

        var currentlyFacingTarget = IsFacingTarget (target);
        if (!facingTarget && currentlyFacingTarget)
        {
            onStartedFacingTarget?.Invoke ();
        }
        if (facingTarget && !currentlyFacingTarget)
        {
            onStoppedFacingTarget?.Invoke ();
        }
        facingTarget = currentlyFacingTarget;
    }
    bool IsFacingTarget (Transform target) {
        if (!target)
            return false;
        return Vector3.Angle (transform.forward, (target.position - transform.position).normalized) < minimumValidAngle;
    }
    public void AssignTarget (Collider newTarget) => AssignTarget (newTarget.transform);
    public void AssignTarget (Transform newTarget)
    {
        target = newTarget;
		if (facingTarget)
			onStoppedFacingTarget?.Invoke ();
        facingTarget = false;
        enabled = true;
    }
    public void ClearTarget () 
    {
        target = null;
		if (facingTarget)
			onStoppedFacingTarget?.Invoke ();
    }
	public void SetSpeed (float _speed) => speed = _speed;
	public void ClearSpeed () => speed = turnSpeed;

    void OnDrawGizmosSelected ()
    {
		if (!enabled)	
			return;
        Gizmos.color = Color.blue;
        Gizmos.matrix = transform.localToWorldMatrix;
		var forward = useLocal ? Vector3.forward : transform.forward;
        if (!lockX && xLimit != null && xLimit.hasLimit)
        {
            Gizmos.DrawRay (Vector3.zero, Quaternion.AngleAxis (xLimit.limitMin, Vector3.right) * forward * 5);
            Gizmos.DrawRay (Vector3.zero, Quaternion.AngleAxis (xLimit.limitMax, Vector3.right) * forward * 5);
        }
        if (!lockY && yLimit != null  && yLimit.hasLimit)
        {
            Gizmos.DrawRay (Vector3.zero, Quaternion.AngleAxis (yLimit.limitMin, Vector3.up) * forward * 5);
            Gizmos.DrawRay (Vector3.zero, Quaternion.AngleAxis (yLimit.limitMax, Vector3.up) * forward * 5);
        }
    }

    [System.Serializable]
    class RotationLimit 
    {
        public bool hasLimit;
        [Range (0, -180)] public float limitMin = -30;
        [Range (0, 180)] public float limitMax = 30;

    }
}
