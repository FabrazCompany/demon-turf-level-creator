using System.Collections.Generic;
using UnityEngine;
#if !DT_EXPORT && !DT_MOD
using KinematicCharacterController;
#endif

public class DeliverDragKnockback : DeliverKnockback 
{
	[System.Serializable]
	public struct Resident 
	{
#if !DT_EXPORT && !DT_MOD
		public KinematicCharacterMotor motor;
#endif
		public Rigidbody rigid;
		public Transform point;
		public Collider coll;
		public ReceiveHold hold;
#if !DT_EXPORT && !DT_MOD
		public Resident (KinematicCharacterMotor _motor, Transform _point)
		{
			motor = _motor;
			coll = motor.Capsule;
			rigid = null;
			point = _point;
			hold = _motor.GetComponent<ReceiveHold> ();
		}
#endif
		public Resident (Rigidbody _rigid, Collider _coll, Transform _point)
		{
#if !DT_EXPORT && !DT_MOD
			motor = null;
#endif
			rigid = _rigid;
			coll = _coll;
			point = _point;
			hold = _rigid.GetComponentInChildren<ReceiveHold> ();
		}
	}
	List<Resident> residents;
	Vector3 lastPos;

	public List<Resident> getResidents => residents;

	void Awake () 
	{
		residents = new List<Resident> ();
	}

	void OnDisable () 
	{
#if !DT_EXPORT && !DT_MOD
		foreach (var resident in residents)
		{
			if (resident.motor == null || resident.motor.Capsule == null)
				continue;
			if (resident.hold)
				resident.hold.EndHold ();
			if (resident.motor && resident.motor.TryGetComponent<IPlayerController> (out var player))
				player.ClearHold ();
			Destroy (resident.point.gameObject);
			Contact (resident.motor.Capsule);
		}
		residents.Clear ();
#endif
	}
#if !DT_EXPORT && !DT_MOD
	protected virtual bool IsValidTarget(Collider other, out ReceiveKnockback knockback, out KinematicCharacterMotor controller) 
	{
		var isValid = base.IsValidTarget (other, out knockback);
		if (!other.TryGetComponent<KinematicCharacterMotor> (out var _controller))
			isValid = false;
		if (isValid && residents.Find (i => i.motor == _controller).motor != null)
			isValid = false;
		controller = isValid ? _controller : null;
		return isValid;
	}
#endif
	protected virtual bool IsValidTarget(Collider other, out ReceiveKnockback knockback, out Rigidbody rigid) 
	{

		var isValid = base.IsValidTarget (other, out knockback);
		if (!other.TryGetComponent<Rigidbody> (out var _rigid))
			isValid = false;
#if !DT_EXPORT && !DT_MOD
		if (isValid && residents.Find (i => i.rigid == _rigid).motor != null)
			isValid = false;
#endif
		rigid = isValid ? _rigid : null;

		return isValid;
	}

	void OnTriggerEnter (Collider other) 
	{
#if !DT_EXPORT && !DT_MOD
		if (IsValidTarget (other, out var knockback, out KinematicCharacterMotor motor))
		{
			var point = new GameObject (motor.ToString ()).transform;
			point.position = motor.transform.position;
			point.SetParent (transform);
			residents.Add (new Resident (motor, point));

			if (other.TryGetComponent<IPlayerController> (out var player))
				player.TriggerHold ();
		} 
		else if (IsValidTarget (other, out knockback, out Rigidbody rigid))
		{
			var point = new GameObject (rigid.ToString ()).transform;
			point.position = rigid.transform.position;
			point.SetParent (transform);
			residents.Add (new Resident (rigid, other, point));

			var hold = other.GetComponentInChildren<ReceiveHold> ();
			if (hold)
				hold.TriggerHold ();
		} 
#endif
    }
	void OnTriggerExit (Collider other) 
	{
#if !DT_EXPORT && !DT_MOD
		var resident = residents.Find (i => (i.motor && i.motor.Capsule == other) || (i.rigid && i.rigid.Equals (other.attachedRigidbody)));
		if (resident.motor == null && resident.rigid == null)
			return;
		
		if (resident.hold)
			resident.hold.EndHold ();
		if (resident.motor && resident.motor.TryGetComponent<IPlayerController> (out var player))
			player.ClearHold ();
		Destroy (resident.point.gameObject);
		residents.Remove (resident);
		Contact (other);
#endif
	}

	void FixedUpdate () 
	{
#if !DT_EXPORT && !DT_MOD
		foreach (var resident in residents)
		{
			if (resident.motor != null)
				resident.motor.SetPosition (resident.point.position);
		}
#endif
	}
}