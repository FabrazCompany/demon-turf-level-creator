﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DeliverBounce : MonoBehaviour
{
    [SerializeField] bool muteBounce;
    [SerializeField] float bounceForce = 130;
    
    public UnityEvent onBounceTriggered;
    public float BounceForce => bounceForce;

    public void SetMute (bool active) => muteBounce = active;
    public bool getMute => muteBounce; 
    public void SetBounceForce (float force) => bounceForce = force;
    public void TriggerBounce () {
        if (muteBounce)
            return;
            
        onBounceTriggered?.Invoke ();
    }
}
