using UnityEngine;

public class DeliverBurst : MonoBehaviour 
{
    enum DirectionType 
	{
        Center,
        Override,
        Radial,
    }
    [SerializeField] float force;
    [SerializeField] DirectionType hitType;
    [SerializeField, Conditional ("hitType", DirectionType.Override)] Facing forceDirection;
    [SerializeField] bool refreshAerialState;

    void OnTriggerEnter (Collider hit)
    {
		if (!hit.TryGetComponent<IPlayerController> (out var player))
			return;

        if (refreshAerialState)
            player.ResetJumpState ();
        
        player.ForceUnground ();
        var dir = Vector3.zero;
        switch (hitType) {
            default:
            case DirectionType.Center:
                dir = (player.transform.position - transform.position).With (y:0).normalized;
                break;

            case DirectionType.Radial:
                dir = (player.transform.position - transform.position).normalized;
                break;
                
            case DirectionType.Override:
                switch (forceDirection) {
                    default:
                    case Facing.Forward:
                        dir = transform.forward;
                        break;
                    case Facing.Up:
                        dir = transform.up;
                        break;
                    case Facing.Right:
                        dir = transform.right;
                        break;
                }
                break;
        }

        player.AddPush (dir * force);
    }
}