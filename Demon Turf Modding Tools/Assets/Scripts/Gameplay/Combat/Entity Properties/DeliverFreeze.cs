﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeliverFreeze : MonoBehaviour
{
    enum DirectionType 
    {
        Center,
        Direction,
        Radial,
    }
    [Header ("Force")]
    [SerializeField] bool applyForce;
    [SerializeField, Conditional ("applyForce")] DirectionType hitType;
    [SerializeField, Conditional ("applyForce")] Facing forceDirection;
    [SerializeField, Conditional ("applyForce")] float velocityCutoff = 2;
    [SerializeField, Conditional ("applyForce")] float appliedForce = 10;
    [SerializeField] bool mute;

    public void SetMute (bool _mute) => mute = _mute;
    public event System.Action<Transform> onFreeze;

    public void OnTriggerEnter (Collider other) => Contact (other);
    public void OnCollisionEnter(Collision coll) 
    {
        Contact (coll.collider);
    }
    void Contact (Collider other) 
    {
        if (mute)
            return;
        
        TriggerFreeze (other);   
    }
    public void TriggerFreeze (Collider other, Vector3 direction) => TriggerFreeze (other);
    public void TriggerFreeze (Collider other) 
    {
        if (!other?.GetComponent<ReceiveFreeze> ())
            return;
            
        other.GetComponent<ReceiveFreeze> ().TriggerFreeze (this);
        if (applyForce && other.attachedRigidbody && other.attachedRigidbody.GetComponent<ReceiveKnockback> ())
        {
            var knockback = other.attachedRigidbody.GetComponent<ReceiveKnockback> ();
            var dir = GetDirection (other);
#if !DT_EXPORT && !DT_MOD
            var kinetic = other.attachedRigidbody.GetComponent<KinematicCharacterController.KinematicCharacterMotor> ();
            if (kinetic)
            {
                if (kinetic.BaseVelocity.magnitude < velocityCutoff)
                    knockback.ApplyKnockback (dir, appliedForce);
            }
            else 
            {
                if (other.attachedRigidbody.velocity.magnitude / Time.fixedDeltaTime < velocityCutoff)
                    knockback.ApplyKnockback (dir, appliedForce);
            }
#endif
        }
        onFreeze?.Invoke (other.transform);
    }

    Vector3 GetDirection (Collider other) 
    {
        switch (hitType)
        {
            default:
            case DirectionType.Direction:
            switch (forceDirection) 
            {
                default:
                case Facing.Forward:
                    return transform.forward;
                case Facing.Up:
                    return transform.up;
                case Facing.Right:
                    return transform.right;
            }
            case DirectionType.Center:
                return (other.transform.position - transform.position).With (y:0).normalized;
            
            case DirectionType.Radial:
                return (other.transform.position - transform.position).normalized;
        }
    }
}
