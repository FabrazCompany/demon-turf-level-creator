using UnityEngine;
using System.Collections.Generic;

public class DeliverHold : MonoBehaviour
{
    [SerializeField] float massThreshold;
    List<ReceiveHold> held;
    void Awake () 
    {
        held = new List<ReceiveHold> ();
    }
    void OnDisable () 
    {
        if (held == null || held.Count == 0)
            return;
            
        for (int i = 0; i < held.Count; i++)
        {
            held[i].EndHold ();
        }
        held.Clear ();
    }

    void OnTriggerEnter (Collider hit)
    {
        var holdable = hit.GetComponent<ReceiveHold> ();
        if (holdable && !holdable.GetMute) 
        {
            held.Add (holdable);
            holdable.TriggerHold ();
        }
    }
    void OnTriggerExit (Collider hit)
    {
        var holdable = hit.GetComponent<ReceiveHold> ();
        if (holdable && !holdable.GetMute)
        {
            held.Remove (holdable);
            holdable.EndHold ();
        }   
    }
}