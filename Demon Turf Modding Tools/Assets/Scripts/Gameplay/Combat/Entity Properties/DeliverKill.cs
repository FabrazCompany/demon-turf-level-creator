﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeliverKill : MonoBehaviour
{
    public bool mute;
	public bool externallyTriggered;
    public KillTypes killType;
    
    public void SetMute (bool val) => mute = val;
    public void TriggerKill (Collider hit, Vector3 pos) => hit?.GetComponent<ReceiveKill> ()?.TriggerKill (this);
	public void TriggerKillIgnoreMute (Collider hit, Vector3 pos) => hit?.GetComponent<ReceiveKill> ()?.TriggerKill (this);
    public void OnTriggerEnter (Collider other) => Contact (other);
    public void OnCollisionEnter(Collision coll) => Contact (coll.collider);
    void Contact (Collider other) 
    {
        if (mute) return;
		if (externallyTriggered) return;

        var killable = other?.GetComponent<ReceiveKill> ();
        killable?.TriggerKill (this);
    }

    public enum KillTypes
    {
        NULL = 0,

        Spikes = 1,
        Lava = 2,
        Bullet = 3,
        Explosion = 4,
        Laser = 5,

        Fire = 10,

        Bounds = 500,

    }
}