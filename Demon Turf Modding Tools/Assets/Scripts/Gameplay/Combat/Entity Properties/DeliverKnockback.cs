﻿using UnityEngine;

public class DeliverKnockback : MonoBehaviour {
    enum DirectionType {
        Center,
        Override,
        Radial,
		Normal,
		RadialLift,
    }
    // [SerializeField] bool active = true;
    [SerializeField] bool mute;
    [SerializeField] DirectionType hitType;
    [SerializeField, Conditional ("hitType", DirectionType.Override)] Facing forceDirection;
    [SerializeField] bool forceExternallyDetermined;
    [SerializeField, Conditional ("forceExternallyDetermined", hide: true)] float force;
	[SerializeField, Conditional ("hitType", DirectionType.RadialLift)] float addtionalForce;
    [SerializeField] float stunMultiplier = 3;
    
    public UnityEventCollider onHit;
	public float getForce => force;

    Vector3 normal;
    public void SetMute (bool val) => mute = val;
    void OnTriggerEnter (Collider other) {
        Contact (other);
    }
    void OnCollisionEnter(Collision coll) {
		normal = coll.contacts[0].normal;
        Contact (coll.collider);
    }
	protected virtual bool IsValidTarget (Collider other, out ReceiveKnockback knockback)
	{
		knockback = null;
		if (mute)
            return false;
		if (!other.TryGetComponent<ReceiveKnockback> (out var _knockback))
            return false;
        if (_knockback.getRigid && transform.IsChildOf (_knockback.getRigid.transform))
            return false;

		knockback = _knockback;
		return true;
	}
    public void Contact (Collider other) {
		if (!IsValidTarget (other, out var knockback))
			return;

        var hitForce = force;

        switch (hitType) {
            default:
            case DirectionType.Center:
            {
                var dir = (other.transform.position - transform.position).With (y:0).normalized;
                knockback.ApplyKnockback (dir, hitForce);
                Debug.DrawRay (transform.position, dir * hitForce, Color.red, 1);
                break;
            }
            
            case DirectionType.Radial:
            {
                var dir = (other.transform.position - transform.position).normalized;
                knockback.ApplyKnockback (dir, hitForce);
                Debug.DrawRay (transform.position, dir * hitForce, Color.red, 1);
                break;    
            }
                
            case DirectionType.Override:
                switch (forceDirection) {
                    default:
                    case Facing.Forward:
                        knockback.ApplyKnockback (transform.forward, hitForce);
                        break;
                    case Facing.Up:
                        knockback.ApplyKnockback (transform.up, hitForce);
                        break;
                    case Facing.Right:
                        knockback.ApplyKnockback (transform.right, hitForce); 
                        break;
                }
                break;
			case DirectionType.Normal:
				knockback.ApplyKnockback (-normal, hitForce);
				break;
			case DirectionType.RadialLift:
				{
					var force = (other.transform.position - transform.position).With (y:0).normalized * hitForce;
					force.y = addtionalForce;
					knockback.RecieveKnockback (force);
					// Debug.DrawRay (transform.position, dir * hitForce, Color.red, 1);
				}				
				break;
        }
        onHit?.Invoke (other);
    }
    public void OverrideForce (float newForce) => force = newForce;

    public void TriggerKnockback (Collider hit, Vector3 pos) {
        var knockback = hit.GetComponent<ReceiveKnockback> ();
        if (!knockback)
            return;
            
        var hitForce = force;
        if (hit.GetComponent<ReceiveStun> ()?.isStunned ?? false)
            hitForce *= stunMultiplier;
        var dir = (hit.transform.position - pos).With (y:0).normalized;
        knockback.ApplyKnockback (dir, hitForce);
        onHit?.Invoke (hit);
    }

}
