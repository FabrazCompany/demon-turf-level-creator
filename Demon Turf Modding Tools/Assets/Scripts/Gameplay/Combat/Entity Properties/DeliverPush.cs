﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeliverPush : MonoBehaviour
{
    enum DirectionType 
	{
        Center,
        Override,
        Radial,
    }
    [SerializeField] float force;
    [SerializeField] DirectionType hitType;
    [SerializeField, Conditional ("hitType", DirectionType.Override)] Facing forceDirection;
    [SerializeField] bool refreshAerialState;
	[SerializeField] bool forceUnground = true;
	[SerializeField] bool mute;

	void OnTriggerStay (Collider hit) => Push (hit);
	public void SetMute (bool state) => mute = state; 
    public void FreezeMomentum (Collider hit)
    {
		if (!hit.TryGetComponent<IPlayerController> (out var player))
			return;
		player.ZeroOutVelocity ();
    }
    public void Push (Collider hit)
    {
		if (!hit.TryGetComponent<IPlayerController> (out var player))
			return;

        if (refreshAerialState)
            player.ResetJumpState ();
        
		if (forceUnground)
	        player.ForceUnground ();
        switch (hitType) {
            default:
            case DirectionType.Center:
            {
                var dir = (player.transform.position - transform.position).With (y:0).normalized;
                player.AddSecondaryVelocity (dir * force);
                break;
            }

            case DirectionType.Radial:
            {
                var dir = (player.transform.position - transform.position).normalized;
                player.AddSecondaryVelocity (dir * force);
                break;
            }
                
            case DirectionType.Override:
                switch (forceDirection) {
                    default:
                    case Facing.Forward:
                        player.AddSecondaryVelocity (transform.forward * force);
                        break;
                    case Facing.Up:
                        player.AddSecondaryVelocity (transform.up * force);
                        break;
                    case Facing.Right:
                        player.AddSecondaryVelocity (transform.right * force);
                        break;
                }
                break;
        }
    }
}
