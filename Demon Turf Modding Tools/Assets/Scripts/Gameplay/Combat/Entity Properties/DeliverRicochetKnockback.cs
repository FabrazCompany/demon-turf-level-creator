using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DeliverRicochetKnockback : MonoBehaviour 
{
	[SerializeField] bool muteRicochet;
	[SerializeField, UnityEngine.Serialization.FormerlySerializedAs ("ricochetThreshold")] float receiveRicochetThreshold = 12;
	[SerializeField] float triggerKnockbackThreshold = 15;
	[SerializeField, Range (-1f, 1f)] float triggerKnockbaackHeadingAlightment = 0.25f;
	const float stabilizeThreshold = .5f;
	[SerializeField] GameObject ricochetEffect;
	[SerializeField] float selfVelocityDropoff = 0.9f;
	[SerializeField] float ricochetVelocityTransfer = .5f;

	[SerializeField] List<ReceiveKnockback> knockbacks;

	Rigidbody rigid;
	SlowMoEntity timeEntity;
	float buffer;
	const float bufferMin = 0.2f;

	public UnityEvent onRicocheted;
	public UnityEvent onDeliverRicochet;
	bool isRicocheting => enabled;
	public void SetRigid (Rigidbody _rigid) => rigid = _rigid;
	bool knockbackActive 
	{
		get
		{
			var active = false;
			foreach (var knocker in knockbacks)
			{
				if (knocker.enabled)
				{
					active = true;
					break;
				}
			}
			return active;
		}
	}
	public void SetMute (bool active) => muteRicochet = active;

	void Awake () {
		rigid = GetComponent<Rigidbody> ();
		timeEntity = rigid.GetComponent<SlowMoEntity> ();
		if (knockbacks == null || knockbacks.Count == 0)
		{
			GetComponentsInChildren<ReceiveKnockback> (true, knockbacks);
		}
		foreach (var knocker in knockbacks) 
		{
			knocker.onAttacked.AddListener (TriggerRicochet);
		}

		// Debug.Log (knockbacks.Count, this);

		enabled = false;
	}
	void OnEnable () 
	{
		buffer = bufferMin;
	}
	void OnDestroy () 
	{
		foreach (var knocker in knockbacks) 
		{
			knocker?.onAttacked?.RemoveListener (TriggerRicochet);
		}
	}
	void TriggerRicochet (Vector3 dir) 
	{
		if (muteRicochet)
			return;
		buffer = bufferMin;
		onRicocheted?.Invoke ();
		enabled = true;
	}

	void OnCollisionEnter(Collision coll) {
		if (muteRicochet)
			return;
			
		if (!coll.rigidbody)
			return;
		
		if (coll.rigidbody.GetComponent<NoRicochet>())
			return;
		
		// spawn hit effects at contact points
		if (!enabled)    
		{
			var velocity = rigid.velocity.With (0);
			var threshold = receiveRicochetThreshold * timeEntity?.timeScale ?? receiveRicochetThreshold;
			if (velocity.With (y: 0).sqrMagnitude < threshold * threshold)
				return;
				
			if (ricochetEffect && !knockbackActive && coll.contactCount > 0)  //being knocked into from idle
			{
				for (int i = 0; i < coll.contactCount; i++)
				{
					TrashMan.spawn (ricochetEffect, coll.GetContact(i).point, Quaternion.identity);
				}
			}
			onRicocheted?.Invoke ();
			enabled = true;
		}
		else //already ricocheting, add velocity decay
		{
			var targetDiff = (coll.rigidbody.position - transform.position.With (y: coll.rigidbody.position.y)).normalized;
			var ricochetVelocity = rigid.velocity.With (y: 0);

			var threshold = triggerKnockbackThreshold * (coll.rigidbody.GetComponent<SlowMoEntity> ()?.timeScale ?? 1f);
			if (ricochetVelocity.sqrMagnitude < (threshold * threshold)) return;
				
			var knockback = coll.rigidbody.GetComponentInChildren<ReceiveKnockback> ();
			knockback?.ApplyKnockback (targetDiff, coll.impulse.magnitude * ricochetVelocityTransfer);
			onDeliverRicochet?.Invoke();
		} 	
	}

	void Update () 
	{
		if (buffer > 0)
		{
			buffer -= Time.deltaTime * timeEntity?.timeScale ?? Time.deltaTime;
			return;
		}

		var threshold = stabilizeThreshold * timeEntity?.timeScale ?? stabilizeThreshold;
		if (rigid.velocity.With (y: 0).sqrMagnitude < threshold * threshold)
			enabled = false;
	}

	public void RegisterKnockback (ReceiveKnockback knockback) 
	{
		if (knockbacks.Contains (knockback))
			return;
		knockbacks.Add (knockback);
		knockback.onAttacked.AddListener (TriggerRicochet);
	}
	public void UnregisterKnockback (ReceiveKnockback knockback) 
	{
		if (!knockbacks.Contains (knockback))
			return;
		knockbacks.Remove (knockback);
		knockback.onAttacked.RemoveListener (TriggerRicochet);
	}
}
