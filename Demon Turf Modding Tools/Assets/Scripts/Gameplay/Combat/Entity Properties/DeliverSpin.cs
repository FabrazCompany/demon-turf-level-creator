﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeliverSpin : MonoBehaviour
{
    [SerializeField] bool forceExternallyDetermined;
    [SerializeField, Conditional ("forceExternallyDetermined", hide: true)] float spinForce;
    [SerializeField] UnityEventCollider onHit;
	[SerializeField] bool effectPlayer;
	[SerializeField, Conditional ("effectPlayer")] float playerStunTime = 1;
	[SerializeField, Conditional ("effectPlayer")] float playerVelocityDamping = 1;
    public UnityEventCollider getOnHit => onHit;

    void OnTriggerEnter (Collider other) => Contact (other);
    void OnCollisionEnter(Collision coll) => Contact (coll.collider);
    void Contact (Collider other) 
	{
		if (other.TryGetComponent<ReceiveSpin> (out var spinReceiver))
			spinReceiver.ApplySpin (spinForce);
        else if (other.TryGetComponent<PlayerSpinTrigger> (out var spinTrigger))
            spinTrigger.Trigger ();
		else if (effectPlayer && other.TryGetComponent <IPlayerController> (out var player))
			player.TriggerSpinStun (playerStunTime, playerVelocityDamping, spinForce);
		else // no spin targets, cancel out
			return;
        onHit?.Invoke (other);
    }

    public void SetSpinForce (float newForce) => spinForce = newForce;
	public void SetPlayerEffectDuration (float dur) => playerStunTime = dur;
}
