﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DeliverStun : MonoBehaviour {
    [SerializeField] float stun;

    [SerializeField] UnityEventCollider onHit;

    void OnTriggerEnter (Collider other) {
        Contact (other);
    }
    void OnCollisionEnter(Collision coll) {
        Contact (coll.collider);
    }
    void Contact (Collider other) {
        var stunReceiver = other.GetComponent<ReceiveStun> ();
        if (!stunReceiver)
            return;

        stunReceiver.ApplyStun ();
        onHit?.Invoke (other);
    }
}
