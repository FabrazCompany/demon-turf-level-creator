using UnityEngine;

public class EnablePlayerBounce : MonoBehaviour
{
    public void OnTriggerEnter (Collider hit) 
    {
		if (!hit.TryGetComponent<IPlayerController> (out var player))
			return;
        player.SetBounceCollider (true);
    }
}