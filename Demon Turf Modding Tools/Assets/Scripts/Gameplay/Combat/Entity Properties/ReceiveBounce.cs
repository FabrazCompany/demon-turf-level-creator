﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReceiveBounce : MonoBehaviour
{  
    [SerializeField] UnityEventFloat onBounce;

    void OnTriggerEnter(Collider other) 
    {
        TryTriggerBounce (other);
    }
    void OnCollisionEnter(Collision coll) 
    {
        TryTriggerBounce (coll.collider);
    }
    void TryTriggerBounce (Collider other) 
    {
        var bouncer = other.GetComponent<DeliverBounce> ();
        if (!bouncer || bouncer.getMute)
            return;
        ApplyBounce (bouncer.BounceForce);
        bouncer.TriggerBounce ();
    }
    public void ApplyBounce (float force) 
    {
        onBounce?.Invoke (force);  
    }
}
