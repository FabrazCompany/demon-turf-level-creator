﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ReceiveFreeze : MonoBehaviour
{
	[SerializeField] bool mute;
    [SerializeField] GameObject frozenVisual;
    [SerializeField] Renderer frozenInteriorVisual;
    [SerializeField] ParticleSystem breakParticle;
    Vector3 frozenVisualOffset;
    [SerializeField] float freezeDuration = 5;
    [SerializeField] float struggleReduction = 0.5f; 
    [SerializeField] float struggleDelay = 0.25f;
    [SerializeField] float struggleShakeMagnitude = 0.1f;
    [SerializeField] int struggleShakeVibrato = 20;
    [SerializeField] float struggleShakeDuration = 0.1f;
	[Space]
	[SerializeField] ReceiveFreeze externalFreeze;

    public event System.Action onFrozen;
    public event System.Action onUnfrozen;

	[SerializeField] protected AudioClip iceBreaksClip;
    
    float freezeTimer;
    float struggleTimer;

    void Awake () 
    {
        frozenVisualOffset = frozenVisual.transform.localPosition;
        if (enabled)
            enabled = false;
    }

	[ContextMenu ("Trigger Freeze")]
	public void TriggerFreeze () => TriggerFreeze (null);
    public void TriggerFreeze (DeliverFreeze freeze)
    {
		if (mute)
			return;

        if (enabled)
            return;
		
		if (externalFreeze)
		{
			externalFreeze.TriggerFreeze (freeze);
			return;
		}

        frozenVisual.SetActive (true);
        freezeTimer = 0;
        enabled = true;    
        onFrozen?.Invoke ();
    }
    void Update () 
    {
        freezeTimer += Time.deltaTime;
        frozenInteriorVisual.material.SetFloat ("_CrackAmt", freezeTimer / freezeDuration);
        if (freezeTimer >= freezeDuration)
            Break (); 
    }

	public bool Struggle () 
    {
        if (struggleTimer > Time.time)
            return false;
        
        struggleTimer = Time.time + struggleDelay;
        freezeTimer += struggleReduction;
        frozenVisual.transform.localPosition = frozenVisualOffset;
        frozenVisual.transform.DOShakePosition (struggleShakeDuration, struggleShakeMagnitude, struggleShakeVibrato);
        return true;
    }
	public void Break ()
	{
		frozenVisual.SetActive (false);
        enabled = false;
        breakParticle?.Play ();
		SoundManagerStub.Instance.PlaySoundGameplay3D (iceBreaksClip, transform.position, data: new AudioData( _volume:.6f, _min:10f , _max:15f));
        onUnfrozen?.Invoke ();
	}

	public void ResetFreeze ()
	{
		frozenVisual.SetActive (false);
        enabled = false;
	}
}
