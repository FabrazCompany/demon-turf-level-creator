using UnityEngine;
using UnityEngine.Events;


public class ReceiveHold : MonoBehaviour
{
    bool mute;
    public UnityEvent onStartHold;
    public UnityEvent onEndHold;
	// public 

    public void SetMute (bool _mute) => mute = _mute;
    public bool GetMute => mute;
    public void TriggerHold ()
    {
        if (mute)
            return;
        onStartHold?.Invoke ();
    }
    public void EndHold () 
    {
        if (mute)
            return;
        onEndHold?.Invoke ();
    }
}