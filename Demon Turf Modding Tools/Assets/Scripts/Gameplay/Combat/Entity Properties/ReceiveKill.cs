﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class ReceivedKillEvent : UnityEvent<DeliverKill.KillTypes> {} 

public class ReceiveKill : MonoBehaviour
{
    bool mute;
    Rigidbody owner;
    public ReceivedKillEvent onKilled;
    [SerializeField] DeliverKill.KillTypes[] immunities;
    List<DeliverKill> potentialKills;
	[SerializeField] ReceiveKill externalKill;

	float lastKilledTime;
	const float killedBuffer = 0.1f;

    public void SetMute (bool val) => mute = val;

    void Awake ()
    {
        owner = GetComponentInParent<Rigidbody> ();
        potentialKills = new List<DeliverKill> ();
    }
    bool TryKill (DeliverKill kill)
    {
		if (lastKilledTime > Time.time)
			return false;
        if (kill.transform.IsChildOf (owner.transform))
            return false;

        if (kill.killType != DeliverKill.KillTypes.NULL)
        {
            for (int i = 0; i < immunities.Length; i++)
            {
                if (immunities[i] == kill.killType)
                    return false;
            }
        }
		lastKilledTime = Time.time + killedBuffer;
        onKilled?.Invoke (kill.killType);
        return true;
    }
    public bool TriggerKill (DeliverKill kill) 
    {
        if ((mute || kill.mute) && kill.killType != DeliverKill.KillTypes.Bounds)
            return false;
		
		if (externalKill)
		{
			return externalKill.TriggerKill (kill);
		}
        return TryKill (kill);
    } 

    [ContextMenu ("Trigger Kill")]
    public void TriggerKill ()
    {
        onKilled?.Invoke (DeliverKill.KillTypes.NULL);
    }

    public void Despawn (GameObject _object) => TrashMan.despawn (_object);
    public void Spawn (GameObject _object) => Instantiate (_object, transform.position, Quaternion.identity);
    public void Destroy (GameObject _object) => GameObject.Destroy (_object);
}
