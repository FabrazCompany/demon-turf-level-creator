﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ReceiveKnockback : MonoBehaviour 
{
	[SerializeField] AIEnemy enemy;
	[SerializeField] Collider hitbox;	
    [SerializeField] Rigidbody rigid;
    [SerializeField] SlowMoEntity entityTime;
    [SerializeField] bool muteKnockback;
    [SerializeField] float knockbackVulnerability = 1;
	[SerializeField] float stabilizeThresholdScalar = 1;
    const float stabilizeThreshold = .75f;
	[SerializeField] ReceiveKnockback externalKnockback;
	[SerializeField] bool externalKnockbackAdjustedByVulnerability;

    [Header ("Feedback")]
    [SerializeField] GameObject hitEffect;
    [SerializeField] Vector3 hitEffectOffset = new Vector3 (0,1,0);
    [SerializeField] float hitEffectRepeatDelay = .5f;
    [SerializeField] float scalingRangeMin = 60;
    [SerializeField] float scalingRangeMax = 150;
    [SerializeField] float scalingSizeMin = 0.7f;
    [SerializeField] float scalingSizeMax = 1.2f;
	[Space]
	float yScaling = 1.25f;
    public UnityEventVector3 onAttacked;
    public UnityEvent onKnockbackEnded;
    float buffer;
    const float bufferMin = 0.2f;
    float knockbackLimit = -1;
    float lastHitTime;
    

    float cachedVulnerability;
    public void SetMute (bool active) => muteKnockback = active;
    public void SetVulnerability (float val) => knockbackVulnerability = val;
    public void ResetVulnerability () => knockbackVulnerability = cachedVulnerability;
    public void SetKnockbackLimit (float limit) => knockbackLimit = limit;
    public void ClearKnockbackLimit () => knockbackLimit = -1;
	public bool GetActive () => hitbox && hitbox.enabled && !muteKnockback;
	public void SetExternalKnockback (ReceiveKnockback knockback) => externalKnockback = knockback;
    public void SetRigid (Rigidbody _rigid) => rigid = _rigid;    
	public Rigidbody getRigid => rigid;

	void OnValidate ()
	{
		if (!rigid)
			rigid = GetComponentInParent<Rigidbody> ();
		if (!entityTime)
			entityTime = GetComponentInParent<SlowMoEntity> ();
		if (!hitbox)
			hitbox = GetComponent<Collider> ();
		if (!enemy)
			enemy = GetComponentInParent<AIEnemy> ();		
	}	

	void Awake () 
	{
        if (!rigid)
			rigid = GetComponentInParent<Rigidbody> ();
		if (!entityTime)
			entityTime = GetComponentInParent<SlowMoEntity> ();
		if (!hitbox)
			hitbox = GetComponent<Collider> ();
		if (!enemy)
			enemy = GetComponentInParent<AIEnemy> ();

        cachedVulnerability = knockbackVulnerability;
    }
	public void ApplyKnockback (Vector3 dir, float force) => ApplyKnockback (dir, force, true); 
    public void ApplyKnockback (Vector3 dir, float force, bool resetVel) 
    {
        if (muteKnockback) return;
		if (dir == Vector3.zero || force == 0) return;		
		
		if (externalKnockback)
		{
			externalKnockback.ApplyKnockback (dir, force);
			return;
		}

		// Debug.Log ($"Component Knockback - Dir: {dir}, Force: {force}");

        ApplyKnockbackForce (dir, force, resetVel);
        
        buffer = bufferMin;
        onAttacked?.Invoke(dir * force);
        if (hitEffect && lastHitTime < Time.time) 
        {
            lastHitTime = Time.time + hitEffectRepeatDelay;
            var pct = Mathf.InverseLerp (scalingRangeMin, scalingRangeMax, force);
            var _hitEffect = TrashMan.spawn (hitEffect, transform.position + hitEffectOffset);
            _hitEffect.transform.localScale = Vector3.one * Mathf.Lerp (scalingSizeMin, scalingSizeMax, pct);
        }
        enabled = true;
    }
	public void ApplyKnockbackForce (Vector3 dir, float force) => ApplyKnockbackForce (dir, force, true); 
    public void ApplyKnockbackForce (Vector3 dir, float force, bool resetVel) 
    {
        if (muteKnockback) return;
		if (dir == Vector3.zero || force == 0) return;		
		if (externalKnockback)
		{
			if (externalKnockbackAdjustedByVulnerability)
				force *= knockbackVulnerability;
			externalKnockback.ApplyKnockbackForce (dir, force, resetVel);
			return;
		}

        force *= knockbackVulnerability;

        if (knockbackLimit > -1)
            force = Mathf.Min (force, knockbackLimit);

		var finalForce = dir * force;
		if (enemy && enemy.isGrounded && dir.y < 0)
		{
			finalForce.y *= -1;
			finalForce.y *= yScaling;
		}
		
        if (!rigid.isKinematic) 
        {
			if (rigid.IsSleeping ())
				rigid.WakeUp ();
			if (!enabled && resetVel)
	            rigid.velocity = Vector3.zero;
            rigid.AddForce (finalForce, ForceMode.Impulse);
        }
    }

    public void RecieveKnockback (Vector3 force) => ApplyKnockback (force.normalized, force.magnitude);
	public void RecieveKnockbackCosmetic (Vector3 force) => ApplyKnockbackForce (force.normalized, force.magnitude);
    public void RecieveKnockbackDown (float force) => ApplyKnockback (Vector3.down, force);
    public void RecieveKnockbackUp (float force) => ApplyKnockback (Vector3.up, force);
    
    void Update ()
    {
        if (buffer > 0)
        {
            buffer -= entityTime ? Time.deltaTime * entityTime.timeScale : Time.deltaTime;
            return;
        }
            
        var threshold =  stabilizeThreshold * entityTime?.timeScale ?? stabilizeThreshold;
		threshold *= stabilizeThresholdScalar;
        if (rigid.velocity.sqrMagnitude > threshold * threshold)
            return;

        onKnockbackEnded?.Invoke ();
        enabled = false;
    }
}