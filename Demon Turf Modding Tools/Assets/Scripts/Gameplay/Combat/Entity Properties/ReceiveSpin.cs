﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ReceiveSpin : MonoBehaviour
{
    [SerializeField] bool muteSpin;
	[SerializeField] ReceiveSpin externalSpin;
    [SerializeField] float spinVulnerability = 1;
    [SerializeField] float dropoffThreshold = 2;
    public UnityEventFloat onSpun;
    public UnityEvent onSpinEnded;
	public event System.Action onPreSpin;
    
    Rigidbody rigid;    
    SlowMoEntity entityTime;
    float buffer;
    const float bufferMin = 0.2f;

    public void SetSpinVulnerability (float val) => spinVulnerability = val;
    public void ResetVulnerability () => spinVulnerability = cachedVulnerability;
    public void SetMute (bool active) => muteSpin = active;
    public bool GetUpdraftCancel (float scale) 
    {
        if (!enabled) return true;
        if (buffer > 0) return false;

        var threshold = dropoffThreshold * entityTime?.timeScale ?? dropoffThreshold;
        threshold *= scale;
        if (rigid.isKinematic)
        {
            if (kinematicSpinForce > threshold)
                return false;
        }
        else 
        {                
            if (rigid.angularVelocity.sqrMagnitude > threshold * threshold)
                return false;
        }
        return true;
    }

    float cachedVulnerability;
    void Awake () {
        rigid = GetComponentInParent<Rigidbody> ();
        entityTime = GetComponentInParent<SlowMoEntity> ();
        kinematicSpinForce = 0;
        cachedVulnerability = spinVulnerability;
    }

    float kinematicSpinForce;

    public void ApplySpin (float spinForce) 
    {
        if (muteSpin) return;
		
		if (externalSpin)
		{
			externalSpin.ApplySpin (spinForce);
			return;
		}

		onPreSpin?.Invoke ();

        enabled = true;
		if (rigid.IsSleeping ())
			rigid.WakeUp ();
        spinForce *= spinVulnerability;
        if (!rigid.isKinematic)
        {
            // rigid.AddTorque (0,spinForce,0,ForceMode.VelocityChange);
			rigid.angularVelocity += Vector3.up * spinForce;
        }
        else 
        {  
            kinematicSpinForce += spinForce;
        }
        buffer = bufferMin;
        onSpun?.Invoke (spinForce);
    }

    void Update () 
    {
        if (buffer > 0)
        {
            buffer -= entityTime ? Time.deltaTime * entityTime.timeScale : Time.deltaTime;
            return;
        }


        var threshold = dropoffThreshold * entityTime?.timeScale ?? dropoffThreshold;
        if (!rigid || rigid.isKinematic)
        {
            if (kinematicSpinForce > threshold)
                return;
        }
        else 
        {                
            if (rigid.angularVelocity.sqrMagnitude > threshold * threshold)
                return;
        }
        
        kinematicSpinForce = 0;
        onSpinEnded?.Invoke ();
        enabled = false;
    }
    void FixedUpdate () 
    {
		if (!rigid) return;
        if (!rigid.isKinematic) return;
        
        var newRot = transform.rotation * Quaternion.Euler(transform.up * kinematicSpinForce);
        rigid.MoveRotation (newRot);
        kinematicSpinForce *= Mathf.Exp (-rigid.angularDrag * Time.fixedDeltaTime);
    }
}
