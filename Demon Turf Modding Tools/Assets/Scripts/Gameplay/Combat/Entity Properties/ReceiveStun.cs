using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ReceiveStun : MonoBehaviour, IResumePathingModifier {
    // enum StunState {
    //     Inactive,
    //     Stunned,
    //     // Immune,
    // }

    // [System.Serializable]
    // struct StunParameters {
    //     public float stunDurationRatio;
    //     public float immunityDuration;
    //     public float descalationDuration;
    // }
    // [SerializeField, ReadOnly] StunState state;
    

	
    [SerializeField] bool muteStun;
    [SerializeField] float stunDuration = 2f;
    [SerializeField, ReadOnly] float stunTimer;
    [SerializeField] bool retriggerable = true;
	[SerializeField] ReceiveStun externalStun;
    
    public UnityEventFloat onStunned;
    public UnityEvent onStunEnded;
    SlowMoEntity timeEntity;   
    public bool isStunned => enabled;    
    public void SetMute (bool active) => muteStun = active;

	bool IResumePathingModifier.ShouldSkipCheck () 
	{
		return !gameObject.activeInHierarchy;
	}
    bool IResumePathingModifier.ShouldHaltPathing ()
    {
        return isStunned;
    }
    bool IResumePathingModifier.ShouldResumePathing ()
    {
        return !isStunned;
    }

    void Awake () 
    {
        timeEntity = GetComponentInParent<SlowMoEntity> ();
        enabled = false;
    }
    public void ApplyStun () => ApplyStun (stunDuration);
    public void ApplyStun (float dur) {
        if (muteStun) return;
		if (externalStun)
		{
			externalStun.ApplyStun (dur);
			return;
		}

        if (dur == 0) return;
        if (retriggerable && enabled) return;
        
        stunTimer = dur;
        enabled = true;
        onStunned?.Invoke (dur);        
    }
    public void CancelStun () {
        stunTimer = 0;
        enabled = false;
        onStunEnded?.Invoke ();
    }
    
    void Update () {
        stunTimer -= Time.deltaTime * timeEntity?.timeScale ?? Time.deltaTime;
        if (stunTimer <= 0)
            CancelStun ();
    }


    #if UNITY_EDITOR
    [ContextMenu ("Trigger Stun")]
    public void Debug_ApplyStun () 
    {
        ApplyStun (2);
    }
    #endif
}