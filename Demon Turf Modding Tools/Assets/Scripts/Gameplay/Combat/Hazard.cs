using UnityEngine;

public class Hazard : MonoBehaviour, IHazard
{
	Rigidbody rigid;

	Rigidbody IHazard.getRigid => rigid;
	float IHazard.getSize => size;

	float size;
	void Awake () 
	{
		rigid = GetComponent<Rigidbody> ();
		var coll = GetComponent<Collider> ();
		size = coll?.bounds.size.magnitude ?? 1;
	}

	void OnDrawGizmosSelected () 
	{
		Gizmos.color = Color.red;
		var dir = rigid ? rigid.velocity : transform.forward;
		Gizmos.DrawRay (transform.position, dir * 2);
	}
}