

public interface IAttackAvailabilityModifier
{
    bool CanAttack ();
}