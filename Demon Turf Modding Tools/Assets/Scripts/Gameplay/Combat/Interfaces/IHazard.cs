﻿using UnityEngine;
public interface IHazard 
{
    Transform transform { get; }
    GameObject gameObject { get; }
    Rigidbody getRigid { get; }
    float getSize { get; }
}