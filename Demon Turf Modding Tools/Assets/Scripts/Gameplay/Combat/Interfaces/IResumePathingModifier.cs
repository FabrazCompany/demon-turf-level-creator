using UnityEngine;

public interface IResumePathingModifier
{
    string name { get; }
    // GameObject gameObject { get; }
	bool ShouldSkipCheck ();
    bool ShouldResumePathing ();
    bool ShouldHaltPathing ();
}