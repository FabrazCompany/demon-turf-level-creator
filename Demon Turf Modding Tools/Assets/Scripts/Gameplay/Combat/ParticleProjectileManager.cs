using UnityEngine;

public struct ParticleProjectilesData
{

}
public class ParticleProjectilesManager : FlyweightManager<ParticleProjectiles, ParticleProjectilesData, ParticleProjectilesManager>
{
	void Update ()
	{
		var items = slots.RawItems;
		var length = slots.Count;
		for (int i = 0; i < length; i++)
		{
			ref var slot = ref items[i];
			if (!slot.active) continue;

			slot.flyweight.StateUpdate ();
		}
	}
}