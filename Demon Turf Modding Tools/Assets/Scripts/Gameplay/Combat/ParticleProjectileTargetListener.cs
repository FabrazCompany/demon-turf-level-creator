﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleProjectileTargetListener : MonoBehaviour
{
    ParticleProjectiles projectiles;
    Collider trigger;

    void Awake () 
    {
        trigger = GetComponent<Collider> ();
        projectiles = GetComponentInParent<ParticleProjectiles> ();
    }
    void OnEnable () 
    {
        trigger.enabled = true;
        
        if (projectiles)
        {
            projectiles.onHit.AddListener (Refresh);
            projectiles.onIgnoreRemoved += Refresh;
        }
        
    }
    void OnDisable () 
    {
        if (projectiles)
        {
            projectiles.onHit.RemoveListener (Refresh);
            projectiles.onIgnoreRemoved -= Refresh;
        }
    }
    void OnTriggerEnter (Collider hit) 
    {
        if (!projectiles) return;
        if (hit.TryGetComponent<ParticleProjectileTarget> (out var particle))
            projectiles.AddColliderInteraction (hit.transform);
    }

    void OnTriggerExit (Collider hit) 
    {
        if (!projectiles) return;
        if (hit.TryGetComponent<ParticleProjectileTarget> (out var particle))
            projectiles.RemoveColliderInteraction (hit.transform);
    }
    void Refresh (Collider hit, Vector3 dir) => Refresh ();
    void Refresh () 
    {
        projectiles.ClearColliderInteractions ();
        trigger.enabled = false;
        StartCoroutine (this.WaitAndDo (0.05f, ()=> {
            trigger.enabled = true;
        }));
    }
}
