﻿using System.Collections.Generic;
using UnityEngine;

public class ParticleProjectiles : FlyweightBehavior<ParticleProjectiles, ParticleProjectilesData, ParticleProjectilesManager>
{   
    ParticleSystem partSystem;
    List<ParticleSystem.Particle> onTriggerParticles;
    [SerializeField] float multiHitDelay = 0.1f;
    [SerializeField] LayerMask detectedLayers;
    
	[SerializeField] bool hasTriggerSound;
	
	[SerializeField, Conditional ("hasTriggerSound")] AudioClipSet triggerSound;
	[SerializeField, Conditional ("hasTriggerSound")] AudioData triggerSoundData = new AudioData (5, 15, true);

	float audioTimer;
	
    
    List<Transform> colliders;
    List<Transform> ignoredColliders;
    Collider[] collidersInfo;
    Dictionary<Collider, float> collidersHit;
    float radiusScale;

    struct HitInfo {
        public Collider hit;
        public float time;
    }

    public UnityEventColliderAndPosition onHit;
    public event System.Action onIgnoreRemoved;
    
    
    void Awake () 
	{
        partSystem = GetComponent<ParticleSystem> ();
        onTriggerParticles = new List<ParticleSystem.Particle> ();
        colliders = new List<Transform> ();
        ignoredColliders = new List<Transform> ();
        collidersHit = new Dictionary<Collider, float> ();
        collidersInfo = new Collider[10];
        radiusScale = partSystem.trigger.radiusScale;
    }

    protected override void OnEnable () 
    {
		base.OnEnable ();
        onTriggerParticles?.Clear ();
        colliders?.Clear ();
        ignoredColliders?.Clear ();

        if (!partSystem)
            return;
        var trigger = partSystem.trigger;
        for (int i = 0; i < trigger.maxColliderCount; i++) 
        {
            trigger.SetCollider (i, null);
        }
    }
    public void AddColliderInteraction (Transform newTr) 
	{
        if (!colliders.Contains (newTr) && !ignoredColliders.Contains (newTr)) 
		{
            colliders.Add (newTr);
            RefreshTriggerColliders ();
        }
    }
    public void RemoveColliderInteraction (Transform newTr) 
	{
        if (colliders.Contains (newTr)) 
		{
            colliders.Remove (newTr);
            RefreshTriggerColliders ();
        }
    }
    public void ClearColliderInteractions () 
    {
        colliders.Clear ();
        RefreshTriggerColliders ();
    }
    void RefreshTriggerColliders () 
	{
        var trigger = partSystem.trigger;
        var ind = 0;
        var coll = trigger.GetCollider (ind);
        while (coll != null)
        {
            trigger.SetCollider (ind, null);
            ind++;
            coll = trigger.GetCollider (ind);
        }

        for (int i = 0; i < colliders.Count; i++) {
            trigger.SetCollider (i, colliders[i]);
        }
    }

    public void IgnoreCollider (Transform tr) 
    {
        if (ignoredColliders.Contains (tr))
            return;
        ignoredColliders.Add (tr);
        RemoveColliderInteraction(tr);
    }
    public void TrackCollider (Transform tr)
    {
        if (!ignoredColliders.Contains (tr))    
            return;
        ignoredColliders.Remove (tr);
        onIgnoreRemoved?.Invoke ();
    }
	bool particlesPresent;
	public void StateUpdate ()
	{
		if (!hasTriggerSound || !triggerSound.HasSound) return;

		if (partSystem.isPlaying)
		{
			audioTimer -= Time.deltaTime * partSystem.main.simulationSpeed;
			if (audioTimer <= 0)
			{
				audioTimer = partSystem.main.startLifetime.constant;
				SoundManagerStub.Instance.PlaySoundGameplay3D (triggerSound.GetSound, transform.position, triggerSoundData);
			}
		}
		else 
			audioTimer = 0;
	}
    void OnParticleTrigger () 
	{
        var hits = ParticlePhysicsExtensions.GetTriggerParticles (partSystem, ParticleSystemTriggerEventType.Enter, onTriggerParticles);
        for (int i = 0; i < hits; i++) 
        {
            var hit = onTriggerParticles[i];
            var pos = hit.position;
            if (partSystem.main.simulationSpace == ParticleSystemSimulationSpace.Local)
                pos = transform.TransformPoint (pos);
            var size = hit.GetCurrentSize (partSystem);
            var totalHits = Physics.OverlapSphereNonAlloc (pos, size * radiusScale, collidersInfo, detectedLayers);
            for (int j = 0; j < totalHits; j++) 
            {
                var collider = collidersInfo[j];
                if (!collidersHit.ContainsKey (collider)) {
                    collidersHit.Add (collider, Time.time + multiHitDelay);
                    onHit?.Invoke (collider, pos);
                } else if (collidersHit[collider] < Time.time) {
                    collidersHit[collider] = Time.time + multiHitDelay;
                    onHit?.Invoke (collider, pos);
                }
            }
        }
    }
}