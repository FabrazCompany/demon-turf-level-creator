﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecayingMine : MonoBehaviour
{
	[SerializeField] float duration = 5;
	[SerializeField] Vector3 scaleTarget = Vector3.zero;
	[SerializeField] Transform parentingRoot;
	[SerializeField] LayerMask platformTarget;
	[SerializeField] float checkDistance = 2;
	[SerializeField] new Light light;
	Vector3 startScale;
	float startLightRange;
	float timer = 0;
	SlowMoEntity slowMo;

	void Awake ()
	{
		startScale = transform.localScale;
		startLightRange = light.range;
		slowMo = GetComponentInParent<SlowMoEntity> ();
	}
	void OnEnable () 
	{
		transform.localScale = startScale;
		light.range = startLightRange;
		timer = 0;

		if (Physics.Raycast (new Ray (transform.position, Vector3.down), out var hitInfo, checkDistance, platformTarget))
		{
			parentingRoot.position = hitInfo.point;
			parentingRoot.SetParent (hitInfo.transform);
		}
	}

	void Update () 
	{
		var delta = Time.deltaTime;
		if (slowMo)
			delta *= slowMo.timeScale;
		
		timer += delta;
		var pct = timer / duration;
		transform.localScale = startScale.Times (Vector3.Lerp (Vector3.one, scaleTarget, pct));
		light.range = Mathf.Lerp (startLightRange, 0, pct);
		if (timer >= duration)
		{
			gameObject.SetActive (false);
		}
	}
}
