using UnityEngine;

public class LineTrap : MonoBehaviour 
{
	[SerializeField] LineRenderer visual;
	[SerializeField] CapsuleCollider trigger;
	[SerializeField] Transform leftPoint;
	[SerializeField] Transform rightPoint;
	[Space]
	[SerializeField] float width = 15f;
	[SerializeField] float expandDuration = 2.5f;
	[SerializeField] float cleanupSpeed = 1;
	[SerializeField] AnimationCurve expandCurve;
	[SerializeField] float activeDuration;
	[SerializeField] ParticleSystem despawnParticle;

	float timer;
	bool triggering;
	bool cleaningUp;
	float pct;

	void Awake () 
	{
		PlayerControllerEvents.onReset += OnPlayerReset;
	}
	void OnDestroy () 
	{
		PlayerControllerEvents.onReset -= OnPlayerReset;
	}
	void OnEnable () 
	{
		timer = 0;
		triggering = true;
		cleaningUp = false;
		leftPoint.localPosition = Vector3.zero;
		rightPoint.localPosition = Vector3.zero;
		trigger.height = 0;
		visual.SetPosition (0, Vector3.zero);
		visual.SetPosition (1, Vector3.zero);
		TrashMan.spawn (despawnParticle.gameObject, transform.position);
	}
	void OnPlayerReset () 
	{
		TrashMan.despawn (gameObject);
	}

	void Update () 
	{
		timer += Time.deltaTime;

		if (triggering)
		{
			Scale (timer / expandDuration);
			if (timer >= expandDuration)
			{
				timer = 0;
				triggering = false;
			}
				
		}
		else if (cleaningUp)
		{	
			Scale (pct - (cleanupSpeed * Time.deltaTime));
			if (pct <= 0)
			{
				TrashMan.spawn (despawnParticle.gameObject, leftPoint.position);
				TrashMan.spawn (despawnParticle.gameObject, rightPoint.position);
				TrashMan.despawn (gameObject);
			}
		}
		else 
		{
			if (timer >= activeDuration)
			{
				timer = 0;
				cleaningUp = true;
			}
		}
	}

	void Scale (float progress)
	{
		pct = progress;
		var size = Mathf.Lerp (0, width, progress);
		leftPoint.localPosition = -Vector3.right * size * 0.5f;
		rightPoint.localPosition = Vector3.right * size * 0.5f;
		trigger.height = size + 1;
		visual.SetPosition (0, leftPoint.localPosition);
		visual.SetPosition (1, rightPoint.localPosition);
	}

	public void Cleanup ()
	{
		timer = 0;
		cleaningUp = true;
	}

}