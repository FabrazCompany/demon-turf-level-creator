﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Projectile : MonoBehaviour, ISpinnable, ISlowable, IHazard
{
    [Header ("Events")]
    [SerializeField] UnityEventCollider onCollision;
    [SerializeField] UnityEvent onSelfDestruct;
    [SerializeField] ReflectProjectilesEvent onReflect;

    [Header ("Reflection Effects")]
    [SerializeField] GameObject reflectFeedbackPrefab;
    [SerializeField] float reflectionVelocityAdjust = 1;
    [SerializeField] float reflectionScaleAdjust = 1;

    [Header ("Collision Safety Check")]
    [SerializeField] bool useSafetyCheck;
    [SerializeField, Conditional ("useSafetyCheck")] LayerMask layerMask;

	[Header ("Navigation Avoidance")]
	[SerializeField] float navAvoidanceSide = 1.5f;
	[SerializeField] float navAvoidanceSpeedScalar = 1;
    const float ignoreProjectileCollisionDuration = 0.2f;

    float slowdownTimer;
    GameObject activeTimer;
    bool slowdownActive => slowdownTimer > Time.time;

    float fireTime;
    Renderer rend;
    Rigidbody rigid;
    Transform ignoredRoot;
    Collider hitbox;
    SlowMoEntity entityTime;
	List<Collider> ignoredColliders;
	HasGravity gravity;
#if !DT_EXPORT && !DT_MOD
	Pathfinding.NavmeshCut navAvoidance;
	Pathfinding.RVO.RVOSquareObstacle navObstacle;
#endif

    float force;
	bool reflected;
    public Rigidbody getRigid => rigid;
    public float getSize => transform.lossyScale.magnitude;
    public float getForce => force;
	public List<Collider> getIgnoredColliders => ignoredColliders;
    public ReflectProjectilesEvent getOnReflect => onReflect;
	public void SetForce (float _force) => force = _force;

    void Awake () 
    {
        rend = GetComponentInChildren<Renderer> ();
        rigid = GetComponent<Rigidbody> ();
        hitbox = GetComponent<Collider> ();
        entityTime = GetComponent<SlowMoEntity> ();
		ignoredColliders = new List<Collider> ();
#if !DT_EXPORT && !DT_MOD
		navAvoidance = GetComponentInChildren<Pathfinding.NavmeshCut> ();
		navObstacle = GetComponentInChildren<Pathfinding.RVO.RVOSquareObstacle> ();
#endif
		gravity = GetComponent<HasGravity> () ?? gameObject.AddComponent<HasGravity> ();
    }
    void OnEnable () 
    {
		reflected = false;
		ignoredColliders.Clear ();
        ResetSlowdown ();
        fireTime = Time.time;
    }
    void OnDisable ()
	{
        if (rigid)
		{
			rigid.velocity = Vector3.zero;
			rigid.angularVelocity = Vector3.zero;
		}
        ignoredRoot = null;
	}
	void Update () 
	{
		var deltaTime = Time.deltaTime;
        if (entityTime)
            deltaTime *= entityTime.timeScale;
	}
    void FixedUpdate () 
    {
        var activeForce = force;
        var deltaTime = Time.deltaTime;
        if (entityTime)
            deltaTime *= entityTime.timeScale;
        if (useSafetyCheck) 
        {            
            if (Physics.Raycast (transform.position, transform.forward, out var hitInfo, activeForce * deltaTime, layerMask, QueryTriggerInteraction.Collide) 
                && (!hitInfo.collider.isTrigger 
                    || hitInfo.collider.GetComponent<ReflectProjectiles> ()))
            {
                activeForce = hitInfo.distance / deltaTime;
            }
        }
        if (entityTime)
            activeForce *= entityTime.timeScale;
        rigid.velocity = transform.forward * activeForce;
    }
	
    void OnCollisionEnter(Collision coll) 
	{
		if (coll.collider.GetComponent<Projectile> () && reflected)
			return;
		Contact (coll.collider);
	}
    public void OnTriggerEnter (Collider hit) 
    {
        if (!ShouldContact (hit))
            return;
        Contact (hit);
    }
    bool ShouldContact (Collider hit) 
    {  

		if (hit.transform.IsChildOf (transform))
			return false;
		if (ignoredColliders.Contains (hit))
			return true;
			
		if (hit.GetComponent<Projectile> ())
			return !reflected;
        else if (hit.isTrigger)
        {
            if (hit.GetComponent<DeliverKill> ())
                return true;
            return false;
        }
        else 
            return true;
    }
    void Contact (Collider hit)
    {
        if (ignoredRoot && hit.transform.IsChildOf (ignoredRoot))
            return;

        if (hit?.attachedRigidbody?.GetComponent<Projectile> () 
            && fireTime + ignoreProjectileCollisionDuration > Time.time)
        {
            return;
        }
        onCollision?.Invoke (hit);
    }
    public void FreezeMomentum () 
    {
        rigid.velocity = Vector3.zero;
    }
    public void SetIgnoredRoot (Transform _object) => ignoredRoot = _object;
	public void AddIgnoredColliders (List<Projectile> others)
	{
		foreach (var other in others)
		{
			if (ignoredColliders.Contains (other.hitbox) || other == this)
				continue;
			ignoredColliders.Add (other.hitbox);
			Physics.IgnoreCollision (hitbox, other.hitbox, true);
		}
		
	}
	public void AddIgnoredColliders (List<Collider> others)
	{
		foreach (var other in others)
		{
			if (ignoredColliders.Contains (other))
				continue;
			ignoredColliders.Add (other);
			Physics.IgnoreCollision (hitbox, other, true);
		}
		
	}
    public void Format (float size, Vector3 facing)
    {
        transform.localScale = Vector3.one * size;
        transform.forward = facing;
    }
    public void LaunchProjectile (float _force) 
    {
        force = _force;
#if !DT_EXPORT && !DT_MOD
		if (navAvoidance != null)
		{
			var forward = force * navAvoidanceSpeedScalar;
			navAvoidance.rectangleSize = new Vector2 (navAvoidanceSide, forward + (getSize * 0.5f));
			navAvoidance.center = Vector3.forward * ((forward * 0.5f) - (getSize * 0.5f));
		}
		if (navObstacle != null)
		{
			var forward = force * navAvoidanceSpeedScalar;
			navObstacle.size = new Vector2 (navAvoidanceSide, forward + (getSize * 0.5f));
			navObstacle.center = Vector2.up * ((forward * 0.5f) - (getSize * 0.5f));
		}
#endif
    }
    public void ResetSlowdown () {
        slowdownTimer = 0;
        if (activeTimer != null) {
            TrashMan.despawn (activeTimer);
            activeTimer = null;
        }
        
    }
    public void ApplySlowdown () {
        if (activeTimer != null) {
            TrashMan.despawn (activeTimer);
        }
        activeTimer.transform.localScale = Vector3.one;
        activeTimer.transform.SetParent (transform);
        this.RestartCoroutine (SlowdownMat (), ref slowdownMatRoutine);
    }

    public void Reflect (ReflectProjectiles reflector, Vector3 newDir, float overrideForce)
    {
        transform.forward = newDir;
		reflected = true;
		force *= reflector.getReflectVelocityScalar;
		transform.localScale *= reflector.getReflectSizeScalar;
        rigid.velocity = newDir * rigid.velocity.magnitude;
        if (reflectFeedbackPrefab)
            TrashMan.spawn (reflectFeedbackPrefab, transform.position);
        onReflect?.Invoke (reflector);
        if (rigid.velocity.sqrMagnitude < overrideForce * overrideForce)
            rigid.velocity = newDir * overrideForce;

        ignoredRoot = null;
		foreach (var other in ignoredColliders)
		{
			if (other)
				Physics.IgnoreCollision (hitbox, other, false);
		}
		ignoredColliders.Clear ();
    }

    Coroutine slowdownMatRoutine;
    IEnumerator SlowdownMat () 
    {
        yield return new WaitUntil (() => slowdownTimer <= Time.time);
    }
    public void Detonate () {
        if (rigid)
            rigid.velocity = Vector3.zero;
        onSelfDestruct?.Invoke ();
        TrashMan.despawn (gameObject);
    }
}
