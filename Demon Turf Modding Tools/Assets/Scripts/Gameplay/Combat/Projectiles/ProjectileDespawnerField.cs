﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileDespawnerField : MonoBehaviour
{
	[SerializeField] bool triggerOnEnter = false;
	[SerializeField] bool triggerOnExit = true;
    void OnTriggerEnter (Collider other) 
	{
		if (triggerOnEnter)	
			CheckDetonation (other);
    }
	void OnTriggerExit (Collider other) 
	{
		if (triggerOnExit)	
			CheckDetonation (other);
    }

	public void CheckDetonation (Collider other)
	{
		var projectile = other.GetComponent<Projectile> ();
        if (projectile == null)
            return;
        
		projectile.Detonate ();
	}
}
