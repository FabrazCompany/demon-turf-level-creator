using UnityEngine;

public class ProjectileMod_Accelerating : MonoBehaviour
{	
	[SerializeField] Rigidbody rigid;
	[SerializeField] Projectile projectile;
	[SerializeField] SlowMoEntity slowMo;
	[SerializeField] float delay = 1f;
	[SerializeField] float accelerateDuration = 2f;
	[SerializeField] float accelerationScaling = 4;
	[SerializeField] AnimationCurve accelerationCurve = AnimationCurve.Linear (0,0,1,1);

	float timer;
	bool triggered;
	float force;

	void OnValidate ()
	{
		if (!rigid)
			rigid = GetComponent<Rigidbody> ();
		if (!projectile)
			projectile = GetComponent<Projectile> ();
		if (!slowMo)
			slowMo = GetComponent<SlowMoEntity> ();
	}

	void OnEnable ()
	{
		triggered = false;
		timer = delay;
	}	

	void Update ()
	{
		var delta = Time.deltaTime * slowMo.timeScale;
		timer -= delta;
		if (timer <= 0)
		{
			if (triggered)
				enabled = false;
			else 
			{
				triggered = true;
				force = projectile.getForce;
				timer = accelerateDuration;
			}
		}
	}
	void FixedUpdate ()
	{
		if (!triggered)
			return;
		
		projectile.SetForce (Mathf.Lerp (force, force * accelerationScaling, accelerationCurve.Evaluate (1 - (timer / accelerateDuration))));
	}
}