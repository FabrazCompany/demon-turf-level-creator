using UnityEngine;

public class ProjectileMod_BlinkBomb : MonoBehaviour
{
    [SerializeField] float blinkDuration = 0.05f;
    [SerializeField] Material normalMat;
    [SerializeField] Material blinkMat;
    float inactiveDuration;
    float activeDuration;
    float blinkRateStart;
    float blinkRateEnd;

    float triggerTimer;
    bool blinkTimerActive;

    float blinkTimer;
    bool blinkActive;

    Projectile projectile;
    MeshRenderer rend;
	SlowMoEntity slowMo;
	[SerializeField] AudioClip BlinkBeepClip;

    void Awake () 
    {
        projectile = GetComponent<Projectile> ();
        rend = GetComponent<MeshRenderer> ();
		slowMo = GetComponent<SlowMoEntity> ();
    }
    void Update () 
    {
        var delta = slowMo ? slowMo.timeScale * Time.deltaTime : Time.deltaTime;
        triggerTimer -= delta;
        if (triggerTimer <= 0)
        {
            if (!blinkTimerActive)
                TriggerBlinkPhase ();
            else 
                projectile.Detonate ();
        }

        if (blinkTimerActive)
        {
            blinkTimer -= delta;
            if (blinkTimer <= 0)
            {
                blinkActive = !blinkActive;
                blinkTimer = blinkActive ? blinkDuration : Mathf.Lerp (blinkRateStart, blinkRateEnd, 1 - (triggerTimer / activeDuration));
                rend.material = blinkActive ? blinkMat : normalMat;
				SoundManagerStub.Instance.PlaySoundGameplay3D (BlinkBeepClip, transform.position, data: new AudioData(_volume:.3f, _min:5f, _max:25f));
            }
        }
    }

    public void Init (float _inactiveDuration, float _activeDuration, float _blinkRateStart, float _blinkRateEnd)
    {
        inactiveDuration = _inactiveDuration;
        activeDuration = _activeDuration;
        blinkRateStart = _blinkRateStart;
        blinkRateEnd = _blinkRateEnd;
        triggerTimer = inactiveDuration;
        blinkTimerActive = blinkActive = false;
		rend.material = normalMat;
    }

    public void TriggerBlinkPhase () 
    {  
        if (blinkTimerActive)
            return;
        blinkTimerActive = true;
        triggerTimer = activeDuration;
		
    }
}