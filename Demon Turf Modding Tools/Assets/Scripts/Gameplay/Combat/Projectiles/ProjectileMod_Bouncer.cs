using UnityEngine;

public class ProjectileMod_Bouncer : MonoBehaviour
{
    [SerializeField] float bounceScale = 2;
    [SerializeField] float bounceNoise;
    Rigidbody rigid;
    Vector3 lastVel;

    void Awake () 
    {
        rigid = GetComponent<Rigidbody> ();
    }

    void FixedUpdate () 
    {
        lastVel = rigid.velocity;
    }
    void OnCollisionEnter (Collision info)
    {
        if (info.contactCount == 0)
            return;
        
        var dir = info.GetContact (0).normal;
        dir += AddNoiseOnAngle (-bounceNoise, bounceNoise);
        rigid.AddForce (Vector3.Reflect (lastVel.normalized, dir) * lastVel.magnitude * bounceScale, ForceMode.VelocityChange);
    }

    Vector3 AddNoiseOnAngle ( float min, float max)  {
        float xNoise = Random.Range (min, max);
        float yNoise = Random.Range (min, max);
        // float zNoise = 0;
        // Now get the angle between w.r.t. a vector 3 direction
        Vector3 noise = new Vector3 (
            Mathf.Sin (2f * 3.1415926f * xNoise / 360), 
            Mathf.Sin (2f * 3.1415926f * yNoise / 360), 
                            0
                        );
        return noise;
    }
}