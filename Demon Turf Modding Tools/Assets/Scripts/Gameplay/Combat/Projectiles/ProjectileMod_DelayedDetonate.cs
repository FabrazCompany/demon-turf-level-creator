using UnityEngine;

public class ProjectileMod_DelayedDetonate : MonoBehaviour
{
	Projectile projectile;
	SlowMoEntity slowMo;
	float timer;

	[SerializeField] bool disableOnReflect = true;

	void Awake () 
	{
		projectile = GetComponent<Projectile> ();
		slowMo = GetComponent<SlowMoEntity> ();

		if (disableOnReflect)
			projectile.getOnReflect.AddListener ((reflector) => enabled = false );
	}

	public void Initialize (float _timer) 
	{
		timer = _timer;
		enabled = true;
	}
	void Update ()
	{
		var delta = slowMo ? slowMo.timeScale * Time.deltaTime : Time.deltaTime;
		timer -= delta;
		if (timer <= 0)
		{
			projectile.Detonate ();
			enabled = false;
		}
	}

}