using UnityEngine;
using System.Collections;

public class ProjectileMod_Laser : MonoBehaviour 
{
    [SerializeField] LayerMask hitLayer;
    [SerializeField] LineRendererAnimator linePrefab;
    [SerializeField] UnityEventCollider onHit;
    [SerializeField] float slowdownScale = .25f;
    [SerializeField] ParticleSystem reflectionSpark;
    
     
    Projectile projectile;
    DeliverKill kill;
    DeliverKnockback knocker;
    LineRendererAnimator line;
    ParticleSystem spark;
    Timer timer;
    float activeDuration = 10;
    float cleanupDuration = .5f;
	bool reflectActive;
    void Awake () 
    {
        projectile = GetComponent<Projectile> ();
        timer = GetComponent<Timer> ();
        kill = GetComponent<DeliverKill> ();
        knocker = GetComponent<DeliverKnockback> ();
    }
    void OnValidate () 
    {
        if (slowdownScale <= 0)
            slowdownScale = 0.1f;
    }
    void OnDisable () 
    {
        if (reflectRoutine != null)
        {
            StopCoroutine (reflectRoutine);
            if (Game.timeScale == slowdownScale)
                Game.timeScale = Game.defaultTime;
        }
        if (spark && spark.isEmitting)
		{
			spark.Stop ();
			Debug.Log ("On Disable - Stopping Spark", this);
		}	
    }
    public void Init (float _activeDuration, float _cleanupDuration, float _size, float _length)
    {
        if (line)
		{
			TrashMan.despawn (line.gameObject);
			line = null;
		}            
		if (spark)
		{
			spark.Stop ();
			TrashMan.despawn (spark.gameObject);
			spark = null;
		}
			
			
		reflectActive = false;
        activeDuration = _activeDuration;
        cleanupDuration = _cleanupDuration;
        timer.SetTimer (activeDuration);

        kill?.SetMute (false);
        knocker?.SetMute (false);
        
        line = TrashMan.spawn (linePrefab.gameObject, transform.position, transform.rotation).GetComponent<LineRendererAnimator> ();
		line.transform.localScale = Vector3.one;
        line.transform.SetParent (transform);
        line.transform.localEulerAngles = Vector3.zero;		
		line.SetIgnores (projectile.getIgnoredColliders);
        line.SetWidth (_size);
        line.SetLength (_length);
        line.SetTimer (activeDuration);
        line.SetOrigin ();
    }
    public void DetachTrail () => line?.Detach (cleanupDuration);
    public void OnReflect (ReflectProjectiles reflector) 
    {
		if (!gameObject.activeInHierarchy) return;
        if (reflectRoutine != null)
            StopCoroutine (reflectRoutine);
        reflectRoutine = StartCoroutine (ReflectBehaviour (reflector));
    }

    Coroutine reflectRoutine;
    IEnumerator ReflectBehaviour (ReflectProjectiles reflector) 
    {
        if (reflector.getReflectType == ReflectProjectiles.ReflectType.PlayerSpinAttack)
            Game.timeScale = slowdownScale;
        
        enabled = false;
		reflectActive = true;
        line.StartReflect ();
        projectile.enabled = false;
        projectile.getRigid.velocity = Vector3.zero;
        kill?.SetMute (true);
        knocker?.SetMute (true);
		if (spark)
		{
			spark.Stop ();
			TrashMan.despawn (spark.gameObject);
		}			
        spark = TrashMan.spawn (reflectionSpark.gameObject, transform.position, transform.rotation).GetComponent<ParticleSystem> ();
		spark.Play (true);
		Debug.Log ("Playing Spark", this);
        while (line.getReflecting)
        {
            if (reflector.getReflectType == ReflectProjectiles.ReflectType.PlayerSpinAttack)
                Game.timeScale = slowdownScale;

            projectile.getRigid.velocity = Vector3.zero;
            line.UpdateReflect ();            
            yield return null;
        }
		line.ClearIgnores (projectile.getIgnoredColliders);
        spark.Stop ();
		Debug.Log ("Coroutine - Stopping Spark", this);
        kill?.SetMute (false);
        knocker?.SetMute (false);
        projectile.enabled = true;
        
        if (reflector.getReflectType == ReflectProjectiles.ReflectType.PlayerSpinAttack)
            Game.timeScale = Game.defaultTime;
        
		reflectActive = false;
        enabled = true;
    }    
}