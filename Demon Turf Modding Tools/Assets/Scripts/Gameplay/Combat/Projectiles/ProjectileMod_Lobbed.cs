﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileMod_Lobbed : MonoBehaviour
{
    float duration = 1.5f;
    Rigidbody rigid;
	SlowMoEntity slowMo;
    float gravity;

	HasGravity gravityComp;

    void Awake ()
    {
        rigid = GetComponent<Rigidbody> ();
		slowMo = rigid.GetComponent<SlowMoEntity> ();
		gravityComp = GetComponent<HasGravity> () ?? gameObject.AddComponent<HasGravity> ();
		gravityComp.enabled = false;
    }    
    void Start () 
    {
        var timer = GetComponent<Timer> ();
        timer?.SetTimer (duration, false);
    }
	void OnEnable () 
	{
		gravityComp.enabled = false;
	}
	void OnDisable () 
	{
		gravityComp.enabled = true;
	}

    public void SetGravity (float _gravity) => gravity = _gravity;
    public void SetDuration (float _duration) => duration = _duration;
    void FixedUpdate () 
    {
        rigid.AddForce (0, -gravity * slowMo.timeScale, 0, ForceMode.Acceleration);
    }
}
