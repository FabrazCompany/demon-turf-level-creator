using UnityEngine;
using UnityEngine.Events;

[RequireComponent (typeof (Projectile))]
public class ProjectileMod_Split : MonoBehaviour
{
    public enum SplitType 
    {
        Horizontal = 0,
        Vertical = 1,
        Cone = 10,   //full circle or randomized within min-max (buckshot)
    }
    [Header ("Split Behaviour")]
    [SerializeField] SplitType splitType;
    [SerializeField] float duration;
    [SerializeField] int splitNumber;
    [SerializeField, Range (5, 180)] float splitSpread;
    [SerializeField] UnityEvent onSplit;
    
    [Header ("Spawned Projectiles")]
    [SerializeField] Projectile spawnedProjectile;
    [SerializeField] float spawnOffset;
    [SerializeField] float spawnedSize;
    [SerializeField] bool scaleForceFromSpawner;
    [SerializeField] float spawnedForce;    

    [Header ("FX")]
    [SerializeField] Transform splitIndicatorRoot;
    [SerializeField] Transform splitIndicator;
    [SerializeField] float spinSpeed;
    
    
    Projectile baseProjectile;
    float offset;
    float timer;

    void Awake ()
    {
        baseProjectile = GetComponent<Projectile> ();
        offset = splitIndicator.localPosition.y;
        SpawnSplitIndicators ();
    }
    void Validate () 
    {
        splitNumber = Mathf.Max (splitNumber, 2);
    }
    void SpawnSplitIndicators () 
    {
        var splitAngle = 360 / splitNumber;
        for (int i = 1; i < splitNumber; i++)
        {
            var newSplitIndicator = Transform.Instantiate (splitIndicator);
            newSplitIndicator.SetParent (splitIndicatorRoot);
            newSplitIndicator.localPosition = splitIndicator.localPosition;
            newSplitIndicator.RotateAround (splitIndicatorRoot.position, splitIndicatorRoot.forward, i * splitAngle);
            newSplitIndicator.localPosition = newSplitIndicator.up * offset;
            newSplitIndicator.up = -(splitIndicatorRoot.position - newSplitIndicator.position).normalized;
        }
    }
    
    void OnEnable () 
    {
        timer = duration;
    }

    void Update () 
    {
        var deltaTime = Time.deltaTime;
        timer -= deltaTime;

        splitIndicatorRoot.Rotate ((splitIndicatorRoot.forward + splitIndicatorRoot.right).normalized, spinSpeed * deltaTime);
        for (int i = 0; i < splitIndicatorRoot.childCount; i++) 
        {
            var child = splitIndicatorRoot.GetChild (i);
            child.localPosition = child.up * Mathf.Lerp (offset, 0, 1 - (timer / duration));
        }

        if (timer <= 0)
        {
            
            TriggerSplit ();
            baseProjectile.Detonate ();
        }
    }

    public void AssignValues (SplitType _splitType, int _splitNumber, float _duration, float _spread) 
    {
        splitType = _splitType;
        splitNumber = _splitNumber;
        duration = _duration;
        splitSpread = _spread;
        timer = duration;
    }
    public void TriggerSplit () 
    {
        switch (splitType)
        {
            case SplitType.Horizontal:
            case SplitType.Vertical:
                var angleIncrement = splitSpread / splitNumber;
                var axis = splitType == SplitType.Horizontal ? transform.up : transform.right;
                var spawnPos = rotatePointAroundAxis (transform.forward * spawnOffset, (splitSpread * -0.5f) + (angleIncrement * 0.5f), axis);
                for (int i = 0; i < splitNumber; i++) 
                {
                    var projectile = TrashMan.spawn (spawnedProjectile.gameObject, transform.position + spawnPos, Quaternion.identity).GetComponent<Projectile> ();
                    projectile.Format (spawnedSize, (projectile.transform.position - transform.position).normalized);
                    projectile.LaunchProjectile (scaleForceFromSpawner ? baseProjectile.getForce * spawnedForce : spawnedForce);
                    spawnPos = rotatePointAroundAxis (spawnPos, angleIncrement, axis);
                } 
                break;

            case SplitType.Cone:
                break;
        }
        onSplit?.Invoke ();
    }

    Vector3 rotatePointAroundAxis(Vector3 point, float angle, Vector3 axis)
    {
        Quaternion q = Quaternion.AngleAxis(angle, axis);
        return q * point; //Note: q must be first (point * q wouldn't compile)
    }

}