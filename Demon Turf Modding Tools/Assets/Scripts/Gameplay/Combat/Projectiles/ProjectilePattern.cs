﻿using UnityEngine;

public abstract class ProjectilePattern : MonoBehaviour
{
    public abstract int getShotsPerTrigger { get; }
    public abstract void UpdateFirePoints ();
	public virtual Vector3 getFirePosition (int ind) => Vector3.zero;
	public virtual Vector3 getFireDirection (int ind) => Vector3.forward;
	public virtual void GetPatternInfo (Transform firePointRoot, int firePointInd, out Vector3 position, out Vector3 direction) 
	{
		position = firePointRoot.position + firePointRoot.TransformDirection (getFirePosition(firePointInd));
		direction = firePointRoot.TransformDirection (getFireDirection(firePointInd));
	}

    public float debug_getBulletSize => GetComponent<ProjectileSpawner> ()?.getBulletSize * 0.5f ??  0.35f;
    public float debug_getBulletSpeed => GetComponent<ProjectileSpawner> ()?.getBulletSpeed ??  1.25f;
	public abstract void DrawDebug ();

	
	[SerializeField] protected bool drawDebug;
	#if UNITY_EDITOR
	void OnDrawGizmos () 
	{
		if (!drawDebug)
			return;
		DrawDebug ();
	}
	#endif

}

public abstract class ProjectilePattern_Complex : ProjectilePattern
{
    public enum FireType {
        Burst,
        Sequential,
        Subset,
    }
    protected enum RepeatType {
        Loop,
        PingPong,
    }
    

    [Header ("General Settings")]
    [SerializeField] protected FireType fireType;
    [SerializeField, Conditional ("fireType", FireType.Burst, true)] protected RepeatType repeatType;
    [SerializeField, Conditional ("fireType", FireType.Subset), Delayed] protected int subsetFrequency; 
    
    protected int index;
    protected int direction;

    public FireType getFireType => fireType;
	protected abstract int getFirePointQuantity_DEPRECATED { get; }

    public override int getShotsPerTrigger => fireType == FireType.Burst ? getFirePointQuantity_DEPRECATED : 
                                    fireType == FireType.Subset ? getFirePointQuantity_DEPRECATED / subsetFrequency : 
                                    1;  

    protected virtual void OnValidate () {
        if (subsetFrequency > getFirePointQuantity_DEPRECATED)
            subsetFrequency = getFirePointQuantity_DEPRECATED;
        if (subsetFrequency <= 0)
            subsetFrequency = 2;
        while (getFirePointQuantity_DEPRECATED % subsetFrequency != 0) {
            subsetFrequency++;
        }
    }


    protected virtual void Awake () {
        index = 0;
        direction = 1;
    }

    protected void CycleIndex () {
        var targetVal = fireType == FireType.Subset ? subsetFrequency : getFirePointQuantity_DEPRECATED;
        if (index + direction < 0) {
            if (repeatType == RepeatType.PingPong) {
                direction = 1;
            } else {
                index = targetVal;
            }
        } else if (index + direction > targetVal - 1) {
            if (repeatType == RepeatType.PingPong) {
                direction = -1;
            } else {
                index = -1;
            }
        }
        index = Mathf.Clamp (index + direction, 0, targetVal - 1); 
    }
}