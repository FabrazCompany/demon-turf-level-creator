using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectilePattern_Custom : ProjectilePattern_Complex
{
    [SerializeField] Transform[] firePointTransforms;
    [SerializeField] float positionOffset;

    Vector3[] firePositions;
    Vector3[] fireDirections;
    int firePointQuantity;

	public override Vector3 getFirePosition (int ind) => firePositions[ind];
	public override Vector3 getFireDirection (int ind) => fireDirections[ind];
    protected override int getFirePointQuantity_DEPRECATED => firePointTransforms.Length;

    protected override void Awake () {
        base.Awake ();
        firePointQuantity = firePointTransforms.Length;
        switch (fireType) {
            case FireType.Sequential:
                firePositions = new Vector3[1];
                fireDirections = new Vector3[1];
                UpdateFirePoints ();
                break;
            
            case FireType.Burst:
                firePositions = new Vector3[firePointQuantity];
                fireDirections = new Vector3[firePointQuantity];
                for (int i = 0; i < firePointQuantity; i++) {
                    firePositions[i] = firePointTransforms[i].localPosition;
                    fireDirections[i] = firePointTransforms[i].forward;
                }
                break;
            
            case FireType.Subset:
                firePositions = new Vector3[firePointQuantity / subsetFrequency];
                fireDirections = new Vector3[firePointQuantity / subsetFrequency];
                UpdateFirePoints ();
                break;
        }
    }

    public override void UpdateFirePoints () {
        switch (fireType) {
            case FireType.Sequential:
                firePositions[0] = firePointTransforms[index].localPosition + (firePointTransforms[index].forward * positionOffset);
                fireDirections[0] = firePointTransforms[index].forward;
                CycleIndex ();
                break;
            case FireType.Subset:
                var length = firePointQuantity / subsetFrequency;
                int j = index;
                for (int i = 0; i < length; i++) {
                    firePositions[i] = firePointTransforms[j].localPosition + (firePointTransforms[j].forward * positionOffset);
                    fireDirections[i] = firePointTransforms[j].forward;
                    j += subsetFrequency;
                }
                CycleIndex ();
                break;            
            
        }
    }

	public override void DrawDebug () {
		Gizmos.color = Color.red;
        var debugLineLength = debug_getBulletSpeed;
        var length = firePointTransforms.Length;
        for (int i = 0; i < length; i++) 
        {
            var pos = transform.position + firePointTransforms[i].localPosition + (firePointTransforms[i].forward * positionOffset);
            var heatCol = fireType == FireType.Sequential ? Color.Lerp (Color.red, Color.white, (float)i / (float)(firePointTransforms.Length - 1)) 
                        : fireType == FireType.Subset ? Color.Lerp (Color.red, Color.white, (float)(i % subsetFrequency) / (float)(subsetFrequency - 1))
                        : Color.red;
            //draw pattern lines
            Gizmos.color = heatCol;
            if (i < length - 1) {
                var nextPos = transform.position + firePointTransforms[i + 1].localPosition + (firePointTransforms[i + 1].forward * positionOffset);
                Gizmos.DrawLine(pos, nextPos);
            } else if (repeatType == RepeatType.Loop) {
                var originalPos = transform.position + firePointTransforms[0].localPosition + (firePointTransforms[0].forward * positionOffset);
                Gizmos.DrawLine(pos, originalPos);
            }

            //draw fire line
            Gizmos.color = Color.red;
            var lineLength = debugLineLength;
            if (fireType == FireType.Sequential) 
                lineLength = Mathf.Lerp (debugLineLength, debugLineLength * 0.15f, (float)i / (float)length);
            var forward = ((pos) - transform.position).normalized;
            Gizmos.DrawLine (pos, pos + (forward * lineLength));

            //draw fire point
            Gizmos.color = heatCol;
            Gizmos.DrawSphere (pos, debug_getBulletSize);
        }
    }

}