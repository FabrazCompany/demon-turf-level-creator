using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectilePattern_Group : ProjectilePattern
{
	public override Vector3 getFirePosition (int ind) => firePoints[ProcessIndex (ind, out var subInd)].getFirePosition (subInd);
	public override Vector3 getFireDirection (int ind) => firePoints[ProcessIndex (ind, out var subInd)].getFireDirection (subInd);

	int ProcessIndex (int ind, out int subIndex) 
	{
		int baseInd = 0;
		int subInd = 0;
		foreach (var pattern in firePoints)
		{
			if (subInd + pattern.getShotsPerTrigger > ind) break;
			subInd += pattern.getShotsPerTrigger;
			baseInd++;
		}
		subIndex = ind - subInd;
		return baseInd;
	}
    public override int getShotsPerTrigger => firePointsLength;
	[SerializeField] ProjectilePattern[] firePoints;
	int firePointsLength;
	void Awake () 
	{
		firePointsLength = 0;
		foreach (var pattern in firePoints)
		{
			firePointsLength += pattern.getShotsPerTrigger;
		}
	}
	public override void GetPatternInfo (Transform firePointRoot, int firePointInd, out Vector3 position, out Vector3 direction) 
	{
		var baseInd = ProcessIndex (firePointInd, out var subInd);
		var pos = getFirePosition(firePointInd);
		position = firePoints[baseInd].transform.position;
		var dir = getFireDirection(firePointInd);
		direction = firePoints[baseInd].transform.TransformDirection (dir);
	}
	public override void UpdateFirePoints () 
	{
		for (int i = 0; i < firePoints.Length; i++)
		{
			firePoints[i].UpdateFirePoints ();
		}
	}

	[SerializeField, Conditional ("drawDebug")] float debugBulletSize = 0.1f;
	public override void DrawDebug () 
	{
		foreach (var pattern in firePoints)
		{
			pattern.DrawDebug ();
		}
	}
}