using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectilePattern_Line : ProjectilePattern_Complex
{
    [SerializeField] protected bool reverseFiringOrder;
    [Header ("Pattern Settings")]
    [SerializeField] Orientation spreadOrientation;
    [SerializeField] Orientation shotOrientation;
    [SerializeField, Delayed] int firePointQuantity;

    [SerializeField] float firePointSpacing;

    //  [Header ("Debugging")]
    // [SerializeField] float debugLineLength = 1;
    // [SerializeField] float debugSphereSize = 0.05f;

    Vector3[] firePoints;
    Vector3[] firePositions;
    Vector3[] fireDirections;

    // public override Vector3[] getFirePositions => firePositions;
    // public override Vector3[] getFireDirections => fireDirections;
	public override Vector3 getFirePosition (int ind) => firePositions[ind];
	public override Vector3 getFireDirection (int ind) => fireDirections[ind];
    protected override int getFirePointQuantity_DEPRECATED => firePointQuantity;

    protected override void OnValidate() {
        base.OnValidate ();
        firePointQuantity = Mathf.Max (firePointQuantity, 1);
    }

    protected override void Awake () {
        base.Awake ();
        firePoints = FzMath.CalculateLinePoints (transform, spreadOrientation, firePointQuantity, firePointSpacing, reverseFiringOrder);

        
        switch (fireType) {
            case FireType.Sequential:
                firePositions = new Vector3[1];
                fireDirections = new Vector3[1];
                UpdateFirePoints ();
                break;
            
            case FireType.Burst:
                firePositions = new Vector3[firePointQuantity];
                fireDirections = new Vector3[firePointQuantity];
                var fireDir = shotOrientation == Orientation.x ? transform.right :
                        shotOrientation == Orientation.y ? transform.up :
                        transform.forward;

                for (int i = 0; i < firePointQuantity; i++) {
                    firePositions[i] = firePoints[i];
                    fireDirections[i] = fireDir;
                }
                break;
            
            case FireType.Subset:
                firePositions = new Vector3[firePointQuantity / subsetFrequency];
                fireDirections = new Vector3[firePointQuantity / subsetFrequency];
                UpdateFirePoints ();
                break;
        }
    }

    public override void UpdateFirePoints () {
        var fireDir = shotOrientation == Orientation.x ? transform.right :
                        shotOrientation == Orientation.y ? transform.up :
                        transform.forward;

        switch (fireType) {
            case FireType.Sequential:
                firePositions[0] = firePoints[index];
                fireDirections[0] = fireDir;
                CycleIndex ();
                break;
            case FireType.Subset:
                var length = firePointQuantity / subsetFrequency;
                int j = index;
                for (int i = 0; i < length; i++) {
                    firePositions[i] = firePoints[j];
                    fireDirections[i] = fireDir; 
                    j += subsetFrequency;
                }
                CycleIndex ();
                break;                    
        }
    }

    public override void DrawDebug () {
        if (firePointQuantity < 2)
            return;

        var points = FzMath.CalculateLinePoints (transform, spreadOrientation, firePointQuantity, firePointSpacing, reverseFiringOrder);
        var direction = shotOrientation == Orientation.x ? transform.right :
                        shotOrientation == Orientation.y ? transform.up :
                        transform.forward;

        var debugLineLength = debug_getBulletSpeed;
        for (int i = 0; i < firePointQuantity; i++) 
        {
            var heatCol = fireType == FireType.Sequential ? Color.Lerp (Color.red, Color.white, (float)i / (float)(firePointQuantity - 1)) 
                        : fireType == FireType.Subset ? Color.Lerp (Color.red, Color.white, (float)(i % subsetFrequency) / (float)(subsetFrequency - 1))
                        : Color.red;
            //draw pattern lines
            Gizmos.color = heatCol;
            if (i < firePointQuantity - 1) {
                Gizmos.DrawLine(transform.position + points[i], transform.position + points[i + 1]);
            }
            //draw fire line
            var lineLength = fireType == FireType.Sequential ? Mathf.Lerp (debugLineLength, debugLineLength * 0.15f, (float)i / (float)firePointQuantity) 
                        : fireType == FireType.Subset ? Mathf.Lerp (debugLineLength, debugLineLength * 0.15f, (float)(i % subsetFrequency) / (float)(subsetFrequency - 1))
                        : debugLineLength;
            Gizmos.color = Color.red;
            Gizmos.DrawLine (points[i] + transform.position, points[i] + transform.position + (direction * lineLength));
            
            //draw fire point
            Gizmos.color = heatCol;
            Gizmos.DrawSphere (transform.position + points[i], debug_getBulletSize);
        }
    }
}