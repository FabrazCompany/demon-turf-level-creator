﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectilePattern_Lobbed : ProjectilePattern
{
    public override int getShotsPerTrigger => 1;
    public override void UpdateFirePoints () {}

    public override void DrawDebug () {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere (transform.position, debug_getBulletSize);
        Gizmos.DrawRay (transform.position, transform.forward * debug_getBulletSpeed);
    }
}