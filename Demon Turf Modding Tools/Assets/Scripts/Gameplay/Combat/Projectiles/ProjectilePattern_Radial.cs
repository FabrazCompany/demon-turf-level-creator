using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectilePattern_Radial : ProjectilePattern_Complex
{
    [SerializeField] protected bool reverseFiringOrder;
    [Header ("Pattern Settings")]
    [SerializeField] Orientation orientation;
    [SerializeField, Delayed] int firePointQuantity;
    [SerializeField] float radius; 
    [SerializeField] float startAngle;
    [SerializeField] float targetAngle;  

    Vector3[] firePoints;
    Vector3[] firePositions;
    Vector3[] fireDirections;
	public override Vector3 getFirePosition (int ind) => firePositions[ind];
	public override Vector3 getFireDirection (int ind) => fireDirections[ind];
    protected override int getFirePointQuantity_DEPRECATED => firePointQuantity;

    protected override void OnValidate() {
        base.OnValidate ();
        firePointQuantity = Mathf.Max (firePointQuantity, 1);
    }

    protected override void Awake () {
        base.Awake ();
        firePoints = FzMath.CalculateCirclePoints (orientation, radius, firePointQuantity, startAngle, targetAngle, reverseFiringOrder);
        switch (fireType) {
            case FireType.Sequential:
                firePositions = new Vector3[1];
                fireDirections = new Vector3[1];
                UpdateFirePoints ();
                break;
            
            case FireType.Burst:
                firePositions = new Vector3[firePointQuantity];
                fireDirections = new Vector3[firePointQuantity];

                for (int i = 0; i < firePointQuantity; i++) {
                    firePositions[i] = firePoints[i];
                    fireDirections[i] = ((firePositions[i] + transform.position) - transform.position).normalized;
                }
                break;
            
            case FireType.Subset:
                firePositions = new Vector3[firePointQuantity / subsetFrequency];
                fireDirections = new Vector3[firePointQuantity / subsetFrequency];
                UpdateFirePoints ();
                break;
        }
    }

    public override void UpdateFirePoints () {
        switch (fireType) {
            case FireType.Sequential:
                firePositions[0] = firePoints[index];
                fireDirections[0] = ((firePoints[index] + transform.position) - transform.position).normalized;
                CycleIndex ();
                break;
            case FireType.Subset:
                var length = firePointQuantity / subsetFrequency;
                int j = index;
                for (int i = 0; i < length; i++) {
                    firePositions[i] = firePoints[j];
                    fireDirections[i] = ((firePoints[j] + transform.position) - transform.position).normalized;
                    j += subsetFrequency;
                }
                CycleIndex ();
                break;            
        }
    }

    public override void DrawDebug () {
        if (firePointQuantity < 2)
            return;

        var debugLineLength = debug_getBulletSpeed;
        var points = FzMath.CalculateCirclePoints (orientation, radius, firePointQuantity, startAngle, targetAngle, reverseFiringOrder);
        for (int i = 0; i < points.Length; i++)
        {
            points[i] = transform.InverseTransformDirection (points[i]);
        }
        for (var i = 0; i < firePointQuantity; i++)
        {
            var heatCol = fireType == FireType.Sequential ? Color.Lerp (Color.red, Color.white, (float)i / (float)(firePointQuantity - 1)) 
                        : fireType == FireType.Subset ? Color.Lerp (Color.red, Color.white, (float)(i % subsetFrequency) / (float)(subsetFrequency - 1))
                        : Color.red;

            Gizmos.color = heatCol;
            var nextPoint = i < firePointQuantity - 1 ? points[i + 1] : points[0];
            Gizmos.DrawLine(transform.position + points[i], transform.position + nextPoint);

            //draw fire line
            Gizmos.color = Color.red;
            var lineLength = fireType == FireType.Sequential ? Mathf.Lerp (debugLineLength, debugLineLength * 0.15f, (float)i / (float)firePointQuantity) 
                        : fireType == FireType.Subset ? Mathf.Lerp (debugLineLength, debugLineLength * 0.15f, (float)(i % subsetFrequency) / (float)(subsetFrequency - 1))
                        : debugLineLength;
            
            var forward = ((points[i] + transform.position) - transform.position).normalized;
            Gizmos.DrawLine (points[i] + transform.position, points[i] + transform.position + (forward * lineLength));

            //draw fire point
            Gizmos.color = heatCol;
            Gizmos.DrawSphere (transform.position + points[i], debug_getBulletSize);
        }
	}
}