using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ProjectilePattern_RandomizedAngle : ProjectilePattern 
{
	public override Vector3 getFireDirection (int ind) => facing;
    public override int getShotsPerTrigger => 1;
	Vector3 facing;
	[SerializeField] float angle;
	[SerializeField] float angleVariance;
	[SerializeField] bool maxAngle;
    public override void UpdateFirePoints () {
		var _angle = (angle + (Random.value * angleVariance)) * 0.5f;
		facing = Vector3.forward;
		facing = facing.Rotate (ApplyAngle (), Vector3.up);
		facing = facing.Rotate (ApplyAngle (), Vector3.right);

		// facing = transform.TransformDirection (facing);

		float ApplyAngle () 
		{
			return maxAngle 
				? _angle * Mathf.Sign (Random.value - 0.5f)
				: Random.Range (-_angle, _angle);
		}
	}

	

    // [SerializeField] bool drawDebug;
	
    [SerializeField, Conditional ("drawDebug")] float debugBulletSize = 0.1f;
    public override void DrawDebug () {
	#if UNITY_EDITOR
        if (!drawDebug)
            return;

        Gizmos.color = Color.red;
        Gizmos.DrawSphere (transform.position, debugBulletSize);
        Gizmos.DrawRay (transform.position, transform.forward * debug_getBulletSpeed);
		#endif
    }
	
}