﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectilePattern_Simple : ProjectilePattern
{
    public override int getShotsPerTrigger => 1;
    public override void UpdateFirePoints () {}

	[SerializeField, Conditional ("drawDebug")] float debugBulletSize = 0.1f;
    public override void DrawDebug ()
	{
        Gizmos.color = Color.red;
        Gizmos.DrawSphere (transform.position, debugBulletSize);
        Gizmos.DrawRay (transform.position, transform.forward * debug_getBulletSpeed);
    }
}