﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectilePattern_Spread : ProjectilePattern
{
    [SerializeField, Delayed] int firePointQuantity;
    [SerializeField] float radius; 
	[SerializeField] float angle; 

	public override Vector3 getFirePosition (int ind) => getFireDirection(ind) * radius;
	public override Vector3 getFireDirection (int ind) => Vector3.forward.Rotate (
		(firePointQuantity % 2 == 1 ? 0 : angle * .5f)
		+ (angle * (ind == 0 ? 0 : (ind + 1) / 2) * (ind % 2 == 0 ? 1 : -1))
	);

	public override int getShotsPerTrigger => firePointQuantity;
    public override void UpdateFirePoints () {}


    public override void DrawDebug () {
        Gizmos.color = Color.red;
		for (int i = 0; i < firePointQuantity; i++)
		{
			var pos = transform.TransformPoint (getFirePosition (i));
			Gizmos.DrawSphere (pos, debug_getBulletSize);
			Gizmos.DrawRay (pos, transform.TransformDirection (getFireDirection (i)) * debug_getBulletSpeed);
		}
    }
}
