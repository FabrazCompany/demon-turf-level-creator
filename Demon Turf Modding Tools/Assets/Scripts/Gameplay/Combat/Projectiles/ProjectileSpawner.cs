﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ProjectileSpawner : FlyweightTransformJobBehavior<ProjectileSpawner, bool, ProjectileSpawnerOptimizerJobData, ProjectileSpawnerManager>
{
    [Header ("Fire Pattern")]
    [SerializeField] ProjectilePattern pattern;
    [SerializeField] float placementOffset;
    
    [SerializeField] float startupDuration;
    [SerializeField] float rateOfFire;
    [SerializeField] Transform firePoint;
    int sequenceIndex;

    [Header ("Bullet Behavior")]
    [SerializeField] Projectile projectilePrefab;
    [SerializeField] float size = 0.35f;
    [SerializeField] float speed;
    [SerializeField] Transform ignoreRootCheck;

	List<Collider> ignoreColliders;

    
    [Header("Lifecycle")]
    [SerializeField] UnityEvent onEnable;
    [SerializeField] bool repeatOnEnable = false;
    [SerializeField] UnityEvent onFire;

	
	[Space]
	[SerializeField] float optimizeDistanceScalar = 1;

    protected bool optimize;
	public bool Optimize { get => optimize; set => optimize = value; }
    public float getBulletSize => size;
    public float getBulletSpeed => speed;
    public float getStartupDuration => startupDuration;
    public float getRateOfFire => rateOfFire;
    Transform getfirePoint => firePoint ? firePoint : transform;  

    List<IProjectileSpawnerMod> projectileMods;
	List<Projectile> projectilesList;
    public void RegisterMod (IProjectileSpawnerMod mod)
    {
        if (!projectileMods.Contains (mod))
            projectileMods.Add (mod);
    }
    public void UnregisterMod (IProjectileSpawnerMod mod)
    {
        if (projectileMods.Contains (mod))
            projectileMods.Remove (mod);
    }

    float timer;
    SlowMoEntity entityTime;
    bool prepTriggered;

    void Awake () 
    {
        projectileMods = new List<IProjectileSpawnerMod> ();
		projectilesList = new List<Projectile> ();
		ignoreColliders = new List<Collider> ();
        GetComponentsInChildren<IProjectileSpawnerMod> (true, projectileMods);
        entityTime = GetComponentInParent<SlowMoEntity> ();
    }

    protected override void OnEnable () 
    {
		base.OnEnable ();
		projectilesList.Clear ();
        timer = startupDuration;
        sequenceIndex = 0;
        onEnable?.Invoke ();
    }
    public void TryFiringProjectiles ()
    {
		if (!optimize)
		{
			timer -= entityTime ? Time.deltaTime * entityTime.timeScale : Time.deltaTime;
			if (timer <= 0)
			{
				timer = rateOfFire;
				FireProjectiles ();
			} 
			else if (repeatOnEnable 
						&& !prepTriggered 
						&& timer <= rateOfFire - startupDuration)
			{
				prepTriggered = true;
				onEnable?.Invoke ();
			}
		}
		else 
		{
			timer -= entityTime ? Time.deltaTime * entityTime.timeScale : Time.deltaTime;
			if (timer <= 0)
				timer = rateOfFire;
		}
        
    }
	public void FireProjectiles () => FireProjectiles (projectilePrefab);
	public void FireProjectiles (float speed) => FireProjectiles (projectilePrefab, speed);
    public void FireProjectiles (Projectile prefab, float? _speed = null, float? _size = null) 
    {
        var length = pattern.getShotsPerTrigger;
        var firePoint = getfirePoint;
		pattern.UpdateFirePoints ();
		projectilesList.Clear ();
        for (int i = 0; i < length; i++) 
        {
			pattern.GetPatternInfo (firePoint, i, out var position, out var direction);	
            var projectile = SpawnProjectile (position, direction, prefab, _size);
            projectile.LaunchProjectile (_speed ?? speed);
			projectilesList.Add (projectile);

            if (ignoreRootCheck)
                projectile.SetIgnoredRoot (ignoreRootCheck);
            projectile.AddIgnoredColliders (ignoreColliders);
			
            for (int j = 0; j < projectileMods.Count; j++) 
            {
                projectileMods[j].ApplyMod (projectile);
            }
        }

		foreach (var projectile in projectilesList)
		{
			projectile.AddIgnoredColliders (projectilesList);
		}
        prepTriggered = false;
        onFire?.Invoke ();
    }
    protected virtual Projectile SpawnProjectile (Vector3 position, Vector3 facing, Projectile prefab, float? _size) 
    {
        var projectile = TrashMan.spawn (prefab?.gameObject, position).GetComponent<Projectile> ();
        if (projectile == null) 
        {
            Debug.LogError ("No projectile found!");
            return null;
        }
        projectile.Format (_size ?? size, facing);
        return projectile;
    }

    public void SetStartupDuration (float val) => startupDuration = val;
    public void SetRateOfFire (float val) => rateOfFire = val;
    public void SetSpeed (float val) => speed = val;
	public void SetProjectile (Projectile _projectile) => projectilePrefab = _projectile;
	public void AddIgnoredCollider (Collider _collider) => ignoreColliders.Add (_collider);
	public void AddIgnoredColliders (IEnumerable<Collider> _colliders) => ignoreColliders.AddRange (_colliders);
	public void SetPattern (ProjectilePattern _pattern) => pattern = _pattern;

	protected override ProjectileSpawnerOptimizerJobData InitializeJobData() => new ProjectileSpawnerOptimizerJobData () { scalar = optimizeDistanceScalar, optimize = false };
}


public interface IProjectileSpawnerMod
{
    void ApplyMod (Projectile projectile);
}