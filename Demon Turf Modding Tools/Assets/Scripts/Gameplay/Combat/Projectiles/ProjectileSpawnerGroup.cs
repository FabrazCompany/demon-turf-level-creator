using UnityEngine;

public class ProjectileSpawnerGroup : MonoBehaviour
{
	
	ProjectileSpawner[] spawners;

	void Awake () 
	{
		spawners = GetComponentsInChildren<ProjectileSpawner> ();
	}

	public void TriggerShots ()
	{
		foreach (var spawner in spawners)
		{
			spawner.FireProjectiles ();
		}
	}
}