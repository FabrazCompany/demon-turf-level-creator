﻿using UnityEngine;
using UnityEngine.Jobs;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;

public struct ProjectileSpawnerOptimizerJobData
{
	public float scalar;
	public bool optimize;
}


[BurstCompile(CompileSynchronously = true)]
struct ProjectileSpawnerJob : IJobParallelForTransform
{
	public float3 camPos;
	public float distance;
	public NativeArray<ProjectileSpawnerOptimizerJobData> stateArray;

	void IJobParallelForTransform.Execute(int transformIndex, TransformAccess transform)
	{
		var data = stateArray[transformIndex];
		data.optimize = math.distance (camPos, transform.position) >= (distance * data.scalar);
		stateArray[transformIndex] = data;
	}	
}


public class ProjectileSpawnerManager : FlyweightTransformJobManager<ProjectileSpawner, bool, ProjectileSpawnerOptimizerJobData, ProjectileSpawnerManager>
{
	Camera cam;
	Transform camTr;
	bool isJobScheduled;
	JobHandle jobHandle;
	NativeArray<ProjectileSpawnerOptimizerJobData> tempJobArray;


	const float optimizeDistance = 50f;

	protected virtual void Start () 
	{
		CameraSpawnedBroadcast.OnCameraSpawned += SetCamera;
		SetCamera (Camera.main);
	}
	void OnDestroy () 
	{
		CameraSpawnedBroadcast.OnCameraSpawned -= SetCamera;
	}
	void SetCamera (Camera _cam) 
	{
		if (cam == _cam)
			return;

		cam = _cam;
		camTr = cam.transform;
	}
	void Update ()
	{
		if (!camTr)
		{
			if (isJobScheduled)
			{
				isJobScheduled = false;
				jobHandle.Complete ();
			}
			if (tempJobArray.IsCreated)
				tempJobArray.Dispose ();
			return;
		}

		if (isJobScheduled)
		{
			jobHandle.Complete ();
			var items = slots.RawItems;
			var length = slots.Count;
			for (int i = 0; i < length; i++)
			{
				ref var slot = ref items[i];
				if (!slot.active || slot.jobIndex >= tempJobArray.Length) continue;

				var state = tempJobArray[slot.jobIndex];
				if (state.optimize != slot.flyweight.Optimize)
					slot.flyweight.Optimize = state.optimize;

				slot.flyweight.TryFiringProjectiles ();	
			}
			tempJobArray.Dispose ();
			isJobScheduled = false;
		}

		var count = numJobData;
		tempJobArray = new NativeArray<ProjectileSpawnerOptimizerJobData> (count, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
		NativeArray<ProjectileSpawnerOptimizerJobData>.Copy (dataFields, 0, tempJobArray, 0, count);
		var job = new ProjectileSpawnerJob ()
		{
			camPos = camTr.position,
			distance = optimizeDistance,
			stateArray = tempJobArray
		};
		jobHandle = job.Schedule (transforms);
		isJobScheduled = true;	
	}

	public override void unregisterAll ()
	{
		if (isJobScheduled)
		{
			jobHandle.Complete ();
			isJobScheduled = false;
		}
		if (tempJobArray.IsCreated)
			tempJobArray.Dispose ();
		base.unregisterAll ();
	}
}