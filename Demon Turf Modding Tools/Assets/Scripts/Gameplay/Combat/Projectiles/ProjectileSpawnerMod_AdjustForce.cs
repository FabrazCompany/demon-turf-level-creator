using UnityEngine;

public class ProjectileSpawnerMod_AdjustForce : MonoBehaviour, IProjectileSpawnerMod
{
	[SerializeField] float forceScaleMin = .8f;
	[SerializeField] float forceScaleMax = 1.2f;
	void IProjectileSpawnerMod.ApplyMod(Projectile projectile)
    {
		projectile.getRigid.velocity *= Random.Range (forceScaleMin, forceScaleMax);
    }

}