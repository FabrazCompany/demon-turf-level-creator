using UnityEngine;

public class ProjectileSpawnerMod_BlinkBomb : MonoBehaviour, IProjectileSpawnerMod
{

    [SerializeField] float inactiveDuration = 5;
	[SerializeField] float inactiveDurationVariance = 0f;
    [SerializeField] float activeDuration = 5;
    [SerializeField] float blinkRateStart = 1;
    [SerializeField] float blinkRateEnd = 0.1f;

    void IProjectileSpawnerMod.ApplyMod(Projectile projectile)
    {
        var blinker = projectile.GetComponent<ProjectileMod_BlinkBomb> ();
        blinker?.Init (inactiveDuration + (Random.value * inactiveDurationVariance), activeDuration, blinkRateStart, blinkRateEnd);
    }
}