using UnityEngine;

public class ProjectileSpawnerMod_DelayedDetonate : MonoBehaviour, IProjectileSpawnerMod
{
	[SerializeField] float detonateTimer = 5f;

	void IProjectileSpawnerMod.ApplyMod(Projectile projectile)
	{
		var delayedDetonate = projectile.GetComponent<ProjectileMod_DelayedDetonate> ();
		delayedDetonate?.Initialize (detonateTimer);
	}
}