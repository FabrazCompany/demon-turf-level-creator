using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(ProjectileSpawner))]
public class ProjectileSpawnerMod_Geyser : MonoBehaviour, IProjectileSpawnerMod
{

	[SerializeField] LayerMask targetLayer;
	[SerializeField] float targetCheckDistance = 15f;
	[SerializeField] float minRadius = 0;
	[SerializeField, UnityEngine.Serialization.FormerlySerializedAs ("radius")] float maxRadius = 10;
	[SerializeField] float speed = 3;
	[SerializeField] bool useLobbedLogic;
	
	void Awake () 
    {
        targetedComps = new List<RotateToPosition> (5);
    }
	Vector3 lobbedShotDir = Vector3.forward;
    List<RotateToPosition> targetedComps; 
    public void ApplyMod (Projectile projectile)
	{
		Vector3 pos = RandomPoint ();
		var timeout = 20;
		RaycastHit hitInfo;
		while (!Physics.Raycast(pos, Vector3.down, out hitInfo, targetCheckDistance, targetLayer) && timeout > 0)
		{
			pos = RandomPoint ();
			timeout--;
		}
		var target = pos.With (y: hitInfo.point.y);
		projectile.GetComponentsInChildren<RotateToPosition>(true, targetedComps);

		for (int i = 0; i < targetedComps.Count; i++)
		{
			targetedComps[i].AssignTarget(target);
			targetedComps[i].SetSpeed (speed);
		}


		Vector3 RandomPoint ()
		{
			var returnPos = transform.position;
			var rand = FzMath.GetPointOnUnitCircleCircumference () * Mathf.Lerp (minRadius, maxRadius, Random.value);
			returnPos.x += rand.x;
			returnPos.z += rand.y;
			return returnPos;
		}
	}

	void OnDrawGizmosSelected()
	{
		Gizmos.color = new Color (1, .5f, 0, 1);
		Gizmos.DrawWireSphere (transform.position, minRadius);
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere (transform.position, maxRadius);
	}


}