using UnityEngine;

public class ProjectileSpawnerMod_Laser : MonoBehaviour, IProjectileSpawnerMod
{
    [SerializeField] float activeDuration = 10;
    [SerializeField] float cleanupDuration = .5f;
    [SerializeField] float length = 3;
    
    void IProjectileSpawnerMod.ApplyMod(Projectile projectile)
    {
        var laser = projectile.GetComponent<ProjectileMod_Laser> ();
        laser?.Init (activeDuration, cleanupDuration, projectile.getSize, length);
    }

    //TODO add linecast check behind that applies kill to anything inbetween 
}