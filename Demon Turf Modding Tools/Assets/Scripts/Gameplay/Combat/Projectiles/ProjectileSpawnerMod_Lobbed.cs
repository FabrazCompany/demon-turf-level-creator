﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileSpawnerMod_Lobbed : MonoBehaviour, IProjectileSpawnerMod
{
    Vector3? lobForce;
    [SerializeField] bool applyAiming = true;
    [SerializeField] float duration = 1.5f;
	[SerializeField] float durationVariance = 0;
    [SerializeField] float gravity = 3;
    [SerializeField] float torque = 60;

    public float getGravity => gravity;
    public void SetLobForce (Vector3 force) => lobForce = force;
    public void ApplyMod (Projectile projectile) 
    {
        var force = lobForce != null ? lobForce.Value : projectile.transform.forward * projectile.getForce;
        projectile.enabled = false;
        
        if (applyAiming)
            transform.forward = force.normalized;
            
        projectile.getRigid.velocity = force;
        projectile.getRigid.angularVelocity = UnityEngine.Random.onUnitSphere * torque;
        
        var bobLobLaw = projectile.GetComponent<ProjectileMod_Lobbed> ();
        bobLobLaw?.SetGravity (gravity);
		bobLobLaw?.SetDuration (duration + (Random.value * durationVariance));
    }
}
