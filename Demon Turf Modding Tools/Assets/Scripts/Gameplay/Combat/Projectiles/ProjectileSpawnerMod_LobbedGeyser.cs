using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(ProjectileSpawner))]
public class ProjectileSpawnerMod_LobbedGeyser : MonoBehaviour, IProjectileSpawnerMod
{
	[SerializeField] float gravity = 3;
	[SerializeField] bool hasDuration = true;
	[SerializeField] float duration = 5;
	

	Vector3 lobbedShotDir = Vector3.forward;
    public void ApplyMod (Projectile projectile)
	{
		projectile.enabled = false;
		projectile.getRigid.velocity = projectile.transform.forward * projectile.getForce;
		
		var bobLobLaw = projectile.GetComponent<ProjectileMod_Lobbed> ();
        bobLobLaw?.SetGravity (gravity);
		if (hasDuration)
	        bobLobLaw?.SetDuration (duration);
	}
}