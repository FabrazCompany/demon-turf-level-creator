using UnityEngine;

[RequireComponent (typeof(ProjectileSpawner))]
public class ProjectileSpawnerMod_Split : MonoBehaviour, IProjectileSpawnerMod
{
    [SerializeField] ProjectileMod_Split.SplitType splitType;
    [SerializeField] int splitNumber;
    [SerializeField] float duration;
    [SerializeField, Range (0, 180)] float spread;
    public void ApplyMod (Projectile projectile) 
    {
        var splitProjectile = projectile.GetComponent<ProjectileMod_Split> ();
        if (!splitProjectile)
            return;

        splitProjectile.AssignValues (splitType, splitNumber, duration, spread);
    }
}