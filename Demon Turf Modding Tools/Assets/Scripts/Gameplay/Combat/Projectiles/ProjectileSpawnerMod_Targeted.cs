using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(ProjectileSpawner))]
public class ProjectileSpawnerMod_Targeted : MonoBehaviour, IProjectileSpawnerMod
{
    Transform target;
    void Awake () 
    {
        targetedComps = new List<ITargeted> (5);
    }
    public void OnTargetAcquired (Transform _target)
    {
        target = _target;
    }
    public void OnTargetLost () 
    {
        target = null;
    }

    List<ITargeted> targetedComps; 
    public void ApplyMod (Projectile projectile) 
    {
		if (!target)
			return;
        projectile.GetComponentsInChildren<ITargeted> (true, targetedComps);
        for (int i = 0; i < targetedComps.Count; i++) 
        {
            targetedComps[i].AssignTarget (target);
        }
    }
}