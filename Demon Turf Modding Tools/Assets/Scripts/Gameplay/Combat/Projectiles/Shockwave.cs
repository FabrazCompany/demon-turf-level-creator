using UnityEngine;

public class Shockwave : MonoBehaviour 
{

	[SerializeField] float duration = 1f;
	[SerializeField] float startScale = 0;
	[SerializeField] float planarScale = 2;
	[SerializeField] AnimationCurve yScaleCurve;
	[SerializeField] float yScale = 4;
	[SerializeField] ParticleSystem feedbackParticle;
	[SerializeField] GameObject despawnTarget;

	float timer = 0;
	SlowMoEntity time;
	Renderer rend;
	Collider coll;

	void Awake () 
	{
		rend = GetComponent<Renderer> ();
		coll = GetComponent<Collider> ();
		time = GetComponent<SlowMoEntity> ();
		
	}

	void OnEnable () 
	{
		timer = 0;
		rend.enabled = true;
		coll.enabled = true;
		transform.localScale = new Vector3 (startScale, 0, startScale);
		// timer
	}

	void Update () 
	{
		if (timer < duration) 
		{
			timer += (time ? time.timeScale * Time.deltaTime : Time.deltaTime);
			var scale = transform.localScale;
			var t = timer / duration;
			scale.x = scale.z = Mathf.Lerp (startScale, planarScale, t);
			scale.y = yScaleCurve.Evaluate (t) * yScale;
			transform.localScale = scale;
			if (timer >= duration) {
				rend.enabled = false;
				coll.enabled = false;
				if (feedbackParticle)
					feedbackParticle.Stop ();
			}
		}
		else if (feedbackParticle && !feedbackParticle.IsAlive ()) 
			Despawn ();
	}
	
	public void Despawn () => TrashMan.despawn (despawnTarget ? despawnTarget : gameObject);
}