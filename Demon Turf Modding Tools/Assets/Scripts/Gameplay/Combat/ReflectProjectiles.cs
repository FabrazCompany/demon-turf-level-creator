﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ReflectProjectiles : MonoBehaviour
{
    [System.Serializable]
    public enum ReflectType 
    {
        PlayerSpinAttack = 0,
        PlayerSpinAttackUpdraft = 2,
        PlayerSpinAttackBubble = 3,
        PlayerSpinAttackWhirlwind = 4,
        EnemyReflectBubble = 10
    }

    // [System.Serializable]    
    // class ReflectProjectilesEvent : UnityEvent<ReflectProjectiles> {}
    
    [SerializeField] ReflectType reflectType;
    [SerializeField] bool mute;
    [SerializeField] Vector3 reflectDirection;
    [SerializeField] float reflectForce;
	[SerializeField] float reflectVelocityScalar = 1f;
	[SerializeField] float reflectSizeScalar = 1.1f;

    [Header ("Feedback")]
    [SerializeField] float freezeFrameDuration;
    [SerializeField] ReflectProjectilesEvent onProjectileReflected;

    Coroutine freezeFrameRoutine;
    List<Projectile> reflectedProjectiles;

    public ReflectType getReflectType => reflectType;
    public ReflectProjectilesEvent getOnProjectileReflected => onProjectileReflected;
	public float getReflectVelocityScalar => reflectVelocityScalar;
	public float getReflectSizeScalar => reflectSizeScalar;

    void Awake () 
    {
        reflectedProjectiles = new List<Projectile> ();
    }

    void OnEnable ()
    {
        reflectedProjectiles.Clear ();
    }
    public void ClearReflectionList () => reflectedProjectiles.Clear ();

    void OnTriggerEnter(Collider other)
    {
        if (mute)
            return;
        if (!enabled)
            return;
        var projectile = other.GetComponent<Projectile> ();
        if (!projectile) 
            return;
        if (reflectedProjectiles.Contains (projectile))
            return;
		if (reflectType == ReflectType.EnemyReflectBubble && other.TryGetComponent<DeliverKill> (out var kill))
			return;

        //reflect projectile
        var newDir = reflectDirection == Vector3.zero 
            ? (projectile.transform.position.With (y: transform.position.y) - transform.position).normalized
            : transform.TransformDirection (reflectDirection.normalized);

        projectile.Reflect (this, newDir, reflectForce);
        
        reflectedProjectiles.Add (projectile);

        if (freezeFrameDuration > 0)
            this.RestartCoroutine (Feedback.FreezeFrame (freezeFrameDuration), ref freezeFrameRoutine);
        onProjectileReflected?.Invoke (this);
    }
}
