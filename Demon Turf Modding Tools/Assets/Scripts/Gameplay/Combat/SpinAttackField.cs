﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class SpinAttackField : MonoBehaviour {
    [SerializeField] Collider triggerZone;
    [SerializeField] Renderer placeholderVisual;
    [SerializeField] Image cooldownImg;
    [SerializeField] bool unslowOnAttack;
    [SerializeField] float spinForce;
    [SerializeField] float pullForce;
    [SerializeField] float redirectForce;
    [SerializeField] float maxActiveTime;
    [SerializeField] float staminaRecoveryRatio = .5f;    
    List<ISpinnable> spinners = new List<ISpinnable> ();

    [SerializeField] UnityEvent onStaminaExpended;
    



    // bool AttackAvailable => false;

    bool counterClockwise;
    Vector3 facingDir;
    Vector3 cachedPos;
    float timer;
    bool active => triggerZone.enabled;
    public void Enable (bool _counterClockwise) {
        triggerZone.enabled = true;
        placeholderVisual.enabled = true;
        cooldownImg.enabled = true;
        counterClockwise = _counterClockwise;
        cachedPos = Vector3.zero;
        spinners.Clear ();
    }
    public void Disable () {
        triggerZone.enabled = false;
        placeholderVisual.enabled = false;
        for (int i = 0; i < spinners.Count; i++) {
            var spinned = spinners[i];
            var targetDir = Vector3.RotateTowards (spinned.transform.forward, facingDir, redirectForce, 0);
            spinned.transform.forward = targetDir;
        }
        spinners.Clear ();
    }
    public void SetDirection (Vector3 dir) {
        facingDir = dir;
    }
    void OnTriggerEnter (Collider other) {
        var spinned = other.GetComponent<ISpinnable> ();
        if (spinned == null)
            return;
        
        if (unslowOnAttack) {
            var slowed = spinned.transform.GetComponent<ISlowable> ();
            slowed?.ResetSlowdown ();
        }
        spinners.Add (spinned);
    }
    void OnTriggerExit (Collider other) {
        var spinned = other.GetComponent<ISpinnable> ();
        if (spinned == null)
            return;
        spinners.Remove (spinned);
    }
    
    void FixedUpdate () {
        var diff = cachedPos - transform.position;
        for (int i = 0; i < spinners.Count; i++) {
            var spinned = spinners[i];
            spinned.transform.position -= diff;
            var adjustedPos = transform.position.With (y: spinned.transform.position.y);
            var targetDir = Quaternion.AngleAxis (-90 * (!counterClockwise ? 1 : -1), Vector3.up) * (adjustedPos - spinned.transform.position).normalized;
            spinned.transform.forward = Vector3.RotateTowards (spinned.transform.eulerAngles, targetDir, spinForce * Time.fixedDeltaTime, 0);
            spinned.transform.position = Vector3.MoveTowards (spinned.transform.position, adjustedPos, pullForce * Time.fixedDeltaTime);
        }
        cachedPos = transform.position;
    }

    void Update () {
        timer = Mathf.Max (timer + Time.deltaTime * (active ? 1 : -1 * staminaRecoveryRatio), 0);
        cooldownImg.fillAmount = 1 - (timer / maxActiveTime);
        if (active && timer >= maxActiveTime) {
            cooldownImg.fillAmount = 0;
            timer = maxActiveTime + 1;
            onStaminaExpended?.Invoke ();
        } else if (timer <= 0 && !active && cooldownImg.enabled) {
            cooldownImg.fillAmount = 1;
            cooldownImg.enabled = false;
        }
    }
}

public interface ISpinnable {
    Transform transform { get; }
    Rigidbody getRigid { get; }
}