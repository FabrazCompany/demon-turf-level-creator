﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeBubbleField : MonoBehaviour
{
    [SerializeField] float duration;
    [SerializeField] Color startColor;
    [SerializeField] Color endColor;
    [SerializeField] float radius;
    [SerializeField] LayerMask collisionMask;
    Renderer rend;
    Material material;
    void Awake () {
        rend = GetComponent<MeshRenderer> ();
        material = rend.material;
        bubbleHits = new Collider[20];
    }
    void OnEnable () {
        StartCoroutine (Bubble ());
    }
    Collider[] bubbleHits;
    IEnumerator Bubble () {
        float timer = -Time.deltaTime;
        material.SetColor ("_Color", startColor);
        int hits = Physics.OverlapSphereNonAlloc (transform.position, radius, bubbleHits, collisionMask);
        for (int i = 0; i < hits; i++) {
            var slowable = bubbleHits[i].GetComponent<ISlowable> ();
            slowable?.ApplySlowdown ();
        }

        while (timer < duration) {
            timer += Time.deltaTime;
            material.SetFloat ("_SliceAmount", timer / duration);
            material.SetColor ("_Color", Color.Lerp (startColor, endColor, timer / duration));
            yield return null;
        }
        material.SetFloat ("_SliceAmount", 1);
        material.SetColor ("_Color", endColor);
        TrashMan.despawn (gameObject);
    }

    void OnDrawGizmosSelected () {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere (transform.position, radius);
    }
}

public interface ISlowable {
    void ApplySlowdown ();
    void ResetSlowdown ();
}
