using System.Collections;
using UnityEngine;


public static class Feedback {
    public static IEnumerator FreezeFrame (float dur) {
        // Debug.Log ("START FREEZE");
        Game.timeScale = 0;
        yield return new WaitForSecondsRealtime (dur);
        Game.timeScale = Game.defaultTime;
    }
}