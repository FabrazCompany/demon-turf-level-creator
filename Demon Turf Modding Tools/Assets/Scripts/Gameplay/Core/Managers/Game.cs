using UnityEngine;

public static partial class Game
{
	
#if UNITY_EDITOR
	public const UnityEditor.BuildTarget buildTarget_GameCoreXboxOne = (UnityEditor.BuildTarget)43;
	public const UnityEditor.BuildTarget buildTarget_GameCoreSeriesX = (UnityEditor.BuildTarget)42;
	public const UnityEditor.BuildTargetGroup buildTargetGroup_GameCoreXboxOne = (UnityEditor.BuildTargetGroup)32;
	public const UnityEditor.BuildTargetGroup buildTargetGroup_GameCoreSeriesX = (UnityEditor.BuildTargetGroup)31;
#endif
	public const RuntimePlatform runtimePlatform_GameCoreXboxOne = (RuntimePlatform)37;
	public const RuntimePlatform runtimePlatform_GameCoreSeriesX = (RuntimePlatform)36;


	public const string PREPROC_STEAM = "DT_STEAM";
	#if DT_STEAM
	public static bool isSteamVersion => true;
	#else 
    public static bool isSteamVersion => false;
	#endif

	public const string PREPROC_EPIC = "DT_EPIC";
	#if DT_STEAM
	public static bool isEpicVersion => true;
	#else 
    public static bool isEpicVersion => false;
	#endif

	public const string PREPROC_GOG = "DT_GOG";
	#if DT_GOG
	public static bool isGoGVersion => true;
	#else 
    public static bool isGoGVersion => false;
	#endif


	public static float version => 0.9f;

	static Game () 
	{
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.playModeStateChanged += OnPlayModeChanged;
		#endif
		gameActive = true;
	} 

	#if UNITY_EDITOR
	static void OnPlayModeChanged (UnityEditor.PlayModeStateChange state) 
	{
		switch (state)
		{
			case UnityEditor.PlayModeStateChange.ExitingPlayMode:
				gameActive = false;
				break;
			case UnityEditor.PlayModeStateChange.EnteredPlayMode:
				gameActive = true;
				break;
		}
	}

	public static void DisableCheats ()
	{
#if !DT_MOD
		if (Cheat_UnlockLevels.Enabled)
			Cheat_UnlockLevels.Toggle ();
		if (Cheat_UnlockTurfAbilities.Enabled)
			Cheat_UnlockTurfAbilities.Toggle ();
		if (Cheat_UnlockPhotoHuntQuests.Enabled)
			Cheat_UnlockPhotoHuntQuests.Toggle ();
		if (Cheat_UnlockTurfState2.Enabled)
			Cheat_UnlockTurfState2.Toggle ();
		if (Cheat_IgnorePurchaseCosts.Enabled)
			Cheat_IgnorePurchaseCosts.Toggle ();

		if (Cheat_UnlockAllDyes.Enabled)
			Cheat_UnlockAllDyes.Toggle ();
		if (Cheat_UnlockAllHeadPets.Enabled)
			Cheat_UnlockAllHeadPets.Toggle ();
		if (Cheat_UnlockAllMods.Enabled)
			Cheat_UnlockAllMods.Toggle ();
#endif
	}
	#endif

    static public float timeScale 
    {
        get => Time.timeScale;
        set => Time.timeScale = value; 
    }
	static public float defaultTime 
	{
		get 
		{
#if !DT_EXPORT && !DT_MOD
			return GameDataManager.Instance.Data.GetSetting (SettingIDBool.CROWN_SlowMo) ? 0.5f : 1;	
#else
			return 1;
#endif
		}
		
	}
	static bool _gameActive = true;
	static public bool gameActive 
	{
		get => _gameActive;
		set => _gameActive = value;
	}
    static public bool allowDebugMessages = true;

    static public void Log (object msg, Object context = null) {
        Log (msg.ToString (), context);
    }
    static public void Log (string msg, Object context = null) {
        if (!allowDebugMessages)
            return;
        Debug.Log (msg, context);
    }
    static public void LogWarning (object msg, Object context = null) {
        LogWarning (msg.ToString (), context);
    }
    static public void LogWarning (string msg, Object context = null) {
        if (!allowDebugMessages)
            return;
        Debug.LogWarning (msg, context);
    }
    static public void LogError (object msg, Object context = null) {
        LogError (msg.ToString (), context);
    }
    static public void LogError (string msg, Object context = null) {
        if (!allowDebugMessages)
            return;
        Debug.LogError (msg, context);
    }
}