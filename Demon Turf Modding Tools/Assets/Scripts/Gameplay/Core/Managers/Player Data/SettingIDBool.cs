public enum SettingIDBool
{
	Fullscreen = 5,
	//audio
	BeebzChatter = 10,
    //vfx
    AntiAliasing = 50,
    VSync = 55,
	AmbientOcclusion = 57,
    FrameCapTo60 = 65,
    // VolumetricLighting = 70,
	Bloom = 75,
	LightOptimizer = 77,
	WaterOptimizer = 78,
    // DepthOfField = 70,

    //camera
    InvertCameraY = 150,
	InvertCameraX = 151,
    CameraFollow = 155,

    CameraAngling_Manual = 160,
    CameraCuts_Manual = 162,

    //Gameplay
    ScreenShake = 200,
	FreezeFrame = 202,
	UIPrompts = 210,
	UITurfState = 211,
	TutorialSigns = 220,
	SkipCutscenes = 225,
	AlwaysShowTimer = 230,
	AutoResetOnDeath = 232,
	Rumble = 235,

	DisplayStreamerName = 240,
	DispaySpeedrunTimer = 242,

	// PerformanceMode = 250,
	

    WalkToggle = 300,
	SuperJumpBumpHoldSpin = 310,
    TurfMenuActiveHold = 330,
    TurfMenuAutoClose = 340,
	TurfMenuSlowMo = 342,
    TurfAbilityToggle = 345,

    TurfAbilityHookshot_HideTargetsWhenUnequipped = 350,
	TurfAbilityGlide_InvertY = 360,

    CHEAT_LevelsUnlocked = 1000,
    CHEAT_State2Unlocked = 10005,
    CHEAT_TurfAbilitiesUnlocked = 10010,
	
	CROWN_LethalPunch = 20000,
	CROWN_SlowMo = 20005,
	CROWN_SuperSpeed = 20010,
	CROWN_MoonJump = 20015,

	CHEAT_DebugVFX = 30000,
	CHEAT_DynamicResolution = 30015,

	// CHEAT_DoF = 30020,
	// CHEAT_KinematicInterpolate = 30030,
}
