public enum SettingIDFloat
{
    //50 (sound)
    VolumeMaster = 50,
    VolumeMusic = 55,
    VolumeSFX = 60,
    VolumeUI = 65,
	VolumeVO = 70,
    

    //100 (gameplay)
    LookSensititityMouse = 105,
	LookSensititityGamepadX = 106,
	LookSensititityGamepadY = 107,
    MoveThumbstickDeadzone = 110,

	RolloutThumbstickSensitivityX = 115,
	RolloutThumbstickSensitivityY = 116,
	GlideThumbstickSensitivityX = 118,
	GlideThumbstickSensitivityY = 119,

	//150 (vfx)
	Gamma = 150,

	//200 (ui)
	
	GUIScalar = 200,

	DEBUG_ResolutionScale = 160,
}