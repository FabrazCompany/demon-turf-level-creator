public enum SettingIDInt
{
    Resolution = 50,
    ColorBlind = 100,
    QualiyShadow = 150,
    QualityTexture = 155,
	QualityDof = 170,
	QualityVolumetricFog = 175,
	QualityVolumetricLight = 176,
	LODBiasLevel = 180,
	RenderDistance = 185,
	PerformanceMode = 200,
	FrameCap = 220,
}