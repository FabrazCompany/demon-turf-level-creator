using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

#if DT_EXPORT || DT_MOD
public class SoundManagerStub : MonoBehaviour
{
	public static SoundManagerStub Instance { get; }
	public static bool IsInitialized { get; }
	public bool usingDynamicMusic => false;
	public bool IsMusicPlaying (AudioClip music) => false;
	
	public void PlayBackgroundMusic(AudioClip music, float volume, bool play = true) {}
	public void PlayBackgroundMusicHub (int section) {} 
	public void PlayBackgroundMusic(IDynamicMusic cue, System.Enum section) {}
	public void PlayBackgroundMusic(IDynamicMusic cue, int section = 0) {}

	public void StopMusic() {}
	public void ClearMusic () {}
	public void PauseMusic () {}
	public void PlayMusic () {}
	public void ResumeMusic () {}

	public AudioSource PlaySoundUI (AudioClip Sfx) => null;
	public AudioSource PlaySoundUI (AudioClip Sfx, AudioData data) => null;
	public AudioSource PlaySoundGameplay2D (AudioClip Sfx, Vector3 Location) => null;
	public AudioSource PlaySoundGameplay2D (AudioClip Sfx, Vector3 Location, AudioData data) => null;
	public AudioSource PlaySoundGameplay3D (AudioClip Sfx, Vector3 Location) => null;
	public AudioSource PlaySoundGameplay3D (AudioClip Sfx, Vector3 Location, AudioData data) => null;
	public void ReleaseSource (AudioSource source, bool checkDespawnQueue = true) {}
	public void FadeDestroy (AudioSource audio, float delay) {}

	public AudioMixerGroup GetAudioGroup (SoundMixerTarget target) => null;
	public void MuteAudioGroup (SoundMixerTarget target, bool mute) {}
	public SoundMixerTarget GetMixerTarget (AudioMixerGroup mixer) => SoundMixerTarget.Master;
	public AudioMixerGroup GetGameplayMixerGroup (bool useFiltered) => null;

	public AudioMixerSnapshot[] getCurrentSnapshots => null;
	public bool hasSnapshotOverrides => false;
	public void AddSnapshot (AudioMixerSnapshot snapshot, float transitionTime) {}
	public void RemoveSnapshot (AudioMixerSnapshot snapshot, float transitionTime) {}
	public void AssignTurfSnapshot (LevelID level, float transition) {}
	public void AssignTurfSnapshot (AudioMixerSnapshot snapshot, float transition) {}
	public void ClearSnapshots (float transitionTime) {}
	public void SetCurrentSnapshots (AudioMixerSnapshot[] newSnapshots) {}
	public void TriggerFadeInBGMVolume () {}
	public void CancelFadeInBGM () {}
	public void FadeBackgroundMusicRatio (float target, float duration) {}
	public void SetPitchScalar (float targetScalar) {}
	public void GetMusicSpectrumData (float[] samples) {}
	public static double dspTime;
}
#else 
public class SoundManagerStub : SoundManager
{
}
#endif