using UnityEngine;
using UnityEngine.Events;

public class PlayerActionListener : MonoBehaviour
{
    enum PlayerVerbType
    {
        Jump,
        Spin,
        Punch,
		DoubleJump,
		SuperJump,
    }

    [SerializeField] PlayerVerbType verbType;
    [SerializeField] UnityEvent onVerbTriggered;
	IPlayerController character;
    void Awake () 
    {
        PlayerControllerEvents.onSpawned += Init;
    }
    void OnDestroy ()
    {
        PlayerControllerEvents.onSpawned -= Init;
		Cleanup ();
    }
	void Trigger () => onVerbTriggered?.Invoke();
    void Init (IPlayerController _character)
    {
		character = _character;
        switch (verbType)
        {
            case PlayerVerbType.Jump:
                character.onJump += Trigger;
                break;
            case PlayerVerbType.DoubleJump:
				character.onDoubleJump += Trigger;
				break;
			case PlayerVerbType.SuperJump:
				character.onSuperJump += Trigger;
				break;
            case PlayerVerbType.Spin:
                character.onSpin += Trigger;
                break;
			case PlayerVerbType.Punch:
				character.onPunch += Trigger;
				break;
        }
    }
	void Cleanup ()
	{
		if (character == null) return;
		character.onJump -= Trigger;
		character.onDoubleJump -= Trigger;
		character.onSuperJump -= Trigger;
		character.onSpin -= Trigger;
		character.onPunch -= Trigger;
	}
}