using UnityEngine;
using System.Collections.Generic;

public class DeliverPoison : MonoBehaviour
{
    [SerializeField] float triggerAmount;
    [SerializeField] bool isConstant;
    [SerializeField, Conditional ("isConstant")] float buildupRate = 1;
    [SerializeField, Conditional ("isConstant")] float buildupAmount = 10;
	[SerializeField] bool clearResidentOnExit = true;

	List<Collider> residents;

	void Awake () 
	{
		residents = new List<Collider> ();
	}
	void OnDisable () 
	{
		residents.ForEach (ClearPoison);
		residents.Clear ();
	}
    public void SetTriggerAmount (float _triggerAmount) => triggerAmount = _triggerAmount;
	public void SetBuildupRate (float _buildupRate) => buildupRate = _buildupRate;
	public void SetBuildupAmount (float _buildupAmount) => buildupAmount = _buildupAmount;
    public void TriggerPoison_Particle (Collider hit, Vector3 dir) 
    {
        TriggerPoison (hit, false);
    }
    void OnCollisionEnter (Collision other)
    {
        TriggerPoison (other?.collider, false);
    }
    void OnTriggerEnter (Collider other) 
    {
        TriggerPoison (other, isConstant);
    }
    void OnTriggerExit (Collider other) 
    {
        ClearPoison (other);
		if (clearResidentOnExit && residents.Contains (other))
			residents.Remove (other);
    }

    void TriggerPoison (Collider hit, bool constant) 
    {
        var poisonable = hit?.GetComponent<IPoisonable> ();
        if (poisonable == null)
            return;
        
		if (residents.Contains (hit))
			return;
			
		residents.Add (hit); 
        if (constant) 
            poisonable.TriggerPoison (triggerAmount, buildupRate, buildupAmount);
        else
            poisonable.AddPoison (triggerAmount);
    }
    void ClearPoison (Collider hit)
    {
        if (!isConstant)
            return;
		
		if (!hit)
			return;        

        var poisonable = hit.GetComponent<IPoisonable> ();
        if (poisonable == null)
            return;

        poisonable.LeavePoison ();
    }    
}