﻿using UnityEngine;

public interface IPoisonable 
{
    void TriggerPoison (float triggerAmount, float buildupRate, float buildupAmount);
    void AddPoison (float triggerAmount);
    void LeavePoison ();
}