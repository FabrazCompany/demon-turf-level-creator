﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoisonZone : MonoBehaviour
{
    public float triggerAmount = 0;
    public float buildupRate = 2.5f;
    public float buildupAmount = 10;

    public void TriggerPoison (Collider hit)
    {
        var poisanable = hit.GetComponent<IPoisonable> ();
        if (poisanable == null)
            return;
        
        poisanable.TriggerPoison (triggerAmount, buildupRate, buildupAmount);
    }

    public void LeavePoison (Collider hit)
    {
        var poisanable = hit.GetComponent<IPoisonable> ();
        if (poisanable == null)
            return;
        
        poisanable.LeavePoison ();
    }
}
