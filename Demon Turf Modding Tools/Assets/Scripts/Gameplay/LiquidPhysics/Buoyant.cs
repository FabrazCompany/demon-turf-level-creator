﻿using UnityEngine;

[RequireComponent (typeof (SubmersionListener))]
public class Buoyant : MonoBehaviour //, ISwimmable
{
    Rigidbody rigid;
    SubmersionListener submersion;
    Transform tr;
    [SerializeField] float restingHeight = -0.5f;
    [SerializeField] float buoyancySpeed;
    [SerializeField, Range (0, 1)] float diveDamping = 0.3f;
    [SerializeField, UnityEngine.Serialization.FormerlySerializedAs ("damping")] Vector3 waveDamping = new Vector3(0.3f, 1.0f, 0.3f);
	[SerializeField] float timeOffset = 0.0f;
    [SerializeField] bool updateWaterMaterialPerFrame = false;
    [SerializeField] float waterReentryBuffer = 0;
    float waterReentryBufferTimer = 0;

    void Awake () 
    {
        tr = transform;
        rigid = GetComponent<Rigidbody> ();
        submersion = GetComponent<SubmersionListener> ();
    }
	
    void FixedUpdate() 
	{    
        var pos = tr.position;
        pos -= submersion.getPrevWaveOffset; //offset;  //	Reset pos by subtracting the last Offset
        if (submersion.getInLiquid)
        {
            pos.y = submersion.getVolumeTop; 
            rigid.MovePosition (Vector3.MoveTowards (tr.position, pos + submersion.getWaveOffset, buoyancySpeed * Time.fixedDeltaTime));
        }
    }
}
