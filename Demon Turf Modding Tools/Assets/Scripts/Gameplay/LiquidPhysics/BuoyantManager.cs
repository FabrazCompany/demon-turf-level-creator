using UnityEngine;
using UnityEngine.Jobs;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;

public struct BuoyantData 
{	
	
}
public class BuoyantManager : FlyweightManager<SubmersionListener, BuoyantData, BuoyantManager>
{
	void Update ()
	{
		var items = slots.RawItems;
		var length = slots.Count;
		for (int i = 0; i < length; i++)
		{
			ref var slot = ref items[i];
			if (!slot.active) continue;

			slot.flyweight.StateUpdate ();
		}
	}
}