﻿using UnityEngine;

[RequireComponent (typeof (SubmersionListener))]
public class BuoyantPlatform : BaseMovingPlatform
{    
    Rigidbody rigid;
    SubmersionListener submersion;

    [Header ("Buoyancy")]
    [SerializeField, Range (0, 1)] float diveDamping = 0.3f;
    [SerializeField] float buoyancySpeed = 3;
    [SerializeField] bool applyWaveRotation;
    [SerializeField, Conditional ("applyWaveRotation"), Range (0, 1)] float waveRotationRatio = 0.3f;
    [SerializeField, Conditional ("applyWaveRotation")] float waveRotationSpeed = 3f;

    protected override void Awake () 
    {
		base.Awake ();
        rigid = GetComponent<Rigidbody> ();
        submersion = GetComponent<SubmersionListener> ();
        submersion.getOnEnterLiquid.AddListener (OnContactWater);
    }
    void OnContactWater () 
    {
        rigid.velocity *= diveDamping;
    }

    protected override bool Movement(out Vector3 goalPosition, out Quaternion goalRotation, float deltaTime) {
        goalPosition = tr.position;
        goalRotation = tr.rotation;        
        var pos = tr.position;
        pos -= submersion.getPrevWaveOffset;  //	Reset pos by subtracting the last Offset
        var offset = submersion.getWaveOffset;
        rigid.useGravity = submersion.getInLiquid; //!inWater;
        if (submersion.getInLiquid)
        {
            pos.y = submersion.getVolumeTop;
            goalPosition = Vector3.MoveTowards (tr.position, pos + offset, buoyancySpeed * deltaTime);
            if (applyWaveRotation)
            {
                var normal = offset.With (y: Mathf.Abs (offset.y)).normalized;
                var waveRot = Quaternion.Slerp (Quaternion.identity, Quaternion.LookRotation (normal, Vector3.up), waveRotationRatio);
                goalRotation = Quaternion.RotateTowards (tr.rotation, waveRot, waveRotationSpeed * deltaTime);
            }
        }
        else
        {
            goalPosition += Physics.gravity * deltaTime;
        }

		return true;
    }
}
