﻿using UnityEngine;
using UnityEngine.Events;

public class SubmersionListener : FlyweightBehavior<SubmersionListener, BuoyantData, BuoyantManager>, ISwimmable
{
	[SerializeField, ReadOnly] bool inLiquid;
	[SerializeField, ReadOnly] bool underLiquid;
	[Header ("Water Contact")]
	[SerializeField] float contactHeight;
	float cachedContactHeight;
	[SerializeField] float waterReentryBuffer = 0.2f;
	[SerializeField] float entryBuffer = 0.1f;
	[SerializeField, Range (0,1)] float entryDamping = 0.2f;
	[SerializeField] UnityEvent onEnterLiquid;
	[SerializeField] UnityEvent onExitLiquid;
	[SerializeField] bool updateWaterMaterialPerFrame = false;

	[Header ("Buoyancy")]
	[SerializeField] float submergedHeight;
	float cachedSubmergedHeight;
	[SerializeField, Range (0,1)] float motionDamping = 1;
	[SerializeField] UnityEvent onEnterSubmerged;
	[SerializeField] UnityEvent onExitSubmerged;
	[SerializeField] ParticleSystem[] managedWaterProjectors;

	[Header ("Damp Tinting")]
	[SerializeField] float dampHeightScaling = 0.8f;
	
	
	Transform tr;
	Rigidbody rigid;
	SwimmableVolume swimmableVolume;
	Vector3 prevWaveOffset;
	Vector3 waveOffset;
	Material waveMaterial;
#if !DT_EXPORT && !DT_MOD
	LuxWaterUtils.GersterWavesDescription waveDescription;
#endif
	float submersionLevel;

	float waterReentryBufferTimer = 0;
	
	public void SetOffsetHeight (float _offset) 
	{
		contactHeight = cachedContactHeight + _offset;
	}

	public SwimmableVolume getSwimVolume => swimmableVolume;
	public UnityEvent getOnEnterLiquid => onEnterLiquid;
	public UnityEvent getOnExitLiquid => onExitLiquid;
	public UnityEvent getOnEnterSubmerged => onEnterSubmerged;
	public UnityEvent getOnExitSubmerged => onExitSubmerged;
	public bool getInLiquid => inLiquid;
	public bool getUnderLiquid => underLiquid;
	public Vector3 getWaveOffset => waveOffset;
	public Vector3 getPrevWaveOffset => prevWaveOffset;
	public float getSubmersionLevel => submersionLevel;
	public float getBuoyancyPoint => (contactHeight + submergedHeight) * 0.5f;
	public float getVolumeTop => swimmableVolume != null
		? swimmableVolume.transform.position.y - submergedHeight
		: -1;

	public float getWaveTop => swimmableVolume != null
		? swimmableVolume.transform.position.y + waveOffset.y
		: transform.position.y;

	void Awake ()
	{
		tr = transform;
		rigid = GetComponent<Rigidbody> ();
		cachedContactHeight = contactHeight;
		cachedSubmergedHeight = submergedHeight;

#if UNITY_SWITCH
		for (int i = 0; i < managedWaterProjectors.Length; i++)
		{
			managedWaterProjectors[i].gameObject.SetActive (false);
		}
#endif

	}

	void OnValidate () 
	{
		if (contactHeight > submergedHeight)
			contactHeight = submergedHeight;
	}
	
	public void OnSwimmableEntered (SwimmableVolume _water) 
	{
		underLiquid = false;
		inLiquid = false;
		swimmableVolume = _water;
		swimmableVolume.onCleared += ForceExitFromVolume;
		waveMaterial = swimmableVolume.waterMaterial;
#if !DT_EXPORT && !DT_MOD
		LuxWaterUtils.GetGersterWavesDescription(ref waveDescription, waveMaterial);
#endif
		enabled = true;
	}

	public void OnSwimmableExited (SwimmableVolume _water) 
	{
		if (_water != swimmableVolume) return;

		if (underLiquid)
		{
			onExitSubmerged?.Invoke ();
			underLiquid = false;
		}	
		if (inLiquid)
		{
			onExitLiquid?.Invoke ();
			inLiquid = false;
		}
		if (swimmableVolume)
		{
			swimmableVolume.onCleared -= ForceExitFromVolume;
		}
		swimmableVolume = null;
		waveMaterial = null;
		enabled = false;
	}
	void ForceExitFromVolume (SwimmableVolume volume)
	{
		if (volume != swimmableVolume) return;

		OnSwimmableExited (volume);
	}
	public Vector3 CalculateWaveOffset (Vector3 pos, SwimmableVolume volume)
	{
#if !DT_EXPORT && !DT_MOD
		LuxWaterUtils.GetGersterWavesDescription (ref waveDescription, volume.waterMaterial);
		return LuxWaterUtils.GetGestnerDisplacement (pos, waveDescription, 0);
#else
		return Vector3.zero;
#endif
	}
	public void Clear () 
	{
		if (swimmableVolume)
			OnSwimmableExited (swimmableVolume);
	}

	public void ForceExit () 
	{
		waveOffset = prevWaveOffset = Vector3.zero;
		inLiquid = underLiquid = false;
		waterReentryBufferTimer = waterReentryBuffer;
	}

	[SerializeField] bool debug;

	public void StateUpdate () 
	{
		if (!swimmableVolume) 
		{
			if (inLiquid)
			{
				inLiquid = false;
				onExitSubmerged?.Invoke ();
				onExitLiquid?.Invoke ();
			}
			return;
		}

		if (updateWaterMaterialPerFrame && waveMaterial != null) 
		{
#if !DT_EXPORT && !DT_MOD
			LuxWaterUtils.GetGersterWavesDescription(ref waveDescription, waveMaterial);	//	Update the Gestner Wave settings from the material if needed
#endif
		}
			
		var pos = tr.position;
		pos -= waveOffset;

#if !UNITY_SWITCH
		var count = managedWaterProjectors.Length;
		if(count > 0) 
		{
			for(int i = 0; i != count; i++) 
			{
				var cpos = managedWaterProjectors[i].transform.position;
				cpos.x = pos.x;
				cpos.z = pos.z;
				managedWaterProjectors[i].transform.position = cpos;
			}
		}
#endif 

		prevWaveOffset = waveOffset;
#if !DT_EXPORT && !DT_MOD
		waveOffset = LuxWaterUtils.GetGestnerDisplacement (pos, waveDescription, 0);
#endif
		waveOffset *= motionDamping;


		var wasTouchingLiquid = inLiquid;
		var adjust = wasTouchingLiquid ? -entryBuffer : entryBuffer;
		var volumeHeight = swimmableVolume.transform.position.y + waveOffset.y + adjust;
		inLiquid = tr.position.y + contactHeight < volumeHeight;

		if (!inLiquid)
		{
			if (wasTouchingLiquid)
			{
				waveOffset = prevWaveOffset = Vector3.zero;
				underLiquid = false;
#if !UNITY_SWITCH
				for(int i = 0; i != count; i++) 
				{
					managedWaterProjectors[i].Stop ();
				}
#endif
				onExitLiquid?.Invoke ();
			}
		}
		else
		{
			if (!wasTouchingLiquid)
			{
				rigid.velocity *= entryDamping;
#if !UNITY_SWITCH
				for(int i = 0; i != count; i++) 
				{
					managedWaterProjectors[i].Play ();
				}
#endif
				onEnterLiquid?.Invoke ();
			}

			var wasUnderLiquid = underLiquid;
			underLiquid = tr.position.y + submergedHeight < volumeHeight;
			if (!wasUnderLiquid && underLiquid)
			{
				onEnterSubmerged?.Invoke ();
			}
			else if (wasUnderLiquid && !underLiquid)
			{
				onExitSubmerged?.Invoke ();
			}
		}

		submersionLevel = Mathf.InverseLerp (volumeHeight, volumeHeight - submergedHeight, tr.position.y);
	}

	public void OnDrawGizmos () 
	{
		if (!debug)
			return;
		Gizmos.color = Color.blue;
		Gizmos.DrawWireSphere (transform.position + (Vector3.up * contactHeight), 0.5f);
		
		Gizmos.color = Color.green;
		Gizmos.DrawWireSphere (transform.position + (Vector3.up * submergedHeight), 0.5f);
	}

}
