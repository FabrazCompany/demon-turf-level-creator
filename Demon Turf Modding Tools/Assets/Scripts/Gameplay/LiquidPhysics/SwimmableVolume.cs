﻿using UnityEngine;
#if !DT_EXPORT && !DT_MOD
using LuxWater;
#endif

public class SwimmableVolume : MonoBehaviour
{
	[SerializeField] Color waterTint = new Color (.6f, .6f, .6f, 1);
	[SerializeField] FootstepSurfaceType waterType = FootstepSurfaceType.WATER;
    BoxCollider volume;
#if !DT_EXPORT && !DT_MOD
    LuxWater_WaterVolume luxWater;
    LuxWaterUtils.GersterWavesDescription waveDescription;
#endif
    public Material waterMaterial { get; private set; }
    public float getVolumeY => volume.bounds.max.y;
	public Color getWaterTint => waterTint;
	public FootstepSurfaceType getWaterType => waterType;
	public event System.Action<SwimmableVolume> onCleared;

    void Awake () 
    {
        volume = GetComponent<BoxCollider> ();
#if !DT_EXPORT && !DT_MOD
        luxWater = GetComponent<LuxWater_WaterVolume> ();
#endif
        var renderer = GetComponent<MeshRenderer> ();
        waterMaterial = renderer?.material;
    }

#if UNITY_SWITCH || UNITY_PS4 || UNITY_PS5
	void Start ()
	{
		waterMaterial.SetFloat ("_Culling", 2);
	}
#endif

    void OnTriggerEnter (Collider hit)
    {
        if (hit.isTrigger)
            return;

        var swimmable = hit?.attachedRigidbody?.GetComponent<ISwimmable> ();
        swimmable?.OnSwimmableEntered (this);
    }

    void OnTriggerExit (Collider hit)
    {
        if (hit.isTrigger)
            return;

        var swimmable = hit?.attachedRigidbody?.GetComponent<ISwimmable> ();
        swimmable?.OnSwimmableExited (this);
    }

	public void ClearResidents ()
	{
		onCleared?.Invoke (this);
	}
}

public interface ISwimmable 
{
    void OnSwimmableEntered (SwimmableVolume volume);
    void OnSwimmableExited (SwimmableVolume volume);
}