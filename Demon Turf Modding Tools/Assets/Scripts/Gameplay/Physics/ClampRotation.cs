using UnityEngine;

public class ClampRotation : MonoBehaviour
{
	[SerializeField] bool useParentY;
	void Update () 
	{
		transform.rotation = Quaternion.identity;
		if (useParentY && transform.parent)
			transform.localEulerAngles = transform.localEulerAngles.With (y: 0);
	}
}