﻿using UnityEngine;

[RequireComponent (typeof (Cloth))]
public class ClothAssigner : MonoBehaviour
{
    Cloth cloth;
    void Awake () 
    {
        cloth = GetComponent<Cloth> ();
        cloth.capsuleColliders = new CapsuleCollider[1];
    }
    public void OnEnter (Collider hit)
    {
        var capsule = hit.GetComponent<CapsuleCollider> ();
        if (!capsule)  return;
        
        var colliders = cloth.capsuleColliders;
        colliders[0] = capsule;
        cloth.capsuleColliders = colliders;
    }
    public void OnExit (Collider hit)
    {
		
        var capsule = hit.GetComponent<CapsuleCollider> ();
        if (!capsule || capsule != cloth.capsuleColliders[0]) return;
        var colliders = cloth.capsuleColliders;
        colliders[0] = null;
        cloth.capsuleColliders = colliders;
    }
}