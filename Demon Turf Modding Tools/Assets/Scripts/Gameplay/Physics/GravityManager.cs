using UnityEngine;
using UnityEngine.Jobs;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;

public struct GravityData 
{
	public float gravityScale;
	public Rigidbody rigid;
	public SlowMoEntity time;
	public float sleepTimer;
}
public struct GravityJobData 
{
	public float velocityThresholdScalar;
	public float torqueThresholdScalar;
	public bool belowSleepThreshold;
}

[BurstCompile(CompileSynchronously = true)]
struct GravityOptimizerJob : IJobParallelFor
{
	// public float3 camPos;
	public NativeArray<GravityJobData> stateArray;
	public float scalar;

	void IJobParallelFor.Execute(int index)
	{
		var data = stateArray[index];
		// var data = stateArray[transformIndex];
		// data.optimize = math.distance (camPos, transform.position) >= (data.maxDistance * scalar);
		// stateArray[transformIndex] = data;
	}	
}
public class GravityManager : FlyweightTransformJobManager<HasGravity, GravityData, GravityJobData, GravityManager>
{
	const float gravity = 19.62f;
	const float velocityThreshold = 0.3f;
	const float torqueThreshold = 0.1f;
	const float sleepTriggerDelay = 0.25f;


	void FixedUpdate()
	{
		var items = slots.RawItems;
		var length = slots.Count;
		var delta = Time.deltaTime;
		for (int i = 0; i < length; i++)
		{
			ref var slot = ref items[i];
			if (!slot.active) continue;

			ref var data = ref slot.data;
			if (data.gravityScale == 0) continue;

			if (data.rigid.IsSleeping ())
			{
				if (!data.rigid.useGravity)
					data.rigid.useGravity = true;
			}
			else 
			{
				if (data.gravityScale == 1f)
				{
					var lowVelocity = Mathf.Abs (data.rigid.velocity.sqrMagnitude) <= velocityThreshold * velocityThreshold;
					var lowTorque = Mathf.Abs (data.rigid.angularVelocity.sqrMagnitude) <= torqueThreshold * torqueThreshold;
					if (lowVelocity && lowTorque)
					{
						data.sleepTimer += Time.deltaTime;
						if (data.sleepTimer >= sleepTriggerDelay)
						{
							data.rigid.useGravity = true;
							continue;
						}
					}
					else 
						data.sleepTimer = 0;
				}

				if (data.rigid.useGravity)
					data.rigid.useGravity = false;
				var timeScale = data.time?.timeScale ?? Time.timeScale;
				var velocity = data.rigid.velocity;
				velocity.y -= gravity * data.gravityScale * delta * timeScale;
				data.rigid.velocity = velocity;
			}
		}
	}
}