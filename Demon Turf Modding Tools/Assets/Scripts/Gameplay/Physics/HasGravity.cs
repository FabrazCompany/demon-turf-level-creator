using UnityEngine;

[DisallowMultipleComponent, RequireComponent (typeof (Rigidbody), typeof (SlowMoEntity))]
public class HasGravity : FlyweightTransformJobBehavior<HasGravity, GravityData, GravityJobData, GravityManager>
{
	[SerializeField] float gravityScale = 1f;
	[SerializeField] Rigidbody rigid;
	[SerializeField] SlowMoEntity time;
	float sleepTimer;
	
	void OnValidate ()
	{
		if (!rigid)
			rigid = GetComponent<Rigidbody> ();
		if (!time)
			time = GetComponent<SlowMoEntity> ();
	}
	void Awake ()
	{
		if (!rigid)
			rigid = GetComponent<Rigidbody> ();
		if (!time)
			time = GetComponent<SlowMoEntity> ();
	}
	
	protected override void OnEnable () 
	{
		base.OnEnable ();

		ref var d = ref data;
		d.gravityScale = gravityScale;
		d.rigid = rigid;
		d.time = time;
		d.sleepTimer = sleepTimer;

		rigid.useGravity = false;
	}

	public void SetGravityScale (float val)
	{
		if (!isSet) return;

		var data = this.data;
		data.gravityScale = gravityScale = val;
	}

	protected override GravityJobData InitializeJobData()
	{
		return new GravityJobData () 
		{
			velocityThresholdScalar = 1,
			torqueThresholdScalar = 1,
			belowSleepThreshold = false,
		};
	}
}