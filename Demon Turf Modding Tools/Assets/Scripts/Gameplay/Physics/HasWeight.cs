using System.Collections.Generic;
using UnityEngine;

public class HasWeight : FlyweightBehavior<HasWeight, WeightData, WeightManager>, IWeighted, IReceivePlayerWeight
{
    [SerializeField] float weight;
    [SerializeField] Rigidbody rigid;
	List<IWeighted> occupants;

    public float getWeight => weight + data.addedWeight;
    public float setWeight (float value) => weight = value;
    public Rigidbody getRigid => rigid;
    
	void OnValidate ()
	{
		if (!rigid)
	        rigid = GetComponent<Rigidbody> ();
	}
    void Awake ()
    {
		if (!rigid)
	        rigid = GetComponent<Rigidbody> ();
        occupants = new List<IWeighted> ();
    }

	protected override void OnEnable ()
	{
		base.OnEnable ();

		ref var d = ref data;
		d.addedWeight = weight;
		d.occupants = occupants;
	}
    const float playerWeightScaler = -.025f;
    void IReceivePlayerWeight.ApplyPlayerWeight(Vector3 origin, float massVal)
    {
		ref var d = ref data;
        d.playerWeight = massVal * playerWeightScaler;
        enabled = true;
    }

    void OnCollisionStay (Collision hit) 
    {

        var weight = hit?.rigidbody?.GetComponent<IWeighted> ();
        if (weight == null)
            return;

        if (hit.contactCount == 0)
            return;
            
        if (hit.GetContact(0).normal.y >= 0.8f)
            return;

        if (occupants.Contains (weight))
            return;

        occupants.Add (weight);
        enabled = true;
    }

    void OnCollisionExit (Collision hit)
    {
        var weight = hit?.rigidbody?.GetComponent<IWeighted> ();
        if (weight == null)
            return;

        if (!occupants.Contains (weight))
            return;

        occupants.Remove (weight);
        enabled = true;
    }    
}