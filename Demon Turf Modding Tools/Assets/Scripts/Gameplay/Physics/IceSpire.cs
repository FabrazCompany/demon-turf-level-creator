using System.Collections;
using UnityEngine;

public class IceSpire : MonoBehaviour
{
    [SerializeField] Transform appearRoot;
    DeliverFreeze freeze;
    MeshCollider coll;
    Vector3 initialScale;
    

    [SerializeField] ParticleSystem destroyParticles;
    [SerializeField] ParticleSystem appearingParticlesTop;
    ParticleSystem appearingParticlesTopInstance;
    [SerializeField] ParticleSystem appearingParticlesGround;
    ParticleSystem appearingParticlesGroundInstance;

    float timer;
    void Awake () 
    {
        coll = GetComponent<MeshCollider> ();
        freeze = GetComponent<DeliverFreeze> ();
        initialScale = transform.localScale;
        
        freeze.onFreeze += OnFreeze;
    }
    void OnDestroy () 
    {
        if (freeze)
            freeze.onFreeze -= OnFreeze;
    }
    public void OnAppeared () 
    {
        if (appearingParticlesTopInstance)
            TrashMan.despawn (appearingParticlesTopInstance.gameObject);

        appearingParticlesTopInstance = TrashMan.spawn (appearingParticlesTop.gameObject, appearRoot.position + new Vector3 (0, 0.5f, 0)).GetComponent<ParticleSystem> ();
		appearingParticlesTopInstance.transform.localScale = Vector3.one;
        appearingParticlesTopInstance.transform.SetParent (transform);
        appearingParticlesGroundInstance = TrashMan.spawn (appearingParticlesGround.gameObject, transform.position).GetComponent<ParticleSystem> ();  
        ApplyYScaling (0); 
    }

    public void ApplyYScaling (float pct) => transform.localScale = initialScale.With (y: initialScale.y * pct);
    public void OnFormed () 
    {
        appearingParticlesTopInstance?.transform.SetParent (null);
        appearingParticlesTopInstance?.Stop ();
        appearingParticlesGroundInstance?.Stop ();   
    }
    public void OnExpired () => Despawn ();

    void OnFreeze (Transform target) => Despawn ();
    void Despawn () 
    {
        TrashMan.spawn (destroyParticles.gameObject, transform.position, transform.rotation);
        TrashMan.despawn (gameObject);
    }

}