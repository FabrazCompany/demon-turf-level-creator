using UnityEngine;

[RequireComponent (typeof (Rigidbody))]
public class LockLocalVelocity : MonoBehaviour 
{
    Rigidbody rigid;
    [SerializeField] bool x;
    [SerializeField] bool y;
    [SerializeField] bool z;
    Vector3 startPosition;

    void Awake () 
    {
        rigid = GetComponent<Rigidbody> ();
        startPosition = transform.localPosition;
    }

    void LateUpdate ()
    {
        var localVel = transform.InverseTransformDirection (rigid.velocity);
        var localPos = transform.localPosition;
        if (x)
        {
            localVel.x = 0;
            localPos.x = startPosition.x;
        }
            
        if (y)
        {
            localVel.y = 0;
            localPos.y = startPosition.y;
        }
            
        if (z)
        {
            localVel.z = 0;
            localPos.z = startPosition.z;
        }
        transform.localPosition = localPos;
        rigid.velocity = transform.TransformDirection (localVel);
    }
}