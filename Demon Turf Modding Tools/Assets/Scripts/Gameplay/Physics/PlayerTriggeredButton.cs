using UnityEngine;
using UnityEngine.Events;

public class PlayerTriggeredButton : MonoBehaviour, IReceivePlayerWeight
{    
    [SerializeField] UnityEvent onTriggered;
    bool triggered;

    public void setTriggered (bool value) => triggered = value;
    public void ApplyPlayerWeight (Vector3 origin, float massVal) 
    {
        if (triggered)
            return;

        triggered = true;
        onTriggered?.Invoke ();
    }
}
