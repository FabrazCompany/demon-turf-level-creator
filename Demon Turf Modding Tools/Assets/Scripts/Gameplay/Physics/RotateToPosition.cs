using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateToPosition : MonoBehaviour, ITargeted
{
    [SerializeField] float turnSpeed;
	[SerializeField] bool useConstantForce;
    Vector3 target;

    public void AssignTarget (Transform _target) => target = _target.position;
	public void AssignTarget (Vector3 _target) => target = _target;
    public void ClearTarget () => target = Vector3.zero;
	public void SetSpeed (float speed) => turnSpeed = speed;

    void FixedUpdate () 
    {
        var heading = (target - transform.position).normalized;
		// transform.rotation = Quaternion.RotateTowards (transform.rotation, Quaternion.LookRotation (heading, Vector3.up), turnSpeed * Time.deltaTime)).normalized;
		if (useConstantForce)
			transform.rotation = Quaternion.RotateTowards (transform.rotation, Quaternion.LookRotation (heading, transform.up), turnSpeed * Time.deltaTime).normalized;
		else 
 	       transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation (heading, Vector3.up), 1 - Mathf.Exp (-turnSpeed * Time.deltaTime)).normalized;
    }

	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawSphere (target, .5f);	
	}
}
