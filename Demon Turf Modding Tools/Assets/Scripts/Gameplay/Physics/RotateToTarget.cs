﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateToTarget : MonoBehaviour, ITargeted
{
    [SerializeField] float turnSpeed;
    Transform target;

    public void AssignTarget (Transform _target)
    {
        target = _target;
    }
    public void ClearTarget ()
    {
        target = null;
    }

    void FixedUpdate () 
    {
        if (!target)
            return;
        var heading = (target.position - transform.position).normalized;
        transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation (heading, Vector3.up), 1 - Mathf.Exp (-turnSpeed * Time.deltaTime)).normalized;
    }
    
    
}
