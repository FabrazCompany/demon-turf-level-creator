﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snowball : MonoBehaviour
{
    [SerializeField, Range (0,1)] float scale = 0;
    [SerializeField] BallState startValues;
    [SerializeField] BallState endValues;
    [SerializeField] float velocityCutoff = 2f;
    [SerializeField] float velocityScalar = 0.1f;
    [SerializeField] LayerMask groundLayer;

    Rigidbody rigid;
    HasWeight weight;
    SnowDeformZone snowZone;
    new SphereCollider collider;
    Collider[] neighbours;
    const int maxNeighbours = 8;

    void Awake ()
    {
        rigid = GetComponent<Rigidbody> ();
        weight = GetComponent<HasWeight> ();
        collider = GetComponent<SphereCollider> ();
        neighbours = new Collider[maxNeighbours];
        enabled = snowZone != null;
    }
    void Start () 
    {
        SetScaleValues ();
    }


    public void RegisterSnowZone (SnowDeformZone zone)
    {
        snowZone = zone;
        enabled = true;
    }
    public void ClearSnowZone () 
    {
        snowZone = null;
        enabled = false;
    }

    void FixedUpdate () 
    {
        var curVel = rigid.velocity.magnitude / Time.deltaTime;
        if (curVel == Mathf.Infinity || curVel == float.NaN || curVel < velocityCutoff)
            return;
        
        scale = Mathf.Clamp01 (scale + (curVel * velocityScalar));
        SetScaleValues ();
    }

    void SetScaleValues () 
    {
        rigid.mass = Mathf.Lerp (startValues.mass, endValues.mass, scale);
        transform.localScale = Vector3.one * Mathf.Lerp (startValues.scale, endValues.scale, scale);
        weight.setWeight (Mathf.Lerp (startValues.weight, endValues.weight, scale));
        
        int colliderCount = Physics.OverlapSphereNonAlloc (transform.position, collider.radius, neighbours, groundLayer);
        for (int i = 0; i < colliderCount; i++)
        {
            var coll = neighbours[i];
            if (coll == collider)
                return;

            Vector3 otherPosition = coll.gameObject.transform.position;
            Quaternion otherRotation = coll.gameObject.transform.rotation;

            if (Physics.ComputePenetration( collider, transform.position, transform.rotation,
                                            coll, otherPosition, otherRotation,
                                            out var direction, out var distance))
            {
                transform.Translate (direction * distance);
            }
        }
    }


    [System.Serializable]
    struct BallState 
    {
        public float scale;
        public float mass;
        public float weight;
    }
}
