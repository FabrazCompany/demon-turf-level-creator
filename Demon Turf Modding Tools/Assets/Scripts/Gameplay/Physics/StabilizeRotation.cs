﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StabilizeRotation : MonoBehaviour
{
    Rigidbody rigid;
    [SerializeField] Vector3 targetRotation;
    [SerializeField] float correctionSpeed;
    [SerializeField] bool scaleDownWhenClose;

    void Awake () {
        rigid = GetComponent<Rigidbody>();    
    }

    void FixedUpdate () {
        var pct = (Vector3.Dot (Vector3.up, transform.up) + 1);
		// var pct = (Vector3.Dot (Vector3.up, targetRotation) + 1);
        var force = scaleDownWhenClose ? Mathf.Lerp (0, correctionSpeed, pct) : correctionSpeed;
 
		//get the angle between transform.forward and target delta
		float angleDiff = Vector3.Angle(transform.up, Vector3.up);
 
		// get its cross product, which is the axis of rotation to
		// get from one vector to the other
		Vector3 cross = Vector3.Cross(transform.up, Vector3.up);
 
		// apply torque along that axis according to the magnitude of the angle.
		rigid.AddTorque(cross * angleDiff * force);
    }

}
