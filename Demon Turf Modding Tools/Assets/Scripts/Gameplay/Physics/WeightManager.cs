using System.Collections.Generic;
using UnityEngine;

public struct WeightData 
{
	public float addedWeight;
	public float playerWeight;
	public List<IWeighted> occupants;
}
public class WeightManager : FlyweightManager<HasWeight, WeightData, WeightManager>
{
	const float gravity = 19.62f;
	const float sleepThreshold = 1f;
	const float sleepTriggerDelay = 0.25f;


	void FixedUpdate()
	{
		var items = slots.RawItems;
		var length = slots.Count;
		var delta = Time.deltaTime;
		for (int i = 0; i < length; i++)
		{
			ref var slot = ref items[i];
			if (!slot.active || !slot.visible) 
				continue;

			ref var data = ref slot.data;

			var addedWeight = 0f;
			if (data.playerWeight > 0)
			{
				addedWeight += data.playerWeight;
				data.playerWeight = 0;
			}

			var occupants = data.occupants;
			var count = occupants.Count;
			for (int ii = 0; ii < count; ii++)
			{
				addedWeight += occupants[ii].getWeight;
			}

			data.addedWeight = addedWeight;
		}
	}
}