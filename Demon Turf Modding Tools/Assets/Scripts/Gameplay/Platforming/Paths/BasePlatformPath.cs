﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class BasePlatformPath : MonoBehaviour
{
    protected enum MovementType {
        OneWay,
        Loop,
        PingPong,
    }

    [SerializeField] protected MovementType movementType;
    protected int direction = 1;
	int _index = 0;
	int _lastIndex = 0;
	protected int index 
	{
		get => _index;
		set 
		{
			_lastIndex = _index;
			_index = value;
		}
	}
    public abstract Transform firstPoint { get; }
    public abstract bool atEnd { get; }
	public bool getDirection => direction == 1;
	public int getIndex => index;
	public int getLastIndex => _lastIndex;

	public void SetDirection (bool forward) 
	{
		direction = forward ? 1 : -1;
	}
	public void FlipDirection () 
	{
		direction *= -1;
	}

    public abstract IEnumerator<Transform> GetPathEnumerator();
}
