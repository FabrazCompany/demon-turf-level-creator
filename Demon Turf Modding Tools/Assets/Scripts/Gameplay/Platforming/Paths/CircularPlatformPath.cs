﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CircularPlatformPath : BasePlatformPath
{   
    [SerializeField] protected UnityEvent onEndReached;
    [SerializeField] Orientation orientation;
    [SerializeField] float radius;
    [SerializeField] bool counterClockwise;
    [SerializeField, Range(0, 360)] float circumference = 360;
    [SerializeField, Range(0, 360)] float startPoint ;
    [SerializeField] int segments = 100;

    Vector3[] CirclePoints;
    Transform pathPoint;
	int startingInd = 0;


    public override Transform firstPoint => pathPoint;
    public override bool atEnd => movementType == MovementType.OneWay && index >= CirclePoints.Length - 1;
	public float getRadius => radius;
    void OnEnable () {
        if (pathPoint == null) {
            pathPoint = new GameObject ().transform;
            pathPoint.name = "PathPoint";
            pathPoint.parent = transform;    
        }
        direction = counterClockwise ? 1 : -1;
        CirclePoints = FzMath.CalculateCirclePoints (orientation, radius, segments, startPoint, circumference);
    }

	public override IEnumerator<Transform> GetPathEnumerator()
	{
		if(CirclePoints == null || CirclePoints.Length < 1) {
			yield break;
		}
		index = direction >= 0 ? startingInd : (startingInd > 0 ? startingInd - 1 : CirclePoints.Length - 1);
		while (true) {
            pathPoint.position = transform.position + CirclePoints[index];
			yield return pathPoint;
			
			if (CirclePoints.Length == 1)
				continue;
                			
			if (index + direction < 0) {
				if (movementType == MovementType.PingPong) {
					direction = 1; 
                    onEndReached?.Invoke ();
				} else {
					index = CirclePoints.Length;
				}
            }							
			else if(index + direction > CirclePoints.Length - 1) {
                if (movementType == MovementType.PingPong) {
                    direction = -1;
                    onEndReached?.Invoke ();
                } else {
                    index = 0;
                }
            }
			index = Mathf.Clamp (index + direction, 0, CirclePoints.Length - 1);
		}
	}

	public void ResetIndex (Vector3 owner)
	{
		var dist = Mathf.Infinity;
		var ind = -1;
		for (int i = 0; i < CirclePoints.Length; i++)
		{
			var newDist = Vector3.SqrMagnitude (owner - (transform.position + CirclePoints[i]));
			if (newDist < dist)
			{
				ind = i;
				dist = newDist;
			}
		}
		if (ind >= 0)
			startingInd = index = ind;
	}
	
	void OnDrawGizmos() {
	    var points = FzMath.CalculateCirclePoints (orientation, radius, segments, startPoint, circumference);//CalculatePoints (startPoint, circumference);
		
		if (points.Length < 2)
			return;

		// Gizmos.color = Color.red;
		// Gizmos.DrawWireSphere (transform.position + points[startingInd], 2f);
		
		for (var i = 1; i < points.Length; i++)
		{
            Gizmos.color = i == 1 ? Color.red : Color.white; 
			Gizmos.DrawLine(transform.position + points[i - 1], transform.position + points[i]);
		}
        
        if (movementType == MovementType.Loop)
            Gizmos.DrawLine (transform.position + points[0], transform.position + points[points.Length - 1]);
	}
}
