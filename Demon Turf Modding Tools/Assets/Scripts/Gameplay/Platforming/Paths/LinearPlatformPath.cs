﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LinearPlatformPath : BasePlatformPath
{
	[SerializeField] Transform[] points;
    public override Transform firstPoint => points?[0];
    public override bool atEnd => movementType == MovementType.OneWay && index >= points.Length - 1;

	public override IEnumerator<Transform> GetPathEnumerator() {
		if(points == null || points.Length < 1)
			yield break;
						
		direction = 1;
		index = 0;

		while (true) {
			yield return points[index];
			
			if(points.Length == 1)
				continue;

            if (index <= 0) {
                direction = 1;
            } else if (index >= points.Length - 1) {  //reached end of path
                if (movementType == MovementType.Loop)  {
                    index = -1;
                } else if (movementType == MovementType.OneWay) {
                    direction = 0;
                } else if (movementType == MovementType.PingPong) {
                    direction = -1;
                }
            }
            index = index + direction;
		}
	}
	
	void OnDrawGizmos() {		
		if(points == null || points.Length < 2)
			return;			
		
		var _points = points.Where(t=> t != null).ToList();
		
		if (_points.Count < 2)
			return;
		
        for (int i = 0; i < _points.Count - 1; i++) {
            var curNode = _points[i];
            Gizmos.color = Color.white;
            Gizmos.DrawSphere (curNode.position, 0.1f);
            Gizmos.DrawLine (curNode.position, _points[i + 1].position);
            var dir = _points[i + 1].position - curNode.position;
            Gizmos.color = Color.red;
            Gizmos.DrawRay (curNode.position, dir.normalized);
        }

        if (movementType == MovementType.Loop) {
            Gizmos.color = Color.white;
            var lastNode = _points[_points.Count - 1];
            var firstNode = _points[0];
            Gizmos.DrawLine (lastNode.position, firstNode.position);
            
            var dir = firstNode.position - lastNode.position;
            Gizmos.color = Color.red;
            Gizmos.DrawRay (lastNode.position, dir.normalized);
        } else {
            Gizmos.color = Color.white;
            Gizmos.DrawSphere (_points[_points.Count - 1].position, 0.1f);
        }
    }
}
