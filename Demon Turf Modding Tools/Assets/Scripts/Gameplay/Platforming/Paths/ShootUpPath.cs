﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootUpPath : BasePlatformPath
{  
    [SerializeField] Vector3 offset;
    Transform _startPoint;

    Transform _endPoint;

    void Start () 
    {
        _startPoint = new GameObject ("Start Point").transform;
        _startPoint.position = transform.position;
        Debug.Log (_startPoint, this);

        _endPoint = new GameObject ("End Point").transform;
        _endPoint.position = _startPoint.position + offset;
    }
    void OnDestroy () 
    {
        if (_startPoint)
            Destroy (_startPoint.gameObject);
        if (_endPoint)
            Destroy (_endPoint.gameObject);
    }

    public override Transform firstPoint => _startPoint;
    public override bool atEnd => Vector3.Distance (transform.position, _endPoint.position) <= 0.05f;

    public override IEnumerator<Transform> GetPathEnumerator()
    {
        yield return _startPoint;
        yield return _endPoint;
    }

    void OnDrawGizmosSelected () 
    {
        Gizmos.color = Color.white;
        if (Application.isPlaying)
        {
            Gizmos.DrawLine (_startPoint.position, _endPoint.position);
        }
        else
        {
            Gizmos.DrawLine (transform.position, transform.position + offset);
        }
    }
}
