using UnityEngine;

public class AcceleratingMovingPlatform : MovingPlatform 
{
	[Space]
	[SerializeField] float acceleration;
	[SerializeField] float deacceleration;
	float currentAcceleration;
	bool reversing;

	protected override void OnEnable () 
	{
		base.OnEnable ();
		mover.enabled = true;
		currentAcceleration = 0;
		currentSpeed = 0;
	}
	protected override void OnDisable ()
	{
		base.OnDisable ();
		mover.enabled = false;
	}

	public void SetSpeeds (float _acceleration, float _speed)
	{
		acceleration = _acceleration;
		speed = _speed;
	}
	public void Trigger () => Trigger (speed);
	public void Trigger (float _speed)
	{
		speed = _speed;
		currentAcceleration = acceleration;
		reversing = false;
	} 
	
	public void Halt () 
	{
		speed = 0;
		currentAcceleration = deacceleration;
		reversing = false;
	}

	public void FlipDirection () 
	{
		if (currentSpeed == 0)
		{
			SetDirection (!path.getDirection);
		}
		else 
		{
			currentAcceleration = deacceleration;
			reversing = true;
		}
	}

	public void SetDirection (bool _forward)
	{
		if (path.getDirection == _forward) return;
		path.SetDirection (_forward);
		_currentPoint.MoveNext ();
		currentAcceleration = acceleration;
		reversing = false;
	}
	protected override bool Movement (out Vector3 goalPosition, out Quaternion goalRotation, float deltaTime)
	{
		if (reversing)
		{
			currentSpeed = Mathf.MoveTowards (currentSpeed, 0, currentAcceleration * deltaTime);
			if (Mathf.Approximately (currentSpeed, 0))
			{
				SetDirection (!path.getDirection);
			}
		}
		else 
		{
			currentSpeed = Mathf.MoveTowards (currentSpeed, speed, currentAcceleration * deltaTime);
		}
		return base.Movement (out goalPosition, out goalRotation, deltaTime);
	}

}