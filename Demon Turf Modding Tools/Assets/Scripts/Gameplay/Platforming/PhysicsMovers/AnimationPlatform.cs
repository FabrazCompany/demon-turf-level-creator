using UnityEngine;
#if !DT_EXPORT && !DT_MOD
using KinematicCharacterController;
#endif

public class AnimationPlatform : MonoBehaviour, IMoverController
{
    void Awake () 
    {
        ModStub_PhysicsMover mover = GetComponent<ModStub_PhysicsMover> ();
        mover.MoverController = this;
    }
    bool IMoverController.UpdateMovement (out Vector3 goalPosition, out Quaternion goalRotation, float deltaTime)
    {
        goalPosition = transform.position;
        goalRotation = transform.rotation;
		return false;
    }
}