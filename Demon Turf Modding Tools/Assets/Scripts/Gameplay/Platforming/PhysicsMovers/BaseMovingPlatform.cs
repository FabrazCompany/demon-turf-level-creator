﻿using UnityEngine;

[RequireComponent(typeof(ModStub_PhysicsMover))]
public abstract class BaseMovingPlatform : FlyweightTransformJobBehavior<BaseMovingPlatform, MovingPlatformData, MovingPlatformJobData, MovingPlatformManager>, IMoverController
{
	protected Transform tr;
	[SerializeField] protected ModStub_PhysicsMover mover;
	[SerializeField] float distanceCheckScalar = 1;
	[SerializeField] float optimizerScalar = 1;
	protected bool optimize;
	bool deltaAdjustQueued;
	
	float lastDeltaRecordTime;
	
	float lastOptimizeRecordTime;

	public virtual bool Optimize 
	{
		get => optimize;
		set 
		{
			if (optimize != value)
			{
				if (value)
				{
					calculatedPosition = tr.position;
					calculatedRotation = tr.rotation;
				}

				lastOptimizeRecordTime = Time.time;
				optimize = value;
			}			
		}
	}
	float getDeltaAdjust => lastOptimizeRecordTime - lastDeltaRecordTime;
	
	protected Vector3 calculatedPosition;
	protected Quaternion calculatedRotation;
	protected Vector3 cachedPosition;
	protected Quaternion cachedRotation;

	protected bool ignoreTimeScale;
	public void SetIgnoreTimeScale (bool value) => ignoreTimeScale = value;

	void OnValidate ()
	{
		if (!mover)
			mover = GetComponent<ModStub_PhysicsMover> ();
	}
	protected virtual void Awake ()
	{
		tr = transform;
		if (!mover)
	        mover = GetComponent<ModStub_PhysicsMover> ();
		mover.MoverController = this;
		mover.Interpolate = true;
		mover.InitialTickPosition = calculatedPosition = cachedPosition = tr.position;
		mover.InitialTickRotation = calculatedRotation = cachedRotation = tr.rotation;
	}
	protected override void OnEnable ()
	{
		base.OnEnable ();
		ref var d = ref data;
		d.mover = mover;

		optimize = false;
	}
	protected override MovingPlatformJobData InitializeJobData() 
	{
		return new MovingPlatformJobData ()
		{
			optimize = false,
			dist = -1,
			distScalar = distanceCheckScalar,
		};
	}

	public void StateUpdate (float deltaTime, float unscaledDeltaTime) 
	{
		lastDeltaRecordTime = Time.time;
		var delta = ignoreTimeScale ? unscaledDeltaTime : deltaTime;

		if (!ignoreTimeScale && !optimize) return;

		if (deltaAdjustQueued)
		{
			deltaAdjustQueued = false;
			delta += getDeltaAdjust;
		}
		bool hasMoved = Movement (out calculatedPosition, out calculatedRotation, delta);
		if (hasMoved)
		{
			mover.InitialTickPosition = calculatedPosition;
			mover.InitialTickRotation = calculatedRotation;
			mover.SetPositionAndRotation (calculatedPosition, calculatedRotation);
		}
	}	
	bool IMoverController.UpdateMovement (out Vector3 goalPosition, out Quaternion goalRotation, float deltaTime)
	{
		lastDeltaRecordTime = Time.time;
		if (ignoreTimeScale || optimize)
		{
			goalPosition = calculatedPosition;
			goalRotation = calculatedRotation;
			return false;
		}
		else 
		{
			if (deltaAdjustQueued)
			{
				deltaAdjustQueued = false;
				deltaTime += getDeltaAdjust;
			}
			return Movement (out goalPosition, out goalRotation, deltaTime);
		}
	}
	protected abstract bool Movement(out Vector3 goalPosition, out Quaternion goalRotation, float deltaTime);
}
