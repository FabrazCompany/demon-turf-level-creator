﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
#if !DT_MOD
using KinematicCharacterController;
#endif

[RequireComponent (typeof (ModStub_PhysicsMover))]
public class MovingPlatform : BaseMovingPlatform
{
	enum RotationType {
		None,
		AtTurns,
		Constant,
		AtTarget,
		FromLastPoint,
	}
	[SerializeField] protected BasePlatformPath path;
    [SerializeField] protected float speed;
	protected float currentSpeed;
	[SerializeField] RotationType rotationType;
	[SerializeField, Conditional ("rotationType", RotationType.Constant)] Vector3 turnAxis;
    [SerializeField, Conditional ("rotationType", RotationType.None, true)] float turnSpeed;
	[SerializeField, Conditional ("rotationType", RotationType.AtTarget)] Transform rotationTarget;
	[SerializeField, Conditional ("rotationType", RotationType.AtTurns)] float rotationOffset;
	[SerializeField] float maxDistanceToGoal = .1f;
	[SerializeField] bool overrideResetPath = false;

    [Header ("Timing")]
    [SerializeField] float startDelay = 0;
	[SerializeField] float waitDelay = 0;
	[SerializeField] bool disableIfFinished;
	[SerializeField] UnityEvent onFinished;
	[SerializeField] UnityEventInt onNodeLeft;
	[SerializeField] UnityEventInt onNodeReached;
	[SerializeField] NodeReachedEvent[] onTargetNodesReached;
	[SerializeField] bool snapToPathOnStart;

	[System.Serializable]
	struct NodeReachedEvent 
	{
		public Transform node;
		public UnityEvent onNodeReached;
	}
    protected IEnumerator<Transform> _currentPoint;
	Vector3 lastPoint;
	Vector3 lastForward;	
	SlowMoEntity timeEntity;
	public bool getDirection => path.getDirection;
	
	public void SetSpeed (float _speed) => currentSpeed = speed = _speed;
	public UnityEventInt getOnNodeReached => onNodeReached;
	public void SetRotationTarget (Transform target) => rotationTarget = target;
	public void AssignToMover ()
	{
		if (mover == null)
			return;
		mover.MoverController = this;
	}
	void OnValidate ()
	{
		if (mover == null)
			mover = GetComponent<ModStub_PhysicsMover> ();
	}	
	protected override void Awake () 
	{
		base.Awake ();
		currentSpeed = speed;
		timeEntity = this.GetOrAddComponent<SlowMoEntity> ();
	}
	bool startHasRun;
	protected virtual void Start () {
		
		if (path == null) // if the path is null we trigger an error and exit
		{
			Debug.LogError("Path Cannot be null!", gameObject);
			return;
		}
		startHasRun = true;
		InitializePlatform ();
	}
	protected override void OnEnable () 
	{
		base.OnEnable ();
		atEnd = false;
		if (!startHasRun)
			return;		
		InitializePlatform ();
	}
	bool initd;
	public void InitializePlatform ()
	{
		if (initd) return;
		initd = true;
		if (mover.MoverController != (IMoverController)this)
			mover.MoverController = this;
		mover.InitialTickPosition = cachedPosition;
		mover.InitialTickRotation = cachedRotation;
		mover.Rigidbody.position = cachedPosition;
		mover.Rigidbody.rotation = cachedRotation;
		ResetPath (!overrideResetPath);
	}
	public void ResetPlatform () 
	{
		ResetPath ();
		if (log)
			Debug.LogFormat ("Resetting! Position {0}, Rotation {1}", tr.position, tr.rotation);
		mover.enabled = true;
	}
	public void SetPath (BasePlatformPath _path) 
	{
		path = _path;
		ResetPath (false);
	}
	public void SetStartupState (Vector3 pos, Quaternion rot)
	{
		cachedPosition = pos;
		lastPoint = cachedPosition;
		cachedRotation = rot;
		lastForward = cachedRotation.eulerAngles;
		mover.InitialTickPosition = cachedPosition;
		mover.InitialTickRotation = cachedRotation;
		atEnd = false;
		Debug.Log ($"Setting position {cachedPosition}");
		
		if (mover)
		{
			mover.SetPosition (cachedPosition);
			mover.SetRotation (cachedRotation);
		}
	}
	[ContextMenu ("Reset Path")]
	public void ResetPathPosition () => ResetPath (true);
	public void ResetPoint () => _currentPoint.MoveNext ();
	protected virtual void ResetPath (bool resetPosition = true) {
		_currentPoint = path.GetPathEnumerator();
		_currentPoint.MoveNext();
		
		waitTimer = startDelay;
		atEnd = false;
		if(_currentPoint.Current == null) return;		
		
		calculatedPosition = _currentPoint.Current.position;
		goalPoint = calculatedPosition;
		calculatedRotation = cachedRotation;
		if (resetPosition)
		{
			lastPoint = (Vector3)_currentPoint?.Current.position;
			lastForward = _currentPoint?.Current.forward ?? tr.forward;
			tr.position = calculatedPosition;
			tr.rotation = calculatedRotation;
			mover.InitialTickPosition = calculatedPosition;
			mover.InitialTickRotation = calculatedRotation;
			if (!ignoreTimeScale)
			{
				mover.SetPosition (calculatedPosition);
				mover.SetRotation (calculatedRotation);
			} 			
			_currentPoint.MoveNext();
		}
	}
		
	float waitTimer;
	Vector3 goalPoint;
	public void SetWaitDelay (float _delay) => waitTimer = _delay;
	protected override bool Movement (out Vector3 goalPosition, out Quaternion goalRotation, float deltaTime) 
	{
        var _point = _currentPoint?.Current;
		goalPosition = tr.position;
		goalRotation = tr.rotation;
        if (_point == null || !enabled) 
			return false;

		deltaTime *= timeEntity.timeScale;
		var willTurn = turnSpeed != 0.0f;
		if (willTurn)
		{
			switch (rotationType) 
			{
				case RotationType.AtTurns: {
					var dir = (_point.position - lastPoint).normalized;
					goalRotation = Quaternion.RotateTowards (tr.rotation, Quaternion.LookRotation (dir, tr.up) * Quaternion.AngleAxis (rotationOffset, Vector3.up), turnSpeed * deltaTime);
				} break;
				case RotationType.Constant:
					goalRotation = tr.rotation * Quaternion.Euler(turnAxis * turnSpeed * deltaTime);
				break;
				case RotationType.AtTarget: {
					var dir = (rotationTarget.position - tr.position).normalized;
					goalRotation = Quaternion.RotateTowards (tr.rotation, Quaternion.LookRotation (dir, rotationTarget.forward), turnSpeed * deltaTime);
				} break;
				case RotationType.FromLastPoint: {
					goalRotation = Quaternion.RotateTowards (tr.rotation, Quaternion.LookRotation (lastForward, Vector3.up), turnSpeed * deltaTime);
				} break;
			}
		}
		
		if (waitTimer > 0) 
		{
			waitTimer -= deltaTime;
			if (waitTimer <= 0)
			{
				onNodeLeft?.Invoke (path.getLastIndex);
			}
			return willTurn;
		}

		var willMove = currentSpeed != 0.0f;
		if (willMove)
		{
			goalPosition = Vector3.MoveTowards(tr.position, _point.position, currentSpeed * deltaTime);

			var distanceSquared = (tr.position - _point.position).sqrMagnitude;
			if (distanceSquared < maxDistanceToGoal * maxDistanceToGoal) 
			{
				if (path.atEnd && !atEnd) 
				{
					atEnd = true;
					StartCoroutine (DelayedFinishedCall ());
					if (disableIfFinished) 
						return true;
				}
				
				onNodeReached?.Invoke (path.getIndex);
				for (int i = 0; i < onTargetNodesReached.Length; i++)
				{
					if (onTargetNodesReached[i].node == _point)
					{
						onTargetNodesReached[i].onNodeReached?.Invoke ();
						break;
					}
				}

				if (waitDelay > 0)
					waitTimer = waitDelay;
				lastPoint = _point.position;
				lastForward = _point.forward;
				_currentPoint.MoveNext ();
			}
		}
		

		if (log)
		{
			Debug.LogFormat (this, "Moving! Position {0}, Rotation {1}", tr.position, tr.rotation);
		}

		return willTurn || willMove;
	}

	bool atEnd;
	WaitForEndOfFrame delayedFinishWait = new WaitForEndOfFrame ();
	IEnumerator DelayedFinishedCall () 
	{
		yield return delayedFinishWait;
		if (disableIfFinished) 
		{
			enabled = false;
			mover.enabled = false;
		}
		onFinished?.Invoke ();
	}
}
