using UnityEngine;
using UnityEngine.Jobs;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;

public struct MovingPlatformData 
{	
	public ModStub_PhysicsMover mover;
}
public struct MovingPlatformJobData 
{
	public bool optimize;
	public float dist;
	public float distScalar;
}
[BurstCompile(CompileSynchronously = true)]
struct MovingPlatformDistanceJob : IJobParallelForTransform
{
	public float3 camPos;
	public float distance;
	public NativeArray<MovingPlatformJobData> stateArray;

	void IJobParallelForTransform.Execute(int transformIndex, TransformAccess transform)
	{
		var dist = math.distance (camPos, transform.position);
		
		var result = stateArray[transformIndex];
		result.dist = dist;
		result.optimize = dist >= (distance * result.distScalar);
		stateArray[transformIndex] = result;
	}	
}

public class MovingPlatformManager : FlyweightTransformJobManager<BaseMovingPlatform, MovingPlatformData, MovingPlatformJobData, MovingPlatformManager>
{
	Camera cam;
	Transform camTr;
	bool isJobScheduled;
	JobHandle jobHandle;
	NativeArray<MovingPlatformJobData> tempJobArray;
	const float optimizeDistance = 20f;

	protected virtual void Start () 
	{
		CameraSpawnedBroadcast.OnCameraSpawned += SetCamera;
		SetCamera (Camera.main);
	}
	void OnDestroy () 
	{
		CameraSpawnedBroadcast.OnCameraSpawned -= SetCamera;
	}
	void SetCamera (Camera _cam) 
	{
		if (cam == _cam) return;

		cam = _cam;
		camTr = cam.transform;
	}


	void Update ()
	{
		if (!camTr)
		{
			if (isJobScheduled)
			{
				isJobScheduled = false;
				jobHandle.Complete ();
			}
			if (tempJobArray.IsCreated)
				tempJobArray.Dispose ();
			return;
		}

		var count = numJobData;
		tempJobArray = new NativeArray<MovingPlatformJobData> (count, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
		NativeArray<MovingPlatformJobData>.Copy (dataFields, 0, tempJobArray, 0, count);
		var job = new MovingPlatformDistanceJob ()
		{
			camPos = camTr.position,
			distance = optimizeDistance,
			stateArray = tempJobArray
		};
		jobHandle = job.Schedule (transforms);
		isJobScheduled = true;

		if (Time.timeScale == 0)
		{
			UpdateStates ();
		}
	}
	void FixedUpdate ()
	{	
		UpdateStates ();
	}
	void UpdateStates ()
	{
		var items = slots.RawItems;
		var length = slots.Count;
		var deltaTime = Time.deltaTime;
		var unscaledDeltaTime = Time.unscaledDeltaTime;
		for (int i = 0; i < length; i++)
		{
			ref var slot = ref items[i];
			if (!slot.active) continue;

			slot.flyweight.StateUpdate (deltaTime, unscaledDeltaTime);	
		}
	}
	void LateUpdate ()
	{
		if (!camTr || !isJobScheduled) return;

		jobHandle.Complete ();
		var items = slots.RawItems;
		var length = slots.Count;
		for (int i = 0; i < length; i++)
		{
			ref var slot = ref items[i];
			if (!slot.active || slot.jobIndex >= tempJobArray.Length) continue;

			var state = tempJobArray[slot.jobIndex].optimize;			
			ref var data = ref slot.data;
			
			slot.flyweight.Optimize = state;
			data.mover.enabled = !state;
		}
		tempJobArray.Dispose ();
		isJobScheduled = false;
	}
	public override void unregisterAll ()
	{
		if (isJobScheduled)
		{
			jobHandle.Complete ();
			isJobScheduled = false;
		}
		if (tempJobArray.IsCreated)
			tempJobArray.Dispose ();
		base.unregisterAll ();
	}
}