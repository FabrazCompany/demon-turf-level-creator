using UnityEngine;

[RequireComponent (typeof (ModStub_PhysicsMover))]
public class PivotPlatform : BaseMovingPlatform
{
    enum RotationType 
    {
        PlayOnce,
        PingPong,
        PingPongOnce
    }
    [SerializeField] RotationType rotationType;
    [SerializeField] float speed;
    [SerializeField] Vector3[] angles;
    [SerializeField] float startDelay;
    [SerializeField] float waitDelay;
	[SerializeField] float anticipationDelay;
    [SerializeField] bool disableIfFinished;
	[SerializeField] AudioClip anticipationClip;
    [SerializeField] AudioClip swivelClip;
	[SerializeField] AudioClip swivelEndClip;
    public Animator shakeAnim;
    float waitTimer;
    
	SlowMoEntity timeEntity;
    bool startHasRun;

    Quaternion toRotation;
    int rotationIndex;
    int rotationDirection;

	AudioSource anticipationSource;
	bool anticipationPlayed;

    protected override void Awake () 
    {
		base.Awake ();
		mover.Interpolate = false;
        timeEntity = GetComponent<SlowMoEntity> () ?? gameObject.AddComponent<SlowMoEntity> ();
    }
    void Start () 
    {
        startHasRun = true;
        cachedPosition = tr.position;
		cachedRotation = tr.rotation;
		Trigger ();
        
    }
    protected override void OnEnable () 
    {
		base.OnEnable ();
        if (!startHasRun)
            return;
        Trigger ();
    }

    void Trigger () 
    {
        if (angles.Length < 2)
        {
            Debug.LogError ("Not Enough Points for Pivot Platform!");
            return;
        }
        mover.InitialTickPosition = cachedPosition;
		mover.InitialTickRotation = cachedRotation;
        waitTimer = startDelay;
		waitReached = true;

        rotationDirection = 1;
        rotationIndex = 0;
        rotationIndex++;
        toRotation = Quaternion.Euler (angles[rotationIndex]);
    }

	bool waitReached;	
	protected override bool Movement (out Vector3 goalPosition, out Quaternion goalRotation, float deltaTime) 
	{
		deltaTime *= timeEntity.timeScale;
        goalPosition = tr.position;

        if (waitTimer > 0 || waitReached) 
        {
			goalRotation = tr.rotation;
			if (waitReached)
			{
				waitReached = false;
				if (waitDelay > 0) 
				{
					waitTimer = waitDelay;
					SoundManagerStub.Instance.PlaySoundGameplay3D(swivelEndClip, tr.position, data: new AudioData(_volume: .8f, _min: 10f, _max: 15f));
					anticipationPlayed = false;
				}
			}
            
            waitTimer -= deltaTime;
			if (!anticipationPlayed)
			{
				if (waitTimer <= anticipationDelay)
				{
					if (anticipationSource) 
						SoundManagerStub.Instance.ReleaseSource (anticipationSource);
					anticipationSource = SoundManagerStub.Instance.PlaySoundGameplay3D(anticipationClip, tr.position, data: new AudioData(_volume: .2f, _min: 10f, _max: 15f, _pitchRangeDir: 1, _isLooping: true));
					shakeAnim.SetBool ("shaking", true);
					anticipationPlayed = true;
				}
			}
			if (waitTimer <= 0)
			{
				if (anticipationSource)
				{
					SoundManagerStub.Instance.ReleaseSource (anticipationSource);
					anticipationSource = null;
				}
				SoundManagerStub.Instance.PlaySoundGameplay3D(swivelClip, tr.position, data: new AudioData(_volume: .8f, _min: 10f, _max: 15f));
				shakeAnim.SetBool ("shaking", false);
				anticipationPlayed = false;
			}
            return false;
        }

		var willMove = speed != 0.0f;

		if (willMove)
		{
			var _speed = speed * deltaTime;
			var rot = Quaternion.RotateTowards (tr.localRotation, toRotation, _speed);
			if (Quaternion.Angle (rot, toRotation) <= Mathf.Epsilon)
			{   
				if ((rotationDirection == 1 && rotationIndex >= angles.Length - 1)
					|| (rotationDirection == -1 && rotationIndex <= 0))
				{
					switch (rotationType)
					{
						case RotationType.PlayOnce:
							enabled = false;
							break;
						case RotationType.PingPong:
							rotationDirection *= -1;
							break;
						case RotationType.PingPongOnce:
							if (rotationDirection == -1)
								enabled = false;
							else 
								rotationDirection = -1;
							break;
						
					}
				}

				if (enabled)
				{
					waitReached = true;
					rotationIndex += rotationDirection;
					toRotation = Quaternion.Euler(angles[rotationIndex]);
				}
			}

			tr.localRotation = rot;
		}
        
        goalRotation = tr.rotation;
		return willMove;
	}
}