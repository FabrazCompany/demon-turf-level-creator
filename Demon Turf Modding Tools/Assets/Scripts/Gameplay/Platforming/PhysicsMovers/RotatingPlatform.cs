﻿using UnityEngine;

public class RotatingPlatform : BaseMovingPlatform, IMoverController
{
    [SerializeField] Vector3 RotationAxis = Vector3.up;
    [SerializeField] float RotSpeed = 10;
    Vector3 _originalPosition;
    Quaternion _originalRotation;
    SlowMoEntity timeEntity;
    float rotation;
	
    public void SetRotationSpeed  (float value) => RotSpeed = value;

    protected override void Awake ()
    {
		base.Awake ();
        var mover = GetComponent<ModStub_PhysicsMover> ();
        if (mover)
            mover.MoverController = this;
            
        _originalPosition = tr.position;
        _originalRotation = tr.rotation;
        timeEntity = GetComponent<SlowMoEntity> () ?? gameObject.AddComponent<SlowMoEntity> ();
    }
	protected override bool Movement(out Vector3 goalPosition, out Quaternion goalRotation, float deltaTime)
    {
        goalPosition = tr.position;

		bool willMove = (RotSpeed != 0.0f);

		if (willMove)
		{
			deltaTime *= timeEntity.timeScale;
			rotation += RotSpeed * deltaTime;
		}

		goalRotation = _originalRotation * Quaternion.Euler(RotationAxis * rotation);

		return willMove;
    }
}
