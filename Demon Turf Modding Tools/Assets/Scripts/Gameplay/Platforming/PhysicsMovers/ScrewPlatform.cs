﻿using UnityEngine;
#if !DT_MOD
using KinematicCharacterController;
#endif
public class ScrewPlatform : BaseMovingPlatform, IMoverController
{
    [Header ("References")]
    [SerializeField] Transform topPoint;
    [SerializeField] Transform bottomPoint;
    [SerializeField] Transform[] spinners;
    [SerializeField] Transform[] contraSpinners;
    [SerializeField] float gearSpin = 20;
    [SerializeField] float gearSpinBoosted = 80;
    [SerializeField] float gearSpinBoostDuration = 0.5f;
    float gearSpinTimer;

    [Header ("Spin")]
    [SerializeField] float spinBoost = .5f;
    [SerializeField] float decay = .2f;

    float spinValue;

    [Header ("Speeds")]
    [SerializeField] float ascentSpeed = 4;
    [SerializeField] float descentSpeed = -2;
    [SerializeField] float spinSpeed = 60;

    [Header ("Feedback")]
    [SerializeField] AudioClip movingSFX;
    [SerializeField] AudioClip topPointReachedSFX;
    [SerializeField] AudioClip bottomPointReachedSFX;
    [SerializeField] float ascendingPlayRate = 0.25f;
    [SerializeField] float ascendingPitchAdjust = 0.5f;
    [SerializeField] float descendingPlayRate = 0.15f;
    [SerializeField] float descendingPitchAdjust = 0.3f;
    float lastY;
    bool peakReached;
    bool baseReached;

    SlowMoEntity timeEntity;
    protected override void Awake () 
    {
		base.Awake ();
        timeEntity = GetComponent<SlowMoEntity> () ?? gameObject.AddComponent<SlowMoEntity> ();
    }
    public void AddSpin () 
    {
        if (spinValue < 0)
            spinValue = 0;
        spinValue = Mathf.Clamp (-1, 1, spinValue + spinBoost);
        gearSpinTimer = gearSpinBoostDuration;
        if (peakReached)
        {
            SoundManagerStub.Instance.PlaySoundGameplay3D (topPointReachedSFX, tr.position, data: new AudioData(_min :8, _max:20 ) );
        }
        if (baseReached)
            baseReached = false;
    }

    protected override bool Movement(out Vector3 goalPosition, out Quaternion goalRotation, float deltaTime)
    {
        deltaTime *= timeEntity.timeScale;
        gearSpinTimer -= deltaTime;
        
        var gearSpinSpeed = Mathf.Lerp (gearSpin, gearSpinBoosted, gearSpinTimer / gearSpinBoostDuration);
        foreach (var spin in spinners)
        {
            spin.Rotate (Vector3.up * gearSpinSpeed * deltaTime);
        }
        foreach (var spin in contraSpinners)
        {
            spin.Rotate (Vector3.up * -gearSpinSpeed * deltaTime);
        }

        spinValue = Mathf.Clamp (spinValue - (decay * deltaTime), -1, 1);
        var speed = spinValue > 0 ? ascentSpeed : descentSpeed;

        if (peakReached && speed < 0)
        {
            peakReached = false;
        }

		bool willMove = speed != 0.0f;
		if (willMove)
		{
			goalPosition = tr.position + Vector3.up * speed * spinValue * deltaTime;

			if (goalPosition.y >= topPoint.position.y)
			{
				goalPosition.y = topPoint.position.y;
				if (!peakReached)
				{
					peakReached = true;
					lastY = topPoint.position.y;
					SoundManagerStub.Instance.PlaySoundGameplay3D (topPointReachedSFX, tr.position, data: new AudioData(_min :8, _max:20));    
				}
			}
			else if (goalPosition.y <= bottomPoint.position.y)
			{
				
				goalPosition.y = bottomPoint.position.y;
				spinValue = 0;
				if (!baseReached)
				{
					baseReached = true;
					SoundManagerStub.Instance.PlaySoundGameplay3D (bottomPointReachedSFX, tr.position, data: new AudioData(_min :8, _max:20));
				}
			}

			if (spinValue > 0)
			{
				if (Mathf.Abs (lastY - goalPosition.y) >= ascendingPlayRate)
				{
					lastY = goalPosition.y;
					SoundManagerStub.Instance.PlaySoundGameplay3D (movingSFX, tr.position, new AudioData (_pitchRangeDir: 1, _pitchRange: ascendingPitchAdjust, _min :8, _max:20));
				}
			}
			else 
			{
				if (Mathf.Abs (lastY - goalPosition.y) >= descendingPlayRate)
				{
					lastY = goalPosition.y;
					SoundManagerStub.Instance.PlaySoundGameplay3D (movingSFX, tr.position, new AudioData (_pitchRangeDir: -1, _pitchRange: descendingPitchAdjust, _min :8, _max:20));
				}
			}
		}
		else 
		{
			goalPosition = tr.position;
		}
        
		bool willSpin = spinSpeed != 0.0f;
		if (willSpin)
		{
			goalRotation = tr.rotation * Quaternion.Euler (Vector3.up * spinSpeed * spinValue * deltaTime);
		}
		else 
		{
			goalRotation = tr.rotation;
		}
        
        return willMove || willSpin;
    }
}
