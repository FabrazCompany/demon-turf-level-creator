using UnityEngine;

[RequireComponent(typeof(ModStub_PhysicsMover))]
public class ShakingPlatform : BaseMovingPlatform, IMoverController
{
	[SerializeField] float shakeMag;
	[SerializeField] float shakeSpeed;
	Vector3 position;
	Vector3 target;
	public void AssignToMover ()
	{
		if (mover == null) return;
		mover.MoverController = this;
	}
	protected override void Awake ()
	{
		base.Awake ();
		position = tr.position;
		mover.MoverController = this;
	}
	protected override void OnEnable ()
	{
		base.OnEnable ();
		target = position + (Random.onUnitSphere * shakeMag);
		if (mover.MoverController != (IMoverController)this)
			mover.MoverController = this;
	}
	
	protected override bool Movement(out Vector3 goalPosition, out Quaternion goalRotation, float deltaTime)
	{
		goalRotation = tr.rotation;
		
		bool willShake = shakeSpeed != 0.0f;
		if (willShake)
		{
			goalPosition = Vector3.MoveTowards (tr.position, target, shakeSpeed * deltaTime);
			if (goalPosition.Equals (target))
				target = position + (Random.onUnitSphere * shakeMag);
		}
		else
		{
			goalPosition = tr.position;
		}
		return willShake;
	}
}