﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WeightSwitch : BaseMovingPlatform, IReceivePlayerWeight
{    
    [SerializeField] Transform plateVisual;
    [SerializeField, VectorLabels ("Min", "Max")] Vector2 plateShakeMag;
    [SerializeField] float targetWeight = 100;
    [SerializeField] float pressedDepth = -1.5f;
    [SerializeField] float sinkSpeed = 3;
    [SerializeField] float riseSpeed = 3;
    [SerializeField] UnityEvent onTriggered;
    [SerializeField] UnityEngine.UI.Image feedbackUI;
    float restingHeight;
    Rigidbody rigid;
    SlowMoEntity timeEntity;
    bool triggered;
    List<IWeighted> occupants;
	bool _processWeight;
	bool processWeight 
	{
		get => _processWeight;
		set => _processWeight = value;
	}
    protected override void Awake () 
    {
		base.Awake ();            
        restingHeight = tr.localPosition.y;
        rigid = GetComponent<Rigidbody> ();
        occupants = new List<IWeighted> ();
        timeEntity = GetComponent<SlowMoEntity> () ?? gameObject.AddComponent<SlowMoEntity> ();
    }


    void OnCollisionStay (Collision hit) 
    {
        if (triggered) return;

        var weight = hit?.rigidbody?.GetComponent<IWeighted> ();
        if (weight == null) return;

        if (occupants.Contains (weight)) return;

        occupants.Add (weight);
		processWeight = true;
    }

    void OnCollisionExit (Collision hit)
    {
        if (triggered) return;

        var weight = hit?.rigidbody?.GetComponent<IWeighted> ();
        if (weight == null) return;

        if (!occupants.Contains (weight)) return;

        occupants.Remove (weight);
		processWeight = true;
    }

	public void ResetPosition ()
	{
		if (mover)
			mover.SetPosition (cachedPosition);
		plateVisual.localPosition = Vector3.zero;
	}
    bool playerWeightApplied;
    float playerWeight;
    const float playerWeightScaler = -.025f;
    public void ApplyPlayerWeight (Vector3 origin, float massVal) 
    {
        if (triggered) return;
        playerWeight = massVal * playerWeightScaler;
        playerWeightApplied = true;

        var pct = (playerWeight + totalCurrentWeight) / targetWeight;
        var targetHeight = Mathf.Lerp (restingHeight, pressedDepth, pct);

        if (!Mathf.Approximately (tr.localPosition.y, targetHeight))
        {
			processWeight = true;
        }
    }

	float printTimer;

    [SerializeField, ReadOnly] float totalCurrentWeight;
	protected override bool Movement (out Vector3 goalPosition, out Quaternion goalRotation, float deltaTime) 
	{
        goalRotation = tr.rotation;
        goalPosition = tr.position;

		if (!processWeight) return false;
        
		if (triggered)
        {
			processWeight = false;
            return false;
        }
        deltaTime *= timeEntity.timeScale;

        totalCurrentWeight = 0;
        for (int i = 0; i < occupants.Count; i++)
        {
            totalCurrentWeight += occupants[i].getWeight;
        }

        if (playerWeightApplied)
        {
            playerWeightApplied = false;
            totalCurrentWeight += playerWeight;
        } 
        var pct = totalCurrentWeight / targetWeight;
        var targetHeight = Mathf.Lerp (restingHeight, pressedDepth, pct);
        var speed = targetHeight >= tr.localPosition.y ? riseSpeed : sinkSpeed;
        var progress = Vector3.MoveTowards (tr.localPosition, tr.localPosition.With (y: targetHeight), speed * deltaTime);

		printTimer -= Time.deltaTime;
		if (printTimer <= 0 && totalCurrentWeight >= 0)
		{
			printTimer = 2;
		}
        
        goalPosition = tr.parent ? tr.parent.TransformPoint (progress) : progress;
        var diff = goalPosition - tr.position;
        for (int i = 0; i < occupants.Count; i++)
        {
            occupants[i].getRigid.MovePosition (occupants[i].getRigid.position + diff);
        }

        var shakeMag = Random.insideUnitSphere.With (y: 0) * Mathf.Lerp (plateShakeMag.x, plateShakeMag.y, pct);
        plateVisual.localPosition = shakeMag;

        if (feedbackUI)
            feedbackUI.fillAmount = Mathf.InverseLerp (restingHeight, pressedDepth, tr.localPosition.y);

        if (pct >= 1 && Mathf.Approximately (tr.localPosition.y, targetHeight))
        {
            triggered = true;
            onTriggered?.Invoke ();
            plateVisual.localPosition = Vector3.zero;
            occupants.Clear ();
			processWeight = false;
        }
        
        if (Mathf.Approximately (tr.localPosition.y, occupants.Count > 0 ? targetHeight : restingHeight))
        {
            plateVisual.localPosition = Vector3.zero;
			processWeight = false;     
        }

		return true;
    }
}
