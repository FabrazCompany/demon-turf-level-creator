using UnityEngine;

public class PingPongRotation : MonoBehaviour
{
	[SerializeField] protected Orientation orientation;
    [SerializeField] float speed = 30;
	[SerializeField] float acceleration = 5;
	[SerializeField] bool useFixedUpdate;
	[SerializeField] SlowMoEntity slowMoEntity;
    float currentSpeed;
	[SerializeField, Range (-180, 180)] float turnRangeOffset;
    [SerializeField, Range (1, 360)] float turnRange;
    float startAngle;
    Vector3 startForward;
    Quaternion minAngle;
    Quaternion maxAngle;
	float timer;
	

    void Awake () 
    {
        startForward = transform.forward;
        startAngle = GetOrientationAngle ();
    }
	void OnEnable () 
	{
		timer = 0;
		currentSpeed = 0;
	}

    void Update () 
    {
		if (useFixedUpdate)
			return;
		RotateUpdate ();
    }
	void FixedUpdate ()
	{
		if (!useFixedUpdate)
			return;
		RotateUpdate ();
	}
	void RotateUpdate ()
	{
		var delta = slowMoEntity ? Time.deltaTime * slowMoEntity.timeScale : Time.deltaTime;
		currentSpeed = Mathf.MoveTowards (currentSpeed, speed, acceleration * delta);
		timer += delta * currentSpeed;
        var progress = Mathf.Sin (timer);
		var angles = transform.localEulerAngles;
        switch (orientation) 
        {
            default:
            case Orientation.x:
                angles.x = startAngle + turnRangeOffset + (progress * turnRange * 0.5f);
                break;
            
            case Orientation.y:
                angles.y = startAngle + turnRangeOffset + (progress * turnRange * 0.5f);
                break;

            case Orientation.z:
                angles.z = startAngle + turnRangeOffset + (progress * turnRange * 0.5f);
                break;
        }
		transform.localRotation = Quaternion.Euler (angles);
	}

	protected float GetOrientationAngle () 
    {
        switch (orientation)
        {
            default:
            case Orientation.x:
            return transform.localEulerAngles.x;

            case Orientation.y:
            return transform.localEulerAngles.y;
            
            case Orientation.z:
            return transform.localEulerAngles.z;
        }
    }
	protected Vector3 GetOrientationAxis ()
    {
        switch (orientation)
        {
            default:
            case Orientation.x:
            return transform.right;

            case Orientation.y:
            return transform.up;
            
            case Orientation.z:
            return transform.forward;
        }
    }
    void OnDrawGizmosSelected () 
    {
        if (!enabled)
			return;
		Gizmos.color = Color.green;
        var forward = Application.isPlaying ? startForward : transform.forward;
        Gizmos.DrawRay (transform.position, Quaternion.AngleAxis (turnRangeOffset + turnRange * 0.5f, GetOrientationAxis ()) * forward * 5);
        Gizmos.DrawRay (transform.position, Quaternion.AngleAxis (turnRangeOffset -turnRange * 0.5f, GetOrientationAxis ()) * forward * 5);
    }
}