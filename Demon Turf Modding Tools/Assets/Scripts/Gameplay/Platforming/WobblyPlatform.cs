using UnityEngine;

public class WobblyPlatform : MonoBehaviour
{
	[SerializeField] ModStub_PhysicsMover mover;
    [SerializeField] SlowMoEntity timeEntity;
	[SerializeField] float velocityScalar = 12f;	//no clue why, but the velocity values were off unless scaled, 12 seemed like the magic number

	Vector3 velocity;
	Vector3 lastPosition;
	
	public Vector3 getVelocity => velocity * velocityScalar * timeEntity.timeScale;
	void OnEnable () 
	{
		lastPosition = mover.transform.position;
	}
	void FixedUpdate ()
	{
		velocity = mover.transform.position - lastPosition;
		lastPosition = mover.transform.position;
	}
}