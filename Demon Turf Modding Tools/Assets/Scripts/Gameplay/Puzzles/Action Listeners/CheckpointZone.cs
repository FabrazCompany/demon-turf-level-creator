using UnityEngine;
using UnityEngine.Events;

public class CheckpointZone : MonoBehaviour {
    [SerializeField] float triggerDelay;
	[SerializeField] bool triggerOnCheckpointTeleport;
    [SerializeField] UnityEvent onCheckpointPlaced;
	[SerializeField] bool revealCheckpointUI;

	public static event System.Action onEnteredCheckpointZone;
    void OnTriggerEnter (Collider hit) 
    {
		if (!hit.TryGetComponent<IPlayerController> (out var player))
			return;
        
        player.onCheckpointCountUpdated -= CheckpointsUpdated;
        player.onCheckpointCountUpdated += CheckpointsUpdated;

		if (revealCheckpointUI)
			onEnteredCheckpointZone?.Invoke ();
    }
    void OnTriggerExit (Collider hit)
    {
		if (!hit.TryGetComponent<IPlayerController> (out var player))
			return;
        
        player.onCheckpointCountUpdated -= CheckpointsUpdated;
    }

    void CheckpointsUpdated (int num) 
    {
        if (onCheckpointPlaced == null)
            return;
        if (triggerDelay > 0)
            StartCoroutine (this.WaitAndDo (triggerDelay, onCheckpointPlaced.Invoke)); 
        else 
            onCheckpointPlaced?.Invoke ();       
    }
}