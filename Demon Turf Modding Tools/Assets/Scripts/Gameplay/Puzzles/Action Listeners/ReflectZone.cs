using UnityEngine;
using UnityEngine.Events;

public class ReflectZone : MonoBehaviour {
    [SerializeField] float triggerDelay;
    [SerializeField] UnityEvent onReflect;
    void OnTriggerEnter (Collider hit) 
    {
        var rigid = hit.GetComponentInParent<Rigidbody> ();
        if (!rigid)
            return;
        
        var reflector = rigid.GetComponentInChildren<ReflectProjectiles> (true);
        if (!reflector)
            return;

        reflector.getOnProjectileReflected.RemoveListener (OnReflect);
        reflector.getOnProjectileReflected.AddListener (OnReflect);
    }
    void OnTriggerExit (Collider hit)
    {
        var rigid = hit.GetComponentInParent<Rigidbody> ();
        if (!rigid)
            return;
        
        var reflector = rigid.GetComponentInChildren<ReflectProjectiles> (true);
        if (!reflector)
            return;
            
        reflector.getOnProjectileReflected.RemoveListener (OnReflect);
    }
    
    void OnReflect (ReflectProjectiles reflect) 
    {
        if (onReflect == null)
            return;

        if (triggerDelay > 0)
            StartCoroutine (this.WaitAndDo (triggerDelay, onReflect.Invoke)); 
        else 
            onReflect.Invoke ();
    }
    
}