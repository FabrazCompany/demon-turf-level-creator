using UnityEngine;
using UnityEngine.Events;
using System.Collections;
public class RepositionZone : MonoBehaviour 
{
    [SerializeField] UnityEvent onReposition;
	[SerializeField] float unsubscribeDelay = 1;
	IPlayerController player;
    void OnTriggerEnter (Collider hit) 
    {
		if (!hit.TryGetComponent<IPlayerController> (out var _player))
			return;
        
		player = _player;
		player.onPlayerDeathReset -= ResetTriggered;
		player.onPlayerRepositioned -= RepositionTriggered;
		player.onPlayerDeathReset += ResetTriggered;
		player.onPlayerRepositioned += RepositionTriggered;
    }
    void OnTriggerExit (Collider hit)
    {
		if (!hit.TryGetComponent<IPlayerController> (out var _player))
			return;
        
		StartCoroutine (this.WaitAndDo (unsubscribeDelay, () => 
		{
			_player.onPlayerDeathReset -= ResetTriggered;
			_player.onPlayerRepositioned -= RepositionTriggered;
			player = null;
		}));
    }
	void OnDisable ()
	{
		if (player != null)
		{
			player.onPlayerDeathReset -= ResetTriggered;
			player.onPlayerRepositioned -= RepositionTriggered;
		}
	}

	void ResetTriggered () => RepositionTriggered (Vector3.zero);
    void RepositionTriggered (Vector3 vector) 
    {
        if (onReposition == null) return;
		StartCoroutine (ResetTriggerRoutine ());
    }
	IEnumerator ResetTriggerRoutine ()
	{
		yield return new WaitWhile (() => player != null && player.characterState != CharacterState.Dead);
#if !DT_EXPORT && !DT_MOD
		yield return new WaitUntil (() => TransitionManager.Instance.transitionDone);
#endif
		onReposition.Invoke ();
	}
}