using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class ResetZone : MonoBehaviour {
    [SerializeField] float triggerDelay;
    [SerializeField] UnityEvent onReset;
	IPlayerController player;
    void OnTriggerEnter (Collider hit) 
    {
		if (!hit.TryGetComponent<IPlayerController> (out var _player))
			return;
        
		player = _player;
        player.onPlayerDeathReset -= ResetTriggered;
        player.onPlayerDeathReset += ResetTriggered;
    }
    void OnTriggerExit (Collider hit)
    {

		if (!hit.TryGetComponent<IPlayerController> (out var _player))
			return;

        _player.onPlayerDeathReset -= ResetTriggered;
    }
    void ResetTriggered () 
    {
        if (onReset == null) return;
        if (triggerDelay > 0)
			StartCoroutine (ResetTriggerRoutine ());			
        else 
            onReset?.Invoke ();       
    }
	IEnumerator ResetTriggerRoutine ()
	{
		yield return new WaitForSeconds (triggerDelay);
		yield return new WaitWhile (() => player != null && player.characterState != CharacterState.Dead);
		onReset.Invoke ();
	}
}