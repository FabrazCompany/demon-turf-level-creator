﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Breakable : MonoBehaviour
{  

    public enum BreakableType
    {
        BombBlock,
    }


    [SerializeField] BreakableType type;
    [SerializeField] ParticleSystem breakParticle;
	[SerializeField] AudioSource breakParticleSFX;
    [SerializeField] float breakParticleTimingBuffer = 0.25f;
    [SerializeField] bool reform;
	[SerializeField] UnityEvent onBreak;
    [SerializeField, Conditional ("reform")] float reformDelay = 5;
    [SerializeField, Conditional ("reform")] ParticleSystem reformParticle;
    [SerializeField, Conditional ("reform")] float reformParticleTimingBuffer = 0.25f;
    [SerializeField, Conditional ("reform")] LayerMask reformBlockingLayers;

    BoxCollider box;
    Renderer rend;
    Collider[] hits;

    void Awake () 
    {
        rend = GetComponent<Renderer> ();
        box = GetComponent<BoxCollider> ();
        hits = new Collider[1];
    }
    
    Coroutine breakRoutine;
    IEnumerator BreakRoutine ()
    {
        breakParticle.Play ();
		breakParticleSFX.Play();
        yield return new WaitForSeconds (breakParticleTimingBuffer);
		onBreak?.Invoke ();
        box.enabled = false;
        rend.enabled = false;
        yield return new WaitWhile (() => breakParticle.isPlaying);
        if (!reform) 
            yield break;
        
        yield return new WaitForSeconds (reformDelay);
        yield return new WaitWhile (() => box.Overlap (hits, reformBlockingLayers) > 0);
        reformParticle.Play ();
        box.enabled = true;
        yield return new WaitForSeconds (reformParticleTimingBuffer);
        rend.enabled = true;
    }

    public void TryTrigger (BreakableType[] _types) 
    {
        foreach (var _type in _types)
        {
            if (_type == type)
            {
                this.RestartCoroutine (BreakRoutine (), ref breakRoutine);           
                break;
            }
        }
    }
}
