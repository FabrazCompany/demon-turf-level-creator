﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breaker : MonoBehaviour
{
    [SerializeField] Breakable.BreakableType[] types;

    void OnCollisionEnter (Collision collision)
    {
        var breakable = collision.collider.GetComponent<Breakable> ();
        breakable?.TryTrigger (types);
    }
    void OnTriggerEnter (Collider hit)
    {
        var breakable = hit.GetComponent<Breakable> ();
        breakable?.TryTrigger (types);
    }
}
