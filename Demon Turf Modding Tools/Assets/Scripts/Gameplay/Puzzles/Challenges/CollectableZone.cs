using System.Collections.Generic;
using UnityEngine;

public class CollectableZone : PlayerChallengeZone<PlayerCollectable> 
{
    public LayerMask collectableLayer;
    [SerializeField] CollectableType[] targetCollectableTypes;
    [SerializeField, ReadOnly] List<PlayerCollectable> residents = new List<PlayerCollectable> ();
    
    protected override LayerMask residentLayer => collectableLayer;
    protected override bool externallyManaged => false;
    // protected override string challengeLabel => "Collect Zone";
    protected override ConditionalChallengeGUI.ChallengeType challengeType => ConditionalChallengeGUI.ChallengeType.KeysChallenge;
    public override int challengeCount => residents.Count;
    protected override QueryTriggerInteraction queryType => QueryTriggerInteraction.Collide;
    protected override bool failOnExit => false;

	protected override void Awake ()
	{
		base.Awake ();
		collectableLayer = LayerMask.GetMask ("TriggerPlayer");
	}
    protected override void AddResident (PlayerCollectable resident) 
    {
        if (!resident)
            return;
        
        if (residents.Contains (resident))
            return;

        bool matchFound = false;
        foreach (var type in targetCollectableTypes)
        {
            if (type == resident.Type)
            {
                matchFound = true;
                break;
            }
        }
        if (!matchFound)
            return;
        
        resident.onPickup.AddListener (OnChallengeProgressed);
        residents.Add (resident);
    }
    protected override void RefreshResidents () 
    {
        foreach (var resident in residents)
        {
            resident.gameObject.SetActive (true);
        }
    }
    protected override void ClearResidents () 
    {
        foreach (var resident in residents)
        {
            resident.onPickup.RemoveListener (OnChallengeProgressed);
        }
        residents.Clear ();
    }
    protected override int TallyProgress () 
    {
        if (targetCollectableTypes == null || targetCollectableTypes.Length == 0)
            return 0;

        int progress = 0;
        foreach (var resident in residents)
        {
            if (!resident.gameObject.activeInHierarchy)
            {
                foreach (var type in targetCollectableTypes)
                {
                    if (type == resident.Type)
                    {
                        progress++;
                        continue;
                    }
                }
            }
        }
        return progress;
    } 

    protected override void OnPlayerReset () 
    {
        base.OnPlayerReset ();
        if (cleared)
            return;
        RefreshResidents ();
    }
}