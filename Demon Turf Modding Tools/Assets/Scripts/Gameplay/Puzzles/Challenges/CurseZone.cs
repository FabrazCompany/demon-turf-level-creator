﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CurseZone : MonoBehaviour
{
    [SerializeField] CurseChallengeType primaryType;
    [SerializeField] CurseChallengeType secondaryType;
    [SerializeField] bool repeatable;
	[SerializeField] bool cancelOnExit = true;
    [SerializeField] UnityEvent onTrigger;
    [SerializeField] UnityEvent onCompleted;
    IPlayerController player;
    bool completed;
    bool active;
    public void TryTriggerCurse (Collider hit) 
    {
        if (completed) return;

        if (primaryType == CurseChallengeType.TimeBubble || secondaryType == CurseChallengeType.TimeBubble)
        {
            var shot = hit?.attachedRigidbody?.GetComponent<SlowMoShot> ();
            if (shot)
                shot.Trigger ();

            var bubble = hit?.attachedRigidbody?.GetComponent<SlowMoBubble> ();
            if (bubble)
                bubble.Despawn ();
        }

		if (!hit.TryGetComponent<IPlayerController> (out var _player))
			return;
        Trigger (_player); 
    }
    public void TryEndCurse (Collider hit) 
    {
        if (!active) return;
        var _player = hit.GetComponent<IPlayerController> ();
        if (_player == null || player == null) return;   
        Complete ();
    }
    public void TryCancelCurse (Collider hit) 
    {
        if (!active) return;
        var _player = hit.GetComponent<IPlayerController> ();
        if (_player == null || player == null) return;   
        Cancel ();
    }
    void Trigger (IPlayerController _player) 
    {
        player = _player;
        player.GetChallengeGUI.Trigger (primaryType, secondaryType);
        player.SetCurse (primaryType, secondaryType);
        player.onKilled += Cancel;
        active = true;
        onTrigger?.Invoke ();
    }
    void Complete () 
    {
        if (player != null)
        {
            player.GetChallengeGUI.Completed ();
            player.ClearCurse ();
            player.onKilled -= Cancel;
            player = null;
            active = false;
            if (!repeatable)
                completed = true;

            onCompleted?.Invoke ();
        }
        
    }
    void Cancel () 
    {
        if (player != null)
        {
			if (cancelOnExit)
	            player.GetChallengeGUI.Fail ();
			else 
				player.GetChallengeGUI.Cancelled ();
            player.ClearCurse ();
            player.onKilled -= Cancel;
            player = null;
            active = false;
        }
    }
}
