﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ObstacleCourse : MonoBehaviour
{
    [SerializeField, ReadOnly] ObstacleCourseRing activeRing;
    [SerializeField, ReadOnly] ObstacleCourseRing followupRing;
    [SerializeField] ObstacleCourseRing[] rings;

    [Header ("Materials")]
    [SerializeField] TextureColorCombo visualStyle_starter;
    [SerializeField] TextureColorCombo visualStyle_standard;
    [SerializeField] TextureColorCombo visualStyle_finisher;
    [SerializeField] Material ringMatStarter;
    [SerializeField] Material ringMatStandard;
    [SerializeField] Material ringMatFinisher;

    [Header ("Events")]
    [SerializeField] UnityEvent onTriggered;
    [SerializeField] UnityEvent onTimedOut;
    [SerializeField] UnityEvent onCompleted;

    IPlayerController player;

    void Awake () 
    {
        PlayerControllerEvents.onSpawned += AssignPlayer;
    }
    void Start () 
    {
        player = gameObject.scene.FindPlayerInterface ();
        if (rings == null || rings.Length < 2)
        {
            Debug.LogWarning ("Needs at least 2 obstacle rings!");
            return;
        }

        for (int i = 0; i < rings.Length; i++) 
        {
            var ring = rings[i];
            var isStarter = i == 0;
            var isFinisher = i >= rings.Length - 1;

            if (isStarter)
            {
                ring.SetVisuals (visualStyle_starter);
                ring.SetActive (true);
                ring.SetVisible (true);
                ring.onTriggered += Trigger;
                ring.enabled = false;   //stop timer
            }                
            else if (isFinisher)
            {
                ring.SetVisuals (visualStyle_finisher);
                ring.onTriggered += Complete;
            }
            else 
            {
                ring.SetVisuals (visualStyle_standard);
                var index = i;
                ring.onTriggered += () => SetRing (index + 1);
            }
            ring.onTimeout += Cancel;
            ring.gameObject.SetActive (isStarter);
        }
    }
    void OnDestroy () 
    {
        for (int i = 0; i < rings.Length; i++) 
        {
            var ring = rings[i];
            if (ring != null)
            {
                ring.onTimeout -= Cancel;
            }
        }
        PlayerControllerEvents.onSpawned -= AssignPlayer;
    }
    void AssignPlayer (IPlayerController _player) 
    {
        if (player != null) return;
        player = _player;
    }

    [ContextMenu ("Trigger")]
    public void Trigger () 
    {
        if (player != null)
        {
            player.GetChallengeGUI.Trigger (ConditionalChallengeGUI.ChallengeType.RingChallenge, string.Empty, 0, rings.Length, 0, false);
            player.onKilled += Cancel;
        }
        SetRing (1);
        onTriggered?.Invoke ();
    }
    void SetRing (int activeIndex)
    {
        player?.GetChallengeGUI.UpdateProgress (activeIndex);
        activeRing = rings[activeIndex];
        if (!activeRing.gameObject.activeInHierarchy)
            activeRing.gameObject.SetActive (true);
        activeRing.SetVisible (true);
        activeRing.SetActive (true);
        
        if (activeIndex < rings.Length - 1)
        {
            followupRing = rings?[activeIndex + 1];
            if (!followupRing.gameObject.activeInHierarchy)
                followupRing.gameObject.SetActive (true);
            followupRing.SetVisible (true);
            followupRing.SetActive (false);
        }
    }
    void Complete () 
    {
        if (player != null)
        {
            player.GetChallengeGUI.Completed ();
            player.onKilled -= Cancel;
        }
        
        onCompleted?.Invoke ();
    }
    void Cancel () 
    {
        activeRing = null;
        followupRing = null;
        for (int i = 0; i < rings.Length; i++) 
        {
            var ring = rings[i];
            var isStarter = i == 0;
            var isFinisher = i <= rings.Length - 1;
            
            ring.SetVisible (isStarter);
            ring.SetActive (isStarter);
        }
        rings[0].enabled = false;
        
        if (player != null)
        {
            player.GetChallengeGUI.Fail ();
            player.onKilled -= Cancel;
        }
        onTimedOut?.Invoke ();
    }

    [ContextMenu ("Rename Rings")]
    public void RenameRings () 
    {
        for (int i = 0; i < rings.Length; i++) 
        {
            var ring = rings[i];
            var isStarter = i == 0;
            var isFinisher = i >= rings.Length - 1;
            if (isStarter)
                ring.name = "Ring - Starter";   
            else if (isFinisher)
                ring.name = "Ring - Finisher";
            else 
                ring.name = string.Format ("Ring - {0}", i);
        }
    }

    [ContextMenu ("Style Rings")]
    public void StyleRings () 
    {
        for (int i = 0; i < rings.Length; i++) 
        {
            var ring = rings[i];
            var isStarter = i == 0;
            var isFinisher = i >= rings.Length - 1;
            if (isStarter)
            {
                ring.SetVisuals (visualStyle_starter);
            }                
            else if (isFinisher)
            {
                ring.SetVisuals (visualStyle_finisher);
            }
            else 
            {
                ring.SetVisuals (visualStyle_standard);
            }
        }
    }

    [System.Serializable]
    public struct TextureColorCombo 
    {
        public Material ringMat;
        public Material triggerMat;
    }
}
