﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ObstacleCourseRing : MonoBehaviour
{
    [SerializeField] float timeOutDuration = 5;
    [SerializeField] float timeOutScale = 0.5f;
    [SerializeField] Animator anim;
    [SerializeField] Renderer ringRend;
    Material ringMat;

    [SerializeField] Collider trigger;
    [SerializeField] Renderer triggerRend;
    Material triggerMat;
    float timer;
    Vector3 startScale;
    
    public event Action onTriggered;
    public event Action onTimeout;


    void Awake () 
    {   
        ringMat = ringRend.material;
        triggerMat = triggerRend.material;
        startScale = transform.localScale;
    }
    void OnDestroy () 
    {
        Destroy (ringMat);
        Destroy (triggerMat);
    }
    void Update () 
    {
        timer -= Time.deltaTime;
        transform.localScale = Vector3.Lerp (startScale, startScale * timeOutScale, 1 - (timer / timeOutDuration));
        if (timer <= 0)
        {
            enabled = false;
            trigger.enabled = false;
            onTimeout?.Invoke ();
        }
            
    }

    public void SetVisuals (ObstacleCourse.TextureColorCombo style) 
    {
        ringRend.material = style.ringMat;
        ringMat = ringRend.material;
        triggerRend.material = style.triggerMat;
        triggerMat = triggerRend.material;
    }
    public void SetVisible (bool visible)
    {
        anim.SetBool ("Visible", visible);
        
        if (visible)
        {
            anim.SetBool ("Acquired", false);
            transform.localScale = startScale;
        }
    }

    public void SetActive (bool active)
    {
        anim.SetBool ("Active", active);
        enabled = active;
        trigger.enabled = active;
        if (active)
            timer = timeOutDuration + Time.deltaTime;
    }

    public void Trigger () 
    {
        anim.SetBool ("Acquired", true);
        enabled = false;
        onTriggered?.Invoke ();
    }
}
