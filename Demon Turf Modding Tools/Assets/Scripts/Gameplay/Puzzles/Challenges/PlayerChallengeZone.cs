using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public abstract class PlayerChallengeZone<T> : MonoBehaviour where T : MonoBehaviour
{
    public UnityEvent onZoneCleared;

    protected IPlayerController player;
    protected bool cleared;
    float entryTime;
    float challengeTimer;
    int progressCount;

    protected abstract LayerMask residentLayer { get; }
    protected abstract bool externallyManaged { get; }
    protected abstract ConditionalChallengeGUI.ChallengeType challengeType { get; }
    public abstract int challengeCount { get; }
    protected abstract QueryTriggerInteraction queryType { get; }
    protected virtual bool failOnExit => true;
	protected virtual string getLayer => "TriggerPlayer";

    protected virtual void Awake () 
    {
		gameObject.layer = LayerMask.NameToLayer (getLayer);
        var stateDependent = GetComponentInParent<LevelStateDependent> ();
        if (stateDependent)
        {
            stateDependent.OnTrigger.AddListener (RefreshZone);
        } 
#if !DT_EXPORT && !DT_MOD
		LevelManager.onTurfStateUpdated += RefreshZone;
		PlayerControllerEvents.onReset += OnPlayerReset;
#endif
    }
	void Start () 
    {
#if !DT_EXPORT && !DT_MOD
        var levelManager = FindObjectOfType<LevelManager> ();
        if (!levelManager)
            Init ();
#endif
    }
	void OnDisable ()
	{
		if (player != null)
        {
            player.onPlayerRepositioned -= OnPlayerRepositioned;
        }
	}
	protected virtual void OnDestroy ()
	{
#if !DT_EXPORT && !DT_MOD
        PlayerControllerEvents.onReset -= OnPlayerReset;
        LevelManager.onTurfStateUpdated -= RefreshZone;
#endif
	}
    public virtual void OnTriggerEnter (Collider hit) 
    {
        var _player = hit.GetComponent<IPlayerController> ();
        if (_player == null || player != null) return;
        
        TriggerZone (_player);
    }
    public virtual void OnTriggerExit (Collider hit) 
    {
		if (!hit.TryGetComponent<IPlayerController> (out var _player))
			return;
        ExitZone ();
    }

    public virtual void Init ()
    {
        AddResidents ();
        cleared = false;
    }
    protected abstract void AddResident (T resident);
    protected virtual void AddResidents () 
    {
        var box = GetComponent<BoxCollider> ();
        Collider[] hits = null;
        if (box)
            hits = Physics.OverlapBox (box.bounds.center, Vector3.Scale (box.size, transform.lossyScale) / 2, transform.rotation, residentLayer, queryType);
        else 
        {
            var sphere = GetComponent<SphereCollider> ();
            if (sphere)
                hits = Physics.OverlapSphere (sphere.bounds.center, sphere.radius * transform.lossyScale.magnitude / 2, residentLayer, queryType);
        }

        if (hits == null || hits.Length == 0)
            return;
             
        for (int i = 0; i < hits.Length; i++) 
        {
            var resident = hits[i].GetComponentInParent<T> ();
            AddResident (resident);
        }   
    }
    protected abstract void RefreshResidents ();
    protected abstract void ClearResidents ();

    public virtual void TriggerZone (IPlayerController _player)
    {
        player = _player;
        player.RegisterCombatZone (true);
        player.onPlayerRepositioned += OnPlayerRepositioned;
        if (!externallyManaged)
        {
            player.GetChallengeGUI.Trigger (challengeType, string.Empty, progressCount, challengeCount, challengeTimer, false);
            entryTime = Time.time - challengeTimer;
        }
    }

    protected virtual void ExitZone () 
    {
        if (player != null)
        {
            player.RegisterCombatZone (false);
            if (!externallyManaged)
            {
                if (failOnExit)
                    player.GetChallengeGUI.Fail ();
                else 
                    player.GetChallengeGUI.Cancelled ();
            }
            player.onPlayerRepositioned -= OnPlayerRepositioned;
            player = null;
        }
        challengeTimer = Time.time - entryTime;
    }
    protected virtual void CleanupZone ()
    {
        if (player != null)
        {
            if (!externallyManaged)
            {
                player.GetChallengeGUI.Fail ();
            }
            player.RegisterCombatZone (false);
            player.onPlayerRepositioned -= OnPlayerRepositioned;
            player = null;
        }

        progressCount = 0;
        challengeTimer = 0;
    }

    protected abstract int TallyProgress ();

    protected virtual void OnChallengeProgressed () 
    {
        progressCount = TallyProgress ();
        player?.GetChallengeGUI.UpdateProgress (progressCount);
        if (progressCount < challengeCount) return;
        
        if (!externallyManaged)
        {
            player?.GetChallengeGUI.Completed ();
        }

        if (player != null)
        {
            player.RegisterCombatZone (false);
            player.onPlayerRepositioned -= OnPlayerRepositioned;
            player = null;
        }
        

        var collider = GetComponent<Collider> ();
        if (collider)
            collider.enabled = false;
        cleared = true;
        onZoneCleared?.Invoke ();
    }
    
    protected virtual void OnPlayerReset () 
    {
        if (cleared) return;
		if (!gameObject.activeInHierarchy) return;
        CleanupZone ();
    }
    protected virtual void OnPlayerRepositioned (Vector3 pos)
    {
        if (cleared) return;
    }

    void RefreshZone (LevelID level, LevelState state) => RefreshZone ();
#if !DT_EXPORT && !DT_MOD
    void RefreshZone (AstarPath path) => RefreshZone ();
#endif
    void RefreshZone ()
    {
        if (!gameObject.activeInHierarchy) return;
        StartCoroutine (RefreshZoneRoutine());
    }
    IEnumerator RefreshZoneRoutine () 
    {
        yield return null;
        RefreshResidents ();
        ClearResidents ();
        AddResidents ();
    }
}