using UnityEngine;
using UnityEngine.Events;

public class RaceZone : MonoBehaviour
{
    [SerializeField] float duration = 15;
    [SerializeField] bool isDSGVariant;
    [SerializeField, Conditional ("isDSGVariant")] DemonGolfBall ballPrefab;
    DemonGolfRespawnPoint ballStart;
    DemonGolfBall ball;
    bool cleared;


    [SerializeField] UnityEvent onTrigger;
    [SerializeField] UnityEvent onCompleted;
	[SerializeField] UnityEvent onFailed;
    float timer;
    IPlayerController player;

    void Awake () 
    {
        if (isDSGVariant)
        {
            ballStart = GetComponentInChildren<DemonGolfRespawnPoint> (true);
            if (!ballStart)
            {
                Debug.LogError ("No Ball Start Point Found!", this);
                return;
            }

            ball = Instantiate (ballPrefab);
			ballStart.Activate (ball);
            ball.Spawn (); 
			ball.FindPlayer ();

            ballStart.onBallReleased += OnBallReleased;            
        }
    }
    void OnDestroy () 
    {
        if (isDSGVariant)
        {
            if (ballStart)
                ballStart.onBallReleased -= OnBallReleased;
        }
        if (player != null)
        {
            player.onKilled -= CancelRace;
            player.onPlayerRepositioned -= OnPlayerRepositioned;
        }
    }

    void OnEnable () 
    {
        timer = duration;
    }
    void Update () 
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            CancelRace ();
        }
    }

    public void TryTriggerRace (Collider hit) 
    {
        if (isDSGVariant) return;
        if (cleared) return;
		if (!hit.TryGetComponent<IPlayerController> (out var _player)) return;
		TriggerRace (_player, ConditionalChallengeGUI.ChallengeType.RacingChallenge);
    }

    void TriggerRace (IPlayerController _player, ConditionalChallengeGUI.ChallengeType _type) 
    {
        player = _player;
        player.GetChallengeGUI.Trigger (_type, duration, true);
        player.onKilled += CancelRace;
        player.onPlayerRepositioned += OnPlayerRepositioned;
		player.EnterNoCheckpointZone ();
        timer = duration;
        enabled = true;
    }
    void OnBallReleased () 
    {
		if (cleared || enabled || !ball) return;
        TriggerRace (ball.getPlayer, ConditionalChallengeGUI.ChallengeType.RacingChallengeSoccer);
    }

    public void TryCancelRace (Collider hit)
    {
        if (!enabled) return;
        
        if (isDSGVariant && hit.GetComponent<DemonGolfBall> ())
        {
            CancelRace ();
        }
    }
    

    public void TryEndRace (Collider hit)
    {
        if (!enabled) return;
        
        if (isDSGVariant)
        {
            var _ball = hit.GetComponent<DemonGolfBall> ();
            if (_ball)
                EndRace ();   
        }
        else 
        {
            // var _player = hit.GetComponent<PlayerController> ();
            // if (_player)
			if (hit.TryGetComponent<IPlayerController> (out var _player))
                EndRace ();
        }
    }
    void EndRace () 
    {
        if (player == null) return;   

        player.GetChallengeGUI.Completed ();
        player.onKilled -= CancelRace;
        player.onPlayerRepositioned -= OnPlayerRepositioned;
		player.ExitNoCheckpointZone ();
        player = null;
        cleared = true;
        onCompleted?.Invoke ();
        enabled = false;
    }
    void OnPlayerRepositioned (Vector3 pos) => CancelRace ();
    void CancelRace () 
    {
        if (player != null)
        {
            player.GetChallengeGUI.Fail ();
            player.onKilled -= CancelRace;
            player.onPlayerRepositioned -= OnPlayerRepositioned;
			player.ExitNoCheckpointZone ();
            player = null;
        }
        if (ball)
        {
            ball.Respawn ();
        }
		onFailed?.Invoke ();
        enabled = false;
    }
}