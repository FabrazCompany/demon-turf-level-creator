﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InteractablePrompt : MonoBehaviour
{
    [SerializeField] string animationName;
    [SerializeField] UnityEvent onInteractionTriggered;

	IPlayerController player;
    void OnTriggerEnter (Collider other) 
	{
		if (!other.TryGetComponent<IPlayerController> (out var _player))
			return;
		player = _player;
        player.AddInteraction (gameObject, transform.GetInstanceID (), animationName, onInteractionTriggered.Invoke);
    }
    void OnTriggerExit (Collider other) 
	{
    	if (!other.TryGetComponent<IPlayerController> (out var _player))
			return;
        player.RemoveInteraction (transform.GetInstanceID ());
		player = null;
    }
	void OnDisable ()
	{
		if (player != null)
		{
			player.RemoveInteraction (transform.GetInstanceID ());
			player = null;
		}
	}
}
