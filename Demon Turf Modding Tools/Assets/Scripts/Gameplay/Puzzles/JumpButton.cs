﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpButton : MonoBehaviour
{
    [SerializeField] SpriteRenderer faceVisual;
    [SerializeField] Sprite faceOn;
    [SerializeField] Sprite faceOff;
    [Space]
    [SerializeField] MeshRenderer buttonVisual;
    [SerializeField] Material buttonOn;
    [SerializeField] Material buttonOff;
    [SerializeField] AudioClip giggleClipSFX;
    [SerializeField] AudioClip sadClipSFX;

    public event System.Action onStateChanged;
    public bool isTriggered { get; private set; }

    bool ignoreChange;

    void Awake () 
    {
        SetState (isTriggered, false);
    }
    public void LockState (bool state) => ignoreChange = state;
    public void TriggerStateChange () 
    {
        SetState (!isTriggered);
        onStateChanged?.Invoke ();
    }

    public void SetState (bool active) => SetState (active, true);
	public void SetState (bool active, bool playSound)
	{
        if (ignoreChange)
            return;
        isTriggered = active;
        faceVisual.sprite = active ? faceOn : faceOff;
        buttonVisual.material = active ? buttonOn : buttonOff;

		if (playSound)
		{
			if (faceVisual.sprite == faceOn)
			{
				SoundManagerStub.Instance.PlaySoundGameplay3D(giggleClipSFX, transform.position, data: new AudioData(_volume: .9f, _min: 5f, _max: 25f, _pitchRange: .15f));
			}
			else if (faceVisual.sprite == faceOff)
			{
				SoundManagerStub.Instance.PlaySoundGameplay3D(sadClipSFX, transform.position, data: new AudioData(_volume: .85f, _min: 5f, _max: 25f, _pitchRange: .15f));
			}
		}
    }
}
