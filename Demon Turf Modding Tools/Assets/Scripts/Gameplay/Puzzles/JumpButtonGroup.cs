﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class JumpButtonGroup : MonoBehaviour
{
    JumpButton[] buttons;
    [SerializeField] UnityEvent onTriggered;
    [SerializeField] UnityEvent onCancelled;
    bool active;
    void Awake () 
    {
        buttons = GetComponentsInChildren<JumpButton> ();
        foreach (var face in buttons)
        {
            face.onStateChanged += ButtonsStateChanged;
        }
    }

    void OnDestroy () 
    {
        foreach (var face in buttons)
        {
            face.onStateChanged -= ButtonsStateChanged;
        }
    }

    void ButtonsStateChanged () 
    {
        foreach (var face in buttons)
        {
            if (!face.isTriggered)
            {
                if (active)
                {
                    active = false;
                    onCancelled?.Invoke ();
                }
                return;
            }
        }
        if (!active)
        {
            active = true;
            LockState (true);
            onTriggered?.Invoke ();
        }
    }

    public void LockState (bool state)
    {
        foreach (var face in buttons)
        {
            face.LockState (state);
        }
    }
    public void ResetState () 
    {
        foreach (var face in buttons)
        {
            face.SetState (false, false);
        }
    }
}
