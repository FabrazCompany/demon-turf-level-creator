﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ToggleSwitchBroadcaster : MonoBehaviour
{
    [SerializeField] bool active;
    [SerializeField] ToggleSwitchListener[] activeToggles;
    [SerializeField] ToggleSwitchListener[] inactiveToggles;
	[SerializeField] AudioSource switchSound;
	// [SerializeField] AudioData switchSoundData;
	bool insideTrigger = false;

	// public delegate void WhenSwitchNear(bool insideTrigger);
    // public static event WhenSwitchNear NearSwitch;


    public void OnValidate () 
    {
        SetListenerStates ();
    }
    public void ToggleState () 
    {
        active = !active;
        SetListenerStates ();
		if (insideTrigger && switchSound && switchSound.isActiveAndEnabled)
			switchSound.Play ();
			// SoundManagerStub.Instance.PlaySoundGameplay3D (switchSound, transform.position, data: switchSoundData);

		// if(NearSwitch != null)
        //     NearSwitch(insideTrigger);
    }
	public void TurnInsideTriggerOn () 
    {
        insideTrigger = true;
    }
	public void TurnInsideTriggerOff () 
    {
        insideTrigger = false;
    }

    void SetListenerStates () 
    {
        for (int i = 0; i < activeToggles?.Length; i++) 
        {
            activeToggles[i].onToggled?.Invoke (active);
        }

        for (int i = 0; i < inactiveToggles?.Length; i++) 
        {
            inactiveToggles[i].onToggled?.Invoke (!active);
        }
    }
}
