﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ToggleSwitchListener : MonoBehaviour
{
    public UnityEventBool onToggled;
}
