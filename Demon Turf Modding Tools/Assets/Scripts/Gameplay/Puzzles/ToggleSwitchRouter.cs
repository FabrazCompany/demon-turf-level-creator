﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleSwitchRouter : MonoBehaviour
{
    [SerializeField] ToggleSwitchBroadcaster[] toggleSwitches;

    public void Toggle () 
    {
        for (int i = 0; i < toggleSwitches.Length; i++)
        {
            toggleSwitches[i].ToggleState ();
        }
    }
}
