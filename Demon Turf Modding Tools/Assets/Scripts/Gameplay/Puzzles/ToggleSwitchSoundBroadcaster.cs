﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ToggleSwitchSoundBroadcaster : MonoBehaviour
{

	void OnEnable()
    {
        // ToggleSwitchBroadcaster.NearSwitch += PlaySwitchSound;
    }

    void OnDisable()
    {
        // ToggleSwitchBroadcaster.NearSwitch -= PlaySwitchSound;
    }
	[SerializeField] AudioClip switchSound;

    public void PlaySwitchSound (bool insideTrigger) 
    {
		if (insideTrigger == true)
			{
				SoundManagerStub.Instance.PlaySoundGameplay2D (switchSound, transform.position, data: new AudioData(_volume:.65f));
			} 
    }

}
