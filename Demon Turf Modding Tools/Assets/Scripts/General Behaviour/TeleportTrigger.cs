﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
 
 
[RequireComponent (typeof (Collider))]
public class TeleportTrigger : MonoBehaviour
{
    public enum TriggerType {Enter, Exit};

    [Tooltip ("The Transform to teleport to")]
    [SerializeField] Transform teleportTo;

    [Tooltip ("The filter Tag")]
    [SerializeField, FormerlySerializedAs ("tag"), TagSelector] string targetTag = "Player";

    [Tooltip ("Trigger Event to Teleport")]
    [SerializeField] TriggerType type;

    [SerializeField] float transitionDuration = 1f;

	public Transform getDestination => teleportTo;
	void OnDisable ()
	{
		if (teleporting)
			Cleanup ();
	}
    void OnTriggerEnter (Collider other)
    {
        if (type != TriggerType.Enter) return;
        if (targetTag != string.Empty && !(bool)other.CompareTag(targetTag)) return;

        this.RestartCoroutine (Reposition (other), ref respotionRoutine);
    }

    void OnTriggerExit (Collider other)
    {
        if (type != TriggerType.Exit) return;
        if (targetTag != string.Empty && !(bool)other.CompareTag(targetTag)) return;
        if (!other || !other.attachedRigidbody) return;
        this.RestartCoroutine (Reposition (other), ref respotionRoutine);
    }

	bool teleporting;
	IPlayerController player;
    Coroutine respotionRoutine;
    IEnumerator Reposition (Collider other) 
	{
#if !DT_EXPORT && !DT_MOD
		if (TransitionManager.Instance.isActiveAndEnabled)
			yield break;
			
		teleporting = true;
		player = other.attachedRigidbody.GetComponent<IPlayerController> ();
		if (player != null)
		{
			player.SetPlayerInputLock (100);
		}
        if (transitionDuration > 0) 
		{
            Game.timeScale = 0;
            TransitionManager.Instance.StartTransition (transitionDuration * 0.5f);
            yield return new WaitUntil (()=>TransitionManager.Instance.transitionDone);
            
        } 
		
        
        if (player != null) {
            player.RepositionPlayer (teleportTo.position, teleportTo.rotation, true, 0.25f);
        } else {
            other.attachedRigidbody.transform.position = teleportTo.position;
        }
		yield return null;

        if (transitionDuration > 0) 
		{
			Game.timeScale = 0.01f;
            TransitionManager.Instance.EndTransition (transitionDuration * 0.5f);
            yield return new WaitUntil (()=>TransitionManager.Instance.transitionDone);
        } 
#else
		yield return null;
#endif
		Cleanup ();
    }

	void Cleanup ()
	{
		teleporting = false;
		Game.timeScale = Game.defaultTime;
		if (player != null)
		{
			player.ClearPlayerInputLock ();
			player = null;
		}
	}
    
    [SerializeField] bool drawSelected;
	#if UNITY_EDITOR
    void OnDrawGizmos () 
    {
        if (drawSelected) return;

        DrawGizmos ();
    }
    void OnDrawGizmosSelected () 
    {
        if (!drawSelected) return;
        
        DrawGizmos ();
    }
    void DrawGizmos () 
    {
        if (!teleportTo) return;
    
        Gizmos.color = Color.blue;
        Gizmos.DrawLine (transform.position, teleportTo.position);
    }
    #endif
}