using System;
using System.Collections;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;

public abstract class BaseHubManager <TStartEnum, TEventEnum, TData, TDataList> 
#if !DT_EXPORT && !DT_MOD
    : PlayerManager
#else
	: ModStub_PlayerManager
#endif
	, IPlayerCheckpoint 
    where TStartEnum : System.Enum
    where TEventEnum : System.Enum
    where TData : HubStartPointData
    where TDataList : SerializableDictionaryBase<TStartEnum, TData>
{
    [SerializeField] TData defaultStartPoint;
    [SerializeField] TDataList startPoints;
    [SerializeField] float transitionDelay = 0.25f;
    [SerializeField] bool overrideStartPoint;
    [SerializeField, Conditional ("overrideStartPoint")] TStartEnum overriddenStartPoint;
    Transform _startPoint;

    protected abstract TurfID turfID { get; }
    protected abstract TStartEnum stateChangeStartPoint { get; }
    protected override bool spawnPlayerImmediately => false;
    protected override Transform startPoint => _startPoint;
    protected string destinationKey => LevelUtility.GetHubDestinationKey (turfID);
    

    Transform IPlayerCheckpoint.startPoint => _startPoint;
    
    public void TriggerTransition (TStartEnum destination, float delay) => this.RestartCoroutine (TransitionRoutine (destination, delay), ref transitionRoutine);
    public void TriggerTransition (TStartEnum destination, float delay, LevelState state) => this.RestartCoroutine (TransitionRoutine (destination, delay, state), ref transitionRoutine);
    public void SetHubStateTransition (int state) => SetHubStateTransition ((LevelState)state);
    public void SetHubStateTransition (LevelState state) => TriggerTransition (stateChangeStartPoint, 0, state);
    
    
#region STATIC
    static void SetSavedDestination (string destinationKey, TStartEnum startPoint) 
	{	
		// Debug.Log ($"Assigning Start Point. Destination Key: {destinationKey}, Start Point: {startPoint}");
#if !DT_EXPORT && !DT_MOD 
		GameDataManager.Instance.Data.SetDestination (destinationKey, (int)(object)startPoint);
#endif
	}
	protected static void SetSavedDestination (TurfID turf, TStartEnum startPoint) => SetSavedDestination (LevelUtility.GetHubDestinationKey (turf), startPoint);
	
    public static event Action<LevelState> onTurfStateUpdated;
    protected static void TriggerTurfStateUpdatedEvent (LevelState state) => onTurfStateUpdated?.Invoke (state);
#endregion


#region INITIALIZATION
	protected virtual bool IsLoadFinished (TData start) => true;
    protected override void Start () 
    {
        base.Start ();
		enabled = false;
		StartCoroutine (Initialization ());
    }

    protected virtual IEnumerator Initialization () 
    {
        // setup position marker
        _startPoint = new GameObject ("Start Point").transform;
        _startPoint.SetParent (transform);

        // determine start point and place player        
		var startPoint = LoadSavedDestination ();
#if UNITY_EDITOR
		if (overrideStartPoint)
			startPoint = overriddenStartPoint;
#endif
        yield return ConfigureStartPoint (GetStartPoint (startPoint));
        SpawnInPlayer ();
        ConfigureCollectables ();
#if !DT_EXPORT && !DT_MOD 
		if (!MenuManager.Instance.anyMenuTimeFreezeActive)
        	Game.timeScale = Game.defaultTime;
#endif
		enabled = true;
    }
    protected override void SpawnInPlayer ()
    {
        base.SpawnInPlayer ();
        character.EnterCheckpointZone (this, false);
    }
    
    TStartEnum LoadSavedDestination ()
    {
#if !DT_EXPORT && !DT_MOD 
        var startPoint = (TStartEnum)(object)GameDataManager.Instance.Data.GetDestination (destinationKey, 0);
        return startPoint;
#else
		return (TStartEnum)(object)0;
#endif
    }
    
    TData GetStartPoint (TStartEnum startPoint)
    {
        return startPoints.TryGetValue (startPoint, out TData startPointData) 
            ? startPointData 
            : defaultStartPoint;
    }
    protected virtual IEnumerator ConfigureStartPoint (TData startPointData) 
    {
        if (startPointData == null) yield break;

        _startPoint.position = startPointData.startPosition.position;
        _startPoint.rotation = startPointData.startPosition.rotation;
    }
    void ConfigureCollectables () 
    {
#if !DT_EXPORT && !DT_MOD 
        var collectables = SceneUtility.FindAllSceneObjectsIncludingDisabled<PlayerCollectable>();
        
        data = GameDataManager.Instance.Data.GetLevelData(LevelUtility.GetHubLevelID (turfID));
        if (!data.collectablesInitd)
            data.InitCollectibles (collectables);
        
        collectableCounter = GetComponent<CollectableCounter>() ?? gameObject.AddComponent<CollectableCounter>();
        collectableCounter.SetLevelProgression(data.collectablesGathered, collectables);

        var collector = character.GetComponent<CollectableCollector> ();
        if (collector)
            collectableCounter.SetCollectorListener(collector);
        
        collector.onPickedUpCake.AddListener (() => OnCollectableAcquired (CollectableType.Cake));
        collector.onPickedUpCandy.AddListener (() => OnCollectableAcquired (CollectableType.Candy));
#endif
    }

    void OnCollectableAcquired (CollectableType type)
    {
#if !DT_EXPORT && !DT_MOD 
        switch (type)
        {
            case CollectableType.Cake:
            case CollectableType.Candy:
                GameDataManager.Instance.Data.IncreaseCollectable (type, 1);
                break;
        }
#endif
    }
#endregion

#region TRANSITION 
    Coroutine transitionRoutine;
    IEnumerator TransitionRoutine (TStartEnum destination, float delay)
    {
        enabled = false;
        yield return new WaitForSecondsRealtime (delay);
#if !DT_EXPORT && !DT_MOD 
        TransitionManager.Instance.StartTransition (transitionDelay);
        Game.timeScale = 0;
        yield return new WaitForSecondsRealtime (transitionDelay);

		var destinationPoint = GetStartPoint (destination);
		yield return ConfigureStartPoint (destinationPoint);

        SetPlayer (startPoint.position.With (y: startPoint.position.y + 0.25f), startPoint.rotation);
        character.SnapToGround ();
        Game.timeScale = Game.defaultTime;
        TransitionManager.Instance.EndTransition (transitionDelay);
#endif
        enabled = true;
    }
    IEnumerator TransitionRoutine (TStartEnum destination, float delay, LevelState state)
    {
#if !DT_EXPORT && !DT_MOD 
        GameDataManager.Instance.Data.SetTurfState (turfID, state);
#endif
        yield return TransitionRoutine (destination, delay);
        onTurfStateUpdated?.Invoke(state);
        character.CleanInteractions ();
    }
#endregion
}

[System.Serializable]
public class HubStartPointData
{
    public Transform startPosition;
}