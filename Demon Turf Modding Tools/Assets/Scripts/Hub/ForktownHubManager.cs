using System.Collections;
using UnityEngine;
using UnityEngine.AddressableAssets;
using RotaryHeart.Lib.SerializableDictionary;


public class ForktownHubManager : BaseHubManager
    <
        ForktownHubManager.StartPoint, 
        ForktownHubManager.HubEvent, 
        ForktownStartPointData, 
        ForktownStartPointDataList
    >, 
	ISceneLoadDelayer
{
    public enum StartPoint {
        Default = 0,

		// BeebzRoom_Access = 2,
		TownSq_Access = 3,
		Underbelly_Access = 4,
		Outskirts_Access = 5,
		// CastleAccess_Access = 6,

        BeebzRoomExt = 10,
        BeebzRoomInt = 11,
        
        Turf1 = 20,
        Turf2 = 21,
        Turf3 = 22,
        Turf4 = 23,
        Turf5 = 24,
        Castle = 25,

        ArcadeExt = 40,
        ArcadeInt = 41,
		ArcadeAccess1 = 42,
		ArcadeAccess2 = 43,
		ArcadeAccess3 = 44,
		ArcadeAccess4 = 45,
		ArcadeAccess5 = 46,
		ArcadeAccess6 = 47,
		ArcadeAccess7 = 48,
		ArcadeAccess8 = 49,
        
        VanityShopExt = 50,
        VanityShopInt = 51,

        ModShopExt = 54,
        ModShopInt = 55,
        
		ArenaAccess = 60,
		DemonSoccerGolfAccess = 62,
		MiniChallengeAccess = 64,
		BatteryThiefAccess = 66,

        PhotoGalleryExt = 70,
        PhotoGalleryInt = 71,

        ChallengesExt = 80,
        ChallengesInt = 81,

        BourgeoisieAbode1Ext = 150,
        BourgeoisieAbode1Int = 151,

        BatteryThiefExt = 170,
        BatteryThiefInt = 171,

        PetShopExt = 180,
        PetShopInt = 181,

		LuciRoomExt = 190,
		LuciRoomInt = 191,
    }
    public enum HubEvent
    {
        TutorialCompleted = 0,
        TutorialCompleted_MidgiCutscene = 5,
        TutorialCompleted_LuciCutscene = 10,
		TutorialSpecialCake_Collected = 15,

        Turf1Completed = 100,
		Turf1Completed_ForktownCutscene = 105,
        //Turf1Completed_MidgiCutscene = 110,
        //Turf1Completed_LuciCutscene = 115,
        Turf1Completed_DemonSoccerGolfCutscene = 120,
	
        Turf2Completed = 200,
        Turf2Completed_ForktownCutscene = 205,
        //Turf2Completed_MidgiCutscene = 210,
        //Turf2Completed_LuciCutscene = 215,

        Turf3Completed = 300,
        Turf3Completed_ForktownCutscene = 305,
        //Turf3Completed_MidgiCutscene = 310,
        //Turf3Completed_LuciCutscene = 315,
        
        Turf4Completed = 400,
        Turf4Completed_ForktownCutscene = 405,
        //Turf4Completed_MidgiCutscene = 410,
        //Turf4Completed_LuciCutscene = 415,

		MiniChallengesIntro = 491,
        MiniChallengesCompleted = 500,
		ArenaIntro = 501,
        ArenaCompleted = 510,
		DemonSoccerGolfIntro = 511,
        DemonSoccerGolfCompleted = 520,
		PetShopIntro = 525,
		PetShopCompleted = 529,
        PhotoHuntIntro = 530,
        PhotoHuntCompleted = 539,
        ModShopIntro = 540,
        ModShopCompleted = 549,
        VanityShopIntro = 550,
        VanityShopCompleted = 559,
        BatteryThiefIntro = 560,
        BatteryThiefCompleted = 569,
        
        ArcadeIntro = 570,
		Arcade1Unlocked = 571,
		Arcade2Unlocked = 572,
		Arcade3Unlocked = 573,
		Arcade4Unlocked = 574,
		Arcade5Unlocked = 575,
		Arcade6Unlocked = 576,
		Arcade7Unlocked = 577,
		Arcade8Unlocked = 578,
        ArcadeCompleted = 579,
		ArcadeAllLevelsCompleted = 580,

		ArcadeCutscenesDone = 600,
		BatteryThiefCutscenesDone = 601,
		MiniChallengesCutscenesDone = 602,
		ArenaCutscenesDone = 603,
		DemonSoccerGolfCutscenesDone = 604,
		PhotoHuntCutscenesDone = 605,
		ModShopCutscenesDone = 606,
		VanityShopCutscenesDone = 607,
		GuardIntroCutsceneDone = 608,
		GuardCompletionCutsceneDone = 609,
		PetShopCutscenesDone = 610,
        
        DemonKingCompleted = 1000,
        DemonKingCompleted_CreditsCutscene = 1005,
		DemonKingCompleted_PostGameCutscene = 1006,
        DemonKingCompleted_VaultCutscene = 1010,
		DemonKingLair_LuciCutscene = 1015,
		DemonKingLair_MidgiCutscene = 1020,
		DemonKingLair_BossGateCutscene = 1025,
		DemonKingLair_VaultCutscene = 1030,
		DemonKingLair_CrownCutscene = 1035,

		EnoughBatteriesCollectedForDK = 1101,
		EnoughTrophiesCollected = 1102,
		EverythingCollected = 1111,
		CrownCollected = 1112,

    }

    GameObject currentRoom;
	bool loaded;
    
    protected override TurfID turfID => TurfID.Forktown;
    protected override StartPoint stateChangeStartPoint => StartPoint.Default;
    public static StartPoint GetStartFromLevelID (LevelID levelID)
    {
        switch (levelID)
        {
            default:
                return StartPoint.Default;
            case LevelID.Tutorial:
                return StartPoint.BeebzRoomInt;
            case LevelID.Forktown_Turf1:
                return StartPoint.Turf1;
            case LevelID.Forktown_Turf2:
                return StartPoint.Turf2;
			case LevelID.Forktown_Turf3:
                return StartPoint.Turf3;
			case LevelID.Forktown_Turf4:
                return StartPoint.Turf4;
        }
    }
	static void SetSavedDestination (StartPoint startPoint) => SetSavedDestination (TurfID.Forktown, startPoint);
    public static void SetStartPoint (TurfID _turfID, LevelID _level, LevelID _target)
    {
		switch (_target)
		{
			case LevelID.ForktownAccess_BeebzRoom:
			case LevelID.ForktownAccess_CastleAccess:
			case LevelID.ForktownAccess_Outskirts:
			case LevelID.ForktownAccess_TownSq:
			case LevelID.ForktownAccess_Underbelly:
			case LevelID.ForktownAccess_Mods:
			case LevelID.ForktownAccess_Dyes:
			case LevelID.ForktownAccess_Pets:
			case LevelID.ForktownAccess_DSG:
			case LevelID.ForktownAccess_Arena:
			case LevelID.ForktownAccess_Trials:
			case LevelID.ForktownAccess_Arcade:
			case LevelID.ForktownAccess_PhotoGallery:
				_turfID = TurfID.Forktown;
				_level = LevelID.Forktown;
				break;
		}
#if !DT_MOD
		var gameData = GameDataManager.Instance.Data;
        switch (_turfID)
        {
            default: 
				if (gameData.getQueuedDemonSoccerGolf != DemonGolfType.INVALID && LevelUtility.IsGolfLevel (_level))
					SetSavedDestination (StartPoint.DemonSoccerGolfAccess);
				else if (gameData.getQueuedArena != ArenaType.INVALID && LevelUtility.IsArenaLevel (_level))
					SetSavedDestination (StartPoint.ArenaAccess);	
				else if (gameData.getQueuedMiniChallenge != MiniChallengeType.INVALID && LevelUtility.IsMiniChallengeLevel (_level))
					SetSavedDestination (StartPoint.MiniChallengeAccess);
				else
				{
					switch (_target)
					{
						default:
							var start = StartPoint.Default;
							if (_level == LevelID.Tutorial)
							{
								start = StartPoint.BeebzRoomInt;
							}
							var data = GameDataManager.Instance.Data;
							if (data.GetTurfEvent (ForktownHubManager.HubEvent.DemonKingCompleted) && !data.GetTurfEvent (ForktownHubManager.HubEvent.DemonKingCompleted_PostGameCutscene))
							{
								start = StartPoint.BeebzRoomInt;
							}
							SetSavedDestination (start);
							break;
						case LevelID.ForktownAccess_BeebzRoom:
							SetSavedDestination (StartPoint.BeebzRoomInt);
							break;
						case LevelID.ForktownAccess_TownSq:
							SetSavedDestination (StartPoint.TownSq_Access);
							break;
						case LevelID.ForktownAccess_Underbelly:
							SetSavedDestination (StartPoint.Underbelly_Access);
							break;
						case LevelID.ForktownAccess_CastleAccess:
							SetSavedDestination (StartPoint.Castle);
							break;
						case LevelID.ForktownAccess_Outskirts:
							SetSavedDestination (StartPoint.Outskirts_Access);
							break;

						case LevelID.ForktownAccess_Mods:
							SetSavedDestination (StartPoint.ModShopInt);
							break;
						case LevelID.ForktownAccess_Dyes:
							SetSavedDestination (StartPoint.VanityShopInt);
							break;
						case LevelID.ForktownAccess_Pets:
							SetSavedDestination (StartPoint.PetShopInt);
							break;
						case LevelID.ForktownAccess_DSG:
							SetSavedDestination (StartPoint.DemonSoccerGolfAccess);
							break;
						case LevelID.ForktownAccess_Arena:
							SetSavedDestination (StartPoint.ArenaAccess);
							break;
						case LevelID.ForktownAccess_Trials:
							SetSavedDestination (StartPoint.MiniChallengeAccess);
							break;
						case LevelID.ForktownAccess_Arcade:
							SetSavedDestination (StartPoint.ArcadeInt);
							break;
						case LevelID.ForktownAccess_PhotoGallery:
							SetSavedDestination (StartPoint.PhotoGalleryInt);
							break;
					}
				} 
				break;
            case TurfID.Apocadesert:
                SetSavedDestination (StartPoint.Turf1);
                break;
			case TurfID.Armadageddon:
                SetSavedDestination (StartPoint.Turf2);
                break;
			case TurfID.NewNeoCity:
                SetSavedDestination (StartPoint.Turf3);
                break;
			case TurfID.PeakPlateau:
                SetSavedDestination (StartPoint.Turf4);
                break;
			case TurfID.DemonKingLair:
				SetSavedDestination (StartPoint.Castle);
				break;
			case TurfID.Arcades:	
				switch (_level)
				{
					case LevelID.Arcades_1:
						SetSavedDestination (StartPoint.ArcadeAccess1);
						break;
					case LevelID.Arcades_2:
						SetSavedDestination (StartPoint.ArcadeAccess2);
						break;
					case LevelID.Arcades_3:
						SetSavedDestination (StartPoint.ArcadeAccess3);
						break;
					case LevelID.Arcades_4:
						SetSavedDestination (StartPoint.ArcadeAccess4);
						break;
					case LevelID.Arcades_5:
						SetSavedDestination (StartPoint.ArcadeAccess5);
						break;
					case LevelID.Arcades_6:
						SetSavedDestination (StartPoint.ArcadeAccess6);
						break;
					case LevelID.Arcades_7:
						SetSavedDestination (StartPoint.ArcadeAccess7);
						break;
					case LevelID.Arcades_8:
						SetSavedDestination (StartPoint.ArcadeAccess8);
						break;
				}
				break;
        }
#endif
    }

	bool ISceneLoadDelayer.isReady => loaded;
	protected override IEnumerator Initialization() 
	{
        yield return base.Initialization ();
		ForktownTransition.onTransitionTriggered += TriggerTransition;
		SoundManagerStub.Instance.PlayBackgroundMusicHub((int)ForktownSection.Forktown);
		loaded = true;
#if !DT_MOD
		GameDataManager.Instance.Data.onTurfEventAdjusted += OnTurfEventTriggered;
#endif
	}
	protected override void OnDestroy()
	{
		base.OnDestroy ();
		ForktownTransition.onTransitionTriggered -= TriggerTransition;
#if !DT_MOD
		if (GameDataManager.DataIsLoaded)
		{
			GameDataManager.Instance.Data.onTurfEventAdjusted -= OnTurfEventTriggered;
		}
#endif
	}

    protected override IEnumerator ConfigureStartPoint (ForktownStartPointData startPointData)
    {
        yield return base.ConfigureStartPoint (startPointData);
		if (currentRoom)
		{
			Addressables.ReleaseInstance (currentRoom);
		}
			
		yield return Resources.UnloadUnusedAssets ();

		if (startPointData.roomAddress.RuntimeKeyIsValid ())
		{
			loaded = false;
			var pos = startPointData.roomSpawnOverride ? startPointData.roomSpawnOverride.position : startPoint.position;
			var rot = startPointData.roomSpawnOverride ? startPointData.roomSpawnOverride.rotation : startPoint.rotation;
			startPointData.roomAddress.GetAsset ((GameObject room) => 
			{
				startPointData.roomAddress.InstantiateAsync (pos, rot).Completed += (operation) => 
				{
					currentRoom = operation.Result;
					loaded = true;
				};
			});
			yield return new WaitUntil (() => loaded);
		}
		else 
			loaded = true;
    }

	void OnTurfEventTriggered (System.Enum eventID, bool state)
	{
		if (!state) return;
		if (EnumUtility.IsValue (eventID, HubEvent.Arcade1Unlocked)
		 	|| EnumUtility.IsValue (eventID, HubEvent.Arcade2Unlocked)
			|| EnumUtility.IsValue (eventID, HubEvent.Arcade3Unlocked)
			|| EnumUtility.IsValue (eventID, HubEvent.Arcade4Unlocked)
			|| EnumUtility.IsValue (eventID, HubEvent.Arcade5Unlocked)
			|| EnumUtility.IsValue (eventID, HubEvent.Arcade6Unlocked)
			|| EnumUtility.IsValue (eventID, HubEvent.Arcade7Unlocked)
			|| EnumUtility.IsValue (eventID, HubEvent.Arcade8Unlocked))
		{
#if !DT_MOD
			PlatformManager.Instance.SetAchievementStat (AchievementCategories.Quests_ArcadesCompleted, GameDataManager.Instance.Data.GetCollectablesOwnedCurrent (CollectableType.Cartridge, true));
#endif
		}
	}
}

[System.Serializable]
public class ForktownStartPointData : HubStartPointData 
{
	public AssetReferenceGameObject roomAddress;
	public Transform roomSpawnOverride;
}

[System.Serializable]
public class ForktownStartPointDataList : SerializableDictionaryBase<ForktownHubManager.StartPoint, ForktownStartPointData> {}