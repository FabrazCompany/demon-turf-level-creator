﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class ForktownTransition : MonoBehaviour
{
    [SerializeField] ForktownHubManager.StartPoint destination;
    [SerializeField] float triggerDelay;
    public static event Action<ForktownHubManager.StartPoint, float> onTransitionTriggered;
	public static void TriggerTransition (ForktownHubManager.StartPoint destination, float delay) => onTransitionTriggered?.Invoke (destination, delay);
    void OnTriggerEnter (Collider hit)
    {
		if (!hit.TryGetComponent<IPlayerController> (out var player))
			return;
        onTransitionTriggered?.Invoke (destination, triggerDelay);
    }
}
