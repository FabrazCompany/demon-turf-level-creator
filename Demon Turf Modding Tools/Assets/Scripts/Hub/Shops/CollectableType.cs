    public enum CollectableType 
    {
        Candy,
        Cake,
        Key,
        Cartridge,
        Battery,
		Crown,
    }