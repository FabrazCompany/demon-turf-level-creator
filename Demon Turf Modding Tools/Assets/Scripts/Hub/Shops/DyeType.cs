public enum DyeType 
{
	Default,
	Black,
	Red,
	Teal,
	DarkGreen,
	Purple,
	White,
	Yellow,
	Orange,
	Brown,
	Grey,
	LightGreen,
	Pink,
}