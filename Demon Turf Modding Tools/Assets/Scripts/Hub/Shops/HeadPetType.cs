public enum HeadPetType
{
	None,
	Gecko,
	Crab,
	Mouse,
	Rabbit,
	Crow,
	Belini,
	Pig,
	Teddy,
	Rock,
	SlimeSan,
	Lin,
	Skippy,
}