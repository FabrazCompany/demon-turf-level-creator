public enum ModCategoryType 
{
    General = 0,
    Movement = 1,
    Spin = 2,
    Punch = 3,
    Turf = 4,
    Other = 5,
}