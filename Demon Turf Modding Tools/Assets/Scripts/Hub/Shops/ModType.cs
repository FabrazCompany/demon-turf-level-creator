public enum ModType
{
	//General Mods = 0-49
	WingdedDemon = 5,
	LightAsAFeather = 10,
	NoTimeToWaste = 15,

	//Other?
	HideAndSeek = 20,
	MadeYouLook = 25,
	FlagHoarder = 30,
	ProtectiveWisp = 35,

	//Punch Mods 50-99
	StoicJustice = 50,
	Ringer = 52,
	RevUp = 55,
	// DemonSlap = 60,

	//Spin Mods 100-149
	Updraft = 100,
	SafetyBubble = 105,
	Whirlwind = 110,
	BackOff = 115,

	//Turf Mods 150-199
	Sidewinder = 150,
	SerpentineHandling = 155,
	NinjaBird = 160,
	Tantrum = 165,
}

[System.Flags]
public enum ModTypeBitShifted
{
	WingdedDemon = 1 << 0,
	LightAsAFeather = 1 << 1,
	NoTimeToWaste = 1 << 2,
	HideAndSeek = 1 << 3,
	MadeYouLook = 1 << 4,
	FlagHoarder = 1 << 5,
	ProtectiveWisp = 1 << 6,
	StoicJustice = 1 << 7,
	Ringer = 1 << 8,
	RevUp = 1 << 9,
	Updraft = 1 << 10,
	SafetyBubble = 1 << 11,
	Whirlwind = 1 << 12,
	BackOff = 1 << 13,
	Sidewinder = 1 << 14,
	SerpentineHandling = 1 << 15,
	NinjaBird = 1 << 16,
	Tantrum = 1 << 17,
}