public enum ArenaType
{
	INVALID,
	Desert1 = 100,
	Desert2 =  120,
	Desert3 =  130,
	Desert4 =  140,
	Desert5 =  150,

	Ocean1 = 200,
	Ocean2 = 220,
	Ocean3 = 230,
	Ocean4 = 240,
	Ocean5 = 250,

	City1 = 300,
	City2 = 320,
	City3 = 330,
	City4 = 340,
	City5 = 350,

	Snow1 = 400,
	Snow2 = 420,
	Snow3 = 430,
	Snow4 = 440,
	Snow5 = 450,

	International1 = 500,
	International2 = 520,
	International3 = 530,
	International4 = 540,
	International5 = 550,
}