using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class DKVault : MonoBehaviour
{
	[SerializeField] TextMeshProUGUI progressLabelBattery;
	[SerializeField] TextMeshProUGUI progressLabelCake;
	[SerializeField] TextMeshProUGUI progressLabelCandy;
	[SerializeField] TextMeshProUGUI progressLabelTrophy;
	[SerializeField] TextMeshProUGUI progressLabelDK;
	
	[Space]
	[SerializeField] TextMeshProUGUI progressLabelMiniChallenges;
	[SerializeField] TextMeshProUGUI progressLabelPhotoHunt;
	[SerializeField] TextMeshProUGUI progressLabelDemonSoccerGolf;
	[SerializeField] TextMeshProUGUI progressLabelArena;
	[SerializeField] TextMeshProUGUI progressLabelArcade;
	
	void Awake ()
	{
#if !DT_EXPORT && !DT_MOD
		GameDataManager.Instance.Data.CollectablesSafetyCheck ();
		var gameData = GameDataManager.Instance.Data;

		var batteryCurrent = gameData.getBatteryTotal;
		var batteryMax = RecordsDataManager.Data.getBatteriesMax;
		progressLabelBattery.text = batteryCurrent + "/" + batteryMax;

		var cakeCurrent = gameData.GetCollectablesOwnedCurrent (CollectableType.Cake, false);
		var cakeMax = RecordsDataManager.Data.getCakesMax;
		progressLabelCake.text = cakeCurrent + "/" + cakeMax;

		var candyCurrent = gameData.GetCollectablesOwnedCurrent (CollectableType.Candy, false);
		var candyMax = RecordsDataManager.Data.getCandyMax;
		progressLabelCandy.text = candyCurrent + "/" + candyMax;

		var trophyCurrent = gameData.getTrophyTotal;
		var trophyMax = RecordsDataManager.Data.getTrophiesMax;
		progressLabelTrophy.text = trophyCurrent + "/" + trophyMax;

		var miniChallengesCurrent = gameData.GetQuestsCompleted (SubQuestType.MiniChallenge);
		var miniChallengesMax = RecordsDataManager.Data.getMiniChallengesTotal;
		progressLabelMiniChallenges.text = miniChallengesCurrent + "/" + miniChallengesMax;

		var photoHuntsCurrent = gameData.GetQuestsCompleted (SubQuestType.PhotoHunt);
		var photoHuntsMax = RecordsDataManager.Data.getPhotoHuntsTotal;
		progressLabelPhotoHunt.text = photoHuntsCurrent + "/" + photoHuntsMax;

		var demonSoccerGolfCurrent = gameData.GetQuestsCompleted (SubQuestType.DemonSoccer);
		var demonSoccerGolfMax = RecordsDataManager.Data.getDemonGolfTotal;
		progressLabelDemonSoccerGolf.text = demonSoccerGolfCurrent + "/" + demonSoccerGolfMax;

		var arenasCurrent = gameData.GetQuestsCompleted (SubQuestType.Arena);
		var arenasMax = RecordsDataManager.Data.getArenaTotal;
		progressLabelArena.text = arenasCurrent + "/" + arenasMax;

		var arcadesCurrent = gameData.GetQuestsCompleted (SubQuestType.Arcade);
		var arcadesMax = RecordsDataManager.Data.getArcadeCartridgesTotal;
		progressLabelArcade.text = arcadesCurrent + "/" + arcadesMax;

		var dkCurrent = 0;
		if (gameData.GetLevelData (LevelID.DemonKing_Level).CompletedStandard)
			dkCurrent++;
		if (gameData.GetLevelData (LevelID.DemonKing_Boss).CompletedStandard)
			dkCurrent++;
		progressLabelDK.text = dkCurrent + "/2";

		if (!GameDataManager.Instance.Data.GetTurfEvent (ForktownHubManager.HubEvent.EverythingCollected)
			&& batteryCurrent >= batteryMax
			&& cakeCurrent >= cakeMax
			&& candyCurrent >= candyMax
			&& trophyCurrent >= trophyMax
			&& miniChallengesCurrent >= miniChallengesMax
			&& photoHuntsCurrent >= photoHuntsMax
			&& demonSoccerGolfCurrent >= demonSoccerGolfMax
			&& arenasCurrent >= arenasMax
			&& arcadesCurrent >= arcadesMax)
		{
			GameDataManager.Instance.Data.SetTurfEvent (ForktownHubManager.HubEvent.EverythingCollected, true);
			PlatformManager.Instance.SetAchievementStat (AchievementCategories.Totals_DKVault, 1);
		}
#endif
	}
}