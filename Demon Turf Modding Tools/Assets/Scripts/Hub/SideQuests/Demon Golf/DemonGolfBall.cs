﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemonGolfBall : MonoBehaviour
{
    [Header ("Idling")]
    [SerializeField] float idleBobSpeed = 3;
    [SerializeField] float idleBobMagnitude = 0.5f;
    [SerializeField] float idleSpinSpeed = 0.5f;

    [Header ("Particles")]
    [SerializeField] float respawnDelay = 0.5f;
    [SerializeField] ParticleSystem teleportParticle;
    [SerializeField] ParticleSystem victoryParticle;
    IPlayerController player;
    DemonGolfRespawnPoint respawn;
    Vector3 respawnPoint;
    Rigidbody rigid;
    ReceiveKnockback knocker;
    ReceiveHold holder;
    Renderer[] renderers;
    float originalAngularDrag;
    public IPlayerController getPlayer => player;

    void Awake () 
    {
        rigid = GetComponent<Rigidbody> ();
        knocker = GetComponent<ReceiveKnockback> ();
        holder = GetComponent<ReceiveHold> ();
        renderers = GetComponentsInChildren<Renderer> ();
        originalAngularDrag = rigid.angularDrag;
        knocker.onAttacked.AddListener (OnKnockback);
        holder.onStartHold.AddListener (OnHold);
        PlayerControllerEvents.onSpawned += PlayerSpawned;
    }

    void Start () 
    {
        rigid.isKinematic = true;
    }
    void OnDestroy ()
    {
        if (knocker)
            knocker.onAttacked.RemoveListener (OnKnockback);
        if (holder)
            holder.onStartHold.RemoveListener (OnHold);
        PlayerControllerEvents.onSpawned -= PlayerSpawned;
        if (player != null)
            player.onKilled -= Spawn;
    }

	public void FindPlayer ()
	{
		if (player != null) return;
		var _player = gameObject.scene.FindPlayerInterface ();
		if (_player != null)
			PlayerSpawned (_player);
	}

    void PlayerSpawned (IPlayerController _player)
    {
        PlayerControllerEvents.onSpawned -= PlayerSpawned;
        player = _player;
        player.onKilled += Spawn;
    }

    void Update () 
    {
        if (!rigid.isKinematic)
        {
            enabled = false;
            return;
        }

        var position = respawnPoint + (Vector3.up * Mathf.Sin (Time.time * idleBobSpeed) * idleBobMagnitude);
        rigid.MovePosition (position);
    }
    void TriggerPhysics ()
    {
        rigid.isKinematic = false;
		rigid.interpolation = RigidbodyInterpolation.Interpolate;
        rigid.angularDrag = originalAngularDrag;
		if (respawn)
        	respawn.ReleaseHold ();
    }
    void OnKnockback (Vector3 force)
    {
        if (rigid.isKinematic)
        {
            TriggerPhysics ();
            knocker.ApplyKnockback (force.normalized, force.magnitude);
        }

    }
    void OnHold () 
    {
        if (rigid.isKinematic)
            TriggerPhysics ();
    }
    public void AssignRespawnPoint (DemonGolfRespawnPoint _respawn)
    {
        if (respawn == _respawn)
            return;

		if (respawn)
	        respawn.Deactivate ();
        respawn = _respawn;
        respawnPoint = respawn.getBallSpawnPoint;
    }
    
    public void Spawn () 
    {
        foreach (var rend in renderers) rend.enabled = true;
        transform.position = respawnPoint;
        transform.rotation = Quaternion.identity;
        rigid.isKinematic = true;
		rigid.velocity = Vector3.zero;
        rigid.angularVelocity = Random.onUnitSphere * idleSpinSpeed;
        rigid.angularDrag = 0;
		if (respawn)
        	respawn.TriggerHold ();
        enabled = true;
    }
    public void Respawn () 
    {
		rigid.isKinematic = true;
		rigid.interpolation = RigidbodyInterpolation.None;
		rigid.velocity = Vector3.zero;
        rigid.angularVelocity = Vector3.zero;
        foreach (var rend in renderers) rend.enabled = false;
        Instantiate (teleportParticle, transform.position, Quaternion.identity);
        if (respawn)
        	respawn.TriggerCamera ();
        
        StartCoroutine (this.WaitAndDoRealtime (respawnDelay, () => 
        {
            Instantiate (teleportParticle, respawnPoint, Quaternion.identity);
            Spawn ();
        }));
    }
    public void Despawn () 
    {
        Instantiate (victoryParticle, transform.position, Quaternion.identity);
        gameObject.SetActive (false);
    }
    
}
