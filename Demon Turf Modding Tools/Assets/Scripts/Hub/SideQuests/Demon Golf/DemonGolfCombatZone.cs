﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemonGolfCombatZone : MonoBehaviour
{
    [SerializeField] LayerMask enemyLayer;
    [SerializeField] List<AIEnemy> residents;

    void Start () 
    {
        AddResidents ();
    }
    void OnTriggerEnter (Collider hit)
    {
        if (!hit.GetComponent<DemonGolfBall> ())
            return;

        foreach (var resident in residents)
        {
            resident.SetAttackTarget (hit.transform);
        }
    }

    void OnTriggerExit (Collider hit)
    {
        if (!hit.GetComponent<DemonGolfBall> ())
            return;
        
        foreach (var resident in residents)
        {
            resident.ClearAttackTarget ();
        }
    }

    void AddResidents () 
    {
        var box = GetComponent<BoxCollider> ();
        Collider[] hits = null;
        if (box)
            hits = Physics.OverlapBox (box.bounds.center, Vector3.Scale (box.size, transform.lossyScale) / 2, transform.rotation, enemyLayer, QueryTriggerInteraction.Ignore);
        else 
        {
            var sphere = GetComponent<SphereCollider> ();
            if (sphere)
                hits = Physics.OverlapSphere (sphere.bounds.center, sphere.radius * transform.lossyScale.magnitude / 2, enemyLayer, QueryTriggerInteraction.Ignore);
        }
    
        if (hits == null || hits.Length == 0)
            return;
             
        for (int i = 0; i < hits.Length; i++) 
        {
            var resident = hits[i].GetComponentInParent<AIEnemy> ();
            AddResident (resident);
        }   
    }
    void AddResident (AIEnemy enemy) 
    {
        if (!enemy)
            return;
        if (residents.Contains (enemy))
            return;
        residents.Add (enemy);
    }
}
