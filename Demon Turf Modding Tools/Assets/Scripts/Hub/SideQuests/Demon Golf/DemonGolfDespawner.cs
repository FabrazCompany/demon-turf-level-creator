﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemonGolfDespawner : MonoBehaviour
{
    void OnTriggerEnter (Collider hit)
    {
        var ball = hit.GetComponent<DemonGolfBall> ();
        if (!ball)
            return;
        
        ball.Respawn ();
    }
}
