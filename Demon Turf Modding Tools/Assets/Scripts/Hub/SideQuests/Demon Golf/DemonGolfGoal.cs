using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// [RequireComponent (typeof (CompleteLevel))]
public class DemonGolfGoal : MonoBehaviour
{   
    [SerializeField] AudioClip victoryClip;
    [SerializeField] bool triggerLevelComplettion = true;
	void Awake ()
	{
		if (triggerLevelComplettion)
			gameObject.AddComponent<CompleteLevel> ();
	}
    void OnTriggerEnter (Collider hit)
    {
        var ball = hit.GetComponent<DemonGolfBall> ();
        if (!ball)
            return;
        
        ball.Despawn ();
        
        if (triggerLevelComplettion)
        {
            SoundManagerStub.Instance.PlaySoundGameplay2D (victoryClip, transform.position);
            var completer = gameObject.GetComponent<CompleteLevel> ();
            completer?.TriggerLevelCompletion ();
        }
    }
}
