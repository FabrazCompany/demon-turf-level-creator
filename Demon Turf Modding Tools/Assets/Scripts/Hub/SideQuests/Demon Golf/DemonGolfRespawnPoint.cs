﻿using System;
using UnityEngine;

public class DemonGolfRespawnPoint : MonoBehaviour
{
    [SerializeField] Transform ballSpawnPoint;
    [SerializeField] GameObject respawnCamera;
    [SerializeField] Animator anim;
    public event Action<bool> onActivated;
    public event Action onBallReleased;
    public Vector3 getBallSpawnPoint => ballSpawnPoint.position;

    public void Activate (DemonGolfBall ball)
    {
        ball.AssignRespawnPoint (this);
        anim.Play ("Active");
        onActivated?.Invoke (true);
    }
    public void Deactivate () 
    {
        anim.Play ("Inactive");
        onActivated?.Invoke (false);
    }
    public void TriggerHold () 
    {
        anim.Play ("Holding Ball");
    }
    public void TriggerCamera () 
    {
        respawnCamera.SetActive (true);
    }
    public void ReleaseHold () 
    {
        anim.Play ("Active");
        onBallReleased?.Invoke ();
    }

    [Header ("Debugging")]
    [SerializeField] bool drawDebug;
    [SerializeField, Conditional ("drawDebug")] float debugBallSize;
    void OnDrawGizmosSelected () 
    {
        if (!drawDebug)
            return;
        if (!ballSpawnPoint)
            return;
        
        Gizmos.color = Color.green;
        Gizmos.DrawSphere (ballSpawnPoint.position, debugBallSize);
    }
}   
