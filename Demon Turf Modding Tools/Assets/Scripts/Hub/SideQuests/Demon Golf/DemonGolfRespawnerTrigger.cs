﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DemonGolfRespawnerTrigger : MonoBehaviour
{
    Animator anim;
    DemonGolfRespawnPoint spawnPoint;

    void Awake () 
    {
        anim = GetComponent<Animator> ();
        spawnPoint = GetComponentInParent<DemonGolfRespawnPoint> ();

        if (spawnPoint)
            spawnPoint.onActivated += Activate;
    }    
    void OnDestroy () 
    {
        if (spawnPoint)
            spawnPoint.onActivated -= Activate;
    }

    void Activate (bool active)
    {
        Debug.Log ("Activate: " + active);
        if (active)
            anim.Play ("Trigger");
        else
            anim.Play ("Inactive");
    }

    void OnTriggerEnter (Collider hit) 
    {
        var ball = hit.GetComponent<DemonGolfBall> ();
        if (!ball)
            return;
        
        Debug.Log ("Triggering");
        spawnPoint.Activate (ball);
    }
}   
