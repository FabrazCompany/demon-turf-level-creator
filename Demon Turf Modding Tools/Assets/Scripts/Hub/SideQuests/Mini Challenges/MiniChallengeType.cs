public enum MiniChallengeType
{
	INVALID,
	Challenge1,
	Challenge2,
	Challenge3,
	Challenge4,
	Challenge5,
	Challenge6,
	Challenge7,
	Challenge8,
	Challenge9,
	Challenge10,
	Challenge11,
	Challenge12,
	Challenge13,
	Challenge14,
	Challenge15,
	Challenge16,
	Challenge17,
	Challenge18,
	Challenge19,
	Challenge20,
	Challenge21,
	Challenge22,
	Challenge23,
	Challenge24,
	Challenge25,
	Challenge26,
	Challenge27,
	Challenge28,
	Challenge29,
	Challenge30,
}