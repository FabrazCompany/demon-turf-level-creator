﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PhotoHuntGalleryItem : MonoBehaviour
{
    [SerializeField] PhotoHuntType targetPhoto;
    [SerializeField] MeshRenderer canvasRend;
    [SerializeField] TextMeshProUGUI titleText;
    [SerializeField] TextMeshProUGUI descriptionText;
    [SerializeField] Light galleryLight;

    Material material;

#if !DT_EXPORT && !DT_MOD
    void Awake () 
    {
        material = canvasRend.material;
        var data = GameDataManager.Instance.Data;
        if (data.GetQuestCompleted (targetPhoto))
            SetPicture ();
        else 
            data.onPhotoHuntCompleted += OnPhotoHuntCompleted;
    }
    void OnDestroy () 
    {
        Destroy (material);
        if (GameDataManager.IsInitialized)
            GameDataManager.Instance.Data.onPhotoHuntCompleted -= OnPhotoHuntCompleted;
    }

    void OnPhotoHuntCompleted (PhotoHuntType completedHunt) 
    {
        if (completedHunt == targetPhoto)
            SetPicture ();
    }

    void SetPicture () 
    {
        material.SetTexture ("_MainTex", GameDataManager.Instance.Data.GetQuestData (targetPhoto).getTexture);
        var record = RecordsDataManager.Data.GetPhotoHuntRecord (targetPhoto);
        titleText.text = record.getName;
        descriptionText.text = record.getArtistStatement;
        galleryLight.enabled = true;
    }
#endif
}
