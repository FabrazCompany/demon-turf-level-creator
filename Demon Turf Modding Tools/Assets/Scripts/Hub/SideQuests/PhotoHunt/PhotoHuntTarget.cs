﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotoHuntTarget : MonoBehaviour
{
    [SerializeField] PhotoHuntType photoHuntType;
    [SerializeField] float range = 100;
    [SerializeField] Transform[] lineOfSightPoints;

	SphereCollider hitbox;
    public PhotoHuntType getPhotoHuntType => photoHuntType;
	void Awake ()
	{
		hitbox = GetComponent<SphereCollider> ();
	}
    public bool CheckVisibility (PhotoHuntViewfinder viewer) 
    {
		var dist = Vector3.Distance (transform.position, viewer.transform.position);
        if (dist > range)
            return false;
		var hit = Physics.Raycast (viewer.transform.position, viewer.transform.Heading (transform), out var hitInfo, dist - (hitbox ? hitbox.radius : 0), viewer.getObstructionLayers, QueryTriggerInteraction.Ignore);
        if (hit && !hitInfo.collider.transform.IsChildOf (transform))
            return false;

        foreach (var sightPoint in lineOfSightPoints)
        {
            hit = Physics.Linecast (transform.position, sightPoint.position, out hitInfo, viewer.getObstructionLayers, QueryTriggerInteraction.Ignore);
            if (hit && !hitInfo.collider.transform.IsChildOf (transform))
                return false;
        }
        return true;
    }
}
