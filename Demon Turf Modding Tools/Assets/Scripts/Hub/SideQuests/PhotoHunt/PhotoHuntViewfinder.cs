﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotoHuntViewfinder : MonoBehaviour
{
    [SerializeField] LayerMask obstructionLayers;
    [SerializeField] float checkRate = .5f;
    float timer;
    Collider trigger;
    List<PhotoHuntTarget> activeTargets;
    PhotoHuntTarget activeTarget;
    

    public LayerMask getObstructionLayers => obstructionLayers;
    public event Action<PhotoHuntType> onActiveTargetChanged;
	bool isCustomLevel;

    void Awake () 
    {
        trigger = GetComponent<Collider> ();
        trigger.enabled = false;
        activeTargets = new List<PhotoHuntTarget> ();
		isCustomLevel = LevelUtility.IsCustomLevel ();
    }

    void OnTriggerEnter (Collider hit)
    {
        var photoTarget = hit.attachedRigidbody ? hit.attachedRigidbody.GetComponentInChildren<PhotoHuntTarget> () : hit.GetComponent<PhotoHuntTarget> ();
        if (!photoTarget)
            return;
        if (activeTargets.Contains (photoTarget))
            return;
        activeTargets.Add (photoTarget);
        enabled = true;
        timer = 0;
    }

    void OnTriggerExit (Collider hit)
    {
        var photoTarget = hit.attachedRigidbody ? hit.attachedRigidbody.GetComponentInChildren<PhotoHuntTarget> () : hit.GetComponent<PhotoHuntTarget> ();
        if (!photoTarget)
            return;
        if (!activeTargets.Contains (photoTarget))
            return;
        activeTargets.Remove (photoTarget);
    }

    void Update ()
    {
		if (isCustomLevel) return;
		
        if (activeTargets.Count == 0)
        {   
            if (activeTarget)
            {
                activeTarget = null;
                onActiveTargetChanged?.Invoke (PhotoHuntType.INVALID);
            }
            enabled = false;
            return;
        }

        timer -= Time.deltaTime;
        if (timer > 0)
            return;
        timer = checkRate;

        activeTargets.Sort ((x, y) => (int)Mathf.Sign ((transform.position - x.transform.position).sqrMagnitude - (transform.position - y.transform.position).sqrMagnitude));   //kinda messy, but should work for simple distance check
        foreach (var target in activeTargets)
        {
            if (target.CheckVisibility (this))
            {
                if (target != activeTarget)
                {
                    activeTarget = target;
                    onActiveTargetChanged?.Invoke (target.getPhotoHuntType);
                }
                return;
            }
        }

        activeTarget = null;
        onActiveTargetChanged?.Invoke (PhotoHuntType.INVALID);
    }

    public void Activate (bool state) 
    {
        trigger.enabled = state;
        if (!state)
        {
            activeTargets.Clear ();
            activeTarget = null;
            onActiveTargetChanged?.Invoke (PhotoHuntType.INVALID);
        }
    }
}
