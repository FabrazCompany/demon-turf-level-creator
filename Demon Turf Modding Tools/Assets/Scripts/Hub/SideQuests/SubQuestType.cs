public enum SubQuestType
{
	PhotoHunt = 5,
	MiniChallenge = 10,
	Arena = 15,
	DemonSoccer = 20,
	Arcade = 25,
}