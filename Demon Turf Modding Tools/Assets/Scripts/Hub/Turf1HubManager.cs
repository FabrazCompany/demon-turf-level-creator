using RotaryHeart.Lib.SerializableDictionary;

public class Turf1HubManager : BaseHubManager
    <
        Turf1HubManager.StartPoint, 
        Turf1HubManager.HubEvent, 
        HubStartPointData, 
        Turf1StartPointDataList
    >
{
    public enum StartPoint 
    {
        Default = 0,
        StateChanger = 5,
        Portal_Forktown = 50,
        Portal_Intro = 55,
        Portal_GrandCanyon = 60,
        Portal_FlowerGarden = 65,
        Portal_ArenaCity = 70,
        Portal_Ravine = 75,
        Portal_CaveDwellings = 80,
        Portal_BurgStash = 95,
        Portal_Boss = 100,
    }
    public enum HubEvent
    {
        IntroLevelBeat = 0,
        IntroLevelBeatUnlock = 1,
        HubVisited = 10,

        BossIntro = 100,
		BossTime=105,
        BossBeaten = 110,
		
		State2Beaten = 200,

		NewGamePlusUnlocked = 1000,
		TurfAbilityUnlocked = 1500,
    }


    protected override TurfID turfID => TurfID.Apocadesert;
    protected override StartPoint stateChangeStartPoint => StartPoint.StateChanger;
    protected override void Start ()
    {
        base.Start ();
#if !DT_EXPORT && !DT_MOD
        TriggerTurfStateUpdatedEvent (GameDataManager.Instance.Data.GetTurfState (turfID));
#endif
		SoundManagerStub.Instance.PlayBackgroundMusicHub((int)ForktownSection.Hub1);

    }

	public static void SetStartPoint (LevelID _level) => SetSavedDestination (TurfID.Apocadesert, GetTurf1Start (_level));
	public static StartPoint GetTurf1Start (LevelID levelID)
    {
        switch (levelID)
        {
            default:
            case LevelID.Forktown:
                return StartPoint.Portal_Forktown;
            case LevelID.Apocadesert_Intro:
                return StartPoint.Portal_Intro;
            case LevelID.Apocadesert_ArenaCity:
                return StartPoint.Portal_ArenaCity;
            case LevelID.Apocadesert_BurgStash:
                return StartPoint.Portal_BurgStash;
            case LevelID.Apocadesert_CaveDwellings:
                return StartPoint.Portal_CaveDwellings;
            case LevelID.Apocadesert_FlowerGarden:
                return StartPoint.Portal_FlowerGarden;
            case LevelID.Apocadesert_GrandCanyon:
                return StartPoint.Portal_GrandCanyon;
            case LevelID.Apocadesert_Ravine:
                return StartPoint.Portal_Ravine;
            case LevelID.Apocadesert_Boss:
                return StartPoint.Portal_Boss;
        }
    }

}

[System.Serializable]
public class Turf1StartPointDataList : SerializableDictionaryBase<Turf1HubManager.StartPoint, HubStartPointData> {}