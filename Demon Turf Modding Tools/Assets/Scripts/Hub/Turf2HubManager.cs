using RotaryHeart.Lib.SerializableDictionary;

public class Turf2HubManager : BaseHubManager
    <
        Turf2HubManager.StartPoint, 
        Turf2HubManager.HubEvent, 
        HubStartPointData, 
        Turf2StartPointDataList
    >
{
    public enum StartPoint 
    {
        Default = 0,
        StateChanger = 5,
        Portal_Forktown = 50,
        Portal_Intro = 55,
        Portal_PortTown = 60,
        Portal_Bay = 65,
        Portal_Volcano = 70,
        Portal_Sewer = 75,
        Portal_Waterfall = 80,
        Portal_Tides = 95,
        Portal_Boss = 100,
    }
    public enum HubEvent
    {
        IntroLevelBeat = 0,
        IntroLevelBeatUnlock = 1,
        HubVisited = 10,

        BossIntro = 100,
		BossTime=105,
        BossBeaten = 110,
		
		State2Beaten = 200,

		NewGamePlusUnlocked = 1000,
		TurfAbilityUnlocked = 1500,
    }


    protected override TurfID turfID => TurfID.Armadageddon;
    protected override StartPoint stateChangeStartPoint => StartPoint.StateChanger;
    protected override void Start ()
    {
        base.Start ();
#if !DT_EXPORT && !DT_MOD
        TriggerTurfStateUpdatedEvent (GameDataManager.Instance.Data.GetTurfState (turfID));
#endif
		SoundManagerStub.Instance.PlayBackgroundMusicHub((int)ForktownSection.Hub2);

    }
    
	
	public static void SetStartPoint (LevelID _level) => SetSavedDestination (TurfID.Armadageddon, GetTurf2Start (_level));
	static StartPoint GetTurf2Start (LevelID levelID)
    {
        switch (levelID)
        {
            default:
                return StartPoint.Default;
            case LevelID.Forktown:
                return StartPoint.Portal_Forktown;
            case LevelID.Armadageddon_Intro:
                return StartPoint.Portal_Intro;
            case LevelID.Armadageddon_PortTown:
                return StartPoint.Portal_PortTown;
            case LevelID.Armadageddon_Bay:
                return StartPoint.Portal_Bay;
            case LevelID.Armadageddon_Volcano:
                return StartPoint.Portal_Volcano;
            case LevelID.Armadageddon_Sewer:
                return StartPoint.Portal_Sewer;
            case LevelID.Armadageddon_Waterfall:
                return StartPoint.Portal_Waterfall;
            case LevelID.Armadageddon_Tides:
                return StartPoint.Portal_Tides;
            case LevelID.Armadageddon_Boss:
                return StartPoint.Portal_Boss;
        }
    }

}

[System.Serializable]
public class Turf2StartPointDataList : SerializableDictionaryBase<Turf2HubManager.StartPoint, HubStartPointData> {}