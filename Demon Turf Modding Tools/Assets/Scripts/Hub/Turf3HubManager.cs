using RotaryHeart.Lib.SerializableDictionary;

public class Turf3HubManager : BaseHubManager
    <
        Turf3HubManager.StartPoint, 
        Turf3HubManager.HubEvent, 
        HubStartPointData, 
        Turf3StartPointDataList
    >
{
    public enum StartPoint 
    {
        Default = 0,
        StateChanger = 5,
        Portal_Forktown = 50,
        Portal_Intro = 55,
        Portal_Highway = 60,
        Portal_Shopping = 65,
        Portal_Skyscraper = 70,
        Portal_BuildingTop = 75,
        Portal_Construction = 80,
        Portal_Subway = 95,
        Portal_Boss = 100,
    }
    public enum HubEvent
    {
        IntroLevelBeat = 0,
        IntroLevelBeatUnlock = 1,
        HubVisited = 10,

        BossIntro = 100,
		BossTime=105,
        BossBeaten = 110,
		
		State2Beaten = 200,

		NewGamePlusUnlocked = 1000,
		TurfAbilityUnlocked = 1500,
    }


    protected override TurfID turfID => TurfID.NewNeoCity;
    protected override StartPoint stateChangeStartPoint => StartPoint.StateChanger;    
    protected override void Start ()
    {
        base.Start ();
#if !DT_EXPORT && !DT_MOD
        TriggerTurfStateUpdatedEvent (GameDataManager.Instance.Data.GetTurfState (turfID));
#endif
		SoundManagerStub.Instance.PlayBackgroundMusicHub((int)ForktownSection.Hub3);

    }

	public static void SetStartPoint (LevelID _level) => SetSavedDestination (TurfID.NewNeoCity, GetTurf3Start (_level));
	static StartPoint GetTurf3Start (LevelID levelID)
    {
        switch (levelID)
        {
            default:
                return StartPoint.Default;
            case LevelID.Forktown:
                return StartPoint.Portal_Forktown;
            case LevelID.NewNeoCity_Intro:
                return StartPoint.Portal_Intro;
            case LevelID.NewNeoCity_Highway:
                return StartPoint.Portal_Highway;
            case LevelID.NewNeoCity_Shopping:
                return StartPoint.Portal_Shopping;
            case LevelID.NewNeoCity_Skyscraper:
                return StartPoint.Portal_Skyscraper;
            case LevelID.NewNeoCity_BuildingTop:
                return StartPoint.Portal_BuildingTop;
            case LevelID.NewNeoCity_Construction:
                return StartPoint.Portal_Construction;
            case LevelID.NewNeoCity_Subway:
                return StartPoint.Portal_Subway;
            case LevelID.NewNeoCity_Boss:
                return StartPoint.Portal_Boss;
        }
    }


}

[System.Serializable]
public class Turf3StartPointDataList : SerializableDictionaryBase<Turf3HubManager.StartPoint, HubStartPointData> {}