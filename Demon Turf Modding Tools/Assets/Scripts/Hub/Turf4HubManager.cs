using RotaryHeart.Lib.SerializableDictionary;

public class Turf4HubManager : BaseHubManager
    <
        Turf4HubManager.StartPoint, 
        Turf4HubManager.HubEvent, 
        HubStartPointData, 
        Turf4StartPointDataList
    >
{
    public enum StartPoint 
    {
        Default = 0,
        StateChanger = 5,
        Portal_Forktown = 50,
        Portal_Intro = 55,
        Portal_Woodlands = 60,
        Portal_CliffTemples = 65,
        Portal_HangingCeiling = 70,
        Portal_IceCave = 75,
        Portal_AncientRuins = 80,
        Portal_SkiPiste = 95,
        Portal_Boss = 100,
    }
    public enum HubEvent
    {
        IntroLevelBeat = 0,
        IntroLevelBeatUnlock = 1,
        HubVisited = 10,

        BossIntro = 100,
		BossTime=105,
        BossBeaten = 110,
		
		State2Beaten = 200,

		NewGamePlusUnlocked = 1000,
		TurfAbilityUnlocked = 1500,
    }


    protected override TurfID turfID => TurfID.PeakPlateau;
    protected override StartPoint stateChangeStartPoint => StartPoint.StateChanger;
    protected override void Start ()
    {
        base.Start ();
#if !DT_EXPORT && !DT_MOD
        TriggerTurfStateUpdatedEvent (GameDataManager.Instance.Data.GetTurfState (turfID));
#endif
		SoundManagerStub.Instance.PlayBackgroundMusicHub((int)ForktownSection.Hub4);
    }
	public static void SetStartPoint (LevelID _level) => SetSavedDestination (TurfID.PeakPlateau, GetTurf4Start (_level));
	static StartPoint GetTurf4Start (LevelID levelID)
    {
        switch (levelID)
        {
            default:
                return StartPoint.Default;
            case LevelID.Forktown:
                return StartPoint.Portal_Forktown;
            case LevelID.PeakPlateau_Intro:
                return StartPoint.Portal_Intro;
            case LevelID.PeakPlateau_Woodlands:
                return StartPoint.Portal_Woodlands;
            case LevelID.PeakPlateau_CliffTemples:
                return StartPoint.Portal_CliffTemples;
            case LevelID.PeakPlateau_HangingCeiling:
                return StartPoint.Portal_HangingCeiling;
            case LevelID.PeakPlateau_IceCave:
                return StartPoint.Portal_IceCave;
            case LevelID.PeakPlateau_AncientRuins:
                return StartPoint.Portal_AncientRuins;
            case LevelID.PeakPlateau_SkiPiste:
                return StartPoint.Portal_SkiPiste;
            case LevelID.PeakPlateau_Boss:
                return StartPoint.Portal_Boss;
        }
    }


}

[System.Serializable]
public class Turf4StartPointDataList : SerializableDictionaryBase<Turf4HubManager.StartPoint, HubStartPointData> {}