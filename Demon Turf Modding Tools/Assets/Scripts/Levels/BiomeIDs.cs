public enum BiomeID
{
	CUSTOM = 0,

	Forktown_Base = 10,
	Forktown_Tutorial = 11,
	Forktown_DemonSoccerGolf = 12,
	Forktown_DKLair = 13,

	Apocadesert_Base = 20,
	Apocadesert_Oasis = 21,
	Apocadesert_Flower = 22,
	Apocadesert_Cyclone = 23,

	Armadageddon_Base = 30,
	Armadageddon_Lava = 31,
	Armadageddon_Volcano = 32,
	Armadageddon_Poison1 = 33,
	Armadageddon_Poison2 = 34,

	NewNeoCity_Base = 40,
	NewNeoCity_Construction = 41,
	NewNeoCity_Spooky = 43,

	PeakPlateau_Base = 50,
	PeakPlateau_HangingCeiling = 51,

	Arcade = 60,
	Trials = 61,

	DKBiome_Base = 70,
	DKBiome_Vault = 71,
}