﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompleteLevel : MonoBehaviour
{
    public event Action onLevelCompleted;
    public void TriggerLevelCompletion () 
    {
        onLevelCompleted?.Invoke ();
    }
}
