using UnityEngine;

public abstract class BaseHubEventSetter<TEvent> : MonoBehaviour where TEvent : System.Enum
{
    [SerializeField] protected TEvent targetEvent;
    public virtual void SetEventState (bool state) 
    {
#if !DT_EXPORT && !DT_MOD 
        GameDataManager.Instance.Data.SetTurfEvent (targetEvent, state);
#endif
    }
}