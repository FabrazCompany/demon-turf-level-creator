public class ForktownEventSetter : BaseHubEventSetter<ForktownHubManager.HubEvent>
{
	
	public override void SetEventState(bool state)
	{
		base.SetEventState(state);
#if !DT_EXPORT && !DT_MOD 
		if (targetEvent >= ForktownHubManager.HubEvent.Arcade1Unlocked 
			&& targetEvent <= ForktownHubManager.HubEvent.Arcade8Unlocked)
		{
			if (!GameDataManager.Instance.Data.GetTurfEvent (ForktownHubManager.HubEvent.ArcadeCompleted))
			{
				GameDataManager.Instance.Data.SetTurfEvent (ForktownHubManager.HubEvent.ArcadeCompleted, true);
			}
			PlatformManager.Instance.SetAchievementStat (AchievementCategories.Quests_ArcadesCompleted, GameDataManager.Instance.Data.GetCollectablesOwnedCurrent (CollectableType.Cartridge, true));
		}
#endif
	}
}