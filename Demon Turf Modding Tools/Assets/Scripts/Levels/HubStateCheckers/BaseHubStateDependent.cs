using UnityEngine;
using UnityEngine.Events;
using RotaryHeart.Lib.SerializableDictionary;

public abstract class BaseHubStateDependent<TEvent, TEventList> : MonoBehaviour 
    where TEvent : System.Enum
    where TEventList : SerializableDictionaryBase<TEvent, bool>
{
    [SerializeField, ReadOnly] bool triggered;
    [SerializeField] LevelPrerequisiteList levelCompletionRequirements;
    [SerializeField] TEventList turfEventRequirements;
    [SerializeField] UnityEvent onRequirementsMet;
    [SerializeField] UnityEvent onRequirementsFailed;

    void Start () 
    {
        triggered = RequirementsMet ();
        if (triggered)
            onRequirementsMet?.Invoke ();
        else
            onRequirementsFailed?.Invoke ();
    }
    bool RequirementsMet () 
    {
#if !DT_EXPORT && !DT_MOD
        var data = GameDataManager.Instance.Data;
        foreach (var key in levelCompletionRequirements.Keys)
        {
            var prereq = levelCompletionRequirements[key];
            if (prereq.firstTripDependent && prereq.firstTripCompleted != data.GetLevelData (key).CompletedStandard)
                return false;
            if (prereq.returnTripDependent && prereq.returnTripCompleted != data.GetLevelData (key).CompletedReturnTrip)
                return false;
            if (prereq.newGamePlusDependent && prereq.newGamePlusCompleted != data.GetLevelData (key).CompletedNewGamePlus)
                return false;
        }
        
        foreach (var key in turfEventRequirements.Keys) 
        {
            if (data.GetTurfEvent (LevelUtility.GetEventKey (key)) != turfEventRequirements[key])
                return false;
        }
#endif
        return true;
    }   
}
[System.Serializable]
public class LevelEventList<TEvent> : SerializableDictionaryBase<TEvent, bool> where TEvent : System.Enum {}

[System.Serializable]
public class LevelPrerequisiteList : SerializableDictionaryBase<LevelID, LevelPrerequisite> {}

[System.Serializable]
public struct LevelPrerequisite 
{
    public bool firstTripDependent;
    [Conditional ("firstTripDependent")]
    public bool firstTripCompleted;
    [Space]
    public bool returnTripDependent;
    [Conditional ("returnTripDependent")]
    public bool returnTripCompleted;
    [Space]
    public bool newGamePlusDependent;
    [Conditional ("newGamePlusDependent")]
    public bool newGamePlusCompleted;
}