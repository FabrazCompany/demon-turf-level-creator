using UnityEngine;
using UnityEngine.Events;

public class DyeStateDependent : MonoBehaviour 
{
	[SerializeField] DyeType dyeType;
	[SerializeField] DyeCategoryType dyeCategoryType;
	[SerializeField] UnityEvent onDyeNotOwned;
	[SerializeField] UnityEvent onDyeOwned;
	[SerializeField] UnityEvent onDyeEquipped;

#if !DT_EXPORT && !DT_MOD
	void Awake ()
	{
		var data = GameDataManager.Instance.Data;
		data.onEquippedDyeUpdated += CheckEvents;
	}
	void OnDestroy ()
	{
		if (GameDataManager.IsInitialized)
		{
			var data = GameDataManager.Instance.Data;
			data.onEquippedDyeUpdated -= CheckEvents;
		}
	}
	void OnEnable ()
	{
		CheckEvents ();
	}

	void CheckEvents (DyeType type, DyeCategoryType categoryType)
	{
		CheckEvents ();
	}
	void CheckEvents ()
	{
		var data = GameDataManager.Instance.Data;
		if (data.IsDyeOwned (dyeType, dyeCategoryType))
		{
			onDyeOwned?.Invoke ();
			if (data.IsDyeEquipped (dyeType, dyeCategoryType))
				onDyeEquipped?.Invoke ();
		}
		else 
		{
			onDyeNotOwned?.Invoke ();
		}
	}
#endif
}