using RotaryHeart.Lib.SerializableDictionary;
public class ForktownStateDependent : BaseHubStateDependent<ForktownHubManager.HubEvent, ForktownEventList> { }
[System.Serializable]
public class ForktownEventList : SerializableDictionaryBase <ForktownHubManager.HubEvent, bool> {}