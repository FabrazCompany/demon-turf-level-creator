using UnityEngine;
using UnityEngine.Events;

public class ModStateDependent : MonoBehaviour 
{
	[SerializeField] ModType modType;
	[SerializeField] UnityEvent onModNotOwned;
	[SerializeField] UnityEvent onModOwned;
	[SerializeField] UnityEvent onModEquipped;

#if !DT_EXPORT && !DT_MOD
	void Awake ()
	{
		var data = GameDataManager.Instance.Data;
		data.onModsAvailableUpdated += CheckEvents;
		data.onEquippedModsAdded += CheckEvents;
		data.onEquippedModsRemoved += CheckEvents;
	}
	void OnDestroy ()
	{
		if (GameDataManager.IsInitialized)
		{
			var data = GameDataManager.Instance.Data;
			data.onHeadPetsAvailableUpdated -= CheckEvents;
			data.onEquippedModsAdded -= CheckEvents;
			data.onEquippedModsRemoved -= CheckEvents;
		}
	}
	void OnEnable ()
	{
		CheckEvents ();
	}

	void CheckEvents (ModType type)
	{
		CheckEvents ();
	}
	void CheckEvents ()
	{
		var data = GameDataManager.Instance.Data;
		if (data.IsModOwned (modType))
		{
			onModOwned?.Invoke ();
			if (data.IsModEquipped (modType))
				onModEquipped?.Invoke ();
		}
		else 
		{
			onModNotOwned?.Invoke ();
		}
	}
#endif
}