using UnityEngine;
using UnityEngine.Events;

public class PetStateDependent : MonoBehaviour 
{

	[SerializeField] HeadPetType petType;
	[SerializeField] UnityEvent onPetNotOwned;
	[SerializeField] UnityEvent onPetOwned;
	[SerializeField] UnityEvent onPetEquipped;

#if !DT_EXPORT && !DT_MOD
	void Awake ()
	{
		var data = GameDataManager.Instance.Data;
		data.onHeadPetsAvailableUpdated += CheckEvents;
		data.onEquippedHeadPetUpdated += CheckEvents;
	}
	void OnDestroy ()
	{
		if (GameDataManager.IsInitialized)
		{
			var data = GameDataManager.Instance.Data;
			data.onHeadPetsAvailableUpdated -= CheckEvents;
			data.onEquippedHeadPetUpdated -= CheckEvents;
		}
	}
	void OnEnable ()
	{
		CheckEvents ();
	}

	void CheckEvents (HeadPetType type)
	{
		CheckEvents ();
	}
	void CheckEvents ()
	{
		var data = GameDataManager.Instance.Data;
		if (data.IsHeadPetOwned (petType))
		{
			onPetOwned?.Invoke ();
			if (data.IsHeadPetEquipped (petType))
				onPetEquipped?.Invoke ();
		}
		else 
		{
			onPetNotOwned?.Invoke ();
		}
	}
#endif
}