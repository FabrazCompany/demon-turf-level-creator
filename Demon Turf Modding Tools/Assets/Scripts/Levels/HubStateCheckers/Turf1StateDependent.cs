using RotaryHeart.Lib.SerializableDictionary;
public class Turf1StateDependent : BaseHubStateDependent<Turf1HubManager.HubEvent, Turf1EventList> { }
[System.Serializable]
public class Turf1EventList : SerializableDictionaryBase <Turf1HubManager.HubEvent, bool> {}