using RotaryHeart.Lib.SerializableDictionary;
public class Turf2StateDependent : BaseHubStateDependent<Turf2HubManager.HubEvent, Turf2EventList> { }
[System.Serializable]
public class Turf2EventList : SerializableDictionaryBase <Turf2HubManager.HubEvent, bool> {}