using RotaryHeart.Lib.SerializableDictionary;
public class Turf3StateDependent : BaseHubStateDependent<Turf3HubManager.HubEvent, Turf3EventList> { }
[System.Serializable]
public class Turf3EventList : SerializableDictionaryBase <Turf3HubManager.HubEvent, bool> {}