using RotaryHeart.Lib.SerializableDictionary;
public class Turf4StateDependent : BaseHubStateDependent<Turf4HubManager.HubEvent, Turf4EventList> { }
[System.Serializable]
public class Turf4EventList : SerializableDictionaryBase <Turf4HubManager.HubEvent, bool> {}