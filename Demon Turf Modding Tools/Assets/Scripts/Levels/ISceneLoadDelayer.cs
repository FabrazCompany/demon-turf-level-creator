
public interface ISceneLoadDelayer 
{
	bool isReady { get; }
}