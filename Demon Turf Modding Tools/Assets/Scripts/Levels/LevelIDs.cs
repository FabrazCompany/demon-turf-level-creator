public enum LevelID
{
    INVALID = -1,

	//0-49 Hubs
    Forktown = 0,
    Forktown_Turf1 = 5,
    Forktown_Turf2 = 10,
    Forktown_Turf3 = 15,
    Forktown_Turf4 = 20,
	Forktown_DemonKingLair = 25,

	ForktownAccess_Mods = 30,
	ForktownAccess_Dyes = 31,
	ForktownAccess_Pets = 32,
	ForktownAccess_DSG = 33,
	ForktownAccess_Arena = 34,
	ForktownAccess_Trials = 35,
	ForktownAccess_Arcade = 36,
	ForktownAccess_PhotoGallery = 37,


	ForktownAccess_BeebzRoom = 40,
	ForktownAccess_TownSq = 41,
	ForktownAccess_Underbelly = 42,
	ForktownAccess_Outskirts = 43,
	ForktownAccess_CastleAccess = 44,

	
	
    
	//50-99 Tutorial
    Tutorial = 50,
	Trials_Playground = 60,
    
	//100-199 Turf 1
    Apocadesert_Intro = 100,
    Apocadesert_ArenaCity = 105,
    Apocadesert_BurgStash = 110,
    Apocadesert_CaveDwellings = 115,
    Apocadesert_FlowerGarden = 120,
    Apocadesert_GrandCanyon = 125,
    Apocadesert_Ravine = 130,
    Apocadesert_Boss = 135,

	//200-299 Turf 2
    Armadageddon_Intro = 200,
    Armadageddon_PortTown = 205,
    Armadageddon_Bay = 210,
    Armadageddon_Volcano = 215,
    Armadageddon_Sewer = 220,
    Armadageddon_Waterfall = 225,
    Armadageddon_Tides = 230,
    Armadageddon_Boss = 235,

	//300-399 Turf 3
    NewNeoCity_Intro = 300,
    NewNeoCity_Highway = 305,
    NewNeoCity_Shopping = 310,
    NewNeoCity_Skyscraper = 315,
    NewNeoCity_BuildingTop = 320,
    NewNeoCity_Construction = 325,
    NewNeoCity_Subway = 330,
    NewNeoCity_Boss = 335,

	//400-499 Turf 4
    PeakPlateau_Intro = 400,
    PeakPlateau_Woodlands = 405,
    PeakPlateau_CliffTemples = 410,
    PeakPlateau_HangingCeiling = 415,
    PeakPlateau_IceCave = 420,
    PeakPlateau_AncientRuins = 425,
    PeakPlateau_SkiPiste = 430,
    PeakPlateau_Boss = 435,

	//500-599 Arcades
    Arcades_1= 500,
    Arcades_2 = 505,
    Arcades_4 = 510,
    Arcades_7 = 515,
    Arcades_8 = 520,
    Arcades_6 = 525,
    Arcades_5 = 530,
    Arcades_3 = 535,

    //600 - 699 Arena
    Quest_Arena1 = 600,
    Quest_Arena2 = 605,
    Quest_Arena3 = 610,
    Quest_Arena4 = 615,
    Quest_Arena5 = 620,
    Quest_Arena6 = 625,
    Quest_Arena7 = 630,
    Quest_Arena8 = 635,
    Quest_Arena9 = 640,
    Quest_Arena10 = 645,
	Quest_Arena11 = 646,
	Quest_Arena12 = 647,
	Quest_Arena13 = 648,
	Quest_Arena14 = 649,
	Quest_Arena15 = 650,
	Quest_Arena16 = 651,
	Quest_Arena17 = 652,
	Quest_Arena18 = 653,
	Quest_Arena19 = 654,
	Quest_Arena20 = 655,
	Quest_Arena21 = 656,
	Quest_Arena22 = 657,
	Quest_Arena23 = 658,
	Quest_Arena24 = 659,
	Quest_Arena25 = 660,

    //700 - 799 Demon Soccer
    Quest_DemonSoccer1 = 700,
    Quest_DemonSoccer2 = 705,
    Quest_DemonSoccer3 = 710,
    Quest_DemonSoccer4 = 715,
    Quest_DemonSoccer5 = 720,
    Quest_DemonSoccer6 = 725,
    Quest_DemonSoccer7 = 730,
    Quest_DemonSoccer8 = 735,
	Quest_DemonSoccer9 = 740,
    Quest_DemonSoccer10 = 745,

    // 800 - 899 Mini Challenges
    Quest_MiniChallenge1 = 801,
    Quest_MiniChallenge2 = 802,
    Quest_MiniChallenge3 = 803,
    Quest_MiniChallenge4 = 804,
    Quest_MiniChallenge5 = 805,
    Quest_MiniChallenge6 = 806,
    Quest_MiniChallenge7 = 807,
    Quest_MiniChallenge8 = 808,
    Quest_MiniChallenge9 = 809,
    Quest_MiniChallenge10 = 810,
    Quest_MiniChallenge11 = 811,
    Quest_MiniChallenge12 = 812,
    Quest_MiniChallenge13 = 813,
    Quest_MiniChallenge14 = 814,
    Quest_MiniChallenge15 = 815,
    Quest_MiniChallenge16 = 816,
    Quest_MiniChallenge17 = 817,
	Quest_MiniChallenge18 = 818,
	Quest_MiniChallenge19 = 819,
	Quest_MiniChallenge20 = 820,
	Quest_MiniChallenge21 = 821,
	Quest_MiniChallenge22 = 822,
	Quest_MiniChallenge23 = 823,
	Quest_MiniChallenge24 = 824,
	Quest_MiniChallenge25 = 825,
	Quest_MiniChallenge26 = 826,
	Quest_MiniChallenge27 = 827,
	Quest_MiniChallenge28 = 828,
	Quest_MiniChallenge29 = 829,
	Quest_MiniChallenge30 = 830,

	//900-999 Demon King
    DemonKing_Level = 900,
    DemonKing_Boss = 905,
	DemonKing_Vault = 910,

	CustomLevel = 1000,
}