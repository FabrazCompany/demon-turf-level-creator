﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent (typeof (DrawGizmoIcon))]
public class LevelStateDependent : MonoBehaviour, ILevelStateDependent
{
    [SerializeField] LevelState[] validStates;
    [SerializeField] UnityEvent onTrigger;
    public UnityEvent OnTrigger => onTrigger;

    void Awake () 
    {
        Init ();
    }
    bool initd;
    public void SafetyCheck () 
    {
        if (!initd)
            gameObject.SetActive (true);
    }
    void Init () 
    {
        if (initd) return;
        initd = true;
#if !DT_EXPORT && !DT_MOD
        LevelManager.onTurfStateUpdated += SetActive;
        ForktownHubManager.onTurfStateUpdated += SetActive;
        Turf1HubManager.onTurfStateUpdated += SetActive;
        Turf2HubManager.onTurfStateUpdated += SetActive;
        Turf3HubManager.onTurfStateUpdated += SetActive;
		Turf4HubManager.onTurfStateUpdated += SetActive;
#endif
        gameObject.SetActive (false);
    }
    void OnDestroy () 
    {
#if !DT_EXPORT && !DT_MOD
        LevelManager.onTurfStateUpdated -= SetActive;
        ForktownHubManager.onTurfStateUpdated -= SetActive;
        Turf1HubManager.onTurfStateUpdated -= SetActive;
        Turf2HubManager.onTurfStateUpdated -= SetActive;
        Turf3HubManager.onTurfStateUpdated -= SetActive;
		Turf4HubManager.onTurfStateUpdated -= SetActive;
#endif
    }

    public void SetActive (LevelID level, LevelState curState) => SetActive (curState);
    public void SetActive (LevelState curState)
    {
        for (int i = 0; i < validStates.Length; i++)
        {
            if (validStates[i] == curState)
            {
                gameObject.SetActive (true);
                onTrigger?.Invoke ();
                return;
            }
        }
        gameObject.SetActive (false);
    }
}

public interface ILevelStateDependent 
{
    void SetActive (LevelState curState);
}