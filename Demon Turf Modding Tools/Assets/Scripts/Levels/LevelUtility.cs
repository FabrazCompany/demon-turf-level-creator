using UnityEngine.SceneManagement;

#if UNITY_EDITOR
using UnityEditor;
#endif


public static class LevelUtility 
{
    public static bool IsHubScene (LevelID levelID)
    {
        switch (levelID)
        {
            default:
                return false;
            case LevelID.Forktown:
			case LevelID.ForktownAccess_BeebzRoom:
			case LevelID.ForktownAccess_TownSq:
			case LevelID.ForktownAccess_Underbelly:
			case LevelID.ForktownAccess_Outskirts:
			case LevelID.ForktownAccess_CastleAccess:
			case LevelID.ForktownAccess_Mods:
			case LevelID.ForktownAccess_Dyes:
			case LevelID.ForktownAccess_Pets:
			case LevelID.ForktownAccess_DSG:
			case LevelID.ForktownAccess_Arena:
			case LevelID.ForktownAccess_Trials:
			case LevelID.ForktownAccess_Arcade:
			case LevelID.ForktownAccess_PhotoGallery:
            case LevelID.Forktown_Turf1:
            case LevelID.Forktown_Turf2:
            case LevelID.Forktown_Turf3:
            case LevelID.Forktown_Turf4:
			case LevelID.Forktown_DemonKingLair:
			case LevelID.Trials_Playground:
                return true;
        }
    }
    public static bool IsHubScene (string sceneName)
    {
        switch (sceneName)
        {
            default:
                return false;
            case "Forktown":
            case "Forktown_Turf1":
            case "Forktown_Turf2":
            case "Forktown_Turf3":
            case "Forktown_Turf4":
			case "Foktown_DemonKingLair":
                return true;
        }
    }

	public static bool IsGameplayScene (string scene)
	{
		switch (scene)
		{
			default: 
				return true;
			case "Managers":
			case "Staging":
			case "Menu_TitleScreen":
			case "Cutscene_Credits":
			case "Cutscene_DemoTrailer":
			case "Cutscene_Intro":
			case "Cutscene_Splash":
			case "Cutscene_DKUnlock":
				return false;
		}
	}
    
    public static bool IsArenaLevel (LevelID levelId) => levelId >= LevelID.Quest_Arena1 && levelId < LevelID.Quest_DemonSoccer1;
    public static bool IsGolfLevel (LevelID levelId) => levelId >= LevelID.Quest_DemonSoccer1 && levelId < LevelID.Quest_MiniChallenge1;
    public static bool IsMiniChallengeLevel (LevelID levelID) => levelID >= LevelID.Quest_MiniChallenge1 && levelID < LevelID.DemonKing_Level;
	public static bool IsArcadeLevel (LevelID levelID) => levelID >= LevelID.Arcades_1 && levelID <= LevelID.Arcades_3;

	public static string GetBossName (LevelID levelID)
	{
		switch (levelID)
		{
			default:
				return "BOSS NAME NOT SET";
			case LevelID.Apocadesert_Boss:
				return "Fredo";
			case LevelID.Armadageddon_Boss:
				return "Capn Damp";
			case LevelID.NewNeoCity_Boss:
				return "Tomoe";
			case LevelID.PeakPlateau_Boss:
				return "Gol and Lem";
			case LevelID.DemonKing_Boss:
				return "The Demon King";
			
		}
	}
	public static bool IsBossLevel (LevelID levelID) => levelID == LevelID.Apocadesert_Boss || levelID == LevelID.Armadageddon_Boss || levelID == LevelID.NewNeoCity_Boss || levelID == LevelID.PeakPlateau_Boss || levelID == LevelID.DemonKing_Boss;
    public static bool IsCheckpointLevel (string levelName) => IsCheckpointLevel (GetLevelID (levelName));
    public static bool IsCheckpointLevel (LevelID levelID) => !IsHubScene (levelID) && !IsGolfLevel (levelID) && !IsArenaLevel (levelID) && !IsMiniChallengeLevel (levelID) && !IsBossLevel (levelID);
	public static bool IsGameplayLevel (LevelID levelID) => !IsHubScene (levelID);
	public static bool IsIntroLevel (LevelID levelID) => levelID == LevelID.Apocadesert_Intro || levelID == LevelID.Armadageddon_Intro || levelID == LevelID.NewNeoCity_Intro || levelID == LevelID.PeakPlateau_Intro || levelID == LevelID.DemonKing_Level;
	public static LevelID GetIntroLevel (TurfID turf)
	{
		switch (turf)
		{
			default: return LevelID.INVALID;
			case TurfID.Apocadesert: 	return LevelID.Apocadesert_Intro;
			case TurfID.Armadageddon: 	return LevelID.Armadageddon_Intro;
			case TurfID.NewNeoCity: 	return LevelID.NewNeoCity_Intro;
			case TurfID.PeakPlateau: 	return LevelID.PeakPlateau_Intro;
			case TurfID.DemonKingLair: 	return LevelID.DemonKing_Level;
			case TurfID.Forktown: 		return LevelID.Tutorial;
			case TurfID.Arcades: 		return LevelID.Arcades_1;
		}
	}
	public static bool IsTurfLevel (LevelID levelID) => LevelUtility.IsCheckpointLevel (levelID) && !LevelUtility.IsArcadeLevel (levelID) && (LevelUtility.GetTurfID (levelID).IsIn (TurfID.Apocadesert, TurfID.Armadageddon, TurfID.NewNeoCity, TurfID.PeakPlateau));
	public static bool IsBatteryLevel (LevelID levelID) => IsTurfLevel (levelID);
	public static bool IsTrophyLevel (LevelID levelID) 
	{
	#if !DT_EXPORT && !DT_MOD
		return RecordsDataManager.Data.GetLevelTrophyTimeStandard (levelID) > 0;
	#else 
		return false;
	#endif
	}
	
	static public bool IsLeaderboardLevel (LevelID level)
	{
#if UNITY_STANDALONE && (!DT_STEAM && !DT_EPIC && !DT_GOG)
		return false;
#endif
		if (level == LevelID.DemonKing_Vault)
			return false;
		return level == LevelID.Tutorial
			|| level == LevelID.DemonKing_Level
			|| LevelUtility.IsTurfLevel (level)
			|| LevelUtility.IsBossLevel (level)
			|| LevelUtility.IsArcadeLevel (level);
	}



#region Scene ID to Scene String
    public static LevelID GetLevelID () => GetLevelID (SceneManager.GetActiveScene ().name);
    public static LevelID GetLevelID (string sceneName)
    {
        switch (sceneName)
        {
            default:
                return LevelID.INVALID;
            
            case "Minimal Test":
                return LevelID.Armadageddon_Intro;

            case "Moveset Recording":
                return LevelID.Armadageddon_Intro;


            // hub
            case "Forktown":
                return LevelID.Forktown;
			case "Forktown_BeebzRoom":
				return LevelID.ForktownAccess_BeebzRoom;
			case "Forktown_TownSq":
				return LevelID.ForktownAccess_TownSq;
			case "Forktown_Underbelly":
				return LevelID.ForktownAccess_Underbelly;
			case "Forktown_Outskirts":
				return LevelID.ForktownAccess_Outskirts;
			case "Forktown_CastleAccess":
				return LevelID.ForktownAccess_CastleAccess;

			case "Forktown_Mods":
				return LevelID.ForktownAccess_Mods;
			case "Forktown_Dyes":
				return LevelID.ForktownAccess_Dyes;
			case "Forktown_Pets":
				return LevelID.ForktownAccess_Pets;
			case "Forktown_DSG":
				return LevelID.ForktownAccess_DSG;
			case "Forktown_Arena":
				return LevelID.ForktownAccess_Arena;
			case "Forktown_Trials":
				return LevelID.ForktownAccess_Trials;
			case "Forktown_Arcade":
				return LevelID.ForktownAccess_Arcade;
			case "Forktown_PhotoGallery":
				return LevelID.ForktownAccess_PhotoGallery;
				
            case "Tutorial_Level":
                return LevelID.Tutorial;
            
            // turf 1
            case "Forktown_Turf1":
                return LevelID.Forktown_Turf1;
            case "Apocadesert_Level_Intro":
                return LevelID.Apocadesert_Intro;
            case "Apocadesert_Level_ArenaCity":
                return LevelID.Apocadesert_ArenaCity;
            case "Apocadesert_Level_BurgStash":
                return LevelID.Apocadesert_BurgStash;
            case "Apocadesert_Level_CaveDwellings":
                return LevelID.Apocadesert_CaveDwellings;
            case "Apocadesert_Level_FlowerGarden":
                return LevelID.Apocadesert_FlowerGarden;
            case "Apocadesert_Level_GrandCanyon":
                return LevelID.Apocadesert_GrandCanyon;
            case "Apocadesert_Level_Ravine":
                return LevelID.Apocadesert_Ravine;
            case "Apocadesert_Level_Boss":
                return LevelID.Apocadesert_Boss;

            // turf 2
            case "Forktown_Turf2":
                return LevelID.Forktown_Turf2;
            case "Armadageddon_Level_Intro":
                return LevelID.Armadageddon_Intro;
            case "Armadageddon_Level_Bay":
                return LevelID.Armadageddon_Bay;
            case "Armadageddon_Level_PortTown":
                return LevelID.Armadageddon_PortTown;
            case "Armadageddon_Level_Sewer":
                return LevelID.Armadageddon_Sewer;
            case "Armadageddon_Level_Tides":
                return LevelID.Armadageddon_Tides;
            case "Armadageddon_Level_Volcano":
                return LevelID.Armadageddon_Volcano;
            case "Armadageddon_Level_Waterfall":
                return LevelID.Armadageddon_Waterfall;
            case "Armadageddon_Level_Boss":
                return LevelID.Armadageddon_Boss;

            // turf 3
            case "Forktown_Turf3":
                return LevelID.Forktown_Turf3;
            case "NewNeoCity_Level_Intro":
                return LevelID.NewNeoCity_Intro;
            case "NewNeoCity_Level_Subway":
                return LevelID.NewNeoCity_Subway;  
            case "NewNeoCity_Level_Rooftops":
                return LevelID.NewNeoCity_BuildingTop;  
            case "NewNeoCity_Level_Construction":
                return LevelID.NewNeoCity_Construction;  
            case "NewNeoCity_Level_Highway":
                return LevelID.NewNeoCity_Highway;  
            case "NewNeoCity_Level_Shopping":
                return LevelID.NewNeoCity_Shopping;  
            case "NewNeoCity_Level_Skyscraper":
                return LevelID.NewNeoCity_Skyscraper;  
            case "NewNeoCity_Level_Boss":
                return LevelID.NewNeoCity_Boss;   

            // turf 4
            case "Forktown_Turf4":
                return LevelID.Forktown_Turf4;
            case "PeakPlateau_Level_Intro":
                return LevelID.PeakPlateau_Intro;
            case "PeakPlateau_Level_SnowyWoodlands":
                return LevelID.PeakPlateau_Woodlands;  
            case "PeakPlateau_Level_CliffTemples":
                return LevelID.PeakPlateau_CliffTemples;  
            case "PeakPlateau_Level_HangingCeiling":
                return LevelID.PeakPlateau_HangingCeiling;  
            case "PeakPlateau_Level_IceCave":
                return LevelID.PeakPlateau_IceCave;  
            case "PeakPlateau_Level_AncientRuins":
                return LevelID.PeakPlateau_AncientRuins;  
            case "PeakPlateau_Level_SkiPiste":
                return LevelID.PeakPlateau_SkiPiste;  
            case "PeakPlateau_Level_Boss":
                return LevelID.PeakPlateau_Boss;   

			// Demon King Lair
			case "Forktown_DemonKingLair":
				return LevelID.Forktown_DemonKingLair;
            case "DemonKing_Level":
                return LevelID.DemonKing_Level;  
            case "DemonKing_Boss":
                return LevelID.DemonKing_Boss;  
			case "DemonKing_Vault":
                return LevelID.DemonKing_Vault;  

            // arcade
            case "Arcades_Level_1":
                return LevelID.Arcades_1;
            case "Arcades_Level_2":
                return LevelID.Arcades_2;
            case "Arcades_Level_7":
                return LevelID.Arcades_7;
            case "Arcades_Level_8":
                return LevelID.Arcades_8;
            case "Arcades_Level_4":
                return LevelID.Arcades_4;
            case "Arcades_Level_6":
                return LevelID.Arcades_6;
            case "Arcades_Level_5":
                return LevelID.Arcades_5;
            case "Arcades_Level_3":
                return LevelID.Arcades_3;

            // Arena
            case "Quest_Arena":
            case "Quest_Arena_1":
                return LevelID.Quest_Arena1;
			case "Quest_Arena_2":
                return LevelID.Quest_Arena2;
			case "Quest_Arena_3":
                return LevelID.Quest_Arena3;
			case "Quest_Arena_4":
                return LevelID.Quest_Arena4;
			case "Quest_Arena_5":
                return LevelID.Quest_Arena5;
			case "Quest_Arena_6":
                return LevelID.Quest_Arena6;
			case "Quest_Arena_7":
                return LevelID.Quest_Arena7;
			case "Quest_Arena_8":
                return LevelID.Quest_Arena8;
			case "Quest_Arena_9":
                return LevelID.Quest_Arena9;
			case "Quest_Arena_10":
                return LevelID.Quest_Arena10;
			case "Quest_Arena_11":
                return LevelID.Quest_Arena11;
			case "Quest_Arena_12":
                return LevelID.Quest_Arena12;
			case "Quest_Arena_13":
                return LevelID.Quest_Arena13;
			case "Quest_Arena_14":
                return LevelID.Quest_Arena14;
			case "Quest_Arena_15":
                return LevelID.Quest_Arena15;
			case "Quest_Arena_16":
                return LevelID.Quest_Arena16;
			case "Quest_Arena_17":
                return LevelID.Quest_Arena17;
			case "Quest_Arena_18":
                return LevelID.Quest_Arena18;
			case "Quest_Arena_19":
                return LevelID.Quest_Arena19;
			case "Quest_Arena_20":
                return LevelID.Quest_Arena20;
			case "Quest_Arena_21":
                return LevelID.Quest_Arena21;
			case "Quest_Arena_22":
                return LevelID.Quest_Arena22;
			case "Quest_Arena_23":
                return LevelID.Quest_Arena23;
			case "Quest_Arena_24":
                return LevelID.Quest_Arena24;
			case "Quest_Arena_25":
                return LevelID.Quest_Arena25;

            // Demon Golf
            case "Quest_DemonSoccerGolf_1":
                return LevelID.Quest_DemonSoccer1;
            case "Quest_DemonSoccerGolf_2":
                return LevelID.Quest_DemonSoccer2;
            case "Quest_DemonSoccerGolf_3":
                return LevelID.Quest_DemonSoccer3;
            case "Quest_DemonSoccerGolf_4":
                return LevelID.Quest_DemonSoccer4;
            case "Quest_DemonSoccerGolf_5":
                return LevelID.Quest_DemonSoccer5;
			case "Quest_DemonSoccerGolf_6":
                return LevelID.Quest_DemonSoccer6;
			case "Quest_DemonSoccerGolf_7":
                return LevelID.Quest_DemonSoccer7;
			case "Quest_DemonSoccerGolf_8":
                return LevelID.Quest_DemonSoccer8;
			case "Quest_DemonSoccerGolf_9":
                return LevelID.Quest_DemonSoccer9;
			case "Quest_DemonSoccerGolf_10":
                return LevelID.Quest_DemonSoccer10;

            case "Quest_MiniChallenge_1":
                return LevelID.Quest_MiniChallenge1;
            case "Quest_MiniChallenge_2":
                return LevelID.Quest_MiniChallenge2;
            case "Quest_MiniChallenge_3":
                return LevelID.Quest_MiniChallenge3;
            case "Quest_MiniChallenge_4":
                return LevelID.Quest_MiniChallenge4;
            case "Quest_MiniChallenge_5":
                return LevelID.Quest_MiniChallenge5;
            case "Quest_MiniChallenge_6":
                return LevelID.Quest_MiniChallenge6;
            case "Quest_MiniChallenge_7":
                return LevelID.Quest_MiniChallenge7;
            case "Quest_MiniChallenge_8":
                return LevelID.Quest_MiniChallenge8;
            case "Quest_MiniChallenge_9":
                return LevelID.Quest_MiniChallenge9;
            case "Quest_MiniChallenge_10":
                return LevelID.Quest_MiniChallenge10;
            case "Quest_MiniChallenge_11":
                return LevelID.Quest_MiniChallenge11;
            case "Quest_MiniChallenge_12":
                return LevelID.Quest_MiniChallenge12;
            case "Quest_MiniChallenge_13":
                return LevelID.Quest_MiniChallenge13;
            case "Quest_MiniChallenge_14":
                return LevelID.Quest_MiniChallenge14;
            case "Quest_MiniChallenge_15":
                return LevelID.Quest_MiniChallenge15;
            case "Quest_MiniChallenge_16":
                return LevelID.Quest_MiniChallenge16;
            case "Quest_MiniChallenge_17":
                return LevelID.Quest_MiniChallenge17;
			case "Quest_MiniChallenge_18":
                return LevelID.Quest_MiniChallenge18;
			case "Quest_MiniChallenge_19":
                return LevelID.Quest_MiniChallenge19;
			case "Quest_MiniChallenge_20":
                return LevelID.Quest_MiniChallenge20;
			case "Quest_MiniChallenge_21":
                return LevelID.Quest_MiniChallenge21;
			case "Quest_MiniChallenge_22":
                return LevelID.Quest_MiniChallenge22;
			case "Quest_MiniChallenge_23":
                return LevelID.Quest_MiniChallenge23;
			case "Quest_MiniChallenge_24":
                return LevelID.Quest_MiniChallenge24;
			case "Quest_MiniChallenge_25":
                return LevelID.Quest_MiniChallenge25;
			case "Quest_MiniChallenge_26":
                return LevelID.Quest_MiniChallenge26;
			case "Quest_MiniChallenge_27":
                return LevelID.Quest_MiniChallenge27;
			case "Quest_MiniChallenge_28":
                return LevelID.Quest_MiniChallenge28;
			case "Quest_MiniChallenge_29":
                return LevelID.Quest_MiniChallenge29;
			case "Quest_MiniChallenge_30":
                return LevelID.Quest_MiniChallenge30;

			case "Trials_TestRoom":
				return LevelID.Trials_Playground;
        }
    }
	public static string GetTurfLabel (TurfID turf)
	{
		switch (turf)
		{
			default:
				return "INVALID";
			case TurfID.Forktown:
			case TurfID.Arcades:
				return "Forktown";
			case TurfID.Apocadesert:
				return "Apocadesert";
			case TurfID.Armadageddon:
				return "Armadageddon";
			case TurfID.NewNeoCity:
				return "New Neo City";
			case TurfID.PeakPlateau:
				return "Peak Plateau";
			case TurfID.DemonKingLair:
				return "Demon King's Lair";
		}
	}
	public static string GetStateLabel (LevelState state) 
	{
		switch (state)
		{
			default:
			case LevelState.FirstTrip:      return "First Trip";
			case LevelState.ReturnTrip:     return "Return Trip";
			case LevelState.NewGamePlus:    return "New Game Plus";

		}
	}
    public static TurfID GetTurfID () => GetTurfID (SceneManager.GetActiveScene ().name);
    public static TurfID GetTurfID (string sceneName) => GetTurfID (GetLevelID (sceneName));
    public static TurfID GetTurfID (LevelID levelID)
    {
        switch (levelID)
        {
            default:
                // return TurfID.INVALID;

            case LevelID.Forktown:
            case LevelID.Tutorial:
			// case LevelID.forkto
                return TurfID.Forktown;
            
            case LevelID.Forktown_Turf1:
            case LevelID.Apocadesert_Intro:
            case LevelID.Apocadesert_ArenaCity:
            case LevelID.Apocadesert_BurgStash:
            case LevelID.Apocadesert_CaveDwellings:
            case LevelID.Apocadesert_FlowerGarden:
            case LevelID.Apocadesert_GrandCanyon:
            case LevelID.Apocadesert_Ravine:
            case LevelID.Apocadesert_Boss:
                return TurfID.Apocadesert;
            
            case LevelID.Forktown_Turf2:
            case LevelID.Armadageddon_Intro:
            case LevelID.Armadageddon_PortTown:
            case LevelID.Armadageddon_Bay:
            case LevelID.Armadageddon_Volcano:
            case LevelID.Armadageddon_Sewer:
            case LevelID.Armadageddon_Waterfall:
            case LevelID.Armadageddon_Tides:
            case LevelID.Armadageddon_Boss:
                return TurfID.Armadageddon;

            case LevelID.Forktown_Turf3:
            case LevelID.NewNeoCity_Intro:
            case LevelID.NewNeoCity_Subway:
            case LevelID.NewNeoCity_BuildingTop:
            case LevelID.NewNeoCity_Construction:
            case LevelID.NewNeoCity_Highway:
            case LevelID.NewNeoCity_Shopping:
            case LevelID.NewNeoCity_Skyscraper:
            case LevelID.NewNeoCity_Boss:
                return TurfID.NewNeoCity;

            case LevelID.Forktown_Turf4:
            case LevelID.PeakPlateau_Intro:
            case LevelID.PeakPlateau_Woodlands:
            case LevelID.PeakPlateau_CliffTemples:
            case LevelID.PeakPlateau_HangingCeiling:
            case LevelID.PeakPlateau_IceCave:
            case LevelID.PeakPlateau_AncientRuins:
            case LevelID.PeakPlateau_SkiPiste:
            case LevelID.PeakPlateau_Boss:
                return TurfID.PeakPlateau;

			case LevelID.Forktown_DemonKingLair:
            case LevelID.DemonKing_Level:
            case LevelID.DemonKing_Boss:
			case LevelID.DemonKing_Vault:
                return TurfID.DemonKingLair;
			
			case LevelID.Arcades_1:
			case LevelID.Arcades_2:
			case LevelID.Arcades_3:
			case LevelID.Arcades_4:
			case LevelID.Arcades_5:
			case LevelID.Arcades_6:
			case LevelID.Arcades_7:
			case LevelID.Arcades_8:
				return TurfID.Arcades;
        }
    }
    public static string GetHubDestinationKey (TurfID turfID)
    {
        switch (turfID)
        {
            default:
                return string.Empty;
            case TurfID.Forktown:
                return "ForktownEntry";
            case TurfID.Apocadesert:
                return "Turf1Entry";
            case TurfID.Armadageddon:
                return "Turf2Entry";
            case TurfID.NewNeoCity:
                return "Turf3Entry";
            case TurfID.PeakPlateau:
                return "Turf4Entry";
			case TurfID.DemonKingLair:
				return "DemonKingLairEntry";
        }
    }
    public static string GetHubSceneName (LevelID levelID) => GetHubSceneName (GetTurfID (levelID));
    public static string GetHubSceneName (TurfID turfID) 
    {
        switch (turfID)
        {
            default:
                return "Forktown";
            case TurfID.Apocadesert:
                return "Forktown_Turf1";
            case TurfID.Armadageddon:
                return "Forktown_Turf2";
            case TurfID.NewNeoCity:
                return "Forktown_Turf3";
            case TurfID.PeakPlateau:
                return "Forktown_Turf4";
			case TurfID.DemonKingLair:
				return "Forktown_DemonKingLair";
        }
    }
    public static string GetLevelSceneName (LevelID levelID)
    {
        switch (levelID)
        {
            default:
                return "NEED SCENE NAME";
            
			case LevelID.Tutorial:
				return "Tutorial_Level";
			case LevelID.Forktown:
			case LevelID.ForktownAccess_BeebzRoom:
				return "Forktown_BeebzRoom";
			case LevelID.ForktownAccess_TownSq:
				return "Forktown_TownSq";
			case LevelID.ForktownAccess_Underbelly:
				return "Forktown_Underbelly";
			case LevelID.ForktownAccess_Outskirts:
				return "Forktown_Outskirts";
			case LevelID.ForktownAccess_CastleAccess:
				return "Forktown_CastleAccess";
			
			case LevelID.ForktownAccess_Mods:
				return "Forktown_Mods";
			case LevelID.ForktownAccess_Dyes:
				return "Forktown_Dyes";
			case LevelID.ForktownAccess_Pets:
				return "Forktown_Pets";
			case LevelID.ForktownAccess_DSG:
				return "Forktown_DSG";
			case LevelID.ForktownAccess_Arena:
				return "Forktown_Arena";
			case LevelID.ForktownAccess_Trials:
				return "Forktown_Trials";
			case LevelID.ForktownAccess_Arcade:
				return "Forktown_Arcade";
			case LevelID.ForktownAccess_PhotoGallery:
				return "Forktown_PhotoGallery";

            case LevelID.Forktown_Turf1:
                return "Forktown_Turf1";
            case LevelID.Apocadesert_Intro:
                return "Apocadesert_Level_Intro";
            case LevelID.Apocadesert_ArenaCity:
                return "Apocadesert_Level_ArenaCity";
            case LevelID.Apocadesert_BurgStash:
                return "Apocadesert_Level_BurgStash";
            case LevelID.Apocadesert_CaveDwellings:
                return "Apocadesert_Level_CaveDwellings";
            case LevelID.Apocadesert_FlowerGarden:
                return "Apocadesert_Level_FlowerGarden";
            case LevelID.Apocadesert_GrandCanyon:
                return "Apocadesert_Level_GrandCanyon";
            case LevelID.Apocadesert_Ravine:
                return "Apocadesert_Level_Ravine";
            case LevelID.Apocadesert_Boss:
                return "Apocadesert_Level_Boss";

            case LevelID.Forktown_Turf2:
                return "Forktown_Turf2";
            case LevelID.Armadageddon_Intro:
                return "Armadageddon_Level_Intro";
            case LevelID.Armadageddon_Bay:
                return "Armadageddon_Level_Bay";
            case LevelID.Armadageddon_PortTown:
                return "Armadageddon_Level_PortTown";
            case LevelID.Armadageddon_Sewer:
                return "Armadageddon_Level_Sewer";
            case LevelID.Armadageddon_Tides:
                return "Armadageddon_Level_Tides";
            case LevelID.Armadageddon_Volcano:
                return "Armadageddon_Level_Volcano";
            case LevelID.Armadageddon_Waterfall:
                return "Armadageddon_Level_Waterfall";
            case LevelID.Armadageddon_Boss:
                return "Armadageddon_Level_Boss";
                
            case LevelID.Forktown_Turf3:
                return "Forktown_Turf3";
            case LevelID.NewNeoCity_Intro:
                return "NewNeoCity_Level_Intro";
            case LevelID.NewNeoCity_Subway:
                return "NewNeoCity_Level_Subway";
            case LevelID.NewNeoCity_BuildingTop:
                return "NewNeoCity_Level_Rooftops";
            case LevelID.NewNeoCity_Construction:
                return "NewNeoCity_Level_Construction";
            case LevelID.NewNeoCity_Highway:
                return "NewNeoCity_Level_Highway";
            case LevelID.NewNeoCity_Shopping:
                return "NewNeoCity_Level_Shopping";
            case LevelID.NewNeoCity_Skyscraper:
                return "NewNeoCity_Level_Skyscraper";
            case LevelID.NewNeoCity_Boss:
                return "NewNeoCity_Level_Boss";

            case LevelID.Forktown_Turf4:
                return "Forktown_Turf4";
            case LevelID.PeakPlateau_Intro:
                return "PeakPlateau_Level_Intro";
            case LevelID.PeakPlateau_Woodlands:
                return "PeakPlateau_Level_SnowyWoodlands";
            case LevelID.PeakPlateau_CliffTemples:
                return "PeakPlateau_Level_CliffTemples";
            case LevelID.PeakPlateau_HangingCeiling:
                return "PeakPlateau_Level_HangingCeiling";
            case LevelID.PeakPlateau_IceCave:
                return "PeakPlateau_Level_IceCave";
            case LevelID.PeakPlateau_AncientRuins:
                return "PeakPlateau_Level_AncientRuins";
            case LevelID.PeakPlateau_SkiPiste:
                return "PeakPlateau_Level_SkiPiste";
            case LevelID.PeakPlateau_Boss:
                return "PeakPlateau_Level_Boss";

			case LevelID.Arcades_1:
                return "Arcades_Level_1";
            case  LevelID.Arcades_2:
                return "Arcades_Level_2";
            case  LevelID.Arcades_7:
                return "Arcades_Level_7";
            case  LevelID.Arcades_8:
                return "Arcades_Level_8";
            case  LevelID.Arcades_4:
                return "Arcades_Level_4";
            case  LevelID.Arcades_6:
                return "Arcades_Level_6";
            case  LevelID.Arcades_5:
                return "Arcades_Level_5";
            case  LevelID.Arcades_3:
                return "Arcades_Level_3";

			case LevelID.Forktown_DemonKingLair:
				return "Forktown_DemonKingLair";
            case LevelID.DemonKing_Level:
                return "DemonKing_Level";
            case LevelID.DemonKing_Boss:
                return "DemonKing_Boss";
			case LevelID.DemonKing_Vault:
				return "DemonKing_Vault";

			case LevelID.Quest_Arena1:
			case LevelID.Quest_Arena2:
			case LevelID.Quest_Arena3:
			case LevelID.Quest_Arena4:
			case LevelID.Quest_Arena5:
			case LevelID.Quest_Arena6:
			case LevelID.Quest_Arena7:
			case LevelID.Quest_Arena8:
			case LevelID.Quest_Arena9:
			case LevelID.Quest_Arena10:
			case LevelID.Quest_Arena11:
			case LevelID.Quest_Arena12:
			case LevelID.Quest_Arena13:
			case LevelID.Quest_Arena14:
			case LevelID.Quest_Arena15:
			case LevelID.Quest_Arena16:
			case LevelID.Quest_Arena17:
			case LevelID.Quest_Arena18:
			case LevelID.Quest_Arena19:
			case LevelID.Quest_Arena20:
			case LevelID.Quest_Arena21:
			case LevelID.Quest_Arena22:
			case LevelID.Quest_Arena23:
			case LevelID.Quest_Arena24:
			case LevelID.Quest_Arena25:
				return "Quest_Arena";
        }
    }
    
    public static LevelID GetHubLevelID (TurfID turfID) 
    {
        switch (turfID)
        {
            default:
            case TurfID.Forktown:
                return LevelID.Forktown;
            case TurfID.Apocadesert:
                return LevelID.Forktown_Turf1;
            case TurfID.Armadageddon:
                return LevelID.Forktown_Turf2;
            case TurfID.NewNeoCity:
                return LevelID.Forktown_Turf3;
            case TurfID.PeakPlateau:
                return LevelID.Forktown_Turf4;
			case TurfID.DemonKingLair:
				return LevelID.Forktown_DemonKingLair;
        }
    }
    
    public static bool GetLevelVisited (LevelID level)
    {
#if DT_MOD
		return false;
#else 
        if (Cheat_UnlockLevels.Enabled)
            return true;

        switch (level)
        {
            default:
				return GameDataManager.Instance.Data.GetLevelData (level).Visited;
            case LevelID.Forktown:
			case LevelID.ForktownAccess_BeebzRoom:
			case LevelID.ForktownAccess_TownSq:
			case LevelID.ForktownAccess_Underbelly:
			case LevelID.ForktownAccess_Outskirts:
			case LevelID.ForktownAccess_CastleAccess:
			case LevelID.ForktownAccess_Mods:
			case LevelID.ForktownAccess_Dyes:
			case LevelID.ForktownAccess_Pets:
			case LevelID.ForktownAccess_DSG:
			case LevelID.ForktownAccess_Arena:
			case LevelID.ForktownAccess_Trials:
			case LevelID.ForktownAccess_Arcade:
			case LevelID.ForktownAccess_PhotoGallery:
                return GameDataManager.Instance.Data.GetTurfEvent (ForktownHubManager.HubEvent.TutorialCompleted);
            case LevelID.Forktown_Turf1:
                return GameDataManager.Instance.Data.GetTurfEvent (Turf1HubManager.HubEvent.HubVisited);
            case LevelID.Forktown_Turf2:
                return GameDataManager.Instance.Data.GetTurfEvent (Turf2HubManager.HubEvent.HubVisited);
            case LevelID.Forktown_Turf3:
                return GameDataManager.Instance.Data.GetTurfEvent (Turf3HubManager.HubEvent.HubVisited);
            case LevelID.Forktown_Turf4:
                return GameDataManager.Instance.Data.GetTurfEvent (Turf4HubManager.HubEvent.HubVisited);
			case LevelID.Forktown_DemonKingLair:
				return GameDataManager.Instance.Data.GetTurfEvent (ForktownHubManager.HubEvent.DemonKingLair_LuciCutscene);
        }
#endif
    }
	public static ArenaType GetArenaID (LevelID levelID)
	{
		switch (levelID)
		{
			default:
				return ArenaType.INVALID;
			case LevelID.Quest_Arena1:
				return ArenaType.Desert1;
			case LevelID.Quest_Arena2:
				return ArenaType.Desert2;
			case LevelID.Quest_Arena3:
				return ArenaType.Desert3;
			case LevelID.Quest_Arena4:
				return ArenaType.Desert4;
			case LevelID.Quest_Arena5:
				return ArenaType.Desert5;
			case LevelID.Quest_Arena6:
				return ArenaType.Ocean1;
			case LevelID.Quest_Arena7:
				return ArenaType.Ocean2;
			case LevelID.Quest_Arena8:
				return ArenaType.Ocean3;
			case LevelID.Quest_Arena9:
				return ArenaType.Ocean4;
			case LevelID.Quest_Arena10:
				return ArenaType.Ocean5;
			case LevelID.Quest_Arena11:
				return ArenaType.City1;
			case LevelID.Quest_Arena12:
				return ArenaType.City2;
			case LevelID.Quest_Arena13:
				return ArenaType.City3;
			case LevelID.Quest_Arena14:
				return ArenaType.City4;
			case LevelID.Quest_Arena15:
				return ArenaType.City5;
			case LevelID.Quest_Arena16:
				return ArenaType.Snow1;
			case LevelID.Quest_Arena17:
				return ArenaType.Snow2;
			case LevelID.Quest_Arena18:
				return ArenaType.Snow3;
			case LevelID.Quest_Arena19:
				return ArenaType.Snow4;
			case LevelID.Quest_Arena20:
				return ArenaType.Snow5;
			case LevelID.Quest_Arena21:
				return ArenaType.International1;
			case LevelID.Quest_Arena22:
				return ArenaType.International2;
			case LevelID.Quest_Arena23:
				return ArenaType.International3;
			case LevelID.Quest_Arena24:
				return ArenaType.International4;
			case LevelID.Quest_Arena25:
				return ArenaType.International5;
		}
	}
	public static DemonGolfType GetDemonGolfType (LevelID levelID)
	{
		switch (levelID)
		{
			default:
				return DemonGolfType.INVALID;
			case LevelID.Quest_DemonSoccer1:
				return DemonGolfType.Level1;
			case LevelID.Quest_DemonSoccer2:
				return DemonGolfType.Level2;
			case LevelID.Quest_DemonSoccer3:
				return DemonGolfType.Level3;
			case LevelID.Quest_DemonSoccer4:
				return DemonGolfType.Level4;
			case LevelID.Quest_DemonSoccer5:
				return DemonGolfType.Level5;
			case LevelID.Quest_DemonSoccer6:
				return DemonGolfType.Level6;
			case LevelID.Quest_DemonSoccer7:
				return DemonGolfType.Level7;
			case LevelID.Quest_DemonSoccer8:
				return DemonGolfType.Level8;
			case LevelID.Quest_DemonSoccer9:
				return DemonGolfType.Level9;
			case LevelID.Quest_DemonSoccer10:
				return DemonGolfType.Level10;
		}
	}
	public static MiniChallengeType GetMiniChallengeID (LevelID levelID)
	{
		switch (levelID)
		{
			default:
				return MiniChallengeType.INVALID;
			case LevelID.Quest_MiniChallenge1:
				return MiniChallengeType.Challenge1;
			case LevelID.Quest_MiniChallenge2:
				return MiniChallengeType.Challenge2;
			case LevelID.Quest_MiniChallenge3:
				return MiniChallengeType.Challenge3;
			case LevelID.Quest_MiniChallenge4:
				return MiniChallengeType.Challenge4;
			case LevelID.Quest_MiniChallenge5:
				return MiniChallengeType.Challenge5;
			case LevelID.Quest_MiniChallenge6:
				return MiniChallengeType.Challenge6;
			case LevelID.Quest_MiniChallenge7:
				return MiniChallengeType.Challenge7;
			case LevelID.Quest_MiniChallenge8:
				return MiniChallengeType.Challenge8;
			case LevelID.Quest_MiniChallenge9:
				return MiniChallengeType.Challenge9;
			case LevelID.Quest_MiniChallenge10:
				return MiniChallengeType.Challenge10;
			case LevelID.Quest_MiniChallenge11:
				return MiniChallengeType.Challenge11;
			case LevelID.Quest_MiniChallenge12:
				return MiniChallengeType.Challenge12;
			case LevelID.Quest_MiniChallenge13:
				return MiniChallengeType.Challenge13;
			case LevelID.Quest_MiniChallenge14:
				return MiniChallengeType.Challenge14;
			case LevelID.Quest_MiniChallenge15:
				return MiniChallengeType.Challenge15;
			case LevelID.Quest_MiniChallenge16:
				return MiniChallengeType.Challenge16;
			case LevelID.Quest_MiniChallenge17:
				return MiniChallengeType.Challenge17;
			case LevelID.Quest_MiniChallenge18:
				return MiniChallengeType.Challenge18;
			case LevelID.Quest_MiniChallenge19:
				return MiniChallengeType.Challenge19;
			case LevelID.Quest_MiniChallenge20:
				return MiniChallengeType.Challenge20;
			case LevelID.Quest_MiniChallenge21:
				return MiniChallengeType.Challenge21;
			case LevelID.Quest_MiniChallenge22:
				return MiniChallengeType.Challenge22;
			case LevelID.Quest_MiniChallenge23:
				return MiniChallengeType.Challenge23;
			case LevelID.Quest_MiniChallenge24:
				return MiniChallengeType.Challenge24;
			case LevelID.Quest_MiniChallenge25:
				return MiniChallengeType.Challenge25;
			case LevelID.Quest_MiniChallenge26:
				return MiniChallengeType.Challenge26;
			case LevelID.Quest_MiniChallenge27:
				return MiniChallengeType.Challenge27;
			case LevelID.Quest_MiniChallenge28:
				return MiniChallengeType.Challenge28;
			case LevelID.Quest_MiniChallenge29:
				return MiniChallengeType.Challenge29;
			case LevelID.Quest_MiniChallenge30:
				return MiniChallengeType.Challenge30;
		}
	}

	public static bool IsCustomLevel () => SceneManager.GetActiveScene ().name == "CustomLevel";
#endregion

    public static string GetEventKey (System.Enum _event) => string.Format ("{0}.{1}", _event.GetType().ToString (), _event.ToString());

    public static void SetLevelState(LevelState state)
    {
        var levelDependents = SceneUtility.FindAllSceneObjectsIncludingDisabled<LevelStateDependent>();
        var levelDependentsCount = levelDependents.Length;
        for (int i = 0; i < levelDependentsCount; i++)
        {
            levelDependents[i].SetActive(state);
        }
    }
    public static void RestoreLevelState()
    {
        var levelDependents = SceneUtility.FindAllSceneObjectsIncludingDisabled<LevelStateDependent>();
        var levelDependentsCount = levelDependents.Length;
        for (int i = 0; i < levelDependentsCount; i++)
        {
            levelDependents[i].gameObject.SetActive(true);
        }
    }
}