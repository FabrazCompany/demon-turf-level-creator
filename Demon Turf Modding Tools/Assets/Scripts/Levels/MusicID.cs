public enum MusicID 
{
	CUSTOM = 0,
	TheDemonQueen,
	DoDemonsDream,
	LetsGetForkedUp,
	LetsHitTheArcades,
	
	PigPen,
	RedSwine,
	LetsGetPorkedUp,
	PigDisco,
	GrapesAndRedSwine,
	
	CrustyParadise,
	BootyCalls,
	LetsCatchSomeWaves,
	CrustyStorms,
	FunkyBooty,

	NeoOwls,
	HighwaySlicker,
	LetsPaintTheTown,
	OwlCyberlords,
	HighwayPunk,
	
	GhastlyGolems,
	HotNColdBeats,
	LetsChillOut,
	GhastlyGolemGoesBrrr,
	HotNColdSweets,
	
	ADemonsClimb,
	// TheKingsMagnumOpus,
	LetsBeatTheArcadez,
	BeebzTheSlaya,
}