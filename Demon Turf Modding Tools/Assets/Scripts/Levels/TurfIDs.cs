public enum TurfID
{
    Forktown = 5,
    Apocadesert = 10,
    Armadageddon = 15,
    NewNeoCity = 20,
    PeakPlateau = 25,
    DemonKingLair = 30,
	Arcades = 35,
}