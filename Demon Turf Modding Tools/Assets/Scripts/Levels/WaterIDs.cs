public enum WaterID 
{
	Arcades_Water,
	Apocadesert_Water,
	Armadageddon_StillWater,
	NewNeoCity_Water,
	PeakPlateau_IcyWater,
	PeakPlateau_HotspringWater,
	PoisonWater,
}