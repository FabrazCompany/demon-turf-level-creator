using UnityEngine;
using Unity.EditorCoroutines.Editor;

using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;
using UnityEditor.AddressableAssets.Settings.GroupSchemas;
using UnityEngine.ResourceManagement.Util;

using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

using Steamworks;

[InitializeOnLoad]
public static class CustomLevelUploader
{
	static bool steamEverInitialized = false;
	static bool initialized;
	static bool runCallbacks;

	static CustomLevelUploader ()
	{
		EditorApplication.update += Update;
	}

	[MenuItem ("Fabraz Tools/Create Level")]
	public static void CreateLevel ()
	{
		var currentScene = EditorSceneManager.GetActiveScene ();
		if (currentScene.isDirty)
		{
			var requestSaveResponse = EditorSceneManager.SaveModifiedScenesIfUserWantsTo (new UnityEngine.SceneManagement.Scene[] { currentScene });
			if (!requestSaveResponse || currentScene.isDirty)
			{
				EditorUtility.DisplayDialog ("Save Error", "The scene has unsaved changes, please save before continuing!", "Sure Midgi!");
				return;
			}
		}
		var scene = EditorSceneManager.NewScene (NewSceneSetup.EmptyScene, NewSceneMode.Single);
		EditorSceneManager.SaveScene (scene);

		var managers = new GameObject ("Managers");

		var modCorePrefabGUIs = AssetDatabase.FindAssets ("l:ModCore t:prefab");
		foreach (var prefabGUIs in modCorePrefabGUIs)
		{
			Debug.Log (prefabGUIs);
			var prefab = AssetDatabase.LoadAssetAtPath<GameObject> (AssetDatabase.GUIDToAssetPath (prefabGUIs));
			PrefabUtility.InstantiatePrefab (prefab, managers.transform);
		}

		var levelEnvironment = new GameObject ("Level Environment");
		levelEnvironment.AddComponent<Notes> ((Notes note) => note.QuickNotes = "All of the level components sit in here, you can organize or change the hierarchy however you like though.");
		
		var platforms = new GameObject ("Platforms");
		platforms.AddComponent<Notes> ((Notes note) => note.QuickNotes = "Any collidable geometry needs to be on Platform layer. Add NoClipPlatform tag if camera should not collider with it. You can import your own meshes or use ProBuilder!");
		platforms.transform.SetParent (levelEnvironment.transform);

		var decorations = new GameObject ("Decorations");
		decorations.AddComponent<Notes> ((Notes note) => note.QuickNotes = "Decorations are prefabs you can place to add nice visuals! You can also import your own.");
		decorations.transform.SetParent (levelEnvironment.transform);

		var interactables = new GameObject ("Interactables");
		interactables.AddComponent<Notes> ((Notes note) => note.QuickNotes = "Interactables are prefabs that aren't nethal but add mechanics to a level like moving platforms!");
		interactables.transform.SetParent (levelEnvironment.transform);

		var hazards = new GameObject ("Hazards");
		hazards.AddComponent<Notes> ((Notes note) => note.QuickNotes = "Hazards are prefabs that kill the player, there are several types to use.");
		hazards.transform.SetParent (levelEnvironment.transform);

		var enemies = new GameObject ("Enemies");
		enemies.AddComponent<Notes> ((Notes note) => note.QuickNotes = "Enemy prefabs have a collection of hostile enemies with different types of logic.");
		enemies.transform.SetParent (levelEnvironment.transform);

		EditorSceneManager.MarkSceneDirty (scene);
		EditorSceneManager.SaveScene (scene);
	}	
	
	[MenuItem ("Fabraz Tools/Build and upload current level")]
	public static void UploadBuild ()
	{
		if (uploadRoutine != null)
		{
			EditorCoroutineUtility.StopCoroutine (uploadRoutine);
			uploadRoutine = null;
		}
		uploadRoutine = EditorCoroutineUtility.StartCoroutineOwnerless (UploadBuildRoutine ());
	}
	static string FormatGroupName (ulong id) => $"{SteamWorkshop.customLevelName}_{id}";
	static string FormatBundleName (ulong id) => $"customscenes_scenes_{SteamWorkshop.customLevelName}_{id}.bundle";
	static string SanitizeString (string text) 
	{
		if (text == null) return text;
		else return Regex.Replace(text, @"[^\u0009\u000A\u000D\u0020-\u007E]", "*");
	}
	static EditorCoroutine uploadRoutine;
	static IEnumerator UploadBuildRoutine ()
	{
		var currentScene = EditorSceneManager.GetActiveScene ();
		var levelGate = currentScene.FindSceneComponent<ModLevel_WinGate> ();
		if (!levelGate)
		{
			EditorUtility.DisplayDialog ("Level Error", "A ModLevel_WinGate component is required in the scene to build!", "Thanks Midgi!");
			Cleanup ();
			yield break;
		}

		var levelPlayer = currentScene.FindSceneComponent<ModLevel_PlayerInitialization> ();
		if (!levelPlayer)
		{
			EditorUtility.DisplayDialog ("Level Error", "A ModLevel_PlayerInitialization component is required in the scene to build!", "Thanks Midgi!");
			Cleanup ();
			yield break;
		}

		var levelInfo = currentScene.FindSceneComponent<ModLevel_LevelInfo> ();
		if (!levelInfo)
		{
			EditorUtility.DisplayDialog ("Level Error", "A ModLevel_LevelInfo component is required in the scene to build!", "Thanks Midgi!");
			Cleanup ();
			yield break;
		}
		else 
		{
			if (string.IsNullOrWhiteSpace (levelInfo.levelName))
			{
				EditorUtility.DisplayDialog ("Level Error", "The ModLevel_LevelInfo component's levelName field is not set. Please set a proper name to build", "Thanks Midgi!");
				Cleanup ();
				yield break;
			}
			if (string.IsNullOrWhiteSpace (levelInfo.levelDescription))
			{
				EditorUtility.DisplayDialog ("Level Error", "The ModLevel_LevelInfo component's levelDescription field is not set. Please set a proper description to build", "Thanks Midgi!");
				Cleanup ();
				yield break;
			}
			if (!levelInfo.levelPreview)
			{
				EditorUtility.DisplayDialog ("Level Error", "The ModLevel_LevelInfo component's levelPreview image is not set. Please set a proper preview image to build", "Thanks Midgi!");
				Cleanup ();
				yield break;
			}
		}
		var imagePath = Path.Combine (Directory.GetParent (Application.dataPath).FullName, AssetDatabase.GetAssetPath (levelInfo.levelPreview));
		var imageFileInfo = new FileInfo (imagePath);
		if (imageFileInfo.Length >= 1000000)
		{
			EditorUtility.DisplayDialog ("Level Error", "Level preview image is over 1MB, please select a smaller image for preview.", "Sure Midgi!");
			Cleanup ();
			yield break;
		}
		
		if (!SteamAPI.IsSteamRunning ())
		{
			EditorUtility.DisplayDialog ("Steam Error", "Steam is not running, please open Steam to continue with the upload process!", "Sure Midgi!");
			Cleanup ();
			yield break;
		}

		if (!steamEverInitialized || !SteamAPI.IsSteamRunning ())
			InitializeSteam ();
		
		runCallbacks = true;
		var currentSceneName = currentScene.name;
		if (currentScene.isDirty)
		{
			var requestSaveResponse = EditorSceneManager.SaveModifiedScenesIfUserWantsTo (new UnityEngine.SceneManagement.Scene[] { currentScene });
			if (!requestSaveResponse || currentScene.isDirty)
			{
				EditorUtility.DisplayDialog ("Save Error", "The scene has unsaved changes, please save before continuing!", "Sure Midgi!");
				Cleanup ();
				yield break;
			}
		}
		
		var workshop = SteamWorkshop.Instance;
		if (levelInfo.steamID == PublishedFileId_t.Invalid.m_PublishedFileId)
		{
			var createRequestReturned = false;
			var createRequestSuccessful = true;
			workshop.CreateWorkshopItem ((bool success, SteamWorkshop.EFileHandleCreateResult createResult, PublishedFileId_t fileID) => 
			{
				createRequestReturned = true;
				createRequestSuccessful = success;
				levelInfo.steamID = fileID.m_PublishedFileId;
				PrefabUtility.RecordPrefabInstancePropertyModifications (levelInfo);
				bool saveSuccess = EditorSceneManager.SaveScene (currentScene);
			});
			while (!createRequestReturned)
			{
				var sin = FzMath.SinPositive (Time.realtimeSinceStartup * 2);
				EditorUtility.DisplayProgressBar ("Creating Workshop ID", "Waiting on response from Steam Workshop, please wait...", sin);
				yield return null;
			}
			EditorUtility.ClearProgressBar ();

			if (!createRequestSuccessful)
			{
				EditorUtility.DisplayDialog ("Workshop Error", "The user account still needs to accept the workshop user agreement", "Thanks Midgi!");
				Cleanup ();
				yield break;
			}
		}

		yield return new WaitWhile (() => AddressableAssetSettingsDefaultObject.Settings == null);
		

		var addressableSettings = AddressableAssetSettingsDefaultObject.Settings;

		var groups = new List<AddressableAssetGroup> (addressableSettings.groups);
		for (int i = 0; i < groups.Count; i++)
		{
			if (!groups[i].name.Contains ("customscenes")) continue;
			addressableSettings.RemoveGroup (groups[i]);
		}

		var defaultGroup = addressableSettings.DefaultGroup;
		var schemaList = new List<AddressableAssetGroupSchema> ();
		var packingSchema = BundledAssetGroupSchema.CreateInstance<BundledAssetGroupSchema> ();
		packingSchema.Compression = BundledAssetGroupSchema.BundleCompressionMode.LZ4;
		packingSchema.IncludeInBuild = true;
		packingSchema.ForceUniqueProvider = false;
		packingSchema.UseAssetBundleCache = false;
		packingSchema.UseAssetBundleCrc = false;
		packingSchema.UseAssetBundleCrcForCachedBundles = false;
		packingSchema.Timeout = 0;
		packingSchema.ChunkedTransfer = false;
		packingSchema.RedirectLimit = -1;
		packingSchema.RetryCount = 0;
		packingSchema.IncludeAddressInCatalog = true;
		packingSchema.IncludeGUIDInCatalog = true;
		packingSchema.IncludeLabelsInCatalog = false;
		packingSchema.InternalIdNamingMode = BundledAssetGroupSchema.AssetNamingMode.FullPath;
		packingSchema.InternalBundleIdMode = BundledAssetGroupSchema.BundleInternalIdMode.GroupGuid;
		packingSchema.AssetBundledCacheClearBehavior = BundledAssetGroupSchema.CacheClearBehavior.ClearWhenSpaceIsNeededInCache;
		packingSchema.BundleMode = BundledAssetGroupSchema.BundlePackingMode.PackTogether;
		packingSchema.BundleNaming = BundledAssetGroupSchema.BundleNamingStyle.NoHash;
		
		var contentUpdateSchema = ContentUpdateGroupSchema.CreateInstance<ContentUpdateGroupSchema> ();
		contentUpdateSchema.StaticContent = false;
		
		schemaList.Add (packingSchema);
		schemaList.Add (contentUpdateSchema);

		var sceneGroup = addressableSettings.CreateGroup ($"customscenes_{levelInfo.steamID}", true, false, true, schemaList);
		var invalidEntry = false;

		var entry = addressableSettings.CreateOrMoveEntry (AssetDatabase.AssetPathToGUID (currentScene.path), sceneGroup);
		entry.SetAddress (FormatGroupName (levelInfo.steamID));
		try 
		{
			AddressableAssetSettings.BuildPlayerContent (out var result);	

			var folder = Path.GetDirectoryName (result.OutputPath);
			var modFolder = Path.Combine (folder, "mod");
			if (Directory.Exists (modFolder))
				Directory.Delete (modFolder, true);
			Directory.CreateDirectory (modFolder);

			foreach (var _result in result.FileRegistry.GetFilePaths ())
			{
				var name = Path.GetFileName (_result);
				if (name == "catalog.json" || name.Contains (".bundle"))
					File.Copy (_result, Path.Combine (modFolder, name));
			}

			addressableSettings.DefaultGroup = defaultGroup;
			addressableSettings.RemoveGroup (sceneGroup);

			var tags = new string[levelInfo.levelTags.Length];
			for (int i = 0; i < levelInfo.levelTags.Length; i++)
				tags[i] = SanitizeString (levelInfo.levelTags[i]);

			var updateSuccessful = workshop.PrepWorkshopUpdate (
				levelInfo.getSteamID,
				Path.GetFullPath (modFolder), 
				SanitizeString (levelInfo.levelName), 
				SanitizeString (levelInfo.levelDescription), 
				tags, 
				Path.GetFullPath (imagePath), 
				levelInfo.levelWorkshopVisibility,
				out var errorStatus);

			if (!updateSuccessful)
			{
				EditorUtility.DisplayDialog ("Workshop Error", $"There was an issue when preparing to update your workshop item. There was an {errorStatus}", "Thanks Midgi!");
				Cleanup ();
				yield break;
			}
			
			var changes = EditorInputDialog.Show ("Patch Notes", "Building level has finished, please describe what's \nchanged in this update.", "Level updates", "Heres the changes Midgi", "Leave 'em blank Midgi");
			changes = SanitizeString (changes);			
			workshop.SubmitWorkshopUpdate (changes, UploadCompleted);

			
		}
		catch (DirectoryNotFoundException directoryException)
		{
			EditorUtility.DisplayDialog ("Build Error", "There's been a DirectoryNotFoundException. This is likely due to various paths used by Unity hitting Window's 260 character limit. Please try closing and moving the project closer to the root directory, any path over 100 characters is likely to trigger this error.", "Thanks Midgi!");

			addressableSettings.DefaultGroup = defaultGroup;
			addressableSettings.RemoveGroup (sceneGroup);
			invalidEntry = true;
			yield break;
		}
		catch (System.Exception e)
		{
			EditorUtility.DisplayDialog ("Build Error", $"There's been a {e.GetType ()} with the following error message: {e.Message}", "Thanks Midgi!");
			addressableSettings.DefaultGroup = defaultGroup;
			addressableSettings.RemoveGroup (sceneGroup);
			invalidEntry = true;
			yield break;
		}
		finally
		{

		}

		if (!invalidEntry)
		{
			var processing = true;
			var startTime = Time.realtimeSinceStartup;
			while (processing)
			{	
				var progress = workshop.GetSubmissionProgress (out var status);
				EditorUtility.DisplayProgressBar ("Submission Progress", status, progress);
				if (progress >= 1)
					processing = false;
				if (status == "Invalid" && Mathf.Abs (startTime - Time.realtimeSinceStartup) > 20)
				{
					break;
				}
				yield return null;
			}
		}
		
		void UploadCompleted (SteamWorkshop.ELevelSubmitResult status)
		{
			Debug.Log ("Upload Completed!");
			var statusMessage = string.Empty;
			switch (status)
			{
				default:
				case SteamWorkshop.ELevelSubmitResult.GenericFailure:
					statusMessage = "There was some kind of error with the upload, please try again.";
					break;
				case SteamWorkshop.ELevelSubmitResult.Success:
					statusMessage = $"The mod has been uploaded successfully!";
					break;
				case SteamWorkshop.ELevelSubmitResult.PreviouslyDeleted:
					statusMessage = $"The mod was previously deleted, please select 'Reset Workshop ID' from the Fabraz Tools menu and try again!";
					break;
				case SteamWorkshop.ELevelSubmitResult.FileNotFound:
					statusMessage = $"The mod was not found on the workshop. Either confirm the workshop page exists and matches the ID in the ModLevel_LevelInfo component, or select 'Reset Workshop ID' from the Fabraz Tools menu and try again!";
					break;
				case SteamWorkshop.ELevelSubmitResult.LocalSaveFailed:
					statusMessage = $"The mod failed to upload when saving the file locally. Please try again!";
					break;
				case SteamWorkshop.ELevelSubmitResult.WorkshopItemCreationFailed:
					statusMessage = $"The mod was unable to be created on the workshop, please try again!";
					break;
				case SteamWorkshop.ELevelSubmitResult.NeedsToAcceptWorkshopAgreement:
					statusMessage = "The current user has not accepted the Workshop agreement, please do so and try again!";
					break;
			}
			runCallbacks = false;
			EditorUtility.DisplayDialog ("Operation Completed!", statusMessage, "Thanks Midgi!");
			Cleanup ();
		}

		void Cleanup ()
		{
			if (SteamWorkshop.IsInitialized)
			{
				SteamWorkshop.Instance.DestroyInstance ();
			}
			EditorUtility.ClearProgressBar ();
		}
	}
	
	static void Update ()
	{
		if (!runCallbacks) return;
		SteamAPI.RunCallbacks ();
	}
	

	static void InitializeSteam ()
	{
		if(steamEverInitialized) {
			// This is almost always an error.
			// The most common case where this happens is when SteamManager gets destroyed because of Application.Quit(),
			// and then some Steamworks code in some other OnDestroy gets called afterwards, creating a new SteamManager.
			// You should never call Steamworks functions in OnDestroy, always prefer OnDisable if possible.
			throw new System.Exception("Tried to Initialize the SteamAPI twice in one session!");
		}
		// transform.SetParent (null);
		// // We want our SteamManager Instance to persist across scenes.
		// DontDestroyOnLoad(gameObject);

		if (!Packsize.Test()) {
			Debug.LogError("[Steamworks.NET] Packsize Test returned false, the wrong version of Steamworks.NET is being run in this platform.");
		}

		if (!DllCheck.Test()) {
			Debug.LogError("[Steamworks.NET] DllCheck Test returned false, One or more of the Steamworks binaries seems to be the wrong version.");
		}

		try {
			// If Steam is not running or the game wasn't started through Steam, SteamAPI_RestartAppIfNecessary starts the
			// Steam client and also launches this game again if the User owns it. This can act as a rudimentary form of DRM.

			// Once you get a Steam AppID assigned by Valve, you need to replace AppId_t.Invalid with it and
			// remove steam_appid.txt from the game depot. eg: "(AppId_t)480" or "new AppId_t(480)".
			// See the Valve documentation for more information: https://partner.steamgames.com/doc/sdk/api#initialization_and_shutdown
			if (SteamAPI.RestartAppIfNecessary(AppId_t.Invalid)) {
				Application.Quit();
				return;
			}
		}
		catch (System.DllNotFoundException e) { // We catch this exception here, as it will be the first occurrence of it.
			Debug.LogError("[Steamworks.NET] Could not load [lib]steam_api.dll/so/dylib. It's likely not in the correct location. Refer to the README for more details.\n" + e);

			Application.Quit();
			return;
		}

		// Initializes the Steamworks API.
		// If this returns false then this indicates one of the following conditions:
		// [*] The Steam client isn't running. A running Steam client is required to provide implementations of the various Steamworks interfaces.
		// [*] The Steam client couldn't determine the App ID of game. If you're running your application from the executable or debugger directly then you must have a [code-inline]steam_appid.txt[/code-inline] in your game directory next to the executable, with your app ID in it and nothing else. Steam will look for this file in the current working directory. If you are running your executable from a different directory you may need to relocate the [code-inline]steam_appid.txt[/code-inline] file.
		// [*] Your application is not running under the same OS user context as the Steam client, such as a different user or administration access level.
		// [*] Ensure that you own a license for the App ID on the currently active Steam account. Your game must show up in your Steam library.
		// [*] Your App ID is not completely set up, i.e. in Release State: Unavailable, or it's missing default packages.
		// Valve's documentation for this is located here:
		// https://partner.steamgames.com/doc/sdk/api#initialization_and_shutdown
		initialized = SteamAPI.Init();
		if (!initialized) {
			Debug.LogError("[Steamworks.NET] SteamAPI_Init() failed. Refer to Valve's documentation or the comment above this line for more information.");
			return;
		}

		steamEverInitialized = true;
	}
}