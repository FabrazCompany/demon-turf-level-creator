using System;
using UnityEditor;
using UnityEngine;
 
public class EditorInputDialog : EditorWindow
{
    public enum SubmissionType
    {
        String,
        NumberULong,
    }
    string  description, inputText;
    ulong inputLong;
    string  okButton, cancelButton;
    bool    initializedPosition = false;
    SubmissionType submissionType;
    Action  onOKButton;
 
    bool    shouldClose = false;
    Vector2 maxScreenPos;
 
    #region OnGUI()
    void OnGUI()
    {
        // Check if Esc/Return have been pressed
        var e = Event.current;
        if( e.type == EventType.KeyDown )
        {
            switch( e.keyCode )
            {
                // Escape pressed
                case KeyCode.Escape:
                    shouldClose = true;
                    e.Use();
                    break;
 
                // Enter pressed
                case KeyCode.Return:
                case KeyCode.KeypadEnter:
                    onOKButton?.Invoke();
                    shouldClose = true;
                    e.Use();
                    break;
            }
        }
 
        if( shouldClose ) {  // Close this dialog
            Close();
        }
 
        // Draw our control
        var rect = EditorGUILayout.BeginVertical();
 
        EditorGUILayout.Space( 10 );
        EditorGUILayout.LabelField( description, GUILayout.Height (30) );
 
        EditorGUILayout.Space( 8 );
        GUI.SetNextControlName( "inText" );
        if (submissionType == SubmissionType.String)
        {
            inputText = EditorGUILayout.TextArea( inputText, GUILayout.Height (50));
        }
        else 
        {
            inputLong = (ulong)EditorGUILayout.LongField ( (long)inputLong, GUILayout.Height (50));
        }
        
        GUI.FocusControl( "inText" );   // Focus text field
        EditorGUILayout.Space( 12 );
 
        // Draw OK / Cancel buttons
        var r = EditorGUILayout.GetControlRect();
        r.width /= 2;
        if( GUI.Button( r, okButton ) ) {
            onOKButton?.Invoke();
            shouldClose = true;
        }
 
        r.x += r.width;
        if( GUI.Button( r, cancelButton ) ) {
            inputText = null;   // Cancel - delete inputText
            inputLong = 0;
            shouldClose = true;
        }
 
        EditorGUILayout.Space( 8 );
        EditorGUILayout.EndVertical();
 
        // Force change size of the window
        if( rect.width != 0 && minSize != rect.size ) {
            minSize = maxSize = rect.size;
        }
 
        // Set dialog position next to mouse position
        if( !initializedPosition && e.type == EventType.Layout )
        {
            initializedPosition = true;
            
            int x = (Screen.currentResolution.width - (int)position.width) / 2;
            int y = (Screen.currentResolution.height - (int)position.height) / 2;

            position = new Rect(x, y, position.width, position.height);
 
            Focus();
        }
    }
    #endregion OnGUI()
 
    #region Show()
    /// <summary>
    /// Returns text player entered, or null if player cancelled the dialog.
    /// </summary>
    /// <param name="title"></param>
    /// <param name="description"></param>
    /// <param name="inputText"></param>
    /// <param name="okButton"></param>
    /// <param name="cancelButton"></param>
    /// <returns></returns>
    public static string Show( string title, string description, string inputText, string okButton = "OK", string cancelButton = "Cancel")
    {
        // Make sure our popup is always inside parent window, and never offscreen
        // So get caller's window size
        var maxPos = GUIUtility.GUIToScreenPoint( new Vector2( Screen.width, Screen.height ) );
 
        string ret = null;
        var window = CreateInstance<EditorInputDialog>();
        window.maxScreenPos = maxPos;
        window.titleContent = new GUIContent( title );
        window.description = description;
        window.inputText = inputText;
        window.inputLong = 0;
        window.okButton = okButton;
        window.cancelButton = cancelButton;
        window.onOKButton += () => ret = window.inputText;
        window.submissionType = SubmissionType.String;
        window.ShowModal();
 
        return ret;
    }

    public static ulong Show( string title, string description, ulong inputText, string okButton = "OK", string cancelButton = "Cancel")
    {
        // Make sure our popup is always inside parent window, and never offscreen
        // So get caller's window size
        var maxPos = GUIUtility.GUIToScreenPoint( new Vector2( Screen.width, Screen.height ) );
 
        ulong ret = 0;
        var window = CreateInstance<EditorInputDialog>();
        window.maxScreenPos = maxPos;
        window.titleContent = new GUIContent( title );
        window.description = description;
        window.inputText = null;
        window.inputLong = inputText;
        window.okButton = okButton;
        window.cancelButton = cancelButton;
        window.onOKButton += () => ret = window.inputLong;
        window.submissionType = SubmissionType.NumberULong;
        window.ShowModal();
 
        return ret;
    }
    #endregion Show()
}