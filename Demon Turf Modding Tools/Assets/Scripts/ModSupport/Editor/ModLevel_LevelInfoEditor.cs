using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor (typeof (ModLevel_LevelInfo))]
public class ModLevel_LevelInfoEditor : Editor
{
	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();

		var steamIDProp = serializedObject.FindProperty ("steamID");
		if (GUILayout.Button ("Manually Assign Workshop ID"))
		{
            var newValue = EditorInputDialog.Show ("Reassign Workshop ID", "Please enter the ID for your workshop item you'd like to associate this level to.", 0);
            if (newValue != 0)
            {
                steamIDProp.longValue = (long)newValue;
                serializedObject.ApplyModifiedProperties ();
            }
    			
		}
		if (steamIDProp.longValue != 0)
		{
			if (GUILayout.Button ("Open Workshop Page"))
			{
                Application.OpenURL ($"https://steamcommunity.com/sharedfiles/filedetails/?id={steamIDProp.longValue}");
			}
			if (GUILayout.Button ("Reset Workshop ID"))
			{
				steamIDProp.longValue = 0;
                serializedObject.ApplyModifiedProperties ();
			}
		}
	}
}