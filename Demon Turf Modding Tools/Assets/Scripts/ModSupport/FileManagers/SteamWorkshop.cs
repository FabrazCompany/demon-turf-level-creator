using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;

#if UNITY_EDITOR
using Unity.EditorCoroutines.Editor;
#endif


/// <summary>
/// This class was created by Vitor Pêgas on 01/01/2017
/// Link to original script: https://answers.unity.com/questions/1282772/steamworksnet-and-steam-workshop.html
///  
///  Updates: Added thumbnail uploading and added a subscribed item update checker
///  Updated by: Savva Madar
/*   Usage: Call SaveToWorkshop with the correct arguments to upload a file and a thumbnail to the steam workshop.
			SaveToWorkshop(string fileName, string fileData, string workshopTitle, string workshopDescription, string[] tags, string imageLoc);
			
			string fileName = the file name it will be saved as. When someone downloads the item this is the file they will download. Extension optional, doesn't need to match local file name if uploading local file.
			string fileData = the file contents.
			string workshopTitle = the title that the item will appear under.
			string workshopDescription = the description of the item.
			string[] tags = an array of strings each representing a tag for the workshop item.
			string imageLoc = the ABSOLUTE, not relative, location of the image file you would like to use as the thumbnail for your item.
			
			Call GetSubscribedItems to fetch a list of items that current user is subscribed to.
			GetSubscribedItems();
			
			Call GetItemContent(int itemID) with the correct argument and only after calling GetSubscribedItems to download an item IF it doesn't exist or needs to be updated.
			int itemID = an integer representing the index location inside subscribedItemList denoting which item needs to be checked. 

*/
/// </summary>
[ExecuteInEditMode]
public class SteamWorkshop : MonoBehaviour
{
	[System.Serializable]
	public class LevelInfo 
	{
		PublishedFileId_t itemID;
		public string levelTitle;
		public string levelDescription;

		public string levelFilePath;
		public string levelCatalogPath;
		public string levelPreviewURL;
		
		public LevelInfo (SteamUGCDetails_t info, string folderPath, string previewURL)
		{
			itemID = info.m_nPublishedFileId;
			levelTitle = info.m_rgchTitle;
			levelDescription = info.m_rgchDescription;

			levelFilePath = Path.Combine (folderPath, "customscenes_scenes_customlevel.bundle");
			levelCatalogPath = Path.Combine (folderPath, "catalog.json");
			levelPreviewURL = previewURL;
		}
	}
	public enum EFileHandleCreateResult {
        GenericFailure = 0,
        Success = 1,
        NeedsToAcceptWorkshopAgreement = 2
    }
	public enum ELevelSubmitResult {
        GenericFailure,
        Success,
		FileNotFound,
        PreviouslyDeleted,
        LocalSaveFailed,
        WorkshopItemCreationFailed,
        NeedsToAcceptWorkshopAgreement,
    }

	static SteamWorkshop instance; 
	public static SteamWorkshop Instance 
	{
		get 
		{
			if (instance == null)
			{
				instance = new GameObject ().AddComponent<SteamWorkshop> ();
			}
			return instance;
		}
	}
	public static bool IsInitialized => instance != null;
	public const string customLevelName = "customlevelcontent"; 


	void Awake() {
		if (instance != null && instance != this)
		{
			Destroy (this);
			return;
		}
		instance = this;
		hideFlags = HideFlags.DontSave;
		gameObject.hideFlags = HideFlags.DontSave;
	}

	public void DestroyInstance ()
	{
		DestroyImmediate (gameObject);
	}
	
#region UPLOAD TO WORKSHOP
	public void CreateWorkshopItem (Action<bool, EFileHandleCreateResult, PublishedFileId_t> createCallback)
	{
		var createItemCall = SteamUGC.CreateItem (SteamUtils.GetAppID (), EWorkshopFileType.k_EWorkshopFileTypeCommunity);
		EFileHandleCreateResult result = EFileHandleCreateResult.GenericFailure;
		PublishedFileId_t fileId = PublishedFileId_t.Invalid;
		var callResult = CallResult<CreateItemResult_t>.Create((callback, ioFailure) => {
			Debug.Log (ioFailure);
			if (!ioFailure) {
				if (callback.m_bUserNeedsToAcceptWorkshopLegalAgreement) {
					result = EFileHandleCreateResult.NeedsToAcceptWorkshopAgreement;
					createCallback?.Invoke (false, result, PublishedFileId_t.Invalid);
				}
				else {
					result = EFileHandleCreateResult.Success;
					fileId = callback.m_nPublishedFileId;
					createCallback?.Invoke (true, result, fileId);
				}
			}
			else
				createCallback?.Invoke (false, result, PublishedFileId_t.Invalid);
		});
		callResult.Set(createItemCall);
	}
	UGCUpdateHandle_t updateHandle;
	public bool PrepWorkshopUpdate (PublishedFileId_t itemID, string folderPath, string workshopTitle, string workshopDescription, string[] tags, string imagePath, ModLevel_LevelInfo.VisibilityState visibility, out string errorStatus)
	{
		errorStatus = string.Empty;
		updateHandle = SteamUGC.StartItemUpdate (SteamUtils.GetAppID (), itemID);
		if (!SteamUGC.SetItemTitle (updateHandle, workshopTitle))
		{
			errorStatus = "issue with the title";
			updateHandle = UGCUpdateHandle_t.Invalid;
			return false;
		}

		if (!SteamUGC.SetItemDescription (updateHandle, workshopDescription))
		{
			errorStatus = "issue with the description";
			updateHandle = UGCUpdateHandle_t.Invalid;
			return false;
		}
		
		var _visibility = ERemoteStoragePublishedFileVisibility.k_ERemoteStoragePublishedFileVisibilityPublic;
		if (visibility == ModLevel_LevelInfo.VisibilityState.Public)
			_visibility = ERemoteStoragePublishedFileVisibility.k_ERemoteStoragePublishedFileVisibilityPublic;
		else if (visibility == ModLevel_LevelInfo.VisibilityState.FriendsOnly)
			_visibility = ERemoteStoragePublishedFileVisibility.k_ERemoteStoragePublishedFileVisibilityFriendsOnly;
		else if (visibility == ModLevel_LevelInfo.VisibilityState.Private)
			_visibility = ERemoteStoragePublishedFileVisibility.k_ERemoteStoragePublishedFileVisibilityPrivate;
		else if (visibility == ModLevel_LevelInfo.VisibilityState.Unlisted)
			_visibility = ERemoteStoragePublishedFileVisibility.k_ERemoteStoragePublishedFileVisibilityUnlisted;
		if (!SteamUGC.SetItemVisibility (updateHandle, _visibility))
		{
			errorStatus = "issue with the workshop visibility";
			updateHandle = UGCUpdateHandle_t.Invalid;
			return false;
		}
		
		if (!SteamUGC.SetItemTags (updateHandle, tags))
		{
			errorStatus = "issue with the tags";
			updateHandle = UGCUpdateHandle_t.Invalid;
			return false;
		}
		
		if (!SteamUGC.SetItemContent (updateHandle, folderPath))
		{
			errorStatus = "issue with updating the content";
			updateHandle = UGCUpdateHandle_t.Invalid;
			return false;
		}

		if (!SteamUGC.SetItemPreview (updateHandle, imagePath))
		{
			errorStatus = "issue with the preview image";
			updateHandle = UGCUpdateHandle_t.Invalid;
			return false;
		}
		
		return true;		
	}
	public void SubmitWorkshopUpdate (string changes, Action<ELevelSubmitResult> submitCallback)
	{
		Debug.Log ("Initiating Content Upload");
		var submitResult = ELevelSubmitResult.GenericFailure;
		var updateItemCall = SteamUGC.SubmitItemUpdate (updateHandle, changes);
		var callResult = CallResult<SubmitItemUpdateResult_t>.Create((callback, ioFailure) => 
		{
			Debug.Log ("Upload callback returned");
			if (!ioFailure) 
			{
				if (callback.m_bUserNeedsToAcceptWorkshopLegalAgreement) {
					submitResult = ELevelSubmitResult.NeedsToAcceptWorkshopAgreement;
				}
				else { 
					if(callback.m_eResult == EResult.k_EResultOK) {
						
						Debug.Log("Submit Item Update success!");
						submitResult = ELevelSubmitResult.Success;
					}
					else {
						if(callback.m_eResult == EResult.k_EResultItemDeleted) {
							submitResult = ELevelSubmitResult.PreviouslyDeleted;
						}
						else {
							submitResult = ELevelSubmitResult.GenericFailure;
						}
						Debug.Log("Submit Item failure: " + callback.m_eResult.ToString());
					}
				}
			}
			submitCallback?.Invoke (submitResult);
			updateHandle = UGCUpdateHandle_t.Invalid;
		});
		callResult.Set(updateItemCall);
	}
	public float GetSubmissionProgress (out string status)
	{
		status = string.Empty;
		if (updateHandle == UGCUpdateHandle_t.Invalid) return 1;
		var _status = SteamUGC.GetItemUpdateProgress (updateHandle, out var processed, out var total);
		switch (_status)
		{
			case EItemUpdateStatus.k_EItemUpdateStatusInvalid:
				status = "Invalid";
				break;
			case EItemUpdateStatus.k_EItemUpdateStatusPreparingConfig:
				status = "Preparing Config";
				break;
			case EItemUpdateStatus.k_EItemUpdateStatusPreparingContent:
				status = "Preparing Content";
				break;
			case EItemUpdateStatus.k_EItemUpdateStatusUploadingContent:
				status = "Uploading Content";
				break;
			case EItemUpdateStatus.k_EItemUpdateStatusUploadingPreviewFile:
				status = "Uploading Preview File";
				break;
			case EItemUpdateStatus.k_EItemUpdateStatusCommittingChanges:
				status = "Committing Changes";
				break;
		}
		return (float)processed / (float)total;
	}

#endregion

#region SUBSCRIBE/UNSUBSCRIBE
	public void Subscribe(PublishedFileId_t file)
	{
		var call = SteamUGC.SubscribeItem (file);
		var callResult = CallResult<RemoteStorageSubscribePublishedFileResult_t>.Create ((callback, ioFailure) => 
		{

		});
	}
	public void Unsubscribe(PublishedFileId_t file)
	{
		var call = SteamUGC.UnsubscribeItem (file);
		var callResult = CallResult<RemoteStorageUnsubscribePublishedFileResult_t>.Create ((callback, ioFailure) => 
		{
			
		});
	}
	public void GetAllSubscribedItems (Action<List<LevelInfo>> onSuccess, Action onFailure)
	{
		if (getAllSubscribedItemsRoutine != null)
		{
			StopCoroutine (getAllSubscribedItemsRoutine);
			getAllSubscribedItemsRoutine = null;
		}
		getAllSubscribedItemsRoutine = StartCoroutine (GetAllSubscribedItemsRoutine (onSuccess, onFailure));
	}
	Coroutine getAllSubscribedItemsRoutine;
	IEnumerator GetAllSubscribedItemsRoutine (Action<List<LevelInfo>> onSuccess, Action onFailure)
	{
		var numSubbedItems = SteamUGC.GetNumSubscribedItems ();
		var subscribedFiles = new PublishedFileId_t[numSubbedItems];
		SteamUGC.GetSubscribedItems (subscribedFiles, numSubbedItems);
		List<PublishedFileId_t> subscribedFilesList = new List<PublishedFileId_t> ();
		for (int i = 0; i < numSubbedItems; i++)
			subscribedFilesList.Add (subscribedFiles[i]);

		for (int i = 0; i < numSubbedItems; i++)
		{
			var currentFileID = subscribedFiles[i];
			Debug.Log ("Checking Item ID: " + currentFileID.m_PublishedFileId);
			if (SteamUGC.DownloadItem (currentFileID, true))
			{
				Debug.Log ("Initiating Download");
				bool? success = null;
                var callResult = Callback<DownloadItemResult_t>.Create(OnDownloadedItem);
				yield return new WaitUntil (() => success.HasValue);

				void OnDownloadedItem (DownloadItemResult_t callback) 
				{
                    //make sure the returned query is the one we're looking for
                    if(callback.m_unAppID == SteamUtils.GetAppID() && callback.m_nPublishedFileId.m_PublishedFileId == currentFileID.m_PublishedFileId) 
					{
                        if(callback.m_eResult == EResult.k_EResultOK) {
                            Debug.Log("Downloaded successfully!");
                            success = true;
                        }
                        else {
                            Debug.LogError("Error downloading: " + callback.m_eResult.ToString());
                            success = false;
                        }
                    }
					else 
						success = false;
                }
			}
			else 
			{
				Debug.Log ("Already Downloaded");
			}
		}

		var levelInfos = new List<LevelInfo> ();
		var queryHandle = SteamUGC.CreateQueryUGCDetailsRequest (subscribedFiles, numSubbedItems);
		var ugcRequest = SteamUGC.SendQueryUGCRequest (queryHandle);

		var queryCallResult = CallResult<SteamUGCQueryCompleted_t>.Create (OnQueryCompleted);
		queryCallResult.Set (ugcRequest);
		
		yield return new WaitWhile (() => queryCallResult.IsActive ());

		onSuccess?.Invoke (levelInfos);
		SteamUGC.ReleaseQueryUGCRequest (queryHandle);

		void OnQueryCompleted (SteamUGCQueryCompleted_t callback, bool ioFailure)
		{
			if (ioFailure) return;
			List<CallResult<RemoteStorageDownloadUGCResult_t>> callResults = new List<CallResult<RemoteStorageDownloadUGCResult_t>>();
			for (uint i = 0; i < callback.m_unNumResultsReturned; i++)
			{
				if (SteamUGC.GetQueryUGCResult (queryHandle, i, out var details))
				{	
					if (!SteamUGC.GetItemInstallInfo (details.m_nPublishedFileId, out var sizeOnDisk, out var folder, 1000, out var timeStamp))
						continue;
					var levelInfo = new LevelInfo (details, folder, SteamUGC.GetQueryUGCPreviewURL (queryHandle, i, out var url, 1000) ? url : null);
					levelInfos.Add (levelInfo);
				}
			}
		}
	}
#endregion
}