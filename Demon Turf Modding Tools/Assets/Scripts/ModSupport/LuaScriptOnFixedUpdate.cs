using UnityEngine;
using MoonSharp.Interpreter;

public class LuaScriptOnFixedUpdate : MonoBehaviour
{	
	[SerializeField] string script;

	void FixedUpdate ()
	{
		Script.RunString(script);
	}
}