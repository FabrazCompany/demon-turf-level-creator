using UnityEngine;
using MoonSharp.Interpreter;

public class LuaScriptOnStart : MonoBehaviour
{	
	[SerializeField] string script;

	void Start ()
	{
		Script.RunString(script);
	}
}