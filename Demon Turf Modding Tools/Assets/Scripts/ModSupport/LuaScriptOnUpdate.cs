using UnityEngine;
using MoonSharp.Interpreter;

public class LuaScriptOnUpdate : MonoBehaviour
{	
	[SerializeField] string script;

	void Update ()
	{
		Script.RunString(script);
	}
}