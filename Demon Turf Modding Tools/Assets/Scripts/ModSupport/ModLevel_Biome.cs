using UnityEngine;

public class ModLevel_Biome : MonoBehaviour
{
	[SerializeField] BiomeID biome;
	[SerializeField] ModLevel_PlayBoundary playBoundary;
	[SerializeField] ModLevel_KillPlane killPlane;
	[SerializeField] bool returnTripState;

	[SerializeField, Conditional ("biome", BiomeID.CUSTOM)] Material skyboxMat;
    [SerializeField, Conditional ("biome", BiomeID.CUSTOM)] Light sunSource;
    [SerializeField, Conditional ("biome", BiomeID.CUSTOM), ColorUsage (true, true)] Color ambientColor;
    [SerializeField, Conditional ("biome", BiomeID.CUSTOM)] Color fogColor;
    [SerializeField, Conditional ("biome", BiomeID.CUSTOM)] float fogDensity;

	public BiomeID getBiomeID => biome;
	public ModLevel_PlayBoundary getPlayBoundary => playBoundary;
	public ModLevel_KillPlane getKillPlane => killPlane;
	public bool getReturnTripState => returnTripState;

	void OnValidate ()
	{
		if (biome != BiomeID.CUSTOM) return;
		if (sunSource == null)
		{
			GameObject lightObj = GameObject.FindGameObjectWithTag ("Sun");
			if (!lightObj)
			{
				lightObj = new GameObject ("Directional Light (Shadow)");
				lightObj.transform.SetParent (transform);
			}				
			
			if (!lightObj.TryGetComponent<Light> (out sunSource))
				sunSource = lightObj.AddComponent<Light> ();
			
			sunSource.shadows = LightShadows.Soft;
			sunSource.shadowStrength = 1;
			sunSource.shadowResolution = UnityEngine.Rendering.LightShadowResolution.FromQualitySettings;
			sunSource.shadowBias = 0.05f;
			sunSource.shadowNormalBias = 0.4f;
			sunSource.shadowNearPlane = 0.2f;
			sunSource.cookieSize = 10;
			sunSource.renderMode = LightRenderMode.Auto;
			sunSource.cullingMask = -1;
		}
		if (sunSource.type != LightType.Directional)
			sunSource.type = LightType.Directional;
#if UNITY_EDITOR
		if (sunSource.lightmapBakeType != LightmapBakeType.Realtime)
			sunSource.lightmapBakeType = LightmapBakeType.Realtime;
#endif
	}

	public void Trigger () 
    {
        RenderSettings.skybox = skyboxMat;
        RenderSettings.sun = sunSource;
        RenderSettings.ambientLight = ambientColor;
        RenderSettings.fogColor = fogColor;
        RenderSettings.fogDensity = fogDensity;
		
#if !DT_MOD && UNITY_STANDALONE
		PlatformManager.Instance.TriggerBiomeShift (fogColor);
#endif
    }
}