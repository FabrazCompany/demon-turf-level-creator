﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModLevel_KillPlane : MonoBehaviour
{
   [SerializeField] BoxCollider killCollider;
   public BoxCollider getKillCollider => killCollider;
}
