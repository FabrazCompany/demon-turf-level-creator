﻿using UnityEngine;

public class ModLevel_LevelInfo : MonoBehaviour
{
	public enum VisibilityState  
	{
		Public, 
		FriendsOnly,
		Private,
		Unlisted,
	}
    public string levelName;
	[TextArea]
	public string levelDescription;
	public string[] levelTags;
	public Texture2D levelPreview;
	[Space]
	[Tooltip ("The visibility of the level on the workshop.")]
	public VisibilityState levelWorkshopVisibility = VisibilityState.Private;
	public bool leaderboardsActive = false;
	[Space]
	public bool hookshotAvailable = true;
	public bool rolloutAvailable = true;
	public bool glideAvailable = true;
	public bool timeoutAvailable = true;

	[Space]
	[ReadOnly] public ulong steamID;
	public Steamworks.PublishedFileId_t getSteamID => new Steamworks.PublishedFileId_t (steamID);
}
