using UnityEngine;

public class ModLevel_PlayBoundary : MonoBehaviour 
{
    [SerializeField] MeshCollider meshCollider;

    public MeshCollider getMeshCollider => meshCollider;
}