using UnityEngine;

public class ModLevel_SetMusic : MonoBehaviour
{
	[SerializeField] MusicID music;
	[SerializeField, Range (0, 1)] float volume = .7f;
	
	[SerializeField, Conditional ("music", MusicID.CUSTOM)] bool useDynamicMusicSystem;
	[SerializeField, Conditional ("usingNonDynamicMusicOverride")] AudioClip musicOverride;
	[SerializeField, Conditional ("usingDynamicMusicOverride")] MusicCue dynamicMusic;

	public MusicID getMusic => music;
	public AudioClip getMusicOverride => musicOverride;
	public bool getUseDynamicMusicSystem => useDynamicMusicSystem;
	public MusicCue getDyanmicMusic => dynamicMusic;
	public float getVolume => volume;

#if UNITY_EDITOR
	[SerializeField, HideInInspector] bool usingNonDynamicMusicOverride;
	[SerializeField, HideInInspector] bool usingDynamicMusicOverride;
	
	void OnValidate ()
	{
		usingNonDynamicMusicOverride = music == MusicID.CUSTOM && !useDynamicMusicSystem;
		usingDynamicMusicOverride = music == MusicID.CUSTOM && useDynamicMusicSystem;

		if (dynamicMusic)
		{
			if (!usingDynamicMusicOverride && dynamicMusic.gameObject.activeInHierarchy)
			{
				dynamicMusic.gameObject.SetActive (false);
			}
			else if (usingDynamicMusicOverride && !dynamicMusic.gameObject.activeInHierarchy)
			{
				dynamicMusic.gameObject.SetActive (true);
			}
		}
		else 
		{
			if (usingDynamicMusicOverride)
			{
				dynamicMusic = new GameObject ("Dynamic Music").AddComponent<MusicCue> ();
				dynamicMusic.transform.SetParent (transform);
				dynamicMusic.gameObject.AddComponent<MusicSection> ();
				dynamicMusic.SetDefaultValues ();
			}
		}
	}
#endif
}