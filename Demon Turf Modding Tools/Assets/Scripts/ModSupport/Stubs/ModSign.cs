using UnityEngine;
using TMPro;

public class ModSign : MonoBehaviour 
{
	[SerializeField] bool useDefaultGameFont = true;
	[SerializeField] TextMeshPro[] texts;

	public void ReplaceFont (TMP_FontAsset font)
	{
		if (!useDefaultGameFont) return;
		for (int i = 0; i < texts.Length; i++)
		{
			var text = texts[i];
			text.font = font;
		}
	}
}