using System;
using UnityEngine;

#if DT_EXPORT || DT_MOD
public class ModStub_2dxFX_AL_Distortion : MonoBehaviour
{
	public Material ForceMaterial;
    // Advanced Lightning
    public bool ActiveChange = true;
    public bool AddShadow = true;
    public bool ReceivedShadow = false;
    public int BlendMode = 0;
    [Range(0, 1)] public float _Alpha = 1f;
    [Range(0f, 128f)] public float _OffsetX = 10f;
    [Range(0f, 128f)] public float _OffsetY = 10f;
    [Range(0f, 1f)] public float _DistanceX = 0.03f;
    [Range(0f, 1f)] public float _DistanceY = 0.03f;
    [Range(0f, 6.28f)] public float _WaveTimeX = 0.16f;
    [Range(0f, 6.28f)] public float _WaveTimeY = 0.12f;
    public bool AutoPlayWaveX = false;
    [Range(0f, 5f)] public float AutoPlaySpeedX = 5f;
    public bool AutoPlayWaveY = false;
    [Range(0f, 50f)] public float AutoPlaySpeedY = 5f;
    public bool AutoRandom = false;
    [Range(0f, 50f)] public float AutoRandomRange = 10f;
	public int ShaderChange = 0;
}
#else 
public class ModStub_2dxFX_AL_Distortion : _2dxFX_AL_Distortion
{
    
}
#endif

