using System;
using UnityEngine;

#if DT_EXPORT || DT_MOD
public class ModStub_2dxFX_AL_Hologram3 : MonoBehaviour
{
	public Material ForceMaterial;
    // Advanced Lightning
    public bool ActiveChange = true;
    public bool AddShadow = true;
    public bool ReceivedShadow = false;
    public int BlendMode = 0;

    [Range(0, 1)] public float _Alpha = 1f;

    [Range(0, 4)] public float Distortion = 1.0f;
    private float _TimeX = 0;[Range(0, 3)]
    public float Speed = 1;
    public Color _ColorX = new Color(1, 1, 1, 1);
	public int ShaderChange = 0;
}
#else 
public class ModStub_2dxFX_AL_Hologram3 : _2dxFX_AL_Hologram3
{
    
}
#endif

