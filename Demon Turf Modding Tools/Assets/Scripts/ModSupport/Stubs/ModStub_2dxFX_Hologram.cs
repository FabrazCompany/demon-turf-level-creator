using System;
using UnityEngine;

#if DT_EXPORT || DT_MOD
public class ModStub_2dxFX_Hologram : MonoBehaviour
{
	public Material ForceMaterial;
    public bool ActiveChange = true;
    [Range(0, 1)] public float _Alpha = 1f;

    [Range(0, 4)] public float Distortion = 1.0f;
    public float Speed = 1;

    public int ShaderChange = 0;
}
#else 
public class ModStub_2dxFX_Hologram : _2dxFX_Hologram
{
    
}
#endif

