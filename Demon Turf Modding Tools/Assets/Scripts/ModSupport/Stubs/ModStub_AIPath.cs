using UnityEngine;

#if DT_EXPORT || DT_MOD
public class AutoRepathPolicy
{
	public Mode mode;
	public float interval;
	public float sensitivity;
	public float maximumInterval;
	public bool visualizeSensitivity;

	public enum Mode
	{
		Never = 0,
		EveryNSeconds = 1,
		Dynamic = 2
	}
}
public enum OrientationMode {
	ZAxisForward,
	YAxisForward,
}
public enum CloseToDestinationMode {
	/// <summary>The character will stop as quickly as possible when within endReachedDistance (field that exist on most movement scripts) units from the destination</summary>
	Stop,
	/// <summary>The character will continue to the exact position of the destination</summary>
	ContinueToExactDestination,
}
public class ModStub_AIPath : MonoBehaviour
{
	public float radius = 0.5f;
	public float height = 2;
	
	public AutoRepathPolicy autoRepath;
	public float repathRate = .5f;
	
	public bool canMove = true;
	public bool canSearch = true;
	public float maxSpeed = 1;
	public float maxAcceleration = -2.5f;
	
	public OrientationMode orientation;

	public bool enableRotation = true;
	public float rotationSpeed = 360;
	public bool slowWhenNotFacingTarget = true;
	public bool preventMovingBackwards = false;

	public float pickNextWaypointDist = 2;
	public float slowdownDistance = 0.6F;
	public float endReachedDistance = 0.2f;
	public CloseToDestinationMode whenCloseToDestination = CloseToDestinationMode.Stop;
	
	public bool alwaysDrawGizmos;
	public bool constrainInsideGraph = false;

	public Vector3 position;
	public Quaternion rotation;
	public Vector3 destination;
	public Vector3 steeringTarget;
	public Vector3 velocity;
	public bool isStopped;
	public bool pathPending;
	public bool hasPath;
	public bool updateRotation;
	public bool reachedDestination;
	public bool reachedEndOfPath;
	public void FindComponents () {}
	public void SearchPath () {}
	public void Move (Vector3 delta) {}
	public void Teleport (Vector3 position) {}
	public void Teleport (Vector3 position, bool bypass) {}
	public void AdjustTimeScale (float ratio) {}
	public bool InGraph () => true;
}
#else 
public class ModStub_AIPath : Pathfinding.AIPath
{
    
}
#endif