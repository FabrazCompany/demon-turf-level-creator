using System;
using UnityEngine;

#if DT_EXPORT || DT_MOD
public class ModStub_DeferredProjector : MonoBehaviour
{
	public float nearClipPlane = 0.1f;
	public float farClipPlane = 100.0f;
	public float fieldOfView = 60.0f;
	public float aspectRatio = 1.0f;
	public bool orthographic = false;
	public int orthographicSize = 10;
}
#else 
public class ModStub_DeferredProjector : DeferredProjector
{
    
}
#endif
