using System;
using UnityEngine;

#if DT_EXPORT || DT_MOD
public enum HxTintMode { Off = 0, Color = 1, Edge = 2, Gradient = 3 };
public class ModStub_HxVolumetricLight : MonoBehaviour
{
	public Texture3D NoiseTexture3D;
	public Vector3 NoiseScale = new Vector3(0.5f, 0.5f, 0.5f);
    public Vector3 NoiseVelocity = new Vector3(1, 1, 0);
    bool dirty = true;

    public Texture LightFalloff;
   
    public float NearPlane = 0;
    public bool NoiseEnabled = false;
    public bool CustomMieScatter = false;
    public bool CustomExtinction = false;
    public bool CustomExtinctionEffect = false;
    public bool CustomDensity = false;
    public bool CustomSampleCount = false;
    public bool CustomColor = false;
    public bool CustomNoiseEnabled = false;
    public bool CustomNoiseTexture = false;
    public bool CustomNoiseScale = false;
    public bool CustomNoiseVelocity = false;
    public bool CustomNoiseContrast = false;

    public bool CustomFogHeightEnabled = false;
    public bool CustomFogHeight = false;
    public bool CustomFogTransitionSize = false;
    public bool CustomAboveFogPercent = false;


    public bool CustomSunSize = false;
    public bool CustomSunBleed = false;
    public bool ShadowCasting = true;
    public bool CustomStrength = false;
    public bool CustomIntensity = false;
    public bool CustomTintMode = false;
    public bool CustomTintColor = false;
    public bool CustomTintColor2 = false;
    public bool CustomTintGradient = false;
    public bool CustomTintIntensity = false;
    public bool CustomMaxLightDistance = false;
    [Range(0, 4)]
    public float NoiseContrast = 1;
    public HxTintMode TintMode = HxTintMode.Off;
    public Color TintColor = Color.red;
    public Color TintColor2 = Color.blue;
    public float TintIntensity = 0.2f;
    [Range(0, 1)]
    public float TintGradient = 0.2f;

    [Range(0, 8)]
    public float Intensity = 1;
    [Range(0, 1)]
    public float Strength = 1;
    public Color Color = Color.white;
    [Range(0.0f, 0.9999f)]
    [Tooltip("0 for even scattering, 1 for forward scattering")]
    public float MieScattering = 0.05f;
    [Range(0.0f, 1)]
    [Tooltip("Create a sun using mie scattering")]
    public float SunSize = 0f;
    [Tooltip("Allows the sun to bleed over the edge of objects (recommend using bloom)")]
    public bool SunBleed = true;
    [Range(0.0f, 10f)]
    [Tooltip("dimms results over distance")]
    public float Extinction = 0.01f;
    [Range(0.0f, 1f)]
    [Tooltip("Density of air")]
    public float Density = 0.2f;
    [Range(0.0f, 1.0f)]
    [Tooltip("Useful when you want a light to have slightly more density")]
    public float ExtraDensity = 0;
    [Range(2, 64)]
    [Tooltip("How many samples per pixel, Recommended 4-8 for point, 6 - 16 for Directional")]
    public int SampleCount = 4;


    [Tooltip("Ray marching Shadows can be expensive, save some frames by not marching shadows")]
    public bool Shadows = true;
    public bool FogHeightEnabled = false;
    public float FogHeight = 5;
    public float FogTransitionSize = 5;
    public float MaxLightDistance = 128;

    public float AboveFogPercent = 0.1f;

    bool OffsetUpdated = false;

    public Vector3 Offset = Vector3.zero;
}
#else 
public class ModStub_HxVolumetricLight : HxVolumetricLight
{
    
}
#endif