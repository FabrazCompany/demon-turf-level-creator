using System;
using UnityEngine;

#if DT_EXPORT || DT_MOD
public class ModStub_KinematicCharacterMotor : MonoBehaviour
{
	[NonSerialized] public Rigidbody AttachedRigidbodyOverride;
	
}
#else 
public class ModStub_KinematicCharacterMotor : KinematicCharacterController.KinematicCharacterMotor
{
    
}
#endif

