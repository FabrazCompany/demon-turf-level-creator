using UnityEngine;

#if DT_EXPORT || DT_MOD
public enum ProjectorType 
{
	FoamProjector,
	NormalProjector
};
public class ModStub_LuxWater_Projector : MonoBehaviour
{
	public ProjectorType Type = ProjectorType.FoamProjector;
}
#else 
public class ModStub_LuxWater_Projector : LuxWater.LuxWater_Projector
{
    
}
#endif

