using UnityEngine;

#if DT_EXPORT || DT_MOD
public class ModStub_NavmeshCut : MonoBehaviour
{

	public float circleRadius = 1;
	public int circleResolution = 6;
	public float height = 1;
	public Vector3 center;
	public float updateDistance = 0.4f;
	public bool useRotationAndScale;
	public float updateRotationDistance = 10;
	public bool isDual;
	public bool cutsAddedGeom = true;
}
#else 
public class ModStub_NavmeshCut : Pathfinding.NavmeshCut
{
    
}
#endif

