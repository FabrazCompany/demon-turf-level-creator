﻿using System;
using UnityEngine;

#if DT_EXPORT || DT_MOD
public interface IMoverController
{
	bool UpdateMovement(out Vector3 goalPosition, out Quaternion goalRotation, float deltaTime);
}
public class ModStub_PhysicsMover : MonoBehaviour
{
	public Rigidbody Rigidbody;
	[System.NonSerialized]
	public IMoverController MoverController;
	[NonSerialized] public bool Interpolate;
	[NonSerialized] public Vector3 InitialTickPosition;
	[NonSerialized] public Quaternion InitialTickRotation;
	void OnValidate ()
	{
		if (TryGetComponent<Rigidbody> (out var rigid))
			Rigidbody = rigid;
	}
	public void SetPosition (Vector3 position) {}
	public void SetRotation (Quaternion rotation) {}
	public void SetPositionAndRotation (Vector3 position, Quaternion rotation) {}
}
#else 
public class ModStub_PhysicsMover : KinematicCharacterController.PhysicsMover
{
    
}
#endif

