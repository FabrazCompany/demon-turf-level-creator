using UnityEngine;

public class ModStub_PlayerManager : MonoBehaviour
{
	protected IPlayerController character;
	protected virtual bool spawnPlayerImmediately => true;
	protected virtual Transform startPoint => transform;
	protected virtual void Start () {}
	protected virtual void OnDestroy () {}
	protected virtual void SpawnInPlayer () {}
	protected void SetPlayer (Vector3 position, Quaternion rotation) {} 
}