using UnityEngine;

#if DT_EXPORT || DT_MOD
public enum RVOLayer {
	DefaultAgent = 1 << 0,
	DefaultObstacle = 1 << 1,
	Layer2 = 1 << 2,
	Layer3 = 1 << 3,
	Layer4 = 1 << 4,
	Layer5 = 1 << 5,
	Layer6 = 1 << 6,
	Layer7 = 1 << 7,
	Layer8 = 1 << 8,
	Layer9 = 1 << 9,
	Layer10 = 1 << 10,
	Layer11 = 1 << 11,
	Layer12 = 1 << 12,
	Layer13 = 1 << 13,
	Layer14 = 1 << 14,
	Layer15 = 1 << 15,
	Layer16 = 1 << 16,
	Layer17 = 1 << 17,
	Layer18 = 1 << 18,
	Layer19 = 1 << 19,
	Layer20 = 1 << 20,
	Layer21 = 1 << 21,
	Layer22 = 1 << 22,
	Layer23 = 1 << 23,
	Layer24 = 1 << 24,
	Layer25 = 1 << 25,
	Layer26 = 1 << 26,
	Layer27 = 1 << 27,
	Layer28 = 1 << 28,
	Layer29 = 1 << 29,
	Layer30 = 1 << 30
}
public class ModStub_RVOController : MonoBehaviour
{
	[Tooltip("How far into the future to look for collisions with other agents (in seconds)")]
	public float agentTimeHorizon = 2;
	[Tooltip("How far into the future to look for collisions with obstacles (in seconds)")]
	public float obstacleTimeHorizon = 2;
	[Tooltip("Max number of other agents to take into account.\n" +
			"A smaller value can reduce CPU load, a higher value can lead to better local avoidance quality.")]
	public int maxNeighbours = 10;
	public RVOLayer layer = RVOLayer.DefaultAgent;
	public RVOLayer collidesWith = (RVOLayer)(-1);
	[Tooltip("How strongly other agents will avoid this agent")]
	[UnityEngine.Range(0, 1)]
	public float priority = 0.5f;
	[Tooltip("Automatically set #locked to true when desired velocity is approximately zero")]
	public bool lockWhenNotMoving = false;
	[Tooltip("A locked unit cannot move. Other units will still avoid it. But avoidance quality is not the best")]
	public bool locked;
	/// <summary>Enables drawing debug information in the scene view</summary>
	public bool debug;
}
#else 
public class ModStub_RVOController : Pathfinding.RVO.RVOController
{
    
}
#endif

