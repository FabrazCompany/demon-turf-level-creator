using UnityEngine;

#if DT_EXPORT || DT_MOD
public class StartEndModifier {
	public bool addPoints;
	public Exactness exactStartPoint = Exactness.ClosestOnNode;
	public Exactness exactEndPoint = Exactness.ClosestOnNode;

	public enum Exactness {
		/// <summary>
		/// The point is snapped to the position of the first/last node in the path.
		/// Use this if your game is very tile based and you want your agents to stop precisely at the center of the nodes.
		/// If you recalculate the path while the agent is moving you may want the start point snapping to be ClosestOnNode and the end point snapping to be SnapToNode however
		/// as while an agent is moving it will likely not be right at the center of a node.
		///
		/// [Open online documentation to see images]
		/// </summary>
		SnapToNode,
		/// <summary>
		/// The point is set to the exact point which was passed when creating the path request.
		/// Note that if a path was for example requested to a point inside an obstacle, then the last point of the path will be inside that obstacle, which is usually not what you want.
		/// Consider using the <see cref="ClosestOnNode"/> option instead.
		///
		/// [Open online documentation to see images]
		/// </summary>
		Original,
		/// <summary>
		/// The point is set to the closest point on the line between either the two first points or the two last points.
		/// Usually you will want to use the NodeConnection mode instead since that is usually the behaviour that you really want.
		/// This mode exists mostly for compatibility reasons.
		/// [Open online documentation to see images]
		/// Deprecated: Use NodeConnection instead.
		/// </summary>
		Interpolate,
		/// <summary>
		/// The point is set to the closest point on the surface of the node. Note that some node types (point nodes) do not have a surface, so the "closest point" is simply the node's position which makes this identical to <see cref="Exactness.SnapToNode"/>.
		/// This is the mode that you almost always want to use in a free movement 3D world.
		/// [Open online documentation to see images]
		/// </summary>
		ClosestOnNode,
		/// <summary>
		/// The point is set to the closest point on one of the connections from the start/end node.
		/// This mode may be useful in a grid based or point graph based world when using the AILerp script.
		///
		/// Note: If you are using this mode with a <see cref="Pathfinding.PointGraph"/> you probably also want to use the <see cref="Pathfinding.PointGraph.NodeDistanceMode Connection"/> for <see cref="Pathfinding.PointGraph.nearestNodeDistanceMode"/>.
		///
		/// [Open online documentation to see images]
		/// </summary>
		NodeConnection,
	}
	public bool useRaycasting;
	public LayerMask mask = -1;
	public bool useGraphRaycasting;

}
public class ModStub_Seeker : MonoBehaviour
{
	public bool drawGizmos = true;
	public bool detailedGizmos;
	public StartEndModifier startEndModifier = new StartEndModifier();
}
#else 
public class ModStub_Seeker : Pathfinding.Seeker
{
    
}
#endif

