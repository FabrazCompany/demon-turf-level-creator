using UnityEngine;

#if DT_EXPORT || DT_MOD
public enum SmoothType 
{
	Simple,
	Bezier,
	OffsetSimple,
	CurvedNonuniform
}
public class ModStub_SimpleSmoothModifier : MonoBehaviour
{
	public SmoothType smoothType = SmoothType.Simple;
	[Tooltip("The number of times to subdivide (divide in half) the path segments. [0...inf] (recommended [1...10])")]
	public int subdivisions = 2;
	[Tooltip("Number of times to apply smoothing")]
	public int iterations = 2;
	[Tooltip("Determines how much smoothing to apply in each smooth iteration. 0.5 usually produces the nicest looking curves")]
	[Range(0, 1)]
	public float strength = 0.5F;

	[Tooltip("Toggle to divide all lines in equal length segments")]
	public bool uniformLength = true;

	[Tooltip("The length of each segment in the smoothed path. A high value yields rough paths and low value yields very smooth paths, but is slower")]
	public float maxSegmentLength = 2F;

	[Tooltip("Length factor of the bezier curves' tangents")]
	public float bezierTangentLength = 0.4F;

	[Tooltip("Offset to apply in each smoothing iteration when using Offset Simple")]
	public float offset = 0.2F;

	[Tooltip("How much to smooth the path. A higher value will give a smoother path, but might take the character far off the optimal path.")]
	public float factor = 0.1F;
}
#else 
public class ModStub_SimpleSmoothModifier : Pathfinding.SimpleSmoothModifier
{
    
}
#endif

