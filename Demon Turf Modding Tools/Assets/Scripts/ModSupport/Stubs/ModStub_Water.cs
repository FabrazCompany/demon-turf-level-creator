using UnityEngine;

public class ModStub_Water : MonoBehaviour
{
	[SerializeField] WaterID waterID;
	[SerializeField] new BoxCollider collider;
	[SerializeField, Conditional ("waterID", WaterID.PoisonWater)] float triggerAmount = 0;
	[SerializeField, Conditional ("waterID", WaterID.PoisonWater)] float buildupRate = 1;
	[SerializeField, Conditional ("waterID", WaterID.PoisonWater)] float buildupAmount = 10;
	public WaterID getWaterID => waterID;
	public float getTriggerAmount => triggerAmount;
	public float getBuildupRate => buildupRate;
	public float getBuildupAmount => buildupAmount;
	public BoxCollider getCollider => collider;
}