﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttractToPlayer : MonoBehaviour
{
    [SerializeField] Transform root;
    [SerializeField] float speed;
    Transform target;
    void OnTriggerEnter (Collider other) {
        if (!other.TryGetComponent<IPlayerController> (out var _player))
            return;

        target = other.transform;
        enabled = true;
    }

    void Update () 
	{
        root.position = Vector3.MoveTowards (root.position, target.transform.position, speed * Time.fixedDeltaTime);
    }
}
