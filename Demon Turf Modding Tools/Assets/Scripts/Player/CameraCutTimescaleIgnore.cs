﻿using System.Collections;
using UnityEngine;
using Cinemachine;

public class CameraCutTimescaleIgnore : MonoBehaviour
{
    ParticleSystem particle;
    MovingPlatform movingPlatform;
    Animator animator;
	AutoMoveAndRotate autoMoveAndRotate;

    Cinemachine.CinemachineVirtualCamera vCam;
    void Awake () 
    {
        particle = GetComponent<ParticleSystem> ();
        movingPlatform = GetComponent<MovingPlatform> ();
        animator = GetComponent<Animator> ();
		autoMoveAndRotate = GetComponent<AutoMoveAndRotate> ();
    }
    public void OnCutTriggered (Cinemachine.CinemachineVirtualCamera _camera, float val1, float val2, bool val3) 
    {
        vCam = _camera;
        enabled = true;
        Set (true);
    }

    void Set (bool state)
    {
        if (particle)
        {
            var part = particle.main;
            part.useUnscaledTime = state;
        }

        movingPlatform?.SetIgnoreTimeScale (state);
        if (animator)
            animator.updateMode = state ? AnimatorUpdateMode.UnscaledTime : AnimatorUpdateMode.Normal;
		if (autoMoveAndRotate)
			autoMoveAndRotate.ignoreTimescale = state;
    }
    void Update () 
    {
        if (vCam == null || Time.timeScale != 0)
        {
            vCam = null;
            Set (false);
            enabled = false;
        }
    }
}
