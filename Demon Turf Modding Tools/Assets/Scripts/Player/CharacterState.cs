
public enum CharacterState
{
    Default = 0,

    Skidding = 20,
    AerialBatJump = 30,
    WallSliding = 40,

    SuperJump = 50,
	SuperJumpBump = 52,
    SideJump = 60,

    // PunchCharge = 70,
    PunchAttack = 80,
    SpinAttack = 90,

    // HookShotPull = 99,
    HookShotting = 100,
    RollingOut = 101,
    RollingOutSkidding = 102,

    Glide = 104,
    GlideDash = 105,

    TimeOutShot = 107,

    Stunned = 110,
    Stabilizing = 111,
    Held = 112,
    Sliding = 120,

    Dead = 130,
    Spawning = 140,
    Resetting = 142,
    PlacingCheckpoint = 143,

    SwimmingSurface = 150,
    SwimmingSubmerged = 160,

    SwimmingDash = 170,

    FallingPanic = 199,
    FallingPanicRecovery = 200,

	RisingPanic = 201,

    Frozen = 220,

    IdlingPhone = 250,
    IdlingSitting = 255,
    IdlingSleeping = 260,
    IdlingDialogue = 275,

    PhotoMode = 1000,    //menu-based states
}