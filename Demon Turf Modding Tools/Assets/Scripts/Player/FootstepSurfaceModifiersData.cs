using System;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;

public class FootstepSurfaceModifiersData : ScriptableObject 
{

	public FootstepSurfaceMaterialPairing footstepsMaterialTable;
	public FootstepSurfaceModifiers footstepSurfaceModifiers;


	[Serializable]
	public class FootstepSurfaceMaterialPairing : SerializableDictionaryBase<Material, FootstepSurfaceType> {}
	[Serializable]
	public class FootstepSurfaceModifiers : SerializableDictionaryBase<FootstepSurfaceType, FoostepSurfaceModifier> {}

	[Serializable]
	public class FoostepSurfaceModifier
	{
		public AudioClipSet soundStep; 
		public AudioClipSet soundLanding;
		[Space]
		public bool overrideParticles;
		public FoostepSurfaceParticleModifier particleStep;
		public FoostepSurfaceParticleModifier particleStepLingering;
		public FoostepSurfaceParticleModifier particleBurst;
		public FoostepSurfaceParticleModifier particleBurstAccent;
		public FoostepSurfaceParticleModifier particleFloorImpact;
		public FoostepSurfaceParticleModifier particleFloorImpactLingering;
	}
	[Serializable]
	public class FoostepSurfaceParticleModifier
	{
		[SerializeField] ParticleSystem.MinMaxCurve lifetime;
		[SerializeField] Material material;
		[SerializeField] ParticleSystem.MinMaxGradient color;
		[SerializeField] ParticleSystem.MinMaxCurve speed;
		[SerializeField] ParticleSystem.MinMaxCurve gravity;
		
		[SerializeField] ParticleSystem.MinMaxCurve size;
		[SerializeField] bool useAnimations = true;

		public void SetValues (ParticleSystem particle)
		{
			var main = particle.main;
			main.startLifetime = lifetime;
			main.startColor = color;
			main.startSpeed = speed;
			main.gravityModifier = gravity;
			main.startSize = size;

			var animations = particle.textureSheetAnimation;
			animations.enabled = useAnimations;

			var rend = particle.GetComponent<ParticleSystemRenderer> ();
			rend.material = material;
		}
	}
}