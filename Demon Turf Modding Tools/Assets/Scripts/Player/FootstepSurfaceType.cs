
[System.Serializable] 
public enum FootstepSurfaceType
{
	WOOD = 10,
	ICE = 20,
	SAND = 30,
	GRASS_GREEN = 40,
	GRASS_RED = 41,
	GRASS_PURPLE = 42,
	DIRT = 50,
	HAY = 60,
	SNOW = 70,
	ROCK = 80,
	METAL = 90,
	BRICK = 100,
	PLASTIC = 110,
	CLOTH = 120,
	WATER = 130,
	GOLD = 140,
}