using UnityEngine;

public interface IFlashTarget 
{
	Transform transform { get; }
}