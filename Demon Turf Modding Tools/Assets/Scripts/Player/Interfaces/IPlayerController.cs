using System;
using UnityEngine;
using Cinemachine;

public interface IPlayerController
{
	Transform transform { get; }
	GameObject gameObject { get; }
	Collider getCollider { get; }
	Rigidbody getRigidbody { get; }
	Transform getTarget { get; }
	CharacterState characterState { get; }
	CharacterState previousState { get; }
	Vector3 velocity { get; }
	bool isGrounded { get; }
	bool isCharging { get; }
	bool isOutOfPlayArea { get; }
	float getPoisonPCT { get; }
	Transform CameraFollowPoint { get; }
	ITargetTraits getTargetTraits { get; }
	ConditionalChallengeGUI GetChallengeGUI { get; }
	
	

	event Action onJump;
	event Action onDoubleJump;
	event Action onSuperJump;
	event Action onSpin;
	event Action onPunch;
	event Action<CharacterState> onStateChanged;
	event Action<Vector3> onPlayerRepositioned;
	event Action onKilled;
	event Action onPlayerDeathReset;
	event Action<int> onCheckpointCountUpdated;
	event Action<IPlayerCheckpoint, int> onActiveCheckpointUpdated;
	
	void SetInitializationState (CharacterState state);
	void SetInvincibility (float duration, bool visualize);
	void SitEnter (PlayerIdlePoint idlePoint);
	void SitExit (PlayerIdlePoint idlePoint);
	void LookPointEnter (CinemachineVirtualCamera lookView, float duration, bool hidePlayer, Action callback, string actionLabel = "Look");
	void LookPointEnter (Transform lookPoint, float duration, Action callback, string actionLabel = "Look");
	void LookPointExit (CinemachineVirtualCamera lookView);
	void LookPointExit (Transform lookPoint);
	void AddRotationOverride (Transform rotation, float adjustSpeed);
	void ClearRotationOverride ();
	void AddZoomOverride (float zoom);
	void ClearZoomOverride ();
	void AddInteraction (GameObject owner, int interactionID, string animationName, Action callback, bool removeOnTriggering = true);
	void RemoveInteraction (int interactionID);
	void TriggerBoost (Vector3 direction, bool isJump, float scalar);
	void OnEnterJetstream (GlideBoostDraft trigger);
	void OnExitJetstream ();
	void SkipDialogue ();
	void SnapToGround ();
	void CleanInteractions ();

	void RegisterCombatZone (bool active);
	void EnterCheckpointZone (IPlayerCheckpoint checkpoint, bool zoneBased = true);
	void ExitCheckpointZone (IPlayerCheckpoint checkpoint);
	void RepositionPlayerToCheckpoint ();
	void EnterNoCheckpointZone ();
	void ExitNoCheckpointZone ();
	void SetCurse (CurseChallengeType primaryType, CurseChallengeType secondaryType);
	void ClearCurse ();
	void EnterTimeOutGateTrigger (float duration, float scale);

	void ResetJumpState ();
	void ResetAerialState ();
	void ForceUnground ();
	bool RegisterSticky (ISticky sticky);

	void TriggerHold ();
	void ClearHold ();
	void AddKnockback (Vector3 force);
	void AddPush (Vector3 force);
	void TriggerSpinStun (float duration, float velDamping, float spinStrength);
	void TriggerDeath (DeliverKill.KillTypes type);
	void SetPoisonVulnerability (float vulnerability);
	
	void AddSecondaryVelocity (Vector3 force);
	void ApplyAccelerationAdjustment (float ratio);
	void OnWallSlideGravityAdjustTriggerEnter (Surface_WallSlideGravityAdjust trigger);
	void OnWallSlideGravityAdjustTriggerExit (Surface_WallSlideGravityAdjust trigger);
	void ZeroOutVelocity ();
	void CancelMomentum ();
	void CancelSuperJumpBump ();
	void SetBounceCollider (bool active);
	void TriggerPanicRise ();
	void ClearPanicRise ();
	void SetPlayerInputLock (float duration);
	void ClearPlayerInputLock ();
	void RepositionPlayer (Vector3 position, Quaternion rotation, bool haltMomentum = false, float freezeInputDur = 0);
}