using UnityEngine;

public interface ISticky 
{
    void OnSpin ();
    Transform transform { get; }
}