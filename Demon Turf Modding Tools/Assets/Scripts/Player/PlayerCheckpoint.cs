﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using DG.Tweening;

public class PlayerCheckpoint : BasePlayerCheckpoint, IPlayerCheckpoint
{
	[SerializeField] Collider trigger;
    [SerializeField] ParticleSystem activeParticle;
    [SerializeField] ParticleSystem entryParticle;
    [SerializeField] AudioClip entryClip;
	AudioMixerSnapshot[] checkpointSnapshots;
	PlayerShelteredTrigger checkpointShelterState;

    public override Transform startPoint => transform;
	public AudioMixerSnapshot[] getCheckpointSnapshots => checkpointSnapshots;
	public bool hasCheckpointSnapshots => checkpointSnapshots != null && checkpointSnapshots.Length > 0;
	public void SetCheckpointSnapshots (AudioMixerSnapshot[] newSnapshots) => checkpointSnapshots = newSnapshots;
	public void SetCheckpointShelterState (PlayerShelteredTrigger _shelter) => checkpointShelterState = _shelter;
	public void TriggerShelterState () => checkpointShelterState?.Trigger ();
	public Collider getTrigger => trigger;

    void OnActiveCheckpointUpdated (IPlayerCheckpoint checkpoint, int ind) 
	{
        if (!activeParticle) return;

        if (this == checkpoint as PlayerCheckpoint) {
            activeParticle?.Play ();
        } else {
            activeParticle?.Stop ();
        }
    }

    void OnTriggerEnter(Collider other) 
	{
		if (!other.TryGetComponent<IPlayerController> (out var player))
			return;

        SetAsActiveCheckpoint (player);
        if (entryParticle)
            entryParticle?.Play ();
    }
    public override void SetAsActiveCheckpoint (IPlayerController player) 
    {
        player.onActiveCheckpointUpdated -= OnActiveCheckpointUpdated;
        player.onActiveCheckpointUpdated += OnActiveCheckpointUpdated;
        player.EnterCheckpointZone (this);
    }
    
    void OnTriggerExit(Collider other) 
	{
		if (other.TryGetComponent<IPlayerController> (out var player))
			player.ExitCheckpointZone (this);
    }
}

public abstract class BasePlayerCheckpoint : MonoBehaviour
{
	public abstract Transform startPoint { get; }
	public abstract void SetAsActiveCheckpoint (IPlayerController player);
}

public interface IPlayerCheckpoint 
{
    
    GameObject gameObject { get; }
    Transform startPoint { get; }
}