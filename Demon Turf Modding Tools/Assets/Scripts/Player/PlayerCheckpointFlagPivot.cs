using UnityEngine;
using DG.Tweening;

public class PlayerCheckpointFlagPivot : MonoBehaviour 
{
	[SerializeField] Transform flag;
	[SerializeField] SphereCollider trigger;
	[SerializeField] float maxAngle = 30f;
	[SerializeField] AnimationCurve maxAngleCurve = AnimationCurve.Constant(0, 1, 1);
	[SerializeField] float releaseSnapSpeed = 90;
#if !DT_EXPORT && !DT_MOD
	IPlayerController player;
	DKBossController_Beebz dk;
#endif
	Vector3 velocity;

	void OnTriggerEnter (Collider hit)
	{
#if !DT_EXPORT && !DT_MOD
		if (hit.TryGetComponent <IPlayerController> (out var _player))
			player = _player;
		if (hit.TryGetComponent <DKBossController_Beebz> (out var _dk))
			dk = _dk;
#endif
	}
	void OnTriggerExit (Collider hit)
	{
#if !DT_EXPORT && !DT_MOD
		if (hit.TryGetComponent <IPlayerController> (out var _player))
			player = null;
		if (hit.TryGetComponent <DKBossController_Beebz> (out var _dk))
			dk = null;
#endif
	}
	void Update ()
	{
#if !DT_EXPORT && !DT_MOD
		if (player != null || dk)
		{
			Transform target = player?.transform ?? dk?.transform;
			var distPCT = flag.DistanceSqr (target.position) / (trigger.radius * trigger.radius);
			flag.localEulerAngles = flag.Heading (target) * ( maxAngle * maxAngleCurve.Evaluate (1 - distPCT));
		}
		else 
		{
			flag.rotation = Quaternion.RotateTowards (flag.rotation, Quaternion.identity, releaseSnapSpeed * Time.deltaTime);
		}
#endif
	}
}