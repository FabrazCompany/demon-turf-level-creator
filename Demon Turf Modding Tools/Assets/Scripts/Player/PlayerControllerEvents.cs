using System;
using UnityEngine;

public class PlayerControllerEvents 
{
	static public event Action onReset;
	public void TriggerOnReset () => onReset?.Invoke ();

    static public event Action onKilledStatic;
	public void TriggerOnKilledStatic () => onKilledStatic?.Invoke ();

	static public event Action<IPlayerController> onSpawned;
	public void TriggerOnSpawned (IPlayerController spawned) => onSpawned?.Invoke (spawned);

	static public event Action onPlayerKnockedback;
	public void TriggerOnPlayerKnockback () => onPlayerKnockedback?.Invoke ();

	static public event Action onHookshotPulled;
	public void TriggerOnHookshotPulled () => onHookshotPulled?.Invoke ();
	
	static public event Action<Vector3> onPlayerRepositionedStatic;
	public void TriggerOnPlayerRepositionedStatic (Vector3 position) => onPlayerRepositionedStatic?.Invoke (position);
}