﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerIdlePoint : MonoBehaviour
{

    [System.Serializable]
    public enum IdleType 
    {
        Sitting,
        Sleeping,
    }


	[SerializeField] Collider hitbox;
    [SerializeField] IdleType type;
    [SerializeField] float exitForceUp;
    [SerializeField] float exitForceForward;
    [SerializeField] UnityEvent onTrigger;
    [SerializeField] UnityEvent onCancel;    

    public IdleType getIdleType => type;
    public float getExitForceUp => exitForceUp;
    public float getExitForceForward => exitForceForward;
	public Collider getHitbox => hitbox;
    public UnityEvent getOnTrigger => onTrigger;
    public UnityEvent getOnCancel => onCancel;
	
	void OnValidate ()
	{
		if (hitbox == null && transform.parent != null && transform.parent.TryGetComponent<Collider> (out var coll))
			hitbox = coll;
	}
	void Awake () 
	{
		GetComponentsInChildren<Collider> ();
	}
    void OnTriggerEnter (Collider hit) 
    {
        var player = hit.GetComponent<IPlayerController> ();
        if (player == null) return;

        player.SitEnter (this);
    }
    
    void OnTriggerExit (Collider hit)
    {
        var player = hit.GetComponent<IPlayerController> ();
        if (player == null) return;

        player.SitExit (this);
    }
}
