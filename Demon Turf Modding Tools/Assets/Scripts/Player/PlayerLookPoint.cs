﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLookPoint : MonoBehaviour
{
    [SerializeField] float duration = 2.5f;
    [SerializeField] float repeatDelay = 1f;
    [Header ("Shot Type")]
    [SerializeField] bool customCameraShot;
    [SerializeField, Conditional ("customCameraShot", hide: true)] Transform lookTarget;
    [SerializeField, Conditional ("customCameraShot")] Cinemachine.CinemachineVirtualCamera lookCamera;
    [SerializeField, Conditional ("customCameraShot")] bool hidePlayer = true;
    
    Collider triggerZone;

    void Awake () 
    {
        triggerZone = GetComponent<Collider> ();
        enabled = false;
    }

    void OnTriggerEnter (Collider hit) 
    {
		if (!hit.TryGetComponent<IPlayerController> (out var player))
			return;

        if (!customCameraShot)
            player.LookPointEnter (lookTarget, duration, DelayedReactivate);
        else
            player.LookPointEnter (lookCamera, duration, hidePlayer, DelayedReactivate);
    }
    
    void OnTriggerExit (Collider hit)
    {
		if (!hit.TryGetComponent<IPlayerController> (out var player))
			return;
        
        if (!customCameraShot)
            player.LookPointExit (lookTarget);
        else 
            player.LookPointExit (lookCamera);
    }
    void DelayedReactivate () 
    {
        if (triggerZone)
            triggerZone.enabled = false;
        timer = repeatDelay;
        enabled = true;
    }
    float timer;
    void Update () 
    {
        timer -= Time.deltaTime;
        if (timer > 0) return;

        if (triggerZone)
            triggerZone.enabled = true;
        enabled = false;
    }
}
