using UnityEngine;
using Cinemachine;

public class PlayerLookTarget : MonoBehaviour
{
    [SerializeField] CinemachineVirtualCamera lookCamera;
    [SerializeField] float lookDuration = 2;
    public CinemachineVirtualCamera getLookCamera => lookCamera;
    public float getLookDuration => lookDuration;
}