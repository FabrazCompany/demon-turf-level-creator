﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerPunchAttackCanceller : MonoBehaviour
{
    [SerializeField] bool active = true;
    [SerializeField] bool receiveKnockback;
	[SerializeField] bool ignoreStun;
    [SerializeField] bool jabArmor;
    [SerializeField] bool chargeArmor;
	[SerializeField] UnityEventVector3 onBlocked;

    public void setActive (bool _active) => active = _active;
    public bool getActive => active;
    public bool getReceiveKnockback => receiveKnockback;
	public bool getIgnoreStun => ignoreStun;
    public bool getJabArmor => jabArmor;
    public bool getChargeArmor => chargeArmor;
	
	public UnityEventVector3 getOnBlocked => onBlocked;

}
