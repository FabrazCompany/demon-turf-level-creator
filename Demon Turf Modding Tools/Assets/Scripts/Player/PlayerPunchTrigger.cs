using UnityEngine;
using UnityEngine.Events;
using Malee;

public class PlayerPunchTrigger : MonoBehaviour 
{
	[SerializeField] bool mute;
	[SerializeField] Transform offsetCheck;
	[SerializeField] bool hasProtectionRange;
	[SerializeField, Conditional ("hasProtectionRange")] float protectionRange;
	[SerializeField] Vector3 customForward = new Vector3 (0,0,-1);
	[SerializeField] AudioClipSet jabTiggerClips;
	[SerializeField] AudioData jabAudioData;
	[SerializeField] AudioClipSet chargedTriggerClips;
	[SerializeField] AudioData chargedAudioData;
	public bool flattenY;
	[Space]
	public bool triggerOnJab;
	public float jabScaling = 1;
	public UnityEventVector3 onTriggeredJab;
	[Space]
	public bool triggerOnCharge;
	public float chargeScaling = 1;
	public UnityEventVector3 onTriggeredCharge;
	public void SetMute (bool _mute) => mute = _mute;
	public bool ShouldTrigger (Vector3 hitDir)
	{

		if (mute) return false;
		if (hasProtectionRange)
		{
			var dot = Vector3.Dot (transform.TransformDirection (customForward), hitDir);
			Debug.Log (dot);
			if (dot > protectionRange)
				return false;
		}
		return true;
	}
	public void PlayTriggerClip (bool isCharged)
	{
		var triggerClips = isCharged ? chargedTriggerClips : jabTiggerClips;
#if !DT_MOD
		if (triggerClips.HasSound)
			SoundManagerStub.Instance.PlaySoundGameplay3D (triggerClips.GetSound, transform.position, isCharged ? chargedAudioData : jabAudioData);
#endif
	}
}