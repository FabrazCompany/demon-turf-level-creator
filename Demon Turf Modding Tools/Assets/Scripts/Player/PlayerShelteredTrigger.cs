using System;
using UnityEngine;

public class PlayerShelteredTrigger : MonoBehaviour
{
	[SerializeField] bool indoors;
	[SerializeField] bool includeExit;
	[Space]
	[Conditional ("indoors")]
	public AmbienceData ambience;
	public bool isIndoors => triggeringExit ? !indoors : indoors;

	bool triggeringExit;
	IPlayerController player;
	public bool getTriggeringExit => triggeringExit;

	public static event Action<PlayerShelteredTrigger> onAmbienceUpdated;

	[System.Serializable]
	public class AmbienceData 
	{
		public bool stopsParticles = true;
		public bool lowersAmbience = true;
		public bool muteAmbeince = false;
	}
	void OnTriggerEnter (Collider hit)
	{
		if (!hit.TryGetComponent<IPlayerController> (out var _player)) return;
		player = _player;
		player.onPlayerRepositioned -= OnPlayerRepositioned;
		player.onPlayerRepositioned += OnPlayerRepositioned;
		triggeringExit = false;
		Trigger ();
	}
	void OnTriggerExit (Collider hit)
	{
		if (player == null) return;
		if (!includeExit) return;
		if (!hit.TryGetComponent<IPlayerController> (out var _player)) return;
		player.onPlayerRepositioned -= OnPlayerRepositioned;
		player = null;
		triggeringExit = true;
		Trigger ();
	}
	void OnPlayerRepositioned (Vector3 pos)
	{
		player.onPlayerRepositioned -= OnPlayerRepositioned;
		triggeringExit = true;
		Trigger ();
		player = null;
	}

	public void Trigger () => onAmbienceUpdated?.Invoke (this);

	void OnDestroy ()
	{
		if (player != null)
		{
			player.onPlayerRepositioned -= OnPlayerRepositioned;
			player = null;
		}
	}
}