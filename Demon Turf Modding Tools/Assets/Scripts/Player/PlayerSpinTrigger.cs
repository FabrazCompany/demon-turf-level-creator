using UnityEngine;
using UnityEngine.Events;

public class PlayerSpinTrigger : MonoBehaviour 
{
	[SerializeField] UnityEvent onSpun;

	public void Trigger () => onSpun?.Invoke ();
}