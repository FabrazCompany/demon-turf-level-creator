﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReceivePlayerWeight : MonoBehaviour, IReceivePlayerWeight
{
    Rigidbody rigid;
    Collider coll;
    [SerializeField] Vector3 offset = new Vector3 (0, 0.01f, 0);
    [SerializeField] float massScaling = 1;

    void Awake ()
    {
        rigid = GetComponentInParent<Rigidbody> ();
        coll = GetComponent<Collider> ();
        // Debug.Log (rigid.sleepThreshold);
    }

    public void ApplyPlayerWeight (Vector3 origin, float massVal) 
    {
        var point = origin + offset;
        var force = Vector3.up * massVal * massScaling * Time.deltaTime;
        rigid.AddForceAtPosition (force, point);
    }
}