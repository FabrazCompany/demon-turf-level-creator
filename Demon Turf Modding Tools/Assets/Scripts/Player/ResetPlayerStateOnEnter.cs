﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPlayerStateOnEnter : MonoBehaviour
{
    [SerializeField] float refreshRate = .5f;
    float timer;
    IPlayerController player;

    void Awake () 
    {
        enabled = false;
    }
    void Update () 
    {
        if (player == null)
        {
            enabled = false;
            return;
        }
            
        timer += Time.deltaTime;
        if (timer < refreshRate)
            return;    

        timer = 0;
        player.ResetAerialState ();
    }

    void OnTriggerEnter (Collider hit)
    {
		if (!hit.TryGetComponent<IPlayerController> (out var _player))
			return;
        player = _player;
        enabled = true;
    }
    void OnTriggerExit (Collider hit) 
    {
		if (!hit.TryGetComponent<IPlayerController> (out var _player))
			return;
        player = null;
        enabled = false;
    }

    
}
