using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPlayerLookTarget : MonoBehaviour
{
    [Header ("Triggering")]
    [SerializeField, UnityEngine.Serialization.FormerlySerializedAs ("triggerOnEnable")] bool registerOnEnable;
    [SerializeField] bool autoTrigger;
    [SerializeField] bool repeatable;
    bool triggered = false;
    [Header ("Timing")]
    [SerializeField] float availability = 3;
    [SerializeField] float duration = 2.5f;
    [Header ("Shot Type")]
    [SerializeField] bool customCameraShot;
    [SerializeField, Conditional ("customCameraShot", hide: true)] Transform lookTarget;
    [SerializeField, Conditional ("customCameraShot")] Cinemachine.CinemachineVirtualCamera lookCamera;

    public static event System.Action <Transform, float, float, bool> onLookTriggered;
    public static event System.Action <Cinemachine.CinemachineVirtualCamera, float, float, bool> onCutTriggered;
	CameraCutTimescaleIgnore[] timescaleIgnorers;
	void Awake ()
	{
		timescaleIgnorers = SceneUtility.FindAllSceneObjectsIncludingDisabled<CameraCutTimescaleIgnore> ();
	}
    void OnEnable ()
    {
#if !DT_MOD
        if (SceneTransitionManager.IsInitialized && SceneTransitionManager.Instance.timeSinceTransitionFinished <= 0.5f)
            return;
		if (SceneTransitionManager.IsInitialized && SceneTransitionManager.Instance.unlockEnableLogicBlocked)
			return;
#endif
        if (Time.timeSinceLevelLoad <= 0.5f)
            return;
        if (!registerOnEnable)
            return;
        SetLookTarget ();
    }
    public void SetLookTarget () 
    {
        if (!repeatable)
        {
            if (triggered)
                return;
            triggered = true;
        }
        if (customCameraShot)
		{
			onCutTriggered?.Invoke (lookCamera, availability, duration, autoTrigger);
			for (int i = 0; i < timescaleIgnorers.Length; i++)
			{
				if (timescaleIgnorers[i] == null)
					continue;
				timescaleIgnorers[i].OnCutTriggered (lookCamera, availability, duration, autoTrigger);
			}
		}
        else 
            onLookTriggered?.Invoke (lookTarget ? lookTarget : transform, availability, duration, autoTrigger);
    }
}
