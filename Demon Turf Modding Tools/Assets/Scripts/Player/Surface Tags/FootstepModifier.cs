using UnityEngine;

public class FootstepModifier : MonoBehaviour 
{
	[SerializeField] FootstepSurfaceType footstepType;
	public FootstepSurfaceType getFootstepType => footstepType;
}