using UnityEngine;

public class NoCheckpoint : MonoBehaviour 
{
	IPlayerController player;
	void OnTriggerEnter (Collider hit)
	{
		if (!hit.TryGetComponent<IPlayerController> (out var _player)) return;
		player = _player;
		player.EnterNoCheckpointZone ();
	}

	void OnTriggerExit (Collider hit)
	{
		if (!hit.TryGetComponent<IPlayerController> (out var _player)) return;
		_player.ExitNoCheckpointZone ();
		player = null;
	}

	void OnDisable ()
	{
		if (player != null)
		{
			player.ExitNoCheckpointZone ();
			player = null;
		}
	}
}