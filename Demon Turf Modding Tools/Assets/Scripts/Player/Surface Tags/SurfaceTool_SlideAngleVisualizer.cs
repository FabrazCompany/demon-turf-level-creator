using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurfaceTool_SlideAngleVisualizer : MonoBehaviour
{

    public bool showAdjustedNormal = true;
    public float checkDistance = 4;
    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere (transform.position, 0.1f);
        if (!Physics.Raycast (transform.position, Vector3.down, out RaycastHit hit, checkDistance)) {
            Gizmos.DrawRay (transform.position, Vector3.down * checkDistance);
            return;
        }
        Gizmos.DrawLine (transform.position, hit.point);
        
        Gizmos.color = Color.blue;
        if (showAdjustedNormal) {
            Gizmos.DrawRay (hit.point, hit.normal.With (y:0).normalized * 3);
        } else {
            Gizmos.DrawRay (hit.point, hit.normal * 3);
        }
        
        
        // Gizmos.color = Color.red;
        
    }
}
