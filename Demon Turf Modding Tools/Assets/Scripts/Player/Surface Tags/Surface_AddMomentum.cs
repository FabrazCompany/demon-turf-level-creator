using UnityEngine;


public class Surface_AddMomentum : MonoBehaviour 
{
    [SerializeField] Orientation orientation;
    [SerializeField] float force;
    public Vector3 GetMomentum => transform.GetDirection (orientation) * force;

    void OnDrawGizmosSelected () {
        Gizmos.color = Color.white;
        Gizmos.DrawRay (transform.position, GetMomentum);
        Gizmos.DrawSphere (transform.position, 0.1f);        
    }
}