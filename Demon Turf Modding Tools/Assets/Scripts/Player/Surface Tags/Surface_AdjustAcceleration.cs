using UnityEngine;

public class Surface_AdjustAcceleration : MonoBehaviour 
{
    [SerializeField] float accelerationRatio;
    public float GetAccelerationRatio => accelerationRatio;

    void OnTriggerStay (Collider hit) 
    {
		if (!hit.TryGetComponent<IPlayerController> (out var player))
			return;
        player.ApplyAccelerationAdjustment (accelerationRatio);
    }
}