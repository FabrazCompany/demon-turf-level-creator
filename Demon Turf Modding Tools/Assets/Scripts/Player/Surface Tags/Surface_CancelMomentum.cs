using UnityEngine;

public class Surface_CancelMomentum : MonoBehaviour 
{
    void OnTriggerEnter (Collider hit)
	{
		if (!hit.TryGetComponent<IPlayerController> (out var player))
			return;
		player.CancelMomentum ();
	}
}