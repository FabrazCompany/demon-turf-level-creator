﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Surface_WallSlideGravityAdjust : MonoBehaviour
{
    [SerializeField] float gravityStartScalar = 1;
    [SerializeField] float gravityEndScalar = 1;
    public float getGravityStartScalar => gravityStartScalar;
    public float getGravityEndScalar => gravityEndScalar;

	void OnTriggerEnter (Collider hit)
	{
		if (!hit.TryGetComponent<IPlayerController> (out var player))
			return;
		player.OnWallSlideGravityAdjustTriggerEnter (this);
	}
	void OnTriggerExit (Collider hit)
	{
		if (!hit.TryGetComponent<IPlayerController> (out var player))
			return;
		player.OnWallSlideGravityAdjustTriggerExit (this);
	}
}
