using UnityEngine;

public class Zone_EnsureBounce : MonoBehaviour
{
	void OnTriggerEnter (Collider hit)
	{
		if (hit.TryGetComponent<IPlayerController> (out var player))
			return;
		player.SetBounceCollider (true);
	}

	void OnTriggerExit (Collider hit)
	{
		if (!hit.TryGetComponent<IPlayerController> (out var player) || !player.isGrounded)
			return;
		player.SetBounceCollider (false);
	}
}