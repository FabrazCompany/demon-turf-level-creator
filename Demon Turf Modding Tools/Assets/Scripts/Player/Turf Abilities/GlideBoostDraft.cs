﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlideBoostDraft : MonoBehaviour
{
    [SerializeField] Facing forceDirection;
    
    public Vector3 GetDirection () 
    {
        switch (forceDirection) {
            default:
            case Facing.Forward:
                return transform.forward;
            case Facing.Up:
                return transform.up;
            case Facing.Right:
                return transform.right;
        }
    }

    void OnTriggerEnter (Collider hit) 
    {
        if (!hit.TryGetComponent<IPlayerController> (out var player))
            return;
        player.OnEnterJetstream (this);
    }
    void OnTriggerExit (Collider hit)
    {
		if (!hit.TryGetComponent<IPlayerController> (out var player))
            return;
        player.OnExitJetstream ();
    }
}
