﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
// using UnityEngine.Syst;
using UnityEngine.Serialization;

#if UNITY_EDITOR
using UnityEditor;

[UnityEditor.CustomEditor (typeof (HookshotTarget)), CanEditMultipleObjects]
public class HookshotTargetEditor : UnityEditor.Editor
{
    public override void OnInspectorGUI() 
    {
        serializedObject.Update ();
        var typeProp = serializedObject.FindProperty ("type");

        EditorGUILayout.PropertyField (serializedObject.FindProperty ("mute"));
        EditorGUILayout.PropertyField (typeProp);
        EditorGUILayout.PropertyField (serializedObject.FindProperty ("tooCloseRange"));
		EditorGUILayout.PropertyField (serializedObject.FindProperty ("maxRangeScalar"));
        EditorGUILayout.PropertyField (serializedObject.FindProperty ("visualOffset"));
		EditorGUILayout.PropertyField (serializedObject.FindProperty ("visualOffsetDistance"));
		
        EditorGUILayout.Space ();

        var type = (HookshotTarget.HookshotTargetType)typeProp.enumValueIndex;
        if (type == HookshotTarget.HookshotTargetType.Zip)
        {
            EditorGUILayout.PropertyField (serializedObject.FindProperty ("navigationOffset"));
            EditorGUILayout.PropertyField (serializedObject.FindProperty ("exitForce"));
            EditorGUILayout.PropertyField (serializedObject.FindProperty ("exitDistance"));
        }
        else
        {

			
			if (type == HookshotTarget.HookshotTargetType.Pull)
			{
				EditorGUILayout.PropertyField (serializedObject.FindProperty ("instantYank"));
			}
			var invertVerticalProp = serializedObject.FindProperty ("invertVerticalYank");
			EditorGUILayout.PropertyField (invertVerticalProp);
			if (invertVerticalProp.boolValue)
			{
				EditorGUILayout.PropertyField (serializedObject.FindProperty ("invertVerticalYankThreshold"));
			}

			EditorGUILayout.PropertyField (serializedObject.FindProperty ("tugSpeedScale"));
            EditorGUILayout.PropertyField (serializedObject.FindProperty ("pullBuildupTime"));
            EditorGUILayout.Space ();
            EditorGUILayout.PropertyField (serializedObject.FindProperty ("tugVulnerability"));
            EditorGUILayout.PropertyField (serializedObject.FindProperty ("tugVerticalBias"));
            EditorGUILayout.Space ();
            EditorGUILayout.PropertyField (serializedObject.FindProperty ("pullVulnerability"));
            EditorGUILayout.PropertyField (serializedObject.FindProperty ("pullVerticalBias"));
            EditorGUILayout.Space ();
			EditorGUILayout.PropertyField (serializedObject.FindProperty ("onPullStarted"));
			EditorGUILayout.PropertyField (serializedObject.FindProperty ("onTug"));
			EditorGUILayout.PropertyField (serializedObject.FindProperty ("onPullReleased"));
			if (type == HookshotTarget.HookshotTargetType.ChargedPull)
			{
				EditorGUILayout.PropertyField (serializedObject.FindProperty ("onFullChargeReached"));
				EditorGUILayout.PropertyField (serializedObject.FindProperty ("onFullChargeFailed"));
			}
			EditorGUILayout.PropertyField (serializedObject.FindProperty ("onDetached"));
            EditorGUILayout.PropertyField (serializedObject.FindProperty ("ignoredBlockers"), true);
        }
        serializedObject.ApplyModifiedProperties ();
    }
}
#endif


public class HookshotTarget : MonoBehaviour
{
    public enum HookshotTargetType
    {
        Zip,
        Pull,
        ChargedPull
    }
    

    [SerializeField] bool mute;
    [SerializeField] HookshotTargetType type;
    [SerializeField] float tooCloseRange = 5f;
	[SerializeField, Range (0, 1)] float maxRangeScalar = 1;
    [SerializeField] Vector3 visualOffset;
	[SerializeField] float visualOffsetDistance = 0;
    [SerializeField] List<Collider> ignoredBlockers;
    

    //zip only
    [SerializeField] Vector3 navigationOffset;    
    [SerializeField] Vector2 exitForce;
    [SerializeField] float exitDistance = 2f;

    //pull only
	[SerializeField] bool instantYank;
	[SerializeField] bool invertVerticalYank;
	[SerializeField] float invertVerticalYankThreshold = -0.1f;
    [SerializeField, FormerlySerializedAs ("pullResistanceTime")] float pullBuildupTime = 4f;
	[SerializeField] float tugSpeedScale = 1;
    [SerializeField] float tugVulnerability = 1;
    [SerializeField, Range (-1, 1)] float tugVerticalBias = .7f;    
    [SerializeField] float pullVulnerability = 1;
    [SerializeField, Range (-1, 1)] float pullVerticalBias = .3f;

    [SerializeField] UnityEventVector3 onTug;
    [SerializeField] UnityEvent onPullStarted;
    [SerializeField, FormerlySerializedAs ("onPullStopped")] UnityEventVector3 onPullReleased;
    [SerializeField] UnityEventVector3 onFullChargeReached;
    [SerializeField] UnityEventVector3 onFullChargeFailed;
	[SerializeField] UnityEvent onDetached;


    public bool getMute => mute;
    public HookshotTargetType getType => type;
	public bool getInstantYank => instantYank;
	public float getTugSpeedScale => tugSpeedScale;
    public float getTooCloseRange => tooCloseRange;
	public float getMaxRangeScalar => maxRangeScalar;
    public Vector3 getNavigationPosition => transform.position + navigationOffset; 
    public Vector3 getVisualPosition => transform.position + visualOffset;
	public float getVisualOffsetDistance => visualOffsetDistance;
    public float getPullResistanceTime => pullBuildupTime;
    public Vector2 getExitForce => exitForce;
    public float getExitDistance => exitDistance;
    public bool IsBlocker (Collider hit) => !ignoredBlockers.Contains (hit) && gameObject != hit.gameObject;
    public void setMute (bool _mute) => mute = _mute;
    public void AttachHook () => onPullStarted?.Invoke ();
    public void ApplyTugForce (Vector3 direction, float force) 
	{
		var tugForce = ModifyPullDirection (direction, tugVerticalBias) * force * tugVulnerability;
		if (direction.y <= invertVerticalYankThreshold)
			tugForce.y *= -1f;
		onTug?.Invoke (tugForce);
	}

	public UnityEventVector3 getOnTug => onTug;
	public UnityEvent getOnPullStarted => onPullStarted;
	public UnityEventVector3 getOnPullReleased => onPullReleased;
	public UnityEventVector3 getOnFullChargeReached => onFullChargeReached;
	public UnityEventVector3 getOnFullChargeFailed => onFullChargeFailed;
	public event Action<HookshotTarget> onUnavailable;
	public UnityEvent getOnnDetached => onDetached;

	void OnDisable () 
	{
		onUnavailable?.Invoke (this);
	}

	public void TriggerDetach () => onDetached?.Invoke (); 

    public void ApplyPullForce (Vector3 direction, float force, bool fullyCharged)
    {
        var _force = ModifyPullDirection (direction, pullVerticalBias) * force * pullVulnerability;
        onPullReleased?.Invoke (_force);
        if (type == HookshotTargetType.ChargedPull)
        {
            if (fullyCharged)
                onFullChargeReached?.Invoke (_force);
            else 
                onFullChargeFailed?.Invoke (_force);
        }
    }

    Vector3 ModifyPullDirection (Vector3 direction, float verticalBias) => Vector3.Lerp (direction, Vector3.up * Mathf.Sign (verticalBias), Mathf.Abs (verticalBias)); 
    void OnDrawGizmosSelected()
    {
        if (type == HookshotTargetType.Zip)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawSphere (getNavigationPosition, 0.25f);    
        }

        Gizmos.color = Color.red;
        Gizmos.DrawSphere (getVisualPosition, 0.25f);    
    }
	
	[ContextMenu ("Set Values - Small")]
	public void SetWeightValuesSmall ()
	{
		visualOffset = Vector3.up * 0.5f;
		tugSpeedScale = 1.5f;
		pullBuildupTime = 1.75f;
		tugVulnerability = 1.5f;
		tugVerticalBias = 0.55f;
		pullVulnerability = 1.25f;
		pullVerticalBias = 0.3f;
		instantYank = false;
		invertVerticalYank = false;
	}	

	[ContextMenu ("Set Values - Large")]
	public void SetWeightValuesLarge ()
	{
		visualOffset = Vector3.up * 1.5f;
		tugSpeedScale = 1f;
		pullBuildupTime = 2.5f;
		tugVulnerability = 3f;
		tugVerticalBias = 0.6f;
		pullVulnerability = 2f;
		pullVerticalBias = 0.3f;
		instantYank = false;
		invertVerticalYank = false;
	}

	[ContextMenu ("Set Values - Flying")]
	public void SetWeightValuesFlying ()
	{
		visualOffset = Vector3.up * 0.2f;
		tugSpeedScale = 1.5f;
		pullBuildupTime = 1.75f;
		tugVulnerability = 1.5f;
		tugVerticalBias = 0.55f;
		pullVulnerability = 1.25f;
		pullVerticalBias = 0.3f;
		instantYank = false;
		invertVerticalYank = true;
	}

	[ContextMenu ("Set Values - Tiny")]
	public void SetWeightValuesTiny ()
	{
		visualOffset = Vector3.up * 0.2f;
		tugSpeedScale = 1.5f;
		pullBuildupTime = 1.75f;
		tugVulnerability = 1.5f;
		tugVerticalBias = 0.55f;
		pullVulnerability = 2f;
		pullVerticalBias = 0.7f;
		instantYank = true;
		invertVerticalYank = false;
	}
}
