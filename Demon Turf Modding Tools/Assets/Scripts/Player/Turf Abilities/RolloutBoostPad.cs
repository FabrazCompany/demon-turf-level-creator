﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RolloutBoostPad : MonoBehaviour
{
    [SerializeField] bool onlyTriggerWhenRolling = true;
	float boostScalar = 1;
    public void Trigger (Collider hit)
    {
        if (!hit.TryGetComponent<IPlayerController> (out var player))
			return;
        
        if (onlyTriggerWhenRolling && player.characterState != CharacterState.RollingOut)
            return;

        player.TriggerBoost (transform.forward, false, boostScalar);
    }
    public void TriggerJump (Collider hit)
    {
		if (!hit.TryGetComponent<IPlayerController> (out var player))
			return;
        
        if (onlyTriggerWhenRolling && player.characterState != CharacterState.RollingOut)
            return;
        
        player.TriggerBoost (transform.forward, true, boostScalar);
    }

    [Header ("Debuggibg")]
    [SerializeField] bool drawDebug;
    [SerializeField, Conditional ("drawDebug")] Vector3 debugVisualOffset;
    [SerializeField, Conditional ("drawDebug")] float debugVisualLength = 3;
    void OnDrawGizmosSelected () 
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawRay (transform.position + debugVisualOffset, transform.forward * debugVisualLength);
        Gizmos.DrawSphere (transform.position + debugVisualOffset, 0.2f);
    }
}
