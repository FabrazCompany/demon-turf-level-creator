using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using DG.Tweening;

public class SlowMoBubble : MonoBehaviour 
{
	[System.Serializable]
	enum SlowMoBubbleType 
	{
		Player,
		Hider,
		Grower,
	}
	[SerializeField, UnityEngine.Serialization.FormerlySerializedAs ("bubbleContactResponse")] SlowMoBubbleType bubbleType;
	[SerializeField] float slowdownScale = .3f;
	[Space]
	[SerializeField] float fadeDuration = 0.5f;
	[SerializeField, Conditional ("bubbleType", SlowMoBubbleType.Player, hide: true)] float triggerDurationScalar = 1;
	[SerializeField, Conditional ("bubbleType", SlowMoBubbleType.Grower)] float growerRiseDuration = 0.5f;
	[SerializeField, Conditional ("bubbleType", SlowMoBubbleType.Grower), UnityEngine.Serialization.FormerlySerializedAs ("growerScale")] AnimationCurve growerDecayArc;

	
	[Space]
	[SerializeField] MeshRenderer rendExterior;
	[SerializeField] MeshRenderer rendInterior;

	[Header ("Particles")]
	[SerializeField] ParticleSystem activeParticle;
	[SerializeField] float activeParticleLifetime = 3f;
	[SerializeField] float cleanupParticleLifetime = .5f;
	[SerializeField, Conditional ("bubbleType", SlowMoBubbleType.Hider), UnityEngine.Serialization.FormerlySerializedAs ("inactiveParticles")] ParticleSystem hiderInactiveParticles;
	[SerializeField] ParticleSystemForceField particleSystemForceField;
	 
	[Header ("Sound")]
	[SerializeField] AudioSource activeSound;
	[SerializeField] float activeSoundRangeScalarMin = 2;
	[SerializeField] float activeSoundRangeScalarMax = 3;
	[Space]
	[SerializeField] AudioClip tickSound;
	[SerializeField] AudioClip finishSound;
	[SerializeField] int tickAmount = 5;
	[SerializeField] float tickPitch = 1f;
	
	bool cleaningUp;
	bool hiding;
	float duration = 1;
	float tickTimer;
	float timer;
	float scale;
	float ticks;
	float activeSlowdownScale;

	List<SlowMoEntity> residents;
	Collider hitbox;
	Material externalMaterial;
	Material internalMaterial;
	AITrait_TimeOutFeedback tantrumFeedback;
	PositionConstraint positionConstraint;

	SlowMoBubble controlledBubble;
	List<Collider> colliderChecks;

	bool active;

	void OnValidate () 
	{
		var _scale = transform.localScale.x;
		activeSound.minDistance = _scale * activeSoundRangeScalarMin;
		activeSound.maxDistance = _scale * activeSoundRangeScalarMax;
	}
	void Awake () 
	{
		hitbox = GetComponentInChildren<Collider> ();
		positionConstraint = GetComponent<PositionConstraint> ();
		residents = new List<SlowMoEntity> ();
		rendExterior.enabled = true;
		rendInterior.enabled = false;
		externalMaterial = rendExterior.material;
		internalMaterial = rendInterior.material;
		scale = transform.localScale.x;
		activeSound.minDistance = scale * activeSoundRangeScalarMin;
		activeSound.maxDistance = scale * activeSoundRangeScalarMax;
		duration = 1f;
		tickPitch = 1f; 
		activeSlowdownScale = slowdownScale;
		// enabled = false;
		active = false;
		colliderChecks = new List<Collider> ();

		if (bubbleType == SlowMoBubbleType.Grower)
		{
			Hide (true);
		}
	}
	void OnDestroy () 
	{
		residents.ForEach (i => 
		{
			if (i != null) 
				i.Deactivate ();
		});
		residents.Clear ();
		Destroy (internalMaterial);
		Destroy (externalMaterial);		
	}
	void OnDisable () 
	{
		cleaningUp = false;
		residents.ForEach (i => 
		{
			if (i != null) 
				i.Deactivate ();
		});
		residents.Clear ();
	}

	public void CameraCheckEnter (Collider hit) 
	{
		if (!hit.CompareTag ("MainCamera"))
			return;
		rendExterior.enabled = false;
		rendInterior.enabled = true;
	}

	public void CameraCheckExit (Collider hit) 
	{
		if (!hit.CompareTag ("MainCamera"))
			return;
		rendExterior.enabled = true;
		rendInterior.enabled = false;
	}

	public void OnEntityEnter (Collider hit)
	{
		if (!hit.attachedRigidbody)
			return;
		if (hit.GetComponent<SlowMoIgnore> () || hit.attachedRigidbody.GetComponent<SlowMoIgnore> ())
			return;
		if (hit.isTrigger)
		{
			hit.attachedRigidbody.GetComponentsInChildren<Collider> (false, colliderChecks);
			for (int i = 0; i < colliderChecks.Count; i++)
			{
				if (!colliderChecks[i].isTrigger)
					return;
			}
		}
		TryAddResident (hit.attachedRigidbody.GetComponent<SlowMoEntity> ());
	}

	public void OnEntityExit (Collider hit) 
	{
		if (!hit.attachedRigidbody)
			return;
		if (hit.GetComponent<SlowMoIgnore> () || hit.attachedRigidbody.GetComponent<SlowMoIgnore> ())
			return;
		if (hit.isTrigger)
		{
			hit.attachedRigidbody.GetComponentsInChildren<Collider> (false, colliderChecks);
			for (int i = 0; i < colliderChecks.Count; i++)
			{
				if (!colliderChecks[i].isTrigger)
					return;
			}
		}
		TryRemoveResident (hit.attachedRigidbody.GetComponent<SlowMoEntity> ());        
	}

	public void TryAddResident (SlowMoEntity entity) 
	{
		Debug.Log ("Adding Resident", entity);
		if (!entity || residents.Contains (entity))
			return;
		residents.Add (entity);
		entity.Activate (activeSlowdownScale);
		entity.onCancelSlowdown -= TryRemoveResident;
		entity.onCancelSlowdown += TryRemoveResident;
	}
	public void TryRemoveResident (SlowMoEntity entity)
	{
		if (!entity || !residents.Contains (entity))
			return;
		residents.Remove (entity);
		entity.Deactivate ();
		entity.onCancelSlowdown -= TryRemoveResident;
	}

	void Update ()
	{
		if (!active) return;
		if (cleaningUp)
		{
			if (activeParticle.isStopped && internalMaterial.GetFloat ("_SliceAmount") >= 1)
			{
				particleSystemForceField.gameObject.SetActive (false);
				TrashMan.despawn (gameObject);
				return;
			}
		} 
		else 
		{
			if (tickTimer > 0)
			{
				tickTimer -= Time.deltaTime;
				if (tickTimer <= 0)
				{
					if (--ticks > 0)
					{
						SoundManagerStub.Instance.PlaySoundGameplay2D (controlledBubble ? controlledBubble.tickSound : tickSound, transform.position).pitch = tickPitch;
						SetTickTimer ();
					}
				}
			}
		}

		

		timer += Time.deltaTime;
		if (tantrumFeedback)
		{
			tantrumFeedback.UpdateTimer (1 - (timer / duration));
		}
		if (controlledBubble && controlledBubble.bubbleType == SlowMoBubbleType.Grower)
		{
			if (timer <= 0)
				controlledBubble.transform.localScale = Vector3.one * Mathf.Lerp (0, scale, 1 - (Mathf.Abs (timer) / growerRiseDuration)) * controlledBubble.scale;
			else
				controlledBubble.transform.localScale = Vector3.one * controlledBubble.growerDecayArc.Evaluate (timer / duration) * controlledBubble.scale; 
		}

		if (timer >= duration)
			Despawn ();
	}

	void Trigger () 
	{
		var main = activeParticle.main;
		main.startLifetimeMultiplier = activeParticleLifetime;
		activeParticle.Play ();

		if (bubbleType == SlowMoBubbleType.Hider)
			hiderInactiveParticles?.Stop ();

		particleSystemForceField.gameObject.SetActive (false);
		rendExterior.enabled = true;
		rendInterior.enabled = false;
		
		internalMaterial.SetFloat ("_SliceAmount", 1);
		internalMaterial.DOKill ();
		internalMaterial.DOFloat (0, "_SliceAmount", fadeDuration);
		externalMaterial.SetFloat ("_SliceAmount", 1);
		externalMaterial.DOKill ();
		externalMaterial.DOFloat (0, "_SliceAmount", fadeDuration);

		activeSound.enabled = true;
		
		hiding = false;
		cleaningUp = false;
		ticks = tickAmount;
		hitbox.enabled = true;
	}
	void Hide (bool instant = false) 
	{
		hiding = true;
		hitbox.enabled = false;
		residents.ForEach (i => {
			if (i != null)
			{
				i.ClearTantrum ();
				i.Deactivate ();
			}		
		});
		residents.Clear ();

		if (positionConstraint.sourceCount > 0)
		{
			positionConstraint.RemoveSource (0);
			positionConstraint.constraintActive = false;
		}

		activeParticle.Stop ();
		var main = activeParticle.main;
		main.startLifetimeMultiplier = cleanupParticleLifetime;
		particleSystemForceField.gameObject.SetActive (true);
		if (bubbleType == SlowMoBubbleType.Hider)
			hiderInactiveParticles?.Play ();
		
		activeSound.enabled = false;

		if (instant)
		{
			internalMaterial.SetFloat ("_SliceAmount", 1);
			rendInterior.enabled = false;
			externalMaterial.SetFloat ("_SliceAmount", 1);
			rendExterior.enabled = false;
		}
		else 
		{
			internalMaterial.DOKill ();
			internalMaterial.DOFloat (1, "_SliceAmount", fadeDuration).OnComplete (() => rendInterior.enabled = false);
			externalMaterial.DOKill ();
			externalMaterial.DOFloat (1, "_SliceAmount", fadeDuration).OnComplete (() => rendExterior.enabled = false);
		}
	}


	//used by player/trigger type
	void SetTimer (float dur) 
	{
		duration = dur;
		timer = 0;
		ticks = tickAmount;
		SetTickTimer ();
	}
	void SetTickTimer () => tickTimer = duration / tickAmount;
	public void Trigger (float _scale, float _dur, float _pitch, Transform positionTarget)
	{
		positionConstraint.AddOrSet (0, positionTarget);
		positionConstraint.constraintActive = true;
		Trigger (_scale, _dur, _pitch);
	}
	public void Trigger (float _scale, float _dur, float _pitch)
	{   
		scale = _scale;
		tickPitch = _pitch; 
		transform.localScale = Vector3.zero;	
		SetTimer (_dur);
		transform.DOScale (scale, fadeDuration);
		activeSound.minDistance = scale * activeSoundRangeScalarMin;
		activeSound.maxDistance = scale * activeSoundRangeScalarMax;
		activeSlowdownScale = slowdownScale;
		Trigger ();
		active = true;
	}
	public void TantrumTrigger (SlowMoEntity _entity, float _timeoutScalar, float _dur, float _pitch) 
	{
		Hide (true);
		tickPitch = _pitch;
		SetTimer (_dur);
		activeSlowdownScale = slowdownScale / _timeoutScalar;
		tantrumFeedback = _entity.GetComponentInChildren<AITrait_TimeOutFeedback> (true);
		tantrumFeedback?.Trigger ();
		TryAddResident (_entity);
		_entity.SetTantrum (this);
		active = true;
	}
	public void Despawn () 
	{
		if (cleaningUp)
			return;
		
		cleaningUp = true;
		
		transform.DOScale (Vector3.zero, fadeDuration);
		if (controlledBubble)
		{
			switch (controlledBubble.bubbleType)
			{
				case SlowMoBubbleType.Hider:
					controlledBubble.Trigger ();
					break;
				case SlowMoBubbleType.Grower:
					controlledBubble.Hide ();
					break;
			}
			controlledBubble = null;
		}
		if (tantrumFeedback)
		{
			internalMaterial.SetFloat ("_SliceAmount", 1);
			externalMaterial.SetFloat ("_SliceAmount", 1);
			tantrumFeedback.Deactivate ();
		}
		Hide ();
		Debug.Log ("Play Sound", this);
		SoundManagerStub.Instance.PlaySoundGameplay2D(finishSound, transform.position, data: new AudioData(_volume: .7f, _isLooping: false));
	}
	public void OnBubbleEnter (Collider hit)
	{		
		if (bubbleType != SlowMoBubbleType.Player)
			return;

		if (!hit.attachedRigidbody)
			return;

		var otherBubble = hit.attachedRigidbody.GetComponent<SlowMoBubble> ();
		if (!otherBubble)
		{
			var bubbleTrigger = hit.attachedRigidbody.GetComponent<SlowMoBubbleTrigger> ();
			if (bubbleTrigger)
			{
				otherBubble = bubbleTrigger.owner;
				bubbleTrigger.onTrigger?.Invoke ();
			}
				
		}

		if (!otherBubble)
			return;

		switch (otherBubble.bubbleType)
		{
			case SlowMoBubbleType.Hider:
				Hide (otherBubble);
				break;
			case SlowMoBubbleType.Grower:
				Grow (otherBubble);
				break;
		}
	}

	void Hide (SlowMoBubble _controlledBubble)
	{
		controlledBubble = _controlledBubble;
		controlledBubble.Hide ();
		SetTimer (duration * controlledBubble.triggerDurationScalar);
		Hide ();
	}
	void Grow (SlowMoBubble _controlledBubble)
	{
		controlledBubble = _controlledBubble;
		controlledBubble.Trigger ();
		SetTimer (duration * controlledBubble.triggerDurationScalar);
		tickTimer += growerRiseDuration;
		timer -= growerRiseDuration;
		Hide ();
	}
}	