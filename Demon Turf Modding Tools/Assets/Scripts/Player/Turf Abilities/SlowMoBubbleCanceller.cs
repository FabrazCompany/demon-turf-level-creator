﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowMoBubbleCanceller : MonoBehaviour
{

	void OnTriggerEnter (Collider hit)
	{
		if (hit.attachedRigidbody == null)
			return;
			
		var shot = hit?.attachedRigidbody?.GetComponent<SlowMoShot> ();
		shot?.Detonate ();

		var bubble = hit?.attachedRigidbody?.GetComponent<SlowMoBubble> ();
		bubble?.Despawn ();	

		var entity = hit?.attachedRigidbody?.GetComponent<SlowMoEntity> ();
		entity?.ClearTantrum ();
	}
}
