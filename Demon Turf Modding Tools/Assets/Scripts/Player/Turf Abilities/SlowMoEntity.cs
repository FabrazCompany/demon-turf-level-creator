using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowMoEntity : MonoBehaviour, IPathingSpeedModifier
{
	[SerializeField] float slowResistance = 1;
	[SerializeField] bool ignorePhysicsAspect;
    float _timeScale = 1;
    bool first = true;
	[SerializeField] bool log;
    Rigidbody rigid;
    RigidbodyInterpolation cachedInterpolation;
    Animator[] anims;
    List<ParticleSystem> particles;
    Timer[] timers;
	AutoMoveAndRotate[] autoRotates;
    
	SlowMoBubble tantrumController;

	public event Action<SlowMoEntity> onCancelSlowdown;


    public event System.Action<float> onTimeScaleChanged;
    public float timeScale {
		get => _timeScale;
        set {
            if (_timeScale == value)
                return;

            if( !first )
            {
                if (rigid && !ignorePhysicsAspect)
                {
                    rigid.mass *= timeScale;
                    rigid.velocity /= timeScale;
                    rigid.angularVelocity /= timeScale;
                }
            }
            first = false;
            var lastValue = _timeScale;
			_timeScale = Mathf.Abs(value);
			if (log)
				Debug.Log (_timeScale);
    
			if (rigid && !ignorePhysicsAspect)
            {
                rigid.mass /= timeScale;
                rigid.velocity *= timeScale;
                rigid.angularVelocity *= timeScale;
            }

            foreach (var anim in anims)
            {
				if (anim == null)
					continue;
                anim.speed = timeScale;
            }
            foreach (var particle in particles)
            {
				if (particle == null)
					continue;
                var main = particle.main;
                main.simulationSpeed = timeScale;
            }
            foreach (var timer in timers)
            {
				if (timer == null)
					continue;
                timer.SetScalar (timeScale);
            }
			foreach (var rot in autoRotates)
			{
				if (rot == null)
					continue;
				rot.SetScalar (timeScale);
			}
            onTimeScaleChanged?.Invoke (timeScale / lastValue);
        }
    }

    float IPathingSpeedModifier.modifier => timeScale;
    event Action<float> IPathingSpeedModifier.onModValuesUpdated
    {
        add => onTimeScaleChanged += value;
        remove => onTimeScaleChanged -= value;
    }

    protected virtual void Awake () 
    {
        rigid = GetComponent<Rigidbody> ();
        if (rigid && !ignorePhysicsAspect)
            cachedInterpolation = rigid.interpolation;

        anims = GetComponentsInChildren<Animator> (true);
		particles = new List<ParticleSystem> ();
		GetComponentsInChildren (true, particles);
		particles.RemoveAll (i => i.GetComponent<SlowMoIgnore> ());
        timers = GetComponentsInChildren<Timer> (true);
		autoRotates = GetComponentsInChildren<AutoMoveAndRotate> (true);
        timeScale = _timeScale;
     }
	void OnDisable ()
	{
		if (routineRunning)
		{
			routineRunning = false;
		}
		timeScale = 1;
	}
	void OnEnable ()
	{
		timeScale = _timeScale;
	}
    public virtual void Activate (float scale) 
    {
        if (rigid && !ignorePhysicsAspect)
            rigid.interpolation = RigidbodyInterpolation.Interpolate;
		if (individualSlowdownRoutine != null)
			StopCoroutine (individualSlowdownRoutine);
        timeScale = scale * slowResistance;
    }
    public virtual void Deactivate () 
    {
        if (rigid && !ignorePhysicsAspect)
            rigid.interpolation = cachedInterpolation;
		if (individualSlowdownRoutine != null && this != null)
			StopCoroutine (individualSlowdownRoutine);
        timeScale = 1;
    }

    void OnTriggerEnter (Collider hit) 
    {
        if (!rigid && hit.attachedRigidbody)
        {
            var bubble = hit.attachedRigidbody.GetComponent<SlowMoBubble> ();
            bubble?.TryAddResident (this);
        }
    }
    void OnTriggerExit (Collider hit) 
    {
        if (!rigid && hit.attachedRigidbody)
        {
            var bubble = hit.attachedRigidbody.GetComponent<SlowMoBubble> ();
            bubble?.TryRemoveResident (this);
        }
    }
	public void CancelSlowdown () => onCancelSlowdown?.Invoke (this);
	public void SetTantrum (SlowMoBubble bubble) => tantrumController = bubble;
	public void ClearTantrum () 
	{
		if (!tantrumController)
			return;
		tantrumController.Despawn ();
		tantrumController = null;
	}

	public void TriggerIndividualSlowdown (float duration) 
	{
			
		if (individualSlowdownRoutine != null && routineRunning)
		{
			timeScale = individualSlowdownScaleCache;
			if (!ignorePhysicsAspect)
				rigid.velocity = individualSlowdownVelocityCache;
			StopCoroutine (individualSlowdownRoutine);
		}
		individualSlowdownRoutine = StartCoroutine (IndividualSlowdownRoutine (duration));
	}
	bool routineRunning;
	Vector3 individualSlowdownVelocityCache;
	float individualSlowdownScaleCache;
 	Coroutine individualSlowdownRoutine;
	IEnumerator IndividualSlowdownRoutine (float duration)
	{
		routineRunning = true;
		individualSlowdownScaleCache = timeScale;
		individualSlowdownVelocityCache = rigid.velocity;
		yield return new WaitForFixedUpdate ();
		yield return new WaitForFixedUpdate ();
		individualSlowdownScaleCache = timeScale;
		individualSlowdownVelocityCache = rigid.velocity;
		timeScale = 0.01f;
		rigid.velocity = Vector3.zero;
		yield return new WaitForSecondsRealtime (duration);
		timeScale = individualSlowdownScaleCache;
		rigid.velocity = individualSlowdownVelocityCache;
		routineRunning = false;
		individualSlowdownRoutine = null;		
	}
}