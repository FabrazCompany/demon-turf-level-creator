using UnityEngine;

public class SlowMoGateTrigger : MonoBehaviour
{
	[SerializeField] float duration = 5f;
	[SerializeField] float scale = 1f;
	void OnTriggerEnter (Collider hit) 
	{
		if (!hit.TryGetComponent<IPlayerController> (out var player))
			return;
		player.EnterTimeOutGateTrigger (duration, scale);
	}
}