using UnityEngine;
using UnityEngine.Animations;

public class SlowMoShot : MonoBehaviour
#if !DT_EXPORT && !DT_MOD
, IReceiveCameraEffects
#endif
{
	[SerializeField] float scaleMin = .75f;
	[SerializeField] float scaleMax = 1.5f;
	[Space]
	[SerializeField] float speedMin = 5;
	[SerializeField] float speedMax = 8;
	[Space]
	[SerializeField] float durationMin = 1;
	[SerializeField] float duartionMax = 2;
	[Space]
	[SerializeField] float shockwaveForceScalar = 0.75f;
	[SerializeField] float shockwaveDecayScalar = 0.25f;
	[Space]
	[SerializeField] ParticleSystem activeParticles;
	[SerializeField] ParticleSystem triggerParticle;
	[Space]
	[SerializeField] ParticleSystem tantrumParticlesActive;
	[SerializeField] ParticleSystem tantrumParticleBurst;
	[SerializeField] PositionConstraint tantrumLock;	
	[Space]
	[SerializeField] SlowMoBubble bubblePrefab;

	Rigidbody rigid;
	MeshRenderer rend;
	Collider hitbox;
#if !DT_EXPORT && !DT_MOD
	CameraEffects cameraEffects;
#endif
	
	float timer;
	float force;
	bool triggered;
	bool cleaningUp;
	bool isTantrum;
	float tantrumTimeoutScale;

	public event System.Action<SlowMoBubble> onSlowMoBubbleSpawned;
#if !DT_EXPORT && !DT_MOD
	void IReceiveCameraEffects.SetCameraEffects (CameraEffects _cameraEffects) => cameraEffects = _cameraEffects;
#endif
	void Awake () 
	{
		rigid = GetComponent<Rigidbody> ();
		rend = GetComponentInChildren<MeshRenderer> ();
		activeParticles = GetComponentInChildren<ParticleSystem> ();
		hitbox = GetComponentInChildren<Collider> ();
	}
	void OnEnable ()
	{
		triggered = false;
	}

	void OnCollisionEnter (Collision coll) 
	{
		var position = coll.contactCount > 0 ? coll.GetContact (0).point : transform.position;
		if (isTantrum 
			&& coll?.rigidbody?.GetComponent<AIEnemy> () 
			&& coll?.rigidbody?.GetComponent<SlowMoEntity> ())
		{
			TantrumTrigger (coll?.rigidbody?.GetComponent<SlowMoEntity> (), position);
		}
		else 
			Trigger (coll?.rigidbody?.GetComponent<SlowMoEntity> ());

		var knockback = coll.collider.GetComponent<ReceiveKnockback> () ?? coll?.rigidbody?.GetComponent<ReceiveKnockback> ();
		if (knockback)
		{
			knockback.ApplyKnockback (transform.forward, force);
#if !DT_EXPORT && !DT_MOD
			if (cameraEffects)
				cameraEffects.TriggerShockwave (position, force * shockwaveForceScalar, force * shockwaveDecayScalar);
#endif
		}
	}

	void Update () 
	{
		if (!cleaningUp)
		{
			timer -= Time.deltaTime;
			if (timer <= 0) 
				Trigger ();
		}
		else 
		{
			if (activeParticles.isStopped && triggerParticle.isStopped && tantrumParticleBurst.isStopped)
			{
				if (tantrumLock.constraintActive)
				{
					tantrumLock.constraintActive = false;
					if (tantrumLock.sourceCount > 0)
						tantrumLock.RemoveSource (0);
					tantrumLock.transform.localPosition = Vector3.zero;
				}
				TrashMan.despawn (gameObject);
			}
		}
		
	}
	public void LaunchTantrum (float tantrumScale, float chargeLevel, float shotScale, float shotSpeed, float shotKnockback, float shotDuration, float bubScale, float bubDuration, float bubPitch)
	{
		tantrumTimeoutScale = tantrumScale;
		tantrumParticlesActive.Play ();
		Launch (chargeLevel, shotScale, shotSpeed, shotKnockback, shotDuration, bubScale, bubDuration, bubPitch);
		isTantrum = true;
	}
	public void AddForce (Vector3 addedForce)
	{
		var pct = Mathf.Clamp01 (Vector3.Dot (rigid.velocity.normalized, addedForce.normalized));
		rigid.velocity += addedForce * pct;
	}
	public void Launch (float chargeLevel, float shotScale, float shotSpeed, float shotKnockback, float shotDuration, float bubScale, float bubDuration, float bubPitch) 
	{
		transform.localScale = Vector3.one * shotScale;
		rigid.velocity = transform.forward * shotSpeed;
		force = shotKnockback;
		timer = shotDuration;
		rend.enabled = true;
		hitbox.enabled = true;
		cleaningUp = false;
		isTantrum = false;
		bubbleScale = bubScale;
		bubbleDuration = bubDuration;
		bubblePitch = bubPitch;
	}
	float bubbleScale;
	float bubbleDuration;
	float bubblePitch;
	public void Trigger (SlowMoEntity slowMo = null)
	{
		if (triggered)
			return;
		triggered = true;
		Detonate ();		
		var bubble = TrashMan.spawn (bubblePrefab.gameObject, transform.position, Quaternion.identity).GetComponent<SlowMoBubble> ();
		bubble.Trigger (bubbleScale, bubbleDuration, bubblePitch);
		if (slowMo && !slowMo.GetComponent<SlowMoIgnore> ())
			bubble.TryAddResident (slowMo);
		onSlowMoBubbleSpawned?.Invoke (bubble);
	}
	void TantrumTrigger (SlowMoEntity entity, Vector3 hitPoint)
	{
		tantrumLock.transform.position = hitPoint;
		tantrumLock.AddOrSet (0, entity.transform);
		tantrumLock.constraintActive = true;

		Detonate ();

		var bubble = TrashMan.spawn (bubblePrefab.gameObject, transform.position, Quaternion.identity).GetComponent<SlowMoBubble> ();
		bubble.TantrumTrigger (entity, tantrumTimeoutScale, bubbleDuration, bubblePitch);
		onSlowMoBubbleSpawned?.Invoke (bubble);
	}
	public void Detonate () 
	{
		rigid.velocity = Vector3.zero;
		rend.enabled = false;
		hitbox.enabled = false;
		cleaningUp = true;
		activeParticles.Stop ();
		triggerParticle.Play ();
		if (isTantrum) 
		{
			tantrumParticleBurst.Play (true);
		}
			
	}
	public void IgnoreCollider (Collider ignored)
    {
		Physics.IgnoreCollision (ignored, hitbox, true);
    }
}