using UnityEngine;

public class AmbientSound : MonoBehaviour 
{
	[SerializeField] AudioSource audioSource;
	[SerializeField] AudioSourceExtender audioSourceExtender;
	[SerializeField] AudioLowPassFilter lowPassFilter;
	[SerializeField] AudioHighPassFilter highPassFilter;
	[SerializeField] bool outdoors;
#if !DT_EXPORT && !DT_MOD
	CameraEffects camEffects;
#endif
	bool cameraSubmerged;

	void OnValidate ()
	{
		if (!audioSource)
			audioSource = GetComponent<AudioSource> ();
		if (!audioSourceExtender)
			audioSourceExtender = GetComponent<AudioSourceExtender> ();
		if (!lowPassFilter)
			lowPassFilter = GetComponent<AudioLowPassFilter> ();
		if (!highPassFilter)
			highPassFilter = GetComponent<AudioHighPassFilter> ();
	}
	void OnEnable ()
	{
		activeState = !outdoors;
		PlayerShelteredTrigger.onAmbienceUpdated += OnAmbienceUpdated;
		PlayerControllerEvents.onPlayerRepositionedStatic += OnPlayerRepositioned;
	}
	void Start ()
	{
#if !DT_EXPORT && !DT_MOD
		if (Camera.main.TryGetComponent<CameraEffects> (out var cameraEffects))
		{
			camEffects = cameraEffects;
			cameraSubmerged = camEffects.getSubmerged;
			camEffects.onSubmergedStatusChanged += OnSubmersionChanged;
		}
#endif
	}
	void OnDisable ()
	{
		PlayerShelteredTrigger.onAmbienceUpdated -= OnAmbienceUpdated;
		PlayerControllerEvents.onPlayerRepositionedStatic -= OnPlayerRepositioned;
	}
	void OnDestroy ()
	{
#if !DT_EXPORT && !DT_MOD
		if (camEffects)
		{
			camEffects.onSubmergedStatusChanged -= OnSubmersionChanged;
		}
#endif
	}

	void OnPlayerRepositioned (Vector3 pos)
	{
		if (lowPassFilter)
			lowPassFilter.enabled = false;
		if (highPassFilter)
			highPassFilter.enabled = false;
	}
	void OnSubmersionChanged (bool submerged)
	{
		if (cameraSubmerged == submerged) return;
		cameraSubmerged = submerged;
		if (highPassFilter)
			highPassFilter.enabled = submerged || activeState;
		if (lowPassFilter)
			lowPassFilter.enabled = submerged || activeState;

		if (submerged || activeState)
			audioSourceExtender.FadeOut (0.5f);
		else if (audioSource.volume == 0 || audioSourceExtender.getFadeTarget == 0)
			audioSourceExtender.FadeIn (0.5f);
	}
	bool activeState;
	void OnAmbienceUpdated (PlayerShelteredTrigger ambience)
	{
		var active = ambience 
					&& (outdoors ? !ambience.isIndoors : ambience.isIndoors) 
					&& ambience.ambience.lowersAmbience;
		activeState = active;
		if (lowPassFilter)
			lowPassFilter.enabled = active;
		if (highPassFilter)
			highPassFilter.enabled = active;
		
		if (ambience)
		{
			if (ambience.isIndoors && ambience.ambience.muteAmbeince)
				audioSourceExtender.FadeOut (0.5f);
			else if (audioSource.volume == 0 || audioSourceExtender.getFadeTarget == 0)
				audioSourceExtender.FadeIn (0.5f);
		}
	}
}