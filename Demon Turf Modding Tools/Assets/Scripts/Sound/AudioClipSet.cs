using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AudioClipSet 
{
    [SerializeField] AudioClip[] clips;
	[SerializeField, Range (0, 1)] float playChance = 1;
    public AudioClip GetSound => clips?.Length == 0 || Random.value > playChance ? null : clips[Random.Range (0, clips.Length)];
    public bool HasSound => clips != null && clips.Length > 0;
}