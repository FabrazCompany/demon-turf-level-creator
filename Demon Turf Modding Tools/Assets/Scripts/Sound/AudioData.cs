[System.Serializable]
public struct AudioData {
	public float minDistance;
	public float maxDistance;
	public bool is3D;
	public float volume;
	public bool isLooping;
	public bool autoDestroy;
	public float pitchRange;
	public int pitchRangeDirection;
	public float stereoPan;
	public float spread;
	public bool ignoreBuffer;

	public bool notSet;
	public AudioData (
		float _min = 1, 
		float _max = 5, 
		bool _is3D = false, 
		float _stereoPan = 0,
		float _volume = 1, 
		float _pitchRange = 0, 
		int _pitchRangeDir = 0,
		bool _isLooping = false,
		bool _autoDestroy = true,
		float _spread = 0,
		bool _ignoreBuffer = false) 
	{
		minDistance = _min;
		maxDistance = _max;
		is3D = _is3D;
		stereoPan = _stereoPan;
		volume = _volume;
		isLooping = _isLooping;
		autoDestroy = _autoDestroy;
		pitchRange = _pitchRange;
		pitchRangeDirection = _pitchRangeDir;
		spread = _spread;
		ignoreBuffer = _ignoreBuffer;
		notSet = false;
	}
}
