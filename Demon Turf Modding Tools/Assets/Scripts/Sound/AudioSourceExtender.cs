﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (AudioSource))]
public class AudioSourceExtender : MonoBehaviour
{
	[SerializeField] new AudioSource audio;
    float cachedVolume; 
	bool startHasRun;
	void OnValidate ()
	{
		if (!audio)
			audio = GetComponent<AudioSource> ();
	}
	void Awake () 
	{
		audio = GetComponent<AudioSource> ();
		cachedVolume = audio.volume;
	}
    void Start ()
    {
        var mixerTarget = SoundManagerStub.Instance.GetMixerTarget (audio.outputAudioMixerGroup);
        audio.ignoreListenerPause = mixerTarget == SoundMixerTarget.Music || mixerTarget == SoundMixerTarget.UI;
		startHasRun = true;
    }

    public void ResetVolume () => audio.volume = startHasRun ? cachedVolume : 1; 
    public void FadeIn (float duration) => Fade (cachedVolume, duration);
    public void FadeOut (float duration) => Fade (0, duration);
    
	float fadeTarget;
	public float getFadeTarget => fadeTarget;
	void Fade (float target, float duration)
	{
		if (!gameObject.activeInHierarchy)
		{
			audio.volume = target;
			return;
		}
		if (!audio)
			return;
		Debug.Log (target, this);
		this.RestartCoroutine (FadeRoutine (target, duration), ref fadeRoutine);
	}
    Coroutine fadeRoutine;
    IEnumerator FadeRoutine (float target, float duration) 
    {
		fadeTarget = target;
        var start = audio.volume;
        var timer = -Time.deltaTime;
        if (target > 0 && !audio.isPlaying)
            audio.Play ();
		if (duration > 0)
		{
			while (timer < duration)
			{
				timer += Time.deltaTime;
				audio.volume = Mathf.Lerp (start, target, timer / duration);
				yield return null;
			}
		}
        audio.volume = target;
        if (target == 0)
            audio.Stop ();
    }


    public void SetUseGameplayFilter (bool useFiltered) 
    {
        audio.outputAudioMixerGroup = SoundManagerStub.Instance.GetGameplayMixerGroup (useFiltered);
    }

    public void PlayFromNormalizedPoint (float normalizedTime) 
    {
        audio.Play ();
        audio.time = normalizedTime * audio.clip.length;
    }
    [ContextMenu ("Trigger Random")]
    public void PlayFromRandomizedPoint () => PlayFromNormalizedPoint (Random.value);

    // public void SnapshotTransition_AddRoom (float delay) => SoundManager?.Instance.AddSnapshot (SoundManager.Snapshots.)
}
