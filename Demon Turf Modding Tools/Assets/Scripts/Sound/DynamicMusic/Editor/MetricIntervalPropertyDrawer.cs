﻿using System;
using UnityEditor;
using UnityEditor.AddressableAssets.GUI;
using UnityEditor.UIElements;
using UnityEngine;

[CustomPropertyDrawer(typeof(MetricInterval), true)]
public class MetricIntervalPropertyDrawer : PropertyDrawer 
{
	private const int padding = 5;

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) 
	{
		EditorGUI.BeginProperty(position, label, property);
		
		position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
    
		var indent = EditorGUI.indentLevel;
    EditorGUI.indentLevel = 0;
    
		var rect = new Rect(position.x, position.y, 30, position.height);
		var measuresProperty = property.FindPropertyRelative("measures");
    EditorGUI.PropertyField(rect, measuresProperty, GUIContent.none, true);
    
    rect.x += rect.width + padding;
    rect.width = 30;
    EditorGUI.LabelField(rect, "bars");
		
		rect.x += + rect.width + padding;
		rect.width = 80;
		var beatsProperty = property.FindPropertyRelative("beats");
		EditorGUI.BeginChangeCheck();
		EditorGUI.PropertyField(rect, beatsProperty, GUIContent.none, true);
		if (EditorGUI.EndChangeCheck())
		{
			property.serializedObject.ApplyModifiedProperties(); // Make sure we capture the existing change
			string fieldLabel = "";
			var time = property.GetActualObjectForSerializedProperty<MetricInterval>(fieldInfo, ref fieldLabel);
			var reducedTime = time.Reduced();
			if (!time.Equals(reducedTime))
			{
				measuresProperty.intValue = reducedTime.measures;
				beatsProperty.FindPropertyRelative("numerator").intValue = reducedTime.beats.numerator;
				beatsProperty.FindPropertyRelative("denominator").intValue = reducedTime.beats.denominator;
				property.serializedObject.ApplyModifiedProperties();
			}
		}
		
    rect.x += rect.width + padding;
    rect.width = 50;
    EditorGUI.LabelField(rect, "beats");
		
		EditorGUI.indentLevel = indent;
		
		EditorGUI.EndProperty();
	}
}
