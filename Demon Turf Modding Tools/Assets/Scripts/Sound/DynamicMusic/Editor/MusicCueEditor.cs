﻿using System;
using System.Collections.Generic;
using MidiParser;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MusicCue), true)]
public class MusicCueEditor : Editor
{
	private static MusicCue previewCue = null;

	private int currentSection = -1;

	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		base.OnInspectorGUI();

		var cue = (MusicCue)target;

		// Import button
		if (GUILayout.Button("Import MIDI Map"))
		{
			ImportMidiMap(cue);
		}

		GUILayout.BeginHorizontal();
		// Play preview button
		var buttonStyle = new GUIStyle();
		buttonStyle.padding.left = 4;
		buttonStyle.padding.top = 10;
		bool playing = previewCue == target;
		Texture play = EditorGUIUtility.Load("PlayButton.png") as Texture;
		Texture stop = EditorGUIUtility.Load("StopButton.png") as Texture;
		if (GUILayout.Button(playing ? stop : play, buttonStyle, GUILayout.ExpandWidth(false)))
		{
			if (playing)
			{
				StopPreview();
			}
			else
			{
				PlayPreview(cue, currentSection);
			}
		}

		// Jump ahead button
		GUILayout.BeginVertical();
		GUILayout.FlexibleSpace();

		if (GUILayout.Button("Skip to End", GUILayout.ExpandHeight(true)))
		{
			SkipToEnd();
		}

		if (GUILayout.Button("Next Track", GUILayout.ExpandHeight(true)))
		{
			NextSection();
		}

		GUILayout.FlexibleSpace();
		GUILayout.EndVertical();

		GUILayout.BeginVertical();
		GUILayout.FlexibleSpace();

		EditorGUILayout.LabelField("Current Section");
		var sections = cue.GetSections();
		var sectionNames = new string[sections.Length];
		for (int i = 0; i < sections.Length; ++i)
		{
			sectionNames[i] = $"{i}: {sections[i].displayName}";
		}
		int activeSection = cue.GetActiveSectionIndex();
		currentSection = EditorGUILayout.Popup(activeSection, sectionNames);
		if (activeSection != currentSection)
		{
			cue.Transition(currentSection);
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndVertical();

		GUILayout.EndHorizontal();
		serializedObject.ApplyModifiedProperties();
	}

	private static void PlayPreview(MusicCue cue, int currentSection)
	{
		StopPreview();
		previewCue = cue;
		previewCue.Play(currentSection >= 0 ? currentSection : 0);
	}

	private static void StopPreview()
	{
		if (previewCue == null) return;
		previewCue.Stop();
		previewCue = null;
	}

	private static void SkipToEnd()
	{
		if (previewCue == null) return;
		var section = previewCue.GetActiveSection();
		if (section == null) return;
		if (section.GetLoopPoints(out _, out var endTime))
		{
			section.trackTime = endTime.ToSeconds(section) - 3.0f;
		}
	}

	private static void NextSection()
	{
		if (previewCue == null) return;
		var section = previewCue.GetActiveSection();
		if (section == null) return;
		previewCue.Transition(previewCue.GetActiveSectionIndex() + 1);
	}

	private static void AddSection(MusicCue cue, Dictionary<string, MusicSection> existingSections, TempoEvent[] tempos, MeterEvent[] meters, string name, float startTime, float? endTime = null)
	{
		if (existingSections.TryGetValue(name, out var section))
		{
			existingSections.Remove(name);
		}
		else
		{
			section = cue.gameObject.AddComponent<MusicSection>();
			section.displayName = name;
		}
		section.SetMeterMap(tempos, meters);
		var startTimeInterval = new TimeInterval(startTime).ConvertTo(TimeInterval.Type.Metric, section);
		section.SetStartTime(startTimeInterval);
		if (endTime != null)
		{
			var endTimeInterval = new TimeInterval(endTime.Value).ConvertTo(TimeInterval.Type.Metric, section);
			section.SetLoopPoints(startTimeInterval, endTimeInterval);
		}
		section.SendMessage("OnValidate", null, SendMessageOptions.DontRequireReceiver);
	}

	private static void ImportMidiMap(MusicCue cue)
	{
		string path = EditorUtility.OpenFilePanel("Import MIDI Map", "", "mid");
		if (string.IsNullOrEmpty(path)) return;
		var midiFile = new MidiFile(path);
		var tempoTrack = FindTempoTrack(midiFile);
		if (tempoTrack == null)
		{
			Debug.LogError("No tempo track found. Aborting import...");
			return;
		}
		var data = ParseTempoTrack(tempoTrack, midiFile.TicksPerQuarterNote);
		var existingSections = new Dictionary<string, MusicSection>();
		foreach (var section in cue.GetComponentsInChildren<MusicSection>())
		{
			if (existingSections.ContainsKey(section.displayName))
			{
				// Can't handle duplicates
				DestroyImmediate(section);
				continue;
			}
			existingSections[section.displayName] = section;
		}

		MarkerEvent firstMarker = default;
		for (int i = 0; i < data.markers.Length; ++i)
		{
			ref var marker = ref data.markers[i];
			if (!string.IsNullOrWhiteSpace(marker.text))
			{
				if (string.IsNullOrWhiteSpace(firstMarker.text))
				{
					// Found the start of a section
					firstMarker = marker;
				}
				continue;
			}
			// Get our existing section or add a new one
			var sectionName = firstMarker.text ?? "";
			AddSection(cue, existingSections, data.tempos, data.meters, sectionName, firstMarker.time, marker.time);
			firstMarker = default;
		}
		if (!string.IsNullOrWhiteSpace(firstMarker.text))
		{
			AddSection(cue, existingSections, data.tempos, data.meters, firstMarker.text ?? "", firstMarker.time);
		}

		// Remove any unused sections
		foreach (var e in existingSections)
		{
			DestroyImmediate(e.Value);
		}

		cue.SendMessage("OnValidate", null, SendMessageOptions.DontRequireReceiver);
	}

	private static MidiTrack FindTempoTrack(MidiFile midiFile)
	{
		foreach (var track in midiFile.Tracks)
		{
			foreach (var e in track.MidiEvents)
			{
				if (e.MidiEventType == MidiEventType.MetaEvent && (e.MetaEventType == MetaEventType.Tempo || e.MetaEventType == MetaEventType.TimeSignature))
				{
					return track;
				}
			}
		}
		return null;
	}

	private struct TempoTrackData
	{
		public TempoEvent[] tempos;
		public MeterEvent[] meters;
		public MarkerEvent[] markers;
	}

	private static TempoTrackData ParseTempoTrack(MidiTrack tempoTrack, float ticksPerQuarterNote)
	{
		var tempos = new List<TempoEvent>();
		var meters = new List<MeterEvent>();
		var markers = new List<MarkerEvent>();
		float currentBPM = 120.0f;
		var currentMeter = RationalNumber.commonTime;
		float tickDuration = (60.0f / currentBPM) / ticksPerQuarterNote; // in seconds
		float currentTime = 0.0f;
		float currentMeasures = 0.0f;
		int currentTick = 0;
		int i = 0;
		foreach (var e in tempoTrack.MidiEvents)
		{
			i = AddMarkersUntil(tempoTrack, i, currentTick, e.Time, currentTime, tickDuration, markers);
			int ticks = e.Time - currentTick;
			currentTime += ticks * tickDuration;
			float quarterNotes = ticks / ticksPerQuarterNote;
			currentMeasures += quarterNotes / currentMeter.quarterNotes;
			currentTick = e.Time;
			switch (e.MetaEventType)
			{
				case MetaEventType.Tempo:
					currentBPM = (float)Math.Round(6.0e7 / e.Arg2, 2);
					tickDuration = (60.0f / currentBPM) / ticksPerQuarterNote;
					if (tempos.Count > 0 && Math.Abs(tempos[tempos.Count - 1].time - currentTime) < 0.00001)
					{
						// Don't allow multiple events at the same time
						tempos.RemoveAt(tempos.Count - 1);
					}
					tempos.Add(new TempoEvent(currentTime, currentMeasures, currentBPM));
					break;
				case MetaEventType.TimeSignature:
					currentMeter = new RationalNumber(e.Arg2, e.Arg3);
					if (meters.Count > 0 && Math.Abs(meters[meters.Count - 1].time - currentTime) < 0.00001)
					{
						// Don't allow multiple events at the same time
						meters.RemoveAt(meters.Count - 1);
					}
					meters.Add(new MeterEvent(currentTime, currentMeasures, currentMeter));
					break;
			}
		}

		AddMarkersUntil(tempoTrack, i, currentTick, Int32.MaxValue, currentTime, tickDuration, markers);
		return new TempoTrackData { tempos = tempos.ToArray(), meters = meters.ToArray(), markers = markers.ToArray() };
	}

	private static int AddMarkersUntil(MidiTrack tempoTrack, int i, int currentTick, int untilTick, float currentTime, float tickDuration, List<MarkerEvent> markers)
	{
		while (i < tempoTrack.TextEvents.Count && tempoTrack.TextEvents[i].Time <= untilTick)
		{
			var textEvent = tempoTrack.TextEvents[i];
			if (textEvent.TextEventType == TextEventType.Marker)
			{
				float time = currentTime + (textEvent.Time - currentTick) * tickDuration;
				markers.Add(new MarkerEvent(time, textEvent.Value));
			}
			++i;
		}
		return i;
	}
}
