﻿using System;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;

[CustomPropertyDrawer(typeof(RationalNumber), true)]
public class RationalNumberPropertyDrawer : PropertyDrawer 
{
	private const int padding = 5;
	private const int boxWidth = 30;
	
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) 
	{
		EditorGUI.BeginProperty(position, label, property);
		
		position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
    
		var indent = EditorGUI.indentLevel;
    EditorGUI.indentLevel = 0;
    
		var rect = new Rect(position.x, position.y, boxWidth, position.height);
    EditorGUI.PropertyField(rect, property.FindPropertyRelative("numerator"), GUIContent.none, true);
    
    rect.x += rect.width + padding;
    rect.width = 8;
    EditorGUI.LabelField(rect, "/");
		
		rect.x += + rect.width + padding;
		rect.width = boxWidth;
		EditorGUI.PropertyField(rect, property.FindPropertyRelative("denominator"), GUIContent.none, true);
		
		EditorGUI.indentLevel = indent;
		
		EditorGUI.EndProperty();
	}
}
