﻿using System;
using System.Reflection;
using UnityEditor;
using UnityEditor.AddressableAssets.GUI;
using UnityEngine;

[CustomPropertyDrawer(typeof(TimeInterval), true)]
public class TimeIntervalPropertyDrawer : PropertyDrawer 
{
	private const int padding = 5;
	
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) 
	{
		EditorGUI.BeginProperty(position, label, property);
		
		position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
		
		var indent = EditorGUI.indentLevel;
		EditorGUI.indentLevel = 0;
		
		var typeProperty = property.FindPropertyRelative("type");
		var type = (TimeInterval.Type)typeProperty.enumValueIndex;
		var timeProperty = property.FindPropertyRelative(GetPropertyName(type));

		if (property.serializedObject.targetObject is IMusicTemporalMap map &&
				GUI.Button(new Rect(position.x - position.height, position.y, position.height, position.height), "⇄"))
		{
			var time = (TimeInterval)fieldInfo.GetValue(map);
			var toggledTime = time.ConvertTo(type == TimeInterval.Type.Metric ? TimeInterval.Type.Realtime : TimeInterval.Type.Metric, map);
			Undo.RecordObject(property.serializedObject.targetObject, "Toggle time type");
			fieldInfo.SetValue(map, toggledTime);
		}
		
		var rect = new Rect(position.x, position.y, 100, position.height);
		EditorGUI.PropertyField(rect, typeProperty, GUIContent.none, true);

		rect.x += rect.width + padding;
		rect.width = 50;
		EditorGUI.PropertyField(rect, timeProperty, GUIContent.none, true);

		if (type == TimeInterval.Type.Realtime)
		{
			rect.x += rect.width + padding;
			rect.width = 100;
			EditorGUI.LabelField(rect, "seconds");
		}
		
		EditorGUI.indentLevel = indent;
		
		EditorGUI.EndProperty();
	}

	private string GetPropertyName(TimeInterval.Type type)
	{
		switch (type)
		{
		case TimeInterval.Type.Realtime: return "realtimeInterval";
		case TimeInterval.Type.Metric: return "metricInterval";
		default: throw new ArgumentException($"Unknown TimeInterval type '{type}'");
		}
	}
}
