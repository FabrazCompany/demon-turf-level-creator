

public interface IDynamicMusic 
{
	bool IsPlaying ();
	int GetActiveSectionIndex ();
	void Stop ();
	void Play (int section = 0);
	void Transition (int section);
}