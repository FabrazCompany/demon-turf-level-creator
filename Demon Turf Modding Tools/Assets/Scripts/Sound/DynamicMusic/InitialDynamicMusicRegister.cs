using UnityEngine;

public class InitialDynamicMusicRegister : MonoBehaviour 
{	
	[SerializeField] MusicCue music;
	public void Register ()
	{
		SoundManagerStub.Instance.PlayBackgroundMusic (music);
		music.Pause ();
	}
	public void Play ()
	{
		SoundManagerStub.Instance.PlayBackgroundMusic (music);
	}
}