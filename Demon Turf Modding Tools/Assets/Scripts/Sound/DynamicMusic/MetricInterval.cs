﻿using System;
using UnityEngine;

[Serializable]
public struct MetricInterval : IComparable<MetricInterval> {
  [Delayed] public int measures;
  public RationalNumber beats;

  public bool empty => measures == 0 && beats.numerator == 0;
  
  public float fractionalMeasures => measures + beats.ratio; 
  
  public MetricInterval(int m)
  {
    beats = default;
    measures = m;
  }
  
  public MetricInterval(RationalNumber b)
  {
    beats = b;
    measures = 0;
  }
  
  public MetricInterval(int m, RationalNumber b)
  {
    beats = b;
    measures = m;
  }
  
  public MetricInterval(double time, IMusicTemporalMap map)
  {
    var tempo = map.GetTempoEventBeforeTime(time);
    var meter = map.GetMeterEventBeforeTime(time);
    double lastEventTime = Math.Max(tempo.time, meter.time);
    float lastEventMeasures = Mathf.Max(tempo.measure, meter.measure);
    double timeAfterEvent = time - lastEventTime;
    
    float quarterNotesPerMeasure = meter.timeSignature.quarterNotes;
    float quarterNotes = tempo.tempo / 60.0f * (float)timeAfterEvent;
    float totalMeasures = lastEventMeasures + quarterNotes / quarterNotesPerMeasure; 
    measures = (int)totalMeasures;
    beats = default;
    beats.quarterNotes = quarterNotesPerMeasure*(totalMeasures - measures);
    Reduce();
  }
  
  public void Reduce()
  {
    if (Mathf.Abs(beats.numerator) < beats.denominator || beats.denominator == 0) return;
    measures += beats.numerator / beats.denominator;
    beats.numerator %= beats.denominator;
  }
  
  public MetricInterval Reduced()
  {
    MetricInterval i = this;
    i.Reduce();
    return i;
  }
  
  public double ToSeconds(IMusicTemporalMap map)
  {
    float totalMeasures = fractionalMeasures;
    var tempo = map.GetTempoEventBeforeMeasure(totalMeasures);
    var meter = map.GetMeterEventBeforeMeasure(totalMeasures);
    double lastEventTime = Math.Max(meter.time, tempo.time);
    float lastEventMeasures = Math.Max(meter.measure, tempo.measure);
    Debug.Assert(lastEventMeasures <= measures);
		
    double measureDuration = meter.timeSignature.ToSeconds(tempo.tempo);
    double timeSinceLastEvent = measureDuration * (totalMeasures - lastEventMeasures);
    return lastEventTime + timeSinceLastEvent;
  }
  
  /// <summary>
  /// Converts a metric interval in beats and measures to a duration in seconds. 
  /// </summary>
  /// <param name="bpm">Tempo in quarter note beats per minute</param>
  /// <param name="timeSignature">Time signature ratio, e.g. 4/4</param>
  /// <returns></returns>
  public double ToSeconds(float bpm, RationalNumber timeSignature) {
    float quarterNotes = 0.0f;
    if (measures > 0) {
      // the numerator represents number of measures
      quarterNotes = timeSignature.quarterNotes * measures;
    }
    quarterNotes += beats.quarterNotes;
    return quarterNotes / bpm * 60;
  }

  public int CompareTo(MetricInterval other)
  {
    var lhs = Reduced();
    var rhs = other.Reduced();
    if (lhs.measures > rhs.measures) return 1;
    if (lhs.measures < rhs.measures) return -1;
    var lhsQ = lhs.beats.quarterNotes;
    var rhsQ = lhs.beats.quarterNotes;
    if (lhsQ > rhsQ) return 1;
    if (lhsQ < rhsQ) return -1;
    return 0;
  }
}
