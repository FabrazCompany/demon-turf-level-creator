[System.Serializable]
public struct MusicAutoTransition
{
  public TimeInterval triggerTime;
	public MusicTransition transition;
}