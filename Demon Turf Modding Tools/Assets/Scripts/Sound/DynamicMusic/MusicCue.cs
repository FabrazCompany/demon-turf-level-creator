using System;
using UnityEngine;

[ExecuteAlways]
public class MusicCue : MonoBehaviour, IDynamicMusic {
	[SerializeField] private bool playOnStart;
	[SerializeField] private MusicSection[] sections;
	[SerializeField] private TimeInterval fadeInDuration = new TimeInterval(new MetricInterval(new RationalNumber(1, 4)));
	[SerializeField] private TimeInterval fadeOutDuration = new TimeInterval(new MetricInterval(1));
	[SerializeField] private MusicTransition[] transitions;
	[SerializeField] private MusicTransition defaultTransition = new MusicTransition {
		from = -1,
		to = -1,
		fadeInDuration = new TimeInterval(new MetricInterval(new RationalNumber(1, 4))),
		fadeOutDuration = new TimeInterval(new MetricInterval(1)),
		quantized = true,
		quantization = new TimeInterval(new MetricInterval(1))
	};
	
	private const double latency = 0.5;

	private int activeSection = -1;
	
	private void Start()
	{
		if (playOnStart && Application.isPlaying)
		{
			Play();
		}
	}

	public MusicSection[] GetSections()
	{
		return sections;
	}
	
	public MusicSection GetActiveSection()
	{
		return activeSection >= 0 && activeSection < sections.Length ? sections[activeSection] : null;
	}
	
	public int GetActiveSectionIndex()
	{
		return activeSection;
	}
	
	public void Play(int section = 0)
	{
		activeSection = section;
		sections[activeSection].PlayAtTime(SoundManagerStub.dspTime + latency, fadeInDuration);
	}
	
	public void Transition(int section)
	{
		TransitionAtTime(section, SoundManagerStub.dspTime + latency);
	}
	
	public bool IsPlaying()
	{
		return activeSection >= 0;
	}
	
	public void Stop()
	{
		foreach (var section in sections)
		{
		section.StopAtTime(SoundManagerStub.dspTime + latency, fadeOutDuration, default);
		}
		activeSection = -1;
	}
	public void Pause ()
	{
		foreach (var section in sections)
		{
			section.StopAtTime(SoundManagerStub.dspTime + latency, fadeOutDuration, default);
		}
	}
	public void Resume ()
	{
		if (activeSection < 0) return;
		//   activeSection = section;
		sections[activeSection].PlayAtTime(SoundManagerStub.dspTime + latency, fadeInDuration);
	}
	private ref readonly MusicTransition GetTransition(int from, int to)
	{
		if (transitions == null) return ref defaultTransition;
		for (int i = 0; i < transitions.Length; ++ i)
		{
			ref readonly var t = ref transitions[i];
			if ((t.from < 0 || t.from == from) && (t.to < 0 || t.to == to)) return ref t;
		}
		return ref defaultTransition;
	}
	
	public void TransitionAtTime(int section, double time)
	{
		TransitionAtTime(in GetTransition(activeSection, section), section, time);
	}
	
	public void TransitionAtTime(in MusicTransition transition, int section, double scheduledTime)
	{
		if (activeSection == section) return;
		double? currentTimeInTrack = null;
		bool hasNextSection = IsPlaying() && activeSection >= 0 && activeSection < sections.Length;
		if (activeSection >= 0 && activeSection < sections.Length)
		{
			var s = sections[activeSection];
			currentTimeInTrack = s.trackTime;
			if (transition.quantized)
			{
				var scheduledDelay = scheduledTime - SoundManagerStub.dspTime; 
				var timeInTrack = currentTimeInTrack.Value + scheduledDelay;
				var quantizedTime = s.Quantize(timeInTrack, transition.quantization);
				var diff = quantizedTime - timeInTrack;
				scheduledTime += diff;
			}
		TimeInterval delay = hasNextSection ? sections[section].GetPrelapDuration() : default;
		sections[activeSection].StopAtTime(scheduledTime, transition.fadeOutDuration, delay);
		}
		// Debug.Log($"Transitioning to section {section} from section {activeSection}: ");
		activeSection = section;
		if (hasNextSection)
		{
			double? targetTimeInTrack = null;
			if (transition.relative && currentTimeInTrack != null)
			{
			targetTimeInTrack = currentTimeInTrack.Value + (scheduledTime - SoundManagerStub.dspTime);
			}
			sections[activeSection].PlayAtTime(scheduledTime, transition.fadeInDuration, targetTimeInTrack);
		}
	}
	
	private void OnValidate()
	{
		sections = GetComponentsInChildren<MusicSection>();
	}

	[ContextMenu ("Reset Sections Volume")]
	private void SetMusicSectionsVolume ()
	{
		for (int i = 0; i < sections.Length; i++)
		{
			var section = sections[i];
			var type = typeof (MusicSection);
			var fieldInfo = type.GetField ("volume", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
			fieldInfo.GetValue (section);
			fieldInfo.SetValue (section, 1f);
		}
	}

	public void SetDefaultValues ()
	{
		sections = GetComponentsInChildren<MusicSection> ();
		foreach (var section in sections)
		{
			section.SetDefaultValues ();
		}
	}
}