using System.Collections.Generic;

public interface MusicEvent
{
	double GetTime();
	float GetMeasure();
}

[System.Serializable]
public struct MarkerEvent
{
	public float time;
	public string text;

	public MarkerEvent(float time, string text)
	{
		this.time = time;
		this.text = text;
	}
}

[System.Serializable]
public struct MeterEvent : MusicEvent
{
	public double time;
	public float measure;
	public RationalNumber timeSignature;
	
	public double GetTime() => time;
	public float GetMeasure() => measure;
	
	public MeterEvent(float time, float measure, RationalNumber timeSignature)
	{
		this.time = time;
		this.measure = measure;
		this.timeSignature = timeSignature;
	}
}

[System.Serializable]
public struct TempoEvent : MusicEvent
{
	public double time;
	public float measure;
	public float tempo;
	
	public double GetTime() => time;
	public float GetMeasure() => measure;
	
	public TempoEvent(float time, float measure, float tempo)
	{
		this.time = time;
		this.measure = measure;
		this.tempo = tempo;
	}
}

public class MusicEventTimeComparer<T> : IComparer<T> where T : struct, MusicEvent {
	public int Compare(T lhs, T rhs) {
    return Comparer<double>.Default.Compare(lhs.GetTime(), rhs.GetTime());
  }
}

public class MusicEventMetricComparer<T> : IComparer<T> where T : struct, MusicEvent {
	public int Compare(T lhs, T rhs) {
    return Comparer<float>.Default.Compare(lhs.GetMeasure(), rhs.GetMeasure());
  }
}