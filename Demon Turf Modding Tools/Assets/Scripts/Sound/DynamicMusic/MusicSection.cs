using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Debug = UnityEngine.Debug;

public interface IMusicTemporalMap
{
	RationalNumber GetMeterAtTime(double timeInTrack);
	MeterEvent GetMeterEventBeforeTime(double timeInTrack);
	MeterEvent GetMeterEventBeforeMeasure(float measure);
	float GetTempoAtTime(double timeInTrack);
	TempoEvent GetTempoEventBeforeTime(double timeInTrack);
	TempoEvent GetTempoEventBeforeMeasure(float measure);
}

[ExecuteAlways]
public class MusicSection : MonoBehaviour, IMusicTemporalMap
{
	[Header("General")]
	[SerializeField, ReadOnly] private MusicCue cue;
	public string displayName;
	[SerializeField, Range (0, 1)] private float volume = 1.0f;
	[SerializeField] private AudioClip[] layers;

	[Header("Metadata")]
	[SerializeField] private TimeInterval startTime;
	[SerializeField] private MeterEvent[] meters;
	[SerializeField] private TempoEvent[] tempos;
	[SerializeField, Tooltip("Amount of preceding time before the true downbeat of this section.")]
	private TimeInterval prelapDuration;

	[Header("Looping")]
	[SerializeField] private bool looping;
	[SerializeField] private TimeInterval loopStartTime;
	[SerializeField] private TimeInterval loopEndTime;
	[SerializeField] private TimeInterval loopFadeInDuration = new TimeInterval(0.0f);
	[SerializeField] private TimeInterval loopFadeOutDuration = new TimeInterval(2.0f);
	
	[Header("Auto Transitions")]
	[SerializeField] private MusicAutoTransition[] autoTransitions;

	[Header("Layers")]
	[SerializeField] private MusicVoice.LayerMask _layerMask = MusicVoice.LayerMask.AllLayers;
	[SerializeField] private TimeInterval layerFadeInDuration = new TimeInterval(0.25f);
	[SerializeField] private TimeInterval layerFadeOutDuration = new TimeInterval(2.0f);

	// Precomputed times in seconds
	[Header("Calculated Times")]
	[SerializeField, ReadOnly] private double startTimeSeconds;
	[SerializeField, ReadOnly] private double loopStartTimeSeconds;
	[SerializeField, ReadOnly] private double loopEndTimeSeconds;
	[SerializeField, ReadOnly] private double[] transitionTimes;

	private List<MusicVoice> voices;
	
	private double lastDspTime;
	private double lastTrackTime;

	private const double loopLookahead = 1.0;
	private const double autoTransitionLookahead = 0.5;

	private void OnValidate()
	{
		double loopStart = loopStartTime.ToSeconds(this);
		if (looping && loopEndTime.ToSeconds(this) <= loopStart)
		{
			double time = Math.Max(loopStart, loopEndTime.realtimeInterval);
			loopEndTime.FromSeconds(time, this);
		}
		
		if (tempos == null)
		{
			tempos = new TempoEvent[0];
		}
		if (meters == null)
		{
			meters = new MeterEvent[0];
		}
		
		if (voices == null)
		{
			voices = new List<MusicVoice>();
		}
		
		if (autoTransitions == null)
		{
			autoTransitions = new MusicAutoTransition[0];
		}
		
		cue = GetComponentInParent<MusicCue>();

		// Handle the user toggling the layer mask during playback in the editor
		foreach (var voice in voices)
		{
			if (voice.GetLayerMask() != layerMask)
			{
				UpdateVoiceLayerMasks();
				break;
			}
		}

		if (autoTransitions != null)
		{
			Array.Sort(autoTransitions, (lhs, rhs) => lhs.triggerTime.CompareTo(rhs.triggerTime));
		}
		
		Array.Sort(tempos, tempoComparer);
		Array.Sort(meters, meterComparer);
		PrecomputeTimes();
		CheckInvariants();
	}
	
	private void PrecomputeTimes()
	{
		startTimeSeconds = startTime.ToSeconds(this);
		loopStartTimeSeconds = loopStartTime.ToSeconds(this);
		loopEndTimeSeconds = loopEndTime.ToSeconds(this);
		loopEndTimeSeconds -= DurationToSeconds(loopEndTimeSeconds, prelapDuration);
		transitionTimes = new double[autoTransitions?.Length ?? 0];
		for (int i = 0; i < transitionTimes.Length; ++ i)
		{
			transitionTimes[i] = autoTransitions[i].triggerTime.ToSeconds(this);
		}
	}

	private static readonly IComparer<MeterEvent> meterComparer = new MusicEventTimeComparer<MeterEvent>();
  private static readonly IComparer<TempoEvent> tempoComparer = new MusicEventTimeComparer<TempoEvent>();
  
	private static readonly IComparer<MeterEvent> meterMetricComparer = new MusicEventMetricComparer<MeterEvent>();
  private static readonly IComparer<TempoEvent> tempoMetricComparer = new MusicEventMetricComparer<TempoEvent>();

  private static readonly MeterEvent defaultMeter = new MeterEvent(0.0f, 0.0f, RationalNumber.commonTime);
  private static readonly TempoEvent defaultTempo = new TempoEvent(0.0f, 0.0f, 120.0f);

  [System.Diagnostics.Conditional("UNITY_EDITOR")]
  private void CheckInvariants()
  {
	  for (int i = 1; i < tempos.Length; ++ i)
	  {
		  Debug.Assert(tempoComparer.Compare(tempos[i-1], tempos[i]) <= 0);
	  }
	  for (int i = 1; i < meters.Length; ++ i)
	  {
		  Debug.Assert(meterComparer.Compare(meters[i-1], meters[i]) <= 0);
	  }
  }
  
  private bool GetPreviousEvent<T>(T[] events, T e, IComparer<T> comparer, out T prev)
  {
    CheckInvariants();
    int index = Array.BinarySearch(events, e, comparer);
    if (index < 0) {
      index = (~index) - 1; // index is the bitwise complement of the next greater item, so grab the previous index
    }
    Debug.Assert(index >= -1 && index < events.Length);
    if (index < 0)
    {
	    prev = default;
			return false;
	  }
    prev = events[index];
    return true;
  }
  
  public MeterEvent GetMeterEventBeforeTime(double timeInTrack)
  {
	  return GetPreviousEvent(meters, new MeterEvent{time = timeInTrack}, meterComparer, out var prev) ? prev : defaultMeter;
  }
  
  public TempoEvent GetTempoEventBeforeTime(double timeInTrack)
  {
	  return GetPreviousEvent(tempos, new TempoEvent{time = timeInTrack}, tempoComparer, out var prev) ? prev : defaultTempo;
  }

  public RationalNumber GetMeterAtTime(double timeInTrack)
  {
	  return GetMeterEventBeforeTime(timeInTrack).timeSignature;
  }
  
  public MeterEvent GetMeterEventBeforeMeasure(float measure)
  {
	  return GetPreviousEvent(meters, new MeterEvent{measure = measure}, meterMetricComparer, out var prev) ? prev : defaultMeter;
  }
  
  public TempoEvent GetTempoEventBeforeMeasure(float measure)
  {
	  return GetPreviousEvent(tempos, new TempoEvent{measure = measure}, tempoMetricComparer, out var prev) ? prev : defaultTempo;
  }
  
  public float GetTempoAtTime(double timeInTrack)
  {
	  return GetTempoEventBeforeTime(timeInTrack).tempo;
  }
  
  public double Quantize(double timeInTrack, TimeInterval quantization)
  {
	  var interval = DurationToSeconds(timeInTrack, quantization);
	  if (Math.Abs(interval - 0) < 0.00001) return timeInTrack;
	  var tempo = GetTempoEventBeforeTime(timeInTrack);
    var meter = GetMeterEventBeforeTime(timeInTrack);
    double lastEventTime = Math.Max(tempo.time, meter.time);
    double timeAfterEvent = timeInTrack - lastEventTime;
    Debug.Assert(timeAfterEvent >= 0);
    return lastEventTime + Math.Ceiling(timeAfterEvent / interval) * interval;
  }

  public double DurationToSeconds(double timeInTrack, TimeInterval t)
	{
		if (t.type == TimeInterval.Type.Realtime) return t.realtimeInterval;
		return t.ToSeconds(GetTempoAtTime(timeInTrack), GetMeterAtTime(timeInTrack));
	}
	
	public TimeInterval GetPrelapDuration()
	{
		return prelapDuration;
	}
	
	private MusicVoice GetActiveVoice()
	{
		foreach (var voice in voices)
		{
			if (voice.playing) return voice;
		}
		return null;
	}
	
	public double trackTime
	{
		get => GetActiveVoice()?.trackTime ?? 0.0;
		set
		{
			var voice = GetActiveVoice();
			if (voice)
			{
				voice.trackTime = value;
			}
		}
	}

	// Plays at some time in the future (relative to AudioSettings.dspTime), fading in `offset` seconds into the clips.
	public void PlayAtTime(double time, TimeInterval fadeInDuration, double? timeInTrack = null)
	{
		var voice = MusicVoicePool.Instance.SpawnVoice();
		voices.Add(voice);
		voice.SetLayers(layers);
		voice.trackTime = timeInTrack ?? startTimeSeconds;
		voice.PlayAtTime(time, layerMask);
		voice.ChangeVolumeAtTime(time, DurationToSeconds(voice.trackTime, fadeInDuration), volume);
		UpdateVoice(voice);
		//Debug.Log($"Playing voice section {displayName} ({voice.name}) at time {voice.trackTime}");
	}

	public void StopAtTime(double time, TimeInterval fadeOutDuration, TimeInterval delay)
	{
		for (int i = 0; i < voices.Count; ++ i)
		{
			var voice = voices[i];
			if (!voice.playing) continue;
			voice.StopAtTime(time + DurationToSeconds(voice.trackTime, delay), DurationToSeconds(voice.trackTime, fadeOutDuration));
			if (!UpdateVoice(voice))
			{
				-- i;
			}
		}
	}
  
	public void StopImmediate()
	{
		// Stop all voices immediately without fading out
		while (voices.Count > 0)
		{
			StopAndReleaseVoice(voices[voices.Count - 1]);
		}
	}

	private void StopAndReleaseVoice(MusicVoice voice)
	{
		for (int i = 0; i < voices.Count; ++ i)
		{
			if (voices[i] == voice)
			{
				// Swap to the back
				int last = voices.Count - 1;
				voices[i] = voices[last];
				voices.RemoveAt(last);
				break;
			}
		}
		if (voice == null) return; // These should only be destroyed in edit-mode
		//Debug.Log($"Releasing voice '{voice.name}'");
		voice.Stop();
		MusicVoicePool.Instance.ReleaseVoice(voice);
	}
  
	private void Update()
	{
		double dspTime = SoundManagerStub.dspTime;
		bool isPlaying = false;
		for (int i = 0; i < voices.Count;)
		{
			var voice = voices[i];
			Debug.Assert(!voice.stopped);
			if (!UpdateVoice(voice)) continue; // voice was stopped and removed, so don't increment
			isPlaying = true;
			++ i;
		}
		
		var timeInTrack = trackTime;
		if (isPlaying)
		{
			ApplyAutoTransitions(dspTime, timeInTrack);
		}
		lastDspTime = dspTime;
		lastTrackTime = timeInTrack;
	}
	
	private void ApplyAutoTransitions(double dspTime, double timeInTrack)
	{
		for (int i = 0; i < autoTransitions.Length; ++ i)
		{
			// Check if there's an auto transition coming up
			double triggerTime = transitionTimes[i];
			if (trackTime + autoTransitionLookahead > triggerTime && lastTrackTime < triggerTime)
			{
				double transitionTime = dspTime + triggerTime - trackTime;
				ref readonly var t = ref autoTransitions[i].transition;
				cue.TransitionAtTime(in t, t.to, transitionTime);
			}
		}
	}

	private bool UpdateVoice(MusicVoice voice)
	{
		double dspTime = SoundManagerStub.dspTime;
		var timeInTrack = voice.trackTime;
		// Cue up our loop in a new voice
		if (looping && voice.playing && timeInTrack + loopLookahead >= loopEndTimeSeconds && lastTrackTime < loopEndTimeSeconds)
		{
			double loopTime = dspTime + (loopEndTimeSeconds - timeInTrack);
			voice.StopAtTime(loopTime + DurationToSeconds(voice.trackTime, prelapDuration), DurationToSeconds(voice.trackTime, loopFadeOutDuration));
			PlayAtTime(loopTime, loopFadeInDuration, loopStartTimeSeconds);
		}
		// Apply our current volume ramp
		bool doneRamping = voice.ApplyRamps(dspTime);
		// Mark any stopping voices that are no longer fading out as stopped
		if ((voice.stopping && doneRamping) || !voice.trackPlaying)
		{
			StopAndReleaseVoice(voice);
			return false;
		}
		return true;
	}
	
	public void SetLoopPoints(TimeInterval start, TimeInterval end)
	{
		looping = true;
		loopStartTime = start;
		loopEndTime = end;
		PrecomputeTimes();
	}
	
	public bool GetLoopPoints(out TimeInterval start, out TimeInterval end)
	{
		start = loopStartTime;
		end = loopEndTime;
		return looping;
	}

	public void SetMeterMap(TempoEvent[] tempos, MeterEvent[] meters)
	{
		this.tempos = tempos;
		this.meters = meters;
	}
	
	public void SetStartTime(TimeInterval start)
	{
		startTime = start;
		PrecomputeTimes();
	}

	public MusicVoice.LayerMask layerMask
	{
		get => _layerMask;
		set
		{
			_layerMask = value;
			UpdateVoiceLayerMasks();
		}
	}
	
	private void UpdateVoiceLayerMasks()
	{
		if (voices.Count <= 0) return;
		foreach (var voice in voices)
		{
			if (voice.playing)
			{
				voice.SetLayerMask(_layerMask, DurationToSeconds(voice.trackTime, layerFadeInDuration), DurationToSeconds(voice.trackTime, layerFadeOutDuration));
			}
		}
	}

	private void OnEnable()
	{
#if UNITY_EDITOR
		if (!Application.IsPlaying(this))
		{
			EditorApplication.update += Update;
		}
#endif // UNITY_EDITOR
		if (voices != null)
		{
			voices.Clear();
		}
		else
		{
			voices = new List<MusicVoice>(4);
		}
		PrecomputeTimes();
	}
	
	private void OnDisable()
	{
		if (Application.IsPlaying(this))
		{
			StopImmediate();
		}
#if UNITY_EDITOR
		if (!Application.IsPlaying(this))
		{
			EditorApplication.update -= Update;
		}
#endif // UNITY_EDITOR
	}

	public void SetDefaultValues ()
	{
		if (layers == null || layers.Length == 0)
			layers = new AudioClip[1];

		if (meters == null || meters.Length == 0)
		{
			meters = new MeterEvent[1];
			meters[0] = new MeterEvent (0, 0, new RationalNumber (4,4));
		}
		if (tempos == null || tempos.Length == 0)
		{
			tempos = new TempoEvent[1];
			tempos[0] = new TempoEvent (0, 0, 120);
		}
		// prelapDuration.ConvertTo (TimeInterval.Type.Realtime, )
	}
}