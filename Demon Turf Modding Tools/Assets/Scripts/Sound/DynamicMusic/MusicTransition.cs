using UnityEngine;

[System.Serializable]
public struct MusicTransition
{
  public int from;
  public int to;
  public TimeInterval fadeInDuration;
  public TimeInterval fadeOutDuration;
  public bool quantized;
  public TimeInterval quantization;

  [Tooltip("Transition to the same time in the destination.")]
  public bool relative;
}