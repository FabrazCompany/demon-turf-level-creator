using System;
using UnityEngine;

[ExecuteAlways]
public class MusicVoice : MonoBehaviour
{
	[Flags]
	public enum LayerMask
	{
		Layer1 = 1 << 0,
		Layer2 = 1 << 1,
		Layer3 = 1 << 2,
		Layer4 = 1 << 3,
    
		AllLayers = -1
	}
	
	private struct Layer
	{
		public AudioSource source;
		private Ramp volumeRamp;
		
		public void ChangeVolumeAtTime(double rampTime, double duration, float volume)
		{
			volumeRamp = new Ramp(rampTime, duration, source.volume, volume);
		}
		
		public bool ApplyRamps(double rampTime, float scale)
		{
			volumeRamp.GetValueAtTime(rampTime, out float volume);
			source.volume = scale * volume;
			return rampTime >= volumeRamp.endTime;
		}
	}
	
	private LayerMask layerMask;
	private Layer[] layers;

	private PlaybackState playbackState;
	
	private Ramp volumeRamp;
  private Ramp stopRamp;
	private float currentVolume = 0.0f;

	private const double layerUpdateDelay = 0.5;

	private enum PlaybackState
	{
		Stopped,
		Stopping,
		Playing
	}
  
	private void OnEnable()
	{
		if (layers == null)
		{
			var sources = GetComponentsInChildren<AudioSource>(true);
			layers = new Layer[sources.Length];
			for (int i = 0; i < layers.Length; ++i )
			{
				layers[i] = new Layer { source = sources[i] };
			}
		}
		for (int i = 0 ; i < layers.Length; ++ i)
		{
			layers[i].source.clip = null;
		}
		layerMask = 0;
		volumeRamp = default;
		playbackState = PlaybackState.Stopped;
	}
	
	private static double ToRampTime(double dspTime)
	{
		return dspTime; // useful if we ever need to use a different timebase than dspTime
	}
	
	public void ChangeVolumeAtTime(double time, double duration, float volume)
	{
		var rampTime = ToRampTime(time);
		//Debug.Log($"ChangeVolumeAtTime: {time} {duration} {volume} {layers[0].source.name}");
		volumeRamp = new Ramp(rampTime, duration, currentVolume, volume);
	}

	public bool ApplyRamps(double time)
	{
		var rampTime = ToRampTime(time);
		volumeRamp.GetValueAtTime(rampTime, out currentVolume);

		if (playbackState == PlaybackState.Stopping)
		{
			stopRamp.GetValueAtTime(rampTime, out var stopScale);
			currentVolume *= stopScale;
		}

		bool done = rampTime >= Math.Max(volumeRamp.endTime, stopRamp.endTime);
		for (int i = 0; i < layers.Length; ++ i)
		{
			if (!layers[i].ApplyRamps(rampTime, currentVolume))
			{
				done = false;
			}
		}
		return done;
	}
	
	public void PlayAtTime(double time, LayerMask newLayerMask)
	{
		playbackState = PlaybackState.Playing;
		layerMask = newLayerMask;
		// Schedule the voice to play at the specified time
		for (int i = 0; i < layers.Length; ++ i)
		{
			var layer = (LayerMask)(1 << i);
			bool isActive = (layerMask & layer) != 0;
			layers[i].ChangeVolumeAtTime(0, 0, isActive ? 1.0f : 0.0f);
			var source = layers[i].source;
			source.volume = isActive ? 1.0f : 0.0f;
			source.PlayScheduled(time);
		}
	}

	/// <summary>
	/// Immediately stops this voice.
	/// </summary>
	public void Stop()
	{
		playbackState = PlaybackState.Stopped;
		for (int i = 0; i < layers.Length; ++ i)
		{
			if (layers[i].source != null) // can be null when stopping in the editor
			{
				layers[i].source.Stop();
			}
		}
	}
	
	public bool trackPlaying
	{
		get
		{
			for (int i = 0; i < layers.Length; ++ i)
			{
				if (layers[i].source.isPlaying) return true; 
			}
			return false;
		}
	}
	
	/// <summary>
	/// Stops this voice at some time in the future with a linear fade out.
	/// </summary>
	/// <param name="time">absolute time in seconds based on AudioSettings.dspTime</param>
	/// <param name="fadeOutDuration">duration in seconds over which to fade out</param>
	public void StopAtTime(double time, double fadeOutDuration)
	{
		var rampTime = ToRampTime(time);
		stopRamp = new Ramp(rampTime, fadeOutDuration, 1.0f, 0.0f);
		playbackState = PlaybackState.Stopping;
	}
	
	public void SetLayerMask(LayerMask newLayerMask, double fadeInDuration, double fadeOutDuration)
	{
    LayerMask changed = layerMask ^ newLayerMask;
    LayerMask removed = layerMask & changed;
    LayerMask added = (~layerMask) & changed;
    layerMask = newLayerMask;
    var rampTime = ToRampTime(SoundManagerStub.dspTime + layerUpdateDelay);
    for (int i = 0; i < layers.Length; ++ i)
    {
	    var layer = (LayerMask)(1 << i);
	    if ((added & layer) != 0)
	    {
		    // TODO_MUSIC: Quantize to beats?
		    layers[i].ChangeVolumeAtTime(rampTime, fadeInDuration, 1.0f);
	    }
	    else if ((removed & layer) != 0)
	    {
		    layers[i].ChangeVolumeAtTime(rampTime, fadeOutDuration, 0.0f);
	    }
    }
	}
	
	public LayerMask GetLayerMask()
	{
		return layerMask;
	}
	
	public void SetLayers(AudioClip[] clips)
	{
		for (int i = 0; i < layers.Length; ++ i)
		{
			layers[i].source.clip = i < clips.Length ? clips[i] : null;
		}
	}
	
	public double frequency => layers.Length > 0 ? layers[0].source.clip.frequency : 48000.0;
	
	/// <summary>
	/// Current playback time in this voice's clip.
	/// </summary>
	public double trackTime
	{
		get => layers.Length > 0 ? layers[0].source.timeSamples / frequency : 0.0;
		set
		{
			for (int i = 0; i < layers.Length; ++ i)
			{
				layers[i].source.timeSamples = (int)(value * frequency);
			}
		}
	}
	
	public bool playing => playbackState == PlaybackState.Playing;
	public bool stopping => playbackState == PlaybackState.Stopping;
	public bool stopped => playbackState == PlaybackState.Stopped;
}