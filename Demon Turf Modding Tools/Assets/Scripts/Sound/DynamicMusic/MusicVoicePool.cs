using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif //UNITY_EDITOR

public class MusicVoicePool : MonoBehaviour
{
	private const int maxLayersPerVoice = 4;
	private const int voicePoolInitialSize = 8;
	private readonly List<MusicVoice> allVoices = new List<MusicVoice>(16);
	private readonly Stack<MusicVoice> freeVoices = new Stack<MusicVoice>(16);
	
	private static MusicVoicePool _instance;
	
	public static MusicVoicePool Instance
	{
		get
		{
			if (_instance != null) return _instance;
#if UNITY_EDITOR
			if (SoundManagerStub.IsInitialized)
			{
				_instance = SoundManagerStub.Instance.GetOrAddComponent<MusicVoicePool>();
			}
			else
			{
				var instances = Resources.FindObjectsOfTypeAll<MusicVoicePool>();
				Debug.Assert(instances.Length <= 1);
				if (instances.Length > 0)
				{
					_instance = instances[0];
				}
				else
				{
					var go = EditorUtility.CreateGameObjectWithHideFlags("Music Voice Pool (Editor)", HideFlags.DontSave);
					_instance = go.AddComponent<MusicVoicePool>();	
				}
			}
#else
			_instance = SoundManagerStub.Instance.GetOrAddComponent<MusicVoicePool>();
#endif // UNITY_EDITOR
			return _instance;
		}
	}
	
#if UNITY_EDITOR
	[UnityEditor.Callbacks.DidReloadScripts]
	private static void OnScriptsReloaded()
	{
		if (_instance && (_instance.gameObject.hideFlags & HideFlags.DontSave) == HideFlags.DontSave)
		{
			DestroyImmediate(_instance.gameObject);
			_instance = null;
		}
	}
#endif // UNITY_EDITOR

	public MusicVoice SpawnVoice()
	{
		if (allVoices.Count == 0)
		{
			InitializeVoices();
		}
		if (freeVoices.Count == 0)
		{
			AddVoice();
		}
		var voice = freeVoices.Pop();
		Debug.Assert(voice.stopped);
		voice.gameObject.SetActive(true);
		return voice;
	}
	
	public void ReleaseVoice(MusicVoice voice)
	{
		Debug.Assert(allVoices.Contains(voice) && !freeVoices.Contains(voice));
		freeVoices.Push(voice);
		voice.gameObject.SetActive(false);
	}
	
	private void InitializeVoices()
	{
		ClearVoices();
		for (int i = 0; i < voicePoolInitialSize; ++ i)
		{
			AddVoice();
		}
	}
	
	private void ClearVoices()
	{
		foreach (var voice in GetComponentsInChildren<MusicVoice>(true))
		{
			if (Application.IsPlaying(this))
			{
				Destroy(voice.gameObject);
			}
			else
			{
				DestroyImmediate(voice.gameObject);
			}
		}
		allVoices.Clear();
		freeVoices.Clear();
	}

	private void AddVoice()
	{
		var voice = MakeVoice(allVoices.Count);
		allVoices.Add(voice);
		freeVoices.Push(voice);
		voice.gameObject.SetActive(false);
	}
  
	private AudioSource AddSource(GameObject go)
	{
		var source = go.AddComponent<AudioSource>();
		source.hideFlags = HideFlags.DontSave;
		if (Application.IsPlaying(this))
		{
			source.outputAudioMixerGroup = SoundManagerStub.Instance.GetAudioGroup(SoundMixerTarget.Music);
		}
		source.playOnAwake = false;
		source.ignoreListenerPause = true;
		return source;
	}

	private MusicVoice MakeVoice(int voiceId)
	{
		var go = new GameObject($"Voice-{voiceId}");
		go.hideFlags = HideFlags.DontSave;
		go.transform.parent = transform;
		var sources = new AudioSource[maxLayersPerVoice];
		for (int i = 0; i < sources.Length; ++ i)
		{
			sources[i] = AddSource(go);
		}
		return go.AddComponent<MusicVoice>();;
	}
}