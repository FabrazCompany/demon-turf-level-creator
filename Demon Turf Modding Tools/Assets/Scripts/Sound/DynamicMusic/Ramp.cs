using UnityEngine;

public readonly struct Ramp
{
	public readonly double time;
	public readonly double duration;
	public readonly float startValue;
	public readonly float targetValue;
	
	public double endTime => time + duration;

	public Ramp(double time, double duration, float startValue, float targetValue)
	{
		this.time = time;
		this.duration = duration;
		this.startValue = startValue;
		this.targetValue = targetValue;
	}
	
	public void GetValueAtTime(double t, out float value)
	{
		if (t <= time)
		{
			value = startValue;
		}
		else if (t >= time + duration)
		{
			value = targetValue;
		}
		else
		{
			var x = Mathf.Clamp01((float)((t - time) / duration));
			value = Mathf.Lerp(startValue, targetValue, Mathf.Sqrt(x));
		}
	}
}