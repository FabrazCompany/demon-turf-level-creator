﻿using UnityEngine;

[System.Serializable]
public struct RationalNumber {
  [Delayed] public int numerator;
  [Delayed] public int denominator;
  
  public static readonly RationalNumber commonTime = new RationalNumber(4, 4);

  public RationalNumber(int num, int den)
  {
    numerator = num;
    denominator = den;
  }
  
  private const float NUMERATOR_THRESHOLD = 0.1f;
  
  public float ratio => denominator != 0 ? numerator / (float)denominator : 0.0f;
  
  public double ToSeconds(float bpm) {
    return quarterNotes / bpm * 60;
  }

  /// <summary>
  /// Get the number of quarter notes this ratio represents, e.g. 3/4 is 3 quarter notes, 7/8 is 3.5
  /// </summary>
  public float quarterNotes
  {
    get => denominator != 0 ? (4.0f * numerator) / denominator : 0.0f;
    set {
      denominator = 4;
      // Try to find a power of two rational fraction that's within our threshold for conversion
      for (int i = 0; i < 4; ++ i)
      {
        float n = value * denominator / 4.0f;
        int num = Mathf.RoundToInt(n);
        if (Mathf.Abs(n - num) <= NUMERATOR_THRESHOLD)
        {
          numerator = num;
          return;
        }
        denominator <<= 1;
      }
    }
  }
}
