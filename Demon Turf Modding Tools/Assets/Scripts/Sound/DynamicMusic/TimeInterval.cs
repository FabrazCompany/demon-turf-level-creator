using System;
using UnityEngine;

[Serializable]
public struct TimeInterval : IComparable<TimeInterval>
{
	public enum Type
	{
		Realtime,
		Metric
	}
	public Type type;
	[Delayed] public double realtimeInterval;
	public MetricInterval metricInterval;

	public TimeInterval(float interval)
	{
		type = Type.Realtime;
		realtimeInterval = interval;
		metricInterval = default;
	}
	
	public TimeInterval(MetricInterval interval)
	{
		type = Type.Metric;
		realtimeInterval = default;
		metricInterval = interval;
	}
	
	public double ToSeconds(IMusicTemporalMap map)
	{
		switch (type)
		{
		case Type.Realtime: return realtimeInterval;
		case Type.Metric: return metricInterval.ToSeconds(map);
		default: throw new ArgumentException($"Unsupported duration type '{type}'");
		}
	}
	
	public double ToSeconds(float bpm, RationalNumber timeSignature)
	{
		switch (type)
		{
		case Type.Realtime: return realtimeInterval;
		case Type.Metric: return metricInterval.ToSeconds(bpm, timeSignature);
		default: throw new ArgumentException($"Unsupported duration type '{type}'");
		}
	}

	public TimeInterval ConvertTo(Type t, IMusicTemporalMap map)
	{
		var result = this;
		if (result.type == t) return result;
		var time = ToSeconds(map);
		result.type = t;
		result.FromSeconds(time, map);
		return result;
	}

	public void FromSeconds(double seconds, IMusicTemporalMap map)
	{
		switch (type)
		{
		case Type.Realtime:
			realtimeInterval = seconds;
			break;
		case Type.Metric:
			metricInterval = new MetricInterval(seconds, map);
			break;
		default: throw new ArgumentException($"Unsupported duration type '{type}'");
		}
	}

	public int CompareTo(TimeInterval other)
	{
		if (type != other.type)
		{
			throw new ArgumentException($"Mismatched duration types in comparison '{type}' vs '{other.type}'");
		}
		switch (other.type)
		{
		case Type.Realtime: return realtimeInterval.CompareTo(other.realtimeInterval);
		case Type.Metric: return metricInterval.CompareTo(other.metricInterval);
		}
		Debug.LogAssertion($"Unrecognized time interval type: '{type}'");
		return 0;
	}
}