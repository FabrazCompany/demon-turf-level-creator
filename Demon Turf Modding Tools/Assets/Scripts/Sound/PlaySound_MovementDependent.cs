using UnityEngine;

[RequireComponent (typeof(AudioSource))]
public class PlaySound_MovementDependent : MonoBehaviour 
{
	Rigidbody rigid;
	AudioSource sound;

	// [SerializeField] ParticleSystem.MinMaxCurve targetVelocityLimits = new ParticleSystem.MinMaxCurve (2, 10);
	[SerializeField] float minVelocity = 3;
	[SerializeField] float maxVelocity = 10;
	[SerializeField] ParticleSystem.MinMaxCurve volume = new ParticleSystem.MinMaxCurve (0, 1);
	[SerializeField] ParticleSystem.MinMaxCurve pitch = new ParticleSystem.MinMaxCurve (.7f, 1f);

	[System.Serializable]
	enum PlaybackStyle
	{
		Always,
		Grounded,
		Airborne
	}
	[SerializeField] PlaybackStyle playbackStyle = PlaybackStyle.Always;
	[SerializeField, Conditional ("playbackStyle", PlaybackStyle.Always, true)] LayerMask groundedCheckLayer;
	[SerializeField, Conditional ("playbackStyle", PlaybackStyle.Always, true)] float groundedCheckDistance = 0.5f;

	void Awake () 
	{
		rigid = GetComponentInParent<Rigidbody> ();
		if (!rigid)
		{
			Debug.LogError ("No Rigidbody detected, destroying movement based Sound Player!");
			Destroy (this);
			return;
		}
		sound = GetComponent<AudioSource> ();
	}

	void Update () 
	{
		var min = minVelocity * minVelocity;
		var max = maxVelocity * maxVelocity;
		var curSpeed = Mathf.Min (rigid.velocity.sqrMagnitude, max);
		if (curSpeed < min)
		{
			if (sound.isPlaying)
				sound.Stop ();
			return;
		}
			

		if (playbackStyle != PlaybackStyle.Always)
		{
			var grounded = Physics.Raycast (transform.position, Vector3.down, groundedCheckDistance, groundedCheckLayer, QueryTriggerInteraction.Ignore);
			if ((playbackStyle == PlaybackStyle.Grounded && !grounded)
				|| (playbackStyle == PlaybackStyle.Airborne && grounded))
			{
				if (sound.isPlaying)
					sound.Stop ();
				return;
			}
		}
		if (!sound.isPlaying)
			sound.Play ();
		var pct = Mathf.InverseLerp (min, max, curSpeed);
		sound.volume = volume.EvaluateNonRandom (pct);
		sound.pitch = pitch.EvaluateNonRandom (pct);
	}

	void OnDrawGizmosSelected()
	{
		if (playbackStyle == PlaybackStyle.Always)
			return;
		Gizmos.color = Color.green;
		Gizmos.DrawRay (transform.position, Vector3.down * groundedCheckDistance);
	}
}