using UnityEngine;
using UnityEngine.Audio;

public class SetMixerEffect : MonoBehaviour
{
    [SerializeField] AudioMixerSnapshot snapshot;
	public void SetLevelSnapshot (float transition) => SoundManagerStub.Instance?.AssignTurfSnapshot (snapshot, transition);
    public void AddSnapshot (float transition) => SoundManagerStub.Instance?.AddSnapshot (snapshot, transition);
    public void RemoveSnapshot (float transition) => SoundManagerStub.Instance?.RemoveSnapshot (snapshot, transition);
    public void ClearAllSnapshots (float transition) => SoundManagerStub.Instance?.ClearSnapshots (transition);

	void OnDisable ()
	{
		if (SoundManagerStub.IsInitialized)
			SoundManagerStub.Instance.RemoveSnapshot (snapshot, 0.25f);
	}
}