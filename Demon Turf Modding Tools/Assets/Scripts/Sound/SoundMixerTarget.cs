
public enum SoundMixerTarget 
{
	UI = 0,
	Music = 1,
	Gameplay = 2,
	Master = 3,
	VO = 4,
}
