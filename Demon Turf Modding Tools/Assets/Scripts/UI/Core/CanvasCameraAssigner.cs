﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof (Canvas))]
public class CanvasCameraAssigner : MonoBehaviour
{
    void OnEnable()
    {
        GetComponent<Canvas> ().worldCamera = Camera.main;
    }
}
