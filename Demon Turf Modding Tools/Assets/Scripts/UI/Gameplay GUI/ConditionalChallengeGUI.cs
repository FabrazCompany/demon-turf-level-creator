﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using RotaryHeart.Lib.SerializableDictionary;

public class ConditionalChallengeGUI : MonoBehaviour
{
    public enum ChallengeType
    {
        CombatZone = 0,
        CombatZoneWaves = 1,
        CombatZoneArena = 3,
        
        RingChallenge = 10,
        RingChallengeSoccer = 12,
        
        KeysChallenge = 20,
        
        RacingChallenge = 30,
        RacingChallengeSoccer = 32,

        CurseChallenge
    }
    [System.Serializable]
    class ChallengeTypeDataDict : SerializableDictionaryBase<ChallengeType, ChallengeTypeData> {}
    
    [System.Serializable]
    struct ChallengeTypeData 
    {
        public Sprite imageType;
        public string challengeLabel;
        public Sprite challengeBarImg;
        public Texture challengeBarVisual;
    }

    [System.Serializable]
    struct LabelCombo 
    {
        public Image image;
        public TextMeshProUGUI text;
    }

    [SerializeField] Animator animator;
    [SerializeField] AudioClip audioOnStart;
    [SerializeField] EventUtility_PlaySoundFromSet startZoneGameSFXSetScript;
    [SerializeField] EventUtility_PlaySoundFromSet winZoneGameSFXSetScript;
    [SerializeField] EventUtility_PlaySoundFromSet loseZoneGameSFXSetScript;
	[SerializeField] Canvas canvas;
    [SerializeField] CanvasGroup canvasGroup;
    [Header ("Type Display")]
    [SerializeField] TextMeshProUGUI challengeLabel;


    [Header ("HUD")]
    [SerializeField] RectTransform hudRoot;
    [SerializeField] float hudYTimed = 0;
    [SerializeField] float hudYTimeless = -70;
    [SerializeField] TimerGUI timer;
    [SerializeField] CanvasGroup timerCanv;
    [SerializeField] LabelCombo primaryProgress;
    [SerializeField] LabelCombo secondaryProgress;

    [Header ("Visual Changes")]
    [SerializeField] ChallengeTypeDataDict challengeTypes;
    [SerializeField] RawImage barImageTop;
    [SerializeField] RawImage barImageBottom;
    int challengeTotal;
	ChallengeType currentType;
	public ChallengeType getCurrentType => currentType;
	public bool getChallengeActive => animator.enabled;

    bool ShouldUseTimer (ChallengeType _type)
    {
        switch (_type)
        {
            default:
                return false;
            case ChallengeType.RacingChallenge:
            case ChallengeType.RacingChallengeSoccer:
            case ChallengeType.CombatZoneArena:
                return true;
        }
    }

	public float GetTime => timer.getTimer;
    public void Trigger (CurseChallengeType _primaryType, CurseChallengeType _secondaryType)
    {
        Trigger (ChallengeType.CurseChallenge, 0, true);
        primaryProgress.image.enabled = true;
        TriggerCurseText (_primaryType, primaryProgress);
        if (_secondaryType != CurseChallengeType.None)
        {
            TriggerCurseText (_secondaryType, secondaryProgress);
            secondaryProgress.image.enabled = true;
        }
        void TriggerCurseText (CurseChallengeType type, LabelCombo label)
        {
            switch (type)
            {
                case CurseChallengeType.Jumping:
                    label.text.text = "No Jumping!";
                    break;
                case CurseChallengeType.DoubleJumping:
                    label.text.text = "No Double Jumping!";
                    break;
                case CurseChallengeType.Spin:
                    label.text.text = "No Spinning!";
                    break;
                case CurseChallengeType.Punching:
                    label.text.text = "No Punching!";
                    break;
                case CurseChallengeType.TurfAbility:
                    label.text.text = "No Turf Abilities!";
                    break;
                case CurseChallengeType.Hookshot:
                    label.text.text = "No Hookshot!";
                    break;
                case CurseChallengeType.Rollout:
                    label.text.text = "No Rollout!";
                    break;
                case CurseChallengeType.Glide:
                    label.text.text = "No Gliding!";
                    break;
                case CurseChallengeType.TimeBubble:
                    label.text.text = "No Time Bubble!";
                    break;
            }
        }
    }
    public void Trigger (ChallengeType _type, float _challengeTime, bool _isShrinking) => Trigger (_type, -1, -1, _challengeTime, _isShrinking);
    public void Trigger (ChallengeType _type, int _challengeProgress, int _challengeTotal, float _challengeTime, bool _isShrinking) => Trigger (_type, string.Empty, _challengeProgress, _challengeTotal, _challengeTime, _isShrinking);
    public void Trigger (ChallengeType _type, string _waveLabel, int _challengeProgress, int _challengeTotal, float? _challengeTime, bool _isShrinking) 
    {
		currentType = _type;
        animator.enabled = true;
        SoundManagerStub.Instance.PlaySoundUI (audioOnStart, new AudioData(_volume:1f));
        animator.ResetTrigger ("Success");
        animator.ResetTrigger ("Outro");
        animator.Play ("Intro");
        
        secondaryProgress.text.text = _waveLabel;
        secondaryProgress.image.enabled = _waveLabel != string.Empty;
        
        challengeTotal = _challengeTotal;

        UpdateProgress (_challengeProgress);
        
		canvas.enabled = true;
        canvasGroup.alpha = 1;

        if (challengeTypes.TryGetValue (_type, out var values))
        {
            challengeLabel.text = values.challengeLabel;
            primaryProgress.image.sprite = values.imageType;
            barImageTop.texture = barImageBottom.texture = values.challengeBarVisual;
        }

        if (_challengeTime != null)
        {
            if (ShouldUseTimer (_type))
            {
                SetHUDY (hudYTimed);
                timerCanv.alpha = 1;
                timer.enabled = false;
                timer.enabled = true;
                timer.setTimer (_challengeTime.Value);
                timer.setIsShrinking (_isShrinking);
            }
            else 
            {
                SetHUDY (hudYTimeless);
                timerCanv.alpha = 0;
                timer.enabled = false;
            }
        }   
        
    }
    void SetHUDY (float bottomY)
    {
        hudRoot.localPosition = new Vector2 (0, bottomY * 0.5f);
        hudRoot.anchoredPosition = new Vector2 (0, bottomY * 0.5f);
        hudRoot.sizeDelta = new Vector2 (0, -bottomY);
    }

    public void UpdateProgress (int progress)
    {
        var progresSet = primaryProgress;
        if (progress >= 0)
        {
            progresSet.text.text = string.Format ("{0}/{1}", progress, challengeTotal);
            progresSet.image.enabled = true;
        }
        else 
        {
            progresSet.text.text = string.Empty;
            progresSet.image.enabled = false;
        }
    }

    public void Completed () 
    {
        timer.enabled = false;
        animator.SetTrigger ("Success");
    }
    public void Cancelled () 
    {
        timer.enabled = false;
        animator.SetTrigger ("Outro");
    }
    public void Fail () 
    {
        timer.enabled = false;
        animator.SetTrigger ("Failure");
    }

    public void PlayStartZoneSFX() => startZoneGameSFXSetScript.Trigger2DSound();
    public void PlayWinZoneSFX() => winZoneGameSFXSetScript.Trigger2DSound();
    public void PlayLoseZoneSFX() => loseZoneGameSFXSetScript.Trigger2DSound();
}