public enum CurseChallengeType 
{
    None = 0,
    Jumping = 40,
    DoubleJumping = 42,
    Spin = 44,
    Punching = 46,
    TurfAbility = 48,
    Hookshot = 50,
    Rollout = 52,
    Glide = 54,
    TimeBubble = 56,
}