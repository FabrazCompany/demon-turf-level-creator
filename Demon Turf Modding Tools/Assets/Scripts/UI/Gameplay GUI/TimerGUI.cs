using UnityEngine;
using TMPro;

[RequireComponent (typeof (TextMeshProUGUI))]
public class TimerGUI : MonoBehaviour
{
	[SerializeField] bool resetOnEnable = true;
	[SerializeField] bool shrinkingTimer;
	TextMeshProUGUI text;
	float timer;
	float lastRealtimeSinceStartup;
	
	public float getTimer => timer;
	public void setTimer (float val) => timer = val;
	public void setIsShrinking (bool val) => shrinkingTimer = val;
	
	void Awake () 
	{
		text = GetComponent<TextMeshProUGUI> ();
		lastRealtimeSinceStartup = Time.realtimeSinceStartup;
	}
    void OnEnable ()
	{
		lastRealtimeSinceStartup = Time.realtimeSinceStartup;
		if (resetOnEnable) 
		{
			timer = -Time.deltaTime;
			text.text = MenuUtility.FormatTimer (timer);
		}
	}

	void Update () 
	{
		var deltaTime = Time.realtimeSinceStartup - lastRealtimeSinceStartup;
		lastRealtimeSinceStartup = Time.realtimeSinceStartup;
		deltaTime *= Time.timeScale;

		if (shrinkingTimer)
			deltaTime *= -1;
			
		timer += deltaTime;
		text.text = MenuUtility.FormatTimer (timer);
	}

}
