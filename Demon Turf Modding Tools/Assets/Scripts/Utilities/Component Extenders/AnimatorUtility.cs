using System.Collections;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor.Animations;
using UnityEditor;
#endif

[RequireComponent(typeof(Animator))]
public class AnimatorUtility : MonoBehaviour {
	[SerializeField] int layerMax = 4;
	Animator anim;
	void Awake () {
		anim = GetComponent<Animator> ();
		fadeLayerWeightRoutines = new Coroutine[layerMax];
	}

	public void ParameterBoolEnable (string id) 
	{
		if (!anim)
			return;
		anim.SetBool (id, true);
	}
	public void ParameterBoolDisable (string id) 
	{
		if (!anim)
			return;
		anim.SetBool (id, false);
	}
	public void ParameterFloatZero (string id) 
	{
		if (!anim)
			return;
		anim.SetFloat (id, 0);
	}
	public void ParameterFloatOne (string id) 
	{
		if (!anim)
			return;
		anim.SetFloat (id, 1);
	}
	public void ParameterIntZero (string id) 
	{
		if (!anim)
			return;
		anim.SetInteger (id, 0);
	}
	public void ParameterIntOne (string id) 
	{
		if (!anim)
			return;
		anim.SetInteger (id, 1);
	}
	public void ParameterIntOneNeg (string id) 
	{
		if (!anim)
			return;
		anim.SetInteger (id, -1);
	}
	

	string targetID;
	public void SetTargetID (string val) => targetID = val;
	public void ParameterInt (int val)
	{
		if (!anim)
			return;
		anim.SetInteger (targetID, val);
	}
	public void ParameterFloat (float val) 
	{
		if (!anim)
			return;
		anim.SetFloat (targetID, val);
	}
	public void ParameterBool (bool val) 
	{
		if (!anim)
			return;
		anim.SetBool (targetID, val);
	}

	public void FadeLayerWeight (int layer, float weight, float duration) 
	{
		if (layer > layerMax)
			return;
		
		this.RestartCoroutine (FadeLayerWeightRoutine (layer, weight, duration), ref fadeLayerWeightRoutines[layer]);
	}
	Coroutine[] fadeLayerWeightRoutines;
	IEnumerator FadeLayerWeightRoutine (int layer, float weight, float duration) 
	{
		var timer = 0f;
		var start = anim.GetLayerWeight (layer);
		while (timer < duration)
		{
			anim.SetLayerWeight (layer, Mathf.Lerp (start, weight, timer / duration));
			timer += Time.deltaTime;
			yield return null;
		}
	}

#if UNITY_EDITOR
 
    [MenuItem("AnimTools/GameObject/Asset from Blendtree")]
    static void CreateBlendtree()
    {
        BlendTree BT = Selection.activeObject as BlendTree;
 
        BlendTree BTcopy = Instantiate<BlendTree>(BT);

		// AssetDatabase.fol
		// string path = ProjectUtility.GetCurrentProjectPath ();
		string path = string.Empty;
        AssetDatabase.CreateAsset(BTcopy, AssetDatabase.GenerateUniqueAssetPath(path + BT.name + ".asset"));
    }
#endif

}
