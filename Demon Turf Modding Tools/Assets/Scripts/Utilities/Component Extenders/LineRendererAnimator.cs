using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRendererAnimator : MonoBehaviour
{
    [SerializeField] float lineWidthScaling = 1;
    [SerializeField] float colliderRadiusScaling = 0.5f;
    [SerializeField] float colliderHeightPadding = 0.5f;
    [SerializeField] float reflectSpeed = 4f;
    
    [SerializeField] LaserReflectBubble reflectionBubblePrefab;
    [SerializeField] float reflectionBubbleScale = 1.5f;
    [SerializeField] float reflectionBubbleScaleSpeed = 2;
    LaserReflectBubble reflectionBubble;

    LineRenderer line;
    new CapsuleCollider collider;
    DeliverKill kill;
    DeliverKnockback knocker;
    
    Timer timer;
    float maxLength;
    float curLengthPCT;
    Vector3 origin;
    Vector3 direction;
    bool reflecting;

	public void SetIgnores (List<Collider> colliders)
	{
		// Debug.Log ("Adding Colliders");
		foreach (var other in colliders)
		{
			// Debug.Log ("Adding: " + other);
			if (other != null)
				Physics.IgnoreCollision (collider, other, true);
		}
			
	}
	public void ClearIgnores (List<Collider> colliders)
	{
		foreach (var other in colliders)
		{
			// Debug.Log ("Clearing: " + other);
			if (other != null)
				Physics.IgnoreCollision (collider, other, false);
		}
			
	}
    
    public bool getReflecting => reflecting;

    void Awake () 
    {
        line = GetComponent<LineRenderer> ();
        timer = GetComponent<Timer> ();
        collider = GetComponent<CapsuleCollider> ();
        kill = GetComponent<DeliverKill> ();
        knocker = GetComponent<DeliverKnockback> ();
    }

    void OnEnable () 
    {
        reflecting = false;
        kill?.SetMute (false);
        knocker?.SetMute (false);
        SetOrigin ();
        if (reflectionBubble != null)
        {
            TrashMan.despawn (reflectionBubble.gameObject);
        }
    }
	void OnDisable ()
	{
		if (reflectionBubble)
		{
			TrashMan.despawn (reflectionBubble.gameObject);
			reflectionBubble = null;
		}
	}

    public void SetOrigin () 
    {
        origin = transform.position;
        direction = transform.parent?.forward ?? transform.forward;
    }
    public void SetTimer (float _maxDuration) => timer?.SetTimer (_maxDuration);
    public void SetWidth (float _width) 
    {
        line.widthMultiplier = _width * lineWidthScaling;
        collider.radius = _width * colliderRadiusScaling;
    }
    public void SetLength (float _maxLength)
    {
        maxLength = _maxLength;
        SetLengthPercentage (1);
    }
    
    public void SetLengthPercentage (float pct)
    {
        if (reflecting)
            return;
        curLengthPCT = pct;
        var length = Mathf.Min (Vector3.Distance (transform.position, origin), maxLength * pct);
        AdjustLength (length);
    }
    void AdjustLength (float length) 
    {
        var dir = transform.InverseTransformDirection (-direction);
        line.SetPosition (1, dir * length);
        collider.height = length + colliderHeightPadding;
        collider.center = -Vector3.forward * length * 0.5f;
    }
    public void StartReflect () 
    {
        kill?.SetMute (true);
        knocker?.SetMute (true);
        reflecting = true;
        transform.forward = direction;
        SpawnBubble ();
    }
    public void UpdateReflect () 
    {
        var dist = Vector3.Distance (Vector3.zero, line.GetPosition (1));
        dist = Mathf.MoveTowards (dist, 0, reflectSpeed * Time.unscaledDeltaTime);
        AdjustLength (dist);
        if (dist == 0)
            EndReflect ();
    }
    void EndReflect () 
    {
        reflecting = false;
        kill?.SetMute (false);
        knocker?.SetMute (false);
		if (knocker)
		{
			knocker.SetMute (false);
			knocker.OverrideForce (-knocker.getForce);
		}
        SetOrigin ();
    }

    public void Detach (float cleanupTime) 
    {
        SetTimer (cleanupTime);
        transform.SetParent (null);
        if (reflectBubbleRoutine != null)
        {
            StopCoroutine (reflectBubbleRoutine);
            if (reflectionBubble != null && reflectionBubble.isActiveAndEnabled)
            {
                reflectionBubble.Cleanup ();
            }
        }
    }
    public void CleanUp () 
    {
        TrashMan.despawn (gameObject);
    }

    void SpawnBubble () 
	{
		if (!gameObject.activeInHierarchy) return;
		this.RestartCoroutine (ReflectBubbleRoutine (), ref reflectBubbleRoutine);
	}

	// GameObject reflectionBubble;
    Coroutine reflectBubbleRoutine;
    IEnumerator ReflectBubbleRoutine () 
    {
		if (reflectionBubble)
			TrashMan.despawn (reflectionBubble.gameObject);
        reflectionBubble = TrashMan.spawn (reflectionBubblePrefab.gameObject, transform.position).GetComponent<LaserReflectBubble> ();
        var bubbleTR = reflectionBubble.transform;
        bubbleTR.localScale = Vector3.zero;
        var maxDist = Vector3.Distance (Vector3.zero, line.GetPosition (1));
        while (reflecting) 
        {
            var curDist = Vector3.Distance (Vector3.zero, line.GetPosition (1));
            bubbleTR.localScale = Vector3.Lerp (Vector3.zero, Vector3.one * reflectionBubbleScale, 1 - (curDist / maxDist));
            yield return null;
        }

        while (bubbleTR.localScale != Vector3.zero)
        {
            var dist = Vector3.Distance (bubbleTR.position, transform.position);
            bubbleTR.localScale = Vector3.Lerp (Vector3.zero, Vector3.one * reflectionBubbleScale, 1 - (dist / maxLength * curLengthPCT));
            yield return null;
        }

        TrashMan.despawn (reflectionBubble.gameObject);
		reflectBubbleRoutine = null;
    }
}