using UnityEngine;

public class LineRendererSight : MonoBehaviour
{

    [SerializeField] float maxDistance = 50;
    [SerializeField] float padding = 0.25f;
	// [SerializeField] float distanceScaling = 1.25f;
    [SerializeField] LayerMask blockingLayers;
	
	[SerializeField] TargetDetector detector;

    LineRenderer liner;
	RaycastOptimizer raycaster;
	float scaleAdjust;

    void Awake () 
    {
        liner = GetComponent<LineRenderer> ();
        liner.useWorldSpace = false;
		scaleAdjust = transform.lossyScale.z;

		if (detector)
		{
			maxDistance = Mathf.Min (maxDistance, detector.getEngageDistance);
		}

		if (!raycaster)
		{
			raycaster = RaycastOptimizer.Create (
				gameObject, 
				new RaycastCommand (transform.position, transform.forward, maxDistance, blockingLayers), 
				Vector3.zero,
				Vector3.forward
			);
		}
		
    }

    void FixedUpdate () 
    {
		var hit = raycaster.hit;
        liner.SetPosition (0, Vector3.zero);
		liner.SetPosition (1, Vector3.forward * (hit ? (raycaster.distance / scaleAdjust) + padding : maxDistance));
    }
}