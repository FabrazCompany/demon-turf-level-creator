﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawGizmoIcon : MonoBehaviour {
	public Vector3 offset;
	public string iconPath;
	public bool allowScaling;
	public bool onlyWhenSelected;

	void OnDrawGizmos () {
		if (!onlyWhenSelected)
			Gizmos.DrawIcon (transform.position + offset, iconPath, allowScaling);
	}
	void OnDrawGizmosSelected () {
		if (onlyWhenSelected)
			Gizmos.DrawIcon (transform.position + offset, iconPath, allowScaling);
	}
}
