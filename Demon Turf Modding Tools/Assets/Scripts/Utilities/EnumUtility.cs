using System;
using System.Collections.Generic;


public static class EnumUtility {
    public static IEnumerable<T> GetValues<T>() {
        return Enum.GetValues(typeof(T)) as T[];
    }
    public static bool IsValue<T> (Enum eventID, T targetID)
    where T : Enum
    {
        return Enum.GetUnderlyingType (eventID.GetType()) == typeof (T)
                && targetID.Equals ((T)eventID);
    }
}