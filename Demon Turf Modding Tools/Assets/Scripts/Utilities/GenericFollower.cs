using UnityEngine;

public class GenericFollower : MonoBehaviour
{
	[SerializeField] bool despawn = true;
	[SerializeField, Conditional ("despawn")] float despawnDelay = 0;
	Transform target;
	void Awake () 
	{
		if (!target)
			enabled = false;
	}

	
	protected virtual void LateUpdate () 
	{
		
		if (!target || !target.gameObject.activeInHierarchy)
		{
			if (despawn)
			{	
				if (despawnDelay > 0)
				{
					TrashMan.despawnAfterDelay (gameObject, despawnDelay);
				}
				else 
				{
					TrashMan.despawn (gameObject);
				}
			}			
			enabled = false;
			return;
		}
		
		transform.position = target.position;
	}

	public void AssignTarget (Transform _target) 
	{
		target = _target;
		enabled = true;
	}
}