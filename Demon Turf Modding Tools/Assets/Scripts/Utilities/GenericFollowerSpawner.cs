using UnityEngine;

public class GenericFollowerSpawner : MonoBehaviour 
{
	[SerializeField] GenericFollower prefab;
	public void SpawnFollower () 
	{
		var follower = TrashMan.spawn (prefab.gameObject, transform.position).GetComponent<GenericFollower> ();
		follower?.AssignTarget (transform);
	}
}