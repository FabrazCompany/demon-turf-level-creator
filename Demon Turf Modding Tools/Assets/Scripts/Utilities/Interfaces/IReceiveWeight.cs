using UnityEngine;

public interface IReceivePlayerWeight
{
    void ApplyPlayerWeight (Vector3 origin, float massVal);
}