using UnityEngine;

public interface ITargeted
{
    void AssignTarget (Transform target);
    void ClearTarget ();
}