using UnityEngine;

public interface IWeighted
{
    float getWeight { get; }
    Rigidbody getRigid { get; }
}
