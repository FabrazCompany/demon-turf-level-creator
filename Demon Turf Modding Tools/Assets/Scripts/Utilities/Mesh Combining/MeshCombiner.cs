using System.Collections.Generic;
using UnityEngine;
using AmplifyImpostors;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Callbacks;
#endif

// [ExecuteInEditMode]
public class MeshCombiner : MonoBehaviour 
{
	[System.Serializable]
	class MeshProxy 
	{
		public bool isSet;
		public MeshFilter filter;
		public MeshRenderer rend;
		public MeshCollider coll;
	}
	struct MeshProxyData 
	{
		public Material material;
		public bool hasPhysics;
		public string tag;
	}
	[SerializeField] MeshRenderer[] meshRends;
	[SerializeField] List<MeshProxy> meshProxies;
#if !DT_EXPORT && !DT_MOD
	[SerializeField] AmplifyImpostor impostor;
#endif
	[SerializeField] bool triggerOnAwake = true;
	bool combined;
	void OnValidate ()
	{
		return;
		// if (impostor == null)
		// {
		// 	impostor = GetComponent<AmplifyImpostor> ();
		// }
		// if (meshRends == null || meshRends.Length == 0)
		// {
		// 	var meshRendsRaw = GetComponentsInChildren<MeshRenderer> ();
		// 	var meshRendsList = new List<MeshRenderer> (meshRendsRaw);
		// 	if (impostor)
		// 	{
		// 		var impostorRend = meshRendsList.Find (i => i.gameObject == impostor.m_lastImpostor);
		// 		if (impostorRend)
		// 			meshRendsList.Remove (impostorRend);
		// 	} 
		// 	for (int i = 0; i < meshRendsRaw.Length; i++)
		// 	{
		// 		var parentCombiner = meshRendsRaw[i].GetComponentInParent<MeshCombiner> ();
		// 		if (parentCombiner != this)
		// 			meshRendsList.Remove (meshRendsRaw[i]);
		// 		if (meshRendsRaw[i].TryGetComponent<TMPro.TextMeshProUGUI> (out var text) || meshRendsRaw[i].TryGetComponent<TMPro.TextMeshPro> (out var textWorld))
		// 			meshRendsList.Remove (meshRendsRaw[i]);
		// 	}


		// 	meshRends = meshRendsList.ToArray ();
		// }
		// if (meshProxies == null || meshProxies.Count == 0)
		// {
		// 	if (meshProxies == null)
		// 		meshProxies = new List<MeshProxy> ();
		// 	PrebakeMeshRenderers ();
		// }
	}
	[ContextMenu ("Generate Mesh Proxies")]
	public void PrebakeMeshRenderers ()
	{
#if UNITY_EDITOR
		// if (meshProxies.Count > 0)
		// {
		// 	for (int i = 0; i < meshProxies.Count; i++)
		// 	{
		// 		DestroyImmediate (meshProxies[i].rend.gameObject);
		// 	}
		// }
		// meshProxies.Clear ();

		// List<Material> matsUsed = new List<Material>();
		// for (int i = 0; i < meshRends.Length; i++)
		// {
		// 	if (impostor && impostor.m_lastImpostor == meshRends[i].gameObject)
		// 		continue;
		// 	var mats = meshRends[i].sharedMaterials;
		// 	for (int ii = 0; ii < mats.Length; ii++)
		// 	{
		// 		if (!matsUsed.Contains(mats[ii]))
		// 			matsUsed.Add (mats[ii]);
		// 	}
		// }

		// for (int i = 0; i < matsUsed.Count; i++)
		// {
		// 	var targetMat = matsUsed[i];
		// 	string tagUsed = "Untagged";
		// 	int layer = LayerMask.NameToLayer ("Default");
		// 	bool physics = false;
		// 	bool hasSlideNever = false;
		// 	bool hasSlideAlways = false;
		// 	bool hasAlwaysFall = false;
		// 	StaticEditorFlags staticFlags = 0;
		// 	for (int ii = 0; ii < meshRends.Length; ii++)
		// 	{
		// 		var rend = meshRends[ii];
		// 		var match = false;
		// 		for (int iii = 0; iii < rend.sharedMaterials.Length; iii++)
		// 		{
		// 			if (rend.sharedMaterials[iii] == targetMat)
		// 			{
		// 				match = true;
		// 				break;
		// 			}
		// 		}
		// 		if (!match)
		// 			continue;

		// 		if (rend.gameObject.layer != LayerMask.NameToLayer ("Default"))
		// 			layer = rend.gameObject.layer;
		// 		if (!rend.CompareTag ("Untagged"))
		// 			tagUsed = rend.gameObject.tag;
		// 		if (rend.TryGetComponent<MeshCollider> (out var phys))
		// 			physics = true;
		// 		if (rend.TryGetComponent<Surface_NeverSlide> (out var neverSlide))
		// 			hasSlideNever = true;
		// 		if (rend.TryGetComponent<Surface_AutoSlide> (out var alwaysSlide))
		// 			hasSlideAlways = true;
		// 		if (rend.TryGetComponent<Surface_AutoFall> (out var autoFall))
		// 			hasAlwaysFall = true;

		// 		staticFlags |= GameObjectUtility.GetStaticEditorFlags (meshRends[i].gameObject);
		// 	}
		// 	var proxy = CreateProxy (targetMat, 
		// 							layer: layer,
		// 							tag: tagUsed,
		// 							hasPhysics: physics,
		// 							hasSlideAlways: hasSlideAlways,
		// 							hasSlideNever: hasSlideNever,
		// 							hasAlwaysFall: hasAlwaysFall
		// 						);
		// 	meshProxies.Add (proxy);
		// 	proxy.filter.gameObject.SetActive (false);
		// }
#endif
	}
	
	void Awake ()
	{
		return;
		if (!Application.isPlaying)
			return;
		if (triggerOnAwake)
			CombineMeshes ();
		StaticBatchingUtility.Combine (gameObject);
	}
	void OnDestroy ()
	{
		for (int i = meshProxies.Count - 1; i >= 0; i--)
		{
			DestroyImmediate (meshProxies[i].rend.gameObject);
		}
	}

// #if UNITY_EDITOR
// 	[PostProcessSceneAttribute (0)]
// 	public static void CombineAllMeshes ()
// 	{
// 		var meshCombiners = SceneUtility.FindAllSceneObjectsIncludingDisabled<MeshCombiner> ();
// 		var length = meshCombiners.Length;
// 		for (int i = 0; i < length; i++)
// 		{
// 			Debug.Log ("Combine", meshCombiners[i]);
// 			meshCombiners[i].CombineMeshes ();
// 		}
// 	}
// #endif

	void CombineMeshes ()
	{
		if (combined)
			return;
		Quaternion oldRot = transform.rotation;
        Vector3 oldPos = transform.position;
		Vector3 oldScale = transform.localScale;

// #if UNITY_EDITOR
// 		StaticEditorFlags[] staticFlags = new StaticEditorFlags[meshRends.Length];
// 		for (int i = 0; i < meshRends.Length; i++)
// 		{
// 			staticFlags[i] = GameObjectUtility.GetStaticEditorFlags (meshRends[i].gameObject);
// 			meshRends[i].gameObject.isStatic = false;
// 		}
// #endif
		// transform.rotation = Quaternion.identity;
        // transform.position = Vector3.zero;
		// transform.localScale = Vector3.one;

		// var meshDict = new Dictionary<Material, List<CombineInstance>> ();
		// for (int i = 0; i < meshRends.Length; i++)
		// {
		// 	var meshRend = meshRends[i];
		// 	if (!meshRend)
		// 		continue;
		// 	var meshFilter = meshRends[i].GetComponent<MeshFilter> ();
		// 	if (meshFilter.sharedMesh == null)
		// 		continue;

		// 	if (!meshFilter.gameObject.activeInHierarchy)
		// 		continue;

		// 	if (impostor && impostor.m_lastImpostor == meshRends[i].gameObject)
		// 		continue;
			
		// 	meshFilter.gameObject.isStatic = false;
		// 	var mesh = meshFilter.sharedMesh;
		// 	var sharedMats = meshRend.sharedMaterials;
		// 	for (int ii = 0; ii < sharedMats.Length; ii++)
		// 	{
		// 		var targetMat = sharedMats[ii];
		// 		if (!meshDict.ContainsKey (targetMat))
		// 			meshDict.Add (targetMat, new List<CombineInstance> ());

		// 		CombineInstance combine = new CombineInstance ();
		// 		combine.mesh = meshFilter.sharedMesh;
		// 		combine.subMeshIndex = ii;
		// 		combine.transform = meshFilter.transform.localToWorldMatrix;
		// 		meshDict[targetMat].Add (combine);
		// 	}
		// 	meshRend.enabled = false;
		// 	if (meshFilter.TryGetComponent<MeshCollider> (out var collider))
		// 		collider.enabled = false;
		// }

		// transform.position = oldPos;
		// transform.rotation = oldRot;
		// transform.localScale = oldScale;
		// foreach (var key in meshDict.Keys)
		// {
		// 	var list = meshDict[key];
		// 	var combinedMesh = new Mesh ();
		// 	combinedMesh.CombineMeshes (list.ToArray (), true, true);
		// 	var proxy = GetMeshProxy (key);
		// 	proxy.filter.mesh = combinedMesh;
		// 	if (proxy.coll)
		// 		proxy.coll.sharedMesh = combinedMesh;
		// 	proxy.rend.sharedMaterial = key;
		// 	proxy.filter.transform.localPosition = Vector3.zero;
		// 	proxy.filter.transform.localEulerAngles = Vector3.zero;
		// 	proxy.filter.gameObject.SetActive (true);
		// 	proxy.isSet = true;
		// }
		// var rends = new Renderer[meshProxies.Count];
		// for (int i = 0; i < rends.Length; i++)
		// {
		// 	rends[i] = meshProxies[i].rend;
		// }

		// if (impostor)
		// {
		// 	impostor.Renderers = rends;
		// }

		// var length = meshRends.Length;
		// for (int i = length - 1; i >= 0; i--)
		// {
		// 	var rend = meshRends[i];
		// 	if (rend.transform.childCount > 0)
		// 		continue;
		// 	var components = rend.gameObject.GetComponents<Component> ();
		// 	var destroy = true;
		// 	for (int ii = 0; ii < components.Length; ii++)
		// 	{
		// 		var component = components[ii];
		// 		var type = component.GetType ();
		// 		if (type != typeof (Transform)
		// 			&& type != typeof (MeshFilter) 
		// 			&& type != typeof (MeshRenderer) 
		// 			&& type != typeof (MeshCollider))
		// 		{
		// 			destroy = true;
		// 			break;
		// 		}
		// 	}
		// 	if (destroy)
		// 		DestroyImmediate (rend.gameObject);
		// }

// #if UNITY_EDITOR
// 		for (int i = 0; i < meshRends.Length; i++)
// 		{
// 			GameObjectUtility.SetStaticEditorFlags (meshRends[i].gameObject, staticFlags[i]);
// 		}
// 		UnityEditor.Undo.RecordObject (gameObject, "Combining Meshes");
// #endif
		triggerOnAwake = false;
		combined = true;
	}
	static void SetStaticFlags (GameObject target)
	{
#if UNITY_EDITOR
		UnityEditor.GameObjectUtility.SetStaticEditorFlags (target, UnityEditor.StaticEditorFlags.BatchingStatic 
																			| UnityEditor.StaticEditorFlags.OccludeeStatic 
																			| UnityEditor.StaticEditorFlags.OccluderStatic 
																			| UnityEditor.StaticEditorFlags.ReflectionProbeStatic);
#endif
	}
	MeshProxy CreateProxy (Material material, 
							int layer = 0,
							string tag = "Untagged",
							bool hasPhysics = false,
							bool hasSlideNever = false,
							bool hasSlideAlways = false,
							bool hasAlwaysFall = false
						)
	{
		var combinedObject = new GameObject ("Combined Mesh");
		combinedObject.transform.SetParent (transform);
		combinedObject.transform.localPosition = Vector3.zero;
		combinedObject.transform.localEulerAngles = Vector3.zero;
		combinedObject.tag = tag;
		combinedObject.layer = layer;
		SetStaticFlags (combinedObject);

		var filter = combinedObject.AddComponent<MeshFilter> ();
		var rend = combinedObject.AddComponent<MeshRenderer> ();
		rend.sharedMaterial = material;
		var coll = hasPhysics ? combinedObject.AddComponent<MeshCollider> () : null;

		if (hasSlideNever)
			combinedObject.AddComponent<Surface_NeverSlide> ();
		if (hasSlideAlways)
			combinedObject.AddComponent<Surface_AutoSlide> ();
		if (hasAlwaysFall)
			combinedObject.AddComponent<Surface_AutoFall> ();		

#if UNITY_EDITOR
		UnityEditor.Undo.RegisterCreatedObjectUndo (combinedObject, "Undo Combined Mesh Submesh");
#endif
		
		return new MeshProxy ()
		{
			filter = filter,
			rend = rend,
			coll = coll,
			isSet = false,
		};
	}
	MeshProxy GetMeshProxy (Material material)
	{
		for (int i = 0; i < meshProxies.Count; i++)
		{
			if (!meshProxies[i].isSet && meshProxies[i].rend.sharedMaterial == material)
				return meshProxies[i];
		}
		meshProxies.Add (CreateProxy (material));
		return meshProxies[meshProxies.Count - 1];
	}
}