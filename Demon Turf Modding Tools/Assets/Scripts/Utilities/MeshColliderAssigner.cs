﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (MeshCollider))]
public class MeshColliderAssigner : MonoBehaviour
{
    [SerializeField] MeshFilter targetMesh;
    void Start () 
    {
        var meshCollider = GetComponent<MeshCollider> ();
        if (meshCollider.sharedMesh == null)
            meshCollider.sharedMesh = targetMesh.mesh;
    }
}
