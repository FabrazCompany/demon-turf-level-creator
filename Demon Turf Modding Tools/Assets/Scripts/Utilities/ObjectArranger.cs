using UnityEngine;
public class ObjectArranger : MonoBehaviour
{

	[SerializeField] Orientation orientation;
	[SerializeField] float radius;
	[SerializeField] bool spaceEvenly = true;
	[SerializeField, Conditional ("spaceEvenly", hide: true)] float spacingAngle = 5f;
	[SerializeField] bool invertFacing;
	[SerializeField] bool ignoreInactiveObjects;
	[SerializeField] Vector3 upwardsAnglingAxis = Vector3.left;
	[SerializeField] float upwardsAngling = 0;
	[SerializeField] bool drawDebug;
	[SerializeField, Conditional ("drawDebug")] float debugBallSize = .5f;
	[SerializeField, Conditional ("drawDebug")] float debugBallLength = 3;
#if UNITY_EDITOR
	void OnValidate () 
	{
		if (!spaceEvenly)
			TransformUtility.ArrangeObjects (transform, radius, orientation, invertFacing, ignoreInactiveObjects, spacingAngle, upwardsAnglingAxis, upwardsAngling);
		else 
			TransformUtility.ArrangeObjects (transform, radius, orientation, invertFacing, ignoreInactiveObjects, upwardsAnglingAxis, upwardsAngling);
	}

	void OnDrawGizmosSelected()
	{
		if (!drawDebug)
			return;
		Gizmos.color = Color.blue;
		foreach (Transform point in transform)
		{
			Gizmos.DrawSphere (point.position, debugBallSize);
			Gizmos.DrawRay (point.position, point.forward);
		}	
	}
#endif
}