using UnityEngine;

public class OnStartPositionToCamera : MonoBehaviour {

    void Start () {
        if (!Camera.main)
            return;

        Debug.Log ("Positioning to camera");
        transform.position = Camera.main.transform.position;
    }
}