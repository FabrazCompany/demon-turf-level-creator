using UnityEngine;

public class ParticleFollower : MonoBehaviour
{
	Transform target;
	ParticleSystem particle;
	void Awake () 
	{
		particle = GetComponent<ParticleSystem> ();
		if (!target)
			enabled = false;
	}
	void OnEnable () 
	{
		particle.Clear ();
	}
	// void OnDisable ()
	// {
	// 	particle.Clear ();
	// }
	
	void LateUpdate () 
	{
		if (!target || !target.gameObject.activeInHierarchy)
		{
			particle.Stop ();
			enabled = false;
			return;
		}

		transform.position = target.position;
	}

	public void Disable () 
	{
		particle.Stop ();
		// enabled = false;
	}

	public void AssignTarget (Transform _target) 
	{
		target = _target;
		transform.position = target.position;
		enabled = true;
	}
}