using UnityEngine;

public class ParticleFollowerSpawner : MonoBehaviour
{
	[SerializeField] ParticleFollower followerPrefab;


	ParticleFollower currentFollower;
	int followerID;
	public void SpawnFollower () 
	{
		currentFollower = TrashMan.spawn (followerPrefab.gameObject).GetComponent<ParticleFollower> ();
		followerID = currentFollower.GetInstanceID ();
		currentFollower?.AssignTarget (transform);
	}
	void OnEnable () 
	{
		currentFollower = null;
	}
	void OnDisable () 
	{
		currentFollower = null;
	}

	public void ClearFollower () 
	{
		// if (!currentFollower || !currentFollower.isActiveAndEnabled)
		// 	return;
		// currentFollower.Disable ();
		// currentFollower = null;
	}
}