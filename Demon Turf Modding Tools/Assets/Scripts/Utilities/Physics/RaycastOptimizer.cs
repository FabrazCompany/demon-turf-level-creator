﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastOptimizer : FlyweightTransformJobBehavior<RaycastOptimizer, RaycastSlotData, RaycastCommand, RaycastOptimizerManager>
{
	RaycastCommand _command;
	bool _hit;
	float _distance;
	float _angle;
	Vector3 _normal;
	
	bool _updateDirection;
	Vector3 _direction;

	public bool hit { get => _hit; set => _hit = value; }
	public float distance { get => _distance; set => _distance = value; }
	public float angle { get => _angle; set => _angle = value; }
	public Vector3 normal { get => _normal; set => _normal = value; }
	public bool updateDirection { get => _updateDirection; set => _updateDirection = value; }
	public Vector3 direction { get => _direction; set => _direction = value; }

	public Vector3 point;
	public Vector3 origin;
	public string target;

	protected override void OnEnable()
	{
		base.OnEnable(); 
		manager.registerExtraData (new RaycastPrepInfo () 
		{
			refreshDirection = _updateDirection,
			direction = _direction
		});
		_hit = false;
		_distance = 0;
		_angle = 0;
		_normal = Vector3.zero;
	}

	protected override RaycastCommand InitializeJobData() => _command;
	static public RaycastOptimizer Create (GameObject _owner, 
											
											RaycastCommand _info,
											Vector3 _offset,
											Vector3? _dir = null
										)
	{
#if UNITY_EDITOR
		var raycasterRoot = new GameObject (_owner.name + " - Raycaster");
#else
		var raycasterRoot = new GameObject ("Raycaster");
#endif
		raycasterRoot.SetActive (false);

		var raycaster = raycasterRoot.AddComponent<RaycastOptimizer> ();
		raycaster.transform.SetParent (_owner.transform);
		raycaster.transform.localPosition = _offset;
		raycaster.transform.localEulerAngles = Vector3.zero;
		raycaster._command = _info;
		raycaster._updateDirection = _dir.HasValue;
		raycaster._direction = _dir.HasValue ? _dir.Value : Vector3.down;
					
		raycasterRoot.SetActive (true);
		return raycaster;
	}
}
