﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;
#if UNITY_EDITOR
using UnityEditor;
#endif // UNITY_EDITOR
using Unity.Jobs;


public struct RaycastSlotData 
{
	// public Transform root;
	// public Vector3 offset;
	// public bool hit;
	// public float angle;
	// public Vector3 normal;
}
public struct GroundingInfo 
{
	public bool hit;
	public float distance;
	public float angle;
	public Vector3 normal;
	public Vector3 point;
	// public int 
}
public struct RaycastPrepInfo
{
	public bool refreshDirection;
	public Vector3 direction;
}

[BurstCompile(CompileSynchronously = true)]
public struct TransformPrepJob : IJobParallelForTransform
{
	[ReadOnly]
	[DeallocateOnJobCompletion]
	[NativeDisableParallelForRestriction]
	public NativeArray<RaycastCommand> raycastCommandsRead;

	[ReadOnly]
	[DeallocateOnJobCompletion]
	[NativeDisableParallelForRestriction]
	public NativeArray<RaycastPrepInfo> raycastInfos; 
	
	[WriteOnly]
	[NativeDisableParallelForRestriction]
	public NativeArray<RaycastCommand> raycastCommandsWrite;

	void IJobParallelForTransform.Execute(int transformIndex, TransformAccess transform)
	{
		var index = transformIndex;
		var data = raycastCommandsRead[index];
		data.from = transform.position;
		var prep = raycastInfos[index];
		if (prep.refreshDirection)
			data.direction = (transform.rotation * prep.direction);
		data.direction.Normalize ();

		raycastCommandsWrite[index] = data;
	}	
}

[BurstCompile(CompileSynchronously = true)]
public struct GroundingProcessingJob : IJobParallelFor
{
	[ReadOnly]
	public NativeArray<RaycastHit> results;
	[WriteOnly]
	public NativeArray<GroundingInfo> groundingInfo;
	void IJobParallelFor.Execute(int transformIndex)
	{
		var input = results [transformIndex];
		var grounding = new GroundingInfo ();
		if (input.distance > 0)
		{
			grounding.hit = true;
			grounding.distance = input.distance;
			grounding.normal = math.cross (math.up (), math.cross (math.up (), input.normal));
			grounding.angle = mathfz.angle (math.up (), input.normal);
			grounding.point = input.point;
		}
		groundingInfo [transformIndex] = grounding;
	}	
}

public class RaycastOptimizerManager : FlyweightTransformJobManager <RaycastOptimizer, RaycastSlotData, RaycastCommand, RaycastOptimizerManager>
{
	bool isJobScheduled;

	bool _raycastPrepInfosAssigned;
	NativeArray<RaycastPrepInfo> _raycastPrepInfos;
	protected ref NativeArray<RaycastPrepInfo> raycastPrepInfos 
	{
		get 
		{
			if (!_raycastPrepInfosAssigned)
			{
				_raycastPrepInfos = new NativeArray<RaycastPrepInfo> (slots.Capacity, Allocator.Persistent);
				_raycastPrepInfosAssigned = true;
			}
			return ref _raycastPrepInfos;
		}
	}

	NativeArray<RaycastCommand> raycastCommands;
	NativeArray<RaycastHit> raycastResults;
	NativeArray<GroundingInfo> groundingResults;
	JobHandle transformPrepHandle;
	JobHandle raycastHandle;
	JobHandle groundingHandle;

	public void registerExtraData (RaycastPrepInfo data)
	{
		ref var t = ref transforms;
		int jobIndex = t.length - 1;
		raycastPrepInfos[jobIndex] = data;
	}
	protected override void swapDataFields(int currentIndex, int lastIndex)
	{
		base.swapDataFields (currentIndex, lastIndex);
		if (!_raycastPrepInfosAssigned) return;
		_raycastPrepInfos[currentIndex] = _raycastPrepInfos[lastIndex];
	}

	void Update ()
	{
		var items = slots.RawItems;
		var length = slots.Count;
		
		if (isJobScheduled)
		{
			isJobScheduled = false;
			JobHandle.CompleteAll (ref transformPrepHandle, ref raycastHandle, ref groundingHandle);
			
			var safeLength = Mathf.Min (length, groundingResults.Length);
			for (int i = 0; i < safeLength; i++)
			{
				ref var slot = ref items[i];
				if (!slot.active || slot.jobIndex >= groundingResults.Length) continue;
				
				var result = groundingResults[slot.jobIndex];
				ref var flyweight = ref slot.flyweight;
				flyweight.hit = result.hit;
				flyweight.distance = result.distance;
				flyweight.angle = result.angle;
				flyweight.normal = result.normal;
				flyweight.point = result.point;

				var command = raycastCommands[slot.jobIndex];
				flyweight.origin = command.from;
			}

			raycastCommands.Dispose ();
			raycastResults.Dispose ();
			groundingResults.Dispose ();
		}
		var totalCount = slots.Count;
		var activeCount = numJobData;
		raycastCommands = new NativeArray<RaycastCommand> (totalCount, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
		raycastResults = new NativeArray<RaycastHit> (totalCount, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
		groundingResults = new NativeArray<GroundingInfo> (totalCount, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
		
		var transformPrepJob = new TransformPrepJob ()
		{	
			raycastCommandsRead = new NativeArray<RaycastCommand> (totalCount, Allocator.TempJob, NativeArrayOptions.UninitializedMemory),
			raycastCommandsWrite = raycastCommands,
			raycastInfos = new NativeArray<RaycastPrepInfo> (totalCount, Allocator.TempJob, NativeArrayOptions.UninitializedMemory),
		};		
		NativeArray<RaycastCommand>.Copy 	(dataFields, 		0, transformPrepJob.raycastCommandsRead, 	0, totalCount);
		NativeArray<RaycastCommand>.Copy 	(dataFields, 		0, transformPrepJob.raycastCommandsWrite, 	0, totalCount);
		NativeArray<RaycastPrepInfo>.Copy 	(raycastPrepInfos, 	0, transformPrepJob.raycastInfos, 			0, totalCount);

		var groundingNormalsJob = new GroundingProcessingJob ()
		{
			results = raycastResults,
			groundingInfo = groundingResults
		};

		transformPrepHandle = transformPrepJob.Schedule (transforms);
		raycastHandle = RaycastCommand.ScheduleBatch (transformPrepJob.raycastCommandsWrite, raycastResults, 1, transformPrepHandle);
		groundingHandle = groundingNormalsJob.Schedule (totalCount, 32, raycastHandle);
		isJobScheduled = true;
	}
	public override void unregisterAll()
	{
		if (isJobScheduled)
		{
			JobHandle.CompleteAll (ref transformPrepHandle, ref raycastHandle, ref groundingHandle);
			isJobScheduled = false;
		}
		base.unregisterAll();
		if (_raycastPrepInfosAssigned)
		{
			_raycastPrepInfos.Dispose ();
			_raycastPrepInfosAssigned = false;
		}
		if (raycastResults.IsCreated)
			raycastResults.Dispose ();
		if (groundingResults.IsCreated)
			groundingResults.Dispose ();
		if (raycastCommands.IsCreated)
			raycastCommands.Dispose ();
	} 
}