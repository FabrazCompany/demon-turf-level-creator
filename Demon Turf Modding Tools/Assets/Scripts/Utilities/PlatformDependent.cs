using UnityEngine;

#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

public class PlatformDependent : MonoBehaviour
{
	[SerializeField] RuntimePlatform[] supportedPlatforms;
	[SerializeField] bool requireStrongerVariants;

	void Awake ()
	{
		for (int i = 0; i < supportedPlatforms.Length; i++)
		{
			if (supportedPlatforms[i] == Application.platform)
			{
				if (requireStrongerVariants)
				{
					#if UNITY_GAMECORE_XBOXSERIES && !UNITY_EDITOR
					if (UnityEngine.GameCore.Hardware.version == UnityEngine.GameCore.HardwareVersion.XboxSeriesX
						|| UnityEngine.GameCore.Hardware.version == UnityEngine.GameCore.HardwareVersion.XboxScarlettDevkit)
						return;
					#else 
					return;
					#endif
				}
				else 
					return;
			}
				
		}
		gameObject.SetActive (false);
	}
	

#if UNITY_EDITOR
	// public static void UpdatePlatformDependencies ()
	// {
	// 	try 
	// 	{
	// 		Debug.Log ("Scanning All Prefabs");
	// 		AssetDatabase.StartAssetEditing ();
	// 		var path = "Assets/Prefabs/";
	// 		var list = new List<RuntimePlatform> ();
	// 		var objects = AssetDatabase.FindAssets ("t:prefab", new string[] {path});
	// 		var total = objects.Length;
	// 		EditorUtility.DisplayProgressBar ("Processing Prefabs", "", 0/total);
	// 		for (int i = 0; i < total; i++)
	// 		{
	// 			var assetPath = AssetDatabase.GUIDToAssetPath (objects[i]);
	// 			var prefabRoot = PrefabUtility.LoadPrefabContents (assetPath);
	// 			if (EditorUtility.DisplayCancelableProgressBar ("Processing Prefabs", prefabRoot.name, (float)i/(float)total))
	// 				break;
	// 			var trs = prefabRoot.GetComponentsInChildren<Transform> ();
	// 			var updateCount = 0;
	// 			foreach (var tr in trs)
	// 			{
	// 				if (tr.TryGetComponent<PlatformDependent> (out var platformDependent) 
	// 					&& EnsurePlatformList (platformDependent, list))
	// 					updateCount++;
	// 			}
	// 			if (updateCount > 0)
	// 				PrefabUtility.SaveAsPrefabAsset (prefabRoot, assetPath);
	// 			PrefabUtility.UnloadPrefabContents (prefabRoot);
	// 		}

	// 		//scan all levels
	// 		Debug.Log ("Scanning All Levels");
	// 		var scenes = EditorBuildSettings.scenes;
    //     	var totalScenes = (float)scenes.Length;
	// 		var start = 0;
	// 		var end = scenes.Length;
	// 		// processingIndex = 0;
	// 		for (int i = start; i < end; i++)
	// 		{
	// 			var prevIndex = Mathf.Max (i - 1, 0);
	// 			var scene = scenes[i];
	// 			var sceneObj = EditorSceneManager.OpenScene (scene.path, OpenSceneMode.Single);
				
	// 			EditorSceneManager.SetActiveScene (sceneObj);

	// 			var getInstances = SceneUtility.FindAllSceneComponents<PlatformDependent> (sceneObj, true);
	// 			// var state = ProcessSceneInstance_SceneCurrent ($"Processing {sceneObj.name}: {i}/{total}", 2000);
	// 			// if (state == State.Error)
	// 			// 	break;
	// 			// else if (state == State.Partial)
	// 			// 	i = prevIndex;
				
	// 			// if (state == State.Success || state == State.Partial)
	// 			// {
	// 			// 	if (!EditorSceneManager.MarkSceneDirty (sceneObj))
	// 			// 	{
	// 			// 		Debug.LogError ($"Scene [{Path.GetFileName (scene.path)}] was not marked as ditry");
	// 			// 		i = prevIndex;
	// 			// 		continue;
	// 			// 	}
						
	// 			// 	if (!EditorSceneManager.SaveScene (sceneObj))
	// 			// 	{
	// 			// 		Debug.LogError ($"Scene [{Path.GetFileName (scene.path)}] was not saved");
	// 			// 		i = prevIndex;
	// 			// 		continue;
	// 			// 	}
	// 			// }

	// 			// if (i == 29)
	// 			// 	i = 37;
	// 		}

	// 		AssetDatabase.StopAssetEditing ();
	// 	}
	// 	catch 
	// 	{
	// 		EditorUtility.ClearProgressBar ();
	// 		AssetDatabase.StopAssetEditing ();
	// 	}
		
		
	// }

	// static bool ProcessScene (string label = "Processing Prefabs", int? mbLimit = null)
	// {
	// 	var selections = EditorSceneManager.GetActiveScene ().GetAllGameObjectsInScene ();
	// 	if (selections == null || selections.Length == 0)
	// 		return State.Unnecessary;
		
	// 	int revertCount = 0;
	// 	try
	// 	{	
	// 		// InitCounters ();
	// 		int total = selections.Length;
	// 		int start = Mathf.Min (processingIndex, total);
	// 		EditorUtility.DisplayProgressBar (label, "", 0/total);
	// 		for (int i = start; i < total; i++)
	// 		{
	// 			var selection = selections[i];
	// 			if (EditorUtility.DisplayCancelableProgressBar (label, $"Revert Count: {revertCount}, Processing: {selection.name}", (float)i/(float)total))
	// 				return State.Error;
	// 			if (ProcessInstance (selection, false))
	// 				revertCount++;
	// 			if (mbLimit.HasValue)
	// 			{
	// 				// var val = ramCounter.NextValue ();
	// 				if (revertCount >= mbLimit.Value)
	// 				{
	// 					processingIndex = i;
	// 					return State.Partial;
	// 				}
	// 			} 
	// 		}
			
	// 		Debug.Log ($"Revert Count: {revertCount}");
	// 	}
	// 	catch (System.Exception ex)
	// 	{
	// 		EditorUtility.ClearProgressBar ();
	// 	}
	// 	EditorUtility.ClearProgressBar ();
	// 	processingIndex = 0;
	// 	return revertCount > 0 ? State.Success : State.Unnecessary;
	// }


	// GameCoreScarlett = 36,
	// GameCoreXboxSeries = 36,
	// GameCoreXboxOne = 37,

	static bool EnsurePlatformList (PlatformDependent platformDependent, List<RuntimePlatform> list)
	{
		int changeCount = 0;
		list.Clear ();
		list.AddRange (platformDependent.supportedPlatforms);
		if (list.Contains (RuntimePlatform.XboxOne))
		{
			var index = list.IndexOf (RuntimePlatform.XboxOne);
			
			if (!list.Contains (Game.runtimePlatform_GameCoreSeriesX))	//RuntimePlatform.GameCoreXboxSeries
			{
				list.Insert (index + 1, Game.runtimePlatform_GameCoreSeriesX);
				changeCount++;
			}
			if (!list.Contains (Game.runtimePlatform_GameCoreXboxOne))	//RuntimePlatform.GameCoreXboxOne
			{
				list.Insert (index + 1, Game.runtimePlatform_GameCoreXboxOne);
				changeCount++;
			}
		}

		if (list.Contains (RuntimePlatform.PS4))
		{
			if (!list.Contains (RuntimePlatform.PS5))
			{
				list.Insert (list.IndexOf (RuntimePlatform.PS4), RuntimePlatform.PS5);
				changeCount++;
			}
		}

		if (changeCount > 0)
		{
			platformDependent.supportedPlatforms = list.ToArray ();
		}
		return changeCount > 0;
	}
#endif
}