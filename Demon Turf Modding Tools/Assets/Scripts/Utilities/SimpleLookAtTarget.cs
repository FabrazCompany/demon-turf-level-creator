﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleLookAtTarget : MonoBehaviour
{
    public Transform target;
    void LateUpdate()
    {
        if (!target)
            return;
        transform.LookAt (target);
    }
}
