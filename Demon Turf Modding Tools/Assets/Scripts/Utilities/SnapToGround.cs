﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapToGround : MonoBehaviour
{
    [SerializeField] float placementBuffer;
    [SerializeField] float checkBuffer = 1;
    [SerializeField] float checkDistance = 15;
    [SerializeField] LayerMask platformLayer;
    [SerializeField] bool despawnIfNoHit;

    public void Snap () 
    {
        var pos = transform.position;
        pos.y += checkBuffer;

        if (Physics.Raycast (pos, Vector3.down, out var info, checkDistance + checkBuffer, platformLayer, QueryTriggerInteraction.Ignore))
        {
            pos = info.point;
            pos.y += placementBuffer;
            transform.position = pos;
        }
        else 
            TrashMan.despawn (gameObject);
    }
}
