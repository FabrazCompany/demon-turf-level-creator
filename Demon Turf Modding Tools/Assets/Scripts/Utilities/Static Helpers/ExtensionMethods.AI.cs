using UnityEngine;
#if !DT_EXPORT && !DT_MOD
using Pathfinding;
#endif

public static partial class ExtensionMethods 
{
	
#if !DT_EXPORT && !DT_MOD	
	static public bool InGraph (this AIPath self)
	{
		var nnConstraint = NNConstraint.Default;
		var nearestPoint = AstarPath.active.GetNearest(self.position).position;
		nearestPoint.y = self.position.y;
		return ((nearestPoint - self.position).sqrMagnitude > 0.5*0.5); // Or some other very small constant
	}
#endif
}