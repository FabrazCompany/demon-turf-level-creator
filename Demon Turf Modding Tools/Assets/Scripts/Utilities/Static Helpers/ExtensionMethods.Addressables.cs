using System;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public static partial class ExtensionMethods 
{
	public static void GetAsset<T> (this AssetReference assetRef, Action<T> onLoad)
	{
		if (assetRef.OperationHandle.IsValid ())
		{
			if (!assetRef.OperationHandle.IsDone)
			{
				assetRef.OperationHandle.Completed += (loadRequest) => 
				{
					if (loadRequest.IsValid () && loadRequest.Result != null)
						onLoad?.Invoke ((T)loadRequest.Result);
				};
			}
			else 
			{
				onLoad?.Invoke ((T)assetRef.OperationHandle.Result);
			}
		}
		else 
		{
			assetRef.LoadAssetAsync<T> ().Completed += (loadRequest) => 
			{
				if (loadRequest.IsValid () && loadRequest.Result != null)
					onLoad?.Invoke (loadRequest.Result);
			};
		}
	}
	public static void GetAssets<T> (this AssetReference assetRef, Action<T[]> onLoad)
	{
		if (assetRef.OperationHandle.IsValid ())
		{
			if (!assetRef.OperationHandle.IsDone)
			{
				assetRef.OperationHandle.Completed += (loadRequest) => 
				{
					if (loadRequest.IsValid () && loadRequest.Result != null)
						onLoad?.Invoke ((T[])loadRequest.Result);
				};
			}
			else 
			{
				onLoad?.Invoke ((T[])assetRef.OperationHandle.Result);
			}
		}
		else 
		{
			assetRef.LoadAssetAsync<T[]> ().Completed += (loadRequest) => 
			{
				if (loadRequest.IsValid () && loadRequest.Result != null)
					onLoad?.Invoke (loadRequest.Result);
			};
		}
	}
}