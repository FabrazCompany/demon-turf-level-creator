using System.Collections.Generic;

public static partial class ExtensionMethods 
{
	public static T Peek<T> (this List<T> list, T defaultVal = default (T)) => list.Count > 0 ? list[list.Count - 1] : defaultVal;
    public static void Push<T> (this List<T> list, T item) => list.Add (item);
    public static T Pop<T> (this List<T> list, T defaultVal = default (T))
    {
        if (list.Count > 0)
        {
            T temp = list[list.Count - 1];
            list.RemoveAt(list.Count - 1);
            return temp;
        }
        else
            return defaultVal;
    }

	public static bool IsIn<T>(this T val, params T[] possibles)
    {
		for (int i = 0; i < possibles.Length; i++)
		{
			if (possibles[i].Equals (val))
				return true;
		}
		return false;
        // return possibles.Contains(@this);
    }
    public static T2 TryGetValueOrDefault<T1, T2> (this Dictionary<T1, T2> _dict, T1 _key, T2 _default) {
        return _dict != null && _dict.ContainsKey (_key) 
            ? _dict [_key] 
            : _default;
    }
	public static void Set<T1, T2> (this Dictionary<T1, T2> _dict, T1 _key, T2 _value) {
		if (_dict == null)
			return;
		if (_dict.ContainsKey (_key))
			_dict[_key] = _value;
		else 
			_dict.Add (_key, _value);
    }
}