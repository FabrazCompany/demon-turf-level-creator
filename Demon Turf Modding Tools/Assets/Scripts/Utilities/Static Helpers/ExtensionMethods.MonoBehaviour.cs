using System;
using System.Runtime.CompilerServices;
using System.Collections;
using UnityEngine;

public static partial class ExtensionMethods 
{
	public static T AddComponent<T>(this GameObject obj, System.Action<T> onInit) where T : Component {
        bool oldState = obj.activeSelf;
        obj.SetActive(false);
        T comp = obj.AddComponent<T>();
        onInit?.Invoke(comp);
        obj.SetActive(oldState);
        return comp;
    }

	public static T GetOrAddComponent<T> (this GameObject behaviour) where T : Component => behaviour.TryGetComponent<T> (out var comp) ? comp : behaviour.gameObject.AddComponent<T> ();
	public static T GetOrAddComponent<T> (this MonoBehaviour behaviour) where T : Component => behaviour.TryGetComponent<T> (out var comp) ? comp : behaviour.gameObject.AddComponent<T> ();
    public static void RestartCoroutine (this MonoBehaviour behaviour, IEnumerator method, ref Coroutine routine) {
        if (routine != null)
            behaviour.StopCoroutine (routine);
        routine = behaviour.StartCoroutine (method);
    }

    public static IEnumerator WaitFrameAndDo (this MonoBehaviour behaviour, Action action) {
        yield return null;
        action();
    }
	public static IEnumerator WaitEndOfFrameAndDo (this MonoBehaviour behaviour, Action action) {
        yield return new WaitForEndOfFrame ();
        action();
    }
	public static IEnumerator WaitFixedFrameAndDo (this MonoBehaviour behaviour, Action action) {
        yield return new WaitForFixedUpdate ();
        action();
    }
    public static IEnumerator WaitAndDo (this MonoBehaviour behaviour, float time, Action action) {
        yield return new WaitForSeconds (time);
        action();
    }

    public static IEnumerator WaitAndDoRealtime (this MonoBehaviour behaviour, float time, Action action) {
        yield return new WaitForSecondsRealtime (time);
        action();
    }

	public static IEnumerator WaitUntilAndDo (this MonoBehaviour behaviour, Func<bool> condition, Action action)
	{
		yield return new WaitUntil (condition);
		action?.Invoke ();
	}

	

}