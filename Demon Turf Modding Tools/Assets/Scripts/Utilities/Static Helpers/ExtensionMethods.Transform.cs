using UnityEngine;

public static partial class ExtensionMethods 
{

	public static bool IsChild (this Transform tr, Transform other) {
        var _tr = tr;
        while (_tr != null) {
            // if (_tr.parent != null) 
            _tr = _tr.parent;
            if (_tr == other)
                return true;

        }
        return false;
    }
	public static Vector3 rightScaled (this Transform tr) {
        return tr.right * tr.lossyScale.normalized.x;
    }
    public static Vector3 upScaled (this Transform tr) {
        return tr.up * tr.lossyScale.normalized.y;
    }
    public static Vector3 GetDirection (this Transform tr, Orientation orientation) {
        switch (orientation) {
            default:
            case Orientation.x:
                return tr.right;
            case Orientation.y:
                return tr.up;
            case Orientation.z:
                return tr.forward;
        }
    }
	public static Vector3 Heading (this Transform owner, Transform target) => owner.Heading (target.position);
	public static Vector3 Heading (this Transform owner, Vector3 target) => (target - owner.position).normalized;
	public static Vector3 HeadingPlanar (this Transform owner, Transform target, Orientation orientation = Orientation.y) => owner.HeadingPlanar (target.position, orientation);
	public static Vector3 HeadingPlanar (this Transform owner, Vector3 target, Orientation orientation = Orientation.y)
	{
		var heading = (target - owner.position);
		switch (orientation)
		{
			case Orientation.x:
				heading.x = 0;
				break;
			case Orientation.y:
				heading.y = 0;
				break;
			case Orientation.z:
				heading.z = 0;
				break;
		}
		return heading.normalized;
	}
	public static float Angle (this Transform owner, Transform target) 
	{
		return Vector3.Angle (owner.forward, owner.Heading (target.position));
	}
	public static float AxisAngle (this Transform owner, Transform target, Orientation orientation = Orientation.y) 
	{
		var forward = owner.forward.normalized;
		var heading = owner.HeadingPlanar (target, orientation).normalized;
		var direction = owner.GetDirection (orientation).normalized;
		var angle = Vector3.SignedAngle (forward, heading, direction); 
		return angle;
	} 
	
	public static float DistanceSqr (this Transform owner, Vector3 target) => (target - owner.position).sqrMagnitude;
	public static float DistanceSqrPlanar (this Transform owner, Vector3 target) => (target - owner.position).With (y: 0).sqrMagnitude;
	public static float DistancePlanar (this Transform owner, Vector3 target) => (target - owner.position).With (y: 0).magnitude;
	public static bool InRange (this Transform owner, Vector3 target, float distance) => owner.DistanceSqr (target) <= Mathf.Pow (distance, 2);
	public static bool InRangePlanar (this Transform owner, Vector3 target, float distance) => owner.DistanceSqr (target.With (y: owner.position.y)) <= Mathf.Pow (distance, 2);

}