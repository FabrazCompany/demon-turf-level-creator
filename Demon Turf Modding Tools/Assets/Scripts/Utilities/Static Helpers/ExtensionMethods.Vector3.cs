using UnityEngine;

public static partial class ExtensionMethods 
{
	public static float DistanceSqr (this Vector3 self, Vector3 target) => (target - self).sqrMagnitude;
	public static float DistanceSqrPlanar (this Vector3 self, Vector3 target) => (target - self).With (y: 0).sqrMagnitude;
	public static Vector2 With (this Vector2 original, float? x = null, float? y = null) => new Vector2 (x ?? original.x, y ?? original.y);
    public static Vector3 With (this Vector3 original, float? x = null, float? y = null, float? z = null) => new Vector3 (x ?? original.x, y ?? original.y, z ?? original.z);
    public static bool IsInfinity (this Vector3 original) => float.IsInfinity (original.x) || float.IsInfinity (original.y) || float.IsInfinity (original.z);
	public static bool IsNan (this Vector3 original) => float.IsNaN (original.x) || float.IsNaN (original.y) || float.IsNaN (original.z);
    public static Vector3 Times (this Vector3 vec, Vector3 adj) {
        var val = vec;
        val.x *= adj.x;
        val.y *= adj.y;
        val.z *= adj.z;
        return val;
    }

}