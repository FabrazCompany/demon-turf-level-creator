using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using TMPro;

public static partial class ExtensionMethods 
{
	


	

    public static int Overlap (this BoxCollider box, Collider[] results, LayerMask layer)
    {
        return Physics.OverlapBoxNonAlloc (box.bounds.center, Vector3.Scale (box.size, box.transform.lossyScale) / 2, results, box.transform.rotation, layer); 
    }

    

    public static Color MoveTowards (this Color col, Color targetCol, float magnitude) {
        var color = col;
        color.r = Mathf.MoveTowards (color.r, targetCol.r, magnitude);
        color.g = Mathf.MoveTowards (color.g, targetCol.g, magnitude);
        color.b = Mathf.MoveTowards (color.b, targetCol.b, magnitude);
        color.a = Mathf.MoveTowards (color.a, targetCol.a, magnitude);
        return color;
    }
    public static Color With (this Color original, float? r = null, float? g = null, float? b = null, float? a = null) {
        return new Color (r ?? original.r, g ?? original.g, b ?? original.b, a ?? original.a);
    }
	public static Texture2D CopyToReadableTexture(this Texture source, Texture2D processingArray, int width, int height)
	{
		var tmp = RenderTexture.GetTemporary(width, height, 0, RenderTextureFormat.Default, RenderTextureReadWrite.Linear);
		var prevFilterMode = source.filterMode;
		source.filterMode = FilterMode.Point;
		tmp.filterMode = FilterMode.Point;
		Graphics.Blit(source, tmp);
		source.filterMode = prevFilterMode;

		RenderTexture previous = RenderTexture.active;
		RenderTexture.active = tmp;

		// var result = new Texture2D(width, height, TextureFormat.RGB24, false);
		processingArray.filterMode = FilterMode.Point;
		processingArray.ReadPixels(new Rect(0, 0, width, height), 0, 0);
		processingArray.Apply();

		RenderTexture.active = previous;
		RenderTexture.ReleaseTemporary(tmp);

		return processingArray;
	}
	public static Texture2D DeCompress (this Texture2D source)
	{
		RenderTexture renderTex = RenderTexture.GetTemporary(
                    source.width,
                    source.height,
                    0,
                    RenderTextureFormat.Default,
                    RenderTextureReadWrite.Linear);

        Graphics.Blit(source, renderTex);
        RenderTexture previous = RenderTexture.active;
        RenderTexture.active = renderTex;
        Texture2D readableText = new Texture2D(source.width, source.height);
        readableText.ReadPixels(new Rect(0, 0, renderTex.width, renderTex.height), 0, 0);
        readableText.Apply();
        RenderTexture.active = previous;
        RenderTexture.ReleaseTemporary(renderTex);
        return readableText;
	}

    public static float ToAngle (this float val) {
        var returnVal = val;
        if (returnVal > 360)
            returnVal -= 360;
        else if (returnVal < 0)
            returnVal += 360;
        return returnVal;
    }

	

    public static string SplitCamelCase( this string str ) {
        return System.Text.RegularExpressions.Regex.Replace( 
            System.Text.RegularExpressions.Regex.Replace( 
                str, 
                @"(\P{Ll})(\P{Ll}\p{Ll})", 
                "$1 $2" 
            ), 
            @"(\p{Ll})(\P{Ll})", 
            "$1 $2" 
        );
    }
	

    public static bool HasParameter (this Animator behavior, string id)
    {
        for (int i = 0; i < behavior.parameters.Length; i++) 
        {
            if (behavior.parameters[i].name == id)
            {
                return true;
            }
        }
        return false;
    }
    public static int GetDominantLayer(this Animator anim) 
    {
        int dominant_index = 0;
        float dominant_weight = 0f;
        float weight = 0f;
        for (int index = 0; index < anim.layerCount; index++) {
            weight = anim.GetLayerWeight(index);
            if (weight > 0 && weight >= dominant_weight) {
                dominant_weight = weight;
                dominant_index = index;
            }
        }
        return dominant_index;
    }

    public static bool ContainsLayer (this LayerMask mask, int layer) 
    {
        return mask == (mask | (1 << layer));
    }

    public static float EvaluateNonRandom (this ParticleSystem.MinMaxCurve scalar, float time)
    {
        switch (scalar.mode)
        {
            default:
                return scalar.Evaluate (time);
            case ParticleSystemCurveMode.Constant:
                return scalar.constant;
            case ParticleSystemCurveMode.TwoConstants:
                return Mathf.Lerp (scalar.constantMin, scalar.constantMax, time);
            case ParticleSystemCurveMode.Curve:
                return scalar.curve.Evaluate (time);
        }
    }

	public static void AddOrSet (this PositionConstraint constraint, int ind, Transform target)
	{
		ind = Mathf.Max (ind, 0);
		if (constraint.sourceCount > ind)
			constraint.SetSource (ind, new ConstraintSource () { sourceTransform = target, weight = 1});
		else 
			constraint.AddSource (new ConstraintSource () { sourceTransform = target, weight = 1});
	}


	
#if UNITY_EDITOR
#if !DT_EXPORT && !DT_MOD
    [UnityEditor.MenuItem ("CONTEXT/GraphUpdateScene/Snap Bounds")]
    public static void SnapBounds (UnityEditor.MenuCommand command) 
	{
		var graphUpdater = (Pathfinding.GraphUpdateScene)command.context;
		graphUpdater.points = new Vector3[4];
		graphUpdater.points[0] = new Vector3 (-graphUpdater.minBoundsHeight, 0, -graphUpdater.minBoundsHeight);
		graphUpdater.points[1] = new Vector3 (graphUpdater.minBoundsHeight, 0, -graphUpdater.minBoundsHeight);
		graphUpdater.points[2] = new Vector3 (graphUpdater.minBoundsHeight, 0, graphUpdater.minBoundsHeight);
		graphUpdater.points[3] = new Vector3 (-graphUpdater.minBoundsHeight, 0, graphUpdater.minBoundsHeight);
    }
#endif
#endif
}