using UnityEngine;
using System.Collections.Generic;
using Unity.Mathematics;

public static partial class FzMath 
{
	static public float SinPositive (float time) => (Mathf.Sin (time) + 1) * 0.5f;

	static public float InverseLerp (Vector3 a, Vector3 b, Vector3 value) {
		Vector3 AB = b - a;
		Vector3 AV = value - a;
		return Vector3.Dot(AV, AB) / Vector3.Dot(AB, AB);
	}

    static public float Remap (this float value, float from1, float to1, float from2, float to2) => (value - from1) / (to1 - from1) * (to2 - from2) + from2;

	static public float ClampAngle(float angle, float min, float max) {
		angle = Mathf.Repeat(angle, 360);
		min = Mathf.Repeat(min, 360);
		max = Mathf.Repeat(max, 360);
		bool inverse = false;
		var tmin = min;
		var tangle = angle;
		if(min > 180)
		{
			inverse = !inverse;
			tmin -= 180;
		}
		if(angle > 180)
		{
			inverse = !inverse;
			tangle -= 180;
		}
		var result = !inverse ? tangle > tmin : tangle < tmin;
		if(!result)
			angle = min;

		inverse = false;
		tangle = angle;
		var tmax = max;
		if(angle > 180)
		{
			inverse = !inverse;
			tangle -= 180;
		}
		if(max > 180)
		{
			inverse = !inverse;
			tmax -= 180;
		}
	
		result = !inverse ? tangle < tmax : tangle > tmax;
		if(!result)
			angle = max;
		return angle;
	}
	static public Vector3 SmoothDampAngle (Vector3 current, Vector3 target, ref Vector3 reorientVelocity, float smoothTime, float maxSpeed) 
	{
		return new Vector3 (
			Mathf.SmoothDampAngle (current.x, target.x, ref reorientVelocity.x, smoothTime, maxSpeed),
			Mathf.SmoothDampAngle (current.y, target.y, ref reorientVelocity.y, smoothTime, maxSpeed),
			Mathf.SmoothDampAngle (current.z, target.z, ref reorientVelocity.z, smoothTime, maxSpeed)
		);
	}


    static public Vector2 CircumfrancePoint (Vector2 origin, float r, float a) {
		float x = origin.x + r * Mathf.Cos (a);
		float y = origin.y + r * Mathf.Sin (a);
		return new Vector2 (x, y);
	}

    static public Vector2 CircumfrancePoint (Vector2 origin, float r, Vector2 dir) {
        Vector2 normal = dir.normalized;
        float x = origin.x + r * normal.x;
        float y = origin.y + r * normal.y;
		return new Vector2 (x, y);
	}

	static public Vector2 GetPointOnUnitCircleCircumference()
	{
		float randomAngle = UnityEngine.Random.Range(0f, Mathf.PI * 2f);
		return new Vector2(Mathf.Sin(randomAngle), Mathf.Cos(randomAngle)).normalized;
	}


    static public Vector2 RotateByRadians(Vector2 Center, Vector2 A, float angle)
    {
        //Move calculation to 0,0
        Vector2 v = A - Center;

        //rotate x and y
        float x = v.x * Mathf.Cos(angle) + v.y * Mathf.Sin(angle);
        float y = v.y * Mathf.Cos(angle) - v.x * Mathf.Sin(angle);

        //move back to center
        Vector2 B = new Vector2(x, y) + Center;

        return B;
    }

	static public Vector2 Rotate(this Vector2 v, float degrees) {
		float radians = degrees * Mathf.Deg2Rad;
		float sin = Mathf.Sin(radians);
		float cos = Mathf.Cos(radians);
		
		float tx = v.x;
		float ty = v.y;

		return new Vector2(cos * tx - sin * ty, sin * tx + cos * ty);
	}
	static public Vector2 FloatToVector2 (float angle) {
		float myAngleInRadians = angle * Mathf.Deg2Rad;
		return new Vector2(
			(float)Mathf.Cos(myAngleInRadians),
			-(float)Mathf.Sin(myAngleInRadians));
	}
	
	static public float Vector2ToFloat (Vector2 angle) => (float)Mathf.Atan2(angle.x, -angle.y);

	static public Vector3 Rotate (this Vector3 vector, float degrees) => vector.Rotate (degrees, Vector3.up);
	
	static public Vector3 Rotate (this Vector3 vector, float degrees, Vector3 axis) => Quaternion.AngleAxis(-degrees, axis) * vector;

    // static public Vector3[] MakeSmoothCurve(Vector3[] arrayToCurve, float smoothness) {
	// 	List<Vector3> points;
	// 	List<Vector3> curvedPoints;
	// 	int pointsLength = 0;
	// 	int curvedLength = 0;

	// 	if(smoothness < 1.0f) smoothness = 1.0f;

	// 	pointsLength = arrayToCurve.Length;

	// 	curvedLength = (pointsLength*Mathf.RoundToInt(smoothness))-1;
	// 	curvedPoints = new List<Vector3>(curvedLength);

	// 	Debug.Log (curvedLength);
	// 	float t = 0.0f;
	// 	for(int pointInTimeOnCurve = 0; pointInTimeOnCurve < curvedLength+1; pointInTimeOnCurve++){
	// 		t = Mathf.InverseLerp(0,curvedLength,pointInTimeOnCurve);

	// 		points = new List<Vector3>(arrayToCurve);

	// 		for(int j = pointsLength-1; j > 0; j--){
	// 			for (int i = 0; i < j; i++){
	// 				points[i] = (1-t)*points[i] + t*points[i+1];
	// 			}
	// 		}
	// 		curvedPoints.Add(points[0]);
	// 	}
	// 	Debug.Log (curvedPoints.Count);
	// 	return(curvedPoints.ToArray());
	// }
	static public void MakeSmoothCurve(ref Vector3[] listToFill, Vector3[] arrayToCurve, Vector3[] processingArray) {
		var pointsLength = arrayToCurve.Length;
		var curvedLength = listToFill.Length - 1;
		float t = 0.0f;
		for(int pointInTimeOnCurve = 0; pointInTimeOnCurve < curvedLength+1; pointInTimeOnCurve++){
			t = Mathf.InverseLerp(0,curvedLength,pointInTimeOnCurve);
			arrayToCurve.CopyTo (processingArray, 0);

			for(int j = pointsLength-1; j > 0; j--){
				for (int i = 0; i < j; i++){
					processingArray[i] = (1-t)*processingArray[i] + t*processingArray[i+1];
				}
			}
			listToFill[pointInTimeOnCurve] = processingArray[0];
		}
	}

	static public Vector3[] CalculateCirclePoints (Orientation orientation, float radius, int segments, float startAngle, float targetAngle, bool flip = false) {
        List<Vector3> points = new List<Vector3> ();

		float x = 0;
		float y = 0;
		float z = 0;
		
		float _angle = startAngle;
		for (int i = 0; i < (segments + 1); i++) {
            switch (orientation) {
                default:
                case Orientation.z:
                    x = (Mathf.Sin (Mathf.Deg2Rad * _angle) * radius);
			        y = (Mathf.Cos (Mathf.Deg2Rad * _angle) * radius);
                    break;
                case Orientation.x:
                    z = (Mathf.Sin (Mathf.Deg2Rad * _angle) * radius);
			        y = (Mathf.Cos (Mathf.Deg2Rad * _angle) * radius);
                    break;
                case Orientation.y:
                    x = (Mathf.Sin (Mathf.Deg2Rad * _angle) * radius);
			        z = (Mathf.Cos (Mathf.Deg2Rad * _angle) * radius);
                    break;
            }
			
			Vector3 pos = new Vector3(x,y,z);
            points.Add (pos);
			_angle += (targetAngle / segments);
		}

		if (flip) 
			points.Reverse ();

		return points.ToArray ();
	}

	static public Vector3[] CalculateLinePoints (Transform transform, Orientation orientation, int segments, float spacing, bool flip = false) {
        List<Vector3> points = new List<Vector3> ();
		Vector3 point = Vector3.zero;
		for (int i = 0; i < segments; i++) {
			points.Add (point);
			switch (orientation) {
				case Orientation.x:
					point += transform.right * spacing;
					break;
				case Orientation.y:
					point += transform.up * spacing;
					break;
				case Orientation.z:
					point += transform.forward * spacing;
					break;
			}
		}
		if (flip)
			points.Reverse ();
			
		return points.ToArray ();
	}

	static public Vector3 DetermineSpread (Vector3 direction, int total, int current, float spread)
	{
		return direction.Rotate (
			(total % 2 == 1 ? 0 : spread * .5f)
			+ (spread * (current == 0 ? 0 : (current + 1) / 2) * (current % 2 == 0 ? 1 : -1)));
	}


	static public Vector3 MoveTowardsAngle (Vector3 current, Vector3 target, float delta)
	{
		// var newAngle = current;
		// newAngle.x = Mathf.MoveTowardsAngle (current.x, target.x, delta);
		// newAngle.y = Mathf.MoveTowardsAngle (current.y, target.y, delta);
		// newAngle.z = Mathf.MoveTowardsAngle (current.z, target.z, delta);
		// return newAngle;
		return new Vector3 (
			Mathf.MoveTowardsAngle (current.x, target.x, delta),
			Mathf.MoveTowardsAngle (current.y, target.y, delta),
			Mathf.MoveTowardsAngle (current.z, target.z, delta)
		);
	}
	/// <summary>More accurate but slightly slower angle calculation</summary>
	// public static float Angle2 (float3 from, float3 to)
	// {
	// 	// math
	// 	var abm = from*to.magnitude;
	// 	var bam = to*from.magnitude;
	// 	return 2 * Mathf.Atan2((abm-bam).magnitude, (abm+bam).magnitude) * Mathf.Rad2Deg;
	// }
}