using UnityEngine;

public static class FzPhysics {
    static Color debugHitCol = new Color (0,1,0,1);
    static Color debugMissCol = new Color (1,0,0,1);

    public static bool Raycast (Vector3 position, Vector3 direction, float distance, int collisionLayer, float debugDur = 0) {
        return Raycast (position, direction, out RaycastHit info, distance, collisionLayer, debugHitCol, debugMissCol, debugDur);
    }
    public static bool Raycast (Vector3 position, Vector3 direction, float distance, int collisionLayer, Color hitCol, Color missCol, float debugDur = 0) {
        return Raycast (position, direction, out RaycastHit info, distance, collisionLayer, hitCol, missCol, debugDur);
    }
    public static bool Raycast (Vector3 position, Vector3 direction, out RaycastHit info, float distance, int collisionLayer, float debugDur = 0) {
        return Raycast (position, direction, out info, distance, collisionLayer, debugHitCol, debugMissCol, debugDur);
    }
    public static bool Raycast (Vector3 position, Vector3 direction, out RaycastHit info, float distance, int collisionLayer, Color hitCol, Color missCol, float debugDur = 0) {
        bool hit = Physics.Raycast (position, direction, out info, distance, collisionLayer);
        if (hit)
            Debug.DrawRay (position, direction * info.distance, hitCol, debugDur);
        else
            Debug.DrawRay (position, direction * distance, missCol, debugDur);
        return hit;
    }
}