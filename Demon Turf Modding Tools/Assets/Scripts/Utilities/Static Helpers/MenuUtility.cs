﻿using System;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public static class MenuUtility
{

	public static string FormatTimerSimple (float time)
	{
		TimeSpan span = TimeSpan.FromSeconds (Mathf.Max (time, 0));
		return string.Format ("{0:00}:{1:00}", System.Math.Floor (span.TotalMinutes), span.Seconds);
	}
	public static string FormatTimerDynamic (float time) {
		TimeSpan span = TimeSpan.FromSeconds (Mathf.Max (time, 0));
		return (span.TotalMinutes > 59) ? string.Format ("{0:00}:{1:00}:{2:00}.{3:00}", System.Math.Floor (span.TotalHours), span.Minutes, span.Seconds, span.Milliseconds/10) 
				: (span.TotalSeconds > 59) ? string.Format ("{0:00}:{1:00}.{2:00}", span.Minutes, span.Seconds, span.Milliseconds/10)
					: string.Format ("{0:00}.{1:00}", span.Seconds, span.Milliseconds/10); 
	}
    public static string FormatTimer (float time) {
		TimeSpan span = TimeSpan.FromSeconds (Mathf.Max (time, 0));
		return (span.TotalMinutes > 59) 
			? string.Format ("{0:00}:{1:00}:{2:00}.{3:00}", System.Math.Floor (span.TotalHours), span.Minutes, span.Seconds, span.Milliseconds/10) 
				: string.Format ("{0:00}:{1:00}.{2:00}", span.Minutes, span.Seconds, span.Milliseconds/10); 
	}
	
	public static string FormatTimerLarge (float time) {
		TimeSpan span = TimeSpan.FromSeconds (Mathf.Max (time, 0));
		return (span.TotalHours > 99) 
			? string.Format ("{0:000}:{1:00}:{2:00}", System.Math.Floor (span.TotalDays), span.Hours, span.Minutes, span.Seconds) 
				: string.Format ("{0:00}:{1:00}:{2:00}", System.Math.Floor (span.TotalHours), span.Minutes, span.Seconds); 
	}

	public static void FormatTimer (this TextMeshProUGUI text, float time) {
		TimeSpan span = TimeSpan.FromSeconds (Mathf.Max (time, 0));
		if (span.TotalMinutes > 59)
			text.SetText ("{0:00}:{1:00}:{2:00}", (int)System.Math.Floor (span.TotalHours), span.Minutes, span.Seconds);
		else 
			text.SetText ("{0:00}:{1:00}.{2:00}", span.Minutes, span.Seconds, span.Milliseconds/10);
	}

	public static void SetNavigation (this Selectable select) 
	{
		var nav = select.navigation;
		nav.mode = Navigation.Mode.Automatic;
		select.navigation = nav;
		nav.mode = Navigation.Mode.Explicit;
		nav.selectOnLeft = select.FindSelectableOnLeft ();
		nav.selectOnRight = select.FindSelectableOnRight ();
		nav.selectOnUp = select.FindSelectableOnUp ();
		nav.selectOnDown = select.FindSelectableOnDown ();
		select.navigation = nav;
	}
}
