using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class SceneUtility
{
    public static T[] FindAllSceneObjectsIncludingDisabled<T>()
	{
		var ActiveScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene ();
		var RootObjects = ActiveScene.GetRootGameObjects ();
		var MatchObjects = new List<T> ();

		foreach (var ro in RootObjects) {
			var Matches = ro.GetComponentsInChildren<T> (true);
			MatchObjects.AddRange (Matches);
		}

		return MatchObjects.ToArray ();
	}

	public static T[] FindAllSceneComponents<T> (this Scene scene, bool includeDisable = true)
	{
		var RootObjects = scene.GetRootGameObjects ();
		var MatchObjects = new List<T> ();

		foreach (var ro in RootObjects) {
			var Matches = ro.GetComponentsInChildren<T> (includeDisable);
			MatchObjects.AddRange (Matches);
		}

		return MatchObjects.ToArray ();
	}

	public static T FindSceneComponent<T> (this Scene scene, bool includeDisable = true) where T : class
	{
		var RootObjects = scene.GetRootGameObjects ();
		foreach (var ro in RootObjects) 
		{
			var Matches = ro.GetComponentsInChildren<T> (includeDisable);
			if (Matches != null && Matches.Length > 0)
				return Matches[0];
		}
		return null;
	}


	public static GameObject[] GetAllGameObjectsInScene (this Scene scene)
	{
		var RootObjects = scene.GetRootGameObjects ();
		var MatchObjects = new List<GameObject> ();

		foreach (var ro in RootObjects) {
			var Matches = ro.GetComponentsInChildren<Transform> (true);
			foreach (var match in Matches)
			{
				MatchObjects.Add (match.gameObject);
			}
		}
		return MatchObjects.ToArray ();
	}

	public static IPlayerController FindPlayerInterface (this Scene scene) => scene.FindSceneComponent<IPlayerController> ();
}