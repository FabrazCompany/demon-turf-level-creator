using UnityEngine;

public static class TransformUtility 
{
    static public Vector3 GetCirclePoint (Orientation orientation, Vector3 center , float radius, float ang) 
	{
		Vector3 pos = center;
		var angle1 = radius * Mathf.Sin(ang * Mathf.Deg2Rad);
		var angle2 = radius * Mathf.Cos(ang * Mathf.Deg2Rad);
		switch (orientation)
		{
			default:
			case Orientation.x:
				pos.y += angle1;
				pos.z += angle2;
				break;
			case Orientation.y:
				pos.x += angle2;
				pos.z += angle1;
				break;
			case Orientation.z:
				pos.x += angle1;
				pos.y += angle2;
				break;
		}
        return pos;
    }

	public static readonly Vector3 axisLeft = new Vector3 (-1, 0, 0);

    public static void ArrangeObjects (Transform root, float radius, Orientation orientation, bool invertFacing, bool ignoreInactiveObjects, float spacingAngle, Vector3 upwardsAnglingAxis, float upwardsAngling = 0) => ArrangeObjects (root, radius, spacingAngle, orientation, invertFacing, false, ignoreInactiveObjects, upwardsAnglingAxis, upwardsAngling);
    public static void ArrangeObjects (Transform root, float radius, Orientation orientation, bool invertFacing, bool ignoreInactiveObjects, Vector3 upwardsAnglingAxis, float upwardsAngling = 0) => ArrangeObjects (root, radius, 0, orientation, invertFacing, true, ignoreInactiveObjects, upwardsAnglingAxis, upwardsAngling);
    static void ArrangeObjects (Transform root, float radius, float spacingAngle, Orientation orientation, bool invertFacing, bool spaceEvenly, bool ignoreInactiveObjects, Vector3 upwardsAnglingAxis, float upwardsAngling = 0) 
	{
		if (radius < 0)
			radius = 0;
		
		float count = root.childCount;
		
		if (ignoreInactiveObjects && count > 0)
		{
			foreach (Transform child in root)
			{
				if (!child.gameObject.activeInHierarchy)
					count--;
			}
		}

		if (count == 0)
			return;
		
		var increment = 360f / count;
		var angle = 0f;
		if (!spaceEvenly)
		{
			increment = spacingAngle;
			angle = 90 - (increment * count * 0.5f) + (increment * 0.5f);
		}
		foreach (Transform child in root)
		{
			if (!child.gameObject.activeInHierarchy)
				continue;

			var pos = TransformUtility.GetCirclePoint (orientation, Vector3.zero, radius, angle);
			child.localPosition = pos;
			
			var dir = -pos.normalized;
			if (invertFacing)
				dir *= -1;

			// if (upwardsAngling != 0)
			// 	dir.Rotate (upwardsAngling, Vector3.right);
				
			child.localRotation = Quaternion.LookRotation (dir, root.up);
			if (upwardsAngling != 0)
				child.Rotate (upwardsAnglingAxis * upwardsAngling, Space.Self);

			angle += increment;
		}
	}
}