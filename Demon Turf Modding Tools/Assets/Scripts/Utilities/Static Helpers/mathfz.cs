using Unity.Mathematics;

public static class mathfz 
{
	public static float3 toEuler(this quaternion quaternion) {
		float4 q = quaternion.value;
		double3 res;

		double sinr_cosp = +2.0 * (q.w * q.x + q.y * q.z);
		double cosr_cosp = +1.0 - 2.0 * (q.x * q.x + q.y * q.y);
		res.x = math.atan2(sinr_cosp, cosr_cosp);

		double sinp = +2.0 * (q.w * q.y - q.z * q.x);
		if (math.abs(sinp) >= 1) {
			res.y = math.PI / 2 * math.sign(sinp);
		} else {
			res.y = math.asin(sinp);
		}

		double siny_cosp = +2.0 * (q.w * q.z + q.x * q.y);
		double cosy_cosp = +1.0 - 2.0 * (q.y * q.y + q.z * q.z);
		res.z = math.atan2(siny_cosp, cosy_cosp);

		return (float3) res;
	}
	public static float dot(quaternion a, quaternion b)
	{
		return a.value.x * b.value.x + a.value.y * b.value.y + a.value.z * b.value.z + a.value.w * b.value.w;
	}

	public static float angle (float3 from, float3 to)
	{
		return math.degrees(math.acos(math.dot(math.normalize(from), math.normalize(to))));
	}
	public static float angle(quaternion a, quaternion b)
	{
		float f = mathfz.dot(a, b);
		return math.degrees (math.acos (math.min (math.abs (f), 1f)) * 2f);
	}

	public static float3 moveTowards(float3 current, float3 target, float maxDistanceDelta)
	{
		float deltaX = target.x - current.x;
		float deltaY = target.y - current.y;
		float deltaZ = target.z - current.z;
		float sqdist = deltaX * deltaX + deltaY * deltaY  + deltaZ * deltaZ ;
		if (sqdist == 0 || sqdist <= maxDistanceDelta * maxDistanceDelta)
			return target;
		var dist = (float)math.sqrt(sqdist);
		return new float3(current.x + deltaX / dist * maxDistanceDelta,
							current.y + deltaY / dist * maxDistanceDelta,
							current.z + deltaZ / dist * maxDistanceDelta);
	}

	public static quaternion rotateTowards(quaternion from, quaternion to, float maxDegreesDelta)
	{
		float num = mathfz.angle(from, to);
		if (num == 0f) return to;
		float t = math.min(1f, maxDegreesDelta / num);
		return math.slerp (from, to, t);
		// return mathfz.SlerpUnclamped(from, to, t);
	}
} 