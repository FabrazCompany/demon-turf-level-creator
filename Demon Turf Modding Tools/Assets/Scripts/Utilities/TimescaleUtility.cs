﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimescaleUtility : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0f;
    }

    public void ResetTime () {
        Time.timeScale = Game.defaultTime;
    }
}
