using System.Collections;
using UnityEngine;

public class AdvancedPulseColor : MonoBehaviour
{
    [SerializeField] Color startColor;
    [SerializeField] Color activeColor;
    [SerializeField] float pulseRateStart = 2;
    [SerializeField] float pulseRateEnd = 20f; 
    [SerializeField] EventUtility_MaterialControl[] blinkMaterials;
	[SerializeField] AudioClip BlinkBeepClip;
	[SerializeField] bool usePositiveSin = true;

	void OnEnable ()
	{
		for (int i = 0; i < blinkMaterials.Length; i++)
		{
			blinkMaterials[i].SetPropertyColor (startColor);
		}
	}
    float pulsePhase;
    float pulseRateCalculated;
	float lastPulse;
	bool rising;
    public void Trigger (float duration) => this.RestartCoroutine (TriggerRoutine (duration), ref triggerRoutine);
    Coroutine triggerRoutine;
    IEnumerator TriggerRoutine (float duration)
    {
		lastPulse = 0;
		rising = true;
        float timer = -Time.deltaTime;
        while (timer < duration)
        {
            timer += Time.deltaTime;
            var frequency = Mathf.Lerp (pulseRateStart, pulseRateEnd, timer / duration);
            CalculateNewPhase (timer, frequency);
			var adjustedPulse = timer * frequency + pulsePhase;
			var colorPCT = usePositiveSin ? FzMath.SinPositive (adjustedPulse) : Mathf.Sin (adjustedPulse);
			
			for (int i = 0; i < blinkMaterials.Length; i++)
			{
				if (blinkMaterials[i] == null) yield break;
				blinkMaterials[i].SetPropertyColor (Color.Lerp (startColor, activeColor, colorPCT));
			}

			if (adjustedPulse < lastPulse)
			{
				if (rising)
				{
					rising = false;
					SoundManagerStub.Instance.PlaySoundGameplay3D (BlinkBeepClip, transform.position, data: new AudioData(_volume:.05f, _min:5f, _max:26f));
				}
			}
			else if (!rising) 
				rising = true;

			lastPulse = adjustedPulse;
            yield return null;
        }
    }

	public void Cancel ()
	{
		if (triggerRoutine != null)
			StopCoroutine (triggerRoutine);
		
		for (int i = 0; i < blinkMaterials.Length; i++)
		{
			if (blinkMaterials[i] == null) continue;
			blinkMaterials[i].SetPropertyColor (startColor);
			SoundManagerStub.Instance.PlaySoundGameplay3D (BlinkBeepClip, transform.position, data: new AudioData(_volume:.05f, _min:5f, _max:26f));
		}
	}

    void CalculateNewPhase (float timer, float newFreq)
    {
        float curr = (timer * pulseRateCalculated + pulsePhase) % (2.0f * Mathf.PI);
        float next = (timer * newFreq) % (2.0f * Mathf.PI);
        pulsePhase = curr - next;
        pulseRateCalculated = newFreq;
    }
}