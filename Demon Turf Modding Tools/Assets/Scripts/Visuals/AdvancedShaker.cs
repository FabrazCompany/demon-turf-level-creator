using UnityEngine;
using DG.Tweening;
public class AdvancedShaker : MonoBehaviour 
{
    Tweener shakePosition;
    Vector3 initialPosition;

    [SerializeField] float shakeStrength = 0.15f;
    [SerializeField] AnimationCurve shakeCurve;

    public void Trigger (float duration) 
    {
        shakePosition?.Kill (true);
        shakePosition = DOTween.Shake (
            () => initialPosition, 
            x => transform.localPosition = initialPosition + x * shakeCurve.Evaluate (shakePosition.Elapsed () / shakePosition.Duration ()), 
            duration,
            shakeStrength, 
            fadeOut: false
        );
    }
}