﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateUV : MonoBehaviour {
	[SerializeField] Vector2 animationSpeed;
	[SerializeField] string textureKey = "_DetailAlbedoMap";
	[SerializeField] Renderer rend;
	Vector2 val;

	public void SetScrollSpeedX (float val) => animationSpeed.x = val;
	public void SetScrollSpeedY (float val) => animationSpeed.y = val;
	void OnValidate ()
	{
		if (!rend)
			rend = GetComponent<Renderer> ();
	}
	void Awake () {
		if (!rend)
			rend = GetComponent<Renderer> ();
		val = Vector2.zero;
	}
	
	void LateUpdate () {
		val += (animationSpeed * Time.deltaTime);
		rend.material.SetTextureOffset (textureKey, val);
	}
}
