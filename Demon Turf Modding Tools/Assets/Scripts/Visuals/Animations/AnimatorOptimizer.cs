﻿using UnityEngine;

[RequireComponent (typeof (Animator))]
public class AnimatorOptimizer : FlyweightTransformJobBehavior<AnimatorOptimizer, AnimatorOptimizerData, AnimatorOptimizerJobData, AnimatorOptimizerManager>
{
	[SerializeField] Animator animator;
	[SerializeField] float distance = 50;
	[SerializeField] bool setCullCompletely = true;

	void OnValidate ()
	{
		if (!animator)
			animator = GetComponent<Animator> ();
	}

	protected override AnimatorOptimizerJobData InitializeJobData() => new AnimatorOptimizerJobData () { maxDistance = distance, optimize = false };
	protected override void OnEnable()
	{		
		base.OnEnable(); 
		ref var d = ref data;
		d.animator = animator;
		if (setCullCompletely)
			d.animator.cullingMode = AnimatorCullingMode.CullCompletely;
	}
}