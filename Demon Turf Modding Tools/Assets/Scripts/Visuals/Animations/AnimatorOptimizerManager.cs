﻿using UnityEngine;
using UnityEngine.Jobs;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;

public struct AnimatorOptimizerData 
{	
	public Animator animator;
}

public struct AnimatorOptimizerJobData
{
	public float maxDistance;
	public bool optimize;
}

[BurstCompile(CompileSynchronously = true)]
struct AnimatorOptimizerJob : IJobParallelForTransform
{
	public float3 camPos;
	public NativeArray<AnimatorOptimizerJobData> stateArray;
	public float scalar;

	void IJobParallelForTransform.Execute(int transformIndex, TransformAccess transform)
	{
		var data = stateArray[transformIndex];
		data.optimize = math.distance (camPos, transform.position) >= (data.maxDistance * scalar);
		stateArray[transformIndex] = data;
	}	
}

public class AnimatorOptimizerManager : FlyweightTransformJobManager<AnimatorOptimizer, AnimatorOptimizerData, AnimatorOptimizerJobData, AnimatorOptimizerManager>
{
	Camera cam;
	Transform camTr;
	bool isJobScheduled;
	JobHandle jobHandle;
	NativeArray<AnimatorOptimizerJobData> tempJobArray;
	const float distanceBias = 1;

	protected virtual void Start () 
	{
		CameraSpawnedBroadcast.OnCameraSpawned += SetCamera;
		SetCamera (Camera.main);
	}
	void OnDestroy () 
	{
		CameraSpawnedBroadcast.OnCameraSpawned -= SetCamera;
	}
	void SetCamera (Camera _cam) 
	{
		if (cam == _cam)
			return;

		cam = _cam;
		camTr = cam.transform;
	}
	void Update ()
	{
		if (SafetyDispose ()) return;

		var count = numJobData;
		tempJobArray = new NativeArray<AnimatorOptimizerJobData> (count, Allocator.TempJob, NativeArrayOptions.ClearMemory);
		NativeArray<AnimatorOptimizerJobData>.Copy (dataFields, 0, tempJobArray, 0, count);
		var job = new AnimatorOptimizerJob ()
		{
			camPos = camTr.position,
			scalar = distanceBias,
			stateArray = tempJobArray
		};
		jobHandle = job.Schedule (transforms);
		isJobScheduled = true;
	}
	void LateUpdate ()
	{
		if (!camTr || !isJobScheduled) return;

		jobHandle.Complete ();
		var items = slots.RawItems;
		var length = slots.Count;
		for (int i = 0; i < length; i++)
		{
			ref var slot = ref items[i];
			if (!slot.active || slot.jobIndex >= tempJobArray.Length) continue;
			ref var data = ref slot.data;
			data.animator.enabled = !tempJobArray[slot.jobIndex].optimize;
		}
		tempJobArray.Dispose ();
	}
	bool SafetyDispose ()
	{
		if (camTr) return false;

		if (isJobScheduled)
		{
			isJobScheduled = false;
			jobHandle.Complete ();
		}
		if (tempJobArray.IsCreated)
		{
			tempJobArray.Dispose ();
		}
		return true;
	}
	public override void unregisterAll ()
	{
		if (isJobScheduled)
		{
			jobHandle.Complete ();
			isJobScheduled = false;
		}
		if (tempJobArray.IsCreated)
			tempJobArray.Dispose ();
		base.unregisterAll ();
	}
}