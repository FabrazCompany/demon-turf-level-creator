using UnityEngine;

public class SineBob : MonoBehaviour 
{
	[SerializeField] float speed = 5;
	[SerializeField] float magnitude = 1;
	[SerializeField] float offset;
	[SerializeField] Transform target;
	float progress;

	float cachedY;
	float time;
	[SerializeField] SlowMoEntity slowMo;
	[SerializeField] bool addBaseY;

	void Awake ()
	{
		if (target != null)
		{
			transform.SetParent (null);
		}
	}
	void Start ()
	{
		var tr = target ? target : transform;
		cachedY = tr.localPosition.y;
	}

	void Update ()
	{
		var delta = Time.deltaTime;
		if (slowMo)
			delta *= slowMo.timeScale;
		time += delta;
		var tr = target ? target : transform;
		var pos = tr.localPosition;
		pos.y = Mathf.Sin (offset + (time * speed)) * magnitude;
		if (addBaseY)
			pos.y += cachedY;
		tr.localPosition = pos;
	}

	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.green;
		var pos1 = transform.position;
		pos1.y += magnitude * 0.5f;
		var pos2 = transform.position;
		pos2.y -= magnitude * 0.5f;
		Gizmos.DrawSphere (pos1, 1f);
		Gizmos.DrawSphere (pos2, 1f);
		Gizmos.DrawLine (pos1, pos2);
	}
}