using UnityEngine;

// public class TweenPosition : FlyweightTransformJobBehavior<TweenPosition, TweenPositionData, TweenPositionJobData, TweenPositionManager> 
public class TweenPosition : FlyweightBehavior<TweenPosition, TweenPositionData, TweenPositionManager> 
// public class TweenPosition : MonoBehaviour
{
	[SerializeField] Vector3 point1;
	[SerializeField] Vector3 point2;
	[SerializeField] float duration = 5;
	[SerializeField] float pause;
	[SerializeField] bool loop = true;
	[SerializeField] AnimationCurve curve = new AnimationCurve (new Keyframe (0, 0), new Keyframe (1, 1));

	Vector3 start;
	Vector3 destination;
	float pauseTimer = 0;
	float progressTimer = 0;
	[SerializeField] SlowMoEntity slowMo;

	// protected override TweenPositionJobData InitializeJobData()
	// {
	// 	var data = new TweenPositionJobData ()
	// 	{
	// 		point1 = point1,
	// 		point2 = point2,
	// 		duration = duration,
	// 		pause = pause,
	// 		curve = new NativeCurve (),
	// 		timeScale = slowMo.timeScale,
	// 		finished = false,
	// 	};
	// 	data.curve.Update (curve, 32);
	// 	data.start = data.point1;
	// 	data.destination = data.point2;
	// 	data.pauseTimer = data.progressTimer = 0;
	// 	return data;
	// }




	void Start () 
	{
		start = point1;
		destination = point2;
	}
	void ResetMovement () 
	{
		start = point1;
		destination = point2;
	}

	public void UpdateState () 
	{
		var delta = Time.deltaTime;
		if (slowMo)
			delta *= slowMo.timeScale;
		if (pauseTimer > 0)
		{
			pauseTimer -= delta;
			return;
		}

		progressTimer += delta;
		transform.localPosition = Vector3.LerpUnclamped (start, destination, curve.Evaluate (progressTimer / duration));
		if (progressTimer >= duration)
		{
			if (loop)
			{
				if (start == point1)
				{
					start = point2;
					destination = point1;
				}
				else 
				{
					start = point1;
					destination = point2;
				}
				pauseTimer = pause;
				progressTimer = 0;
			}
			else 
			{
				enabled = false;
			}
		}
	}

	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.green;
		var pos1 = transform.TransformPoint (Application.isPlaying ? start : point1);
		var pos2 = transform.TransformPoint (Application.isPlaying ? destination : point2);
		Gizmos.DrawSphere (pos1, 1f);
		Gizmos.DrawSphere (pos2, 1f);
		Gizmos.DrawLine (pos1, pos2);
	}

}