using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Jobs;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;


public struct TweenPositionData 
{	
	// public Light light;
}
public struct TweenPositionJobData 
{
	// public bool optimize;
	// public float3 prevWaveOffset;
	// public float3 curWaveOffset;
	// public float motionDamping;
	
	public float3 point1;
	public float3 point2;
	public float duration;
	public float pause;
	public bool loop; 
	public NativeCurve curve;
	public float timeScale;
	public bool finished;

	public float3 start;
	public float3 destination;
	public float pauseTimer;
	public float progressTimer;

	public void Update (float deltaTime, ref TransformAccess transform)
	{
		float delta = deltaTime * timeScale;

		
		if (pauseTimer > 0)
		{
			pauseTimer -= delta;
		}
		else 
		{
			progressTimer += delta;
			// math.lerp
			var progress = progressTimer / duration;
			transform.localPosition = math.lerp (start, destination, curve.Evaluate (progress));
			if (progressTimer >= duration)
			{
				if (loop)
				{
					if (progress >= 1)
					{
						if (start.Equals (point1))
						{
							start = point1;
							destination = point2;
						}
						else 
						{
							start = point2;
							destination = point1;
						}
					}
					pauseTimer = pause;
					progressTimer = 0;
				}
				else 
				{
					finished = true;
				// 	enabled = false;
				}
			}
		}
	}
}

[BurstCompile(CompileSynchronously = false)]
struct TweenPositionJob : IJobParallelForTransform
{
	// public float3 camPos;
	// public float maxDistance;
	
	// [WriteOnly]
	public float deltaTime;
	public NativeArray<TweenPositionJobData> stateArray;

	void IJobParallelForTransform.Execute(int transformIndex, TransformAccess transform)
	{
		var entry = stateArray[transformIndex];
		float delta = deltaTime * entry.timeScale;
		entry.Update (delta, ref transform);
		stateArray[transformIndex] = entry;
	}	
}

// public class TweenPositionManager : FlyweightTransformJobManager<TweenPosition, TweenPositionData, TweenPositionJobData, TweenPositionManager>
public class TweenPositionManager : FlyweightManager<TweenPosition, TweenPositionData, TweenPositionManager>
{
	// Camera cam;
	// Transform camTr;
	// bool isJobScheduled;
	// JobHandle jobHandle;
	// NativeArray<TweenPositionJobData> tempJobArray;
	// Queue<TweenPosition> timeScaleRefreshes;


	// const float optimizeDistance = 75f;
	// protected void Awake ()
	// {
	// 	timeScaleRefreshes = new Queue<TweenPosition> (slots.Capacity);
	// }
	// protected virtual void Start () 
	// {
	// 	CameraSpawnedBroadcast.OnCameraSpawned += SetCamera;
	// 	SetCamera (Camera.main);
	// }
	// void OnDestroy () 
	// {
	// 	CameraSpawnedBroadcast.OnCameraSpawned -= SetCamera;
	// }
	// void SetCamera (Camera _cam) 
	// {
	// 	if (cam == _cam)
	// 		return;

	// 	cam = _cam;
	// 	camTr = cam.transform;
	// }
	// void Update ()
	// {
	// 	if (SafetyDispose ()) return;
	// 	var count = numJobData;
	// 	tempJobArray = new NativeArray<TweenPositionJobData> (count, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
	// 	NativeArray<TweenPositionJobData>.Copy (dataFields, 0, tempJobArray, 0, count);
	// 	var job = new TweenPositionJob ()
	// 	{
	// 		deltaTime = Time.deltaTime,
	// 		stateArray = tempJobArray,
	// 	};
	// 	jobHandle = job.Schedule (transforms);
	// 	isJobScheduled = true;
	// }
	// void LateUpdate ()
	// {
	// 	if (!camTr || !isJobScheduled) return;
			
	// 	jobHandle.Complete ();
	// 	// datafi
	// 	var items = slots.RawItems;
	// 	var length = slots.Count;
	// 	for (int i = 0; i < length; i++)
	// 	{
	// 		ref var slot = ref items[i];
	// 		if (!slot.active || slot.jobIndex >= tempJobArray.Length) continue;
			
	// 		ref var data = ref slot.data;
	// 		// data.light.enabled = !tempJobArray[slot.jobIndex].optimize;
	// 	}
	// 	tempJobArray.Dispose ();
	// 	isJobScheduled = false;
	// }
	// bool SafetyDispose ()
	// {
	// 	if (camTr) return false;

	// 	if (isJobScheduled)
	// 	{
	// 		isJobScheduled = false;
	// 		jobHandle.Complete ();
	// 	}
	// 	if (tempJobArray.IsCreated)
	// 	{
	// 		tempJobArray.Dispose ();
	// 	}
	// 	return true;
	// }
	void Update ()
	{
		var items = slots.RawItems;
		var length = slots.Count;
		for (int i = 0; i < length; i++)
		{
			ref var slot = ref items[i];
			if (!slot.active) continue;
			
			slot.flyweight.UpdateState ();
			// ref var data = ref slot.data;

			// data.light.enabled = !tempJobArray[slot.jobIndex].optimize;
		}

	}

	// public override void unregisterAll ()
	// {
	// 	if (isJobScheduled)
	// 	{
	// 		jobHandle.Complete ();
	// 		isJobScheduled = false;
	// 	}
	// 	if (tempJobArray.IsCreated)
	// 		tempJobArray.Dispose ();
	// 	base.unregisterAll ();
	// }
}