﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TweenScale : MonoBehaviour
{
    [SerializeField] Vector3 targetScale = Vector3.one;
    [SerializeField] float duration = 0.5f;

    public void TriggerScale () 
    {
        transform.DOScale (targetScale, duration);
    }
}
