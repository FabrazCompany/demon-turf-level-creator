using UnityEngine;
using UnityEngine.Jobs;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;

public struct BillboardData 
{	
	public bool hasAnim;
	public Animator anim;
	public Transform facingAnchor;
	public bool clampAngle;
	public int paramIdX;
	public int paramIdZ;
}
public struct BillboardJobData 
{
	public float face;
	public float3 axis;
	public float offset;
	public bool keepPreviousRotation;
	public float orientationStrength;	
}

[BurstCompile(CompileSynchronously = true)]
public struct BillboardJob : IJobParallelForTransform
{
	public quaternion camRot;
	public float3 camPos;
	public float delta;

	[ReadOnly]
	[DeallocateOnJobCompletion]
	public NativeArray<BillboardJobData> dataArray;

	void IJobParallelForTransform.Execute(int transformIndex, TransformAccess transform)
	{
		var data = dataArray[transformIndex];
		
		var startingRot = data.keepPreviousRotation ? (quaternion)transform.rotation : quaternion.identity;
		
		var orientation = math.mul (camRot, data.axis);
		var vector = ((float3)transform.position - camPos) * data.face;
		
		var targetRot = quaternion.LookRotation (vector, orientation);
		targetRot = math.mul (targetRot, quaternion.RotateY (math.radians (data.offset)));		
		targetRot.value.x = startingRot.value.x;
		targetRot.value.z = startingRot.value.z;
		
		transform.rotation = targetRot;
	}	
}


public class BillboardManager : FlyweightTransformJobManager<SimpleBillboard, BillboardData, BillboardJobData, BillboardManager>
{
	
	Camera cam;
	Transform camTr;
	bool isJobScheduled;
	JobHandle jobHandle;
	
	const float angleThreshold = 0.8f;

	void Start () 
	{
		CameraSpawnedBroadcast.OnCameraSpawned += SetCamera;
		SetCamera (Camera.main);
	}
	void OnDestroy () 
	{
		CameraSpawnedBroadcast.OnCameraSpawned -= SetCamera;
	}

	void SetCamera (Camera _cam) 
	{
		if (cam == _cam)
			return;
		cam = _cam;
		camTr = cam.transform;
	}
	void Update ()
	{
		if (SafetyDispose ()) return;
		
		var count = numJobData;
		BillboardJob job = new BillboardJob ()
		{
			camRot = camTr.rotation,
			camPos = camTr.position,
			delta = Time.deltaTime,
			dataArray = new NativeArray<BillboardJobData> (count, Allocator.TempJob, NativeArrayOptions.UninitializedMemory),	
		};
		NativeArray<BillboardJobData>.Copy (dataFields, 0, job.dataArray, 0, count);
		jobHandle = job.Schedule (transforms);
		isJobScheduled = true;
	}
	void LateUpdate ()
	{
		if (!camTr || !isJobScheduled) return;

		jobHandle.Complete ();
		var items = slots.RawItems;
		var length = slots.Count;
		for (int i = 0; i < length; i++)
		{
			ref var slot = ref items[i];
			if (!slot.active || !slot.visible) continue;

			ref var data = ref slot.data;
			if (!data.hasAnim) continue;

			var facing = (Vector3)camTr.InverseTransformDirection (data.facingAnchor.forward);
			if (data.clampAngle)
			{
				facing.y = 0;
				facing.Normalize ();
				if (Mathf.Abs (facing.z) > angleThreshold)
					facing = new Vector3 (0, 0, Mathf.Sign (facing.z));
				else if (Mathf.Abs (facing.x) > angleThreshold)
					facing = new Vector3 (Mathf.Sign (facing.x), 0, 0);
				else 
					facing = new Vector3 (Mathf.Sign (facing.x) * 0.5f, 0, Mathf.Sign (facing.z) * 0.5f);
			}
			data.anim.SetFloat (data.paramIdX, facing.x);
			data.anim.SetFloat (data.paramIdZ, facing.z);
		}
		isJobScheduled = false;
	} 

	bool SafetyDispose ()
	{
		if (camTr) return false;
		
		if (isJobScheduled)
		{
			isJobScheduled = false;
			jobHandle.Complete ();
		}
		return true;
	}

	public override void unregisterAll ()
	{
		if (isJobScheduled)
		{
			jobHandle.Complete ();
			isJobScheduled = false;
		}
		base.unregisterAll ();
	}
}