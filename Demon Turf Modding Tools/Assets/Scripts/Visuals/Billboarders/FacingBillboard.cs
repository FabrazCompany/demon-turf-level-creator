using UnityEngine;

public class FacingBillboard : SimpleBillboard {
    [SerializeField] Animator anim;
    [SerializeField] Transform facingAnchor;
	[SerializeField] bool clampAngle;
	[SerializeField] string parameterName = "vel";
	int paramX;
	int paramZ;

	protected override void OnValidate ()
	{
		base.OnValidate ();
		if (!anim)
			anim = GetComponentInParent<Animator> ();
	}
    protected void Awake () 
    {
		// base.Awake ();
		if (!anim)
	        anim = GetComponentInParent<Animator> ();
		paramX = Animator.StringToHash (parameterName+"X");
		paramZ = Animator.StringToHash (parameterName+"Z");
    }
	protected override void OnEnable()
	{
		base.OnEnable();
		ref var d = ref data;
		d.hasAnim = true;
		d.anim = anim;
		d.paramIdX = paramX;
		d.paramIdZ = paramZ;
		d.clampAngle = clampAngle;
		d.facingAnchor = facingAnchor;
	}
}