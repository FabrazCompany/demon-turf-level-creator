﻿using UnityEngine;
public class SimpleBillboard : FlyweightTransformJobBehavior<SimpleBillboard, BillboardData, BillboardJobData, BillboardManager> {
	[SerializeField] Renderer rend;
	public enum Axis {up, down, left, right, forward, back};
	public bool reverseFace = false; 
	public bool keepPreviousRotation;
	public Axis axis = Axis.up; 
	float offset;
	public Vector3 GetAxis (Axis refAxis)
	{
		switch (refAxis)
		{
			case Axis.down:
				return Vector3.down; 
			case Axis.forward:
				return Vector3.forward; 
			case Axis.back:
				return Vector3.back; 
			case Axis.left:
				return Vector3.left; 
			case Axis.right:
				return Vector3.right; 
		}
		return Vector3.up; 		
	}
	public void SetOffset (float val) 
	{
		if (!isSet) return;
		
		var data = getJobData;
		data.offset = offset = val;
		UpdateJobData (ref data);
	} 
	protected virtual void OnValidate ()
	{
		if (!rend)
			rend = GetComponent<Renderer> ();
	}
	protected override BillboardJobData InitializeJobData()
	{
		return new BillboardJobData ()
		{
			face = reverseFace ? 1 : -1,
			keepPreviousRotation = keepPreviousRotation,
			axis = GetAxis (axis),
			offset = offset,
			orientationStrength = 1
		}; 
	}
	protected override void OnEnable()
	{
		base.OnEnable(); 
		ref var data = ref base.data;
		data.hasAnim = false;

		var jobData = getJobData;
		jobData.offset = offset;
		UpdateJobData (ref jobData);
	}
	protected virtual void Start () {
		if (!rend)
			rend = GetComponent<Renderer> ();
	}
}