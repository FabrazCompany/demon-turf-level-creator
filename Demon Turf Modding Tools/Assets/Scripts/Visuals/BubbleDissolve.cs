using System.Collections;
using UnityEngine;


public class BubbleDissolve : MonoBehaviour 
{
    [SerializeField] float duration;
    [SerializeField] Color startColor;
    [SerializeField] Color endColor;

    Renderer rend;
    Material material;

    void Awake () {
        rend = GetComponent<MeshRenderer> ();
        material = rend.material;
    }
    public void TriggerBubble () {
        // return;
        // Debug.Log ("TRIGGERING");
        // this.RestartCoroutine (Bubble (), ref bubbleRoutine);
    }
    Coroutine bubbleRoutine;
    IEnumerator Bubble () {
        float timer = -Time.deltaTime;
        while (timer < duration) {
            timer += Time.deltaTime;
            material.SetFloat ("_SliceAmount", timer / duration);
            material.SetColor ("_Color", Color.Lerp (startColor, endColor, timer / duration));
            yield return null;
        }
        material.SetFloat ("_SliceAmount", 1);
        material.SetColor ("_Color", endColor);
        // gameObject.SetActive (false);
    }
}