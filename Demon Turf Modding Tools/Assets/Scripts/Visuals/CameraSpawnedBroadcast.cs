using UnityEngine;

[RequireComponent (typeof(Camera))]
public class CameraSpawnedBroadcast : MonoBehaviour 
{
    public static event System.Action<Camera> OnCameraSpawned;
    void OnEnable () {
        OnCameraSpawned?.Invoke (GetComponent<Camera> ());
    }

}