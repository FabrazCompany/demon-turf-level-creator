using UnityEngine;

public class ClothOptimizer : FlyweightTransformJobBehavior<ClothOptimizer, ClothOptimizerData, bool, ClothOptimizerManager>
{
	[SerializeField] Cloth cloth;

	void OnValidate () 
	{
		if (!cloth)
			cloth = GetComponent<Cloth> ();
	}
	
	protected override void OnEnable()
	{
		base.OnEnable(); 

		ref var d = ref data;
		d.cloth = cloth;
		d.enabled = true;
	}

	protected override bool InitializeJobData() => false;
}