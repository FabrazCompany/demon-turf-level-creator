﻿using UnityEngine;
using UnityEngine.Jobs;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;
public struct ClothOptimizerData 
{	
	public Cloth cloth;
	public bool enabled;
}

[BurstCompile(CompileSynchronously = true)]
struct ClothOptimizerJob : IJobParallelForTransform
{
	public float3 camPos;
	public float maxDistance;
	
	[WriteOnly]
	public NativeArray<bool> stateArray;

	void IJobParallelForTransform.Execute(int transformIndex, TransformAccess transform)
	{
		stateArray[transformIndex] = math.distance (camPos, transform.position) < maxDistance;
	}	
}


public class ClothOptimizerManager : FlyweightTransformJobManager<ClothOptimizer, ClothOptimizerData, bool, ClothOptimizerManager>
{
	Camera cam;
	Transform camTr;
	bool isJobScheduled;
	JobHandle jobHandle;
	NativeArray<bool> tempJobArray;

	const float optimizeDistance = 20f;
	const float fadeTime = 0.5f;
	void Start () 
	{
		CameraSpawnedBroadcast.OnCameraSpawned += SetCamera;
		SetCamera (Camera.main);
	}
	void OnDestroy () 
	{
		CameraSpawnedBroadcast.OnCameraSpawned -= SetCamera;
	}
	void SetCamera (Camera _cam) 
	{
		if (cam == _cam)
			return;

		cam = _cam;
		camTr = cam.transform;
	}
	void Update ()
	{
		if (SafetyDispose ()) return;

		var count = numJobData;
		tempJobArray = new NativeArray<bool> (count, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
		NativeArray<bool>.Copy (dataFields, 0, tempJobArray, 0, count);
		var job = new ClothOptimizerJob ()
		{
			camPos = camTr.position,
			maxDistance = optimizeDistance,
			stateArray = tempJobArray
		};
		jobHandle = job.Schedule (transforms);
		isJobScheduled = true;
	}
	void LateUpdate ()
	{
		if (!camTr || !isJobScheduled) return;
		
		jobHandle.Complete ();
		var items = slots.RawItems;
		var length = slots.Count;
		for (int i = 0; i < length; i++)
		{
			ref var slot = ref items[i];
			if (!slot.active || slot.jobIndex >= tempJobArray.Length) continue;

			ref var data = ref slot.data;
			// var state = tempJobArray[slot.jobIndex] || slot.visible;
			var state = tempJobArray[slot.jobIndex];
			if (state != data.enabled)
			{
				data.cloth.SetEnabledFading (state, fadeTime);
				data.enabled = state;
			}
		}
		tempJobArray.Dispose ();
		isJobScheduled = false;
	}
	bool SafetyDispose ()
	{
		if (camTr) return false;

		if (isJobScheduled)
		{
			isJobScheduled = false;
			jobHandle.Complete ();
		}
		if (tempJobArray.IsCreated)
		{
			tempJobArray.Dispose ();
		}
		return true;
	}
	public override void unregisterAll ()
	{
		if (isJobScheduled)
		{
			jobHandle.Complete ();
			isJobScheduled = false;
		}
		if (tempJobArray.IsCreated)
			tempJobArray.Dispose ();
		base.unregisterAll ();
	}
}