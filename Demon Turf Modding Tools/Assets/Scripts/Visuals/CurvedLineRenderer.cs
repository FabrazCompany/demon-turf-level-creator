﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurvedLineRenderer : MonoBehaviour {

    [SerializeField] LineRenderer lineRender;
    [SerializeField] Transform start;
    [SerializeField] Transform middle;
    [SerializeField] Transform end;
    [SerializeField] float smoothing = 1;
	[Space]
	[SerializeField, Range (0,1)] float ratioShown = 1;
	[SerializeField] bool ratioReverse;

	public LineRenderer getLineRender => lineRender;
	public void SetEnd (Transform _end) 
	{
		end = _end;
		RefreshPoints ();
	}
	public bool hasEnd => end != null;
	Transform[] transformPoints;
	Vector3[] pointsRaw;
	Vector3[] pointsCurved;
	Vector3[] processingArray;
	public void SetRatioShown (float val) => ratioShown = Mathf.Clamp01 (val);

    // Use this for initialization
    
	void OnValidate () 
	{
		if (!start || !middle || !end)
			return;

		transformPoints = ratioReverse ? new Transform[] { start, middle, end } : new Transform[] { end, middle, start };
		pointsRaw = new Vector3[transformPoints.Length];
		for (int i = 0; i < transformPoints.Length; i++)
			pointsRaw[i] = transformPoints[i].position;

		processingArray = new Vector3[transformPoints.Length];
		var length = (pointsRaw.Length*Mathf.RoundToInt(smoothing));
		pointsCurved = new Vector3[length];

		FzMath.MakeSmoothCurve (ref pointsCurved, pointsRaw, processingArray);
		lineRender.positionCount = length;
		lineRender.SetPositions (pointsCurved);
	}

	void Start () {
        lineRender.sortingOrder = 0;
		Init ();
    }
	bool initd;
	public void Init ()
	{
		if (initd) return;
		initd = true;
		RefreshPoints ();
	}

	void RefreshPoints ()
	{
		transformPoints = ratioReverse ? new Transform[] { start, middle, end } : new Transform[] { end, middle, start };
		pointsRaw = new Vector3[transformPoints.Length];
		processingArray = new Vector3[transformPoints.Length];
		var length = (pointsRaw.Length*Mathf.RoundToInt(smoothing));
		pointsCurved = new Vector3[length];
	}


	[SerializeField] bool track;
    void LateUpdate () => SetCurve ();
	public void SetCurve ()
	{
		if (!start || !middle || !end) return;
		if (pointsRaw == null || transformPoints == null) return;
		
		for (int i = 0; i < transformPoints.Length; i++)
			pointsRaw[i] = transformPoints[i].position;
		
		FzMath.MakeSmoothCurve (ref pointsCurved, pointsRaw, processingArray);
		var revisedCount = (int)((float)pointsCurved.Length * ratioShown);
		lineRender.positionCount = revisedCount;
		lineRender.SetPositions (pointsCurved);
	}
}