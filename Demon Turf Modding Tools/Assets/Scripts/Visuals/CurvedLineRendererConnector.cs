using UnityEngine;

public class CurvedLineRendererConnector : MonoBehaviour 
{
	[SerializeField] CurvedLineRenderer rend;

	void OnValidate ()
	{
		if (rend == null && transform.parent)
			rend = transform.parent.GetComponent<CurvedLineRenderer> ();
	}

	void LateUpdate ()
	{
		if (!rend.gameObject.activeInHierarchy)
			rend.SetCurve ();
	}

	public void SetLine (CurvedLineRenderer _rend) => rend = _rend;
}