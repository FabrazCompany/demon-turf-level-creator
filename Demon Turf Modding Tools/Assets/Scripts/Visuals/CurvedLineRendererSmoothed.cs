using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurvedLineRendererSmoothed : MonoBehaviour {

    [SerializeField] LineRenderer lineRender;
	[SerializeField] Transform[] transformPoints;
    [SerializeField] float smoothing = 1;

    // Use this for initialization
    
	Vector3[] pointsRaw;
	Vector3[] pointsCurved;
	Vector3[] processingArray;

	void OnValidate () 
	{
		if (transformPoints == null || transformPoints.Length == 0)
			return;
		
		pointsRaw = new Vector3[transformPoints.Length];
		for (int i = 0; i < transformPoints.Length; i++)
			pointsRaw[i] = transformPoints[i].position;

		processingArray = new Vector3[transformPoints.Length];
		var length = (pointsRaw.Length * Mathf.RoundToInt(smoothing));
		pointsCurved = new Vector3[length];

		FzMath.MakeSmoothCurve (ref pointsCurved, pointsRaw, processingArray);
		lineRender.positionCount = length;
		lineRender.SetPositions (pointsCurved);
	}

	void Start () {
        lineRender.sortingOrder = 0;
		pointsRaw = new Vector3[transformPoints.Length];
		processingArray = new Vector3[transformPoints.Length];
		var length = (pointsRaw.Length * Mathf.RoundToInt(smoothing));
		pointsCurved = new Vector3[length];

    }


    void LateUpdate () 
	{
		for (int i = 0; i < transformPoints.Length; i++)
			pointsRaw[i] = transformPoints[i].position;
		FzMath.MakeSmoothCurve (ref pointsCurved, pointsRaw, processingArray);
		lineRender.positionCount = pointsCurved.Length;
		lineRender.SetPositions (pointsCurved);
    }
}