﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class EventUtility_Tweener : MonoBehaviour
{
    [SerializeField] float duration;
    public void PunchRotationX (float strength) => transform.DOPunchRotation (Vector3.right * strength, duration);
    public void PunchRotationY (float strength) => transform.DOPunchRotation (Vector3.up * strength, duration);
    public void PunchRotationZ (float strength) => transform.DOPunchRotation (Vector3.forward * strength, duration);
	public void PunchRotation (float strength) => transform.DOPunchRotation (Random.insideUnitSphere * strength, duration);
	

	public void PunchScaleX (float strength) => transform.DOPunchScale (Vector3.right * strength, duration);
    public void PunchScaleY (float strength) => transform.DOPunchScale (Vector3.up * strength, duration);
    public void PunchScaleZ (float strength) => transform.DOPunchScale (Vector3.forward * strength, duration);
	public void PunchScale (float strength) => transform.DOPunchScale (Random.insideUnitSphere * strength, duration);
}
