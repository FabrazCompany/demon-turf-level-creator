﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserReflectBubble : MonoBehaviour
{
    public void Cleanup ()
    {
		StartCoroutine (ScaleDown ());
    }
    float scaleSpeed = 5;
    IEnumerator ScaleDown () 
    {
        while (transform.localScale != Vector3.zero)
        {
            transform.localScale = Vector3.MoveTowards (transform.localScale, Vector3.zero, scaleSpeed * Time.deltaTime);
            yield return null;
        }
        TrashMan.despawn (gameObject);
    }
}
