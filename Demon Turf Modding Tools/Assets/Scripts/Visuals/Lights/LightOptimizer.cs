using UnityEngine;

[RequireComponent (typeof (Light))]
public class LightOptimizer : FlyweightTransformJobBehavior<LightOptimizer, LightOptimizerData, LightOptimizerJobData, LightOptimizerManager>
{
	[SerializeField] new Light light;
	[SerializeField] ModStub_HxVolumetricLight volumetricLight;

	LightShadows lightShadows;

	void OnValidate ()
	{
		light = GetComponent<Light> ();
		volumetricLight = GetComponent<ModStub_HxVolumetricLight> ();
	}

	protected override LightOptimizerJobData InitializeJobData() => new LightOptimizerJobData () { optimize = false };

	protected override void OnEnable()
	{
		if (light.type == LightType.Directional) return;
			
		base.OnEnable(); 
		ref var d = ref data;
		d.light = light;
	}

	void Start ()
	{
		lightShadows = light.shadows;
		light.cullingMask = LayerMask.GetMask ("Default", "Platform", "Physical", "Water", "TransparentFX", "Player", "Enemy"); 
#if !DT_EXPORT && !DT_MOD
		OnSettingAdjusted (SettingIDBool.LightOptimizer, GameDataManager.Instance.Data.GetSetting (SettingIDBool.LightOptimizer));
		GameDataManager.Instance.Data.onSettingAdjustedBool += OnSettingAdjusted;
#endif
	}
	protected override void OnDisable()
	{
		if (light.type == LightType.Directional) return;
			
		base.OnDisable(); 
	}

	void OnDestroy ()
	{
#if !DT_EXPORT && !DT_MOD
		if (GameDataManager.IsInitialized)
			GameDataManager.Instance.Data.onSettingAdjustedBool -= OnSettingAdjusted;
#endif
	}

	void OnSettingAdjusted (SettingIDBool settingID, bool value)
	{
		if (settingID != SettingIDBool.LightOptimizer) return;
		
		if (!value)
			light.enabled = true;
		if (light.type == LightType.Point || light.type == LightType.Spot)
		{
			light.shadows = value ? LightShadows.None : lightShadows;
			
			if (volumetricLight)
				volumetricLight.enabled = !value;	
		}
		else if (light.type == LightType.Directional)
		{
			light.shadows = value ? LightShadows.Hard : lightShadows;
		}

	}
}