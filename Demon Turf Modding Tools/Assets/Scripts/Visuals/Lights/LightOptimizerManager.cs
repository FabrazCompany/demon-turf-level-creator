using UnityEngine;
using UnityEngine.Jobs;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;

public struct LightOptimizerData 
{	
	public Light light;
}
public struct LightOptimizerJobData 
{
	public bool optimize;
}

[BurstCompile(CompileSynchronously = false)]
struct LightOptimizerJob : IJobParallelForTransform
{
	public float3 camPos;
	public float maxDistance;
	
	[WriteOnly]
	public NativeArray<LightOptimizerJobData> stateArray;

	void IJobParallelForTransform.Execute(int transformIndex, TransformAccess transform)
	{
		stateArray[transformIndex] = new LightOptimizerJobData () { optimize = math.distance (camPos, transform.position) >= maxDistance };
	}	
}

public class LightOptimizerManager : FlyweightTransformJobManager<LightOptimizer, LightOptimizerData, LightOptimizerJobData, LightOptimizerManager>
{
	Camera cam;
	Transform camTr;
	bool isJobScheduled;
	JobHandle jobHandle;
	NativeArray<LightOptimizerJobData> tempJobArray;

	const float optimizeDistance = 75f;
	bool shouldOptimize;


	protected virtual void Start () 
	{
		SetCamera (Camera.main);

		CameraSpawnedBroadcast.OnCameraSpawned += SetCamera;
#if !DT_EXPORT && !DT_MOD
		OnSettingAdjusted (SettingIDBool.LightOptimizer, GameDataManager.Instance.Data.GetSetting (SettingIDBool.LightOptimizer));
		GameDataManager.Instance.Data.onSettingAdjustedBool += OnSettingAdjusted;
#endif
	}
	void OnDestroy () 
	{
		CameraSpawnedBroadcast.OnCameraSpawned -= SetCamera;
#if !DT_EXPORT && !DT_MOD	
		if (GameDataManager.IsInitialized)
			GameDataManager.Instance.Data.onSettingAdjustedBool -= OnSettingAdjusted;
#endif
	}
	void SetCamera (Camera _cam) 
	{
		if (cam == _cam) return;

		cam = _cam;
		camTr = cam.transform;
	}
	
	void OnSettingAdjusted (SettingIDBool settingID, bool value)
	{
		switch (settingID)
		{
			case SettingIDBool.LightOptimizer:
				shouldOptimize = value;
				break;
		}
	}
	
	void Update ()
	{
		if (!shouldOptimize) return;
		if (SafetyDispose ()) return;

		var count = numJobData;
		tempJobArray = new NativeArray<LightOptimizerJobData> (count, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
		NativeArray<LightOptimizerJobData>.Copy (dataFields, 0, tempJobArray, 0, count);
		var job = new LightOptimizerJob ()
		{
			camPos = camTr.position,
			maxDistance = optimizeDistance,
			stateArray = tempJobArray
		};		
		jobHandle = job.Schedule (transforms);
		isJobScheduled = true;
	}
	void LateUpdate ()
	{
		if (!camTr || !isJobScheduled) return;
			
		jobHandle.Complete ();		
		var items = slots.RawItems;
		var length = slots.Count;
		
		if (shouldOptimize)
		{
			for (int i = 0; i < length; i++)
			{
				ref var slot = ref items[i];
				if (!slot.active || slot.jobIndex >= tempJobArray.Length) continue;
				
				ref var data = ref slot.data;
				data.light.enabled = !tempJobArray[slot.jobIndex].optimize;
			}
		}
		else 
		{
			for (int i = 0; i < length; i++)
			{
				ref var slot = ref items[i];
				if (slot.jobIndex >= tempJobArray.Length) continue;
				
				ref var data = ref slot.data;
				data.light.enabled = true;
			}
		}
		
		tempJobArray.Dispose ();
		isJobScheduled = false;
	}
	bool SafetyDispose ()
	{
		if (camTr) return false;

		if (isJobScheduled)
		{
			isJobScheduled = false;
			jobHandle.Complete ();
		}
		if (tempJobArray.IsCreated)
		{
			tempJobArray.Dispose ();
		}
		return true;
	}
	public override void unregisterAll ()
	{
		if (isJobScheduled)
		{
			jobHandle.Complete ();
			isJobScheduled = false;
		}
		if (tempJobArray.IsCreated)
			tempJobArray.Dispose ();
		base.unregisterAll ();
	}
}