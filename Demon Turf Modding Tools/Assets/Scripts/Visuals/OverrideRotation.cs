using UnityEngine;

public class OverrideRotation : MonoBehaviour 
{
	[SerializeField] Transform tr;
	[SerializeField] Vector3 facing;
	
	void OnValidate ()
	{
		if (!tr)
			tr = transform;
	}
	void LateUpdate ()
	{
		// tr.rotat
		tr.eulerAngles = facing;
	}
}