using UnityEngine;

public class ProjectorOptimizer : FlyweightTransformJobBehavior<ProjectorOptimizer, ProjectorOptimizerData, ProjectorOptimizerJobData, ProjectorOptimizerManager>
{
	[SerializeField] Projector projector;
	[SerializeField] ModStub_DeferredProjector deferredProjector;

	void OnValidate ()
	{
		if (!projector)
			projector = GetComponent<Projector> ();
		if (!deferredProjector)
			deferredProjector = GetComponent<ModStub_DeferredProjector> ();
	}

	protected override ProjectorOptimizerJobData InitializeJobData() => new ProjectorOptimizerJobData () { optimize = false };
	protected override void OnEnable()
	{
		base.OnEnable(); 
		ref var d = ref data;
		if (projector)
			d.projector = projector as Behaviour;
		else if (deferredProjector)
			d.projector = deferredProjector as Behaviour;
	}
}