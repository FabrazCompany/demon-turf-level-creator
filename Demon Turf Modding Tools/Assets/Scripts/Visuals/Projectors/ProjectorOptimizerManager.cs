using UnityEngine;
using UnityEngine.Jobs;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;

public struct ProjectorOptimizerData 
{	
	public Behaviour projector;	//set to monobehavior to allow for either projectors or deferred projectors
}
public struct ProjectorOptimizerJobData 
{
	public bool optimize;
}

[BurstCompile(CompileSynchronously = false)]
struct ProjectorOptimizerJob : IJobParallelForTransform
{
	public float3 camPos;
	public float maxDistance;
	
	[WriteOnly]
	public NativeArray<ProjectorOptimizerJobData> stateArray;

	void IJobParallelForTransform.Execute(int transformIndex, TransformAccess transform)
	{
		stateArray[transformIndex] = new ProjectorOptimizerJobData () { optimize = math.distance (camPos, transform.position) >= maxDistance };
	}	
}

public class ProjectorOptimizerManager : FlyweightTransformJobManager<ProjectorOptimizer, ProjectorOptimizerData, ProjectorOptimizerJobData, ProjectorOptimizerManager>
{
	Camera cam;
	Transform camTr;
	bool isJobScheduled;
	JobHandle jobHandle;
	NativeArray<ProjectorOptimizerJobData> tempJobArray;

	const float optimizeDistance = 30f;

	protected virtual void Start () 
	{
		CameraSpawnedBroadcast.OnCameraSpawned += SetCamera;
		SetCamera (Camera.main);
	}
	void OnDestroy () 
	{
		CameraSpawnedBroadcast.OnCameraSpawned -= SetCamera;
	}
	void SetCamera (Camera _cam) 
	{
		if (cam == _cam)
			return;

		cam = _cam;
		camTr = cam.transform;
	}
	void Update ()
	{
		if (SafetyDispose ()) return;

		var count = numJobData;
		tempJobArray = new NativeArray<ProjectorOptimizerJobData> (count, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
		NativeArray<ProjectorOptimizerJobData>.Copy (dataFields, 0, tempJobArray, 0, count);
		var job = new ProjectorOptimizerJob ()
		{
			camPos = camTr.position,
			maxDistance = optimizeDistance,
			stateArray = tempJobArray
		};		
		jobHandle = job.Schedule (transforms);
		isJobScheduled = true;
	}
	void LateUpdate ()
	{
		if (!camTr || !isJobScheduled) return;
			
		jobHandle.Complete ();		
		var items = slots.RawItems;
		var length = slots.Count;
		for (int i = 0; i < length; i++)
		{
			ref var slot = ref items[i];
			if (!slot.active || slot.jobIndex >= tempJobArray.Length) continue;
			
			ref var data = ref slot.data;
			data.projector.enabled = !tempJobArray[slot.jobIndex].optimize;
		}
		tempJobArray.Dispose ();
		isJobScheduled = false;
	}
	bool SafetyDispose ()
	{
		if (camTr) return false;

		if (isJobScheduled)
		{
			isJobScheduled = false;
			jobHandle.Complete ();
		}
		if (tempJobArray.IsCreated)
		{
			tempJobArray.Dispose ();
		}
		return true;
	}
	public override void unregisterAll ()
	{
		if (isJobScheduled)
		{
			jobHandle.Complete ();
			isJobScheduled = false;
		}
		if (tempJobArray.IsCreated)
			tempJobArray.Dispose ();
		base.unregisterAll ();
	}
}