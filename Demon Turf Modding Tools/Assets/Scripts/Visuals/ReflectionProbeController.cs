﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReflectionProbeController : MonoBehaviour
{
    [SerializeField] ReflectionProbe reflectionProbe;

	// static ReflectionProbeController currentRenderingProbeController;

	void Awake ()
	{
		// if (reflectionProbe.IsFinishedRendering)
		#if UNITY_GAMECORE && !UNITY_EDITOR
		if (UnityEngine.GameCore.Hardware.version == UnityEngine.GameCore.HardwareVersion.XboxSeriesS)
			gameObject.SetActive (false);
		#endif 
	}
}
