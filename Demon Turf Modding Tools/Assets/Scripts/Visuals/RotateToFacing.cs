using UnityEngine;

public class RotateToFacing : MonoBehaviour 
{
    // [SerializeField] Transform rotationAnchor;
    [SerializeField] float rotationSpeed = 120;
    // [SerializeField] float rotationOffset = 90;
    float yRotation;
    Vector3 dir;
    
    void Awake () {
        dir = transform.forward;
    }

    void LateUpdate () {
        // var rot = transform.localEulerAngles;
        // rot.y = Mathf.MoveTowardsAngle (rot.y, yRotation + rotationOffset, rotationSpeed * Time.deltaTime);
        // transform.localEulerAngles = rot;
        transform.forward = Vector3.MoveTowards (transform.forward, dir, rotationSpeed * Time.deltaTime);
    }
    public void SetRotation (Vector3 direction) {
        dir = direction;
        // dir = new Vector3 (direction.x, 0, direction.y) * -1f;
        // var curDegree = FzMath.Vector2ToFloat (direction);
        // curDegree *= Mathf.Rad2Deg * -1f;
        // yRotation = curDegree;
    }
}