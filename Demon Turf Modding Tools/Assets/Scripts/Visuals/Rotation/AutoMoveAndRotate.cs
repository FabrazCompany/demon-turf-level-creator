using System;
using UnityEngine;

public class AutoMoveAndRotate : FlyweightTransformJobBehavior<AutoMoveAndRotate, AutoMoveAndRotateData, AutoMoveAndRotateJobData, AutoMoveAndRotateManager>
{
	public Vector3andSpace moveUnitsPerSecond;
	public Vector3andSpace rotateDegreesPerSecond;
	public bool ignoreTimescale;
	float scalar = 1;

	public void SetScalar (float val) 
	{
		scalar = val;
		if (isSet)
		{
			var data = getJobData;
			data.timeScalar = scalar;
			UpdateJobData (ref data);
		}
	}
	public void SetRotateSpeedX (float val) 
	{
		rotateDegreesPerSecond.value.x = val;
		if (isSet)
		{
			var data = getJobData;
			data.rotation.x = val * Mathf.Deg2Rad;
			UpdateJobData (ref data);
		}
	}
	public void SetRotateSpeedY (float val) 
	{
		rotateDegreesPerSecond.value.y = val;
		if (isSet)
		{
			var data = getJobData;
			data.rotation.y = val * Mathf.Deg2Rad;
			UpdateJobData (ref data);
		}		
	}
	public void SetRotateSpeedZ (float val) 
	{
		rotateDegreesPerSecond.value.z = val;
		if (isSet)
		{
			var data = getJobData;
			data.rotation.z = val * Mathf.Deg2Rad;
			UpdateJobData (ref data);
		}
	}

	protected override AutoMoveAndRotateJobData InitializeJobData()
	{
		return new AutoMoveAndRotateJobData ()
		{
			hasMovement = moveUnitsPerSecond.value != Vector3.zero,
			movement = moveUnitsPerSecond.value,
			movementSpaceLocal = moveUnitsPerSecond.space == Space.Self,
			
			hasRotation = rotateDegreesPerSecond.value != Vector3.zero,
			rotation = rotateDegreesPerSecond.value * Mathf.Deg2Rad,
			rotationSpaceLocal = rotateDegreesPerSecond.space == Space.Self,

			timeScalar = scalar,
			ignoreTimescale = ignoreTimescale,
		};
	}

	[Serializable]
	public class Vector3andSpace
	{
		public Vector3 value;
		public Space space = Space.Self;
	}
}