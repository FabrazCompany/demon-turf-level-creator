﻿using UnityEngine;
using UnityEngine.Jobs;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;

public struct AutoMoveAndRotateJobData 
{
	public bool hasMovement;
	public float3 movement;
	public bool movementSpaceLocal;

	public bool hasRotation;
	public float3 rotation;

	public bool rotationSpaceLocal;
	public float timeScalar;
	public bool ignoreTimescale;

	
}
public struct AutoMoveAndRotateData
{
	
}

[BurstCompile(CompileSynchronously = true)]
public struct AutoMoveAndRotateJob : IJobParallelForTransform
{
	public float unscaledDeltaTime;
	public float deltaTime;
	public int executeCount;

	[ReadOnly]
	[DeallocateOnJobCompletion]
	public NativeArray<AutoMoveAndRotateJobData> dataArray;

	void IJobParallelForTransform.Execute(int transformIndex, TransformAccess transform)
	{
		executeCount++;
		var data = dataArray[transformIndex];
		var delta = data.ignoreTimescale ? unscaledDeltaTime : (deltaTime * data.timeScalar);
		if (data.hasMovement)
		{
			if (data.movementSpaceLocal)
				transform.localPosition += (Vector3)(data.movement * delta);
			else 
				transform.position += (Vector3)(data.movement * delta);
		}
		if (data.hasRotation)
		{
			if (data.rotationSpaceLocal)
				transform.localRotation = math.mul (transform.localRotation, quaternion.Euler (data.rotation * delta));
			else 
				transform.rotation = math.mul (transform.rotation, quaternion.Euler (data.rotation * delta));
		}
	}	
}

public class AutoMoveAndRotateManager : FlyweightTransformJobManager<AutoMoveAndRotate, AutoMoveAndRotateData, AutoMoveAndRotateJobData, AutoMoveAndRotateManager>
{
	bool isJobScheduled;
	JobHandle jobHandle;

	void Update ()
	{
		var numJobs = numJobData;
		AutoMoveAndRotateJob job = new AutoMoveAndRotateJob ()
		{
			dataArray = new NativeArray<AutoMoveAndRotateJobData> (numJobs, Allocator.TempJob, NativeArrayOptions.UninitializedMemory),
			deltaTime = Time.deltaTime, 
			unscaledDeltaTime = Time.unscaledDeltaTime
		};
		NativeArray<AutoMoveAndRotateJobData>.Copy (dataFields, 0, job.dataArray, 0, numJobs);
		jobHandle = job.Schedule (transforms);
		isJobScheduled = true;
	}
	void LateUpdate ()
	{
		if (!isJobScheduled)
			return;
		jobHandle.Complete ();
		isJobScheduled = false;
	} 
	public override void unregisterAll ()
	{
		if (isJobScheduled)
		{
			jobHandle.Complete ();
			isJobScheduled = false;
		}
		base.unregisterAll ();
	}
}