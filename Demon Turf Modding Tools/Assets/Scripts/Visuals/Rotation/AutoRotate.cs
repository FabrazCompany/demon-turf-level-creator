using UnityEngine;


public class AutoRotate : MonoBehaviour 
{
    [SerializeField] protected Orientation orientation;
    [SerializeField] protected bool isLocal;
    [SerializeField] protected float speed;
    protected float currentSpeed;
    protected Rigidbody rigid;
	protected SlowMoEntity slowMo;
	protected float getDeltaTime => slowMo ? slowMo.timeScale * Time.deltaTime : Time.deltaTime;
	public void SetSpeed (float speed) => currentSpeed = speed;
    protected virtual void Awake () 
    {
        rigid = GetComponent<Rigidbody> ();
		slowMo = GetComponent<SlowMoEntity> ();
        currentSpeed = speed;
    }
    protected virtual void FixedUpdate () 
    {
        if (rigid)
            rigid.MoveRotation (rigid.rotation * Quaternion.Euler (GetOrientationAxis () * currentSpeed * getDeltaTime));
        else 
            transform.rotation *= Quaternion.Euler (GetOrientationAxis () * currentSpeed * getDeltaTime);
    }

    protected Vector3 GetOrientationAxis ()
    {
        switch (orientation)
        {
            default:
            case Orientation.x:
            return isLocal ? transform.right : Vector3.right;

            case Orientation.y:
            return isLocal ? transform.up : Vector3.up;
            
            case Orientation.z:
            return isLocal ? transform.forward : Vector3.forward;
        }
    }
    protected float GetOrientationAngle () 
    {
        switch (orientation)
        {
            default:
            case Orientation.x:
            return isLocal ? transform.localEulerAngles.x : transform.eulerAngles.x;

            case Orientation.y:
            return isLocal ? transform.localEulerAngles.y : transform.eulerAngles.y;
            
            case Orientation.z:
            return isLocal ? transform.localEulerAngles.z : transform.eulerAngles.z;
        }
    }
}