using UnityEngine;

public class AutoRotateClamped : AutoRotate
{
    [SerializeField, Range (1, 360)] float turnRange;
	[SerializeField] float offset;
    float startAngle;
    // Vector3 startForward;
    Quaternion minAngle;
    Quaternion maxAngle;
	float timer;
    protected override void Awake () 
    {
        base.Awake ();
        // startForward = transform.forward;
        startAngle = GetOrientationAngle ();
    }
	void OnEnable()
	{
		timer = offset;
	}
	

    protected override void FixedUpdate () 
    {
        var angles = isLocal ? transform.localEulerAngles : transform.eulerAngles;
		timer += getDeltaTime;
        var progress = Mathf.Sin (timer * speed);
        switch (orientation) 
        {
            default:
            case Orientation.x:
                angles.x = startAngle + (progress * turnRange * 0.5f);
                break;
            
            case Orientation.y:
                angles.y = startAngle + (progress * turnRange * 0.5f);
                break;

            case Orientation.z:
                angles.z = startAngle + (progress * turnRange * 0.5f);
                break;
        }
        if (rigid)
            rigid.MoveRotation (Quaternion.Euler (angles));
        else
            transform.rotation = Quaternion.Euler (angles);
    }

    // void OnDrawGizmosSelected () 
    // {
    //     var forward = Application.isPlaying ? startForward : transform.forward;
    //     Gizmos.DrawRay (transform.position, Quaternion.AngleAxis (turnRange * 0.5f, GetOrientationAxis ()) * forward * 5);
    //     Gizmos.DrawRay (transform.position, Quaternion.AngleAxis (-turnRange * 0.5f, GetOrientationAxis ()) * forward * 5);
    // }
}