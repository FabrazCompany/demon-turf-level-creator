﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimplePulseColor : MonoBehaviour
{
    [SerializeField] Color startColor;
    [SerializeField] Color activeColor;
    [SerializeField] AnimationCurve pulse;
    [SerializeField] EventUtility_MaterialControl[] materials;

    public void Trigger (float duration) => this.RestartCoroutine (TriggerRoutine (duration), ref triggerRoutine);
    Coroutine triggerRoutine;
    IEnumerator TriggerRoutine (float duration)
    {
        float timer = -Time.deltaTime;
        while (timer < duration)
        {
            timer += Time.deltaTime;
            var colorPCT = pulse.Evaluate (timer / duration);
            foreach (var item in materials)
            {
                item.SetPropertyColor (Color.Lerp (startColor, activeColor, colorPCT));
            }
            yield return null;
        }
    }

    public void SetAll (float value)
    {
		if (triggerRoutine != null)
			StopCoroutine (triggerRoutine);
        foreach (var item in materials)
        {
            item.SetPropertyColor (Color.Lerp (startColor, activeColor, value));
        }
    }
}
