﻿using UnityEngine;
using DG.Tweening;

public class SimpleShaker : MonoBehaviour
{
    enum ShakeType
    {
        Position,
        Rotation,
        Scale,
    }

    [SerializeField, Min (0)] float delay = 0;
    [SerializeField] float duration = 5;
    [SerializeField] bool loop = true;
    [SerializeField] ShakeType shakeType;
    [SerializeField] float magnitude = 1;
    [SerializeField] int vibrato = 10;
    [SerializeField] bool resetPositionOnTrigger;
    Tween shaker;

    Vector3? cachedPosition;

    void Awake () 
    {
        cachedPosition = transform.localPosition;
    }

    public void setMagnitude (float val) => magnitude = val;
    public void setVibrato (int val) => vibrato = val;
    public void Trigger () => Trigger (duration);
    public void Trigger (float _duration) 
    {
        if (resetPositionOnTrigger)
        {
            if (cachedPosition == null)
                cachedPosition = transform.localPosition;
            else 
                transform.localPosition = cachedPosition.Value;
        }
        

        shaker?.Kill (true);
        switch (shakeType)
        {
            case ShakeType.Position:
                shaker = transform.DOShakePosition (_duration, magnitude, vibrato);
                break;
            case ShakeType.Rotation:
                shaker = transform.DOShakeRotation (_duration, magnitude, vibrato);
                break;
            case ShakeType.Scale:   
                shaker = transform.DOShakeScale (_duration, magnitude, vibrato);
                break;
        }
		if (delay > 0)
			shaker.SetDelay (delay);
        shaker.SetLoops (loop ? -1 : 0);
    }
    public void Cancel (bool finish) 
    {
        shaker?.Kill (finish);
    }
}
