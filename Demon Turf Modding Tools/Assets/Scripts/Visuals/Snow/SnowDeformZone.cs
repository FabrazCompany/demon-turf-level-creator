﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowDeformZone : MonoBehaviour
{
    [SerializeField] Shader drawShader;
    [SerializeField] MeshRenderer snowTerrain;
    Material drawMaterial;
    Material myMaterial;
    RenderTexture splatmap;
    RaycastHit groundHit;
    // [SerializeField] 
    int layerMask;
    List<SnowTrackSpawner> entities;

    void Awake () 
    {
        Debug.Log ("Awake");
        drawMaterial = new Material(drawShader);
        layerMask = LayerMask.GetMask("Platform");
        splatmap = new RenderTexture(1024,1024,0,RenderTextureFormat.ARGBFloat);
        myMaterial = snowTerrain.material;
        myMaterial.SetTexture("_Splat", splatmap);
        entities = new List<SnowTrackSpawner> ();
    }

    void OnTriggerEnter (Collider hit) 
    {
        var entity = hit.GetComponent<SnowTrackSpawner> ();
        if (!entity || entities.Contains (entity))
            return;
        
        entities.Add (entity);

        var snowball = entity.GetComponent<Snowball> ();
        snowball?.RegisterSnowZone (this);
    }

    void OnTriggerExit (Collider hit)
    {
        var entity = hit.GetComponent<SnowTrackSpawner> ();
        if (!entity || !entities.Contains (entity))
            return;
        
        entities.Remove (entity);

        var snowball = entity.GetComponent<Snowball> ();
        snowball?.ClearSnowZone ();
    }

    void Update () 
    {       
        bool cleanup = false; 
        for (int i = 0; i < entities.Count; i++) {
            if (entities[i] == null || !entities[i].collider.enabled)
            {
                cleanup = true;
                continue;
            }

            if (Physics.Raycast(entities[i].transform.position, -Vector3.up, out groundHit, entities[i].raycastLength, layerMask))
            {
                drawMaterial.SetVector("_Coordinate", new Vector4(groundHit.textureCoord.x, groundHit.textureCoord.y, 0,0));
                drawMaterial.SetFloat("_Strength", entities[i].brushStrength);
                drawMaterial.SetFloat("_Size", entities[i].brushSize);
                RenderTexture temp = RenderTexture.GetTemporary(splatmap.width,splatmap.height, 0, RenderTextureFormat.ARGBFloat);
                Graphics.Blit(splatmap, temp);
                Graphics.Blit(temp, splatmap, drawMaterial);
                RenderTexture.ReleaseTemporary(temp);
            }
        }

        if (cleanup)
            entities.RemoveAll (i => i == null);
    }
}
