﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowTrackSpawner : MonoBehaviour
{
    [Range(1,500)]
    public float brushSize = 1f;
    [Range(0,1)]
    public float brushStrength = .5f;

    public float raycastLength = 1;

    [HideInInspector, System.NonSerialized] public new Collider collider;

    void Awake () 
    {
        collider = GetComponent<Collider> ();
    }
}
