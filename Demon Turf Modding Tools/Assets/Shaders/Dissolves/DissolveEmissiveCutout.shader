Shader "Dissolves/Dissolve Emissive Cutout" {
    Properties {
        [Header (General)]
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Color ("Color", Color) = (1,1,1,1)
		[HDR] _Emission ("Emission ", Color) = (0,0,0)
        _Metallic ("Metallic", Range (0,1)) = 0
        _Smoothness ("Smoothness", Range (0, 1)) = 0

        [Header (Dissolve)]
        _SliceGuide("Slice Guide (RGB)", 2D) = "white" {}
        _SliceAmount("Slice Amount", Range(0.0, 1.0)) = 0
 
        _BurnSize("Burn Size", Range(0.0, 1.0)) = 0.15
        [NoScaleOffset] _BurnRamp("Burn Ramp (RGB)", 2D) = "white" {}
        _BurnColor("Burn Color", Color) = (1,1,1,1)
 
        _EmissionAmount("Emission amount", float) = 2.
		_Cutoff ("Alpha Cutoff", Range(0,1)) = .1
    }
    SubShader {
        Tags 
        { 
            "RenderType" = "Cutout" 
        }
        LOD 200
        Cull Back
        CGPROGRAM
        #pragma surface surf Standard fullforwardshadows alphatest:_Cutoff
        #pragma target 3.0
 
        fixed4 _Color;
		fixed4 _Emission;
        sampler2D _MainTex;
        half _Metallic;
        half _Smoothness;
        sampler2D _SliceGuide;
        sampler2D _BumpMap;
        sampler2D _BurnRamp;

        fixed4 _BurnColor;
        float _BurnSize;
        float _SliceAmount;
        float _EmissionAmount;
 
        struct Input {
            float2 uv_MainTex : TEXCOORD0;
            float2 uv_SliceGuide : TEXCOORD1;
        };


        UNITY_INSTANCING_BUFFER_START(Props)
        // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)


        void surf (Input IN, inout SurfaceOutputStandard o) {
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            half dissolve = tex2D(_SliceGuide, IN.uv_SliceGuide).rgb - _SliceAmount;
            clip(dissolve);
             
            if (dissolve < _BurnSize && _SliceAmount > 0) {
                o.Emission = tex2D(_BurnRamp, float2(dissolve * (1 / _BurnSize), 0)) * _BurnColor * _EmissionAmount;
            } else 
			{
				o.Emission = _Emission;
			}
 
            o.Albedo = c.rgb;
            
            o.Metallic = _Metallic;
            o.Smoothness = _Smoothness;

            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
