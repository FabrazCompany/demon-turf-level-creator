// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Projector' with 'unity_Projector'
// Upgrade NOTE: replaced '_ProjectorClip' with 'unity_ProjectorClip'

Shader "Projector/Opaque" {
	Properties
	{
		_ShadowTex ("Cookie", 2D) = "white" { TexGen ObjectLinear }
	}

	Subshader
	{
		Tags { "RenderType"="Opaque" }
		Pass
		{
			Fog { Mode Off }

			Tags {"LightMode"="ForwardBase"}

			ColorMask RGB
			Blend One SrcAlpha
			AlphaTest Greater 0.1


			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_fog_exp2
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
		
			struct v2f
			{
				float4 pos      : SV_POSITION;
				float4 uv       : TEXCOORD0;
			};
		
			sampler2D _ShadowTex;
			float4x4 unity_Projector;
			float4 _Color;
		
			v2f vert(float4 vertex : POSITION)
			{
				v2f o;
				o.pos = UnityObjectToClipPos (vertex);
				o.uv = mul (unity_Projector, vertex);
				return o;
			}
		
			half4 frag (v2f i) : COLOR
			{
				half4 tex = tex2Dproj(_ShadowTex, UNITY_PROJ_COORD(i.uv));
				tex.a = 1-tex.a;
				tex.rgb *= _LightColor0.rgb * 0.5;
				return tex;
			}
			ENDCG
	
		}
	}
}
