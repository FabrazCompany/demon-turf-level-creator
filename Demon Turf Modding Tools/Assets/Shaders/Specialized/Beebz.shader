﻿Shader "Custom/Beebz"
{
    Properties
    {
        [HideInInspector] _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
		_SwapTex("Color Data", 2D) = "transparent" {}
        _Cutoff ("Alpha Cutoff", Range(0,1)) = 0.5
		_Lighten("Sprite Lightener", Float) = 1.5

        //hair
        [Header(Hair Charge)]
		_ChargeTex ("Charge Data", 2D) = "transparent" {}
        _HairCharge ("Charge Level", Range (0,1)) = 0
        [HideInInspector] _HairEmissionScale ("Emission Scale", Float) = 0.1
        [HideInInspector] _Emission ("Emission", Color) = (1,1,1,1)

		[Header (Water Shading)]
		_DampTint ("Damp Tint", Color) = (.6, .6, .6, 1)
		_DampHeight ("Damp Height", Range (0,1)) = 1
		_DampAccentTex ("Damp Accent Texture", 2D) = "white" {}
		_DampAccentScale ("Damp Accent Scale", Float) = .3
		_DampAccentMag ("Damp Accent Magnitude", Float) = .3

		_DampSpriteOffsetY ("Damp Tex Offset", Float) = 0
		_DampSpriteScaleY ("Damp Tex Scale", Float) = 1


        [Header (Obstruction Outline)]
        _OutlineCutoff ("Alpha Cutoff", Range (0,1)) = 0.2
        _OutlineColor ("Color", Color) = (0,0,0,1)
        _OutlineScale ("Scale", Float) = 10
        [NoScaleOffset]_OutlineTex ("Accent", 2D) = "white" { }

        [Header (Proximity Dithering)]
        _DitherStrength ("Strength", Range (0,1)) = 1
        _DitherColor ("Color", Color) = (0,0,0,1)
        _DitherColorBuffer ("Color Ignored Buffer", Range (0,1)) = .3
        _DitherScale ("Scale", Float) = 10
		
		// _Smoothness ("Smoothness", Range (0, 1)) = 0
		// _Specular ("Specular", Range (0, 1)) = 0 
		_SwapColor0 ("Swap Color 0", Color) = (1,1,1,1)
		_SwapColor1 ("Swap Color 1", Color) = (1,1,1,1)
		_SwapColor2 ("Swap Color 2", Color) = (1,1,1,1)
		_SwapColor3 ("Swap Color 3", Color) = (1,1,1,1)
		_SwapColor4 ("Swap Color 4", Color) = (1,1,1,1)
		_SwapColor5 ("Swap Color 5", Color) = (1,1,1,1)
		_SwapColor6 ("Swap Color 6", Color) = (1,1,1,1)
		_SwapTolerance ("Swap Tolerance", Range (0, 1)) = 0.2
    }
    SubShader
    {
        Tags
        {
            "IgnoreProjector" = "True"
            "Queue" = "Geometry"
            "PreviewType" = "Plane"

        }
        
        // =================================
        //  Draw Sprite
        // =================================
        LOD 200
        Cull Off
        ZTest Less

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows vertex:vert alphatest:_Cutoff
        #pragma target 3.0

        #include "UnitySprites.cginc"

		
		struct Input
        {
			float2 sprite_uv;
            fixed4 color : COLOR;

            float4 screenPos;
        };

		fixed _Lighten;
        fixed3 _Emission;
        fixed _HairCharge;
        fixed4 _HairColor1;
        fixed4 _HairColor2;
        fixed4 _ChangedHairColor1;
        fixed4 _ChangedHairColor2;
        fixed _HairEmissionScale;
		
		fixed4 _AtlasPosition;
		fixed4 _DampTint;
		fixed _DampHeight;
		sampler2D _DampAccentTex;
		fixed _DampAccentScale;
		fixed _DampAccentMag;

        fixed _DitherStrength;
        fixed3 _DitherColor;
        fixed _DitherScale;
        fixed _DitherColorBuffer;

		sampler2D _SwapTex;
		sampler2D _ChargeTex;

		fixed4 _SwapColor0;
		fixed4 _SwapColor1;
		fixed4 _SwapColor2;
		fixed4 _SwapColor3;
		fixed4 _SwapColor4;
		fixed4 _SwapColor5;
		fixed4 _SwapColor6;
		fixed _SwapTolerance;
		
		
		
		fixed4 _MainTex_TexelSize;
        fixed4 _MainTex_ST;
        void vert (inout appdata_full v, out Input o)
        {
            UNITY_INITIALIZE_OUTPUT(Input, o);
            o.color = v.color * _Color * _RendererColor;
			o.sprite_uv = v.texcoord.xy;
        }


        void surf (Input IN, inout SurfaceOutputStandard o)
        {
			// setting up uvs
			float2 spriteUV = TRANSFORM_TEX (IN.sprite_uv, _MainTex);
			fixed4 atlasOffsetScale = _AtlasPosition * _MainTex_TexelSize.xyxy;
			fixed2 normalizedUV = (IN.sprite_uv - atlasOffsetScale.xy) / atlasOffsetScale.zw;

			
			fixed4 spriteCol = SampleSpriteTexture (spriteUV);	// base sprite colors


			fixed4 c = spriteCol;
			if (length (abs(spriteCol.rgb - _SwapColor0.rgb)) < _SwapTolerance)
			{
				c = _SwapColor0;
			}
			else if (length (abs(spriteCol.rgb - _SwapColor1.rgb)) < _SwapTolerance)
			{
				c = _SwapColor1;
			}
			else if (length (abs(spriteCol.rgb - _SwapColor2.rgb)) < _SwapTolerance)
			{
				c = _SwapColor2;
			}
			else if (length (abs(spriteCol.rgb - _SwapColor3.rgb)) < _SwapTolerance)
			{
				c = _SwapColor3;
			}
			else if (length (abs(spriteCol.rgb - _SwapColor4.rgb)) < _SwapTolerance)
			{
				c = _SwapColor4;
			}
			else if (length (abs(spriteCol.rgb - _SwapColor5.rgb)) < _SwapTolerance)
			{
				c = _SwapColor5;
			}
			else if (length (abs(spriteCol.rgb - _SwapColor6.rgb)) < _SwapTolerance)
			{
				c = _SwapColor6;
			}

			fixed4 swapCol = tex2D(_SwapTex, float2(spriteCol.x, 0));	// dye swap colors
			c = lerp(spriteCol, swapCol, swapCol.a);
			// fixed4 c = lerp(spriteCol, swapCol, swapCol.a);	

			fixed4 hairChargeCol = tex2D(_ChargeTex, float2(spriteCol.x, 0));	// hair charge color
			c = lerp (c, hairChargeCol, hairChargeCol.a * _HairCharge);

			// damp effects
			fixed4 dampAccent = tex2D (_DampAccentTex, normalizedUV * _DampAccentScale);
			fixed dampHeight  = _DampHeight + dampAccent.y * _DampAccentMag;
			c = lerp (c, c * _DampTint, normalizedUV.y  < dampHeight);
			
			
			// emission from hair charge
			fixed4 hairEmission = lerp (fixed4(0,0,0,0), hairChargeCol * _HairEmissionScale, hairChargeCol.a * _HairCharge);
			o.Emission = max (fixed4(_Emission,0), hairEmission);

            float2 screenUV = IN.screenPos.xy / IN.screenPos.w;
            screenUV.x *= (_ScreenParams.x / _ScreenParams.y);
            fixed grid = _DitherScale;
            float2 scaledUV = screenUV * grid;
            float2 gridUV = frac(scaledUV);
            
            half dist = distance (gridUV, fixed2 (0.5,0.5));
            half pwidth = fwidth(dist);
            half edge = 1;
            float circles = smoothstep(edge, edge - pwidth, max (dist / (1 - _DitherStrength), 0));

			c.rgb *= _Lighten;
            o.Albedo = lerp ((c * IN.color).rgb, _DitherColor, max (0, _DitherStrength - _DitherColorBuffer));
            o.Alpha = min (c.a * IN.color.a, circles);
        }
        ENDCG

        // =================================
        //  Obstruction Dithering Effect
        // =================================
        ZTest Greater

        CGPROGRAM
        #pragma surface surf Lambert alphatest:_OutlineCutoff

        sampler2D _MainTex;
        fixed4 _OutlineColor;
        sampler2D _OutlineTex;
        fixed _OutlineScale;
        
        struct Input {
            float2 uv_MainTex : TEXCOORD0;
            float4 screenPos : TEXCOORD2;
        };
        
        void surf (Input IN, inout SurfaceOutput o)
        {
            half4 primaryTex = tex2D (_MainTex, IN.uv_MainTex);
            float2 screenUV = (IN.screenPos.xy / IN.screenPos.w);
            screenUV.x *= (_ScreenParams.x / _ScreenParams.y);
            half4 secondaryTex = tex2D (_OutlineTex, screenUV * _OutlineScale);
            o.Albedo = secondaryTex.rgb * _OutlineColor;
            o.Alpha = min (primaryTex.a, secondaryTex.a * _OutlineColor.a);
        }
        ENDCG
    }
    FallBack "Diffuse"
}
