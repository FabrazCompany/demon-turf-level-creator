Shader "Custom/Unlit Surface Cutout" 
{
    Properties 
    {
        _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
        _Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
        _Emission ("Emission", Range (0,20)) = 0.5
		[HideInInspector] _Color ("Color", Color) = (1,1,1,1)
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
    }
    SubShader 
    {
        Tags 
        { 
            "Queue"="Geometry" 
            "IgnoreProjector"="True" 
            "RenderType"="Cutout"
        }
        LOD 100

        Lighting Off
        

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Lambert vertex:vert alphatest:_Cutoff
        #pragma target 3.0
        #include "UnitySprites.cginc"

        struct Input
        {
            float2 uv_MainTex;
			fixed4 color;
        };
        fixed4 LightingNoLighting(SurfaceOutput s, fixed3 lightDir, fixed atten)
        {
            return fixed4 (s.Albedo, s.Alpha);
        }

        half _Glossiness;
        half _Metallic;
        fixed _Emission;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

		void vert (inout appdata_full v, out Input o) {
            UNITY_INITIALIZE_OUTPUT(Input, o);
            // o.color = v.color * _Color * _RendererColor;
			o.color = v.color * _Color;
        }

        void surf (Input IN, inout SurfaceOutput o)
        {
            fixed4 c = SampleSpriteTexture (IN.uv_MainTex);
            o.Albedo = c.rgb * _Emission * IN.color;
            o.Alpha = c.a;
        }
        ENDCG
    }

}